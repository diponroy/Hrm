﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class AllowanceData : EntityWithLogRepository<Allowance, AllowanceLog, AllowanceMapper>
    {
        public AllowanceData() : this(new HrmContext())
        {
        }

        public AllowanceData(DbContext context) : base(context)
        {
        }

        public bool AnyActiveOrInactiveTitle(string title)
        {
            return AllActiveOrInactive().Any(x => x.Title.Equals(title));
        }

        public bool AnyActiveOrInactiveTitleExcept ( string title, long id )
        {
            return AllActiveOrInactive().Any(x => x.Id != id && x.Title.Equals(title));
        }

        public override void Add(Allowance allowance)
        {
            base.Add(allowance, MappedLog(allowance));
        }

        public override IQueryable<Allowance> TableWithDetail()
        {
            return Table.Include(x => x.AllowanceType);
        }

        public override IQueryable<AllowanceLog> LogWithDetail()
        {
            return LogTable.Include(x => x.AllowanceTypeLog);
        }

        public override void Remove(Allowance allowance)
        {
            base.Remove(allowance, MappedLog(allowance));
        }


        public override void Replace(Allowance allowance)
        {
            base.Replace(allowance, MappedLog(allowance));
        }
    }
}
