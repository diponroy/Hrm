﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class AllowanceTypeData : EntityWithLogRepository<AllowanceType, AllowanceTypeLog, AllowanceTypeMapper>
    {
        public AllowanceTypeData() : this(new HrmContext())
        {
        }

        public AllowanceTypeData(DbContext context) : base(context)
        {
        }


        public override void Add(AllowanceType entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(AllowanceType entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(AllowanceType entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<AllowanceType> TableWithDetail()
        {
            return Table;
        }

        public override IQueryable<AllowanceTypeLog> LogWithDetail()
        {
            return LogTable;
        }

        public bool AnyActiveOrInactiveName(string name)
        {
            return AllActiveOrInactive().Any(x => x.Name.Equals(name));
        }

        public bool AnyActiveOrInactiveNameExcept(string name, long id)
        {
            return AllActiveOrInactive().Any(x => x.Id != id && x.Name.Equals(name));
        }
    }
}                     
