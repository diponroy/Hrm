﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class AppraisalIndicatorData : EntityWithLogRepository<AppraisalIndicator, AppraisalIndicatorLog, AppraisalIndicatorMapper>
    {
        public AppraisalIndicatorData() : this(new HrmContext())
        {
        }

        public AppraisalIndicatorData(DbContext context) : base(context)
        {
        }

        public bool AnyActiveOrInactiveTitle ( string title )
        {
            return AllActiveOrInactive().Any(x => x.Title.Equals(title));
        }

        public bool AnyActiveOrInactiveTitleExcept ( string title, long id )
        {
            return AllActiveOrInactive().Any(x => x.Id != id && x.Title.Equals(title));
        }

        public override void Add(AppraisalIndicator entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(AppraisalIndicator entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(AppraisalIndicator entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<AppraisalIndicator> TableWithDetail()
        {
            return Table.Include(x => x.ParentAppraisalIndicator);
        }

        public override IQueryable<AppraisalIndicatorLog> LogWithDetail()
        {
            return LogTable.Include(x => x.ParentAppraisalIndicatorLog);
        }

        public DbRawSqlQuery<AppraisalIndicator> ActiveChilds ( long id )
        {
            var clientIdParameter = new SqlParameter("@id", id);
            DbRawSqlQuery<AppraisalIndicator> value = Context.Database.SqlQuery<AppraisalIndicator>(@"
            DECLARE @testId BIGINT;
            SET @testId=@id;
            WITH tblChild AS
                (
                    SELECT [Id]
                           ,[Title]                           
		                   ,[AppraisalIndicator_Id_AsParentId] AS ParentId
                           ,[MaxWeight]
                           ,[Remarks]
		                   ,[Status]
                    FROM (SELECT * FROM AppraisalIndicator WHERE Status = 0) AS ActiveEntity
                    WHERE AppraisalIndicator_Id_AsParentId = @testId
                    UNION ALL
                    SELECT  ActiveChilds.[Id]
                            ,ActiveChilds.[Title]                            
                            ,ActiveChilds.[AppraisalIndicator_Id_AsParentId] AS ParentId
                            ,ActiveChilds.[MaxWeight]
                            ,ActiveChilds.[Remarks]                            
                            ,ActiveChilds.[Status] 
                 FROM (SELECT * FROM AppraisalIndicator WHERE Status = 0) AS ActiveChilds  
		                JOIN tblChild  ON ActiveChilds.AppraisalIndicator_Id_AsParentId = tblChild.Id
            )
            SELECT *
            FROM tblChild
            OPTION(MAXRECURSION 32767)
            ", clientIdParameter);
            return value;
        }
    }
}
