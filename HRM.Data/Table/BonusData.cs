﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class BonusData : EntityWithLogRepository<Bonus,BonusLog,BonusMapper>
    {
        public BonusData() : this(new HrmContext())
        {
        }

        public BonusData ( DbContext context ) : base(context)
        {
        }

        public bool AnyActiveOrInactiveTitle ( string title )
        {
            return AllActiveOrInactive().Any(x => x.Title.Equals(title));
        }

        public bool AnyActiveOrInactiveTitleExcept ( string title, long id )
        {
            return AllActiveOrInactive().Any(x => x.Id != id && x.Title.Equals(title));
        }

        public override void Add(Bonus entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(Bonus entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(Bonus entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<Bonus> TableWithDetail()
        {
            return Table.Include(x=>x.BonusType);
        }

        public override IQueryable<BonusLog> LogWithDetail()
        {
            return LogTable.Include(x => x.BonusTypeLog);
        }
    }
}
