﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class BonusPaymentDateData : EntityWithLogRepository<BonusPaymentDate,BonusPaymentDateLog,BonusPaymentDateMapper>
    {
        public BonusPaymentDateData() : this(new HrmContext())
        {
        }

        public BonusPaymentDateData ( DbContext context )
            : base(context)
        {
        }

        public override void Add(BonusPaymentDate entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(BonusPaymentDate entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(BonusPaymentDate entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<BonusPaymentDate> TableWithDetail()
        {
            return Table.Include(x => x.Bonus);
        }

        public override IQueryable<BonusPaymentDateLog> LogWithDetail()
        {
            return LogTable.Include(x => x.BonusLog);
        }
    }
}
