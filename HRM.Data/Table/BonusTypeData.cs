﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class BonusTypeData : EntityWithLogRepository<BonusType, BonusTypeLog, BonusTypeMapper>
    {
        public BonusTypeData() : this(new HrmContext())
        {
        }

        public BonusTypeData(DbContext context) : base(context)
        {
        }

        public override void Add(BonusType entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(BonusType entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(BonusType entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<BonusType> TableWithDetail()
        {
            return Table;
        }

        public override IQueryable<BonusTypeLog> LogWithDetail()
        {
            return LogTable;
        }

        public bool AnyActiveOrInactiveName(string name)
        {
            return AllActiveOrInactive().Any(x => x.Name.Equals(name));
        }

        public bool AnyActiveOrInactiveNameExcept(string name, long id)
        {
            return AllActiveOrInactive().Any(x => x.Id != id && x.Name.Equals(name));
        }
    }
}
