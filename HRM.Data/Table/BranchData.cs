﻿using System.Data.Entity;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class BranchData : EntityWithLogRepository<Branch, BranchLog, BranchMapper>
    {
        public BranchData() : this(new HrmContext())
        {
        }

        public BranchData(DbContext context) : base(context)
        {
        }

        public Branch GetByName(string name)
        {
            return Table.Single(x => x.Name.Equals(name));
        }

        public override void Add(Branch branch)
        {
            base.Add(branch, MappedLog(branch));
        }

        public override void Replace(Branch branch)
        {
            base.Replace(branch, MappedLog(branch));
        }

        public override void Remove(Branch branch)
        {
            base.Remove(branch, MappedLog(branch));
        }

        public override IQueryable<Branch> TableWithDetail()
        {
            return base.Table;
        }

        public override IQueryable<BranchLog> LogWithDetail()
        {
            return base.LogTable;
        }

        public bool AnyName(string name)
        {
            return Table.Any(x => x.Name.Equals(name));
        }

        public bool AnyNameExceptId(string name, long id)
        {
            return Table.Any(x => x.Id != id && x.Name.Equals(name));
        }

        public IQueryable<Branch> AllActiveAndNotClosed()
        {
            return AllActive().Where(x => x.DateOfClosing == null);
        }
    }
}
