﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CandidateAchievementData : EntityWithLogRepository<CandidateAchievement, CandidateAchievementLog, CandidateAchievementMapper>
    {
        public CandidateAchievementData(DbContext context)
            : base(context)
        {
        }

        public override void Add(CandidateAchievement entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(CandidateAchievement entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(CandidateAchievement entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<CandidateAchievement> TableWithDetail()
        {
            return Table.Include(x => x.CandidateEmployment);
        }

        public override IQueryable<CandidateAchievementLog> LogWithDetail()
        {
            return LogTable.Include(x => x.CandidateEmploymentLog);
        }

        public IQueryable<CandidateAchievement> GetForCandidateEmployment(long candidateEmploymentId)
        {
            return All().Where(x => x.CandidateEmploymentId == candidateEmploymentId);
        }
    }
}
