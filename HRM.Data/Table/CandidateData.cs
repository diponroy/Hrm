﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CandidateData : EntityWithLogRepository<Candidate, CandidateLog, CandidateMapper>
    {
        public CandidateData(DbContext context) : base(context)
        {
        }

        public bool IsEmailUsed(string email)
        {
            return AllActiveOrInactive().Any(x => x.Email.Equals(email));
        }
        public bool IsEmailUsedExceptCandidate(string email, long candidateId)
        {
            return AllActiveOrInactive().Any(x => x.Email.Equals(email) && x.Id != candidateId);
        }

        public override void Add(Candidate entity)
        {
            if (IsEmailUsed(entity.Email))
            {
                throw new Exception("Email is in use.");
            }
            base.Add(entity, MappedLog(entity));
        }
        public override void Replace(Candidate entity)
        {
            if (IsEmailUsedExceptCandidate(entity.Email, entity.Id))
            {
                throw new Exception("Email is in use.");
            }
            base.Replace(entity, MappedLog(entity));
        }
        public override void Remove(Candidate entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<Candidate> TableWithDetail()
        {
            return Table;
        }

        public override IQueryable<CandidateLog> LogWithDetail()
        {
            return LogTable;
        }

        public string GenerateTrackNo()
        {
            var prefix = string.Format("MPR-{0}", DateTime.Now.Year);
            var preEntity = Table.Where(x => x.TrackNo.StartsWith(prefix)).OrderByDescending(x => x.Id).FirstOrDefault();

            long? lastCount = null;
            if (preEntity == null)
            {
                lastCount = 0;
            }
            else
            {
                var lastTrackNoWithPrefix = preEntity.TrackNo;
                lastCount = Convert.ToInt64(lastTrackNoWithPrefix.Split('-').ToArray()[2]);
            }

            return string.Format("{0}-{1}", prefix, ++lastCount);
        }
    }
}
