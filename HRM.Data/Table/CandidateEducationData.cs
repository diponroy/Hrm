﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CandidateEducationData : EntityWithLogRepository<CandidateEducation, CandidateEducationLog, CandidateEducationMapper>
    {
        public CandidateEducationData(DbContext context)
            : base(context)
        {
        }

        public IQueryable<CandidateEducation> GetForCandidate(long candidateId)
        {
            return All().Where(x => x.CandidateId == candidateId);
        }

        public override void Add(CandidateEducation entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(CandidateEducation entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(CandidateEducation entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<CandidateEducation> TableWithDetail()
        {
            return Table.Include(x => x.Candidate);
        }

        public override IQueryable<CandidateEducationLog> LogWithDetail()
        {
            return LogTable.Include(x => x.CandidateLog);
        }
    }
}
