﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CandidateEmploymentData : EntityWithLogRepository<CandidateEmployment, CandidateEmploymentLog, CandidateEmploymentMapper>
    {
        public CandidateEmploymentData(DbContext context)
            : base(context)
        {
        }

        public override void Add(CandidateEmployment entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(CandidateEmployment entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(CandidateEmployment entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<CandidateEmployment> TableWithDetail()
        {
            return Table.Include(x => x.Candidate);
        }

        public override IQueryable<CandidateEmploymentLog> LogWithDetail()
        {
            return LogTable.Include(x => x.CandidateLog);
        }

        public IQueryable<CandidateEmployment> GetForCandidate(long candidateId)
        {
            return All().Where(x => x.CandidateId == candidateId);
        }
    }
}
