﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CandidateLanguageData : EntityWithLogRepository<CandidateLanguage, CandidateLanguageLog, CandidateLanguageMapper>
    {
        public CandidateLanguageData(DbContext context)
            : base(context)
        {
        }

        public IQueryable<CandidateLanguage> GetForCandidate(long candidateId)
        {
            return All().Where(x => x.CandidateId == candidateId);
        }

        public override void Add(CandidateLanguage entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(CandidateLanguage entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(CandidateLanguage entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<CandidateLanguage> TableWithDetail()
        {
            return Table.Include(x => x.Candidate);
        }

        public override IQueryable<CandidateLanguageLog> LogWithDetail()
        {
            return LogTable.Include(x => x.CandidateLog);
        }
    }
}
