﻿using System;
using System.Data.Entity;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CandidateLoginData : EntityWithLogRepository<CandidateLogin, CandidateLoginLog, CandidateLoginMapper>
    {
        public CandidateLoginData(DbContext context) : base(context)
        {
        }

        public CandidateLogin GetBy(long candidateId)
        {
            return Table.Single(x => x.CandidateId == candidateId);
        }
        public bool IsLoginNameUsed(string loginName)
        {
            return AllActiveOrInactive().Any(x => x.LoginName.Equals(loginName));
        }

        public bool HasLogin(long candidateId)
        {
            return AllActiveOrInactive().Any(x => x.CandidateId == candidateId);
        }

        public bool IsLoginNameUsedExcept(string loginName, long candidateId)
        {
            return AllActiveOrInactive().Any(x => x.LoginName.Equals(loginName) && x.CandidateId != candidateId);
        }

        public override void Add(CandidateLogin entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public void ValidateAndAdd(CandidateLogin entity)
        {
            if (IsLoginNameUsed(entity.LoginName))
            {
                throw new Exception("Email is in use.");
            }

            if (HasLogin(entity.CandidateId))
            {
                throw new Exception("The candidate has a loging.");
            }
            Add(entity);        
        }

        public override void Replace(CandidateLogin entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public void ValidateAndReplace(CandidateLogin entity)
        {
            if (IsLoginNameUsedExcept(entity.LoginName, entity.Id))
            {
                throw new Exception("Email is in use.");
            }
            Replace(entity);
        }

        public override void Remove(CandidateLogin entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<CandidateLogin> TableWithDetail()
        {
            return Table.Include(x => x.Candidate);
        }

        public override IQueryable<CandidateLoginLog> LogWithDetail()
        {
            return LogTable.Include(x => x.CandidateLog);
        }
    }
}
