﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CandidateReferenceData : EntityWithLogRepository<CandidateReference, CandidateReferenceLog, CandidateReferenceMapper>
    {
        public CandidateReferenceData(DbContext context)
            : base(context)
        {
        }

        public IQueryable<CandidateReference> GetForCandidate(long candidateId)
        {
            return All().Where(x => x.CandidateId == candidateId);
        }

        public override void Add(CandidateReference entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(CandidateReference entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(CandidateReference entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<CandidateReference> TableWithDetail()
        {
            return Table.Include(x => x.Candidate);
        }

        public override IQueryable<CandidateReferenceLog> LogWithDetail()
        {
            return LogTable.Include(x => x.CandidateLog);
        }
    }
}
