﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CandidateSkillData : EntityWithLogRepository<CandidateSkill, CandidateSkillLog, CandidateSkillMapper>
    {
        public CandidateSkillData(DbContext context)
            : base(context)
        {
        }

        public override void Add(CandidateSkill entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(CandidateSkill entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(CandidateSkill entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<CandidateSkill> TableWithDetail()
        {
            return Table.Include(x => x.Candidate);
        }

        public override IQueryable<CandidateSkillLog> LogWithDetail()
        {
            return LogTable.Include(x => x.CandidateLog);
        }

        public IQueryable<CandidateSkill> GetForCandidate(long candidateId)
        {
            return All().Where(x => x.CandidateId == candidateId);
        }
    }
}
