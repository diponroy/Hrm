﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CandidateTrainingData : EntityWithLogRepository<CandidateTraining, CandidateTrainingLog, CandidateTrainingMapper>
    {
        public CandidateTrainingData(DbContext context)
            : base(context)
        {
        }

        public override void Add(CandidateTraining entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(CandidateTraining entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(CandidateTraining entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<CandidateTraining> TableWithDetail()
        {
            return Table.Include(x => x.Candidate);
        }

        public override IQueryable<CandidateTrainingLog> LogWithDetail()
        {
            return LogTable.Include(x => x.CandidateLog);
        }

        public IQueryable<CandidateTraining> GetForCandidate(long candidateId)
        {
            return All().Where(x => x.CandidateId == candidateId);
        }
    }
}
