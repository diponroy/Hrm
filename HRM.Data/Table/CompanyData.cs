﻿using System.Data.Entity;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class CompanyData : EntityWithLogRepository<Company, CompanyLog, CompanyMapper>
    {
        public CompanyData() : this(new HrmContext())
        {
        }

        public CompanyData(DbContext context) : base(context)
        {
        }

        public override void Add(Company entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(Company entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(Company entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<Company> TableWithDetail()
        {
            return Table;
        }

        public override IQueryable<CompanyLog> LogWithDetail()
        {
            return LogTable;
        }

        //public bool AnyActiveOrInactiveName(string name)
        //{
        //    return AllActiveOrInactive().Any(x => x.Name.Equals(name));
        //}

        public bool AnyActiveOrInactiveNameExcept(string name, long id)
        {
            return AllActiveOrInactive().Any(x => x.Id != id && x.Name.Equals(name));
        }

        public IQueryable<Company> AllActiveAndNotClosed()
        {
            return AllActive().Where(x => x.DateOfClosing == null);
        }
    }
}
