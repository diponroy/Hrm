﻿using System.Data.Entity;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class DepartmentData : EntityWithLogRepository<Department, DepartmentLog, DepartmentMapper>
    {
        public DepartmentData() : this(new HrmContext())
        {
        }

        public DepartmentData(DbContext context) : base(context)
        {
        }

        public Department GetByName(string name)
        {
            return Table.Single(x => x.Name.Equals(name));
        }

        public bool AnyName(string name)
        {
            return Table.Any(x => x.Name.Equals(name));
        }

        public bool AnyNameExceptDepartment(long id, string name)
        {
            return Table.Any(x => x.Id != id && x.Name.Equals(name));
        }

        public override void Add(Department entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(Department entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(Department entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<Department> TableWithDetail()
        {
            return Table;
        }

        public override IQueryable<DepartmentLog> LogWithDetail()
        {
            return LogTable;
        }

        public IQueryable<Department> AllActiveAndNotClosed()
        {
            return AllActive().Where(x => x.DateOfClosing == null);
        }
    }
}
