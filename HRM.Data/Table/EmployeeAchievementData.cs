﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeAchievementData :
        EntityWithLogRepository<EmployeeAchievement, EmployeeAchievementLog, EmployeeAchievementMapper>
    {
        public EmployeeAchievementData() : this(new HrmContext())
        {
        }

        public EmployeeAchievementData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeAchievement entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeeAchievement entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeAchievement entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeAchievement> TableWithDetail()
        {
            return Table
                .Include(x => x.EmployeeAppraisal)
                .Include(x => x.EmployeeAppraisal.EmployeeAssignedAppraisal)
                .Include(x => x.EmployeeAppraisal.EmployeeAssignedAppraisal.AppraisalIndicator);
        }

        public override IQueryable<EmployeeAchievementLog> LogWithDetail()
        {
            return LogTable
                .Include(x => x.EmployeeAppraisalLog)
                .Include(x => x.EmployeeAppraisalLog.EmployeeAssignedAppraisalLog)
                .Include(x => x.EmployeeAppraisalLog.EmployeeAssignedAppraisalLog.AppraisalIndicatorLog); ;
        }

        public bool AnyActiveOrInactiveTitle ( string title )
        {
            return AllActiveOrInactive().Any(x => x.Title.Equals(title));
        }

        public IQueryable<EmployeeAchievement> GetByEmployeeId(long id)
        {
            return AllActiveOrInactive().Where(x => x.EmployeeId == id);
        }

        public ICollection<EmployeeAchievementLog> GetLogs(long id)
        {
            List<EmployeeAchievementLog> empLogs = new EmployeeAchievementData().Detail.AllLog(id).ToList();
            return empLogs;
        }
    }
}
