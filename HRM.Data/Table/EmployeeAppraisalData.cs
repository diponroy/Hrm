﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeAppraisalData : EntityWithLogRepository<EmployeeAppraisal, EmployeeAppraisalLog, EmployeeAppraisalMapper>
    {
        public EmployeeAppraisalData ():this(new HrmContext())
        {
        }
        public EmployeeAppraisalData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeAppraisal entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeAppraisal entity)
        {
           base.Replace(entity,MappedLog(entity));
        }

        public override void Remove(EmployeeAppraisal entity)
        {
            base.Remove(entity,MappedLog(entity));
        }

        public override IQueryable<EmployeeAppraisal> TableWithDetail()
        {
            return Table.Include(x => x.EmployeeAssignedAppraisal);
        }

        public override IQueryable<EmployeeAppraisalLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeAssignedAppraisalLog);
        }
    }
}
