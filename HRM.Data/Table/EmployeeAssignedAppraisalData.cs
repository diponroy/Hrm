﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeAssignedAppraisalData: EntityWithLogRepository<EmployeeAssignedAppraisal,EmployeeAssignedAppraisalLog,EmployeeAssignedAppraisalMapper>
    {
        public EmployeeAssignedAppraisalData(): this(new HrmContext())
        {
        }

        public EmployeeAssignedAppraisalData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeAssignedAppraisal entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeAssignedAppraisal entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeAssignedAppraisal entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeAssignedAppraisal> TableWithDetail()
        {
            return Table.Include(x => x.AppraisalIndicator)
                        .Include(x => x.EmployeeWorkStationUnit)
                        .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit)
                        .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Company)
                        .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Branch)
                        .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Department);
        }

        public override IQueryable<EmployeeAssignedAppraisalLog> LogWithDetail()
        {
            return LogTable.Include(x => x.AppraisalIndicatorLog)
                .Include(x => x.EmployeeWorkStationUnitLog)
                .Include(x => x.EmployeeWorkStationUnitLog.WorkStationUnitLog)
                .Include(x => x.EmployeeWorkStationUnitLog.WorkStationUnitLog.CompanyLog)
                .Include(x => x.EmployeeWorkStationUnitLog.WorkStationUnitLog.BranchLog)
                .Include(x => x.EmployeeWorkStationUnitLog.WorkStationUnitLog.DepartmentLog);
                   
        }
    }
}
