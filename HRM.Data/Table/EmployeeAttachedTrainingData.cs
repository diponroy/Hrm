﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeAttachedTrainingData : EntityWithLogRepository<EmployeeAttachedTraining, EmployeeAttachedTrainingLog, EmployeeAttachedTrainingMapper>
    {
        public EmployeeAttachedTrainingData (): this(new HrmContext()) 
        {
        }
        public EmployeeAttachedTrainingData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeAttachedTraining entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeAttachedTraining entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeAttachedTraining entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeAttachedTraining> TableWithDetail()
        {
            return Table.Include(x => x.Employee).Include(x => x.EmployeeTraining);
        }

        public override IQueryable<EmployeeAttachedTrainingLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeLog).Include(x => x.EmployeeTrainingLog);
        }
    }
}
