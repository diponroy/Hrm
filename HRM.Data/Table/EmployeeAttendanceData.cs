﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data
{
    public class EmployeeAttendanceData:EntityWithLogRepository<EmployeeAttendance,EmployeeAttendanceLog,EmployeeAttendanceMapper>
    {
        public EmployeeAttendanceData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeAttendance entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeAttendance entity)
        {
            base.Replace(entity, MappedLog(entity));

        }

        public override void Remove(EmployeeAttendance entity)
        {
            base.Remove(entity, MappedLog(entity));

        }

        public override IQueryable<EmployeeAttendance> TableWithDetail()
        {
            return Table.Include(x => x.EmployeeWorkStationUnit)
                .Include(x=>x.EmployeeWorkStationUnit.Employee);
        }

        public override IQueryable<EmployeeAttendanceLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeWorkStationUnitLog)
                .Include(x => x.EmployeeWorkStationUnitLog.EmployeeLog);

        }
    }
}
