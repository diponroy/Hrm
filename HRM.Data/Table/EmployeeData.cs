﻿using System;
using System.Data.Entity;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeData : EntityWithLogRepository<Employee, EmployeeLog, EmployeeMapper>
    {
        public EmployeeData() : this(new HrmContext())
        {
        }

        public EmployeeData(DbContext context) : base(context)
        {
        }

        public Employee GetByTrackNo(string trackNo)
        {
            return Table.Single(x => x.TrackNo.Equals(trackNo));
        }

        public override void Add(Employee employee)
        {
            base.Add(employee, MappedLog(employee));
        }

        public override void Replace(Employee employee)
        {
            base.Replace(employee, MappedLog(employee));
        }

        public override void Remove(Employee entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<Employee> TableWithDetail()
        {
            return Table;
        }

        public override IQueryable<EmployeeLog> LogWithDetail()
        {
            return LogTable;
        }

        public bool AnyEmail(string email)
        {
            return Table.Any(x => x.Email.Equals(email));
        }

        public bool AnyEmailExceptEmployee(long id, string email)
        {
            return Table.Any(x => x.Id != id && x.Email.Equals(email));
        }

        public string GenerateTrackNo()
        {
            var prefix = string.Format("EMP-{0}", DateTime.Now.Year);
            var preEntity = Table.Where(x => x.TrackNo.StartsWith(prefix)).OrderByDescending(x => x.Id).FirstOrDefault();

            long? lastCount = null;
            if (preEntity == null)
            {
                lastCount = 0;
            }
            else
            {
                var lastTrackNoWithPrefix = preEntity.TrackNo;
                lastCount = Convert.ToInt64(lastTrackNoWithPrefix.Split('-').ToArray()[2]);
            }

            return string.Format("{0}-{1}", prefix, ++lastCount);
        }
    }
}
