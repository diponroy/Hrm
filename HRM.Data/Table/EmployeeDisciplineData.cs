﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeDisciplineData:EntityWithLogRepository<EmployeeDiscipline, EmployeeDisciplineLog, EmployeeDisciplineMapper>
    {
        public EmployeeDisciplineData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeDiscipline entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeDiscipline entity)
        {
            base.Replace(entity, MappedLog(entity));

        }

        public override void Remove(EmployeeDiscipline entity)
        {
            base.Remove(entity, MappedLog(entity));

        }

        public override IQueryable<EmployeeDiscipline> TableWithDetail()
        {
            return Table.Include(x => x.Employee).Include(x => x.Department);
        }

        public override IQueryable<EmployeeDisciplineLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeLog).Include(x => x.DepartmentLog);
        }
    }
}
