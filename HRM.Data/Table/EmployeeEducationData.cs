﻿using System.Data.Entity;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeEducationData :
        EntityWithLogRepository<EmployeeEducation, EmployeeEducationLog, EmployeeEducationMapper>
    {
        public EmployeeEducationData() : this(new HrmContext())
        {
        }

        public EmployeeEducationData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeEducation entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeeEducation entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeEducation entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeEducation> TableWithDetail()
        {
            return Table.Include(x => x.Employee);
        }

        public override IQueryable<EmployeeEducationLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeLog);
        }
    }
}
