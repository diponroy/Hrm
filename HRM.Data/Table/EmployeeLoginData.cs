﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeLoginData : EntityWithLogRepository<EmployeeLogin, EmployeeLoginLog, EmployeeLoginMapper>
    {

        public EmployeeLoginData() : this(new HrmContext())
        {
        }
        public EmployeeLoginData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeLogin entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeeLogin entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeLogin entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeLogin> TableWithDetail()
        {
            return Table.Include(x => x.Employee);
        }

        public override IQueryable<EmployeeLoginLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeLog);
        }

        public EmployeeLogin GetByEmployeeId(long employeeId)
        {
            return Table.Single(x => x.EmployeeId == employeeId);
        }
    }
}
