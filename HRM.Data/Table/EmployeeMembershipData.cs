﻿using System;
using System.Data.Entity;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeMembershipData :
        EntityWithLogRepository<EmployeeMembership, EmployeeMembershipLog, EmployeeMembershipMapper>
    {
        public EmployeeMembershipData() : this(new HrmContext())
        {
        }

        public EmployeeMembershipData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeMembership entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeeMembership entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeMembership entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeMembership> TableWithDetail()
        {
            throw new NotImplementedException();
        }

        public override IQueryable<EmployeeMembershipLog> LogWithDetail()
        {
            throw new NotImplementedException();
        }
    }
}
