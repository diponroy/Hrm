﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeePayrollInfoData :EntityWithLogRepository<EmployeePayrollInfo, EmployeePayrollInfoLog, EmployeePayrollInfoMapper>
    {
        public EmployeePayrollInfoData() : this(new HrmContext())
        {
        }

        public EmployeePayrollInfoData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeePayrollInfo entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeePayrollInfo entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeePayrollInfo entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeePayrollInfo> TableWithDetail()
        {
            return Table.Include(x => x.Employee);
        }

        public override IQueryable<EmployeePayrollInfoLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeLog);
        }
    }
}
