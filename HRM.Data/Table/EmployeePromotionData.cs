﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeePromotionData : EntityWithLogRepository<EmployeePromotion, EmployeePromotionLog, EmployeePromotionMapper>
    {
        public EmployeePromotionData() : this(new HrmContext())
        {
        }

        public EmployeePromotionData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeePromotion entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeePromotion entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeePromotion entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeePromotion> TableWithDetail()
        {
            return Table
                .Include(x=>x.EmployeeWorkStationUnit)
                .Include(x=>x.EmployeeType)
                .Include(x=>x.WorkStationUnit)
                .Include(x=>x.IncrementSalaryStructure);
        }

        public override IQueryable<EmployeePromotionLog> LogWithDetail()
        {
            return LogTable
                .Include(x => x.EmployeeWorkStationUnitLog)
                .Include(x => x.EmployeeTypeLog)
                .Include(x => x.WorkStationUnitLog)
                .Include(x => x.IncrementSalaryStructureLog);
        }
    }
}
