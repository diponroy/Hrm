﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeReportingData : EntityWithLogRepository<EmployeeReporting, EmployeeReportingLog, EmployeeReportingMapper>
    {
        public EmployeeReportingData() : this(new HrmContext())
        {
        }

        public EmployeeReportingData(DbContext context)
            : base(context)
        {
        }

        public override void Add(EmployeeReporting entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeeReporting entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeReporting entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeReporting> TableWithDetail()
        {
            return Table
                .Include(x => x.EmployeeWorkStationUnit)
                .Include(x => x.ParentEmployeeWorkStationUnit);
        }

        public override IQueryable<EmployeeReportingLog> LogWithDetail()
        {
            return LogTable
                .Include(x=>x.EmployeeWorkStationUnitLog)
                .Include(x=>x.ParentEmployeeWorkStationUnitLog);
        }
    }
}
