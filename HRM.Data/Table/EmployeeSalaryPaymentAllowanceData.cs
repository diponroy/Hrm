﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeSalaryPaymentAllowanceData : EntityWithLogRepository<EmployeeSalaryPaymentAllowance, EmployeeSalaryPaymentAllowanceLog,EmployeeSalaryPaymentAllowanceMapper>
    {
        public EmployeeSalaryPaymentAllowanceData ():this(new HrmContext())
        {
        }
        public EmployeeSalaryPaymentAllowanceData ( DbContext context )
            : base(context)
        {
        }
        public override void Add ( EmployeeSalaryPaymentAllowance entity )
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace ( EmployeeSalaryPaymentAllowance entity )
        {
            throw new NotImplementedException();
        }

        public override void Remove ( EmployeeSalaryPaymentAllowance entity )
        {
            base.Remove(entity,MappedLog(entity));
        }

        public override IQueryable<EmployeeSalaryPaymentAllowance> TableWithDetail ()
        {
            return Table.Include(x => x.EmployeeSalaryPayment);
        }

        public override IQueryable<EmployeeSalaryPaymentAllowanceLog> LogWithDetail ()
        {
            return LogTable.Include(x => x.EmployeeSalaryPaymentLog);
        }
    }
}
