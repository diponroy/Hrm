﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeSalaryPaymentBonusData : EntityWithLogRepository<EmployeeSalaryPaymentBonus, EmployeeSalaryPaymentBonusLog, EmployeeSalaryPaymentBonusMapper>
    {
        public EmployeeSalaryPaymentBonusData ():this(new HrmContext())
        {
        }
        public EmployeeSalaryPaymentBonusData ( DbContext context )
            : base(context)
        {
        }
        public override void Add ( EmployeeSalaryPaymentBonus entity )
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace ( EmployeeSalaryPaymentBonus entity )
        {
            throw new NotImplementedException();
        }

        public override void Remove ( EmployeeSalaryPaymentBonus entity )
        {
            base.Remove(entity,MappedLog(entity));
        }

        public override IQueryable<EmployeeSalaryPaymentBonus> TableWithDetail ()
        {
            return Table.Include(x => x.EmployeeSalaryPayment);
        }

        public override IQueryable<EmployeeSalaryPaymentBonusLog> LogWithDetail ()
        {
            return LogTable.Include(x => x.EmployeeSalaryPaymentLog);
        }
    }
}
