﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeSalaryPaymentData :EntityWithLogRepository<EmployeeSalaryPayment, EmployeeSalaryPaymentLog, EmployeeSalaryPaymentMapper>
    {
        public EmployeeSalaryPaymentData(DbContext context) : base(context)
        {
        }

        public bool AnyActiveOrInactiveTrackNo ( string trackNo )
        {
            return AllActiveOrInactive().Any(x => x.TrackNo.Equals(trackNo));
        }  
        public bool AnyActiveOrInactiveTrackNoExcept ( string trackNo, long id )
        {
            return AllActiveOrInactive().Any(x => x.TrackNo.Equals(trackNo) && ( x.Id != id ));
        }
        
        public override void Add(EmployeeSalaryPayment entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeSalaryPayment entity)
        {
            throw new NotImplementedException();
        }

        public override void Remove(EmployeeSalaryPayment entity)
        {
            throw new NotImplementedException();
        }

        public override IQueryable<EmployeeSalaryPayment> TableWithDetail()
        {
            return Table.Include(x => x.EmployeeSalaryStructure)
                        .Include(x => x.EmployeePayrollInfo)
                        .Include(x => x.EmployeeSalaryStructure.EmployeeWorkStationUnit)
                        .Include(x => x.EmployeeSalaryStructure.SalaryStructure); 
        }

        public override IQueryable<EmployeeSalaryPaymentLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeSalaryStructureLog)
                        .Include(x => x.EmployeePayrollInfoLog)
                        .Include(x => x.EmployeeSalaryStructureLog.EmployeeWorkstationUnitLog)
                        .Include(x => x.EmployeeSalaryStructureLog.SalaryStructureLog);
            ;
        }
    }
}
