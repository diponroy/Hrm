﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Data.Table
{
    public class EmployeeSalaryStructureAllowanceData : EntityWithLogRepository<EmployeeSalaryStructureAllowance, EmployeeSalaryStructureAllowanceLog, EmployeeSalaryStructureAllowanceMapper>
    {
        public EmployeeSalaryStructureAllowanceData ():this(new HrmContext())
        {
        } 
        public EmployeeSalaryStructureAllowanceData(DbContext context) : base(context)
        {
        } 
        public override void Add(EmployeeSalaryStructureAllowance entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeSalaryStructureAllowance entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeSalaryStructureAllowance entity)
        {
            base.Remove(entity,MappedLog(entity));
        }

        public override IQueryable<EmployeeSalaryStructureAllowance> TableWithDetail()
        {
            return Table
                .Include(x => x.EmployeeSalaryStructure)
                .Include(x => x.EmployeeSalaryStructure.SalaryStructure)
                .Include(x=>x.Allowance);
        }

        public override IQueryable<EmployeeSalaryStructureAllowanceLog> LogWithDetail()
        {
            return LogTable
                .Include(x => x.EmployeeSalaryStructureLog)
                .Include(x => x.EmployeeSalaryStructureLog.SalaryStructureLog)
                .Include(x=>x.AllowanceLog);
        }
    }
}
