﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeSalaryStructureBonusData : EntityWithLogRepository<EmployeeSalaryStructureBonus, EmployeeSalaryStructureBonusLog, EmployeeSalaryStructureBonusMapper>
    {
        public EmployeeSalaryStructureBonusData ( ):this(new HrmContext())
        {
        } 
        public EmployeeSalaryStructureBonusData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeSalaryStructureBonus entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeeSalaryStructureBonus entity)
        {
            base.Replace(entity,MappedLog(entity));
        }

        public override void Remove(EmployeeSalaryStructureBonus entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeSalaryStructureBonus> TableWithDetail()
        {
            return Table
                .Include(x=>x.EmployeeSalaryStructure)
                .Include(x => x.EmployeeSalaryStructure.SalaryStructure)
                .Include(x => x.Bonus);
        }

        public override IQueryable<EmployeeSalaryStructureBonusLog> LogWithDetail()
        {
            return LogTable
                .Include(x=>x.EmployeeSalaryStructureLog)
                .Include(x => x.EmployeeSalaryStructureLog.SalaryStructureLog)
                .Include(x => x.BonusLog);
        }
    }
}
