﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeSalaryStructureData :
        EntityWithLogRepository<EmployeeSalaryStructure, EmployeeSalaryStructureLog, EmployeeSalaryStructureMapper>
    {
        public EmployeeSalaryStructureData() : this(new HrmContext())
        {
        }

        public EmployeeSalaryStructureData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeSalaryStructure entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeSalaryStructure entity)
        {
            base.Replace(entity, MappedLog(entity));  
        }

        public override void Remove(EmployeeSalaryStructure entity)
        {
            base.Remove(entity, MappedLog(entity)); 
        }

        public override IQueryable<EmployeeSalaryStructure> TableWithDetail()
        {
            return
                All()
                    .Include(x => x.SalaryStructure)
                    .Include(x => x.EmployeeWorkStationUnit)
                    .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit)
                    .Include(x => x.EmployeeWorkStationUnit.Employee)  
                    .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Company)
                    .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Branch)
                    .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Department);
        }

        public override IQueryable<EmployeeSalaryStructureLog> LogWithDetail()
        {
            return
                AllLog()
                    .Include(x => x.SalaryStructureLog)
                    .Include(x => x.EmployeeWorkstationUnitLog)
                    .Include(x=>x.EmployeeWorkstationUnitLog.EmployeeLog)  
                    .Include(x => x.EmployeeWorkstationUnitLog.WorkStationUnitLog)
                    .Include(x=>x.EmployeeWorkstationUnitLog.WorkStationUnitLog.CompanyLog)
                    .Include(x => x.EmployeeWorkstationUnitLog.WorkStationUnitLog.BranchLog)
                    .Include(x => x.EmployeeWorkstationUnitLog.WorkStationUnitLog.DepartmentLog);
        }
    }
}
