﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeTrainingAttachmentData : EntityWithLogRepository<EmployeeTrainingAttachment, EmployeeTrainingAttachmentLog, EmployeeTrainingAttachmentMapper>
    {
        public EmployeeTrainingAttachmentData ( ):this(new HrmContext())
        {
        }
        public EmployeeTrainingAttachmentData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeTrainingAttachment entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeTrainingAttachment entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeTrainingAttachment entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeTrainingAttachment> TableWithDetail()
        {
            return Table.Include(x => x.EmployeeTraining);
        }

        public override IQueryable<EmployeeTrainingAttachmentLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeTrainingLog);
        }
    }
}
