﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{                                         
    public class EmployeeTrainingBudgetData : EntityWithLogRepository<EmployeeTrainingBudget, EmployeeTrainingBudgetLog, EmployeeTrainingBudgetMapper>
    {
        public EmployeeTrainingBudgetData () : this(new HrmContext())
        {
        }
        public EmployeeTrainingBudgetData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeTrainingBudget entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeTrainingBudget entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeTrainingBudget entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeTrainingBudget> TableWithDetail()
        {
            return Table.Include(x => x.EmployeeTraining);
        }

        public override IQueryable<EmployeeTrainingBudgetLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeTrainingLog);
        }
    }
}
