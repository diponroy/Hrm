﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeTrainingData : EntityWithLogRepository<EmployeeTraining, EmployeeTrainingLog, EmployeeTrainingMapper>
    {
        public EmployeeTrainingData (): this(new HrmContext())
        {
        }

        public EmployeeTrainingData ( DbContext context ) : base(context)
        {
        }

        public override void Add(EmployeeTraining entity)
        {
           base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeeTraining entity)
        {
            base.Replace(entity, MappedLog(entity));

        }

        public override void Remove(EmployeeTraining entity)
        {
            base.Remove(entity, MappedLog(entity));

        }

        public override IQueryable<EmployeeTraining> TableWithDetail()
        {
            return Table;
        }

        public override IQueryable<EmployeeTrainingLog> LogWithDetail()
        {
            return LogTable;
        }
    }
}
