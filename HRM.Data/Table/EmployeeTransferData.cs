﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeTransferData:EntityWithLogRepository<EmployeeTransfer,EmployeeTransferLog,EmployeeTransferMapper>
    {
        public EmployeeTransferData(DbContext context) : base(context)
        {
        }

        public override void Add(EmployeeTransfer entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(EmployeeTransfer entity)
        {
           base.Replace(entity,MappedLog(entity));
        }

        public override void Remove(EmployeeTransfer entity)
        {
            base.Remove(entity,MappedLog(entity));
        }

        public override IQueryable<EmployeeTransfer> TableWithDetail()
        {
            return Table.Include(x => x.Employee)
                        .Include(x => x.EmployeeType)
                        .Include(x => x.WorkStationUnit)
                        .Include(x => x.WorkStationUnit.Company)
                        .Include(x => x.WorkStationUnit.Branch)
                        .Include(x => x.WorkStationUnit.Department)
                        .Include(x => x.IncrementSalaryStructure)
                        .Include(x => x.IncrementSalaryStructure.EmployeeSalaryStructure)
                        .Include(x => x.IncrementSalaryStructure.EmployeeSalaryStructure.SalaryStructure);
        }

        public override IQueryable<EmployeeTransferLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeLog)
                           .Include(x => x.EmployeeTypeLog)
                           .Include(x => x.WorkStationUnitLog)
                           .Include(x => x.WorkStationUnitLog.CompanyLog)
                           .Include(x => x.WorkStationUnitLog.BranchLog)
                           .Include(x => x.WorkStationUnitLog.DepartmentLog)
                           .Include(x => x.IncrementSalaryStructureLog)
                           .Include(x => x.IncrementSalaryStructureLog.EmployeeSalaryStructureLog)
                           .Include(x => x.IncrementSalaryStructureLog.EmployeeSalaryStructureLog.SalaryStructureLog);
        }
    }
}
