﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeTypeData : EntityWithLogRepository<EmployeeType, EmployeeTypeLog, EmployeeTypeMapper>
    {
        public EmployeeTypeData() : this(new HrmContext())
        {
        }

        public EmployeeTypeData(DbContext context) : base(context)
        {
        }

        public bool AnyActiveOrInactiveTitle(string title)
        {
            return AllActiveOrInactive().Any(x => x.Title.Equals(title));
        }

        public bool AnyActiveOrInactiveTitleExcept(string title, long id)
        {
            return AllActiveOrInactive().Any(x => x.Id != id && x.Title.Equals(title));
        }

        public override void Add(EmployeeType aEmployeeType)
        {
            base.Add(aEmployeeType, MappedLog(aEmployeeType));
        }

        public override void Remove(EmployeeType aEmployeeType)
        {
            base.Remove(aEmployeeType, MappedLog(aEmployeeType));
        }


        public override IQueryable<EmployeeType> TableWithDetail()

        {
            return Table.Include(x => x.ParentEmployeeType);
        }

        public override IQueryable<EmployeeTypeLog> LogWithDetail()
        {
            return LogTable.Include(x => x.ParentEmployeeTypeLog);
        }

        public override void Replace(EmployeeType aEmployeeType)
        {
            base.Replace(aEmployeeType, MappedLog(aEmployeeType));
        }

        public DbRawSqlQuery<EmployeeType> ActiveChilds(long id)
        {
            var clientIdParameter = new SqlParameter("@id", id);
            DbRawSqlQuery<EmployeeType> value = Context.Database.SqlQuery<EmployeeType>(@"
            DECLARE @testId BIGINT;
            SET @testId=@id;
            WITH tblChild AS
                (
                    SELECT [Id]
                           ,[Title] 
                           ,[Remarks]
		                   ,[EmployeeType_Id_AsParentId] AS ParentId
		                   ,[Status]
                    FROM (SELECT * FROM EmployeeType WHERE Status = 0) AS ActiveEntity
                    WHERE EmployeeType_Id_AsParentId = @testId
                    UNION ALL
                    SELECT  ActiveChilds.[Id]
                            ,ActiveChilds.[Title]
                            ,ActiveChilds.[Remarks]
                            ,ActiveChilds.[EmployeeType_Id_AsParentId] AS ParentId                            
                            ,ActiveChilds.[Status] 
                 FROM (SELECT * FROM EmployeeType WHERE Status = 0) AS ActiveChilds  
		                JOIN tblChild  ON ActiveChilds.EmployeeType_Id_AsParentId = tblChild.Id
            )
            SELECT *
            FROM tblChild
            OPTION(MAXRECURSION 32767)
            ", clientIdParameter);
            return value;
        }
    }
}
