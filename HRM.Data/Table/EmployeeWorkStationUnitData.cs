﻿using System.Data.Entity;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class EmployeeWorkStationUnitData :
        EntityWithLogRepository<EmployeeWorkStationUnit, EmployeeWorkStationUnitLog, EmployeeWorkStationUnitMapper>
    {
        public EmployeeWorkStationUnitData() : this(new HrmContext())
        {
        }

        public EmployeeWorkStationUnitData(DbContext context) : base(context)
        {
        }

        public bool HasEmployeeWorkStationUnit(long employeeId, long workStationUnitId)
        {
            if (
                All()
                    .Any(
                        x =>
                            x.EmployeeId == employeeId && x.WorkStationUnitId == workStationUnitId &&
                            x.Status == EntityStatusEnum.Active))
            {
                return true;
            }

            if (
                All()
                    .Any(
                        x =>
                            x.EmployeeId == employeeId && x.WorkStationUnitId == workStationUnitId &&
                            x.Status == EntityStatusEnum.Inactive && x.DetachmentDate == null))
            {
                return true;
            }

            return false;
        }

        public override void Add(EmployeeWorkStationUnit entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(EmployeeWorkStationUnit entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(EmployeeWorkStationUnit entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<EmployeeWorkStationUnit> TableWithDetail()
        {
            return All().Include(x => x.Employee)
                        .Include(x => x.EmployeeType)
                        .Include(x => x.WorkStationUnit)
                        .Include(x => x.WorkStationUnit.Company)
                        .Include(x => x.WorkStationUnit.Branch)
                        .Include(x => x.WorkStationUnit.Department);
        }

        public override IQueryable<EmployeeWorkStationUnitLog> LogWithDetail()
        {
            return
                AllLog()
                    .Include(x => x.EmployeeLog)
                    .Include(x => x.EmployeeTypeLog)
                    .Include(x => x.WorkStationUnitLog)
                    .Include(x => x.WorkStationUnitLog.CompanyLog)
                    .Include(x => x.WorkStationUnitLog.BranchLog)
                    .Include(x => x.WorkStationUnitLog.DepartmentLog);
        }
    }
}
