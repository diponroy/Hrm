﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class IncrementSalaryStructureData :EntityWithLogRepository<IncrementSalaryStructure,IncrementSalaryStructureLog,IncrementSalaryStructureMapper>
    {
        public IncrementSalaryStructureData (): this(new HrmContext())
        {
        }
        public IncrementSalaryStructureData(DbContext context) : base(context)
        {
        }


        public override void Add(IncrementSalaryStructure entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(IncrementSalaryStructure entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(IncrementSalaryStructure entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<IncrementSalaryStructure> TableWithDetail()
        {
            return Table.Include(x => x.EmployeeSalaryStructure);
        }

        public override IQueryable<IncrementSalaryStructureLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeSalaryStructureLog);

        }
    }
}
