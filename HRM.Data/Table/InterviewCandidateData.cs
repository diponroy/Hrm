﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class InterviewCandidateData :
        EntityWithLogRepository<InterviewCandidate, InterviewCandidateLog, InterviewCandidateMapper>
    {
        public InterviewCandidateData(DbContext context) : base(context)
        {
        }

        public override void Add(InterviewCandidate entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(InterviewCandidate entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(InterviewCandidate entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<InterviewCandidate> TableWithDetail()
        {
            return Table.Include(x => x.Interview).Include(x => x.Candidate);
        }

        public override IQueryable<InterviewCandidateLog> LogWithDetail()
        {
            return LogTable.Include(x => x.InterviewLog).Include(x => x.CandidateLog);
        }
    }
}
