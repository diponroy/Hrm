﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class InterviewCoordinatorData:EntityWithLogRepository<InterviewCoordinator,InterviewCoordinatorLog,InterviewCoordinatorMapper>
    {
        public InterviewCoordinatorData(DbContext context) : base(context)
        {
        }

        public override void Add(InterviewCoordinator entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(InterviewCoordinator entity)
        {
            base.Replace(entity,MappedLog(entity));
        }

        public override void Remove(InterviewCoordinator entity)
        {
            base.Remove(entity,MappedLog(entity));
        }

        public override IQueryable<InterviewCoordinator> TableWithDetail()
        {
            return Table.Include(x => x.Employee).Include(x => x.Interview);
        }

        public override IQueryable<InterviewCoordinatorLog> LogWithDetail()
        {
            return LogTable.Include(x => x.EmployeeLog).Include(x => x.InterviewLog);
        }
    }
}
