﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Mapper;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class InterviewData : EntityWithLogRepository<Interview, InterviewLog,InterviewMapper>
    {
        public InterviewData(DbContext context) : base(context)
        {
        }

        public bool AnyActiveOrInactiveTrackNo ( string trackNo )
        {
            return AllActiveOrInactive().Any(x => x.TrackNo.Equals(trackNo));
        }

        public bool AnyActiveOrInactiveTrackNoExcept ( string trackNo,long id )
        {
            return AllActiveOrInactive().Any(x => x.TrackNo.Equals(trackNo)&&(x.Id!=id));
        }

        public override void Add(Interview entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(Interview entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(Interview entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<Interview> TableWithDetail()
        {
            return Table.Include(x => x.ManpowerVacancyNotice);
        }

        public override IQueryable<InterviewLog> LogWithDetail()
        {
            return LogTable.Include(x => x.ManpowerVacancyNoticeLog);
        }
    }
}
