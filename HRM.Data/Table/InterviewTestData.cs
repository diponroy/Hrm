﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Web.Controllers
{
    public class InterviewTestData:EntityWithLogRepository<InterviewTest, InterviewTestLog, InterviewTestMapper>
    {
        public InterviewTestData(DbContext context) : base(context)
        {
        }

        public override void Add(InterviewTest entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(InterviewTest entity)
        {
           base.Replace(entity,MappedLog(entity));
        }

        public override void Remove(InterviewTest entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<InterviewTest> TableWithDetail()
        {
            return Table.Include(x => x.Interview);
        }

        public override IQueryable<InterviewTestLog> LogWithDetail()
        {
            return LogTable.Include(x => x.InterviewLog);
        }
    }
}
