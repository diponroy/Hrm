﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class ManpowerCandidateReferenceData : EntityWithLogRepository<ManpowerCandidateReference, ManpowerCandidateReferenceLog, ManpowerCandidateReferenceMapper>
    {
        public ManpowerCandidateReferenceData() : this(new HrmContext())
        {
        }

        public ManpowerCandidateReferenceData(DbContext context) : base(context)
        {
        }

        public override void Add(ManpowerCandidateReference entity)
        {
            
        }

        public override void Replace(ManpowerCandidateReference entity)
        {
            
        }

        public override void Remove(ManpowerCandidateReference entity)
        {
            
        }

        public override IQueryable<ManpowerCandidateReference> TableWithDetail()
        {
            return Table
                .Include(x=>x.ManpowerVacancyNoticeCandidate)
                .Include(x=>x.Employee);
        }

        public override IQueryable<ManpowerCandidateReferenceLog> LogWithDetail()
        {
            return LogTable
                .Include(x=>x.ManpowerVacancyNoticeCandidateLog)
                .Include(x=>x.EmployeeReferrerLog);
        }
    }
}
