﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Data.Table
{
    public class ManpowerRequisitionData : EntityWithLogRepository<ManpowerRequisition, ManpowerRequisitionLog, ManpowerRequisitionMapper>
    {
        public ManpowerRequisitionData() : this(new HrmContext())
        {
        }

        public ManpowerRequisitionData(DbContext context)
            : base(context)
        {
        }

        public string GenerateTrackNo()
        {
            var prefix = string.Format("MPR-{0}", DateTime.Now.Year);
            var preEntity = Table.Where(x => x.TrackNo.StartsWith(prefix)).OrderByDescending(x => x.Id).FirstOrDefault();

            long? lastCount = null;
            if (preEntity == null)
            {
                lastCount = 0;
            }
            else
            {
                var lastTrackNoWithPrefix = preEntity.TrackNo;
                lastCount = Convert.ToInt64(lastTrackNoWithPrefix.Split('-').ToArray()[2]);
            }

            return string.Format("{0}-{1}", prefix, ++lastCount);
        }

        public override void Add(ManpowerRequisition entity)
        {           
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(ManpowerRequisition entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(ManpowerRequisition entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<ManpowerRequisition> TableWithDetail()
        {
            return Table;
        }

        public override IQueryable<ManpowerRequisitionLog> LogWithDetail()
        {
            return LogTable;
        }
    }
}
