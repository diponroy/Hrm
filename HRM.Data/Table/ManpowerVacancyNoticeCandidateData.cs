﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class ManpowerVacancyNoticeCandidateData : EntityWithLogRepository<ManpowerVacancyNoticeCandidate, ManpowerVacancyNoticeCandidateLog, ManpowerVacancyNoticeCandidateMapper>
    {
        public ManpowerVacancyNoticeCandidateData() : this(new HrmContext())
        {
        }

        public ManpowerVacancyNoticeCandidateData(DbContext context)
            : base(context)
        {
        }

        public override void Add(ManpowerVacancyNoticeCandidate entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(ManpowerVacancyNoticeCandidate entity)
        {
            
        }

        public override void Remove(ManpowerVacancyNoticeCandidate entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<ManpowerVacancyNoticeCandidate> TableWithDetail()
        {
            return Table
                .Include(x => x.ManpowerVacancyNotice)
                .Include(x => x.Candidate)
                .Include(x => x.Employee);
        }

        public override IQueryable<ManpowerVacancyNoticeCandidateLog> LogWithDetail()
        {
            return LogTable
                .Include(x=>x.ManpowerVacancyNoticeLog)
                .Include(x=>x.CandidateLog)
                .Include(x=>x.EmployeeLog);
        }

        public string GenerateTrackNo()
        {
            var prefix = string.Format("MVNC-{0}", DateTime.Now.Year);
            var preEntity = Table.Where(x => x.TrackNo.StartsWith(prefix)).OrderByDescending(x => x.Id).FirstOrDefault();

            long? lastCount = null;
            if (preEntity == null)
            {
                lastCount = 0;
            }
            else
            {
                var lastTrackNoWithPrefix = preEntity.TrackNo;
                lastCount = Convert.ToInt64(lastTrackNoWithPrefix.Split('-').ToArray()[2]);
            }

            return string.Format("{0}-{1}", prefix, ++lastCount);
        }
    }
}
