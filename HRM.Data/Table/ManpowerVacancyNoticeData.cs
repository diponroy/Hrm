﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Data.Table
{
    public class ManpowerVacancyNoticeData : EntityWithLogRepository<ManpowerVacancyNotice, ManpowerVacancyNoticeLog, ManpowerVacancyNoticeMapper>
    {
        private readonly ManpowerRequisitionData _requisitionData = new ManpowerRequisitionData();

        public ManpowerVacancyNoticeData() : this(new HrmContext())
        {
        }

        public ManpowerVacancyNoticeData(DbContext context)
            : base(context)
        {
        }

        public string GenerateTrackNo()
        {
            var prefix = string.Format("MVN-{0}", DateTime.Now.Year);
            var preEntity = Table.Where(x => x.TrackNo.StartsWith(prefix)).OrderByDescending(x => x.Id).FirstOrDefault();

            long? lastCount = null;
            if (preEntity == null)
            {
                lastCount = 0;
            }
            else
            {
                var lastTrackNoWithPrefix = preEntity.TrackNo;
                lastCount = Convert.ToInt64(lastTrackNoWithPrefix.Split('-').ToArray()[2]);
            }

            return string.Format("{0}-{1}", prefix, ++lastCount);
        }

        public override void Add(ManpowerVacancyNotice entity)
        {       
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(ManpowerVacancyNotice entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(ManpowerVacancyNotice entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<ManpowerVacancyNotice> TableWithDetail()
        {
            return Table.Include(x => x.ManpowerRequisition);
        }

        public override IQueryable<ManpowerVacancyNoticeLog> LogWithDetail()
        {
            return LogTable.Include(x => x.ManpowerRequisitionLog);
        }

        public ICollection<ManpowerRequisition> GetManpoweRequisitions()
        {
            List<ManpowerRequisition> list = _requisitionData.Detail.AllActive().ToList();
            return list;
        }
    }
}