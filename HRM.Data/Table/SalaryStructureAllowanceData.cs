﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class SalaryStructureAllowanceData:EntityWithLogRepository<SalaryStructureAllowance, SalaryStructureAllowanceLog, SalaryStructureAllowanceMapper>
    {
        public SalaryStructureAllowanceData ( ): this(new HrmContext())
        {
        }
        public SalaryStructureAllowanceData(DbContext context) : base(context)
        {
        }

        public override void Add(SalaryStructureAllowance entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(SalaryStructureAllowance entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(SalaryStructureAllowance entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<SalaryStructureAllowance> TableWithDetail()
        {
            return Table
                .Include(x => x.Allowance)
                .Include(x=>x.SalaryStructure);
        }

        public override IQueryable<SalaryStructureAllowanceLog> LogWithDetail()
        {
            return LogTable
                .Include(x => x.AllowanceLog)
                .Include(x=>x.SalaryStructureLog);
        }
    }
}
