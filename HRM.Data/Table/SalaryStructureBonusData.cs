﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class SalaryStructureBonusData : EntityWithLogRepository<SalaryStructureBonus, SalaryStructureBonusLog, SalaryStructureBonusMapper>
    {
        public SalaryStructureBonusData () : this(new HrmContext())
        {
        }
        
        public SalaryStructureBonusData(DbContext context) : base(context)
        {
        }

        public override void Add(SalaryStructureBonus entity)
        {
            base.Add(entity,MappedLog(entity));
        }

        public override void Replace(SalaryStructureBonus entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(SalaryStructureBonus entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<SalaryStructureBonus> TableWithDetail()
        {
            return Table
                .Include(x => x.Bonus)
                .Include(x=>x.SalaryStructure);
        }

        public override IQueryable<SalaryStructureBonusLog> LogWithDetail()
        {
            return LogTable
                .Include(x => x.BonusLog)
                .Include(x=>x.SalaryStructureLog);
        }
    }
}
