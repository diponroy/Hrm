﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class SalaryStructureData:EntityWithLogRepository<SalaryStructure, SalaryStructureLog, SalaryStructureMapper>
    {
        public SalaryStructureData() : this(new HrmContext())
        {
        }

        public SalaryStructureData(DbContext context) : base(context)
        {
        }

        //public IQueryable<SalaryStructure> AllActiveAndNotClosed ()
        //{
        //    return AllActive().Where(x => x.Status.Equals(EntityStatusEnum.Active)||x.Status.Equals(EntityStatusEnum.Inactive)||x.Status.Equals(EntityStatusEnum.Removed));
        //}

        public bool AnyActiveOrInactiveTitle ( string title )
        {
            return AllActiveOrInactive().Any(x => x.Title.Equals(title));
        }

        public bool AnyActiveOrInactiveTitleExcept ( string title, long id )
        {
            return AllActiveOrInactive().Any(x => x.Id != id && x.Title.Equals(title));
        }

        public override void Add(SalaryStructure entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(SalaryStructure entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(SalaryStructure entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<SalaryStructure> TableWithDetail()
        {
            return Table;
        }

        public override IQueryable<SalaryStructureLog> LogWithDetail()
        {
            return LogTable;
        }
    }
}
