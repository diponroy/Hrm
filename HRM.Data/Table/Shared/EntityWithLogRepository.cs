﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Data.Table.Shared
{
    public abstract class EntityWithLogRepository<TEntity, TLog, TMapper> : Repository<TEntity, TLog>
        where TEntity : class, IEntityStatus, IEntityWithLog<TLog>
        where TLog : class, IEntityLog<TEntity>, new()
        where TMapper : LogMapper<TEntity, TLog>, new()
    {
        public readonly DbContext Context;
        protected readonly DbSet<TEntity> Table;
        protected readonly DbSet<TLog> LogTable;

        private LogMapper<TEntity, TLog> _mapper;

        public LogMapper<TEntity, TLog> Mapper
        {
            get
            {
                _mapper = _mapper ?? new TMapper();
                return _mapper;
            }
        }

        private Repository<TEntity, TLog> _detail;

        public Repository<TEntity, TLog> Detail
        {
            get
            {
                _detail = _detail ?? new Repository<TEntity, TLog>(
                    TableWithDetail(),
                    LogWithDetail().Include(x => x.AffectedByEmployeeLog)
                    );
                return _detail;
            }
        }

        protected EntityWithLogRepository(DbContext context)
            : base(context.Set<TEntity>(), context.Set<TLog>())
        {
            Context = context;
            context.Configuration.ProxyCreationEnabled = false;
            context.Configuration.LazyLoadingEnabled = true;

            Table = Context.Set<TEntity>();
            LogTable = Context.Set<TLog>();
        }

        public abstract void Add(TEntity entity);
        public abstract void Replace(TEntity entity);
        public abstract void Remove(TEntity entity);
        public abstract IQueryable<TEntity> TableWithDetail();
        public abstract IQueryable<TLog> LogWithDetail();


        private void MappLogToEntity(TEntity entity, TLog log, LogStatusEnum logStatus)
        {
            if (log == null)
            {
                throw new Exception("Log of the entity is required.");
            }
            log.LogStatuses = logStatus;
            entity.Logs = new Collection<TLog> {log};
        }

        public virtual TLog MappedLog(TEntity entity)
        {
            return Mapper.FullMapped(Context, entity);
        }

        protected virtual void Add(TEntity entity, TLog log)
        {
            MappLogToEntity(entity, log, LogStatusEnum.Added);
            DbEntityEntry dbEntityEntry = Context.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                Table.Add(entity);
            }
        }

        protected virtual void Replace(TEntity entity, TLog log)
        {
            MappLogToEntity(entity, log, LogStatusEnum.Replaced);
            DbEntityEntry dbEntityEntry = Context.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                Table.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
        }

        protected virtual void Remove(TEntity entity, TLog log)
        {
            MappLogToEntity(entity, log, LogStatusEnum.Removed);
            entity.Status = EntityStatusEnum.Removed;
            DbEntityEntry dbEntityEntry = Context.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                Table.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }
    }
}
