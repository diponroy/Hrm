﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Db.Contexts;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Data.Table.Shared
{
    public class Repository<TEntity, TLog>
        where TEntity : class, IEntityStatus
        where TLog : class, IPrimaryKeyTrack
    {
        private readonly IQueryable<TEntity> _data;
        private readonly IQueryable<TLog> _log;

        public Repository(IQueryable<TEntity> data, IQueryable<TLog> log)
        {
            _data = data;
            _log = log;
        }

        public IQueryable<TEntity> All()
        {
            return _data;
        }
        public IQueryable<TEntity> AllActive()
        {
            return _data.Where(x => x.Status == EntityStatusEnum.Active);
        }
        public IQueryable<TEntity> AllActiveOrInactive()
        {
            return _data.Where(x => x.Status == EntityStatusEnum.Active || x.Status == EntityStatusEnum.Inactive);
        }
        public TEntity Get(long id)
        {
            return _data.Single(x => x.Id == id);
        }
        public IQueryable<TLog> AllLog()
        {
            return _log;
        }
        public IQueryable<TLog> AllLog(long id)
        {
            return _log.Where(x => x.Id == id);
        }

    }
}
