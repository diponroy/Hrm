﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using HRM.Data.Table.Shared;
using HRM.Db.Contexts;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Data.Table
{
    public class WorkStationUnitData :
        EntityWithLogRepository<WorkStationUnit, WorkStationUnitLog, WorkStationUnitMapper>
    {
        public WorkStationUnitData() : this(new HrmContext())
        {
        }

        public WorkStationUnitData(DbContext context) : base(context)
        {
        }

        public override void Add(WorkStationUnit entity)
        {
            base.Add(entity, MappedLog(entity));
        }

        public override void Replace(WorkStationUnit entity)
        {
            base.Replace(entity, MappedLog(entity));
        }

        public override void Remove(WorkStationUnit entity)
        {
            base.Remove(entity, MappedLog(entity));
        }

        public override IQueryable<WorkStationUnit> TableWithDetail()
        {
            return
                Table.Include(x => x.Company)
                     .Include(x => x.Branch)
                     .Include(x => x.Department)
                     .Include(x => x.ParentWorkStationUnit)
                     .Include(x => x.ParentWorkStationUnit.Company)
                     .Include(x => x.ParentWorkStationUnit.Branch)
                     .Include(x => x.ParentWorkStationUnit.Department);
        }

        public override IQueryable<WorkStationUnitLog> LogWithDetail()
        {
            return
                LogTable.Include(x => x.CompanyLog)
                        .Include(x => x.BranchLog)
                        .Include(x => x.DepartmentLog)
                        .Include(x => x.ParentWorkStationUnitLog)
                        .Include(x => x.ParentWorkStationUnitLog.CompanyLog)
                        .Include(x => x.ParentWorkStationUnitLog.BranchLog)
                        .Include(x => x.ParentWorkStationUnitLog.DepartmentLog);
        }


        public IQueryable<WorkStationUnit> AllActiveAndNotClosed()
        {
            return AllActive().Where(x => x.DateOfClosing == null);
        }

        public bool AnyActiveOrInactive(long? companyId, long? branchId, long? departmentId)
        {
            return
                AllActiveOrInactive()
                    .Any(x => x.CompanyId == companyId && x.BranchId == branchId && x.DepartmentId == departmentId);
        }

        public bool AnyActiveOrInactiveExcept(long? companyId, long? branchId, long? departmentId, long id)
        {
            return
                AllActiveOrInactive()
                    .Any(
                        x =>
                            x.CompanyId == companyId && x.BranchId == branchId && x.DepartmentId == departmentId &&
                            x.Id != id);
        }

        public DbRawSqlQuery<WorkStationUnit> GetChilds(long id)
        {
            var clientIdParameter = new SqlParameter("@id", id);
            return Context.Database.SqlQuery<WorkStationUnit>(@"
                DECLARE @stationId BIGINT;
                SET @stationId = @id;
                WITH tblChild AS
                (
                    SELECT [Id]
		                  ,[Company_Id] AS CompanyId
		                  ,[Branch_Id]  AS BranchId
		                  ,[Department_Id]  AS DepartmentId
		                  ,[WorkStationUnit_Id_AsParentId]  AS ParentId
		                  ,[DateOfCreation]
		                  ,[DateOfClosing]
		                  ,[EmployeeIdAsInCharge] AS EmployeeIdAsInCharge
		                  ,[Remarks]
		                  ,[Status]
                        FROM (SELECT * FROM WorkStationUnit WHERE Status = 0) AS ActiveEntity
                        WHERE WorkStationUnit_Id_AsParentId = @stationId
                    UNION ALL
                    SELECT ActiveChilds.[Id]
		                  ,ActiveChilds.[Company_Id] AS CompanyId
		                  ,ActiveChilds.[Branch_Id]  AS BranchId
		                  ,ActiveChilds.[Department_Id]  AS DepartmentId
		                  ,ActiveChilds.[WorkStationUnit_Id_AsParentId]  AS ParentId
		                  ,ActiveChilds.[DateOfCreation]
		                  ,ActiveChilds.[DateOfClosing]
		                  ,ActiveChilds.[EmployeeIdAsInCharge] AS EmployeeIdAsInCharge
		                  ,ActiveChilds.[Remarks]
		                  ,ActiveChilds.[Status] 
		                FROM (SELECT * FROM WorkStationUnit WHERE Status = 0) AS ActiveChilds  
		                JOIN tblChild  ON ActiveChilds.WorkStationUnit_Id_AsParentId = tblChild.Id
                )
                SELECT *
                    FROM tblChild
                OPTION(MAXRECURSION 32767)
            ", clientIdParameter);
        }
    }
}
