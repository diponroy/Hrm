﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class AllowanceConfig : Configuration<Allowance>
    {
        public AllowanceConfig() : base(Table.Allowance)
        {
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("TitleIndex", 1)));

            Property(x => x.AsPercentage)
                .IsRequired();

            Property(x => x.AllowanceTypeId)
                .HasColumnName("AllowanceType_Id");
            HasRequired(x => x.AllowanceType)
                .WithMany(l => l.Allowances)
                .HasForeignKey(x => x.AllowanceTypeId)
                .WillCascadeOnDelete(false);
        }
    }
}
