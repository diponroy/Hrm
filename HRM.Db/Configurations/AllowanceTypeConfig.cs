﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class AllowanceTypeConfig : Configuration<AllowanceType>
    {
        public AllowanceTypeConfig() : base(Table.AllowanceType)
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("NameIndex", 1)));

            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();
        }
    }
}
