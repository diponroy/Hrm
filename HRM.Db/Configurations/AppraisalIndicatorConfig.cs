﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class AppraisalIndicatorConfig : Configuration<AppraisalIndicator>
    {
        public AppraisalIndicatorConfig() : base(Table.AppraisalIndicator)
        {
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("TitleIndex", 1))); ;

            Property(x => x.MaxWeight)
                .IsRequired();

            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();

            /* foreign keys */
            Property(x => x.ParentId)
                .HasColumnName("AppraisalIndicator_Id_AsParentId");
            HasOptional(x=>x.ParentAppraisalIndicator)
                .WithMany(l=>l.ChildAppraisalIndicators)
                .HasForeignKey(f=>f.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}
