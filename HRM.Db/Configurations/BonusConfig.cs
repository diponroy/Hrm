﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class BonusConfig : Configuration<Bonus>
    {
        public BonusConfig(): base(Table.Bonus)
        {
            Property(x => x.Title)
                .HasMaxLength(100)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("Title_Index", 1)));

            Property(x => x.AsPercentage)
                .IsRequired();

            Property(x => x.BonusTypeId)
                .HasColumnName("BonusType_Id");

            HasRequired(x => x.BonusType)
                .WithMany(l => l.Bonuses)
                .HasForeignKey(x => x.BonusTypeId)
                .WillCascadeOnDelete(false);
        }
    }
}
