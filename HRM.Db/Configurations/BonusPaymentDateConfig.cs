﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class BonusPaymentDateConfig : Configuration<BonusPaymentDate>
    {
        public BonusPaymentDateConfig() : base(Table.BonusPaymentDate)
        {
            Property(x => x.EstimatedDate)
                .IsRequired();

            HasRequired(x => x.Bonus)
                .WithMany(l => l.PaymentDates)
                .HasForeignKey(x => x.BonusId)
                .WillCascadeOnDelete(false);               
        }
    }
}
