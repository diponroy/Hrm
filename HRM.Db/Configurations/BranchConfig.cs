﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class BranchConfig:Configuration<Branch>
    {
        public BranchConfig( ) : base(Table.Branch)
        {
            Ignore(x => x.IsClosed);

            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("UK_Name", 1) { IsUnique = true }));

            Property(p => p.Address);

            Property(p => p.Remarks)
                .HasMaxLength(150);

            Property(p => p.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");
        }
    }
}
