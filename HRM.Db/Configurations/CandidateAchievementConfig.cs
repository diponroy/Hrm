﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    class CandidateAchievementConfig : Configuration<CandidateAchievement>
    {
        public CandidateAchievementConfig() : base(Table.CandidateAchievement)
        {
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.Description)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.AttachmentDirectory)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.CandidateEmploymentId)
                .HasColumnName("Candidate_EmploymentId");
            HasRequired(l => l.CandidateEmployment)
                .WithMany(m => m.CandidateAchievements)
                .HasForeignKey(f => f.CandidateEmploymentId)
                .WillCascadeOnDelete(false);
        }
    }
}
