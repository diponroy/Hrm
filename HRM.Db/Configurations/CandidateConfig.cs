﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class CandidateConfig : Configuration<Candidate>
    {
        public CandidateConfig() : base(Table.Candidate)
        {
            //Property(x => x.TrackNo)
            //    .IsRequired()
            //    .HasMaxLength(25)
            //    .HasColumnAnnotation("Index" , new IndexAnnotation(
            //        new IndexAttribute("UK_TrackNo" , 1) { IsUnique = true }));

            //Property(x => x.FirstName)
            //    .IsRequired()
            //    .HasMaxLength(50) ;

            //Property(x => x.LastName)
            //    .IsRequired()
            //    .HasMaxLength(50) ;

            //Property(x => x.FathersName)
            //    .HasMaxLength(50) ;

            //Property(x => x.MothersName)
            //    .HasMaxLength(50);

            //Property(x => x.DateOfBirth)
            //    .IsRequired()
            //    .HasColumnType("DATE");

            //Property(x => x.Gender)
            //    .IsRequired()
            //    .HasMaxLength(20);

            //Property(x => x.BloodGroup)
            //    .IsRequired()
            //    .HasMaxLength(10);

            //Property(x => x.MaritalStatus)
            //    .IsRequired()
            //    .HasMaxLength(20);

            //Property(x => x.Nationality)
            //    .IsRequired()
            //    .HasMaxLength(50);

            //Property(x => x.NationalId)
            //    .HasMaxLength(50);

            //Property(x => x.PassportNo)
            //    .HasMaxLength(50);

            //Property(x => x.Religion)
            //    .IsRequired()
            //    .HasMaxLength(20);

            //Property(x => x.Email)
            //    .IsRequired()
            //    .HasMaxLength(50);

            //Property(x => x.AlternateEmailAddress)
            //    .HasMaxLength(50);

            //Property(x => x.PresentAddress)
            //    .IsRequired()
            //    .HasMaxLength(250);

            //Property(x => x.PermanentAddress)
            //    .IsRequired()
            //    .HasMaxLength(250);

            //Property(x => x.DivisionOrState)
            //    .IsRequired()
            //    .HasMaxLength(50);

            //Property(x => x.City)
            //    .IsRequired()
            //    .HasMaxLength(50);

            //Property(x => x.ImageDirectory)
            //    .IsRequired()
            //    .HasMaxLength(50);

            //Property(x => x.CvDirectory)
            //    .IsRequired()
            //    .HasMaxLength(50);

            //Property(x => x.Status)
            //    .IsRequired();


            Property(x => x.TrackNo)
                .IsRequired()
                .HasMaxLength(25)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("UK_TrackNo", 1) { IsUnique = true }));

            Property(x => x.FirstName)
                .HasMaxLength(50);

            Property(x => x.LastName)
                .HasMaxLength(50);

            Property(x => x.FathersName)
                .HasMaxLength(50);

            Property(x => x.MothersName)
                .HasMaxLength(50);

            Property(x => x.DateOfBirth)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.Gender)
                .IsRequired()
                .HasMaxLength(20);

            Property(x => x.BloodGroup)
                .HasMaxLength(10);

            Property(x => x.MaritalStatus)
                .HasMaxLength(20);

            Property(x => x.Nationality)
                .HasMaxLength(50);

            Property(x => x.NationalId)
                .HasMaxLength(50);

            Property(x => x.PassportNo)
                .HasMaxLength(50);

            Property(x => x.Religion)
                .HasMaxLength(20);

            Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.ContactNo)
                .IsRequired()
                .HasMaxLength(25);

            Property(x => x.AlternateEmailAddress)
                .HasMaxLength(50);

            Property(x => x.PresentAddress)
                .HasMaxLength(250);

            Property(x => x.PermanentAddress)
                .HasMaxLength(250);

            Property(x => x.DivisionOrState)
                .HasMaxLength(50);

            Property(x => x.City)
                .HasMaxLength(50);

            Property(x => x.ImageDirectory)
                .HasMaxLength(50);

            Property(x => x.CvDirectory)
                .HasMaxLength(50);

            Property(x => x.Status)
                .IsRequired();
        }
    }
}