﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class CandidateEducationConfig : Configuration<CandidateEducation>
    {
        public CandidateEducationConfig() : base(Table.CandidateEducation)
        {
            
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Institution)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.DurationFromDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.DurationToDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.Result)
                .IsRequired()
                .HasMaxLength(20);

            Property(x => x.Major)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.AttachmentDirectory)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();

            /* foreign key */
            Property(x => x.CandidateId)
                .HasColumnName("Candidate_Id");
            HasRequired(l => l.Candidate)
                .WithMany(m => m.CandidateEducations)
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
