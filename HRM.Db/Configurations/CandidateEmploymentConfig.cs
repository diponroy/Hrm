﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class CandidateEmploymentConfig : Configuration<CandidateEmployment>
    {
        public CandidateEmploymentConfig() : base(Table.CandidateEmployment)
        {
            Property(p => p.Institution)
                .IsRequired()
                .HasMaxLength(100);

            Property(p => p.Address)
                .HasMaxLength(150);

            Property(p => p.Designation)
                .IsRequired()
                .HasMaxLength(50);

            Property(p => p.Responsibility)
                .IsRequired()
                .HasMaxLength(250);

            Property(p => p.DurationFromDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(p => p.DurationToDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.Status)
                .IsRequired();


            /*foreign keys*/
            Property(x => x.CandidateId)
                .HasColumnName("Candidate_Id");
            HasRequired(r => r.Candidate)
                .WithMany(m => m.CandidateEmployments)
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
