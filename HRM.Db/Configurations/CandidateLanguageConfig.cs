﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class CandidateLanguageConfig : Configuration<CandidateLanguage>
    {
        public CandidateLanguageConfig() : base(Table.CandidateLanguage)
        {
            Property(x => x.LanguageName)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Reading)
                .IsRequired()
                .HasMaxLength(15);

            Property(x => x.Writing)
                .IsRequired()
                .HasMaxLength(15);

            Property(x => x.Speaking)
                .IsRequired()
                .HasMaxLength(15);

            Property(x => x.Listening)
                .IsRequired()
                .HasMaxLength(15);

            Property(x => x.Status)
                .IsRequired();

            /* foreign key */
            Property(x => x.CandidateId)
                .HasColumnName("Candidate_Id");
            HasRequired(x => x.Candidate)
                .WithMany(m => m.CandidateLanguages)
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
