﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class CandidateLoginConfig : Configuration<CandidateLogin>
    {
        public CandidateLoginConfig() : base(Table.CandidateLogin)
        {
            Property(x => x.LoginName)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Password)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Status)
                .IsRequired();

            /* foreign keys */
            Property(x => x.CandidateId)
                .HasColumnName("Candidate_Id");
            HasRequired(x => x.Candidate)
                .WithMany(l => l.CandidateLogins)
                .HasForeignKey(x => x.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}