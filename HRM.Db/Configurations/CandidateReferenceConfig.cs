﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class CandidateReferenceConfig : Configuration<CandidateReference>
    {
        public CandidateReferenceConfig() : base(Table.CandidateReference)
        {
            Property(x => x.Referrer)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Designation)
                .HasMaxLength(50);

            Property(x => x.CompanyOrOrg)
                .HasMaxLength(100);

            Property(x => x.ContactNo)
                .IsRequired()
                .HasMaxLength(20);

            Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Address)
                .IsRequired()
                .HasMaxLength(250);

            Property(x => x.Relation)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Status)
                .IsRequired();

            /*foreign keys*/
            Property(x => x.CandidateId)
                .HasColumnName("Candidate_Id");
            HasRequired(r => r.Candidate)
                .WithMany(m => m.CandidateReferences)
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
