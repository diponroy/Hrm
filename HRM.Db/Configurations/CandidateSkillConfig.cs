﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    class CandidateSkillConfig : Configuration<CandidateSkill>
    {
        public CandidateSkillConfig() : base(Table.CandidateSkill)
        {
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.Description)
                .HasMaxLength(100);

            Property(x => x.AttachmentDirectory)
                .HasMaxLength(250);

            Property(x => x.Type)
                .IsRequired()
                .HasMaxLength(150);

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.CandidateId)
                .HasColumnName("Candidate_Id");
            HasRequired(l => l.Candidate)
                .WithMany(m => m.CandidateSkills)
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
