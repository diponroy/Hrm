﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    class CandidateTrainingConfig : Configuration<CandidateTraining>
    {
        public CandidateTrainingConfig() : base(Table.CandidateTraining)
        {
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.Institution)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.Address)
                .HasMaxLength(250);

            Property(x => x.DurationFromDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.DurationToDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.Achievement)
                .HasMaxLength(100);

            Property(x => x.AttachmentDirectory)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.CandidateId)
                .HasColumnName("Candidate_Id");
            HasRequired(l => l.Candidate)
                .WithMany(m => m.CandidateTrainings)
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
