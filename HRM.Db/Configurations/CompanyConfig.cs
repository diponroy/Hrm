﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class CompanyConfig:Configuration<Company>
    {
        public CompanyConfig( ) : base(Table.Company)
        {
            Ignore(x => x.IsClosed);

            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50) 
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("NameIndex", 1)));

            Property(p => p.Address)
                .HasMaxLength(250);

            Property(p => p.Remarks)
                .HasMaxLength(150);

            Property(p => p.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");

            Property(p => p.DateOfClosing)
                .HasColumnType("DATE");
        }
    }
}
