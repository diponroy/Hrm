﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums ;

namespace HRM.Db.Configurations
{
    public class EmployeeAchievementConfig : Configuration<EmployeeAchievement>
    {
        public EmployeeAchievementConfig () : base(Table.EmployeeAchievement)
        {
            Property(x => x.Title)
                .HasMaxLength(50);

            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.AttachmentDirectory)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(x=>x.Employee)
                .WithMany(l => l.EmployeeAchievements)
                .HasForeignKey(f=>f.EmployeeId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeAppraisalId)
                .HasColumnName("EmployeeAppraisal_Id");
            HasRequired(x => x.EmployeeAppraisal)
                .WithMany(l => l.EmployeeAchievements)
                .HasForeignKey(f => f.EmployeeAppraisalId)
                .WillCascadeOnDelete(false);
        }
    }
}
