﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeAppraisalConfig : Configuration<EmployeeAppraisal>
    {
        public EmployeeAppraisalConfig() : base(Table.EmployeeAppraisal)
        {
            Property(x => x.Weight)
                .IsRequired();

            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.EmployeeAssignedAppraisalId)
                .HasColumnName("EmployeeAssignedAppraisal_Id");
            HasRequired(x=>x.EmployeeAssignedAppraisal)
                .WithMany(x=>x.EmployeeAppraisals)
                .HasForeignKey(x=>x.EmployeeAssignedAppraisalId)
                .WillCascadeOnDelete(false);
        }
    }
}
