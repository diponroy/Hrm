﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeAssignedAppraisalConfig : Configuration<EmployeeAssignedAppraisal>
    {
        public EmployeeAssignedAppraisalConfig() : base(Table.EmployeeAssignedAppraisal)
        {
            Ignore(x => x.IsDetached);


            Property(x => x.Type)
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("TypeIndex", 1))); ;

            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.AttachmentDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.DetachmentDate)
                .HasColumnType("DATE");

            Property(x => x.Status)
                .IsRequired();


            /* foreign keys */
            Property(x => x.EmployeeWorkStationUnitId)
                .HasColumnName("EmployeeWorkStationUnit_Id");
            HasRequired(x => x.EmployeeWorkStationUnit)
                .WithMany(x=>x.EmployeeAssignedAppraisals)
                .HasForeignKey(x => x.EmployeeWorkStationUnitId);

            Property(x => x.AppraisalIndicatorId)
                .HasColumnName("AppraisalIndicator_Id");
            HasRequired(x => x.AppraisalIndicator)
                .WithMany()
                .HasForeignKey(x => x.AppraisalIndicatorId);
        }
    }
}
