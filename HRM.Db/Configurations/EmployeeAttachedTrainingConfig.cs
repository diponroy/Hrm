﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeAttachedTrainingConfig : Configuration<EmployeeAttachedTraining>
    {
        public EmployeeAttachedTrainingConfig() : base(Table.EmployeeAttachedTraining)
        {
            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();

            /* foreign key */
            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(x=>x.Employee)
                .WithMany(x=>x.EmployeeAttachedTrainings)
                .HasForeignKey(x=>x.EmployeeId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeTrainingId)
                .HasColumnName("EmployeeTraining_Id");
            HasRequired(x => x.EmployeeTraining)
                .WithMany(x => x.EmployeeAttachedTrainings)
                .HasForeignKey(x => x.EmployeeTrainingId)
                .WillCascadeOnDelete(false);
        }
    }
}
