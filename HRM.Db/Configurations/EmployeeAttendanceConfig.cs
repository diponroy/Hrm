﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeAttendanceConfig : Configuration<EmployeeAttendance>
    {
        public EmployeeAttendanceConfig() : base(Table.EmployeeAttendance)
        {
            Property(x => x.AttendanceTime).IsRequired();

            Property(x => x.AttendanceDate).IsRequired().HasColumnType("DATE");

            Property(x => x.Remarks).HasMaxLength(250);

            Property(x => x.EmployeeWorkStationUnitId).HasColumnName("EmployeeWorkStationUnit_Id");

            HasRequired(x => x.EmployeeWorkStationUnit)
                .WithMany(x => x.EmployeeAttendances)
                .HasForeignKey(x=>x.EmployeeWorkStationUnitId)
                .WillCascadeOnDelete(false);  
        }
    }
}
