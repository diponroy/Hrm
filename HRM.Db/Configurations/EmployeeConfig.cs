﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeConfig:Configuration<Employee>
    {                                                      
        public EmployeeConfig( ) : base(Table.Employee)
        {
            Ignore(x => x.FullName);

            Property(p => p.TrackNo)
                .IsRequired()
                .HasMaxLength(20)
                .HasColumnAnnotation("Index" , new IndexAnnotation(
                    new IndexAttribute("UK_TrackNo" , 1) { IsUnique = true })) ;

            Property(p => p.Salutation)
                .IsRequired()
                .HasMaxLength(15) ;

            Property(x => x.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.LastName)
                .HasMaxLength(50);

            Property(x => x.FathersName)
                .HasMaxLength(50);

            Property(x => x.MothersName)
                .HasMaxLength(50);

            Property(x => x.DateOfBirth)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.Gender)
                .IsRequired()
                .HasMaxLength(20);

            Property(x => x.BloodGroup)
                .HasMaxLength(10);

            Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(50);

            Property(p => p.ContactNo)
                .IsRequired()
                .HasMaxLength(50) ;

            Property(p => p.HomePhone)
                .HasMaxLength(50);

            Property(p => p.OfficePhone)
                .HasMaxLength(50);

            Property(x => x.PresentAddress)
                .IsRequired()
                .HasMaxLength(250);

            Property(x => x.PermanentAddress)
                .HasMaxLength(250);

            Property(x => x.DivisionOrState)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.City)
                .HasMaxLength(50);

            Property(x => x.ImageDirectory)
                .HasMaxLength(50);

            Property(x => x.Status)
                .IsRequired();


            /* foreign keys */
            Property(x => x.CandidateId)
                .HasColumnName("Candidate_Id");
            HasOptional(x => x.Candidate)
                .WithMany()
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);


            //Property(x => x.EmployeeWorkStationId)
            //    .HasColumnName("EmployeeWorkStation_Id");
            //HasRequired(r => r.EmployeeWorkStation)
            //    .WithMany(m => m.Employees)
            //    .HasForeignKey(f => f.EmployeeWorkStationId);
        }
    }
}
