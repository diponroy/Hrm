﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeDisciplineConfig : Configuration<EmployeeDiscipline>
    {
        public EmployeeDisciplineConfig() : base(Table.EmployeeDiscipline)
        {
            Property(x => x.Title).IsRequired().HasMaxLength(50);

            Property(x => x.DateOfCreation).IsRequired().HasColumnType("DATE");

            Property(x => x.AttachmentDirectory).HasMaxLength(250);

            Property(x => x.Description).HasMaxLength(250);

            Property(x => x.ActionTaken).HasMaxLength(250);

            Property(x => x.Remarks).HasMaxLength(150);

            Property(x => x.Status).IsRequired();


            /* foreign key */
            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(x => x.Employee)
                .WithMany(m => m.EmployeeDisciplines)
                .HasForeignKey(f => f.EmployeeId)
                .WillCascadeOnDelete(false);


            Property(x => x.DepartmentId)
                .HasColumnName("Department_Id");
            HasRequired(x => x.Department)
                .WithMany(m => m.EmployeeDisciplines)
                .HasForeignKey(f => f.DepartmentId)
                .WillCascadeOnDelete(false);
        }
    }
}
