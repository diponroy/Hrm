﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeEducationConfig : Configuration<EmployeeEducation>
    {
        public EmployeeEducationConfig() : base(Table.EmployeeEducation)
        {
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Institution)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.DurationFromDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.DurationToDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.Result)
                .IsRequired()
                .HasMaxLength(20);

            Property(x => x.Major)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Remarks)
                .HasMaxLength(150);

            Property(x => x.AttachmentDirectory)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(x=>x.Employee)
                .WithMany(m=>m.EmployeeEducations)
                .HasForeignKey(f=>f.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
