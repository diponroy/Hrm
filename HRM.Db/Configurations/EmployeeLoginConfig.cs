﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeLoginConfig : Configuration<EmployeeLogin>
    {
        public EmployeeLoginConfig()
            : base(Table.EmployeeLogin)
        {
            Property(x => x.LoginName)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Password)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(x => x.Employee)
                .WithMany(l => l.EmployeeLogin)
                .HasForeignKey(x => x.EmployeeId);
        }
    }
}
