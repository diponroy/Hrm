﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeMembershipConfig : Configuration<EmployeeMembership>
    {
        public EmployeeMembershipConfig() : base(Table.EmployeeMembership)
        {
            Property(x => x.Organization)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.Description)
                .HasMaxLength(250);

            Property(x => x.Type)
                .IsRequired()
                .HasMaxLength(30);

            Property(x => x.Remarks)
                .IsRequired()
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(x=>x.Employee)
                .WithMany(l=>l.EmployeeMemberships)
                .HasForeignKey(f=>f.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
