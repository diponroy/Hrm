﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeePayrollInfoConfig : Configuration<EmployeePayrollInfo>
    {
        public EmployeePayrollInfoConfig() : base(Table.EmployeePayrollInfo)
        {
            Property(x => x.BankName)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.BankDetail)
                .IsRequired()
                .HasMaxLength(200);

            Property(x => x.AccountNo)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.TinNumber)
                .IsRequired();

            Property(x => x.Remarks)
                .IsRequired()
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(x=>x.Employee)
                .WithMany(m=>m.EmployeePayrollInfos)
                .HasForeignKey(f=>f.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
