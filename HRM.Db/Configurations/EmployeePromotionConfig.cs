﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeePromotionConfig : Configuration<EmployeePromotion>
    {
        public EmployeePromotionConfig() : base(Table.EmployeePromotion)
        {
            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.IsApproved)
                .IsRequired();

            Property(x => x.Status)
                .IsRequired();


            /* foreign keys */
            Property(x => x.EmployeeWorkStationUnitId)
                .HasColumnName("EmployeeWorkStationUnit_Id");
            HasRequired(x => x.EmployeeWorkStationUnit)
                .WithMany()
                .HasForeignKey(f => f.EmployeeWorkStationUnitId)
                .WillCascadeOnDelete(false);

            Property(x => x.WorkStationUnitId)
                .HasColumnName("WorkStationUnit_Id");
            HasOptional(x => x.WorkStationUnit)
                .WithMany()
                .HasForeignKey(f => f.WorkStationUnitId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeTypeId)
                .HasColumnName("EmployeeType_Id");
            HasOptional(x => x.EmployeeType)
                .WithMany()
                .HasForeignKey(f => f.EmployeeTypeId)
                .WillCascadeOnDelete(false);

            Property(x => x.IncrementSalaryStructureId)
                .HasColumnName("IncrementSalaryStructure_Id");
            HasOptional(x => x.IncrementSalaryStructure)
                .WithMany()
                .HasForeignKey(f => f.IncrementSalaryStructureId)
                .WillCascadeOnDelete(false);
        }
    }
}
