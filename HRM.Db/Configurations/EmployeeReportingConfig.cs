﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeReportingConfig : Configuration<EmployeeReporting>
    {
        public EmployeeReportingConfig() : base(Table.EmployeeReporting)
        {
            Ignore(x => x.IsDetached);


            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.AttachmentDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.DetachmentDate)
                .HasColumnType("DATE");

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.EmployeeWorkStationUnitId)
                .HasColumnName("EmployeeWorkStationUnit_Id");
            HasRequired(x => x.EmployeeWorkStationUnit)
                .WithMany()
                .HasForeignKey(x => x.EmployeeWorkStationUnitId)
                .WillCascadeOnDelete(false);

            Property(x => x.ParentEmployeeWorkStationUnitId)
                .HasColumnName("ParentEmployeeWorkStationUnit_Id");
            HasRequired(x => x.ParentEmployeeWorkStationUnit)
                .WithMany()
                .HasForeignKey(x => x.ParentEmployeeWorkStationUnitId)
                .WillCascadeOnDelete(false);
        }
    }
}
