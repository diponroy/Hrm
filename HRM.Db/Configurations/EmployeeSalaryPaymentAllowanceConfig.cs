﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeSalaryPaymentAllowanceConfig : Configuration<EmployeeSalaryPaymentAllowance>
    {
        public EmployeeSalaryPaymentAllowanceConfig(): base(Table.EmployeeSalaryPaymentAllowance)
        {
            Property(x => x.InAmount).IsRequired();
            
            Property(x => x.EmployeeSalaryPaymentId)
                .HasColumnName("EmployeeSalaryPayment_Id");
            HasRequired(x => x.EmployeeSalaryPayment)
                .WithMany(x => x.AllowancePayments)
                .HasForeignKey(x => x.EmployeeSalaryPaymentId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeSalaryStructureAllowanceId)
                .HasColumnName("EmployeeSalaryStructureAllowance_Id");
            HasRequired(x => x.EmployeeSalaryStructureAllowance)
                .WithMany(x => x.AllowancePayments)
                .HasForeignKey(x => x.EmployeeSalaryStructureAllowanceId)
                .WillCascadeOnDelete(false);
        }
    }
}
