﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeSalaryPaymentBonusConfig : Configuration<EmployeeSalaryPaymentBonus>
    {
        public EmployeeSalaryPaymentBonusConfig(): base(Table.EmployeeSalaryPaymentBonus)
        {
            Property(x => x.InAmount).IsRequired();
            
            Property(x => x.EmployeeSalaryPaymentId)
                .HasColumnName("EmployeeSalaryPayment_Id");
            HasRequired(x => x.EmployeeSalaryPayment)
                .WithMany(x => x.BonusPayments)
                .HasForeignKey(x => x.EmployeeSalaryPaymentId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeSalaryStructureBonusId)
                .HasColumnName("EmployeeSalaryStructureBonus_Id");
            HasRequired(x => x.EmployeeSalaryStructureBonus)
                .WithMany(x => x.BonusPayments)
                .HasForeignKey(x => x.EmployeeSalaryStructureBonusId)
                .WillCascadeOnDelete(false);

        }
    }
}
