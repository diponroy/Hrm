﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeSalaryPaymentConfig : Configuration<EmployeeSalaryPayment>
    {
        public EmployeeSalaryPaymentConfig() : base(Table.EmployeeSalaryPayment)
        {

            Property(x => x.TrackNo)
                .IsRequired()
                .HasMaxLength(20)
                .HasColumnAnnotation("Index" , new IndexAnnotation(
                    new IndexAttribute("UK_TrackNo" , 1) { IsUnique = true })) ;

            Property(x => x.DateOfPayment)
                .IsRequired();

            Property(x => x.TotalInAmount);

            Property(x => x.EmployeeSalaryStructureId)
                .HasColumnName("EmployeeSalaryStructure_Id");
            HasRequired(x => x.EmployeeSalaryStructure)
                .WithMany(x => x.SalaryPayments)
                .HasForeignKey(x => x.EmployeeSalaryStructureId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeePayrollInfoId)
                .HasColumnName("EmployeePayrollInfo_Id");
            HasRequired(x => x.EmployeePayrollInfo)
                .WithMany(x => x.SalaryPayments)
                .HasForeignKey(x => x.EmployeePayrollInfoId)
                .WillCascadeOnDelete(false);
        }
    }
}
