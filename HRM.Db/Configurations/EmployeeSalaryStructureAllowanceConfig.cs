﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeSalaryStructureAllowanceConfig : Configuration<EmployeeSalaryStructureAllowance>
    {
        public EmployeeSalaryStructureAllowanceConfig(): base(Table.EmployeeSalaryStructureAllowance)
        {
            Ignore(x => x.IsDetached);


            Property(x => x.AsPercentage)
                .IsRequired();

            Property(x => x.Weight)
                .IsRequired();

            Property(x => x.AttachmentDate)
                .HasColumnType("DATE");

            Property(x => x.DetachmentDate)
                .HasColumnType("DATE");

            Property(x => x.EmployeeSalaryStructureId)
                .HasColumnName("EmployeeSalaryStructure_Id");
            HasRequired(x => x.EmployeeSalaryStructure)
                .WithMany(l => l.EmployeeSalaryStructureAllowances)
                .HasForeignKey(x => x.EmployeeSalaryStructureId)
                .WillCascadeOnDelete(false);

            Property(x => x.AllowanceId)
                .HasColumnName("Allowance_Id");
            HasRequired(x => x.Allowance)
                .WithMany(l => l.EmployeeSalaryStructureAllowances)
                .HasForeignKey(x => x.AllowanceId)
                .WillCascadeOnDelete(false);
        }
    }
}
