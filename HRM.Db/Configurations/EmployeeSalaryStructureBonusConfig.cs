﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeSalaryStructureBonusConfig : Configuration<EmployeeSalaryStructureBonus>
    {
        public EmployeeSalaryStructureBonusConfig()
            : base(Table.EmployeeSalaryStructureBonus)
        {
            Ignore(x => x.IsDetached);

            Property(x => x.AsPercentage)
                .IsRequired();

            Property(x => x.Weight)
                .IsRequired();

            Property(x => x.AttachmentDate)
                .HasColumnType("DATE");

            Property(x => x.DetachmentDate)
                .HasColumnType("DATE") ;

            Property(x => x.EmployeeSalaryStructureId)
                .HasColumnName("EmployeeSalaryStructure_Id");
            HasRequired(x => x.EmployeeSalaryStructure)
                .WithMany(l => l.EmployeeSalaryStructureBonuses)
                .HasForeignKey(x => x.EmployeeSalaryStructureId)
                .WillCascadeOnDelete(false);

            Property(x => x.BonusId)
                .HasColumnName("Bonus_Id");
            HasRequired(x => x.Bonus)
                .WithMany(l => l.EmployeeSalaryStructureBonuses)
                .HasForeignKey(x => x.BonusId)
                .WillCascadeOnDelete(false);
        }
    }
}
