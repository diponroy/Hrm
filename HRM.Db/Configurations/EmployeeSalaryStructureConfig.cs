﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeSalaryStructureConfig : Configuration<EmployeeSalaryStructure>
    {
        public EmployeeSalaryStructureConfig() : base(Table.EmployeeSalaryStructure)
        {
            Ignore(x => x.IsDetached);

            Property(x => x.Basic)
                .IsRequired();

            Property(x => x.AttachmentDate)
                .IsRequired(); 

            Property(x => x.SalaryStructureId)
                .HasColumnName("SalaryStructure_Id");
            HasRequired(x => x.SalaryStructure)
                .WithMany(l => l.EmployeeSalaryStructures)
                .HasForeignKey(x => x.SalaryStructureId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeWorkstationUnitId)
                .HasColumnName("EmployeeWorkstationUnit_Id");
            HasRequired(x => x.EmployeeWorkStationUnit)
                .WithMany(l => l.EmployeeSalaryStructures)
                .HasForeignKey(x => x.EmployeeWorkstationUnitId)
                .WillCascadeOnDelete(false);
        }
    }
}
