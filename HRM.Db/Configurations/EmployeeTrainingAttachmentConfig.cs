﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeTrainingAttachmentConfig : Configuration<EmployeeTrainingAttachment>
    {
        public EmployeeTrainingAttachmentConfig()
            : base(Table.EmployeeTrainingAttachment)
        {
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(150);

            Property(x => x.Directory)
                .HasMaxLength(250);

            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();


            /* foreign keys */
            Property(x => x.EmployeeTrainingId)
                .HasColumnName("EmployeeTraining_Id");
            HasRequired(x=>x.EmployeeTraining)
                .WithMany(x=>x.EmployeeTrainingAttachments)
                .HasForeignKey(x=>x.EmployeeTrainingId)
                .WillCascadeOnDelete(false);
        }
    }
}
