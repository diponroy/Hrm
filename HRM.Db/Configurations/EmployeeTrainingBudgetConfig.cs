﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeTrainingBudgetConfig : Configuration<EmployeeTrainingBudget>
    {
        public EmployeeTrainingBudgetConfig() : base(Table.EmployeeTrainingBudget)
        {
            Property(x => x.Description).IsRequired().HasMaxLength(400);

            Property(x => x.Remarks).HasMaxLength(250);

            Property(x => x.IsApproved).IsRequired();

            Property(x => x.TotalAmount).IsRequired();

            Property(x => x.Status).IsRequired();


            /* foreign key */
            Property(x => x.EmployeeTrainingId).HasColumnName("EmployeeTraining_Id");
            HasRequired(x => x.EmployeeTraining)
                .WithMany(x => x.EmployeeTrainingBudgets)
                .HasForeignKey(x => x.EmployeeTrainingId)
                .WillCascadeOnDelete(false);
        }
    }
}
