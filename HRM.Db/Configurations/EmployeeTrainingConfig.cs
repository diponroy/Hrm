﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeTrainingConfig : Configuration<EmployeeTraining>
    {
        public EmployeeTrainingConfig() : base(Table.EmployeeTraining)
        {
            Property(x => x.Title).IsRequired().HasMaxLength(100);

            Property(x => x.Description).HasMaxLength(250);

            Property(x => x.Organizer).HasMaxLength(150);

            Property(x => x.Venue).HasMaxLength(150);

            Property(x => x.Trainer).HasMaxLength(100);

            Property(x => x.IsApproved).IsRequired();

            Property(x => x.Remarks).HasMaxLength(150);

            Property(x => x.DurationFromDate).IsRequired().HasColumnType("DATE");

            Property(x => x.DurationToDate).IsRequired().HasColumnType("DATE");

            Property(x => x.Status).IsRequired();
        }
    }
}
