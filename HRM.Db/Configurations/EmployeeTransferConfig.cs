﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeTransferConfig : Configuration<EmployeeTransfer>
    {
        public EmployeeTransferConfig() : base(Table.EmployeeTransfer)
        {
            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.IsApproved)
                .IsRequired();

            Property(x => x.Status)
                .IsRequired();


            /* foreign keys */
            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(x => x.Employee)
                .WithMany(l => l.EmployeeTransfers)
                .HasForeignKey(f => f.EmployeeId)
                .WillCascadeOnDelete(false);

            Property(x => x.WorkStationUnitId)
                .HasColumnName("WorkStationUnit_Id");
            HasRequired(x => x.WorkStationUnit)
                .WithMany(l => l.EmployeeTransfers)
                .HasForeignKey(f => f.WorkStationUnitId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeTypeId)
                .HasColumnName("EmployeeType_Id");
            HasRequired(x => x.EmployeeType)
                .WithMany(l => l.EmployeeTransfers)
                .HasForeignKey(f => f.EmployeeTypeId)
                .WillCascadeOnDelete(false);

            Property(x => x.IncrementSalaryStructureId)
                .HasColumnName("IncrementSalaryStructure_Id");
            HasOptional(x => x.IncrementSalaryStructure)
                .WithMany()
                .HasForeignKey(f => f.IncrementSalaryStructureId)
                .WillCascadeOnDelete(false);
        }
    }
}
