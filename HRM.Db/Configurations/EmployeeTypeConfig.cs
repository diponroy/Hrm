﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeTypeConfig : Configuration<EmployeeType>
    {
        public EmployeeTypeConfig() : base(Table.EmployeeType)
        {
            Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("TitleIndex", 1))); 

            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();

            /* foreign keys */
            Property(x => x.ParentId)
                .HasColumnName("EmployeeType_Id_AsParentId") ;
            HasOptional(x => x.ParentEmployeeType)
                .WithMany(x => x.ChildEmployeeTypes)
                .HasForeignKey(x => x.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}
