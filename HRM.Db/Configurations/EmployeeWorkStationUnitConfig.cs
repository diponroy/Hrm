﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class EmployeeWorkStationUnitConfig: Configuration<EmployeeWorkStationUnit>
    {
        public EmployeeWorkStationUnitConfig()
            : base(Table.EmployeeWorkStationUnit)
        {
            Ignore(x => x.IsDetached);

            Property(x => x.Remarks)
                .HasMaxLength(250);

            Property(x => x.Status)
                .IsRequired();

            Property(x => x.AttachmentDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.DetachmentDate)
                .HasColumnType("DATE");

            /* foreign key */
            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(x=>x.Employee)
                .WithMany(x=>x.EmployeeWorkStationUnits)
                .HasForeignKey(x=>x.EmployeeId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeTypeId)
                .HasColumnName("EmployeeType_Id");
            HasRequired(x => x.EmployeeType)
                .WithMany(x => x.EmployeeWorkStationUnits)
                .HasForeignKey(x => x.EmployeeTypeId)
                .WillCascadeOnDelete(false);

            Property(x => x.WorkStationUnitId)
                .HasColumnName("WorkStationUnit_Id");
            HasRequired(x => x.WorkStationUnit)
                .WithMany(x => x.EmployeeWorkStationUnits)
                .HasForeignKey(x => x.WorkStationUnitId)
                .WillCascadeOnDelete(false);
        }
    }
}
