﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class IncrementSalaryStructureAllowanceConfig : Configuration<IncrementSalaryStructureAllowance>
    {
        public IncrementSalaryStructureAllowanceConfig() : base(Table.IncrementSalaryStructureAllowance)
        {
            Property(x => x.AsPercentage)
                .IsRequired();

            Property(x => x.Weight)
                .IsRequired();

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.IncrementSalaryStructureId)
                .HasColumnName("IncrementSalaryStructure_Id");
            HasRequired(x=>x.IncrementSalaryStructure)
                .WithMany()
                .HasForeignKey(x=>x.IncrementSalaryStructureId)
                .WillCascadeOnDelete(false);

            Property(x => x.AllowanceId)
                .HasColumnName("Allowance_Id");
            HasRequired(x => x.Allowance)
                .WithMany()
                .HasForeignKey(x => x.AllowanceId)
                .WillCascadeOnDelete(false);
        }
    }
}
