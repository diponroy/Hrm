﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class IncrementSalaryStructureBonusConfig : Configuration<IncrementSalaryStructureBonus>
    {
        public IncrementSalaryStructureBonusConfig() : base(Table.IncrementSalaryStructureBonus)
        {
            Property(x => x.AsPercentage)
                 .IsRequired();

            Property(x => x.Weight)
                .IsRequired();

            Property(x => x.Status)
                .IsRequired();


            /* foreign keys */
            Property(x => x.IncrementSalaryStructureId)
                .HasColumnName("IncrementSalaryStructure_Id");
            HasRequired(x => x.IncrementSalaryStructure)
                .WithMany()
                .HasForeignKey(x => x.IncrementSalaryStructureId)
                .WillCascadeOnDelete(false);

            Property(x => x.BonusId)
                .HasColumnName("Bonus_Id");
            HasRequired(x => x.Bonus)
                .WithMany()
                .HasForeignKey(x => x.BonusId)
                .WillCascadeOnDelete(false);
        }
    }
}