﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class IncrementSalaryStructureConfig : Configuration<IncrementSalaryStructure>
    {
        public IncrementSalaryStructureConfig() : base(Table.IncrementSalaryStructure)
        {
            Property(x => x.Basic)
                .IsRequired();

            Property(x => x.IsApproved)
                .IsRequired();

            Property(x => x.Status)
                .IsRequired();


            /* foreign key */
            Property(x => x.EmployeeSalaryStructureId)
                .HasColumnName("EmployeeSalaryStructure_Id");
            HasOptional(x => x.EmployeeSalaryStructure)
                .WithMany()
                .HasForeignKey(l => l.EmployeeSalaryStructureId)
                .WillCascadeOnDelete(false);
        }
    }
}
