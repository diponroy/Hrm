﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class InterviewCandidateConfig : Configuration<InterviewCandidate>
    {
        public InterviewCandidateConfig( ) : base(Table.InterviewCandidate)
        {
            Property(p => p.AppointmentDateTime)
                .HasColumnType("DATE");

            Property(p => p.AttendedDateTime)
                .IsRequired()
                .HasColumnType("DATE");

            Property(p => p.CurrentSalary);

            Property(p => p.ExpectedSalary)
                .IsRequired();

            Property(p => p.ProposedSalary)
                .IsRequired();

            Property(p => p.Remarks)
                .IsRequired()
                .HasMaxLength(250);

            Property(p => p.InterviewId)
                .IsRequired();

            
            /* foreign key */
            //HasMany(x=>x.Candidates)
            //    .WithOptional(x=>x.InterviewCandidate)
            //    .HasForeignKey(x=>x.InterviewCandidateId)
            //    .WillCascadeOnDelete(false);

            Property(x => x.CandidateId)
                .HasColumnName("Candidate_Id");
            HasRequired(x => x.Candidate)
                .WithMany()
                .HasForeignKey(x => x.CandidateId)
                .WillCascadeOnDelete(false);

            Property(x => x.InterviewId)
                .HasColumnName("Interview_Id") ;
            HasRequired(x=>x.Interview)
                .WithMany()
                .HasForeignKey(x=>x.InterviewId)
                .WillCascadeOnDelete(false);

        }
    }
}
