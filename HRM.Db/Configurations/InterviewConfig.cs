﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class InterviewConfig : Configuration<Interview>
    {
        public InterviewConfig() : base(Table.Interview)
        {
            Property(p => p.TrackNo)
               .IsRequired()
               .HasMaxLength(20)
               .HasColumnAnnotation("Index", new IndexAnnotation(
                   new IndexAttribute("UK_TrackNo", 1) { IsUnique = true }));

            Property(p => p.Title)
                .HasMaxLength(250) ;

            Property(p => p.Remarks)
                .HasMaxLength(250) ;

            Property(p => p.DurationFromDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(p => p.DurationToDate)
                .HasColumnType("DATE"); 

             /*Foreign Key*/
            Property(p => p.ManpowerVacancyNoticeId)
                .HasColumnName("ManpowerVacancyNotice_Id") ;
            HasRequired(p => p.ManpowerVacancyNotice)
                .WithMany()
                .HasForeignKey(x => x.ManpowerVacancyNoticeId)
                .WillCascadeOnDelete(false); 
        }
    }
}
