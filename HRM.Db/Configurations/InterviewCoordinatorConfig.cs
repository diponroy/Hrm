﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class InterviewCoordinatorConfig :Configuration<InterviewCoordinator>
    {
        public InterviewCoordinatorConfig( ) : base(Table.InterviewCoordinator)
        {
            Property(p => p.Remarks)
                .HasMaxLength(250);

            /*foreign keys */
            Property(x => x.EmployeeId)
                .HasColumnName("Employee_Id");
            HasRequired(r => r.Employee)
                .WithMany(m => m.InterviewCoordinators)
                .HasForeignKey(f => f.EmployeeId)
                .WillCascadeOnDelete(false);

            Property(x => x.InterviewId)
                .HasColumnName("Interview_Id");
            HasRequired(r => r.Interview)
                .WithMany(m => m.InterviewCoordinators)
                .HasForeignKey(f => f.InterviewId)
                .WillCascadeOnDelete(false);
        }
    }
}
