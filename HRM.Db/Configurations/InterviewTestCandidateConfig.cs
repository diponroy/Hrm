﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class InterviewTestCandidateConfig : Configuration<InterviewTestCandidate>
    {
        public InterviewTestCandidateConfig( ) : base(Table.InterviewTestCandidate)
        {
            Property(p => p.ObtainedMarks)
                .IsRequired() ;

            Property(p => p.Remarks)
                .HasMaxLength(250) ;

            Property(x => x.InterviewTestId)
                 .HasColumnName("InterviewTest_Id");
            HasRequired(r => r.InterviewTest)
                .WithMany()
                .HasForeignKey(f => f.InterviewTestId)
                .WillCascadeOnDelete(false);

            Property(x => x.CandidateId)
                 .HasColumnName("Candidate_Id");
            HasRequired(r => r.Candidate)
                .WithMany()
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);


        }
    }
}
