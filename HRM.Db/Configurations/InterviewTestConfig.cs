﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class InterviewTestConfig :Configuration<InterviewTest>
    {
        public InterviewTestConfig( ) : base(Table.InterviewTest)
        {
            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.Remarks)
                .HasMaxLength(250) ;

            Property(x => x.InterviewId)
                 .HasColumnName("Interview_Id");
            HasRequired(r => r.Interview)
                .WithMany(m => m.InterviewTests)
                .HasForeignKey(f => f.InterviewId)
                .WillCascadeOnDelete(false);

        }
    }
}
