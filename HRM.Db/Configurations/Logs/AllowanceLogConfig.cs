﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class AllowanceLogConfig : LogConfiguration<AllowanceLog, Allowance>
    {
        public AllowanceLogConfig() : base(Table.Allowance)
        {
            HasRequired(r => r.AllowanceTypeLog)
                .WithMany()
                .HasForeignKey(f => f.AllowanceTypeId)
                .WillCascadeOnDelete(false);
        }
    }
}
