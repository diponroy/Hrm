﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class AllowanceTypeLogConfig : LogConfiguration<AllowanceTypeLog, AllowanceType>
    {
        public AllowanceTypeLogConfig() : base(Table.AllowanceType)
        {
        }
    }
}
