﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class AppraisalIndicatorLogConfig : LogConfiguration<AppraisalIndicatorLog, AppraisalIndicator>
    {
        public AppraisalIndicatorLogConfig() : base(Table.AppraisalIndicator)
        {
            /* foreign keys */
            Property(x => x.ParentId)
                .HasColumnName("AppraisalIndicatorLog_LogId_AsParentId");
            HasOptional(x => x.ParentAppraisalIndicatorLog)
                .WithMany()
                .HasForeignKey(y => y.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}
