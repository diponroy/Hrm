﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class BonusLogConfig : LogConfiguration<BonusLog, Bonus>
    {
        public BonusLogConfig(): base(Table.Bonus)
        {
            Property(x => x.BonusTypeId).HasColumnName("BonuesLog_LogId_AsBonusTypeLogId");
            HasRequired(r => r.BonusTypeLog)
                .WithMany()
                .HasForeignKey(f => f.BonusTypeId)
                .WillCascadeOnDelete(false);
        }
    }
}
