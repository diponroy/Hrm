﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class BonusPaymentDateLogConfig : LogConfiguration<BonusPaymentDateLog, BonusPaymentDate>
    {
        public BonusPaymentDateLogConfig()  : base(Table.BonusPaymentDate)
        {
            HasRequired(r => r.BonusLog)
               .WithMany()
               .HasForeignKey(f => f.BonusId)
               .WillCascadeOnDelete(false);
        }
    }
}
