﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class BonusTypeLogConfig : LogConfiguration<BonusTypeLog, BonusType>
    {
        public BonusTypeLogConfig()
            : base(Table.BonusType)
        {
        }
    }
}
