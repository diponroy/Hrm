﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class BranchLogConfig : LogConfiguration<BranchLog, Branch>
    {
        public BranchLogConfig( ) 
            : base(Table.Branch)
        {
            Ignore(x => x.IsClosed);
        }
    }
}
