﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    class CandidateAchievementLogConfig : CandidateLogConfiguration<CandidateAchievementLog, CandidateAchievement>
    {
        public CandidateAchievementLogConfig() : base(Table.CandidateAchievement)
        {
            Property(x => x.CandidateEmploymentId)
                .HasParameterName("CandidateEmploymentLog_Id");
            HasRequired(x => x.CandidateEmploymentLog)
                .WithMany()
                .HasForeignKey(f => f.CandidateEmploymentId)
                .WillCascadeOnDelete(false);
        }
    }
}
