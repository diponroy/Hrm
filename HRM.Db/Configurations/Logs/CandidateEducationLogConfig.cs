﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class CandidateEducationLogConfig : CandidateLogConfiguration<CandidateEducationLog, CandidateEducation>
    {
        public CandidateEducationLogConfig() : base(Table.CandidateEducation)
        {
            Property(x => x.CandidateId)
                .HasParameterName("CandidateLog_Id");
            HasRequired(x => x.CandidateLog)
                .WithMany()
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
