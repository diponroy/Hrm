﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class CandidateEmploymentLogConfig : CandidateLogConfiguration<CandidateEmploymentLog, CandidateEmployment>
    {
        public CandidateEmploymentLogConfig() : base(Table.CandidateEmployment)
        {
            Property(x => x.CandidateId)
                .HasParameterName("CandidateLog_Id");
            HasRequired(x => x.CandidateLog)
                .WithMany()
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
