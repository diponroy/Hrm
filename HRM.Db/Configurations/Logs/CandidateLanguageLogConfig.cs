﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class CandidateLanguageLogConfig : CandidateLogConfiguration<CandidateLanguageLog, CandidateLanguage>
    {
        public CandidateLanguageLogConfig() : base(Table.CandidateLanguage)
        {
            Property(x => x.CandidateId)
                .HasParameterName("CandidateLog_Id");
            HasRequired(x => x.CandidateLog)
                .WithMany()
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
