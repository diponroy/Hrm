﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class CandidateLogConfig : CandidateLogConfiguration<CandidateLog, Candidate>
    {
        public CandidateLogConfig ()
            : base(Table.Candidate)
        {

        }
    }
}
