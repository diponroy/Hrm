﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class CandidateLoginLogConfig : CandidateLogConfiguration<CandidateLoginLog, CandidateLogin>
    {
        public CandidateLoginLogConfig() : base(Table.CandidateLogin)
        {
            HasRequired(x => x.CandidateLog)
                .WithMany()
                .HasForeignKey(x => x.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
