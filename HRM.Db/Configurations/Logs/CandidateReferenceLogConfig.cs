﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class CandidateReferenceLogConfig : CandidateLogConfiguration<CandidateReferenceLog, CandidateReference>
    {
        public CandidateReferenceLogConfig() : base(Table.CandidateReference)
        {
            Property(x => x.CandidateId)
                .HasParameterName("CandidateLog_Id");
            HasRequired(x => x.CandidateLog)
                .WithMany()
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
