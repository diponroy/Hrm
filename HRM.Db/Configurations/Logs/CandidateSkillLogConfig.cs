﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    class CandidateSkillLogConfig : CandidateLogConfiguration<CandidateSkillLog, CandidateSkill>
    {
        public CandidateSkillLogConfig() : base(Table.CandidateSkill)
        {
            Property(x => x.CandidateId)
    .HasParameterName("CandidateLog_Id");
            HasRequired(x => x.CandidateLog)
                .WithMany()
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
