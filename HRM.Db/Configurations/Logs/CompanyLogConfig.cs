﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class CompanyLogConfig : LogConfiguration<CompanyLog, Company>
    {
        public CompanyLogConfig () : base(Table.Company)
        {
            Ignore(x => x.IsClosed);
        }
    }
}
