﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    class DepartmentLogConfig : LogConfiguration<DepartmentLog, Department>
    {
        public DepartmentLogConfig( ) : base(Table.Department)
        {
            
        }
    }
}
