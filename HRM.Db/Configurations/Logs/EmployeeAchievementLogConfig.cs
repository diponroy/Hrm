﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.Log ;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeAchievementLogConfig : LogConfiguration<EmployeeAchievementLog, EmployeeAchievement>

    {
        public EmployeeAchievementLogConfig( ) : base(Table.EmployeeAchievement)
        {
            HasRequired(x => x.LogFor)
                .WithMany(l => l.Logs)
                .HasForeignKey(x => x.Id)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeId)
                .HasParameterName("EmployeeLog_Id");
            HasRequired(x => x.EmployeeLog)
                .WithMany()
                .HasForeignKey(f => f.EmployeeId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeAppraisalId)
                .HasParameterName("EmployeeAppraisalLog_Id");
            HasRequired(x => x.EmployeeAppraisalLog)
                .WithMany()
                .HasForeignKey(f => f.EmployeeAppraisalId)
                .WillCascadeOnDelete(false);
        }
    }
}
