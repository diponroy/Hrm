﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeAssignedAppraisalLogConfig : LogConfiguration<EmployeeAssignedAppraisalLog, EmployeeAssignedAppraisal>
    {
        public EmployeeAssignedAppraisalLogConfig() : base(Table.EmployeeAssignedAppraisal)
        {
            Ignore(x => x.IsDetached);

            HasRequired(x => x.AppraisalIndicatorLog)
                .WithMany()
                .HasForeignKey(x => x.AppraisalIndicatorId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.EmployeeWorkStationUnitLog)
               .WithMany()
               .HasForeignKey(x => x.EmployeeWorkStationUnitId)
               .WillCascadeOnDelete(false);
        }
    }
}
