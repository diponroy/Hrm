﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeAttachedTrainingLogConfig : LogConfiguration<EmployeeAttachedTrainingLog, EmployeeAttachedTraining>
    {
        public EmployeeAttachedTrainingLogConfig() : base(Table.EmployeeAttachedTraining)
        {
            HasRequired(x => x.EmployeeLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.EmployeeTrainingLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeTrainingId)
                .WillCascadeOnDelete(false);
        }
    }
}
