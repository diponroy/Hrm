﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeAttendanceLogConfig : LogConfiguration<EmployeeAttendanceLog, EmployeeAttendance>
    {
        public EmployeeAttendanceLogConfig (): base(Table.EmployeeAttendance) 
        {
            Property(x => x.EmployeeWorkStationUnitId)
                .HasColumnName("EmployeeWorkStationUnitLog_Id");
            
            HasRequired(x => x.EmployeeWorkStationUnitLog)
               .WithMany()
               .HasForeignKey(x => x.EmployeeWorkStationUnitId)
               .WillCascadeOnDelete(false);  
        }
    }
}
