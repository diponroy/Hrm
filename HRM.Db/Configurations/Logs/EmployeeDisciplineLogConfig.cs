﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeDisciplineLogConfig:LogConfiguration<EmployeeDisciplineLog, EmployeeDiscipline>
    {  
        public EmployeeDisciplineLogConfig () : base(Table.EmployeeDiscipline)
        {
            HasRequired(x => x.LogFor)
                .WithMany(l => l.Logs)
                .HasForeignKey(x => x.Id)
                .WillCascadeOnDelete(false);
            
            /* foreign key */
            Property(x => x.EmployeeId)
                .HasColumnName("EmployeeLog_Id");
            HasRequired(x => x.EmployeeLog)
                .WithMany()
                .HasForeignKey(f => f.EmployeeId)
                .WillCascadeOnDelete(false);


            Property(x => x.DepartmentId)
                .HasColumnName("DepartmentLog_Id");
            HasRequired(x => x.DepartmentLog)
                .WithMany()
                .HasForeignKey(f => f.DepartmentId)
                .WillCascadeOnDelete(false); 
        }   
    }
}
