﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeEducationLogConfig : LogConfiguration<EmployeeEducationLog, EmployeeEducation>
    {
        public EmployeeEducationLogConfig() : base(Table.EmployeeEducation)
        {
            /*foreign keys */

            HasRequired(x => x.LogFor)
                .WithMany(l => l.Logs)
                .HasForeignKey(x => x.Id)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeId)
                .HasParameterName("EmployeeLog_Id_AsEmployeeId");
            HasRequired(x => x.EmployeeLog)
                .WithMany()
                .HasForeignKey(f => f.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
