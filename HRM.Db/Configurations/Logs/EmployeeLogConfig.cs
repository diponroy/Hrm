﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeLogConfig : LogConfiguration<EmployeeLog, Employee>
    {
        public EmployeeLogConfig( ) : base(Table.Employee)
        {
            Ignore(x => x.FullName);
        }
    }
}
