﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeLoginLogConfig : LogConfiguration<EmployeeLoginLog, EmployeeLogin>
    {
        public EmployeeLoginLogConfig()
            : base(Table.EmployeeLogin)
        {
            HasRequired(x => x.EmployeeLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
