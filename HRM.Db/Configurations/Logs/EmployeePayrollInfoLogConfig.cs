﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeePayrollInfoLogConfig : LogConfiguration<EmployeePayrollInfoLog, EmployeePayrollInfo>
    {
        public EmployeePayrollInfoLogConfig() : base(Table.EmployeePayrollInfo)
        {
            /*employee*/
            Property(x => x.EmployeeId)
                .HasColumnName("EmployeeLog_Id");
            HasRequired(x => x.EmployeeLog)
                .WithMany()
                .HasForeignKey(l => l.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
