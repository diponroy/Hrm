﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeReportingLogConfig : LogConfiguration<EmployeeReportingLog, EmployeeReporting>
    {
        public EmployeeReportingLogConfig() : base(Table.EmployeeReporting)
        {
            Ignore(x => x.IsDetached);

            HasRequired(x=>x.EmployeeWorkStationUnitLog)
                .WithMany()
                .HasForeignKey(x=>x.EmployeeWorkStationUnitId)
                .WillCascadeOnDelete(false);

            HasRequired(x=>x.ParentEmployeeWorkStationUnitLog)
                .WithMany()
                .HasForeignKey(x=>x.ParentEmployeeWorkStationUnitId)
                .WillCascadeOnDelete(false);
        }
    }
}
