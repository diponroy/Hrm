﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeSalaryPaymentAllowanceLogConfig : LogConfiguration<EmployeeSalaryPaymentAllowanceLog, EmployeeSalaryPaymentAllowance>
    {
        public EmployeeSalaryPaymentAllowanceLogConfig(): base(Table.EmployeeSalaryPaymentAllowance)
        {
            /*EmployeeSalaryPayment*/
            Property(x => x.EmployeeSalaryPaymentId)
                .HasColumnName("EmployeeSalaryPaymentLog_Id");
            HasRequired(x => x.EmployeeSalaryPaymentLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeSalaryPaymentId)
                .WillCascadeOnDelete(false);

            /*EmployeeSalaryStructureAllowance*/
            Property(x => x.EmployeeSalaryStructureAllowanceId)
                .HasColumnName("EmployeeSalaryStructureAllowanceLog_Id");
            HasRequired(x => x.EmployeeSalaryStructureAllowanceLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeSalaryStructureAllowanceId)
                .WillCascadeOnDelete(false);
        }
    }
}
