﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeSalaryPaymentBonusLogConfig : LogConfiguration<EmployeeSalaryPaymentBonusLog, EmployeeSalaryPaymentBonus>
    {
        public EmployeeSalaryPaymentBonusLogConfig(): base(Table.EmployeeSalaryPaymentBonus)
        {
            /*EmployeeSalaryPayment*/
            Property(x => x.EmployeeSalaryPaymentId)
                .HasColumnName("EmployeeSalaryPaymentLog_Id");
            HasRequired(x => x.EmployeeSalaryPaymentLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeSalaryPaymentId)
                .WillCascadeOnDelete(false);


            /*EmployeeSalaryStructureBonus*/
            Property(x => x.EmployeeSalaryStructureBonusId)
                .HasColumnName("EmployeeSalaryStructureBonusLog_Id");
            HasRequired(x => x.EmployeeSalaryStructureBonusLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeSalaryStructureBonusId)
                .WillCascadeOnDelete(false);
        }
    }
}
