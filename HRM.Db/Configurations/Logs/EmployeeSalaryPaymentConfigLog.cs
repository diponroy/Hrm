﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeSalaryPaymentLogConfig : LogConfiguration<EmployeeSalaryPaymentLog, EmployeeSalaryPayment>
    {
        public EmployeeSalaryPaymentLogConfig() : base(Table.EmployeeSalaryPayment)
        {

            /*EmployeeSalaryStructure*/
            Property(x => x.EmployeeSalaryStructureId)
                .HasColumnName("EmployeeSalaryStructureLog_Id");
            HasRequired(x => x.EmployeeSalaryStructureLog)
               .WithMany()
               .HasForeignKey(x => x.EmployeeSalaryStructureId)
               .WillCascadeOnDelete(false);

            /*EmployeePayrollInfo*/
            Property(x => x.EmployeePayrollInfoId)
                .HasColumnName("EmployeePayrollInfoLog_Id");
           HasRequired(x => x.EmployeePayrollInfoLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeePayrollInfoId)
                .WillCascadeOnDelete(false);
        }
    }
}
