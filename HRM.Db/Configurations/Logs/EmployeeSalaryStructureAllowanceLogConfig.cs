﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeSalaryStructureAllowanceLogConfig : LogConfiguration<EmployeeSalaryStructureAllowanceLog, EmployeeSalaryStructureAllowance>
    {
        public EmployeeSalaryStructureAllowanceLogConfig() : base(Table.EmployeeSalaryStructureAllowance)
        {
            Ignore(x => x.IsDetached);

            HasRequired(x => x.EmployeeSalaryStructureLog)
               .WithMany()
               .HasForeignKey(x => x.EmployeeSalaryStructureId)
               .WillCascadeOnDelete(false);

            HasRequired(x => x.AllowanceLog)
                .WithMany()
                .HasForeignKey(x => x.AllowanceId)
                .WillCascadeOnDelete(false);
        }
    }
}
