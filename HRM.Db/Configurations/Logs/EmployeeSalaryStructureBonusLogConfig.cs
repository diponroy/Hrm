﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeSalaryStructureBonusLogConfig : LogConfiguration<EmployeeSalaryStructureBonusLog, EmployeeSalaryStructureBonus>
    {
        public EmployeeSalaryStructureBonusLogConfig() : base(Table.EmployeeSalaryStructureBonus)
        {
            Ignore(x => x.IsDetached);

            HasRequired(x => x.EmployeeSalaryStructureLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeSalaryStructureId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.BonusLog)
                .WithMany()
                .HasForeignKey(x => x.BonusId)
                .WillCascadeOnDelete(false);
        }
    }
}
