﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeSalaryStructureLogConfig : LogConfiguration<EmployeeSalaryStructureLog, EmployeeSalaryStructure>
    {
        public EmployeeSalaryStructureLogConfig(): base(Table.EmployeeSalaryStructure)
        {
            Ignore(x => x.IsDetached);

            HasRequired(x=>x.SalaryStructureLog)
                .WithMany()
                .HasForeignKey(x=>x.SalaryStructureId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.EmployeeWorkstationUnitLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeWorkstationUnitId)
                .WillCascadeOnDelete(false);
        }
    }
}
