﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeTrainingAttachmentLogConfig : LogConfiguration<EmployeeTrainingAttachmentLog, EmployeeTrainingAttachment>
    {
        public EmployeeTrainingAttachmentLogConfig() : base(Table.EmployeeTrainingAttachment)
        {
            HasRequired(x=>x.EmployeeTrainingLog)
                .WithMany()
                .HasForeignKey(y=>y.EmployeeTrainingId)
                .WillCascadeOnDelete(false);
        }
    }
}
