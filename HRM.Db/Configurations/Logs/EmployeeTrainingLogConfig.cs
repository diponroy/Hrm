﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeTrainingLogConfig : LogConfiguration<EmployeeTrainingLog, EmployeeTraining>
    {
        public EmployeeTrainingLogConfig() : base(Table.EmployeeTraining)
        {
        }
    }
}
