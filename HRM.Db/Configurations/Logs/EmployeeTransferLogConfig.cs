﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeTransferLogConfig : LogConfiguration<EmployeeTransferLog, EmployeeTransfer>
    {
        public EmployeeTransferLogConfig() : base(Table.EmployeeTransfer)
        {
            /* foreign keys */
            Property(x => x.EmployeeId)
                .HasColumnName("EmployeeLog_Id");
            HasRequired(x => x.EmployeeLog)
                .WithMany()
                .HasForeignKey(f => f.EmployeeId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeTypeId)
                .HasColumnName("EmployeeTypeLog_Id");
            HasRequired(x => x.EmployeeTypeLog)
                .WithMany()
                .HasForeignKey(f => f.EmployeeTypeId)
                .WillCascadeOnDelete(false);

            Property(x => x.WorkStationUnitId)
                .HasColumnName("WorkStationUnitLog_Id");
            HasRequired(x => x.WorkStationUnitLog)
                .WithMany()
                .HasForeignKey(f => f.WorkStationUnitId)
                .WillCascadeOnDelete(false); 

            Property(x => x.IncrementSalaryStructureId)
                .HasColumnName("IncrementSalaryStructureId_LogId");
            HasOptional(x => x.IncrementSalaryStructureLog)
                .WithMany()
                .HasForeignKey(f => f.IncrementSalaryStructureId)
                .WillCascadeOnDelete(false);
        }
    }
}
