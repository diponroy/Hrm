﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeTypeLogConfig : LogConfiguration<EmployeeTypeLog, EmployeeType>
    {
        public EmployeeTypeLogConfig() : base(Table.EmployeeType)
        {
            /* foreign keys */
            Property(x => x.ParentId)
                .HasColumnName("EmployeeTypeLog_LogId_AsParentId");
            HasOptional(x => x.ParentEmployeeTypeLog)
                .WithMany()
                .HasForeignKey(x => x.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}
