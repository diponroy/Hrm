﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class EmployeeWorkStationUnitLogConfig : LogConfiguration<EmployeeWorkStationUnitLog, EmployeeWorkStationUnit>
    {
        public EmployeeWorkStationUnitLogConfig() : base(Table.EmployeeWorkStationUnit)
        {
            Ignore(x => x.IsDetached);

            /* foreign key */
            Property(x => x.EmployeeId)
                .HasColumnName("EmployeeLog_Id_AsEmployeeId");
            HasRequired(x => x.EmployeeLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeTypeId)
                .HasColumnName("EmployeeTypeLog_Id_AsEmployeeTypeId");
            HasRequired(x => x.EmployeeTypeLog)
                .WithMany()
                .HasForeignKey(x => x.EmployeeTypeId)
                .WillCascadeOnDelete(false);

            Property(x => x.WorkStationUnitId)
                .HasColumnName("WorkStationUnitLog_Id_AsWorkStationUnitId");
            HasRequired(x => x.WorkStationUnitLog)
                .WithMany()
                .HasForeignKey(x => x.WorkStationUnitId)
                .WillCascadeOnDelete(false);
        }
    }
}
