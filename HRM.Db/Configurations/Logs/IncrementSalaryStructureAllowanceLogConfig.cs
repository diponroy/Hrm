﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class IncrementSalaryStructureAllowanceLogConfig : LogConfiguration<IncrementSalaryStructureAllowanceLog, IncrementSalaryStructureAllowance>
    {
        public IncrementSalaryStructureAllowanceLogConfig() : base(Table.IncrementSalaryStructureAllowance)
        {
            Property(x => x.AllowanceId)
                .HasColumnName("AllowanceLog_Id");
            HasRequired(x=>x.AllowanceLog)
                .WithMany()
                .HasForeignKey(x=>x.AllowanceId)
                .WillCascadeOnDelete(false);


            Property(x => x.IncrementSalaryStructureId)
                .HasColumnName("IncrementSalaryStructureLog_Id");
            HasRequired(x=>x.IncrementSalaryStructureLog)
                .WithMany()
                .HasForeignKey(x=>x.IncrementSalaryStructureId)
                .WillCascadeOnDelete(false);
        }
    }
}
