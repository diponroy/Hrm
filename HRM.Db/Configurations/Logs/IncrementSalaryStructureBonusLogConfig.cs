﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class IncrementSalaryStructureBonusLogConfig : LogConfiguration<IncrementSalaryStructureBonusLog, IncrementSalaryStructureBonus>
    {
        public IncrementSalaryStructureBonusLogConfig() : base(Table.IncrementSalaryStructureBonus)
        {
            Property(x => x.IncrementSalaryStructureId)
                .HasColumnName("IncrementSalaryStructureLog_Id");
            HasRequired(x => x.IncrementSalaryStructureLog)
                .WithMany()
                .HasForeignKey(x => x.IncrementSalaryStructureId)
                .WillCascadeOnDelete(false);

            Property(x => x.BonusId)
                .HasColumnName("BonusLog_Id");
            HasRequired(x => x.BonusLog)
                .WithMany()
                .HasForeignKey(x => x.BonusId)
                .WillCascadeOnDelete(false);
        }
    }
}
