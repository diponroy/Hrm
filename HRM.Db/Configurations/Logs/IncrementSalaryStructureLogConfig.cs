﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class IncrementSalaryStructureLogConfig : LogConfiguration<IncrementSalaryStructureLog, IncrementSalaryStructure>
    {
        public IncrementSalaryStructureLogConfig() : base(Table.IncrementSalaryStructure)
        {
            /* foreign key */
            Property(x => x.EmployeeSalaryStructureId)
                .HasColumnName("EmployeeSalaryStructureId_LogId");
            HasOptional(x => x.EmployeeSalaryStructureLog)
                .WithMany()
                .HasForeignKey(l => l.EmployeeSalaryStructureId)
                .WillCascadeOnDelete(false);
        }
    }
}
