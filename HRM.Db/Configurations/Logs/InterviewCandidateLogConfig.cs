﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    class InterviewCandidateLogConfig : LogConfiguration<InterviewCandidateLog, InterviewCandidate>
    {
        public InterviewCandidateLogConfig( ) : base(Table.InterviewCandidate)
        {
            HasRequired(x => x.CandidateLog)
                .WithMany()
                .HasForeignKey(x => x.CandidateId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.InterviewLog)
                .WithMany()
                .HasForeignKey(x => x.InterviewId)
                .WillCascadeOnDelete(false);
        }
    }
}
