﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class InterviewCoordinatorLogConfig : LogConfiguration<InterviewCoordinatorLog, InterviewCoordinator>
    {
        public InterviewCoordinatorLogConfig ()
            : base(Table.InterviewCoordinator)
        {
            HasRequired(r => r.EmployeeLog)
                 .WithMany()
                 .HasForeignKey(f => f.EmployeeId)
                 .WillCascadeOnDelete(false);

            HasRequired(r => r.InterviewLog)
                .WithMany()
                .HasForeignKey(f => f.InterviewId)
                .WillCascadeOnDelete(false);
        }
    }
}
