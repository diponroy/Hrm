﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class InterviewLogConfig : LogConfiguration<InterviewLog, Interview>
    {
        public InterviewLogConfig (): base(Table.Interview)
        {
            HasRequired(p => p.ManpowerVacancyNoticeLog)
                 .WithMany()
                 .HasForeignKey(x => x.ManpowerVacancyNoticeId)
                 .WillCascadeOnDelete(false); 
        }
    }
}
