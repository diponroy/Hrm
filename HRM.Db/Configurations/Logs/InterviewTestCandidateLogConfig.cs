﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class InterviewTestCandidateLogConfig : LogConfiguration<InterviewTestCandidateLog, InterviewTestCandidate>
    {
        public InterviewTestCandidateLogConfig( ) : 
            base(Table.InterviewTestCandidate)
        {
            HasRequired(r => r.InterviewTestLog)
                .WithMany()
                .HasForeignKey(f => f.InterviewTestId)
                .WillCascadeOnDelete(false);

            HasRequired(r => r.CandidateLog)
                .WithMany()
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
