﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class InterviewTestLogConfig : LogConfiguration<InterviewTestLog, InterviewTest>
    {
        public InterviewTestLogConfig () : base(Table.InterviewTest)
        {
            HasRequired(r => r.InterviewLog)
                 .WithMany()
                 .HasForeignKey(f => f.InterviewId)
                 .WillCascadeOnDelete(false); 
        }
    }
}
