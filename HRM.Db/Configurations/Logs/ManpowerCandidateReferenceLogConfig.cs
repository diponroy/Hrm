﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class ManpowerCandidateReferenceLogConfig : LogConfiguration<ManpowerCandidateReferenceLog, ManpowerCandidateReference>
    {
        public ManpowerCandidateReferenceLogConfig() : base(Table.ManpowerCandidateReference)
        {
            Property(x => x.EmployeeId)
                .HasColumnName("EmployeeId_As_ReferencedBy");
            HasRequired(x=>x.EmployeeReferrerLog)
                .WithMany()
                .HasForeignKey(x=>x.EmployeeId)
                .WillCascadeOnDelete(false);

            HasRequired(x=>x.ManpowerVacancyNoticeCandidateLog)
                .WithMany()
                .HasForeignKey(x=>x.ManpowerVacancyNoticeCandidateId)
                .WillCascadeOnDelete(false);
        }
    }
}
