﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.Log ;

namespace HRM.Db.Configurations.Logs
{
    public class ManpowerRequisitionLogConfig : LogConfiguration<ManpowerRequisitionLog, ManpowerRequisition>
    {
        public ManpowerRequisitionLogConfig ()
            : base(Table.ManpowerRequisition)
        {

        }
    }
}
