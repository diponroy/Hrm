﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    class ManpowerVacancyNoticeCandidateLogConfig : CandidateLogConfiguration<ManpowerVacancyNoticeCandidateLog, ManpowerVacancyNoticeCandidate>
    {
        public ManpowerVacancyNoticeCandidateLogConfig() : base(Table.ManpowerVacancyNoticeCandidate)
        {
            HasRequired(r=>r.ManpowerVacancyNoticeLog)
                .WithMany()
                .HasForeignKey(f=>f.ManpowerVacancyNoticeId)
                .WillCascadeOnDelete(false);

            HasRequired(r => r.CandidateLog)
                .WithMany()
                .HasForeignKey(f => f.CandidateId)
                .WillCascadeOnDelete(false);

            HasOptional(r => r.EmployeeLog)
                .WithMany()
                .HasForeignKey(f => f.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
