﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    class ManpowerVacancyNoticeLogConfig : LogConfiguration<ManpowerVacancyNoticeLog, ManpowerVacancyNotice>
    {
        public ManpowerVacancyNoticeLogConfig( ) : base(Table.ManpowerVacancyNotice)
        {
            HasRequired(r => r.ManpowerRequisitionLog)
                .WithMany()
                .HasForeignKey(f => f.ManpowerRequisitionId)
                .WillCascadeOnDelete(false);
        }
    }
}
