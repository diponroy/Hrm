﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class SalaryStructureAllowanceLogConfig : LogConfiguration<SalaryStructureAllowanceLog, SalaryStructureAllowance>
    {
        public SalaryStructureAllowanceLogConfig(): base(Table.SalaryStructureAllowance)
        {
            HasRequired(x => x.SalaryStructureLog)
              .WithMany()
              .HasForeignKey(x => x.SalaryStructureId)
              .WillCascadeOnDelete(false);

            HasRequired(x => x.AllowanceLog)
                .WithMany()
                .HasForeignKey(x => x.AllowanceId)
                .WillCascadeOnDelete(false);
        }
    }
}
