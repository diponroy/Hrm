﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class SalaryStructureBonusLogConfig : LogConfiguration<SalaryStructureBonusLog, SalaryStructureBonus>
    {
        public SalaryStructureBonusLogConfig() : base(Table.SalaryStructureBonus)
        {
            HasRequired(x => x.SalaryStructureLog)
              .WithMany()
              .HasForeignKey(x => x.SalaryStructureId)
              .WillCascadeOnDelete(false);

            HasRequired(x => x.BonusLog)
                .WithMany()
                .HasForeignKey(x => x.BonusId)
                .WillCascadeOnDelete(false);
        }
    }
}
