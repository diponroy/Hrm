﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class SalaryStructureLogConfig : LogConfiguration<SalaryStructureLog, SalaryStructure>
    {
        public SalaryStructureLogConfig() : base(Table.SalaryStructure)
        {
        }
    }
}
