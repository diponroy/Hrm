﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Configurations.Logs
{
    public class WorkStationUnitLogConfig : LogConfiguration<WorkStationUnitLog, WorkStationUnit>
    {
        public WorkStationUnitLogConfig() : base(Table.WorkStationUnit)
        {
            Ignore(x => x.IsClosed);

            /*foreign keys */
            /*Company*/
            HasOptional(r => r.CompanyLog)
                .WithMany()
                .HasForeignKey(f => f.CompanyId)
                .WillCascadeOnDelete(false);

            /*Branch*/
            HasOptional(r => r.BranchLog)
                .WithMany()
                .HasForeignKey(f => f.BranchId)
                .WillCascadeOnDelete(false);

            /*Department*/
            HasOptional(r => r.DepartmentLog)
                .WithMany()
                .HasForeignKey(f => f.DepartmentId)
                .WillCascadeOnDelete(false);

            /*WorkStationUnit*/
            HasOptional(p => p.ParentWorkStationUnitLog)
                .WithMany()
                .HasForeignKey(p => p.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}
