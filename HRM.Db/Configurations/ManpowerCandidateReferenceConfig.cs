﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class ManpowerCandidateReferenceConfig : Configuration<ManpowerCandidateReference>
    {
        public ManpowerCandidateReferenceConfig() : base(Table.ManpowerCandidateReference)
        {
            Property(x => x.Remarks)
                .IsOptional()
                .HasMaxLength(250);

            Property(x => x.ManpowerVacancyNoticeCandidateId)
                .IsRequired();
            HasRequired(x=>x.ManpowerVacancyNoticeCandidate)
                .WithMany()
                .HasForeignKey(x=>x.ManpowerVacancyNoticeCandidateId)
                .WillCascadeOnDelete(false);

            Property(x => x.EmployeeId)
                .IsRequired();
            HasRequired(x=>x.Employee)
                .WithMany()
                .HasForeignKey(x=>x.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}