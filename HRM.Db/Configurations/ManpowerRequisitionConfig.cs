﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums ;

namespace HRM.Db.Configurations
{
    public class ManpowerRequisitionConfig : Configuration<ManpowerRequisition>
    {
        public ManpowerRequisitionConfig( ) : base(Table.ManpowerRequisition)
        {
            Property(p => p.TrackNo)
               .IsRequired()
               .HasMaxLength(20)
               .HasColumnAnnotation("Index", new IndexAnnotation(
                   new IndexAttribute("UK_TrackNo", 1) { IsUnique = true }));

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.PositionForDesignation)
                .IsRequired() 
                .HasMaxLength(50);

            Property(p => p.NumberOfPersons)
                .IsRequired() ;

            Property(p => p.Description)
               .HasMaxLength(250);

            Property(p => p.DateOfCreation)
                .IsRequired() ;

            Property(p => p.DedlineDate) ;

            Property(p => p.IsApproved) ;
 
            /*Foreign key*/
            //HasMany(x => x.ManpowerVacancyNotices)
            //    .WithMany(x => x.ManpowerRequisitions)
            //    .Map(x =>
            //         {
            //             x.ToTable("RequisitionVacancyNotice") ;
            //             x.MapLeftKey("ManpowerVacancyNotice_Id") ;
            //             x.MapRightKey("ManpowerRequisition_Id") ;
            //         }) ;


        }
    }
}
