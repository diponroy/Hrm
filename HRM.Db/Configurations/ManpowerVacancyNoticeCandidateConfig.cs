﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class ManpowerVacancyNoticeCandidateConfig : Configuration<ManpowerVacancyNoticeCandidate>
    {
        public ManpowerVacancyNoticeCandidateConfig() : base(Table.ManpowerVacancyNoticeCandidate)
        {
            Property(p => p.TrackNo)
                .IsRequired()
                .HasMaxLength(20)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("UK_TrackNo", 1) {IsUnique = true}));

            Property(p => p.ManpowerVacancyNoticeId)
                .IsRequired();
            HasRequired(x=>x.ManpowerVacancyNotice)
                .WithMany(l=>l.ManpowerVacancyNoticeCandidates)
                .HasForeignKey(x=>x.ManpowerVacancyNoticeId)
                .WillCascadeOnDelete(false);

            Property(p => p.CandidateId)
                .HasColumnName("Candidate_Id");
            HasRequired(x => x.Candidate)
                .WithMany()
                .HasForeignKey(l => l.CandidateId)
                .WillCascadeOnDelete(false);

            Property(p => p.EmployeeId)
                .HasColumnName("Employee_Id_As_Applicant");
            HasOptional(x=>x.Employee)
                .WithMany()
                .HasForeignKey(x=>x.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
