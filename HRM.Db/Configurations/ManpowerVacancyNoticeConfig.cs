﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class ManpowerVacancyNoticeConfig : Configuration<ManpowerVacancyNotice>
    {
        public ManpowerVacancyNoticeConfig() : base(Table.ManpowerVacancyNotice)
        {
            Property(p => p.TrackNo)
                .IsRequired()
                .HasMaxLength(20)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("UK_TrackNo", 1) {IsUnique = true}));

            Property(p => p.Type)
                .HasMaxLength(50);

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.DurationFromDate)
                .IsRequired()
                .HasColumnType("DATE");

            Property(p => p.DurationToDate)
                .HasColumnType("DATE");

            Property(p => p.Description)
                .HasMaxLength(250);

            Property(p => p.ManpowerRequisitionId)
                .IsRequired();
            HasRequired(x => x.ManpowerRequisition)
                .WithMany(l => l.ManpowerVacancyNotices)
                .HasForeignKey(x => x.ManpowerRequisitionId)
                .WillCascadeOnDelete(false);
        }
    }
}
