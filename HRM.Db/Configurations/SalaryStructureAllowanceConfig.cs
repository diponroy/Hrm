﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class SalaryStructureAllowanceConfig : Configuration<SalaryStructureAllowance>
    {
        public SalaryStructureAllowanceConfig()
            : base(Table.SalaryStructureAllowance)
        {
            Property(x => x.Remarks)
                .HasMaxLength(150);

            /*Foreign Keys*/
            /*SalaryStructure*/
            Property(x => x.SalaryStructureId)
                .HasColumnName("SalaryStructure_Id");
            HasRequired(x => x.SalaryStructure)
                .WithMany(l => l.SalaryStructureAllowances)
                .HasForeignKey(x => x.SalaryStructureId)
                .WillCascadeOnDelete(false);

            /*Allowance*/
            Property(x => x.AllowanceId)
                .HasColumnName("Allowance_Id");
            HasRequired(x => x.Allowance)
                .WithMany(l => l.SalaryStructureAllowances)
                .HasForeignKey(x => x.AllowanceId)
                .WillCascadeOnDelete(false);
        }
    }
}
