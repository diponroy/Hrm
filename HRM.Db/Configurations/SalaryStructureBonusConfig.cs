﻿using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class SalaryStructureBonusConfig : Configuration<SalaryStructureBonus>
    {
        public SalaryStructureBonusConfig() : base(Table.SalaryStructureBonus)
        {
            Property(x => x.Remarks)
                .HasMaxLength(150);

            /*Foreign Keys*/
            /*SalaryStructure*/
            Property(x => x.SalaryStructureId)
                .HasColumnName("SalaryStructure_Id");
            HasRequired(x => x.SalaryStructure)
                .WithMany(l => l.SalaryStructureBonuses)
                .HasForeignKey(x => x.SalaryStructureId)
                .WillCascadeOnDelete(false);

            /*Bonus*/
            Property(x => x.BonusId)
                .HasColumnName("Bonus_Id");
            HasRequired(x => x.Bonus)
                .WithMany(l => l.SalaryStructureBonuses)
                .HasForeignKey(x => x.BonusId)
                .WillCascadeOnDelete(false);
        }
    }
}
