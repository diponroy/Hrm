﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema ;
using System.Data.Entity.Infrastructure.Annotations ;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class SalaryStructureConfig : Configuration<SalaryStructure>
    {
        public SalaryStructureConfig() : base(Table.SalaryStructure)
        {
            Property(x => x.Title)
                .HasMaxLength(100)
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("Title_Index", 1)));

            Property(x => x.Basic)
                .IsRequired();

            Property(x => x.Remarks)
                .HasMaxLength(150);
        }
    }
}
