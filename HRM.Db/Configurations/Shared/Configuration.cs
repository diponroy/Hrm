﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Db.Configurations.Shared
{
    public class Configuration<TSource> : EntityTypeConfiguration<TSource> where TSource: class,  IEntityStatus
    {
        protected Configuration(Table table)
        {
            ToTable(table.ToString());

            Ignore(x => x.AffectedByEmployeeId);
            Ignore(x => x.AffectedDateTime);
            Ignore(x => x.StatusString);

            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(s => s.Status)
            .IsRequired();
        } 
    }
}
