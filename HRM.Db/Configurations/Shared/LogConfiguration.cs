﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Db.Configurations.Shared
{
    public class LogConfiguration<TLog, TLogFor> : EntityTypeConfiguration<TLog>
        where TLog : class, IEntityStatus, IEntityLog<TLogFor>
        where TLogFor : class, IEntityWithLog<TLog>
    {
        protected LogConfiguration(Table table)
        {
            ToTable(table.ToString(), "log");

            HasKey(x => x.LogId);
            Property(x => x.LogId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.AffectedDateTime)
                .IsRequired();

            Property(x => x.Status)
                .IsRequired();

            Property(x => x.LogStatuses)
                .IsRequired();

            /*foreign keys*/
            /*affected employee*/
            Property(x => x.AffectedByEmployeeId)
                .HasColumnName("EmployeeLog_Id_AsAffectedBy");
            HasRequired(x => x.AffectedByEmployeeLog)
                .WithMany()
                .HasForeignKey(x => x.AffectedByEmployeeId)
                .WillCascadeOnDelete(false);

            /*log for entity*/
            HasRequired(x => x.LogFor)
                .WithMany(l => l.Logs)
                .HasForeignKey(x => x.Id)
                .WillCascadeOnDelete(false);
        }
    }

}
