﻿using System.ComponentModel.DataAnnotations.Schema;
using HRM.Db.Configurations.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Configurations
{
    public class WorkStationUnitConfig : Configuration<WorkStationUnit>
    {
        public WorkStationUnitConfig() : base(Table.WorkStationUnit)
        {
            Ignore(x => x.IsClosed);

            HasKey(p => p.Id);
            Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.Remarks)
                .HasMaxLength(250);

            Property(p => p.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");

            Property(p => p.DateOfClosing)
                .HasColumnType("DATE");

            //Property(p => p.EmployeeIdAsInCharge)
            //    .IsRequired();

            /*foreign keys */
            /*WorkStationUnit*/
            Property(x => x.ParentId)
                .HasColumnName("WorkStationUnit_Id_AsParentId");
            HasOptional(p => p.ParentWorkStationUnit)
                .WithMany(p => p.ChildWorkStationUnits)
                .HasForeignKey(p => p.ParentId)
                .WillCascadeOnDelete(false);

            /*Company*/
            Property(x => x.CompanyId)
                .HasColumnName("Company_Id");
            HasOptional(r => r.Company)
                .WithMany(m => m.WorkStationUnits)
                .HasForeignKey(f => f.CompanyId)
                .WillCascadeOnDelete(false);

            /*Branch*/
            Property(x => x.BranchId)
                .HasColumnName("Branch_Id");
            HasOptional(r => r.Branch)
                .WithMany(m => m.WorkStationUnits)
                .HasForeignKey(f => f.BranchId)
                .WillCascadeOnDelete(false);

            /*Department*/
            Property(x => x.DepartmentId)
                .HasColumnName("Department_Id");
            HasOptional(r => r.Department)
                .WithMany(m => m.WorkStationUnits)
                .HasForeignKey(f => f.DepartmentId)
                .WillCascadeOnDelete(false);
        }
    }
}
