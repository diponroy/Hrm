﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Db.Configurations;
using HRM.Db.Contexts;
using HRM.Db.Seeds;
using HRM.Db.SeedSerializers;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.ContextInitializers
{
    internal class BuildContextInitializer : DropCreateDatabaseAlways<BuildContext>
    {
        private readonly DbSeedSerializer _dbSeedSerializer;

        public BuildContextInitializer ()
        {
            _dbSeedSerializer = new DbSeedSerializer();
        }

        protected override void Seed ( BuildContext context )
        {
            context.Set<EmployeeLog>().Add(new EmployeeLog()
            {
                TrackNo = "13331",
                Salutation = "Mrs.",
                FirstName = "Samial",
                LastName = "Mahmuda",
                FathersName = "Saiful Islam",
                MothersName = "Sabera Islam",
                DateOfBirth = DateTime.Parse("1983-08-11"),
                Gender = "Female",
                BloodGroup = "B+",
                Email = "samial@gmail.com",
                ContactNo = "01710565443",
                HomePhone = "01234221399",
                OfficePhone = "345678909876",
                PresentAddress = "Mirpur, Dhaka",
                PermanentAddress = "Mirpur, Dhaka",
                DivisionOrState = "Dhaka",
                City = "Dhaka",
                ImageDirectory = "BC/photo.jpg",
                Status = EntityStatusEnum.Active,

                AffectedByEmployeeId = 1,
                AffectedDateTime = DateTime.Now,
                LogStatuses = LogStatusEnum.Added,

                LogFor = new Employee
                {
                    TrackNo = "13331",
                    Salutation = "Mrs.",
                    FirstName = "Samial",
                    LastName = "Mahmuda",
                    FathersName = "Saiful Islam",
                    MothersName = "Sabera Islam",
                    DateOfBirth = DateTime.Parse("1983-08-11"),
                    Gender = "Female",
                    BloodGroup = "B+",
                    Email = "samial@gmail.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    OfficePhone = "345678909876",
                    PresentAddress = "Mirpur, Dhaka",
                    PermanentAddress = "Mirpur, Dhaka",
                    DivisionOrState = "Dhaka",
                    City = "Dhaka",
                    ImageDirectory = "BC/photo.jpg",
                    Status = EntityStatusEnum.Active,
                }
            });
            context.SaveChanges();


            _dbSeedSerializer.Serialize(context, new EmployeeSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeLoginSeed());

            _dbSeedSerializer.Serialize(context, new BranchSeed());
            _dbSeedSerializer.Serialize(context, new CompanySeed());
            _dbSeedSerializer.Serialize(context, new DepartmentSeed());
            _dbSeedSerializer.Serialize(context, new WorkStationUnitSeed());

            

            _dbSeedSerializer.Serialize(context, new AllowanceTypeSeed());
            _dbSeedSerializer.Serialize(context, new AllowanceSeed());
            _dbSeedSerializer.Serialize(context, new AppraisalIndicatorSeed()); 

            _dbSeedSerializer.Serialize(context, new BonusTypeSeed());
            _dbSeedSerializer.Serialize(context, new BonusSeed());
            _dbSeedSerializer.Serialize(context, new BonusPaymentDateSeed());
            _dbSeedSerializer.Serialize(context, new CandidateSeed());
            _dbSeedSerializer.Serialize(context, new CandidateEducationSeed());
            _dbSeedSerializer.Serialize(context, new CandidateEmploymentSeed());
            _dbSeedSerializer.Serialize(context, new CandidateLanguageSeed());
            _dbSeedSerializer.Serialize(context, new CandidateReferenceSeed());
            _dbSeedSerializer.Serialize(context, new CandidateSkillSeed());
            _dbSeedSerializer.Serialize(context, new CandidateTrainingSeed());
            _dbSeedSerializer.Serialize(context, new CandidateAchievementSeed());
            _dbSeedSerializer.Serialize(context, new CandidateLoginSeed());

            _dbSeedSerializer.Serialize(context, new EmployeeTypeSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeEducationSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeMembershipSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeTrainingSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeAttachedTrainingSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeTrainingAttachmentSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeTrainingBudgetSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeWorkStationUnitSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeReportingSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeAssignedAppraisalSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeAppraisalSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeAchievementSeed());

            _dbSeedSerializer.Serialize(context, new SalaryStructureSeed());
            _dbSeedSerializer.Serialize(context, new SalaryStructureAllowanceSeed());
            _dbSeedSerializer.Serialize(context, new SalaryStructureBonusSeed());

            _dbSeedSerializer.Serialize(context, new EmployeeSalaryStructureSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeSalaryStructureAllowanceSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeSalaryStructureBonusSeed());

            _dbSeedSerializer.Serialize(context, new EmployeePayrollInfoSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeSalaryPaymentSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeSalaryPaymentAllowanceSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeSalaryPaymentBonusSeed()); 
            
            _dbSeedSerializer.Serialize(context, new IncrementSalaryStructureSeed());
            _dbSeedSerializer.Serialize(context, new IncrementSalaryStructureAllowanceSeed());
            _dbSeedSerializer.Serialize(context, new IncrementSalaryStructureBonusSeed());

            _dbSeedSerializer.Serialize(context, new EmployeePromotionSeed());
            _dbSeedSerializer.Serialize(context, new EmployeeTransferSeed());
            

            /*Requisition and interivew*/

            _dbSeedSerializer.Serialize(context, new ManpowerRequisitionSeed());
            _dbSeedSerializer.Serialize(context, new ManpowerVacancyNoticeSeed());
            _dbSeedSerializer.Serialize(context, new ManpowerVacancyNoticeCandidateSeed());
            _dbSeedSerializer.Serialize(context, new ManpowerCandidateReferenceSeed());


            _dbSeedSerializer.Serialize(context, new InterviewSeed());
            _dbSeedSerializer.Serialize(context, new InterviewCandidateSeed());
            _dbSeedSerializer.Serialize(context, new InterviewCoordinatorSeed());
            _dbSeedSerializer.Serialize(context, new InterviewTestSeed());
            _dbSeedSerializer.Serialize(context, new InterviewTestCandidateSeed());


            _dbSeedSerializer.Serialize(context, new EmployeeDisciplineSeed());

            _dbSeedSerializer.Serialize(context, new EmployeeAttendanceSeed());


        }
    }
}