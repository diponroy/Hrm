﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Db.Contexts;
using HRM.Db.SeedSerializers;

namespace HRM.Db.ContextInitializers
{
    internal class HrmContextInitializer: CreateDatabaseIfNotExists<HrmContext>
    {
        private readonly DbSeedSerializer _dbSeedSerializer;

        public HrmContextInitializer()
        {
            _dbSeedSerializer = new DbSeedSerializer();
        }

        protected override void Seed(HrmContext context)
        {

        }
    }
}
