﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using HRM.Db.ContextInitializers;
using System.Data.Entity;

namespace HRM.Db.Contexts
{
    class BuildContext : Context
    {
        public BuildContext() : base(nameOrConnectionString: "DbHRM")
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DbHRM"].ConnectionString;
        }

        static BuildContext()
        {
            Database.SetInitializer(new BuildContextInitializer());
        }
    }
}
