﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using HRM.Db.Configurations;
using HRM.Db.Configurations.Logs;

namespace HRM.Db.Contexts
{
    public class Context : DbContext
    {
        public DbSet<TType> GetTable<TType>() where TType : class
        {
            return Set<TType>();
        }

        protected Context(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new AppraisalIndicatorConfig());
            modelBuilder.Configurations.Add(new AppraisalIndicatorLogConfig());

            modelBuilder.Configurations.Add(new BranchConfig());
            modelBuilder.Configurations.Add(new BranchLogConfig());

            modelBuilder.Configurations.Add(new CandidateConfig());
            modelBuilder.Configurations.Add(new CandidateLogConfig());
            modelBuilder.Configurations.Add(new CandidateEducationConfig());
            modelBuilder.Configurations.Add(new CandidateEducationLogConfig());
            modelBuilder.Configurations.Add(new CandidateEmploymentConfig());
            modelBuilder.Configurations.Add(new CandidateEmploymentLogConfig());
            modelBuilder.Configurations.Add(new CandidateLanguageConfig());
            modelBuilder.Configurations.Add(new CandidateLanguageLogConfig());
            modelBuilder.Configurations.Add(new CandidateReferenceConfig());
            modelBuilder.Configurations.Add(new CandidateReferenceLogConfig());
            modelBuilder.Configurations.Add(new CandidateSkillConfig());
            modelBuilder.Configurations.Add(new CandidateSkillLogConfig());
            modelBuilder.Configurations.Add(new CandidateTrainingConfig());
            modelBuilder.Configurations.Add(new CandidateTrainingLogConfig());
            modelBuilder.Configurations.Add(new CandidateAchievementConfig());
            modelBuilder.Configurations.Add(new CandidateAchievementLogConfig());
            modelBuilder.Configurations.Add(new CandidateLoginConfig());
            modelBuilder.Configurations.Add(new CandidateLoginLogConfig());

            modelBuilder.Configurations.Add(new DepartmentConfig());
            modelBuilder.Configurations.Add(new DepartmentLogConfig());

            modelBuilder.Configurations.Add(new CompanyConfig());
            modelBuilder.Configurations.Add(new CompanyLogConfig());
 

            modelBuilder.Configurations.Add(new EmployeeConfig());
            modelBuilder.Configurations.Add(new EmployeeLogConfig());
            modelBuilder.Configurations.Add(new EmployeeLoginConfig());
            modelBuilder.Configurations.Add(new EmployeeLoginLogConfig()); 
            modelBuilder.Configurations.Add(new EmployeeTypeConfig());
            modelBuilder.Configurations.Add(new EmployeeTypeLogConfig()); 
            modelBuilder.Configurations.Add(new EmployeeEducationConfig());
            modelBuilder.Configurations.Add(new EmployeeEducationLogConfig()); 
            modelBuilder.Configurations.Add(new EmployeeAchievementConfig());
            modelBuilder.Configurations.Add(new EmployeeAchievementLogConfig()); 
            modelBuilder.Configurations.Add(new EmployeeMembershipConfig());
            modelBuilder.Configurations.Add(new EmployeeMembershipLogConfig()); 


            modelBuilder.Configurations.Add(new EmployeeTrainingConfig());
            modelBuilder.Configurations.Add(new EmployeeTrainingLogConfig());

            modelBuilder.Configurations.Add(new EmployeeAttachedTrainingConfig());
            modelBuilder.Configurations.Add(new EmployeeAttachedTrainingLogConfig());

            modelBuilder.Configurations.Add(new EmployeeTrainingAttachmentConfig());
            modelBuilder.Configurations.Add(new EmployeeTrainingAttachmentLogConfig());

            modelBuilder.Configurations.Add(new EmployeeTrainingBudgetConfig());
            modelBuilder.Configurations.Add(new EmployeeTrainingBudgetLogConfig());

            modelBuilder.Configurations.Add(new EmployeeWorkStationUnitConfig());
            modelBuilder.Configurations.Add(new EmployeeWorkStationUnitLogConfig());

            modelBuilder.Configurations.Add(new EmployeeReportingConfig());
            modelBuilder.Configurations.Add(new EmployeeReportingLogConfig());

            modelBuilder.Configurations.Add(new EmployeeAssignedAppraisalConfig());
            modelBuilder.Configurations.Add(new EmployeeAssignedAppraisalLogConfig());

            modelBuilder.Configurations.Add(new EmployeeAppraisalConfig());
            modelBuilder.Configurations.Add(new EmployeeAppraisalLogConfig());

            modelBuilder.Configurations.Add(new WorkStationUnitConfig());
            modelBuilder.Configurations.Add(new WorkStationUnitLogConfig());


           /*salary and payments*/
            modelBuilder.Configurations.Add(new AllowanceTypeConfig());
            modelBuilder.Configurations.Add(new AllowanceTypeLogConfig());

            modelBuilder.Configurations.Add(new AllowanceConfig());
            modelBuilder.Configurations.Add(new AllowanceLogConfig());

            modelBuilder.Configurations.Add(new BonusTypeConfig());
            modelBuilder.Configurations.Add(new BonusTypeLogConfig());

            modelBuilder.Configurations.Add(new BonusConfig());
            modelBuilder.Configurations.Add(new BonusLogConfig());

            modelBuilder.Configurations.Add(new BonusPaymentDateConfig());
            modelBuilder.Configurations.Add(new BonusPaymentDateLogConfig());

            modelBuilder.Configurations.Add(new SalaryStructureConfig());
            modelBuilder.Configurations.Add(new SalaryStructureLogConfig());

            modelBuilder.Configurations.Add(new SalaryStructureAllowanceConfig());
            modelBuilder.Configurations.Add(new SalaryStructureAllowanceLogConfig());

            modelBuilder.Configurations.Add(new SalaryStructureBonusConfig());
            modelBuilder.Configurations.Add(new SalaryStructureBonusLogConfig());

            modelBuilder.Configurations.Add(new EmployeeSalaryStructureConfig());
            modelBuilder.Configurations.Add(new EmployeeSalaryStructureLogConfig());

            modelBuilder.Configurations.Add(new EmployeeSalaryStructureAllowanceConfig());
            modelBuilder.Configurations.Add(new EmployeeSalaryStructureAllowanceLogConfig());

            modelBuilder.Configurations.Add(new EmployeeSalaryStructureBonusConfig());
            modelBuilder.Configurations.Add(new EmployeeSalaryStructureBonusLogConfig());

            modelBuilder.Configurations.Add(new EmployeePayrollInfoConfig());
            modelBuilder.Configurations.Add(new EmployeePayrollInfoLogConfig());

            modelBuilder.Configurations.Add(new EmployeeSalaryPaymentConfig());
            modelBuilder.Configurations.Add(new EmployeeSalaryPaymentLogConfig());

            modelBuilder.Configurations.Add(new EmployeeSalaryPaymentBonusConfig());
            modelBuilder.Configurations.Add(new EmployeeSalaryPaymentBonusLogConfig());

            modelBuilder.Configurations.Add(new EmployeeSalaryPaymentAllowanceConfig());
            modelBuilder.Configurations.Add(new EmployeeSalaryPaymentAllowanceLogConfig());

            modelBuilder.Configurations.Add(new IncrementSalaryStructureConfig());
            modelBuilder.Configurations.Add(new IncrementSalaryStructureLogConfig());

            modelBuilder.Configurations.Add(new IncrementSalaryStructureAllowanceConfig());
            modelBuilder.Configurations.Add(new IncrementSalaryStructureAllowanceLogConfig());

            modelBuilder.Configurations.Add(new IncrementSalaryStructureBonusConfig());
            modelBuilder.Configurations.Add(new IncrementSalaryStructureBonusLogConfig());  

            /*Manpower Rquisition*/
            modelBuilder.Configurations.Add(new ManpowerRequisitionConfig());
            modelBuilder.Configurations.Add(new ManpowerRequisitionLogConfig());

            modelBuilder.Configurations.Add(new ManpowerVacancyNoticeConfig());
            modelBuilder.Configurations.Add(new ManpowerVacancyNoticeLogConfig());

            modelBuilder.Configurations.Add(new ManpowerVacancyNoticeCandidateConfig());
            modelBuilder.Configurations.Add(new ManpowerVacancyNoticeCandidateLogConfig());

            modelBuilder.Configurations.Add(new ManpowerCandidateReferenceConfig());
            modelBuilder.Configurations.Add(new ManpowerCandidateReferenceLogConfig());


            /*Interivew*/
            modelBuilder.Configurations.Add(new InterviewConfig());
            modelBuilder.Configurations.Add(new InterviewLogConfig());

            modelBuilder.Configurations.Add(new InterviewCoordinatorConfig());
            modelBuilder.Configurations.Add(new InterviewCoordinatorLogConfig());

            modelBuilder.Configurations.Add(new InterviewCandidateConfig());
            modelBuilder.Configurations.Add(new InterviewCandidateLogConfig());

            modelBuilder.Configurations.Add(new InterviewTestConfig());
            modelBuilder.Configurations.Add(new InterviewTestLogConfig());

            modelBuilder.Configurations.Add(new InterviewTestCandidateConfig());
            modelBuilder.Configurations.Add(new InterviewTestCandidateLogConfig());


            modelBuilder.Configurations.Add(new EmployeeTransferConfig());
            modelBuilder.Configurations.Add(new EmployeeTransferLogConfig());

            modelBuilder.Configurations.Add(new EmployeePromotionConfig());
            modelBuilder.Configurations.Add(new EmployeePromotionLogConfig());


            modelBuilder.Configurations.Add(new EmployeeDisciplineConfig());
            modelBuilder.Configurations.Add(new EmployeeDisciplineLogConfig());

            modelBuilder.Configurations.Add(new EmployeeAttendanceConfig());
            modelBuilder.Configurations.Add(new EmployeeAttendanceLogConfig());

        }
    }
}
