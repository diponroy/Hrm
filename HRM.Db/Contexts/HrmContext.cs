﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using HRM.Db.ContextInitializers;
using HRM.Model.Table;

namespace HRM.Db.Contexts
{
    public class HrmContext : Context
    {
        public HrmContext() : base(nameOrConnectionString: "DbHRM")
        {

        }

        static HrmContext()
        {
            Database.SetInitializer(new HrmContextInitializer());
        }
    }
}
