﻿using System.Linq;
using HRM.Db.Contexts;
using HRM.Db.Seeds;
using HRM.Model;
using HRM.Model.Table;

namespace HRM.Db
{
    public class DbInitializer
    {
        public void DropAndCreate()
        {
            using (var db = new BuildContext())
            {
                var appraisalIndicators = db.Set<AppraisalIndicator>().ToList();

                var candidates = db.Set<Candidate>().ToList();
                var candidateEducations = db.Set<CandidateEducation>().ToList();
                var candidateEmployments = db.Set<CandidateEmployment>().ToList();
                var candidateLanguages = db.Set<CandidateLanguage>().ToList();
                var candidateReferences = db.Set<CandidateReference>().ToList();
                var candidateSkills = db.Set<CandidateSkill>().ToList();
                var candidateTrainings = db.Set<CandidateTraining>().ToList();
                var candidateAchievements = db.Set<CandidateAchievement>().ToList();
                var candidatelogins = db.Set<CandidateLogin>().ToList();

                var employees = db.Set<Employee>().ToList();
                var employeeLogin = db.Set<EmployeeLogin>().ToList();
                var employeeTypes = db.Set<EmployeeType>().ToList();
                var employeeEducations = db.Set<EmployeeEducation>().ToList();
                var employeeMemberships = db.Set<EmployeeMembership>().ToList();
                var employeeTrainings = db.Set<EmployeeTraining>().ToList();
                var employeeAttachedTrainings = db.Set<EmployeeAttachedTraining>().ToList();
                var employeeTrainingAttachments = db.Set<EmployeeTrainingAttachment>().ToList();
                var employeeTrainingBudgets = db.Set<EmployeeTrainingBudget>().ToList();
                var employeeWorkStationUnits = db.Set<EmployeeWorkStationUnit>().ToList();
                var employeeReportings = db.Set<EmployeeReporting>().ToList();
                var employeeAssignedAppraisals = db.Set<EmployeeAssignedAppraisal>().ToList();
                var employeeAchievements = db.Set<EmployeeAchievement>().ToList();
                var employeeAppraisals = db.Set<EmployeeAppraisal>().ToList();
                var employeePromotions = db.Set<EmployeePromotion>().ToList();
                var employeeTransfers = db.Set<EmployeeTransfer>().ToList();
               

                var workStationUnits = db.Set<WorkStationUnit>().ToList();
                var branches = db.Set<Branch>().ToList();
                var companies = db.Set<Company>().ToList();
                var departments = db.Set<Department>().ToList();
                var allowanceTypes = db.Set<AllowanceType>().ToList();
                var allowances = db.Set<Allowance>().ToList();
                var bonusTypes = db.Set<BonusType>().ToList();
                var bonuses = db.Set<Bonus>().ToList();
                var bonusEstimatedDates = db.Set<BonusPaymentDate>().ToList();

                /*salary and payments*/
                var salaryStructures = db.Set<SalaryStructure>().ToList();
                var salaryStructureAllowances = db.Set<SalaryStructureAllowance>().ToList();
                var salaryStructureBonus = db.Set<SalaryStructureBonus>().ToList();
                var employeeSalaryStructureBonus = db.Set<EmployeeSalaryStructure>().ToList();
                var employeeSalaryStructureAllowances = db.Set<EmployeeSalaryStructureAllowance>().ToList();
                var employeeSalaryStructureBonuses = db.Set<EmployeeSalaryStructureBonus>().ToList();

                var employeePayrollInfos = db.Set<EmployeePayrollInfo>().ToList();
                var employeeSalaryPayments = db.Set<EmployeeSalaryPayment>().ToList();
                var employeeSalaryPaymentAllowances = db.Set<EmployeeSalaryPaymentAllowance>().ToList();
                var employeeSalaryPaymentBonuses = db.Set<EmployeeSalaryPaymentBonus>().ToList();

                var incrementSalaryStructures = db.Set<IncrementSalaryStructure>().ToList();
                var incrementSalaryStructureAllowances = db.Set<IncrementSalaryStructureAllowance>().ToList();
                var incrementSalaryStructureBonuses = db.Set<IncrementSalaryStructureBonus>().ToList();

                /*Manpower Rquisition*/
                var manpowerRequisitions = db.Set<ManpowerRequisition>().ToList();
                var manpowerVacancyNotices = db.Set<ManpowerVacancyNotice>().ToList();

                var manpowerVacancyNoticeCandidates = db.Set<ManpowerVacancyNoticeCandidate>().ToList();
                var manpowerCandidateReferences = db.Set<ManpowerCandidateReference>().ToList();

                /*Interivew*/
                var interviews = db.Set<Interview>().ToList();
                var interviewCoordinators = db.Set<InterviewCoordinator>().ToList();
                var interviewCandidates = db.Set<InterviewCandidate>().ToList();
                var interviewTests = db.Set<InterviewTest>().ToList();

                var interviewTestCandidates = db.Set<InterviewTestCandidate>().ToList();

                var employeeAttendances = db.Set<EmployeeAttendance>().ToList();       
            }
        }
    }
}
