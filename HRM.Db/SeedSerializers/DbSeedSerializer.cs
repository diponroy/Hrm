﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Db.SeedSerializers
{
    internal class DbSeedSerializer
    {
        public void Serialize<TEntity>(DbContext context, EntitySeedSerializer<TEntity> entitySeedSerializer) where TEntity : class 
        {
            entitySeedSerializer.SerializeInto(context);
        }

        public void Serialize<TEntity, TLog, TMapper>(DbContext context, EntityWithLogSeedSerializer<TEntity, TLog, TMapper> entityWithLogSeedSerializer)
            where TEntity : class, IEntityStatus, IEntityWithLog<TLog>
            where TLog : class, IEntityLog<TEntity>, new ()
            where TMapper : LogMapper<TEntity, TLog>, new ()
        {
            entityWithLogSeedSerializer.SerializeInto(context);
        }
    }
}
