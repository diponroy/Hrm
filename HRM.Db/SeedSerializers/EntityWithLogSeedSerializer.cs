﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Db.SeedSerializers
{
    abstract class EntityWithLogSeedSerializer<TEntity, TLog, TMapper> : EntitySeedSerializer<TEntity>
        where TEntity : class, IAffectedByTrack, IEntityWithLog<TLog>
        where TLog : class, IEntityLog<TEntity>, new()
        where TMapper : LogMapper<TEntity, TLog>, new()
    {
        private LogMapper<TEntity, TLog> _mapper;
        public LogMapper<TEntity, TLog> Mapper
        {
            get
            {
                _mapper = _mapper ?? new TMapper();
                return _mapper;
            }
        }

        public virtual TLog MappedLog(DbContext context, TEntity entity)
        {
            return Mapper.FullMapped(context, entity);
        }

        public override void SerializeInto(DbContext context)
        {
            foreach (TEntity aEntity in Entities)
            {
                try
                {
                    aEntity.AffectedByEmployeeId = aEntity.AffectedByEmployeeId ?? 1;
                    aEntity.AffectedDateTime = aEntity.AffectedDateTime ?? DateTime.Now;

                    var log = MappedLog(context, aEntity);
                    log.LogStatuses = LogStatusEnum.Added;
                    aEntity.Logs = new List<TLog> { log };

                    context.Set<TEntity>().Add(aEntity);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string msg = String.Format("Error to serialize {0} seeds.", typeof(TEntity).Name);
                    throw new Exception(msg, ex);
                }
            }
        }
    }
}
