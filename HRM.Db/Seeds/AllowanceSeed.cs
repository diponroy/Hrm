﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class AllowanceSeed : EntityWithLogSeedSerializer<Allowance,AllowanceLog,AllowanceMapper>
    {
        public AllowanceSeed()
        {
            List<Allowance> list = new List<Allowance>()
            {
                new Allowance(){ AllowanceTypeId = 4, Title = "Medical", AsPercentage = true, Weight = 15, Status = EntityStatusEnum.Active},
                new Allowance(){ AllowanceTypeId = 1, Title = "Transport", AsPercentage = false, Weight = 3000, Status = EntityStatusEnum.Active},
                new Allowance(){ AllowanceTypeId = 4, Title = "House", AsPercentage = true, Weight = 30, Status = EntityStatusEnum.Active},
                new Allowance(){ AllowanceTypeId = 2, Title = "Telephone", AsPercentage = false, Weight = 1000, Status = EntityStatusEnum.Active},
                new Allowance(){ AllowanceTypeId = 3, Title = "Car Loan", AsPercentage = true, Weight = 15, Status = EntityStatusEnum.Active}
            };

            Seed(list);
        }
    }
}
