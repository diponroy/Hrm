﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log ;

namespace HRM.Db.Seeds
{
    class AllowanceTypeSeed : EntityWithLogSeedSerializer<AllowanceType, AllowanceTypeLog, AllowanceTypeMapper>
    {
        public AllowanceTypeSeed()
        {
            List<AllowanceType> list = new List<AllowanceType>()
            {
                new AllowanceType(){Name = "Weekly", Status = EntityStatusEnum.Active},
                new AllowanceType(){Name = "Monthly", Status = EntityStatusEnum.Active},
                new AllowanceType(){Name = "Half-Yearly", Status = EntityStatusEnum.Active},
                new AllowanceType(){Name = "Annual", Status = EntityStatusEnum.Active},
            };

            Seed(list);
        }
    }
}
