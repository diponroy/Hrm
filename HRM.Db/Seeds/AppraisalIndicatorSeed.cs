﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Hosting;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class AppraisalIndicatorSeed : EntityWithLogSeedSerializer<AppraisalIndicator, AppraisalIndicatorLog, AppraisalIndicatorMapper>
    {
        public AppraisalIndicatorSeed ()
        {
            var appraisalIndicators = new List<AppraisalIndicator>()
            {
                new AppraisalIndicator{ Title = "Performance", MaxWeight = 15, Remarks="Performance", Status = EntityStatusEnum.Active},
                new AppraisalIndicator{ Title = "Communication Skill", ParentId = 1, MaxWeight = 30, Remarks="Communication", Status = EntityStatusEnum.Active},
                new AppraisalIndicator{ Title = "Working Skill", ParentId = 2, MaxWeight = 30, Remarks="Working", Status = EntityStatusEnum.Active},
                new AppraisalIndicator{ Title = "Behaviour", ParentId = 2, MaxWeight = 10, Remarks="Behaviour", Status = EntityStatusEnum.Active},
                new AppraisalIndicator{ Title = "Character", ParentId = 3,  MaxWeight = 15, Remarks="Character", Status = EntityStatusEnum.Active}
            }; 
            Seed(appraisalIndicators);
        }
    }
}
