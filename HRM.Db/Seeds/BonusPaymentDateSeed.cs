﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class BonusPaymentDateSeed : EntityWithLogSeedSerializer<BonusPaymentDate,BonusPaymentDateLog,BonusPaymentDateMapper>
    {
        public BonusPaymentDateSeed()
        {
            List<BonusPaymentDate> bonusPaymentDates = new List<BonusPaymentDate>()
            {
                new BonusPaymentDate(){ BonusId = 1, EstimatedDate = DateTime.ParseExact("01-07-2014", "dd-MM-yyyy", null), Status = EntityStatusEnum.Active},
                new BonusPaymentDate(){ BonusId = 2, EstimatedDate = DateTime.ParseExact("01-09-2014", "dd-MM-yyyy", null), Status = EntityStatusEnum.Active},
                new BonusPaymentDate(){ BonusId = 3, EstimatedDate = DateTime.ParseExact("02-09-2014", "dd-MM-yyyy", null), Status = EntityStatusEnum.Active},
                new BonusPaymentDate(){ BonusId = 4, EstimatedDate = DateTime.ParseExact("01-09-2012", "dd-MM-yyyy", null), Status = EntityStatusEnum.Active},
                new BonusPaymentDate(){ BonusId = 5, EstimatedDate = DateTime.ParseExact("01-07-2012", "dd-MM-yyyy", null), Status = EntityStatusEnum.Active}
            };
            Seed(bonusPaymentDates);
        }
    }
}
