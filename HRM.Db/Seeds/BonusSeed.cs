﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class BonusSeed : EntityWithLogSeedSerializer<Bonus,BonusLog,BonusMapper>
    {
        public BonusSeed()
        {
            List<Bonus> bonuses = new List<Bonus>()
            {
                new Bonus(){ BonusTypeId = 1, Title = "title1", AsPercentage = true, Weight = 200, Status = EntityStatusEnum.Active},
                new Bonus(){ BonusTypeId = 2, Title = "title2", AsPercentage = true, Weight = 100, Status = EntityStatusEnum.Active}, 
                new Bonus(){ BonusTypeId = 3, Title = "title3", AsPercentage = true, Weight = 3000, Status = EntityStatusEnum.Active},
                new Bonus(){ BonusTypeId = 4, Title = "title4", AsPercentage = true, Weight = 6000, Status = EntityStatusEnum.Active},
                new Bonus(){ BonusTypeId = 5, Title = "title5", AsPercentage = true, Weight = 7000, Status = EntityStatusEnum.Active}
            };

            Seed(bonuses);
        }
    }
}
