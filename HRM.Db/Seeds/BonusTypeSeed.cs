﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log ;

namespace HRM.Db.Seeds
{
    class BonusTypeSeed : EntityWithLogSeedSerializer<BonusType, BonusTypeLog, BonusTypeMapper>
    {
        public BonusTypeSeed()
        {
            List<BonusType> list = new List<BonusType>()
            {
                new BonusType() {Name = "Weekly", Status = EntityStatusEnum.Active},
                new BonusType() {Name = "Annual", Status = EntityStatusEnum.Active},
                new BonusType() {Name = "Monthly", Status = EntityStatusEnum.Active},
                new BonusType() {Name = "Project Basedd", Status = EntityStatusEnum.Active},
                new BonusType() {Name = "Festival", Status = EntityStatusEnum.Active},
                new BonusType() {Name = "AdHoc", Status = EntityStatusEnum.Active},
                new BonusType() {Name = "Eid", Status = EntityStatusEnum.Active},
                new BonusType() {Name = "Pooja", Status = EntityStatusEnum.Active},
            };

            Seed(list);
        }
    }
}