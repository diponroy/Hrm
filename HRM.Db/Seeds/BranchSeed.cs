﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class BranchSeed : EntityWithLogSeedSerializer<Branch, BranchLog, BranchMapper>
    {
         public BranchSeed ()
         {
             var branches = new List<Branch>
                            {
                                new Branch
                                {
                                    Name = "Bashabo Branch",
                                    Address = "7 no north Bashabo, Dhaka-1412.",
                                    Remarks = "SME Branch",
                                    DateOfCreation = DateTime.ParseExact("2014-09-09", "yyyy-MM-dd", null),
                                    Status = EntityStatusEnum.Active,
                                 },
                                 new Branch
                                 {
                                    Name = "Gulshan Branch",
                                    Address = "12/B Gulshan Avenue, Dhaka-1412.",
                                    Remarks = "Head Office",
                                    DateOfCreation = DateTime.ParseExact("2014-09-09", "yyyy-MM-dd", null),
                                    Status = EntityStatusEnum.Active
                                 },
                                 new Branch
                                 {
                                    Name = "Karwan Bazar Branch",
                                    Address = "15 Kazi Nazrul Islam Avenue, Dhaka-1412.",
                                    Remarks = "Corporate Show Room",
                                    DateOfCreation = DateTime.ParseExact("2014-09-09", "yyyy-MM-dd", null),
                                    Status = EntityStatusEnum.Active
                                 },
                                 new Branch
                                 {
                                    Name = "Bonani Branch",
                                    Address = "13/C Bonanai Lake, Dhaka-1412.",
                                    Remarks = "Customer Care Center",
                                    DateOfCreation = DateTime.ParseExact("2000-09-09", "yyyy-MM-dd", null),
                                    DateOfClosing = DateTime.ParseExact("2014-09-09", "yyyy-MM-dd", null),
                                    Status = EntityStatusEnum.Inactive
                                 },
                                 new Branch
                                 {
                                    Name = "Lalmatia Branch",
                                    Address = "13/C Lalmatia, Dhaka-1122.",
                                    Remarks = "Customer Care Center",
                                    DateOfCreation = DateTime.ParseExact("2000-09-09", "yyyy-MM-dd", null),
                                    DateOfClosing = DateTime.ParseExact("2014-09-09", "yyyy-MM-dd", null),
                                    Status = EntityStatusEnum.Inactive
                                 }
                            } ;

             Seed(branches) ;
          }
    }
}
