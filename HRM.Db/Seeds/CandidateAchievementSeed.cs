﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class CandidateAchievementSeed : EntityWithLogSeedSerializer<CandidateAchievement, CandidateAchievementLog, CandidateAchievementMapper>
    {
        public CandidateAchievementSeed()
        {
            var candidateAchievements = new List<CandidateAchievement>
            {
                new CandidateAchievement
                {
                    Title = "Best Employee Award",
                    Description = "Got awarded as best employee in the year of 2013",
                    AttachmentDirectory = "C:\\Direc\\award.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateEmploymentId = 1
                },
                new CandidateAchievement
                {
                    Title = "Best Employee of the month",
                    Description = "Got awarded as best employee in the year of 2012",
                    AttachmentDirectory = "C:\\Direc\\award.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateEmploymentId = 2
                },
                new CandidateAchievement
                {
                    Title = "Best Developer of the season",
                    Description = "Got awarded as best employee in the year of 2014",
                    AttachmentDirectory = "C:\\Direc\\award.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateEmploymentId = 3
                },
                new CandidateAchievement
                {
                    Title = "Best Developer of the year",
                    Description = "Got awarded as best employee in the year of 2013",
                    AttachmentDirectory = "C:\\Direc\\award.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateEmploymentId = 4
                },
                new CandidateAchievement
                {
                    Title = "Best Developer of the year",
                    Description = "Got awarded as best employee in the year of 2013",
                    AttachmentDirectory = "C:\\Direc\\award.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateEmploymentId = 5
                }
            };

            Seed(candidateAchievements);
        }
    }
}
