﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class CandidateEducationSeed : EntityWithLogSeedSerializer<CandidateEducation, CandidateEducationLog, CandidateEducationMapper>
    {
        public CandidateEducationSeed()
        {
            var candidateEducations = new List<CandidateEducation>
            {
                new CandidateEducation
                {
                    Title = "M.Sc. in C.S.E.",
                    Institution = "Harvard University",
                    DurationFromDate = DateTime.Parse("2011-12-12"),
                    DurationToDate = DateTime.Now,
                    Result = "3.95",
                    Major = "Distributed Database",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateEducation
                {
                    Title = "B.Sc. in C.S.E.",
                    Institution = "Yale University",
                    DurationFromDate = DateTime.Parse("2008-12-12"),
                    DurationToDate = DateTime.Parse("2014-04-04"),
                    Result = "3.85",
                    Major = "Computer Science",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 2
                },
                new CandidateEducation
                {
                    Title = "PhD in C.S.E.",
                    Institution = "Cornell University",
                    DurationFromDate = DateTime.Parse("2012-12-12"),
                    DurationToDate = DateTime.Parse("2014-05-05"),
                    Result = "3.85",
                    Major = "Data Mining",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 3
                },
                new CandidateEducation
                {
                    Title = "Diploma in C.S.E.",
                    Institution = "Bangladesh Polytechnic Institute",
                    DurationFromDate = DateTime.Parse("2012-12-12"),
                    DurationToDate = DateTime.Parse("2014-05-05"),
                    Result = "3.55",
                    Major = "C.S.E.",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 4
                },
                new CandidateEducation
                {
                    Title = "Diploma in CSIT",
                    Institution = "Bhuiyan Institute of IT",
                    DurationFromDate = DateTime.Parse("2012-12-12"),
                    DurationToDate = DateTime.Parse("2014-05-05"),
                    Result = "3.85",
                    Major = "C.S.E.",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 5
                }
            };

            Seed(candidateEducations);
        }
    }
}
