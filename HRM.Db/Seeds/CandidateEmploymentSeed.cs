﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class CandidateEmploymentSeed : EntityWithLogSeedSerializer<CandidateEmployment, CandidateEmploymentLog, CandidateEmploymentMapper>
    {
        public CandidateEmploymentSeed()
        {
            var candidateEmployments = new List<CandidateEmployment>
            {
                new CandidateEmployment
                {
                    Institution = "Oracle Corporation",
                    Address = "221/B, South Block, Silicon Valley, California",
                    Designation = "Senior Software Architect",
                    Responsibility = "Designing whole architecture at the very beginning",
                    DurationFromDate = DateTime.Parse("2011-12-12"),
                    DurationToDate = DateTime.Parse("2014-07-07"),                    
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateEmployment
                {
                    Institution = "VMWare Corporation",
                    Address = "34/C, Silicon Valley, California",
                    Designation = "Software Developer",
                    Responsibility = "Designing the framework",
                    DurationFromDate = DateTime.Parse("2010-12-12"),
                    DurationToDate = DateTime.Now,                    
                    Status = EntityStatusEnum.Active,
                    CandidateId = 2
                },
                new CandidateEmployment
                {
                    Institution = "KAZ Software Ltd.",
                    Address = "Dhanmondi, Bangladesh",
                    Designation = "Senior Software Developer",
                    Responsibility = "MVC developer",
                    DurationFromDate = DateTime.Parse("2007-12-12"),
                    DurationToDate = DateTime.Now,                    
                    Status = EntityStatusEnum.Active,
                    CandidateId = 3
                },
                new CandidateEmployment
                {
                    Institution = "Tiger IT",
                    Address = "Dhanmondi, Bangladesh",
                    Designation = "Senior Software Developer",
                    Responsibility = "MVC developer",
                    DurationFromDate = DateTime.Parse("2007-12-12"),
                    DurationToDate = DateTime.Parse("2009-12-12"),            
                    Status = EntityStatusEnum.Active,
                    CandidateId = 4
                },
                new CandidateEmployment
                {
                    Institution = "Leads Corporation",
                    Address = "Dhanmondi, Bangladesh",
                    Designation = "Software Developer",
                    Responsibility = "SQL developer",
                    DurationFromDate = DateTime.Parse("2010-12-12"),
                    DurationToDate = DateTime.Now,                    
                    Status = EntityStatusEnum.Active,
                    CandidateId = 5
                }
            };

            Seed(candidateEmployments);
        }
    }
}
