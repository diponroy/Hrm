﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class CandidateLanguageSeed : EntityWithLogSeedSerializer<CandidateLanguage, CandidateLanguageLog, CandidateLanguageMapper>
    {
        public CandidateLanguageSeed()
        {
            var candidateLanguages = new List<CandidateLanguage>
            {
                new CandidateLanguage
                {
                    LanguageName = "Bengali",
                    Reading = "Excellent",
                    Writing = "Excellent",
                    Speaking = "Excellent",
                    Listening = "Excellent",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateLanguage
                {
                    LanguageName = "English",
                    Reading = "Excellent",
                    Writing = "Good",
                    Speaking = "Good",
                    Listening = "Satisfactory",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateLanguage
                {
                    LanguageName = "Bengali",
                    Reading = "Excellent",
                    Writing = "Excellent",
                    Speaking = "Excellent",
                    Listening = "Excellent",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 2
                },
                new CandidateLanguage
                {
                    LanguageName = "English",
                    Reading = "Good",
                    Writing = "Good",
                    Speaking = "Good",
                    Listening = "Satisfactory",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 2
                },
                new CandidateLanguage
                {
                    LanguageName = "English",
                    Reading = "Satisfactory",
                    Writing = "Good",
                    Speaking = "Satisfactory",
                    Listening = "Satisfactory",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 3
                },
                new CandidateLanguage
                {
                    LanguageName = "French",
                    Reading = "Satisfactory",
                    Writing = "Good",
                    Speaking = "Good",
                    Listening = "Satisfactory",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 4
                },
                new CandidateLanguage
                {
                    LanguageName = "Russian",
                    Reading = "Good",
                    Writing = "Good",
                    Speaking = "Satisfactory",
                    Listening = "Satisfactory",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 5
                }
            };

            Seed(candidateLanguages);
        }
    }
}
