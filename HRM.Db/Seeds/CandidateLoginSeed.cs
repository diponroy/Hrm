﻿using System.Collections.Generic;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Db.Seeds
{
    internal class CandidateLoginSeed :
        EntityWithLogSeedSerializer<CandidateLogin, CandidateLoginLog, CandidateLoginMapper>
    {
        public CandidateLoginSeed()
        {
            var candidateLogins = new List<CandidateLogin>
            {
                new CandidateLogin
                {
                    LoginName = "Candidate",
                    Password = PasswordUtility.Encode("123"),
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateLogin
                {
                    LoginName = "tom@gmail.com",
                    Password = PasswordUtility.Encode("123"),
                    Status = EntityStatusEnum.Active,
                    CandidateId = 2
                },
                new CandidateLogin
                {
                    LoginName = "kamal@gmail.com",
                    Password = PasswordUtility.Encode("123"),
                    Status = EntityStatusEnum.Active,
                    CandidateId = 3
                },
                new CandidateLogin
                {
                    LoginName = "orpita@hotmail.com",
                    Password = PasswordUtility.Encode("123"),
                    Status = EntityStatusEnum.Active,
                    CandidateId = 4
                },
            };

            Seed(candidateLogins);
        }
    }
}
