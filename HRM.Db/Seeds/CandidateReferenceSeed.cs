﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class CandidateReferenceSeed : EntityWithLogSeedSerializer<CandidateReference, CandidateReferenceLog, CandidateReferenceMapper>
    {
        public CandidateReferenceSeed()
        {
            var candidateReferences = new List<CandidateReference>()
            {
                new CandidateReference
                {
                    Referrer = "John Hopkins",
                    Designation = "Junior Manager",
                    Address = "New York",
                    CompanyOrOrg = "Sun Microsystems",
                    ContactNo = "2313132173",
                    Email = "jh@gmail.com",
                    Relation = "Uncle",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateReference
                {
                    Referrer = "Michael Angelo",
                    Designation = "Software Developer",
                    Address = "New York",
                    CompanyOrOrg = "Microsoft Corporation",
                    ContactNo = "99113123",
                    Email = "michael@gmail.com",
                    Relation = "Brother",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateReference
                {
                    Referrer = "Andrew Paul",
                    Designation = "Assistant Professor",
                    Address = "New York",
                    CompanyOrOrg = "Cornell Universiry",
                    ContactNo = "98932424",
                    Email = "paul@gmail.com",
                    Relation = "Teacher",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 2
                },
                new CandidateReference
                {
                    Referrer = "Dr. Azam Khan",
                    Designation = "Professor",
                    Address = "Bangladesh",
                    CompanyOrOrg = "BUET",
                    ContactNo = "012312344",
                    Email = "azam@gmail.com",
                    Relation = "Teacher",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 3
                },
                new CandidateReference
                {
                    Referrer = "Monzoor Sharif",
                    Designation = "Associate Professor",
                    Address = "Bangladesh",
                    CompanyOrOrg = "University of Dhaka",
                    ContactNo = "01398344",
                    Email = "monzoor@gmail.com",
                    Relation = "Teacher",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 3
                },
                new CandidateReference
                {
                    Referrer = "Shajib Kibria",
                    Designation = "Major General",
                    Address = "Bangladesh",
                    CompanyOrOrg = "Bangladesh Army",
                    ContactNo = "0187612313",
                    Email = "shajib@gmail.com",
                    Relation = "Uncle",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 4
                },
                new CandidateReference
                {
                    Referrer = "Mili Khandokar",
                    Designation = "Associate Professor",
                    Address = "Bangladesh",
                    CompanyOrOrg = "Bangladesh Polytechnic Institute",
                    ContactNo = "01322312321",
                    Email = "mili@gmail.com",
                    Relation = "Teacher",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 4
                },
                new CandidateReference
                {
                    Referrer = "Bappi Ahsan",
                    Designation = "Lecturer",
                    Address = "Bangladesh",
                    CompanyOrOrg = "University of Dhaka",
                    ContactNo = "019871233",
                    Email = "bappi@gmail.com",
                    Relation = "Teacher",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 5
                }
            };

            Seed(candidateReferences);
        }
    }
}
