﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class CandidateSeed : EntityWithLogSeedSerializer<Candidate, CandidateLog, CandidateMapper>
    {
        public CandidateSeed()
        {
            var candidates = new List<Candidate>
            {
                new Candidate
                {
                    TrackNo = "12231",
                    FirstName = "Denzel",
                    LastName = "Washington",
                    FathersName = "Brad Pitt",
                    MothersName = "Meryl Streep",
                    DateOfBirth = DateTime.Parse("1980-12-12"),
                    Gender = "Male",
                    BloodGroup = "B+",
                    MaritalStatus = "Married",
                    Nationality = "American",
                    NationalId = "4141241909",
                    PassportNo = "5552311442",
                    Religion = "Christianism",
                    Email = "denzel@gmail.com",
                    AlternateEmailAddress = "denzi@yahoo.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    PresentAddress = "221 Baker Street, London, England",
                    PermanentAddress = "North South Park, California, USA",
                    DivisionOrState = "California",
                    City = "San Fransisco",
                    ImageDirectory = "BC/photo.jpg",
                    CvDirectory = "DF/cv.doc",
                    Status = EntityStatusEnum.Active
                },
                new Candidate
                {
                    TrackNo = "12232",
                    FirstName = "Tom",
                    LastName = "Hanks",
                    FathersName = "Sylvester Stallone",
                    MothersName = "Kate Perry",
                    DateOfBirth = DateTime.Parse("1980-12-12"),
                    Gender = "Male",
                    BloodGroup = "B+",
                    MaritalStatus = "Married",
                    Nationality = "American",
                    NationalId = "4141241909",
                    PassportNo = "5552311442",
                    Religion = "Christianism",
                    Email = "tom@gmail.com",
                    AlternateEmailAddress = "tom@yahoo.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    PresentAddress = "12/B, Silicon Valley, USA",
                    PermanentAddress = "North South Park, California, USA",
                    DivisionOrState = "California",
                    City = "San Fransisco",
                    ImageDirectory = "BC/photo.jpg",
                    CvDirectory = "DF/cv.doc",
                    Status = EntityStatusEnum.Active
                },
                new Candidate
                {
                    TrackNo = "12233",
                    FirstName = "Kamal",
                    LastName = "Aftab",
                    FathersName = "Khalil Mirza",
                    MothersName = "Amena Begum",
                    DateOfBirth = DateTime.Parse("1980-12-12"),
                    Gender = "Male",
                    BloodGroup = "B+",
                    MaritalStatus = "Married",
                    Nationality = "Bangladeshi",
                    NationalId = "4141241909",
                    PassportNo = "5552311442",
                    Religion = "Islam",
                    Email = "kamal@gmail.com",
                    AlternateEmailAddress = "kamal@yahoo.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    PresentAddress = "Circle 2, Gulshan, Dhaka",
                    PermanentAddress = "Circle 2, Gulshan, Dhaka",
                    DivisionOrState = "Dhaka",
                    City = "Dhaka",
                    ImageDirectory = "BC/photo.jpg",
                    CvDirectory = "DF/cv.doc",
                    Status = EntityStatusEnum.Active
                },
                new Candidate
                {
                    TrackNo = "12234",
                    FirstName = "Orpita",
                    LastName = "Hasan",
                    FathersName = "Galib Hasan",
                    MothersName = "Sharmila Hasan",
                    DateOfBirth = DateTime.Parse("1980-12-12"),
                    Gender = "Female",
                    BloodGroup = "AB+",
                    MaritalStatus = "Unmarried",
                    Nationality = "Bangladeshi",
                    NationalId = "4141241909",
                    PassportNo = "5552311442",
                    Religion = "Islam",
                    Email = "orpita@gmail.com",
                    AlternateEmailAddress = "orpita@yahoo.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    PresentAddress = "Green Road, Dhaka",
                    PermanentAddress = "Mohipal, Feni",
                    DivisionOrState = "Dhaka",
                    City = "Dhaka",
                    ImageDirectory = "BC/photo.jpg",
                    CvDirectory = "DF/cv.doc",
                    Status = EntityStatusEnum.Active
                },
                new Candidate
                {
                    TrackNo = "12235",
                    FirstName = "Azmir",
                    LastName = "Resan",
                    FathersName = "Kamal Hasan",
                    MothersName = "Farida Banu",
                    DateOfBirth = DateTime.Parse("1980-12-12"),
                    Gender = "Male",
                    BloodGroup = "B-",
                    MaritalStatus = "Unmarried",
                    Nationality = "Bangladeshi",
                    NationalId = "4141241909",
                    PassportNo = "5552311442",
                    Religion = "Islam",
                    Email = "azmir@gmail.com",
                    AlternateEmailAddress = "azmir@yahoo.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    PresentAddress = "Indira Road, Farmgate, Dhaka",
                    PermanentAddress = "Rajapur, Chandpur",
                    DivisionOrState = "Dhaka",
                    City = "Dhaka",
                    ImageDirectory = "BC/photo.jpg",
                    CvDirectory = "DF/cv.doc",
                    Status = EntityStatusEnum.Active
                }
            };

            Seed(candidates);
        }
    }
}
