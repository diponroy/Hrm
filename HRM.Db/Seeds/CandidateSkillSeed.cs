﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class CandidateSkillSeed : EntityWithLogSeedSerializer<CandidateSkill, CandidateSkillLog, CandidateSkillMapper>
    {
        public CandidateSkillSeed()
        {
            var candidateSkills = new List<CandidateSkill>
            {
                new CandidateSkill
                {
                    Title = "Debate",
                    Description = "Participated in Colleges and Varsities",
                    AttachmentDirectory = "C:\\Direc\\deb.doc",
                    Type = "Extracurricular",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateSkill
                {
                    Title = "Drawing",
                    Description = "Participated in Colleges and Varsities",
                    AttachmentDirectory = "C:\\Direc\\drawing.docx",
                    Type = "Extracurricular",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateSkill
                {
                    Title = "ACM Contesting",
                    Description = "Participated in Intra University Contests",
                    AttachmentDirectory = "C:\\Direc\\acm.doc",
                    Type = "Professional",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },

                 new CandidateSkill
                {
                    Title = "SQL",
                    Description = "Database programming",
                    AttachmentDirectory = "C:\\Direc\\acm.doc",
                    Type = "Professional",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateSkill
                {
                    Title = "ACM Contesting",
                    Description = "Participated in Intra University Contests",
                    AttachmentDirectory = "C:\\Direc\\acm.doc",
                    Type = "Professional",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 2
                },
                new CandidateSkill
                {
                    Title = "Football",
                    Description = "Participated in Colleges",
                    AttachmentDirectory = "C:\\Direc\\deb.doc",
                    Type = "Extracurricular",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 2
                },
                new CandidateSkill
                {
                    Title = "Chess",
                    Description = "Participated in Colleges and University contests",
                    AttachmentDirectory = "C:\\Direc\\chess.doc",
                    Type = "Extracurricular",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 3
                },
                new CandidateSkill
                {
                    Title = "Singing",
                    Description = "Participated in University contests",
                    AttachmentDirectory = "C:\\Direc\\song.doc",
                    Type = "Extracurricular",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 4
                },
                new CandidateSkill
                {
                    Title = "Poetry",
                    Description = "Published several books in book fair",
                    AttachmentDirectory = "C:\\Direc\\book.doc",
                    Type = "Extracurricular",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 5
                }
            };

            Seed(candidateSkills);
        }
    }
}
