﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class CandidateTrainingSeed : EntityWithLogSeedSerializer<CandidateTraining, CandidateTrainingLog, CandidateTrainingMapper>
    {
        public CandidateTrainingSeed()
        {
            var candidateTrainings = new List<CandidateTraining>
            {   
                new CandidateTraining
                {
                    Title = "Internship Training",
                    Institution = "Microsoft Corporation",
                    Address = "Silicon Valley",
                    DurationFromDate = DateTime.Parse("2013-04-04"),
                    DurationToDate = DateTime.Parse("2013-06-06"),
                    Achievement = "A+",
                    AttachmentDirectory = "C:\\Direc\\Achieve.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 1
                },
                new CandidateTraining
                {
                    Title = "Internship Training",
                    Institution = "Facebook",
                    Address = "Silicon Valley",
                    DurationFromDate = DateTime.Parse("2011-04-04"),
                    DurationToDate = DateTime.Parse("2011-06-06"),
                    Achievement = "A",
                    AttachmentDirectory = "C:\\Direc\\Achieve.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 2
                },
                new CandidateTraining
                {
                    Title = "Basic ASP.NET Training",
                    Institution = "BASIS",
                    Address = "Kawran Bazar",
                    DurationFromDate = DateTime.Parse("2013-12-12"),
                    DurationToDate = DateTime.Now,
                    Achievement = "A+",
                    AttachmentDirectory = "C:\\Direc\\Achieve.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 3
                },
                new CandidateTraining
                {
                    Title = "Basic SEO Training",
                    Institution = "BASIS",
                    Address = "Kawran Bazar",
                    DurationFromDate = DateTime.Parse("2013-12-12"),
                    DurationToDate = DateTime.Parse("2014-02-02"),
                    Achievement = "A+",
                    AttachmentDirectory = "C:\\Direc\\Achieve.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 4
                },
                new CandidateTraining
                {
                    Title = "Basic Web Development",
                    Institution = "BCC",
                    Address = "Agargaon",
                    DurationFromDate = DateTime.Parse("2013-12-12"),
                    DurationToDate = DateTime.Parse("2014-05-05"),
                    Achievement = "A+",
                    AttachmentDirectory = "C:\\Direc\\Achieve.docx",
                    Status = EntityStatusEnum.Active,
                    CandidateId = 5
                }
            };

            Seed(candidateTrainings);
        }
    }
}
