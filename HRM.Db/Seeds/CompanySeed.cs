﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class CompanySeed : EntityWithLogSeedSerializer<Company, CompanyLog, CompanyMapper>
    {
        public CompanySeed( )
        {
            var companies = new List<Company>
                            {
                                new Company
                                {
                                    Name = "Khairul Steel Mills" ,
                                    Address = "Dholaikhal, Dhaka." ,
                                    Remarks = "Steel manufacturer and exporter" ,
                                    DateOfCreation = DateTime.Parse("2014-05-05"),
                                    Status = EntityStatusEnum.Active
                                },
                                new Company
                                {
                                    Name = "Bashundhara Group" ,
                                    Address = "Kalurghat, Chittagong." ,
                                    Remarks = "Group of companies" ,
                                    DateOfCreation = DateTime.Parse("2014-08-08"),
                                    Status = EntityStatusEnum.Active
                                },
                                new Company
                                {
                                    Name = "Jamuna Paper Mills" ,
                                    Address = "Tikatuli, Dhaka." ,
                                    Remarks = "Printing Paper manufacturer and exporter" ,
                                    DateOfCreation = DateTime.Parse("2014-05-05"),
                                    Status = EntityStatusEnum.Active
                                },
                                new Company
                                {
                                    Name = "Jamuna Group" ,
                                    Address = "Kalurghat, Chittagong." ,
                                    Remarks = "Group of companies" ,
                                    DateOfCreation = DateTime.Parse("2013-08-08"),
                                    Status = EntityStatusEnum.Active
                                },
                                new Company
                                {
                                    Name = "Humayun Kniting Factory" ,
                                    Address = "Farmgate, Dhaka." ,
                                    Remarks = "Knitware manufacturer and exporter" ,
                                    DateOfCreation = DateTime.Parse("2013-05-05"),
                                    Status = EntityStatusEnum.Active
                                }
                            } ;

            Seed(companies);
        }
    }
}
