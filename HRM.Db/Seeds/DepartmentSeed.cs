﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class DepartmentSeed : EntityWithLogSeedSerializer<Department, DepartmentLog, DepartmentMapper>
    {
        public DepartmentSeed ()
         {
             var departments = new List<Department>
                            {
                                new Department
                                {
                                    Name = "IT" ,
                                    Description = "Information Technology & Support" ,
                                    Remarks = "IT Sector" ,
                                    DateOfCreation = DateTime.Parse("2014-05-05") ,
                                    Status = EntityStatusEnum.Active
                                },
                                new Department
                                {
                                    Name = "HR" ,
                                    Description = "Human Resource" ,
                                    Remarks = "HR Sector" ,
                                    DateOfCreation = DateTime.Parse("2014-05-05") ,
                                    Status = EntityStatusEnum.Active
                                },
                                new Department
                                {
                                    Name = "Marketing" ,
                                    Description = "Marketing Strategy",
                                    Remarks = "Marketing Sector" ,
                                    DateOfCreation = DateTime.Parse("2014-05-05") ,
                                    Status = EntityStatusEnum.Active
                                },
                                new Department
                                {
                                    Name = "Accounts" ,
                                    Description = "Accounting & Payroll" ,
                                    Remarks = "Accounts Sector" ,
                                    DateOfCreation = DateTime.Parse("2014-05-05") ,
                                    Status = EntityStatusEnum.Active
                                },
                                new Department
                                {
                                    Name = "Sales" ,
                                    Description = "Sales and sales return" ,
                                    Remarks = "Sales Sector" ,
                                    DateOfCreation = DateTime.Parse("2014-05-05") ,
                                    Status = EntityStatusEnum.Active
                                }
                            } ;

             Seed(departments) ;
          }
    }
}