﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeAchievementSeed : EntityWithLogSeedSerializer<EmployeeAchievement, EmployeeAchievementLog, EmployeeAchievementMapper>
    {
        public EmployeeAchievementSeed()
        {
            var achievements = new List<EmployeeAchievement>
            {
                new EmployeeAchievement
                {
                    Title = "Good Achievement",
                    Remarks = "Very well known",
                    EmployeeAppraisalId = 1,
                    EmployeeId = 1,
                    Status = EntityStatusEnum.Active
                },
                new EmployeeAchievement
                {
                    Title = "Nice Achievement",
                    Remarks = "Very well known",
                    EmployeeAppraisalId = 2,
                    EmployeeId = 2,
                    Status = EntityStatusEnum.Active
                },
                new EmployeeAchievement
                {
                    Title = "Impressive Achievement",
                    Remarks = "Very well known",
                    EmployeeAppraisalId = 3,
                    EmployeeId = 3,
                    Status = EntityStatusEnum.Active
                },
                new EmployeeAchievement
                {
                    Title = "Satisfactory Achievement",
                    Remarks = "Very well known",
                    EmployeeAppraisalId = 4,
                    EmployeeId = 4,
                    Status = EntityStatusEnum.Active
                },
                new EmployeeAchievement
                {
                    Title = "Well Achievement",
                    Remarks = "Very well known",
                    EmployeeAppraisalId = 4,
                    EmployeeId = 4,
                    Status = EntityStatusEnum.Active
                }
            };

            Seed(achievements);
        }
    }
}
