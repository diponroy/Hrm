﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeAppraisalSeed : EntityWithLogSeedSerializer<EmployeeAppraisal, EmployeeAppraisalLog, EmployeeAppraisalMapper>
    {
        public EmployeeAppraisalSeed()
        {
            var employeeAppraisals = new List<EmployeeAppraisal>
            {
                new EmployeeAppraisal
                {
                    Weight = 5,
                    Remarks = "Good",
                    Status = EntityStatusEnum.Active,
                    EmployeeAssignedAppraisalId = 1
                },
                new EmployeeAppraisal
                {
                    Weight = 3,
                    Remarks = "Good",
                    Status = EntityStatusEnum.Active,
                    EmployeeAssignedAppraisalId = 2
                },
                new EmployeeAppraisal
                {
                    Weight = 4,
                    Remarks = "Good",
                    Status = EntityStatusEnum.Active,
                    EmployeeAssignedAppraisalId = 3
                },
                new EmployeeAppraisal
                {
                    Weight = 4,
                    Remarks = "Good",
                    Status = EntityStatusEnum.Active,
                    EmployeeAssignedAppraisalId = 4
                }
            };

            Seed(employeeAppraisals);
        }
    }
}
