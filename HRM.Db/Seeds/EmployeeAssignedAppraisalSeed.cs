﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeAssignedAppraisalSeed : EntityWithLogSeedSerializer<EmployeeAssignedAppraisal, EmployeeAssignedAppraisalLog, EmployeeAssignedAppraisalMapper>
    {
        public EmployeeAssignedAppraisalSeed()
        {
            var employeeAssignedAppraisals = new List<EmployeeAssignedAppraisal>
            {
                new EmployeeAssignedAppraisal
                {
                    Type = "Monthly",
                    Remarks = "Good Employee",
                    AttachmentDate = DateTime.Parse("2014-03-05"),
                    Status = EntityStatusEnum.Active,
                    AppraisalIndicatorId = 3,
                    EmployeeWorkStationUnitId = 1
                },
                new EmployeeAssignedAppraisal
                {
                    Type = "Half-yearly",
                    Remarks = "Good Employee",
                    AttachmentDate = DateTime.Parse("2014-05-05"),
                    Status = EntityStatusEnum.Active,
                    AppraisalIndicatorId = 4,
                    EmployeeWorkStationUnitId = 1
                },
                new EmployeeAssignedAppraisal
                {
                    Type = "Half-yearly",
                    Remarks = "Good Employee",
                    AttachmentDate = DateTime.Parse("2014-10-05"),
                    Status = EntityStatusEnum.Active,
                    AppraisalIndicatorId = 2,
                    EmployeeWorkStationUnitId = 2
                },
                new EmployeeAssignedAppraisal
                {
                    Type = "Half-yearly",
                    Remarks = "Good Employee",
                    AttachmentDate =DateTime.Parse("2014-12-05"),
                    Status = EntityStatusEnum.Active,
                    AppraisalIndicatorId = 1,
                    EmployeeWorkStationUnitId = 1
                }
            };

            Seed(employeeAssignedAppraisals);
        }
    }
}
