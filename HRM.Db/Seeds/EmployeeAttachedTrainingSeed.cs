﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeAttachedTrainingSeed : EntityWithLogSeedSerializer<EmployeeAttachedTraining, EmployeeAttachedTrainingLog, EmployeeAttachedTrainingMapper>
    {
        public EmployeeAttachedTrainingSeed()
        {
            var employeeAttachedTrainings = new List<EmployeeAttachedTraining>
            {
                new EmployeeAttachedTraining
                {
                    Remarks = "Good Training",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 1,
                    EmployeeTrainingId = 1
                },
                new EmployeeAttachedTraining
                {
                    Remarks = "Good Training",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 2,
                    EmployeeTrainingId = 1
                },
                new EmployeeAttachedTraining
                {
                    Remarks = "Good Training",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 3,
                    EmployeeTrainingId = 3
                },
                new EmployeeAttachedTraining
                {
                    Remarks = "Excellent Training",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 2,
                    EmployeeTrainingId = 2
                },
                new EmployeeAttachedTraining
                {
                    Remarks = "Excellent Training",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 1,
                    EmployeeTrainingId = 4
                },
                new EmployeeAttachedTraining
                {
                    Remarks = "Excellent Training",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 2,
                    EmployeeTrainingId = 3
                }
            };

            Seed(employeeAttachedTrainings);
        }
    }
}
