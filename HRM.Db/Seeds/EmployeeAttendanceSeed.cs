﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeAttendanceSeed :EntityWithLogSeedSerializer<EmployeeAttendance,EmployeeAttendanceLog,EmployeeAttendanceMapper>
    {
        public EmployeeAttendanceSeed()
        {
            List<EmployeeAttendance> employeeAttendances = new List<EmployeeAttendance>()
            {
                new EmployeeAttendance()
                {
                    EmployeeWorkStationUnitId =2,
                    AttendanceTime="10:00",
                    AttendanceDate =DateTime .Parse("2014-12-01"),
                    Remarks="in time",
                    Status = EntityStatusEnum.Active
                },
                new EmployeeAttendance()
                {
                    EmployeeWorkStationUnitId =1,
                    AttendanceTime="10:00",
                    AttendanceDate =DateTime .Parse("2014-12-02"),
                    Remarks="in time",
                    Status = EntityStatusEnum.Active
                },
                new EmployeeAttendance()
                {
                    EmployeeWorkStationUnitId =4,
                    AttendanceTime="10:00",
                    AttendanceDate =DateTime .Parse("2014-12-03"),
                    Remarks="in time",
                    Status = EntityStatusEnum.Active
                },
                new EmployeeAttendance()
                {
                    EmployeeWorkStationUnitId =5,
                    AttendanceTime="10:00",
                    AttendanceDate =DateTime .Parse("2014-12-04"),
                    Remarks="in time",
                    Status = EntityStatusEnum.Active
                },
                new EmployeeAttendance()
                {
                    EmployeeWorkStationUnitId =3,
                    AttendanceTime="10:00",
                    AttendanceDate =DateTime .Parse("2014-12-05"),
                    Remarks="in time",
                    Status = EntityStatusEnum.Active
                }
            };
            Seed(employeeAttendances);
        }
    }
}
