﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeDisciplineSeed : EntityWithLogSeedSerializer<EmployeeDiscipline, EmployeeDisciplineLog, EmployeeDisciplineMapper>
    {
        public EmployeeDisciplineSeed()
        {
            List<EmployeeDiscipline> employeeDisciplines=new List<EmployeeDiscipline>()
            {
                new EmployeeDiscipline()
                {
                  EmployeeId  = 2,
                  Title = "Misuse of Internet",
                  DateOfCreation = DateTime.Parse("2014-12-15"),
                  DepartmentId = 1,
                  Description = "Using the internet by exceedinthe limit allowed..",
                  ActionTaken = "Provide Counselling",
                  AttachmentDirectory = "",
                  Remarks = "",
                  Status = EntityStatusEnum.Active
                },
                new EmployeeDiscipline()
                {
                  EmployeeId  = 1,
                  Title = "Late Entry",
                  DateOfCreation = DateTime.Parse("2014-03-25"),
                  DepartmentId = 5,
                  Description = "Getting late to come to office daily....",
                  ActionTaken = "Give written warning",
                  AttachmentDirectory = "",
                  Remarks = "",
                  Status = EntityStatusEnum.Active
                },
                new EmployeeDiscipline()
                {
                  EmployeeId  = 3,
                  Title = "Misbehavior",
                  DateOfCreation = DateTime.Parse("2014-09-05"),
                  DepartmentId = 3,
                  Description = "Be polite in your behavior",
                  ActionTaken = "Put on probation",
                  AttachmentDirectory = "",
                  Remarks = "",
                  Status = EntityStatusEnum.Active
                },
                new EmployeeDiscipline()
                {
                  EmployeeId  = 4,
                  Title = "Smoke in Working premises",
                  DateOfCreation = DateTime.Parse("2014-10-01"),
                  DepartmentId = 2,
                  Description = "Smoking is prohibited in workig premises....",
                  ActionTaken = "Give verbal warning",
                  AttachmentDirectory = "",
                  Remarks = "",
                  Status = EntityStatusEnum.Active
                }
            } ;
            Seed(employeeDisciplines);  
        }
    }
}
