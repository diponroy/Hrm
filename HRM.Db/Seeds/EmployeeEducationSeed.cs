﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Seeds
{
    class EmployeeEducationSeed : EntitySeedSerializer<EmployeeEducation>
    {
        public EmployeeEducationSeed()
        {
            var employeeEducations = new List<EmployeeEducation>
            {
                new EmployeeEducation
                {
                    Title = "Ph.D. in C.S.E.",
                    Institution = "MIT",
                    DurationFromDate = DateTime.Parse("2011-12-12"),
                    DurationToDate = DateTime.Parse("2013-12-12"),
                    Major = "C.S.E.",
                    Result = "3.70",
                    Remarks = "Awesome",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 1
                },
                new EmployeeEducation
                {
                    Title = "M.Sc. in C.S.E.",
                    Institution = "Yale University",
                    DurationFromDate = DateTime.Parse("2012-01-12"),
                    DurationToDate = DateTime.Parse("2013-12-12"),
                    Major = "C.S.E.",
                    Result = "3.44",
                    Remarks = "Awesome",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 2
                },
                new EmployeeEducation
                {
                    Title = "M.Sc. in C.S.E.",
                    Institution = "BUET",
                    DurationFromDate = DateTime.Parse("2011-01-12"),
                    DurationToDate = DateTime.Parse("2013-12-12"),
                    Major = "C.S.E.",
                    Result = "3.66",
                    Remarks = "Awesome",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 3
                },
                new EmployeeEducation
                {
                    Title = "B.Sc. in C.S.E.",
                    Institution = "BUET",
                    DurationFromDate = DateTime.Parse("2010-01-12"),
                    DurationToDate = DateTime.Parse("2013-12-12"),
                    Major = "C.S.E.",
                    Result = "3.89",
                    Remarks = "Awesome",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 4
                },
                new EmployeeEducation
                {
                    Title = "B.Sc. in C.S.E.",
                    Institution = "DU",
                    DurationFromDate = DateTime.Parse("2010-01-12"),
                    DurationToDate = DateTime.Parse("2013-12-12"),
                    Major = "C.S.E.",
                    Result = "3.83",
                    Remarks = "Awesome",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 5
                }
            };

            Seed(employeeEducations);
        }
    }
}
