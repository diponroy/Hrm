﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Db.Seeds
{
    class EmployeeLoginSeed : EntityWithLogSeedSerializer<EmployeeLogin, EmployeeLoginLog, EmployeeLoginMapper>
    {
        public EmployeeLoginSeed()
        {
            var employees = new List<EmployeeLogin>
            {
                new EmployeeLogin
                {
                    EmployeeId = 1,
                    LoginName = "Admin",
                    Password = PasswordUtility.Encode("123"),
                    Status = EntityStatusEnum.Active
                }
            };

          Seed(employees);
        }
    }
}
