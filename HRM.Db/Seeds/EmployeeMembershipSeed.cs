﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Db.Seeds
{
    class EmployeeMembershipSeed : EntitySeedSerializer<EmployeeMembership>
    {
        public EmployeeMembershipSeed()
        {
            var employeeMembserships = new List<EmployeeMembership>
            {
                new EmployeeMembership
                {
                    Organization = "Dhaka Club",
                    Description = "Nice one to pass time",
                    Type = "Social Welfare Club",
                    Remarks = "Nice",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 1
                },
                new EmployeeMembership
                {
                    Organization = "BASIS",
                    Description = "Member of board of directors",
                    Type = "Software Firm's Associaiton",
                    Remarks = "Awesome",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 2
                },
                new EmployeeMembership
                {
                    Organization = "A2I",
                    Description = "Project Manager",
                    Type = "Government IT Project",
                    Remarks = "Good",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 3
                },
                new EmployeeMembership
                {
                    Organization = "BCC",
                    Description = "General Member",
                    Type = "Bangladesh Computer Council",
                    Remarks = "Nice",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 4
                },
                new EmployeeMembership
                {
                    Organization = "Chayanat",
                    Description = "Art Teacher",
                    Type = "Social Music Learning School",
                    Remarks = "Good",
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 5
                }
            };

            Seed(employeeMembserships);
        }
    }
}
