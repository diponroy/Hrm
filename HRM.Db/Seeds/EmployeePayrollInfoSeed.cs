﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeePayrollInfoSeed : EntityWithLogSeedSerializer<EmployeePayrollInfo, EmployeePayrollInfoLog, EmployeePayrollInfoMapper>
    {
        public EmployeePayrollInfoSeed()
        {
            var employeePayrollInfos = new List<EmployeePayrollInfo>
            {
                new EmployeePayrollInfo
                {
                    BankName = "DBBL Bank",
                    BankDetail = "A nice one with reputaion",
                    AccountNo = "AC 21312301230",
                    TinNumber = 12333,
                    Remarks = "good",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 1
                },
                new EmployeePayrollInfo
                {
                    BankName = "One Bank",
                    BankDetail = "A nice one with reputaion",
                    AccountNo = "AC 21344441230",
                    TinNumber = 12444,
                    Remarks = "good",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 2
                },
                new EmployeePayrollInfo
                {
                    BankName = "DBBL Bank",
                    BankDetail = "A nice one with reputaion",
                    AccountNo = "AC 21355551230",
                    TinNumber = 12555,
                    Remarks = "good",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 3
                },
                new EmployeePayrollInfo
                {
                    BankName = "Prime Bank",
                    BankDetail = "A nice one with reputaion",
                    AccountNo = "AC 21312222230",
                    TinNumber = 12666,
                    Remarks = "good",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 4
                },
                new EmployeePayrollInfo
                {
                    BankName = "Brack Bank",
                    BankDetail = "A nice one with reputaion",
                    AccountNo = "AC 21333331230",
                    TinNumber = 12777,
                    Remarks = "good",
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 5
                }
            };

            Seed(employeePayrollInfos);
        }
    }
}
