﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeePromotionSeed : EntityWithLogSeedSerializer<EmployeePromotion, EmployeePromotionLog, EmployeePromotionMapper>
    {
        public EmployeePromotionSeed()
        {
            var employeePromotions = new List<EmployeePromotion>
            {
                new EmployeePromotion
                {
                    Remarks = "Good",
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeWorkStationUnitId = 1,
                    EmployeeTypeId = 1,
                    WorkStationUnitId = 1
                },
                new EmployeePromotion
                {
                    Remarks = "Good",
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeWorkStationUnitId = 2,
                    EmployeeTypeId = 2,
                    WorkStationUnitId = 1
                },
                new EmployeePromotion
                {
                    Remarks = "Good",
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeWorkStationUnitId = 3,
                    EmployeeTypeId = 3,
                    WorkStationUnitId = 3
                },
                new EmployeePromotion
                {
                    Remarks = "Good",
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeWorkStationUnitId = 4,
                    EmployeeTypeId = 1,
                    WorkStationUnitId = 2
                },
                new EmployeePromotion
                {
                    Remarks = "Good",
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeWorkStationUnitId = 5,
                    EmployeeTypeId = 3,
                    WorkStationUnitId = 1
                }
            };

            Seed(employeePromotions);
        }
    }
}
