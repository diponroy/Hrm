﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeReportingSeed : EntityWithLogSeedSerializer<EmployeeReporting, EmployeeReportingLog, EmployeeReportingMapper>
    {
        public EmployeeReportingSeed()
        {
            var employeeReportings = new List<EmployeeReporting>
            {
                new EmployeeReporting
                {
                    Remarks = "Good Appointment",
                    AttachmentDate = DateTime.Parse("2012-12-12"),
                    Status = EntityStatusEnum.Active,                 
                    ParentEmployeeWorkStationUnitId = 1,
                    EmployeeWorkStationUnitId = 2
                },
                new EmployeeReporting
                {
                    Remarks = "Good Appointment",
                    AttachmentDate = DateTime.Parse("2012-12-12"),
                    Status = EntityStatusEnum.Active,                 
                    ParentEmployeeWorkStationUnitId = 2,
                    EmployeeWorkStationUnitId = 3
                },
                new EmployeeReporting
                {
                    Remarks = "Good Appointment",
                    AttachmentDate = DateTime.Parse("2012-12-12"),
                    Status = EntityStatusEnum.Active,                 
                    ParentEmployeeWorkStationUnitId = 3,
                    EmployeeWorkStationUnitId = 4
                },
                new EmployeeReporting
                {
                    Remarks = "Good Appointment",
                    AttachmentDate = DateTime.Parse("2012-12-12"),
                    Status = EntityStatusEnum.Active,                 
                    ParentEmployeeWorkStationUnitId = 4,
                    EmployeeWorkStationUnitId = 5
                }
            };

            Seed(employeeReportings);
        }
    }
}
