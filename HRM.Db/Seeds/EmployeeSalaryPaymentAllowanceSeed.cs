﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeSalaryPaymentAllowanceSeed : EntityWithLogSeedSerializer<EmployeeSalaryPaymentAllowance, EmployeeSalaryPaymentAllowanceLog, EmployeeSalaryPaymentAllowanceMapper>
    {
        public EmployeeSalaryPaymentAllowanceSeed ()
        {
            var employeeSalaryPaymentAllowances = new List<EmployeeSalaryPaymentAllowance>
            {
               new EmployeeSalaryPaymentAllowance()
               {
                   EmployeeSalaryPaymentId   = 1,
                   EmployeeSalaryStructureAllowanceId = 2,
                   InAmount = 12000,
                   Status = EntityStatusEnum.Active
               },
               new EmployeeSalaryPaymentAllowance()
               {
                   EmployeeSalaryPaymentId   = 2,
                   EmployeeSalaryStructureAllowanceId = 2,
                   InAmount = 45000,
                   Status = EntityStatusEnum.Active
               },
               new EmployeeSalaryPaymentAllowance()
               {
                   EmployeeSalaryPaymentId   = 1,
                   EmployeeSalaryStructureAllowanceId = 3,
                   InAmount = 45000,
                   Status = EntityStatusEnum.Active
               },
               new EmployeeSalaryPaymentAllowance()
               {
                   EmployeeSalaryPaymentId   = 3,
                   EmployeeSalaryStructureAllowanceId = 4,
                   InAmount = 120000,
                   Status = EntityStatusEnum.Active
               },
               new EmployeeSalaryPaymentAllowance()
               {
                   EmployeeSalaryPaymentId   = 4,
                   EmployeeSalaryStructureAllowanceId = 5,
                   InAmount = 150000,
                   Status = EntityStatusEnum.Active
               }
            };
            Seed(employeeSalaryPaymentAllowances);
        }
    }
}
