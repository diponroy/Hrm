﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeSalaryPaymentBonusSeed : EntityWithLogSeedSerializer<EmployeeSalaryPaymentBonus, EmployeeSalaryPaymentBonusLog, EmployeeSalaryPaymentBonusMapper>
    {
        public EmployeeSalaryPaymentBonusSeed()
        {
            var employeeSalaryPaymentBonuses = new List<EmployeeSalaryPaymentBonus>
            {
              new EmployeeSalaryPaymentBonus()
              {
                  EmployeeSalaryPaymentId = 2,
                  EmployeeSalaryStructureBonusId = 1,
                  InAmount = 45000,
                  Status = EntityStatusEnum.Active
              },
              new EmployeeSalaryPaymentBonus()
              {
                  EmployeeSalaryPaymentId = 1,
                  EmployeeSalaryStructureBonusId = 2,
                  InAmount = 66000,
                  Status = EntityStatusEnum.Active
              },
              new EmployeeSalaryPaymentBonus()
              {
                  EmployeeSalaryPaymentId = 4,
                  EmployeeSalaryStructureBonusId = 4,
                  InAmount = 78000,
                  Status = EntityStatusEnum.Active
              },
              new EmployeeSalaryPaymentBonus()
              {
                  EmployeeSalaryPaymentId = 2,
                  EmployeeSalaryStructureBonusId = 3,
                  InAmount = 76000,
                  Status = EntityStatusEnum.Active
              },
              new EmployeeSalaryPaymentBonus()
              {
                  EmployeeSalaryPaymentId = 3,
                  EmployeeSalaryStructureBonusId = 5,
                  InAmount = 65000,
                  Status = EntityStatusEnum.Active
              }
            };
            Seed(employeeSalaryPaymentBonuses);
        }
    }
}
