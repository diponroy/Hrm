﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeSalaryPaymentSeed : EntityWithLogSeedSerializer<EmployeeSalaryPayment, EmployeeSalaryPaymentLog, EmployeeSalaryPaymentMapper>
    {
        public EmployeeSalaryPaymentSeed()
        {
            var employeeSalaryPayments = new List<EmployeeSalaryPayment>
            {
               new EmployeeSalaryPayment()
               {
                   TrackNo = "trac1",
                   EmployeeSalaryStructureId = 3,
                   DateOfPayment =DateTime.Parse("2012-01-12") ,
                   EmployeePayrollInfoId = 2,
                   TotalInAmount = 30000,
                   Status = EntityStatusEnum.Active
               },
               new EmployeeSalaryPayment()
               {
                   TrackNo = "trac2",
                   EmployeeSalaryStructureId = 5,
                   DateOfPayment = DateTime.Parse("2012-02-12"),
                   EmployeePayrollInfoId = 3,
                   TotalInAmount = 40000,
                   Status = EntityStatusEnum.Active
               },
               new EmployeeSalaryPayment()
               {
                   TrackNo = "trac3",
                   EmployeeSalaryStructureId = 2,
                   DateOfPayment = DateTime.Parse("2012-03-12"),
                   EmployeePayrollInfoId = 1,
                   TotalInAmount = 100000,
                   Status = EntityStatusEnum.Active
               },
               new EmployeeSalaryPayment()
               {
                   TrackNo = "trac4",
                   EmployeeSalaryStructureId = 1,
                   DateOfPayment = DateTime.Parse("2012-04-12"),
                   EmployeePayrollInfoId = 2,
                   TotalInAmount = 40000,
                   Status = EntityStatusEnum.Active
               }
            };
            Seed(employeeSalaryPayments);
        }
    }
}
