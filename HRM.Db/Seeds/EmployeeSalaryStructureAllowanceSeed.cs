﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeSalaryStructureAllowanceSeed : EntityWithLogSeedSerializer<EmployeeSalaryStructureAllowance, EmployeeSalaryStructureAllowanceLog, EmployeeSalaryStructureAllowanceMapper>
    {
        public EmployeeSalaryStructureAllowanceSeed()
        {
            var employeeSalaryStructureAllowances = new List<EmployeeSalaryStructureAllowance>
            {
               new EmployeeSalaryStructureAllowance()
               {
                   EmployeeSalaryStructureId = 1, 
                   AllowanceId = 2,
                   AsPercentage = true, 
                   Weight = 12,
                   AttachmentDate = DateTime.Parse("2014-5-10") ,
                   Status = EntityStatusEnum.Active  
               },
               new EmployeeSalaryStructureAllowance()
               {
                   EmployeeSalaryStructureId = 5, 
                   AllowanceId = 1,
                   AsPercentage = false, 
                   Weight = 40,
                   AttachmentDate = DateTime.Parse("2014-12-3") ,
                   Status = EntityStatusEnum.Active  
               },
               new EmployeeSalaryStructureAllowance()
               {
                   EmployeeSalaryStructureId = 4, 
                   AllowanceId = 3,
                   AsPercentage = false, 
                   Weight = 18,
                   AttachmentDate = DateTime.Parse("2014-10-10") ,
                   Status = EntityStatusEnum.Inactive  
               },
               new EmployeeSalaryStructureAllowance()
               {
                   EmployeeSalaryStructureId = 3, 
                   AllowanceId = 2,
                   AsPercentage = true, 
                   Weight = 14,
                   AttachmentDate = DateTime.Parse("2014-7-10") ,
                   Status = EntityStatusEnum.Active  
               },
               new EmployeeSalaryStructureAllowance()
               {
                   EmployeeSalaryStructureId = 2, 
                   AllowanceId = 5,
                   AsPercentage = true, 
                   Weight = 13,
                   AttachmentDate = DateTime.Parse("2014-5-10") ,
                   DetachmentDate = DateTime.Parse("2014-12-10") ,
                   Status = EntityStatusEnum.Inactive  
               },
            };
            Seed(employeeSalaryStructureAllowances);
        }
    }
}
