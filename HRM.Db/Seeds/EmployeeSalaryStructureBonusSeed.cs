﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeSalaryStructureBonusSeed : EntityWithLogSeedSerializer<EmployeeSalaryStructureBonus, EmployeeSalaryStructureBonusLog, EmployeeSalaryStructureBonusMapper>
    {
        public EmployeeSalaryStructureBonusSeed()
        {
            var employeeSalaryStructureBonuses = new List<EmployeeSalaryStructureBonus>
            {
               new EmployeeSalaryStructureBonus()
               {
                   EmployeeSalaryStructureId = 2,
                   BonusId = 1,
                   AsPercentage = false,
                   Weight = 20,
                   AttachmentDate = DateTime.Parse("2012-5-12"),
                   Status  = EntityStatusEnum.Active
               },
               new EmployeeSalaryStructureBonus()
               {
                   EmployeeSalaryStructureId = 1,
                   BonusId = 2,
                   AsPercentage = false,
                   Weight = 20,
                   AttachmentDate = DateTime.Parse("2013-5-10"),
                   Status  = EntityStatusEnum.Active
               },
               new EmployeeSalaryStructureBonus()
               {
                   EmployeeSalaryStructureId = 3,
                   BonusId = 3,
                   AsPercentage = false,
                   Weight = 20,
                   AttachmentDate = DateTime.Parse("2014-5-12"),
                   Status  = EntityStatusEnum.Active
               },
               new EmployeeSalaryStructureBonus()
               {
                   EmployeeSalaryStructureId = 5,
                   BonusId = 4,
                   AsPercentage = false,
                   Weight = 20,
                   AttachmentDate = DateTime.Parse("2010-5-2"),
                   DetachmentDate = DateTime.Parse("2012-5-12"),
                   Status  = EntityStatusEnum.Active
               },
               new EmployeeSalaryStructureBonus()
               {
                   EmployeeSalaryStructureId = 4,
                   BonusId = 5,
                   AsPercentage = false,
                   Weight = 20,
                   AttachmentDate = DateTime.Parse("2014-5-5"),
                   Status  = EntityStatusEnum.Active
               }
            };
            Seed(employeeSalaryStructureBonuses);
        }
    }
}
