﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeSalaryStructureSeed : EntityWithLogSeedSerializer<EmployeeSalaryStructure, EmployeeSalaryStructureLog, EmployeeSalaryStructureMapper>
    {
        public EmployeeSalaryStructureSeed()
        {
            var employeeSalaryStructures = new List<EmployeeSalaryStructure>
            {
               new EmployeeSalaryStructure()
               {
                   SalaryStructureId = 1, 
                   Basic = 110000,
                   EmployeeWorkstationUnitId = 1, 
                   AttachmentDate = DateTime.Parse("2014-5-10") ,
                   Status = EntityStatusEnum.Active  
               },
               new EmployeeSalaryStructure()
               {
                   SalaryStructureId = 2, 
                   Basic = 200000,
                   EmployeeWorkstationUnitId = 2, 
                   AttachmentDate = DateTime.Parse("2014-5-15") ,
                   Status = EntityStatusEnum.Active  
               } ,
               new EmployeeSalaryStructure()
               {
                   SalaryStructureId = 3, 
                   Basic = 101000,
                   EmployeeWorkstationUnitId = 3, 
                   AttachmentDate = DateTime.Parse("2014-5-21") ,
                   DetachmentDate = DateTime.Parse("2014-5-26") ,
                   Status = EntityStatusEnum.Active  
               },
               new EmployeeSalaryStructure()
               {
                   SalaryStructureId = 4, 
                   Basic = 17000,
                   EmployeeWorkstationUnitId = 4, 
                   AttachmentDate = DateTime.Parse("2014-5-11") ,
                   Status = EntityStatusEnum.Active  
               },
               new EmployeeSalaryStructure()
               {
                   SalaryStructureId = 5, 
                   Basic = 150000,
                   EmployeeWorkstationUnitId = 5, 
                   AttachmentDate = DateTime.Parse("2014-5-14") ,
                   DetachmentDate = DateTime.Parse("2014-5-16") ,
                   Status = EntityStatusEnum.Active  
               }
            };
            Seed(employeeSalaryStructures);
        }
    }
}
