﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeSeed : EntityWithLogSeedSerializer<Employee, EmployeeLog, EmployeeMapper>
    {
        public EmployeeSeed ()
        {
            var employees = new List<Employee>
            {
                new Employee
                {
                    TrackNo = "13332",
                    Salutation = "Mr.",
                    FirstName = "Denzel",
                    LastName = "Washington",
                    FathersName = "Brad Pitt",
                    MothersName = "Meryl Streep",
                    DateOfBirth = DateTime.Parse("1983-08-11"),
                    Gender = "Male",
                    BloodGroup = "B+",
                    Email = "denzel@gmail.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    OfficePhone = "345678909876",
                    PresentAddress = "13/B, Park Street, North Carolina, USA.",
                    PermanentAddress = "California, USA",
                    DivisionOrState = "California",
                    City = "San Fransisco",
                    ImageDirectory = "BC/photo.jpg",
                    Status = EntityStatusEnum.Active,
                },
                new Employee
                {
                    TrackNo = "13333",
                    Salutation = "Mr.",
                    FirstName = "Azmir",
                    LastName = "Resan",
                    FathersName = "Kamal Hasan",
                    MothersName = "Farida Banu",
                    DateOfBirth = DateTime.Parse("1983-08-11"),
                    Gender = "Male",
                    BloodGroup = "O-",
                    Email = "azmir@gmail.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    OfficePhone = "345678909876",
                    PresentAddress = "Indira Road, Farmgate, Dhaka",
                    PermanentAddress = "Rajapur, Chandpur",
                    DivisionOrState = "Dhaka",
                    City = "Dhaka",
                    ImageDirectory = "BC/photo.jpg",
                    Status = EntityStatusEnum.Active,
                },
                new Employee
                 {
                    TrackNo = "13334",
                    Salutation = "Mr.",
                    FirstName = "Ahmed",
                    LastName = "Habib",
                    FathersName = "Reza Habib",
                    MothersName = "Farzana Habib",
                    DateOfBirth = DateTime.Parse("1983-08-11"),
                    Gender = "Male",
                    BloodGroup = "AB-",
                    Email = "habib@gmail.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    OfficePhone = "345678909876",
                    PresentAddress = "23/C Adabor, Shyamoli, Dhaka",
                    PermanentAddress = "Ramgonj, Laxmipur",
                    DivisionOrState = "Dhaka",
                    City = "Dhaka",
                    ImageDirectory = "BC/photo.jpg",
                    Status = EntityStatusEnum.Active,
                },
                new Employee
                 {
                    TrackNo = "13335",
                    Salutation = "Mr.",
                    FirstName = "Siddque",
                    LastName = "Chowdhury",
                    FathersName = "Ahsan Chowdhury",
                    MothersName = "Sabera Chowdhury",
                    DateOfBirth = DateTime.Parse("1983-08-11"),
                    Gender = "Male",
                    BloodGroup = "B+",
                    Email = "siddque@gmail.com",
                    ContactNo = "01710565443",
                    HomePhone = "01234221399",
                    OfficePhone = "345678909876",
                    PresentAddress = "Bazar Road, Jatrabari, Dhaka",
                    PermanentAddress = "Hobigonj, Sylhet.",
                    DivisionOrState = "Dhaka",
                    City = "Dhaka",
                    ImageDirectory = "BC/photo.jpg",
                    Status = EntityStatusEnum.Active,
                }
          };

          Seed(employees);
        }
    }
}
