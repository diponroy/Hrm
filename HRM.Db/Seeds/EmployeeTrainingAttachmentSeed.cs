﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeTrainingAttachmentSeed : EntityWithLogSeedSerializer<EmployeeTrainingAttachment, EmployeeTrainingAttachmentLog, EmployeeTrainingAttachmentMapper>
    {
        public EmployeeTrainingAttachmentSeed()
        {
            var employeeTrainingAttachments = new List<EmployeeTrainingAttachment>
            {
                new EmployeeTrainingAttachment
                {
                    Title = "Professional Training 1",
                    Remarks = "Good",
                    Directory = "C:\\Doc\\training1.docx",
                    Status = EntityStatusEnum.Active,
                    EmployeeTrainingId = 1
                },
                new EmployeeTrainingAttachment
                {
                    Title = "Professional Training 2",
                    Remarks = "Good",
                    Directory = "C:\\Doc\\training2.docx",
                    Status = EntityStatusEnum.Active,
                    EmployeeTrainingId = 2
                },
                new EmployeeTrainingAttachment
                {
                    Title = "Professional Training 3",
                    Remarks = "Good",
                    Directory = "C:\\Doc\\training3.docx",
                    Status = EntityStatusEnum.Active,
                    EmployeeTrainingId = 3
                },
                new EmployeeTrainingAttachment
                {
                    Title = "Professional Training 4",
                    Remarks = "Good",
                    Directory = "C:\\Doc\\training4.docx",
                    Status = EntityStatusEnum.Active,
                    EmployeeTrainingId = 4
                }
            };

            Seed(employeeTrainingAttachments);
        }
    }
}
