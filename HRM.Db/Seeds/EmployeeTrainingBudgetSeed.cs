﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.Configurations.Shared;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeTrainingBudgetSeed : EntityWithLogSeedSerializer<EmployeeTrainingBudget, EmployeeTrainingBudgetLog, EmployeeTrainingBudgetMapper>
    {
        public EmployeeTrainingBudgetSeed()
        {
            var employeeTrainingBudgets = new List<EmployeeTrainingBudget>
            {
                new EmployeeTrainingBudget
                {
                    Description = "Estimated budget",
                    Remarks = "Good",
                    TotalAmount = 10000,
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeTrainingId = 1
                },
                new EmployeeTrainingBudget
                {
                    Description = "Estimated budget",
                    Remarks = "Good",
                    TotalAmount = 3000,
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeTrainingId = 2
                },
                new EmployeeTrainingBudget
                {
                    Description = "Estimated budget",
                    Remarks = "Good",
                    TotalAmount = 4000,
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeTrainingId = 3
                },
                new EmployeeTrainingBudget
                {
                    Description = "Estimated budget",
                    Remarks = "Good",
                    TotalAmount = 5000,
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeTrainingId = 4
                }
            };

            Seed(employeeTrainingBudgets);
        }
    }
}