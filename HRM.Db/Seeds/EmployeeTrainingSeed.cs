﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeTrainingSeed : EntityWithLogSeedSerializer<EmployeeTraining, EmployeeTrainingLog, EmployeeTrainingMapper>
    {
        public EmployeeTrainingSeed()
        {
            var employeeTrainings = new List<EmployeeTraining>
            {
                new EmployeeTraining
                {
                    Title = "Angular JS Training",
                    Description = "Workshop on angular.js development",
                    Organizer = "Office",
                    Trainer = "Md. Sadiq",
                    Venue = "Auditorium",
                    IsApproved = true,
                    Remarks = "Good",
                    DurationFromDate = DateTime.Parse("2011-06-13"),
                    DurationToDate = DateTime.Parse("2011-06-13"),
                    Status = EntityStatusEnum.Active
                },
                new EmployeeTraining
                {
                    Title = "OOP Training",
                    Description = "Workshop on OOP techniques",
                    Organizer = "BCC",
                    Trainer = "Mamun Haider",
                    Venue = "BCC Auditorium",
                    IsApproved = true,
                    Remarks = "Excellent",
                    DurationFromDate = DateTime.Parse("2013-06-13"),
                    DurationToDate = DateTime.Parse("2013-06-14"),
                    Status = EntityStatusEnum.Active
                },
                new EmployeeTraining
                {
                    Title = "Object Oriented CSS Workshop",
                    Description = "Workshop on different new technology on CSS",
                    Organizer = "Office",
                    Trainer = "Md. Sohel",
                    Venue = "Auditorium",
                    IsApproved = true,
                    Remarks = "Good",
                    DurationFromDate = DateTime.Parse("2014-06-13"),
                    DurationToDate = DateTime.Parse("2014-06-13"),
                    Status = EntityStatusEnum.Active
                },
                new EmployeeTraining
                {
                    Title = "MVC Training",
                    Description = "Workshop on ASP.NET MVC",
                    Organizer = "Office",
                    Trainer = "Md. Mojib",
                    Venue = "Auditorium",
                    IsApproved = true,
                    Remarks = "Nice",
                    DurationFromDate = DateTime.Parse("2014-06-13"),
                    DurationToDate = DateTime.Parse("2014-06-15"),
                    Status = EntityStatusEnum.Active
                },
            };

            Seed(employeeTrainings);
        }
    }
}
