﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class EmployeeTransferSeed : EntityWithLogSeedSerializer<EmployeeTransfer, EmployeeTransferLog, EmployeeTransferMapper>
    {
        public EmployeeTransferSeed()
        {
            var employeeTransfers = new List<EmployeeTransfer>
            {
                new EmployeeTransfer
                {
                    Remarks = "Good",
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 1, 
                    EmployeeTypeId = 1,
                    WorkStationUnitId = 1,
                    IncrementSalaryStructureId=2
                },
                new EmployeeTransfer
                {
                    Remarks = "Good",
                    IsApproved = false,
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 2,
                    EmployeeTypeId = 2,
                    WorkStationUnitId = 1,
                    IncrementSalaryStructureId=3
                },
                new EmployeeTransfer
                {
                    Remarks = "Good",
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 3,
                    EmployeeTypeId = 3,
                    WorkStationUnitId = 3,
                    IncrementSalaryStructureId=4
                },
                new EmployeeTransfer
                {
                    Remarks = "Good",
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 4,
                    EmployeeTypeId = 1,
                    WorkStationUnitId = 2,
                    IncrementSalaryStructureId=4
                     
                },
                new EmployeeTransfer
                {
                    Remarks = "Good",
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 5,
                    EmployeeTypeId = 3,
                    WorkStationUnitId = 1,
                    IncrementSalaryStructureId=2
                }
            };

            Seed(employeeTransfers);
        }
    }
}
