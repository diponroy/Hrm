﻿using System.Collections.Generic;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class EmployeeTypeSeed : EntityWithLogSeedSerializer<EmployeeType, EmployeeTypeLog, EmployeeTypeMapper>
    {
        public EmployeeTypeSeed()
        {
            var employeeTypes = new List<EmployeeType>
            {
                //1
                new EmployeeType
                {
                    Title = "CEO",
                    Remarks = "Chief Executive Officer",
                    Status = EntityStatusEnum.Active
                },

                //2
                new EmployeeType
                {
                    Title = "Project Manager",
                    Remarks = "Managerial Job",
                    ParentId = 1,
                    Status = EntityStatusEnum.Active,
                },

                //3
                new EmployeeType
                {
                    Title = "Senior Programmer",
                    Remarks = "Programming Job",
                    ParentId = 2,
                    Status = EntityStatusEnum.Active,
                },

                //4
                new EmployeeType
                {
                    Title = "Junior Programmer",
                    Remarks = "Programming Job",
                    ParentId = 3,
                    Status = EntityStatusEnum.Active,
                }
            };

            Seed(employeeTypes);
        }
    }
}
