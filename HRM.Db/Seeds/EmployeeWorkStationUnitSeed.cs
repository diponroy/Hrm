﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class EmployeeWorkStationUnitSeed : EntityWithLogSeedSerializer<EmployeeWorkStationUnit, EmployeeWorkStationUnitLog, EmployeeWorkStationUnitMapper>
    {
        public EmployeeWorkStationUnitSeed()
        {
            var employeeWorkStationUnits = new List<EmployeeWorkStationUnit>
            {
                new EmployeeWorkStationUnit
                {
                    Remarks = "Very Fine",
                    AttachmentDate = DateTime.Parse("2012-12-12"),
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 1,
                    EmployeeTypeId = 1,
                    WorkStationUnitId = 1
                },
                new EmployeeWorkStationUnit
                {
                    Remarks = "Very Fine",
                    AttachmentDate = DateTime.Parse("2012-12-12"),
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 2,
                    EmployeeTypeId = 2,
                    WorkStationUnitId = 1
                },
                new EmployeeWorkStationUnit
                {
                    Remarks = "Very Fine",
                    AttachmentDate = DateTime.Parse("2012-12-12"),
                    Status = EntityStatusEnum.Active,
                    EmployeeId = 3,
                    EmployeeTypeId = 1,
                    WorkStationUnitId = 3
                },
                new EmployeeWorkStationUnit
                {
                    Remarks = "Very Fine",
                    AttachmentDate = DateTime.Parse("2012-12-12"),
                    DetachmentDate = DateTime.Parse("2014-12-12"),
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 4,
                    EmployeeTypeId = 1,
                    WorkStationUnitId = 4
                },
                new EmployeeWorkStationUnit
                {
                    Remarks = "Very Fine",
                    AttachmentDate = DateTime.Parse("2012-12-12"),
                    DetachmentDate = DateTime.Parse("2014-12-12"),
                    Status = EntityStatusEnum.Inactive,
                    EmployeeId = 5,
                    EmployeeTypeId = 1,
                    WorkStationUnitId = 4
                }
            };

            Seed(employeeWorkStationUnits);
        }
    }
}