﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class IncrementSalaryStructureAllowanceSeed : EntityWithLogSeedSerializer<IncrementSalaryStructureAllowance, IncrementSalaryStructureAllowanceLog, IncrementSalaryStructureAllowanceMapper>
    {
        public IncrementSalaryStructureAllowanceSeed()
        {
            var incrementSalaryStructureAllowances = new List<IncrementSalaryStructureAllowance>
            {
                new IncrementSalaryStructureAllowance
                {
                    AsPercentage = true,
                    Weight = 15,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 1,
                    AllowanceId = 1
                } ,
                new IncrementSalaryStructureAllowance
                {
                    AsPercentage = true,
                    Weight =50,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 2,
                    AllowanceId = 2
                },
                new IncrementSalaryStructureAllowance
                {
                    AsPercentage = true,
                    Weight = 25,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 3,
                    AllowanceId = 3
                },
                new IncrementSalaryStructureAllowance
                {
                    AsPercentage = true,
                    Weight = 24,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 4,
                    AllowanceId = 4
                },              
                new IncrementSalaryStructureAllowance
                {
                    AsPercentage = true,
                    Weight = 45,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 5,
                    AllowanceId = 5
                }
            };

            Seed(incrementSalaryStructureAllowances);
        }
    }
}
