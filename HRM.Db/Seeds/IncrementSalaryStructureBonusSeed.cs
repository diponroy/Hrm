﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class IncrementSalaryStructureBonusSeed : EntityWithLogSeedSerializer<IncrementSalaryStructureBonus, IncrementSalaryStructureBonusLog, IncrementSalaryStructureBonusMapper>
    {
        public IncrementSalaryStructureBonusSeed()
        {
            var incrementSalaryStructureBonuses = new List<IncrementSalaryStructureBonus>
            {
                new IncrementSalaryStructureBonus
                {
                    AsPercentage = true,
                    Weight = 5,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 1,
                    BonusId = 1
                },
                new IncrementSalaryStructureBonus
                {
                    AsPercentage = true,
                    Weight = 7,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 1,
                    BonusId = 2
                },
                new IncrementSalaryStructureBonus
                {
                    AsPercentage = true,
                    Weight = 8,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 3,
                    BonusId = 3
                },
                new IncrementSalaryStructureBonus
                {
                    AsPercentage = true,
                    Weight = 9,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 5,
                    BonusId = 3
                },
                new IncrementSalaryStructureBonus
                {
                    AsPercentage = true,
                    Weight = 10,
                    Status = EntityStatusEnum.Active,
                    IncrementSalaryStructureId = 5,
                    BonusId = 5
                }
            };
        Seed(incrementSalaryStructureBonuses);
        }
    }
}
