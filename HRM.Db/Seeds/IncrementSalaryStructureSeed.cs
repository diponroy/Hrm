﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class IncrementSalaryStructureSeed : EntityWithLogSeedSerializer<IncrementSalaryStructure, IncrementSalaryStructureLog, IncrementSalaryStructureMapper>
    {
        public IncrementSalaryStructureSeed()
        {
            var incrementSalaryStructures = new List<IncrementSalaryStructure>
            {
                new IncrementSalaryStructure
                {
                    Basic = 15000,
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeSalaryStructureId= 1
                } ,
                new IncrementSalaryStructure
                {
                    Basic = 16000,
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeSalaryStructureId = 2
                },
                new IncrementSalaryStructure
                {
                    Basic = 18000,
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeSalaryStructureId = 3
                },
                new IncrementSalaryStructure
                {
                    Basic = 25000,
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeSalaryStructureId = 4
                },
                new IncrementSalaryStructure
                {
                    Basic = 55000,
                    IsApproved = true,
                    Status = EntityStatusEnum.Active,
                    EmployeeSalaryStructureId = 5
                }
            };

            Seed(incrementSalaryStructures);
        }
    }
}
