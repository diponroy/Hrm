﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class InterviewCandidateSeed : EntityWithLogSeedSerializer<InterviewCandidate,InterviewCandidateLog, InterviewCandidateMapper>
    {
        public InterviewCandidateSeed()
        {
            var interviewCandidates = new List<InterviewCandidate>
                             {
                                 new InterviewCandidate
                                 {
                                     
                                     AppointmentDateTime =DateTime.Parse("2012-12-14"),
                                     AttendedDateTime = DateTime.Parse("2014-9-14"),
                                     CurrentSalary = 15000,
                                     ExpectedSalary = 30000 ,
                                     ProposedSalary = 25000,
                                     Remarks = "Remarks Interview Candidate 1",
                                     Status =  EntityStatusEnum.Active,
                                     InterviewId = 1,
                                     CandidateId = 1
                                 },
                                 new InterviewCandidate
                                 {
                                     
                                     AppointmentDateTime = DateTime.Parse("2014-8-13"),
                                     AttendedDateTime = DateTime.Parse("2014-9-14"),
                                     CurrentSalary = 17000,
                                     ExpectedSalary = 40000 ,
                                     ProposedSalary = 35000,
                                     Remarks = "Remarks Interview Candidate 2",
                                     Status =  EntityStatusEnum.Active,
                                     InterviewId = 2,
                                     CandidateId = 2 
                                 },
                                 new InterviewCandidate
                                 {
                                     
                                     AppointmentDateTime = DateTime.Parse("2014-7-13"),
                                     AttendedDateTime = DateTime.Parse("2014-8-14"),
                                     CurrentSalary = 23000,
                                     ExpectedSalary = 34000 ,
                                     ProposedSalary = 20400,
                                     Remarks = "Remarks Interview Candidate 3",
                                     Status =  EntityStatusEnum.Active,
                                     InterviewId = 3,
                                     CandidateId = 3 
                                 },
                                 new InterviewCandidate
                                 {
                                     
                                     AppointmentDateTime = DateTime.Parse("2014-8-15"),
                                     AttendedDateTime = DateTime.Parse("2014-9-16"),
                                     CurrentSalary = 12000,
                                     ExpectedSalary = 30000 ,
                                     ProposedSalary = 25000,
                                     Remarks = "Remarks Interview Candidate 4",
                                     Status =  EntityStatusEnum.Active,
                                     InterviewId = 4,
                                     CandidateId = 4 
                                 },
                                 new InterviewCandidate
                                 {
                                     
                                     AppointmentDateTime = DateTime.Parse("2014-8-17"),
                                     AttendedDateTime = DateTime.Parse("2014-9-18"),
                                     CurrentSalary = 11000,
                                     ExpectedSalary = 30000 ,
                                     ProposedSalary = 15000,
                                     Remarks = "Remarks Interview Candidate 5",
                                     Status =  EntityStatusEnum.Active,
                                     InterviewId = 5,
                                     CandidateId = 5 
                                 }

                             };
            Seed(interviewCandidates);
        }
    }
}
