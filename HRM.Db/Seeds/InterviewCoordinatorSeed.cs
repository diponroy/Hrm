﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class InterviewCoordinatorSeed : EntityWithLogSeedSerializer<InterviewCoordinator, InterviewCoordinatorLog, InterviewCoordinatorMapper>
    {
        public InterviewCoordinatorSeed()
        {
            var interviewCoordinators = new List<InterviewCoordinator>()
                                        {
                                            new InterviewCoordinator
                                            {
                                                Remarks = "Interview Coordinator Remarks 1" ,
                                                Status = EntityStatusEnum.Active ,
                                                InterviewId = 1,
                                                EmployeeId = 1
                                            },
                                            new InterviewCoordinator
                                            {
                                                Remarks = "Interview Coordinator Remarks 2" ,
                                                Status = EntityStatusEnum.Active ,
                                                InterviewId = 2,
                                                EmployeeId = 2
                                            },
                                            new InterviewCoordinator 
                                            {
                                                Remarks = "Interview Coordinator Remarks 3" ,
                                                Status = EntityStatusEnum.Active ,
                                                InterviewId = 3,
                                                EmployeeId = 3
                                            },
                                            new InterviewCoordinator 
                                            {
                                                Remarks = "Interview Coordinator Remarks 4" ,
                                                Status = EntityStatusEnum.Active ,
                                                InterviewId = 4,
                                                EmployeeId = 4
                                            },
                                            new InterviewCoordinator 
                                            {
                                                Remarks = "Interview Coordinator Remarks 5" ,
                                                Status = EntityStatusEnum.Active ,
                                                InterviewId = 5,
                                                EmployeeId = 4
                                            }
                                        } ;
        Seed(interviewCoordinators);
        }
    }
}
