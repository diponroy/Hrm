﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class InterviewSeed : EntityWithLogSeedSerializer<Interview, InterviewLog, InterviewMapper>
    {
        public InterviewSeed()
        {
            var interviews = new List<Interview>
                             {
                                 new Interview
                                 {
                                     TrackNo = "t1110",
                                     Title = "Interview Title 1",
                                     Remarks = "Interview Remarks 1",
                                     DurationFromDate = DateTime.Parse("2012-12-12"),
                                     DurationToDate = DateTime.Parse("2012-12-13"),
                                     ManpowerVacancyNoticeId = 1,
                                     Status = EntityStatusEnum.Active
                                 },
                                 new Interview
                                 {
                                     TrackNo = "t111",
                                     Title = "Interview Title 2",
                                     Remarks = "Interview Remarks 2",
                                     DurationFromDate = DateTime.Parse("2012-12-14"),
                                     DurationToDate = DateTime.Parse("2012-12-15"),
                                     ManpowerVacancyNoticeId = 2,
                                     Status = EntityStatusEnum.Active
                                 },
                                 new Interview
                                 {
                                     TrackNo = "t112",
                                     Title = "Interview Title 3",
                                     Remarks = "Interview Remarks 3",
                                     DurationFromDate = DateTime.Parse("2012-12-16"),
                                     DurationToDate = DateTime.Parse("2012-12-17"),
                                     ManpowerVacancyNoticeId = 3,
                                     Status = EntityStatusEnum.Active
                                 },
                                 new Interview
                                 {
                                     TrackNo = "t113",
                                     Title = "Interview Title 4",
                                     Remarks = "Interview Remarks 4",
                                     DurationFromDate = DateTime.Parse("2012-12-18"),
                                     DurationToDate = DateTime.Parse("2012-12-19"),
                                     ManpowerVacancyNoticeId = 3,
                                     Status = EntityStatusEnum.Active
                                 },
                                 new Interview
                                 {
                                     TrackNo = "t114",
                                     Title = "Interview Title 5",
                                     Remarks = "Interview Remarks 5",
                                     DurationFromDate = DateTime.Parse("2012-11-20"),
                                     DurationToDate = DateTime.Parse("2012-11-21"),
                                     ManpowerVacancyNoticeId = 2,
                                     Status = EntityStatusEnum.Active
                                 }
                             };

            Seed(interviews);
        }
    }
}
