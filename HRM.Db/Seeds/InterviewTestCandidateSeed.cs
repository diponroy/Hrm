﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class InterviewTestCandidateSeed : EntityWithLogSeedSerializer<InterviewTestCandidate, InterviewTestCandidateLog, InterviewTestCandidateMapper>
    {
        public InterviewTestCandidateSeed( )
        {
            var interviewTestCandidates = new List<InterviewTestCandidate>
                                          {
                                              new InterviewTestCandidate()
                                              {
                                                  ObtainedMarks = Convert.ToDecimal(80.50) ,
                                                  Remarks = "REmarks for interview test candidate 1" ,
                                                  Status = EntityStatusEnum.Active ,
                                                  InterviewTestId = 1,
                                                  CandidateId = 1
                                              }   ,
                                              new InterviewTestCandidate()
                                              {
                                                  ObtainedMarks = Convert.ToDecimal(45) ,
                                                  Remarks = "REmarks for interview test candidate 2" ,
                                                  Status = EntityStatusEnum.Active ,
                                                  InterviewTestId = 2,
                                                  CandidateId = 2
                                              },
                                              new InterviewTestCandidate()
                                              {
                                                  ObtainedMarks = Convert.ToDecimal(70) ,
                                                  Remarks = "REmarks for interview test candidate 3" ,
                                                  Status = EntityStatusEnum.Active ,
                                                  InterviewTestId = 1,
                                                  CandidateId = 1
                                              },
                                              new InterviewTestCandidate()
                                              {
                                                  ObtainedMarks = Convert.ToDecimal(89) ,
                                                  Remarks = "REmarks for interview test candidate 4" ,
                                                  Status = EntityStatusEnum.Active ,
                                                  InterviewTestId = 4,
                                                  CandidateId = 4
                                              },
                                              new InterviewTestCandidate()
                                              {
                                                  ObtainedMarks = Convert.ToDecimal(80) ,
                                                  Remarks = "REmarks for interview test candidate 5" ,
                                                  Status = EntityStatusEnum.Active ,
                                                  InterviewTestId = 5,
                                                  CandidateId = 5
                                              }
                                          } ;
            Seed(interviewTestCandidates);
        }
    }
}
