﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class InterviewTestSeed : EntityWithLogSeedSerializer<InterviewTest, InterviewTestLog, InterviewTestMapper>
    {
        public InterviewTestSeed( )
        {
            var interviewTests = new List<InterviewTest>()
                                 {
                                     new InterviewTest
                                     {
                                        Title = "title fot interview test 1",
                                        Remarks = "Remarks for interview test 1",
                                        TotalMarks = Convert.ToDecimal(50.8),
                                        Status = EntityStatusEnum.Active,
                                        InterviewId = 1
                                     } ,
                                     new InterviewTest
                                     {
                                        Title = "title fot interview test 2",
                                        Remarks = "Remarks for interview test 2",
                                        TotalMarks = Convert.ToDecimal(68),
                                        Status = EntityStatusEnum.Active,
                                        InterviewId = 2
                                     }  ,
                                     new InterviewTest
                                     {
                                        Title = "title fot interview test 3",
                                        Remarks = "Remarks for interview test 3",
                                        TotalMarks = Convert.ToDecimal(77),
                                        Status = EntityStatusEnum.Active,
                                        InterviewId = 3
                                     },
                                     new InterviewTest
                                     {
                                        Title = "title fot interview test 4",
                                        Remarks = "Remarks for interview test 4",
                                        TotalMarks = Convert.ToDecimal(68),
                                        Status = EntityStatusEnum.Active,
                                        InterviewId = 4
                                     },
                                     new InterviewTest
                                     {
                                        Title = "title fot interview test 5",
                                        Remarks = "Remarks for interview test 5",
                                        TotalMarks = Convert.ToDecimal(80),
                                        Status = EntityStatusEnum.Active,
                                        InterviewId = 5
                                     }
                                     
                                 } ;
            Seed(interviewTests);
        }
    }
}
