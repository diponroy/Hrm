﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class ManpowerCandidateReferenceSeed : EntityWithLogSeedSerializer<ManpowerCandidateReference, ManpowerCandidateReferenceLog, ManpowerCandidateReferenceMapper>
    {
        public ManpowerCandidateReferenceSeed()
        {
            var candidateReferences = new List<ManpowerCandidateReference>
            {
                new ManpowerCandidateReference
                {
                    Remarks = "Very talented",
                    ManpowerVacancyNoticeCandidateId = 1,
                    EmployeeId = 1,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerCandidateReference
                {
                    Remarks = "Very good",
                    ManpowerVacancyNoticeCandidateId = 2,
                    EmployeeId = 2,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerCandidateReference
                {
                    Remarks = "Very impressive",
                    ManpowerVacancyNoticeCandidateId = 3,
                    EmployeeId = 3,
                    Status = EntityStatusEnum.Inactive
                },
                new ManpowerCandidateReference
                {
                    Remarks = "Very attentive",
                    ManpowerVacancyNoticeCandidateId = 4,
                    EmployeeId = 4,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerCandidateReference
                {
                    Remarks = "Very good looking",
                    ManpowerVacancyNoticeCandidateId = 5,
                    EmployeeId = 5,
                    Status = EntityStatusEnum.Active
                }
            };

            Seed(candidateReferences);
        }
    }
}
