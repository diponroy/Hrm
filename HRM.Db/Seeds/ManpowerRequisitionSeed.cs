﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class ManpowerRequisitionSeed : EntityWithLogSeedSerializer<ManpowerRequisition, ManpowerRequisitionLog, ManpowerRequisitionMapper>
    {
        public ManpowerRequisitionSeed( )
        {
            var manpowerRequisitions = new List<ManpowerRequisition>
                                       {
                                           new ManpowerRequisition
                                           {
                                              TrackNo = "t210",
                                              Title = "Title 1",
                                              PositionForDesignation = "Chairman",
                                              NumberOfPersons = 2,
                                              Description = "Description 1",
                                              DateOfCreation = DateTime.Parse("2014-5-10"),
                                              DedlineDate = DateTime.Parse("2014-5-11"),
                                              IsApproved = true,
                                              Status = EntityStatusEnum.Active
                                           } ,
                                           new ManpowerRequisition
                                           {
                                              TrackNo = "t211",
                                              Title = "Title 2",
                                              PositionForDesignation = "FAVP",
                                              NumberOfPersons = 3,
                                              Description = "Description 2",
                                              DateOfCreation = DateTime.Parse("2014-5-12"),
                                              DedlineDate = DateTime.Parse("2014-5-13"),
                                              IsApproved = true,
                                              Status = EntityStatusEnum.Active
                                           },
                                           new ManpowerRequisition
                                           {
                                              TrackNo = "t212",
                                              Title = "Title 3",
                                              PositionForDesignation = "AVP",
                                              NumberOfPersons = 4,
                                              Description = "Description  3",
                                              DateOfCreation = DateTime.Parse("2014-5-14"),
                                              DedlineDate = DateTime.Parse("2014-5-15"),
                                              IsApproved = true,
                                              Status = EntityStatusEnum.Active
                                           },
                                           new ManpowerRequisition
                                           {
                                              TrackNo = "t213",
                                              Title = "Title 4",
                                              PositionForDesignation = "SEO",
                                              NumberOfPersons = 20,
                                              Description = "Description 4",
                                              DateOfCreation = DateTime.Parse("2014-5-16"),
                                              DedlineDate = DateTime.Parse("2014-5-17"),
                                              IsApproved = true,
                                              Status = EntityStatusEnum.Active
                                           },
                                           new ManpowerRequisition
                                           {
                                              TrackNo = "t214",
                                              Title = "Title 5",
                                              PositionForDesignation = "EO",
                                              NumberOfPersons = 30,
                                              Description = "Description 5",
                                              DateOfCreation = DateTime.Parse("2014-5-18"),
                                              DedlineDate = DateTime.Parse("2014-5-19"),
                                              IsApproved = true,
                                              Status = EntityStatusEnum.Active
                                           }
                                       } ;
           Seed(manpowerRequisitions);
        }
    }
}
