﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class ManpowerVacancyNoticeCandidateSeed : EntityWithLogSeedSerializer<ManpowerVacancyNoticeCandidate, ManpowerVacancyNoticeCandidateLog, ManpowerVacancyNoticeCandidateMapper>
    {
        public ManpowerVacancyNoticeCandidateSeed()
        {
            var vacancyNoticeCandidates = new List<ManpowerVacancyNoticeCandidate>
            {
                new ManpowerVacancyNoticeCandidate
                {
                    TrackNo = "MVNC-2014-1",
                    ManpowerVacancyNoticeId = 1,
                    CandidateId = 1,
                    EmployeeId = 1,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerVacancyNoticeCandidate
                {
                    TrackNo = "MVNC-2014-2",
                    ManpowerVacancyNoticeId = 2,
                    CandidateId = 2,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerVacancyNoticeCandidate
                {
                    TrackNo = "MVNC-2014-3",
                    ManpowerVacancyNoticeId = 3,
                    CandidateId = 3,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerVacancyNoticeCandidate
                {
                    TrackNo = "MVNC-2014-4",
                    ManpowerVacancyNoticeId = 3,
                    CandidateId = 1,
                    EmployeeId = 1,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerVacancyNoticeCandidate
                {
                    TrackNo = "MVNC-2014-5",
                    ManpowerVacancyNoticeId = 4,
                    CandidateId = 4,
                    Status = EntityStatusEnum.Inactive
                }
            };

            Seed(vacancyNoticeCandidates);
        }
    }
}
