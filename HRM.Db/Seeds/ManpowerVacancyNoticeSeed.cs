﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class ManpowerVacancyNoticeSeed : EntityWithLogSeedSerializer<ManpowerVacancyNotice, ManpowerVacancyNoticeLog, ManpowerVacancyNoticeMapper>
    {
        public ManpowerVacancyNoticeSeed()
        {
            var manpowerVacancyNotices = new List<ManpowerVacancyNotice>
            {
                new ManpowerVacancyNotice
                {
                    TrackNo = "91875444",
                    Type = "External",
                    Title = "Senior Software Developer",
                    DurationFromDate = DateTime.Parse("2014-08-08"),
                    DurationToDate = DateTime.Parse("2014-09-09"),
                    Description = "Senior Developer recruitment notice",
                    ManpowerRequisitionId = 1,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerVacancyNotice
                {
                    TrackNo = "91875555",
                    Type = "External",
                    Title = "Senior Software Architect",
                    DurationFromDate = DateTime.Parse("2014-07-07"),
                    DurationToDate = DateTime.Parse("2014-09-09"),
                    Description = "Senior Architect recruitment notice",
                    ManpowerRequisitionId = 2,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerVacancyNotice
                {
                    TrackNo = "91875666",
                    Type = "Internal",
                    Title = "Project Manager",
                    DurationFromDate = DateTime.Parse("2014-07-07"),
                    DurationToDate = DateTime.Parse("2014-08-08"),
                    Description = "Project Manager recruitment notice",
                    ManpowerRequisitionId = 3,
                    Status = EntityStatusEnum.Inactive
                },
                new ManpowerVacancyNotice
                {
                    TrackNo = "91875777",
                    Type = "External",
                    Title = "Junior Software Developer",
                    DurationFromDate = DateTime.Parse("2014-03-03"),
                    DurationToDate = DateTime.Parse("2014-03-18"),
                    Description = "Junior Developer recruitment notice",
                    ManpowerRequisitionId = 4,
                    Status = EntityStatusEnum.Active
                },
                new ManpowerVacancyNotice
                {
                    TrackNo = "91875888",
                    Type = "External",
                    Title = "Web Developer",
                    DurationFromDate = DateTime.Parse("2014-05-05"),
                    DurationToDate = DateTime.Parse("2014-05-18"),
                    Description = "Web Developer recruitment notice",
                    ManpowerRequisitionId = 1,
                    Status = EntityStatusEnum.Active
                }
            };

            Seed(manpowerVacancyNotices);
        }
    }
}
