﻿using System.Collections.Generic;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class SalaryStructureAllowanceSeed : EntityWithLogSeedSerializer<SalaryStructureAllowance, SalaryStructureAllowanceLog, SalaryStructureAllowanceMapper>
    {
        public SalaryStructureAllowanceSeed()
        {
            var list = new List<SalaryStructureAllowance>
            {
                new SalaryStructureAllowance
                {
                    SalaryStructureId = 3,
                    AllowanceId = 1,
                    Remarks = "Remarks 1",
                    Status = EntityStatusEnum.Active
                },
                new SalaryStructureAllowance
                {
                    SalaryStructureId = 5,
                    AllowanceId = 2,
                    Remarks = "Remarks 2",
                    Status = EntityStatusEnum.Active
                },
                new SalaryStructureAllowance
                {
                    SalaryStructureId = 2,
                    AllowanceId = 2,
                    Remarks = "Remarks 3",
                    Status = EntityStatusEnum.Active
                },
                new SalaryStructureAllowance
                {
                    SalaryStructureId = 1,
                    AllowanceId = 3,
                    Remarks = "Remarks 4",
                    Status = EntityStatusEnum.Active
                },
                new SalaryStructureAllowance
                {
                    SalaryStructureId = 2,
                    AllowanceId = 4,
                    Remarks = "Remarks 5",
                    Status = EntityStatusEnum.Active
                }
            };

            Seed(list);
        }
    }
}
