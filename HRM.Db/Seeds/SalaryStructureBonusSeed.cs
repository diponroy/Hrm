﻿using System.Collections.Generic;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class SalaryStructureBonusSeed : EntityWithLogSeedSerializer<SalaryStructureBonus, SalaryStructureBonusLog, SalaryStructureBonusMapper>
    {
        public SalaryStructureBonusSeed()
        {
            var list = new List<SalaryStructureBonus>
            {
                new SalaryStructureBonus
                {
                    SalaryStructureId = 1,
                    BonusId = 1,
                    Remarks = "Remarks 1",
                    Status = EntityStatusEnum.Active
                },
                new SalaryStructureBonus
                {
                    SalaryStructureId = 2,
                    BonusId = 2,
                    Remarks = "Remarks 2",
                    Status = EntityStatusEnum.Active
                },
                new SalaryStructureBonus
                {
                    SalaryStructureId = 2,
                    BonusId = 3,
                    Remarks = "Remarks 3",
                    Status = EntityStatusEnum.Active
                },
                new SalaryStructureBonus
                {
                    SalaryStructureId = 3,
                    BonusId = 4,
                    Remarks = "Remarks 4",
                    Status = EntityStatusEnum.Active
                },
                new SalaryStructureBonus
                {
                    SalaryStructureId = 5,
                    BonusId = 5,
                    Remarks = "Remarks 5",
                    Status = EntityStatusEnum.Active
                }
            };
            Seed(list);
        }
    }
}
