﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using HRM.Db.SeedSerializers ;
using HRM.Mapper.Tables;
using HRM.Model.Table ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    class SalaryStructureSeed : EntityWithLogSeedSerializer<SalaryStructure, SalaryStructureLog, SalaryStructureMapper>
    {
        public SalaryStructureSeed()
        {
            var list = new List<SalaryStructure>()
            {
                new SalaryStructure(){ Title = "structure 1", Basic = 70000, Status = EntityStatusEnum.Active},
                new SalaryStructure(){ Title = "structure 2", Basic = 50000, Status = EntityStatusEnum.Active},
                new SalaryStructure(){ Title = "structure 3", Basic = 260000, Status = EntityStatusEnum.Active},
                new SalaryStructure(){ Title = "structure 4", Basic = 320000, Status = EntityStatusEnum.Active},
                new SalaryStructure(){ Title = "structure 5", Basic = 870000, Status = EntityStatusEnum.Active}
            };

            Seed(list);
        }
    }
}
