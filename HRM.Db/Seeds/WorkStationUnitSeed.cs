﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using HRM.Db.SeedSerializers;
using HRM.Mapper.Tables;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Db.Seeds
{
    internal class WorkStationUnitSeed : EntityWithLogSeedSerializer<WorkStationUnit, WorkStationUnitLog, WorkStationUnitMapper>
    {
        public WorkStationUnitSeed()
        {
            var workStationUnits = new List<WorkStationUnit>
            {
                new WorkStationUnit
                {
                    Remarks = "Dhanmondi Unit",
                    DateOfCreation = DateTime.ParseExact("2014-04-04", "yyyy-MM-dd", null),
                    Status = EntityStatusEnum.Active,
                    CompanyId = 1,
                    BranchId = 1,
                    DepartmentId = 1,
                    AffectedByEmployeeId = 1,
                    AffectedDateTime = DateTime.Now
                },
                new WorkStationUnit
                {
                    Remarks = "Tejgaon Unit",
                    DateOfCreation = DateTime.ParseExact("2014-04-04", "yyyy-MM-dd", null),
                    Status = EntityStatusEnum.Active,
                    ParentId = 1,
                    CompanyId = 1,
                    BranchId = 3,
                    DepartmentId = 2,
                    AffectedByEmployeeId = 1,
                    AffectedDateTime = DateTime.Now
                },
                new WorkStationUnit
                {
                    Remarks = "Dhanmondi Unit",
                    DateOfCreation = DateTime.ParseExact("2014-04-04", "yyyy-MM-dd", null),
                    Status = EntityStatusEnum.Active,
                    ParentId = 1,
                    CompanyId = 2,
                    BranchId = 2,
                    DepartmentId = 1,
                    AffectedByEmployeeId = 1,
                    AffectedDateTime = DateTime.Now
                },
                new WorkStationUnit
                {
                    Remarks = "Gulshan Unit",
                    DateOfCreation = DateTime.ParseExact("2014-04-04", "yyyy-MM-dd", null),
                    Status = EntityStatusEnum.Active,
                    ParentId = 1,
                    CompanyId = 3,
                    BranchId = 2,
                    DepartmentId = 2,
                    AffectedByEmployeeId = 1,
                    AffectedDateTime = DateTime.Now
                }
            };

            Seed(workStationUnits);
        }
    }
}
