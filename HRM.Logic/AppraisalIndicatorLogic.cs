﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class AppraisalIndicatorLogic
    {
        public bool HasAnyActiveChild(long id)
        {
            int totalChilds = new AppraisalIndicatorData().ActiveChilds(id).Count();
            bool value = (totalChilds > 0) ? true : false;
            return value;
        }

        public IEnumerable<AppraisalIndicator> GetAssignableParents(long id)
        {
            var data = new AppraisalIndicatorData();
            IEnumerable<long> childIds = data.ActiveChilds(id).Select(x => x.Id);
            List<AppraisalIndicator> list = data.AllActive().Where(x => !childIds.Contains(x.Id) && x.Id != id).ToList();
            return list;
        }
    }
}
