﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class BonusLogic
    {
        public ICollection<Bonus> Find(int pageNo, int pageSize, Bonus filter)
        {
            List<Bonus> list = new BonusData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public ICollection<BonusType> GetBonusTypes ()
        {
            List<BonusType> list = new BonusTypeData().AllActive().ToList();
            return list;
        }

        public bool HasTitleUsed ( string title )
        {
            bool isUsed = new BonusData().AnyActiveOrInactiveTitle(title);
            if(isUsed)
                return true;
            return false;
        }
        public void Create ( Bonus Bonus )
        {
            if(HasTitleUsed(Bonus.Title))
            {
                throw new Exception("Bonus title already exist.");
            }
            if(Bonus.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!");
            }

            var data = new BonusData();
            data.Add(Bonus);
            data.SaveChanges();
        }
        
        public bool Remove ( Bonus model )
        {
            var data = new BonusData();
            Bonus bonus = data.Get(model.Id);
            bonus.AffectedByEmployeeId = model.AffectedByEmployeeId;
            bonus.AffectedDateTime = model.AffectedDateTime;
            data.Remove(bonus);
            data.SaveChanges();
            return true;
        }
        public Bonus Get ( long id )
        {
            Bonus Bonus = new BonusData().Detail.Get(id);
            return Bonus;
        }
        public IEnumerable<BonusLog> GetLogs ( long id )
        {
            List<BonusLog> list = new BonusData().Detail.AllLog(id).ToList();
            return list;
        }
        public bool HasTitleUsedExcept ( string title, long id )
        {
            return new BonusData().AnyActiveOrInactiveTitleExcept(title, id);
        }

        public void Update ( Bonus model )
        {
            if(HasTitleUsedExcept(model.Title, model.Id))
            {
                throw new Exception("Bonus title already used.");
            }
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            var data = new BonusData();
            Bonus bonus = data.Get(model.Id);
            model.MapTo(bonus);
            data.Replace(bonus);
            data.SaveChanges();
        }
    }
}

