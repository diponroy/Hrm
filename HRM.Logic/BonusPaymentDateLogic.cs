﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class BonusPaymentDateLogic
    {
        public ICollection<Bonus> GetBonusTitles ()
        {
            List<Bonus> list = new BonusData().AllActive().ToList();
            return list;
        }
        public void Create ( BonusPaymentDate bonusPaymentDate )
        {
            if(bonusPaymentDate.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!");
            }

            var data = new BonusPaymentDateData();
            data.Add(bonusPaymentDate);
            data.SaveChanges();
        }
        public ICollection<BonusPaymentDate> Find(int pageNo, int pageSize, BonusPaymentDate filter)
        {
            List<BonusPaymentDate> list = new BonusPaymentDateData().Detail.AllActiveOrInactive().ToList();
            return list;
        }
        public bool Remove ( BonusPaymentDate model )
        {
            var data = new BonusPaymentDateData();
            BonusPaymentDate bonusPaymentDate = data.Get(model.Id);
            bonusPaymentDate.AffectedByEmployeeId = model.AffectedByEmployeeId;
            bonusPaymentDate.AffectedDateTime = model.AffectedDateTime;
            data.Remove(bonusPaymentDate);
            data.SaveChanges();
            return true;
        }
        public BonusPaymentDate Get ( long id )
        {
            BonusPaymentDate bonusPaymentDate = new BonusPaymentDateData().Detail.Get(id);
            return bonusPaymentDate;
        }
        public IEnumerable<BonusPaymentDateLog> GetLogs ( long id )
        {
            List<BonusPaymentDateLog> list = new BonusPaymentDateData().Detail.AllLog(id).ToList();
            return list;
        }
        public void Update ( BonusPaymentDate model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            var data = new BonusPaymentDateData();
            BonusPaymentDate bonusPaymentDate = data.Get(model.Id);
            model.MapTo(bonusPaymentDate);
            data.Replace(bonusPaymentDate);
            data.SaveChanges();
        }
    }
}
