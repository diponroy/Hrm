﻿using System ;
using System.Collections.Generic ;
using System.Linq ;
using System.Text ;
using HRM.Data.Table ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.Log ;
using HRM.Utility ;

namespace HRM.Logic
{
    public class BonusTypeLogic
    {
        public bool HasNameUsed( string name )
        {
            bool isUsed = new BonusTypeData().AnyActiveOrInactiveName(name) ;
            if (isUsed)
            {
                return true ;
            }
            return false ;
        }

        public void Create( BonusType bonusType )
        {
            if (HasNameUsed(bonusType.Name))
            {
                throw new Exception("Bonus type already exist.") ;
            }
            if (bonusType.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!") ;
            }

            var data = new BonusTypeData() ;
            data.Add(bonusType) ;
            data.SaveChanges() ;
        }

        public ICollection<BonusType> Find ( int pageNo, int pageSize, BonusType filter )
        {
            List<BonusType> allTypes = new BonusTypeData().Detail.AllActiveOrInactive().ToList();
            return allTypes ;
        }

        public BonusType Get ( long id )
        {
            BonusType bonusType = new BonusTypeData().Get(id);
            return bonusType;
        }

        public bool Remove ( BonusType model )
        {
            var data = new BonusTypeData();
            BonusType bonusType = data.Get(model.Id);
            bonusType.AffectedByEmployeeId = model.AffectedByEmployeeId;
            bonusType.AffectedDateTime = model.AffectedDateTime;
            data.Remove(bonusType);
            data.SaveChanges() ;
            return true ;
        }

        public IEnumerable<BonusTypeLog> GetLogs ( long id )
        {
            List<BonusTypeLog> list = new BonusTypeData().Detail.AllLog(id).ToList();
            return list ;
        }

        public bool HasNameUsedExcept( string name , long id )
        {
            return new BonusTypeData().AnyActiveOrInactiveNameExcept(name, id);
        }

        public void Update ( BonusType model )
        {
            if (HasNameUsedExcept(model.Name , model.Id))
            {
                throw new Exception("Bonus Type already used.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.") ;
            }

            var data = new BonusTypeData();
            BonusType bonusType = data.Get(model.Id);
            model.MapTo(bonusType);
            data.Replace(bonusType);
            data.SaveChanges() ;
        }
    }
}
