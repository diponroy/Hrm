﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class BranchLogic
    {
        public bool HasName(string name)
        {
            return new BranchData().AnyName(name);
        }

        public void Create(Branch model)
        {
            if (HasName(model.Name))
            {
                throw new Exception("Branch name is used.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            var data = new BranchData();
            data.Add(model);
            data.SaveChanges();
        }

        public bool HasNameExceptBranch(string name, long id)
        {
            return new BranchData().AnyNameExceptId(name, id);
        }

        public Branch Get(long id)
        {
            Branch branch = new BranchData().Get(id);
            return branch;
        }

        public void Update(Branch model)
        {
            if (HasNameExceptBranch(model.Name, model.Id))
            {
                throw new Exception("Branch name is used.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            model.Status = model.IsClosed ? EntityStatusEnum.Inactive : model.Status;
            var data = new BranchData();
            Branch aBranch = data.Get(model.Id);
            model.MapTo(aBranch);
            data.Replace(aBranch);
            data.SaveChanges();
        }

        public void Delete(Branch model)
        {
            var data = new BranchData();
            Branch branch = data.Get(model.Id);
            branch.AffectedByEmployeeId = model.AffectedByEmployeeId;
            branch.AffectedDateTime = model.AffectedDateTime;
            data.Remove(branch);
            data.SaveChanges();
        }

        public IEnumerable<BranchLog> GetLogs(long id)
        {
            List<BranchLog> list = new BranchData().Detail.AllLog(id).ToList();
            return list;
        }

        public IEnumerable<Branch> Find(int pageNo, int pageSize, Branch filter)
        {
            List<Branch> list = new BranchData().Detail.AllActiveOrInactive().ToList();
            return list;
        }
    }
}
