﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class CompanyManageLogic
    {
        public Company Get(long id)
        {
            Company company = new CompanyData().Get(id);
            return company;
        }

        //public bool HasNameUsed(string name)
        //{
        //    bool isUsed = new CompanyData().AnyActiveOrInactiveName(name);
        //    if (isUsed)
        //        return true;
        //    return false;
        //}

        public bool HasNameUsed ( string name )
        {
            bool isUsed = new CompanyData().AllActiveOrInactive().Any(x => x.Name.Equals(name));
            if(isUsed)
                return true;
            return false;
        }

        public void Create(Company model)
        {
            var data = new CompanyData();
            if (HasNameUsed(model.Name))
            {
                throw new Exception("Company Name already exist.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            data.Add(model);
            data.SaveChanges();
        }

        public ICollection<Company> Find(int pageNo, int pageSize, Company filter)
        {
            List<Company> allValueList = new CompanyData().Detail.AllActiveOrInactive().ToList();
            return allValueList;
        }

        public bool NameUsedExceptItself(string name, long id)
        {
            bool isUsed = new CompanyData().AnyActiveOrInactiveNameExcept(name, id);
            if (isUsed)
                return true;
            return false;
        }

        public void Update(Company model)
        {
            if (NameUsedExceptItself(model.Name, model.Id))
            {
                throw new Exception("Company name already used.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            model.Status = (model.IsClosed) ? EntityStatusEnum.Inactive : model.Status;
            var data = new CompanyData();
            Company aCompany = data.Get(model.Id);
            model.MapTo(aCompany);
            data.Replace(aCompany);
            data.SaveChanges();
        }

        public bool Remove(Company model)
        {
            var data = new CompanyData();
            Company aCompany = data.Get(model.Id);
            aCompany.AffectedByEmployeeId = model.AffectedByEmployeeId;
            aCompany.AffectedDateTime = model.AffectedDateTime;
            data.Remove(aCompany);
            data.SaveChanges();
            return true;
        }

        public IEnumerable<CompanyLog> GetLogs(long id)
        {
            List<CompanyLog> list = new CompanyData().Detail.AllLog(id).ToList();
            return list;
        }
    }
}
