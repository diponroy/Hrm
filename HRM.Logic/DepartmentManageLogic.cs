﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class DepartmentManageLogic
    {
        #region List

        public Department Get(long id)
        {
            Department department = new DepartmentData().Get(id);
            return department;
        }

        public IEnumerable<Department> Find(int pageNo, int pageSize, Department filter)
        {
            List<Department> list = new DepartmentData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        #endregion

        #region create

        public void Create(Department model)
        {
            if (NameUsed(model.Name))
            {
                throw new Exception("Department name is used.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            var data = new DepartmentData();
            data.Add(model);
            data.SaveChanges();
        }

        public bool NameUsed(string name)
        {
            bool isUsed = new DepartmentData().AnyName(name);
            return isUsed;
        }

        public bool NameUsedExceptDepartment(long id, string name)
        {
            bool isUsed = new DepartmentData().AnyNameExceptDepartment(id, name);
            return isUsed;
        }

        #endregion

        #region update

        public void Update(Department model)
        {
            if (!IsValidToUpdate(model))
            {
                throw new Exception("invalid to update.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            model.Status = model.IsClosed ? EntityStatusEnum.Inactive : model.Status;
            var data = new DepartmentData();
            Department aDepartment = data.Get(model.Id);
            model.MapTo(aDepartment);
            data.Replace(aDepartment);
            data.SaveChanges();
        }

        private bool IsValidToUpdate(Department model)
        {
            bool validToUpdate = !new DepartmentData().AnyNameExceptDepartment(model.Id, model.Name);
            return validToUpdate;
        }

        #endregion

        #region Delete

        public void Delete(Department model)
        {
            var data = new DepartmentData();
            Department department = data.Get(model.Id);
            department.AffectedByEmployeeId = model.AffectedByEmployeeId;
            department.AffectedDateTime = model.AffectedDateTime;
            data.Remove(department);
            data.SaveChanges();
        }

        #endregion

        #region Detail

        public IEnumerable<DepartmentLog> GetLogs(long id)
        {
            List<DepartmentLog> deptLogs = new DepartmentData().Detail.AllLog(id).ToList();
            return deptLogs;
        }

        #endregion
    }
}
