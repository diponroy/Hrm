﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeAppraisalManageLogic
    {
        public ICollection<EmployeeAppraisal> Find(int pageNo, int pageSize, EmployeeAppraisal filter)
        {
            List<EmployeeAppraisal> list = new EmployeeAppraisalData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public ICollection<EmployeeAssignedAppraisal> GetEmployeeAssignedAppraisalTypes ()
        {
            List<EmployeeAssignedAppraisal> list = new EmployeeAssignedAppraisalData(new HrmContext()).AllActive().ToList();
            return list;
        }

        public void Create ( EmployeeAppraisal model )
        {
            var data = new EmployeeAppraisalData();
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            data.Add(model);
            data.SaveChanges();
        }

        public EmployeeAppraisal Get ( long id )
        {
            EmployeeAppraisal employeeAppraisal = new EmployeeAppraisalData().Detail.Get(id);
            return employeeAppraisal;
        }

        public void Update ( EmployeeAppraisal model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
           
            var data = new EmployeeAppraisalData();
            EmployeeAppraisal employeeAppraisal = data.Get(model.Id);
            model.MapTo(employeeAppraisal);
            data.Replace(employeeAppraisal);
            data.SaveChanges();
        }

        public bool Delete ( EmployeeAppraisal model )
        {
            var data = new EmployeeAppraisalData();
            EmployeeAppraisal employeeAppraisal = data.Get(model.Id);
            employeeAppraisal.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeAppraisal.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employeeAppraisal);
            data.SaveChanges();
            return true;
        }

        public IEnumerable<EmployeeAppraisalLog> GetLogs ( long id )
        {
            List<EmployeeAppraisalLog> list = new EmployeeAppraisalData().Detail.AllLog(id).ToList();
            return list;
        }
    }
}
