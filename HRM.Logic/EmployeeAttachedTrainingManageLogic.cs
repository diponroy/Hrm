﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeAttachedTrainingManageLogic
    {
        public ICollection<EmployeeAttachedTraining> Find ( int pageNo, int pageSize, EmployeeAttachedTraining filter )
        {
            List<EmployeeAttachedTraining> list = new EmployeeAttachedTrainingData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public ICollection<Employee> GetEmployee ()
        {
            List<Employee> list = new EmployeeData().Detail.AllActive().ToList();
            return list;
        }

        public ICollection<EmployeeTraining> GetTrainingTitles ()
        {
            List<EmployeeTraining> list = new EmployeeTrainingData().Detail.AllActive().ToList();
            return list;
        }

        public void Create ( EmployeeAttachedTraining employeeAttachedTraining )
        {
            if(employeeAttachedTraining.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!");
            }

            var data = new EmployeeAttachedTrainingData();
            data.Add(employeeAttachedTraining);
            data.SaveChanges();
        }

        public EmployeeAttachedTraining Get ( long id )
        {
            EmployeeAttachedTraining employeeAttachedTraining = new EmployeeAttachedTrainingData().Detail.Get(id);
            return employeeAttachedTraining;
        }

        public void Update ( EmployeeAttachedTraining model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            var data = new EmployeeAttachedTrainingData();
            EmployeeAttachedTraining employeeAttachedTraining = data.Get(model.Id);
            model.MapTo(employeeAttachedTraining);
            data.Replace(employeeAttachedTraining);
            data.SaveChanges();
        }

        public bool Remove ( EmployeeAttachedTraining model )
        {
            var data = new EmployeeAttachedTrainingData();
            EmployeeAttachedTraining employeeAttachedTraining = data.Get(model.Id);
            employeeAttachedTraining.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeAttachedTraining.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employeeAttachedTraining);
            data.SaveChanges();
            return true;
        }

        public IEnumerable<EmployeeAttachedTrainingLog> GetLogs ( long id )
        {
            List<EmployeeAttachedTrainingLog> list = new EmployeeAttachedTrainingData().Detail.AllLog(id).ToList();
            return list;
        }
    }
}
