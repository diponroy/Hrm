﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeEducationManageLogic
    {
        public ICollection<Employee> GetEmployees()
        {
            List<Employee> list = new EmployeeData()
                .AllActive()
                .ToList();
            return list;
        }

        public void Create(EmployeeEducation employeeEducation)
        {
            if (employeeEducation.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!");
            }

            var educationData = new EmployeeEducationData();
            educationData.Add(employeeEducation);
            educationData.SaveChanges();
        }

        public EmployeeEducation Get(long id)
        {
            EmployeeEducation empEducation = new EmployeeEducationData().Get(id);
            return empEducation;
        }

        public void Update(EmployeeEducation model)
        {
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!");
            }
            var data = new EmployeeEducationData();
            EmployeeEducation empEducation = data.Get(model.Id);
            model.MapTo(empEducation);
            data.Replace(empEducation);
            data.SaveChanges();
        }

        public void Delete(EmployeeEducation model)
        {
            var data = new EmployeeEducationData();
            EmployeeEducation empEducation = data.Get(model.Id);
            empEducation.AffectedByEmployeeId = model.AffectedByEmployeeId;
            empEducation.AffectedDateTime = model.AffectedDateTime;
            data.Remove(empEducation);
            data.SaveChanges();
        }

        public ICollection<EmployeeEducationLog> GetLogs(long id)
        {
            List<EmployeeEducationLog> logs = new EmployeeEducationData().Detail.AllLog(id).ToList();
            return logs;
        }
        
        public ICollection<EmployeeEducation> GetByEmployeeId(long id)
        {
            List<EmployeeEducation> educations = new EmployeeEducationData().AllActiveOrInactive().Where(x=>x.EmployeeId == id).ToList();
            return educations;
        } 
    }
}
