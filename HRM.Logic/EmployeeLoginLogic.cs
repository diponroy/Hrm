﻿using System.Data.Entity;
using System.Linq;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeLoginLogic
    {
        public string EncodedPassword(string password)
        {
            return PasswordUtility.Encode(password);
        }

        public EmployeeLogin FindActiveLogin(EmployeeLogin login)
        {
            login.Password = EncodedPassword(login.Password);

            EmployeeLogin hasLoging =
                new EmployeeLoginData().Detail.AllActive()
                    .FirstOrDefault(x =>
                            x.LoginName == login.LoginName
                            && x.Password == login.Password);

            if (hasLoging != null)
            {
                hasLoging = (hasLoging.Employee.Status == EntityStatusEnum.Active) ? hasLoging : null;
            }
            return hasLoging;
        }

        public void Create(EmployeeLogin login)
        {
            login.Password = EncodedPassword(login.Password);
            var data = new EmployeeLoginData();
            data.Add(login);
            data.SaveChanges();
        }

        public void Update(EmployeeLogin login)
        {
            login.Password = EncodedPassword(login.Password);
            var data = new EmployeeLoginData();
            EmployeeLogin oldLogin = data.GetByEmployeeId(login.EmployeeId);
            login.MapTo(oldLogin);
            data.Replace(login);
            data.SaveChanges();
        }

        public void UpdatePassword(EmployeeLogin login)
        {
            var data = new EmployeeLoginData();
            EmployeeLogin oldLogin = data.GetByEmployeeId(login.EmployeeId);
            oldLogin.Password = EncodedPassword(login.Password);
            data.Replace(login);
            data.SaveChanges();
        }
    }
}
