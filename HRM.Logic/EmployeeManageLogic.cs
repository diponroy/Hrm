﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeManageLogic
    {
        public Employee Get(long id)
        {
            Employee employee = new EmployeeData().Get(id);
            return employee;
        }

        public Employee GetByTrackNo(string trackNo)
        {
            Employee employee = new EmployeeData().GetByTrackNo(trackNo);
            return employee;
        }

        public void Create(Employee model)
        {
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!");
            }
            var data = new EmployeeData();
            data.Add(model);
            data.SaveChanges();
        }

        public bool HasEmail(string email)
        {
            return new EmployeeData().AnyEmail(email);
        }

        public bool IsEmailUsedExceptEmployee(long id, string email)
        {
            bool isUsed = new EmployeeData().AnyEmailExceptEmployee(id, email);
            return isUsed;
        }

        public void Update(Employee model)
        {
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!!");
            }

            var data = new EmployeeData();
            Employee anEmployee = data.Get(model.Id);
            model.MapTo(anEmployee);
            data.Replace(anEmployee);
            data.SaveChanges();
        }

        public void UpdateAdditional(long id, Employee employeeAdditional)
        {
            var data = new EmployeeData();
            Employee anEmployee = data.Get(id);

            anEmployee.FathersName = employeeAdditional.FathersName;
            anEmployee.MothersName = employeeAdditional.MothersName;
            anEmployee.BloodGroup = employeeAdditional.BloodGroup;
            anEmployee.ImageDirectory = employeeAdditional.ImageDirectory;
            anEmployee.HomePhone = employeeAdditional.HomePhone;
            anEmployee.OfficePhone = employeeAdditional.OfficePhone;
            anEmployee.PermanentAddress = employeeAdditional.PermanentAddress;
            anEmployee.City = employeeAdditional.City;
            anEmployee.AffectedByEmployeeId = employeeAdditional.AffectedByEmployeeId;
            anEmployee.AffectedDateTime = employeeAdditional.AffectedDateTime;

            //employeeAdditional.Id = anEmployee.Id;
            //employeeAdditional.MapTo(anEmployee);
            data.Replace(anEmployee);
            data.SaveChanges();
        }

        public IEnumerable<Employee> Find(int pageNo, int pageSize, Employee filter)
        {
            List<Employee> list = new EmployeeData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public void Delete(Employee model)
        {
            var data = new EmployeeData();
            Employee employee = data.Get(model.Id);
            employee.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employee.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employee);
            data.SaveChanges();
        }

        public ICollection<EmployeeLog> GetLogs(long id)
        {
            List<EmployeeLog> empLogs = new EmployeeData().Detail.AllLog(id).ToList();
            return empLogs;
        }

        public string GenerateTrackNo()
        {
            var data = new EmployeeData();
            return data.GenerateTrackNo();
        }
    }
}
