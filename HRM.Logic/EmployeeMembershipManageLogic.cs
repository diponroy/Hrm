﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeMembershipManageLogic
    {
        public ICollection<Employee> GetEmployees()
        {
            List<Employee> list = new EmployeeData().AllActive().ToList();
            return list;
        }

        public void Create(EmployeeMembership model)
        {
            var data = new EmployeeMembershipData();
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            data.Add(model);
            data.SaveChanges();
        }

        public EmployeeMembership Get(long id)
        {
            EmployeeMembership empMembership = new EmployeeMembershipData().Get(id);
            return empMembership;
        }

        public void Update(EmployeeMembership model)
        {
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            var data = new EmployeeMembershipData();
            EmployeeMembership empMembership = data.Get(model.Id);
            model.MapTo(empMembership);
            data.Replace(empMembership);
            data.SaveChanges();
        }

        public ICollection<EmployeeMembership> GetByEmployeeId(long id)
        {
            List<EmployeeMembership> memberships = new EmployeeMembershipData().AllActiveOrInactive().Where(x => x.EmployeeId == id).ToList();
            return memberships;
        }

        public void Delete(EmployeeMembership model)
        {
            var data = new EmployeeMembershipData();
            EmployeeMembership membership = data.Get(model.Id);
            membership.AffectedByEmployeeId = model.AffectedByEmployeeId;
            membership.AffectedDateTime = model.AffectedDateTime;
            data.Remove(membership);
            data.SaveChanges();
        }
    }
}
