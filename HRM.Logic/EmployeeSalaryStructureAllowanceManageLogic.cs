﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Logic
{
    public class EmployeeSalaryStructureAllowanceManageLogic
    {
        public ICollection<Allowance> GetAllowanceTitles ()
        {
            List<Allowance> list = new AllowanceData().AllActive().ToList();
            return list;
        }
        public void Create ( EmployeeSalaryStructureAllowance model)
        {
            var data = new EmployeeSalaryStructureAllowanceData();
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            data.Add(model);
            data.SaveChanges();
        }

        public bool Delete ( EmployeeSalaryStructureAllowance model )
        {
            var data = new EmployeeSalaryStructureAllowanceData();
            EmployeeSalaryStructureAllowance employeeSalaryStructureAllowance = data.Get(model.Id);
            employeeSalaryStructureAllowance.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeSalaryStructureAllowance.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employeeSalaryStructureAllowance);
            data.SaveChanges();
            return true;
        }
    }
}
