﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Logic
{
    public class EmployeeSalaryStructureBonusManageLogic
    {
        public ICollection<Bonus> GetBonusTitles ()
        {
            List<Bonus> list = new BonusData().AllActive().ToList();
            return list;
        }
        public void Create ( EmployeeSalaryStructureBonus model )
        {
            var data = new EmployeeSalaryStructureBonusData();
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            data.Add(model);
            data.SaveChanges();
        }

        public bool Delete ( EmployeeSalaryStructureBonus model )
        {
            var data = new EmployeeSalaryStructureBonusData();
            EmployeeSalaryStructureBonus employeeSalaryStructureBonus = data.Get(model.Id);
            employeeSalaryStructureBonus.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeSalaryStructureBonus.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employeeSalaryStructureBonus);
            data.SaveChanges();
            return true;
        }
    }
}
