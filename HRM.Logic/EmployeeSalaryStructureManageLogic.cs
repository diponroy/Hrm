﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeSalaryStructureManageLogic
    {
        public EmployeeSalaryStructure Get ( long id )
        {
            EmployeeSalaryStructure employeeSalaryStructure = new EmployeeSalaryStructureData().Detail.Get(id);
            return employeeSalaryStructure;
        }
        public ICollection<EmployeeSalaryStructure> Find(int pageNo, int pageSize, EmployeeSalaryStructure filter)
        {
            List<EmployeeSalaryStructure> list = new EmployeeSalaryStructureData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public ICollection<SalaryStructure> GetSalaryStructures ()
        {
             List<SalaryStructure> list = new SalaryStructureData().Detail.AllActive().Include(x => x.SalaryStructureAllowances).Include(x => x.SalaryStructureBonuses).ToList();
             return list;
        }

        public ICollection<EmployeeWorkStationUnit> GetEmployeeWorkStationUnits ()
        {
            List<EmployeeWorkStationUnit> list = new EmployeeWorkStationUnitData().Detail.AllActive().ToList();
            return list;
        }

        public void Create ( EmployeeSalaryStructure model, List<EmployeeSalaryStructureAllowance> allowanceList, List<EmployeeSalaryStructureBonus> bonusList )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            var data = new EmployeeSalaryStructureData();
            data.Add(model);
            data.SaveChanges();

            var allowanceData = new EmployeeSalaryStructureAllowanceData();
            foreach(var salaryStructureAllowance in allowanceList)
            {
                salaryStructureAllowance.EmployeeSalaryStructureId = model.Id;
                allowanceData.Add(salaryStructureAllowance);
            }
            allowanceData.SaveChanges();

            var bonusData = new EmployeeSalaryStructureBonusData();
            foreach(var salaryStructureBonuse in bonusList)
            {
                salaryStructureBonuse.EmployeeSalaryStructureId = model.Id;
                bonusData.Add(salaryStructureBonuse);
            }
            bonusData.SaveChanges();
        }

        public void Update ( EmployeeSalaryStructure model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            model.Status = model.IsDetached ? EntityStatusEnum.Inactive : model.Status;

            var data = new EmployeeSalaryStructureData();
            EmployeeSalaryStructure employeeSalaryStructure = data.Get(model.Id);
            model.MapTo(employeeSalaryStructure);
            data.Replace(employeeSalaryStructure);
            data.SaveChanges();
        }
        public bool Remove ( EmployeeSalaryStructure model )
        {
            var data = new EmployeeSalaryStructureData();
            EmployeeSalaryStructure employeeSalaryStructure = data.Get(model.Id);
            employeeSalaryStructure.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeSalaryStructure.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employeeSalaryStructure);
            data.SaveChanges();
            return true;
        }

        public IEnumerable<EmployeeSalaryStructureLog> GetLogs ( long id )
        {
            List<EmployeeSalaryStructureLog> list = new EmployeeSalaryStructureData().Detail.AllLog(id).ToList();
            return list;
        }
    }
}
