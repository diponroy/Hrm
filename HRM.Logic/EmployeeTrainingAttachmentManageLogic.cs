﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeTrainingAttachmentManageLogic
    {
        public ICollection<EmployeeTrainingAttachment> Find(int pageNo, int pageSize, EmployeeTrainingAttachment filter)
        {
            List<EmployeeTrainingAttachment> list = new EmployeeTrainingAttachmentData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public ICollection<EmployeeTraining> GetTrainingTitles ()
        {
            List<EmployeeTraining> list = new EmployeeTrainingData().Detail.AllActive().ToList();
            return list;
        }

        public void Create ( EmployeeTrainingAttachment employeeTrainingAttachment )
        {
            if(employeeTrainingAttachment.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!");
            }

            var data = new EmployeeTrainingAttachmentData();
            data.Add(employeeTrainingAttachment);
            data.SaveChanges();
        }

        public EmployeeTrainingAttachment Get ( long id )
        {
            EmployeeTrainingAttachment employeeTrainingAttachment = new EmployeeTrainingAttachmentData().Detail.Get(id);
            return employeeTrainingAttachment;
        }

        public void Update ( EmployeeTrainingAttachment model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            var data = new EmployeeTrainingAttachmentData();
            EmployeeTrainingAttachment employeeTrainingAttachment = data.Get(model.Id);
            model.MapTo(employeeTrainingAttachment);
            data.Replace(employeeTrainingAttachment);
            data.SaveChanges();
        }

        public bool Remove ( EmployeeTrainingAttachment model )
        {
            var data = new EmployeeTrainingAttachmentData();
            EmployeeTrainingAttachment employeeTrainingAttachment = data.Get(model.Id);
            employeeTrainingAttachment.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeTrainingAttachment.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employeeTrainingAttachment);
            data.SaveChanges();
            return true;
        }

        public IEnumerable<EmployeeTrainingAttachmentLog> GetLogs ( long id )
        {
            List<EmployeeTrainingAttachmentLog> list = new EmployeeTrainingAttachmentData().Detail.AllLog(id).ToList();
            return list;
        }
    }
}
