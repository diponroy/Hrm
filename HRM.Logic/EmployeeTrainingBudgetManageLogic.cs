﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeTrainingBudgetManageLogic
    {
        public ICollection<EmployeeTrainingBudget> Find(int pageNo, int pageSize, EmployeeTrainingBudget filter)
        {
            List<EmployeeTrainingBudget> list = new EmployeeTrainingBudgetData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public void Create ( EmployeeTrainingBudget employeeTrainingBudget )
        {
            if(employeeTrainingBudget.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!");
            }

            var data = new EmployeeTrainingBudgetData();
            data.Add(employeeTrainingBudget);
            data.SaveChanges();
        }

        public EmployeeTrainingBudget Get ( long id )
        {
            EmployeeTrainingBudget employeeTrainingBudget = new EmployeeTrainingBudgetData().Detail.Get(id);
            return employeeTrainingBudget;
        }

        public void Update ( EmployeeTrainingBudget model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            var data = new EmployeeTrainingBudgetData();
            EmployeeTrainingBudget employeeTrainingBudget = data.Get(model.Id);
            model.MapTo(employeeTrainingBudget);
            data.Replace(employeeTrainingBudget);
            data.SaveChanges();
        }

        public bool Remove ( EmployeeTrainingBudget model )
        {
            var data = new EmployeeTrainingBudgetData();
            EmployeeTrainingBudget employeeTrainingBudget = data.Get(model.Id);
            employeeTrainingBudget.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeTrainingBudget.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employeeTrainingBudget);
            data.SaveChanges();
            return true;
        }

        public IEnumerable<EmployeeTrainingBudgetLog> GetLogs ( long id )
        {
            List<EmployeeTrainingBudgetLog> list = new EmployeeTrainingBudgetData().Detail.AllLog(id).ToList();
            return list;
        }

        public ICollection<EmployeeTraining> GetTrainingTitles()
        {
            List<EmployeeTraining> list = new EmployeeTrainingData().Detail.AllActive().ToList();
            return list;
        }
    }
}
