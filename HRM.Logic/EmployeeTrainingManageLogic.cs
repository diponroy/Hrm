﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeTrainingManageLogic
    {
        public ICollection<EmployeeTraining> Find(int pageNo, int pageSize, EmployeeTraining filter)
        {
            List<EmployeeTraining> list = new EmployeeTrainingData().Detail.AllActiveOrInactive().ToList();
            return list;
        } 
        public void Create ( EmployeeTraining employeeTraining )
        {
            if(employeeTraining.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed!!");
            }

            var data = new EmployeeTrainingData();
            data.Add(employeeTraining);
            data.SaveChanges();
        }

        public EmployeeTraining Get ( long id )
        {
            EmployeeTraining employeeTraining = new EmployeeTrainingData().Detail.Get(id);
            return employeeTraining;
        }
        
        public void Update ( EmployeeTraining model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            var data = new EmployeeTrainingData();
            EmployeeTraining employeeTraining = data.Get(model.Id);
            model.MapTo(employeeTraining);
            data.Replace(employeeTraining);
            data.SaveChanges();
        }

        public bool Remove ( EmployeeTraining model )
        {
            var data = new EmployeeTrainingData();
            EmployeeTraining employeeTraining = data.Get(model.Id);
            employeeTraining.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeTraining.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employeeTraining);
            data.SaveChanges();
            return true;
        }

        public IEnumerable<EmployeeTrainingLog> GetLogs ( long id )
        {
            List<EmployeeTrainingLog> list = new EmployeeTrainingData().Detail.AllLog(id).ToList();
            return list;
        }
    }
}
