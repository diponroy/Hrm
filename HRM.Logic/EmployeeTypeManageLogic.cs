﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeTypeManageLogic
    {
        //public EmployeeType Get(long id)
        //{
        //    EmployeeType employeeType = new EmployeeTypeData().Get(id);
        //    return employeeType;
        //}

        public EmployeeType Get(long id)
        {
            EmployeeType entity = new EmployeeTypeData().Detail.Get(id);
            return entity;
        }

        #region Create

        public IEnumerable<EmployeeType> GetActiveParents ()
        {
            List<EmployeeType> list = new EmployeeTypeData().AllActive().ToList();
            return list;
        }

        public bool HasTitle(string title)
        {
            bool isUsed = new EmployeeTypeData().AnyActiveOrInactiveTitle(title);
            if (isUsed)
            {
                return true;
            }
            return false;
        }

        public void Create(EmployeeType model)
        {
            var data = new EmployeeTypeData();
            if (HasTitle(model.Title))
            {
                throw new Exception("Title already used.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            model.ParentId = (model.Status == EntityStatusEnum.Inactive) ? null : model.ParentId;
            data.Add(model);
            data.SaveChanges();
        }

        #endregion

        #region ListView

        public IEnumerable<EmployeeType> Find(int pageNo, int pageSize, EmployeeType filter)
        {
            List<EmployeeType> list = new EmployeeTypeData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public bool HasActiveChilds(long id)
        {
            int totalChilds = new EmployeeTypeData().ActiveChilds(id).Count();
            bool value = (totalChilds > 0) ? true : false;
            return value;
        }

        public bool Delete(EmployeeType model)
        {
            var data = new EmployeeTypeData();
            EmployeeType employeeType = data.Get(model.Id);
            employeeType.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeType.AffectedDateTime = model.AffectedDateTime;
            data.Remove(employeeType);
            data.SaveChanges();
            return true;
        }

        #endregion

        #region Update

        public IEnumerable<EmployeeType> GetAssignableParents(long id)
        {
            var data = new EmployeeTypeData();
            IEnumerable<long> childIds = data.ActiveChilds(id).Select(x => x.Id);
            List<EmployeeType> list = data.AllActiveOrInactive()
                .Where(x => !childIds.Contains(x.Id) && x.Id != id).ToList();
            return list;
        }

        public bool HasTitleUsedExcept(string title, long id)
        {
            return new EmployeeTypeData().AnyActiveOrInactiveTitleExcept(title, id);
        }

        public void Update(EmployeeType model)
        {
            if (HasTitleUsedExcept(model.Title, model.Id))
            {
                throw new Exception("Employee title already used.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            model.ParentId = (model.Status == EntityStatusEnum.Inactive) ? null : model.ParentId;
            var data = new EmployeeTypeData();
            EmployeeType aEmployeeType = data.Get(model.Id);
            model.MapTo(aEmployeeType);
            data.Replace(aEmployeeType);
            data.SaveChanges();
        }

        #endregion

        #region Detail

        public IEnumerable<EmployeeTypeLog> GetLogs(long id)
        {
            List<EmployeeTypeLog> list = new EmployeeTypeData().Detail.AllLog(id).ToList();
            return list;
        }

        #endregion
    }
}
