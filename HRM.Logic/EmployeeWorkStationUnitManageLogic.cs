﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class EmployeeWorkStationUnitManageLogic
    {
        public EmployeeWorkStationUnit Get(long id)
        {
            EmployeeWorkStationUnit entity = new EmployeeWorkStationUnitData().Get(id);
            return entity;
        }

        public EmployeeWorkStationUnit GetDetail(long id)
        {
            EmployeeWorkStationUnit entity = new EmployeeWorkStationUnitData().Detail.Get(id);
            return entity;
        }

        public IEnumerable<Employee> GetEmployees()
        {
            List<Employee> list = new EmployeeData().AllActive().ToList();
            return list;
        }

        public IEnumerable<WorkStationUnit> GetWorkStationUnits()
        {
            List<WorkStationUnit> list =  new WorkStationUnitData().Detail.AllActive().ToList();
            return list;
        }

        public IEnumerable<EmployeeType> GetEmployeeTypes()
        {
            List<EmployeeType> list = new EmployeeTypeData().Detail.AllActive().ToList();
            return list;
        }

        public IEnumerable<EmployeeWorkStationUnit> Find(int pageNo, int pageSize, EmployeeWorkStationUnit filter)
        {
            List<EmployeeWorkStationUnit> list = new EmployeeWorkStationUnitData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public IEnumerable<EmployeeWorkStationUnitLog> GetLogs(long id)
        {
            List<EmployeeWorkStationUnitLog> unitLogs = new EmployeeWorkStationUnitData().Detail.AllLog(id).ToList();
            return unitLogs;
        }

        public bool IscombinationUsed(long employeeId, long workStationUnitId)
        {
            bool isUsed = new EmployeeWorkStationUnitData().HasEmployeeWorkStationUnit(employeeId, workStationUnitId);
            return isUsed;
        }

        private bool HasEmployeeWorkStationUnit(EmployeeWorkStationUnit model)
        {
            return new EmployeeWorkStationUnitData().HasEmployeeWorkStationUnit(model.EmployeeId,
                model.WorkStationUnitId);
        }

        private bool IsValidToUpdate(EmployeeWorkStationUnit model, EmployeeWorkStationUnit oldUnit)
        {
            if (model.WorkStationUnitId != oldUnit.WorkStationUnitId && oldUnit.Status == EntityStatusEnum.Active)
            {
                return false;
            }

            if (model.WorkStationUnitId != oldUnit.WorkStationUnitId && oldUnit.Status == EntityStatusEnum.Inactive &&
                oldUnit.DetachmentDate == null)
            {
                return false;
            }
            return true;
        }

        public void Add(EmployeeWorkStationUnit model)
        {
            if (HasEmployeeWorkStationUnit(model))
            {
                throw new Exception("This Employee is already assigned to this WorkStationUnit !!");
            }

            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status shouldn't be \"Removed\" !!");
            }

            var data = new EmployeeWorkStationUnitData();
            data.Add(model.CreateMapped<EmployeeWorkStationUnit, EmployeeWorkStationUnit>());
            data.SaveChanges();
        }

        public void Update(EmployeeWorkStationUnit model)
        {
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status couldn't be removed.");
            }

            model.Status = (model.DetachmentDate != null) ? EntityStatusEnum.Inactive : model.Status;
            var data = new EmployeeWorkStationUnitData();
            EmployeeWorkStationUnit anEmployeeWorkStationUnit = data.Get(model.Id);

            if (IsValidToUpdate(model, anEmployeeWorkStationUnit))
            {
                model.MapTo(anEmployeeWorkStationUnit);
                data.Replace(anEmployeeWorkStationUnit);
                data.SaveChanges();
            }
            else
            {
                throw new Exception("There has been an error while updating!!!");
            }
        }

        public void Remove(EmployeeWorkStationUnit model)
        {
            var data = new EmployeeWorkStationUnitData();
            EmployeeWorkStationUnit anEmployeeWorkStationUnit = data.Get(model.Id);
            anEmployeeWorkStationUnit.AffectedByEmployeeId = model.AffectedByEmployeeId;
            anEmployeeWorkStationUnit.AffectedDateTime = model.AffectedDateTime;
            data.Remove(anEmployeeWorkStationUnit);
            data.SaveChanges();
        }
    }
}