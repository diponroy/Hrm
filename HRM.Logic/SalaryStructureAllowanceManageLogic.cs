﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;

namespace HRM.Logic
{
    public class SalaryStructureAllowanceManageLogic
    {
        public ICollection<SalaryStructure> GetSalaryStructureTitles ()
        {
            List<SalaryStructure> list = new SalaryStructureData().AllActive().ToList();
            return list;
        }
        public ICollection<Allowance> GetAllowanceTitles ()
        {
            List<Allowance> list = new AllowanceData().AllActive().ToList();
            return list;
        }
        public void Create ( SalaryStructureAllowance model )
        {
            var data = new SalaryStructureAllowanceData();
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            data.Add(model);
            data.SaveChanges();
        }

        public ICollection<SalaryStructureAllowance> GetSalaryAllowanceList (long id)
        {
            List<SalaryStructureAllowance> list = new SalaryStructureAllowanceData().Detail.AllActive().Where(x=>x.SalaryStructureId==id).ToList();
            return list;
        }

        public void Update ( SalaryStructureAllowance model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            var data = new SalaryStructureAllowanceData();
            SalaryStructureAllowance salaryStructureAllowance = data.Get(model.Id);
            model.MapTo(salaryStructureAllowance);
            data.Replace(salaryStructureAllowance);
            data.SaveChanges();
        }

        public bool Delete ( SalaryStructureAllowance model )
        {
            var data = new SalaryStructureAllowanceData();
            SalaryStructureAllowance salaryStructureAllowance = data.Get(model.Id);
            salaryStructureAllowance.AffectedByEmployeeId = model.AffectedByEmployeeId;
            salaryStructureAllowance.AffectedDateTime = model.AffectedDateTime;
            data.Remove(salaryStructureAllowance);
            data.SaveChanges();
            return true;
        }

    }
}
