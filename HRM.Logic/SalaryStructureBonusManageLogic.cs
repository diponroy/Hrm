﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;

namespace HRM.Logic
{
    public class SalaryStructureBonusManageLogic
    {
        public ICollection<SalaryStructureBonus> GetSalaryBonusList ( long id )
        {
            List<SalaryStructureBonus> list = new SalaryStructureBonusData().Detail.AllActive().Where(x => x.SalaryStructureId == id).ToList();
            return list;
        }
        
        public ICollection<Bonus> GetBonusTitles ()
        {
            List<Bonus> list = new BonusData().AllActive().ToList();
            return list;
        }
        public void Create ( SalaryStructureBonus model )
        {
            var data = new SalaryStructureBonusData();
            model.Status = EntityStatusEnum.Active;
            data.Add(model);
            data.SaveChanges();
        }

        public void Update ( SalaryStructureBonus model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            var data = new SalaryStructureBonusData();
            SalaryStructureBonus salaryStructureBonus = data.Get(model.Id);
            model.MapTo(salaryStructureBonus);
            data.Replace(salaryStructureBonus);
            data.SaveChanges();
        }

        public bool Delete ( SalaryStructureBonus model )
        {
            var data = new SalaryStructureBonusData();
            SalaryStructureBonus salaryStructureBonus = data.Get(model.Id);
            salaryStructureBonus.AffectedByEmployeeId = model.AffectedByEmployeeId;
            salaryStructureBonus.AffectedDateTime = model.AffectedDateTime;
            data.Remove(salaryStructureBonus);
            data.SaveChanges();
            return true;
        }
    }
}
