﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;
using IsolationLevel = System.Data.IsolationLevel;
using TransactionScope = System.Activities.Statements.TransactionScope;

namespace HRM.Logic
{
    public class SalaryStructureManageLogic
    {
        public SalaryStructure Get(long id)
        {
            SalaryStructure entity = new SalaryStructureData().Detail.Get(id);
            return entity;
        }

        public ICollection<SalaryStructure> Find(int pageNo, int pageSize, SalaryStructure filter)
        {
            List<SalaryStructure> list = new SalaryStructureData().Detail.AllActiveOrInactive().ToList();
            return list;
        }

        public bool HasTitle(string title)
        {
            bool isUsed = new SalaryStructureData().AnyActiveOrInactiveTitle(title);
            if (isUsed)
            {
                return true;
            }
            return false;
        }

        public void Create(SalaryStructure model, List<SalaryStructureAllowance> allowanceList, List<SalaryStructureBonus> bonusList)
        {
                var data = new SalaryStructureData();
                if (HasTitle(model.Title))
                {
                    throw new Exception("Title already used.");
                }
                if (model.Status == EntityStatusEnum.Removed)
                {
                    throw new Exception("status shouldn't be removed.");
                }
                data.Add(model);
                data.SaveChanges();

                var allowanceData = new SalaryStructureAllowanceData(data.Context);
                foreach (var salaryStructureAllowance in allowanceList)
                {
                    salaryStructureAllowance.SalaryStructureId = model.Id;
                    allowanceData.Add(salaryStructureAllowance);
                }
                allowanceData.SaveChanges();

                var bonusData = new SalaryStructureBonusData(data.Context);
                foreach (var salaryStructureBonus in bonusList)
                {
                    salaryStructureBonus.SalaryStructureId = model.Id;
                    bonusData.Add(salaryStructureBonus);
                }
                bonusData.SaveChanges();
        }

        public void Create ( SalaryStructure model, List<SalaryStructureAllowance> allowanceList )
        {
            var data = new SalaryStructureData();
            if(HasTitle(model.Title))
            {
                throw new Exception("Title already used.");
            }
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            data.Add(model);
            data.SaveChanges();

            var allowanceData = new SalaryStructureAllowanceData(data.Context);
            foreach(var salaryStructureAllowance in allowanceList)
            {
                salaryStructureAllowance.SalaryStructureId = model.Id;
                allowanceData.Add(salaryStructureAllowance);
            }
            allowanceData.SaveChanges();
        }

        public void Create ( SalaryStructure model, List<SalaryStructureBonus> bonusList )
        {
            var data = new SalaryStructureData();
            if(HasTitle(model.Title))
            {
                throw new Exception("Title already used.");
            }
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            data.Add(model);
            data.SaveChanges();

            var bonusData = new SalaryStructureBonusData(data.Context);
            foreach(var salaryStructureBonus in bonusList)
            {
                salaryStructureBonus.SalaryStructureId = model.Id;
                bonusData.Add(salaryStructureBonus);
            }
            bonusData.SaveChanges();
        }
        public void Create ( SalaryStructure model )
        {
            var data = new SalaryStructureData();
            if(HasTitle(model.Title))
            {
                throw new Exception("Title already used.");
            }
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            data.Add(model);
            data.SaveChanges();
        }



        public bool HasTitleUsedExcept(string title, long id)
        {
            return new SalaryStructureData().AnyActiveOrInactiveTitleExcept(title, id);
        }

        public void Update(SalaryStructure model)
        {
            if (HasTitleUsedExcept(model.Title, model.Id))
            {
                throw new Exception("title already used.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            var data = new SalaryStructureData();
            SalaryStructure salaryStructure = data.Get(model.Id);
            model.MapTo(salaryStructure);
            data.Replace(salaryStructure);
            data.SaveChanges();
        }

        public bool Delete(SalaryStructure model)
        {
            var data = new SalaryStructureData();
            SalaryStructure salaryStructure = data.Get(model.Id);
            salaryStructure.AffectedByEmployeeId = model.AffectedByEmployeeId;
            salaryStructure.AffectedDateTime = model.AffectedDateTime;
            data.Remove(salaryStructure);
            data.SaveChanges();
            return true;
        }

        public IEnumerable<SalaryStructureLog> GetLogs(long id)
        {
            List<SalaryStructureLog> list = new SalaryStructureData().Detail.AllLog(id).ToList();
            return list;
        }
    }
}
