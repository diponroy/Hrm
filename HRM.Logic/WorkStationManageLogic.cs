﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Logic
{
    public class WorkStationManageLogic
    {
        public ICollection<Company> GetCompanies()
        {
            List<Company> list = new CompanyData().AllActiveAndNotClosed().ToList();
            return list;
        }

        public ICollection<Branch> GetBranches()
        {
            List<Branch> list = new BranchData().AllActiveAndNotClosed().ToList();
            return list;
        }

        public ICollection<Department> GetDepartments()
        {
            List<Department> list = new DepartmentData().AllActiveAndNotClosed().ToList();
            return list;
        }

        public ICollection<WorkStationUnit> GetWorkStations()
        {
            List<WorkStationUnit> list = new WorkStationUnitData().AllActiveAndNotClosed().ToList();
            return list;
        }

        public ICollection<WorkStationUnit> GetPossibleParentWorkStations(long id)
        {
            var data = new WorkStationUnitData();

            List<long> childIds = data.GetChilds(id).Select(x => x.Id).ToList();
            childIds = childIds ?? new List<long>();

            List<WorkStationUnit> list = new WorkStationUnitData().AllActiveAndNotClosed()
                .Where(x => !childIds.Contains(x.Id) && x.Id != id)
                .ToList();

            return list;
        }

        public ICollection<Employee> GetEmployees()
        {
            List<Employee> list = new EmployeeData().AllActive().ToList();
            return list;
        }

        public bool IscombinationUsed(long? companyId, long? branchId, long? departmentId)
        {
            bool isUsed = new WorkStationUnitData().AnyActiveOrInactive(companyId, branchId, departmentId);
            return isUsed;
        }

        public bool IscombinationUsed(long? companyId, long? branchId, long? departmentId, long stationId)
        {
            bool isUsed = new WorkStationUnitData().AnyActiveOrInactiveExcept(companyId, branchId, departmentId,
                stationId);
            return isUsed;
        }

        public void Create(WorkStationUnit model)
        {
            if (IscombinationUsed(model.CompanyId, model.BranchId, model.DepartmentId))
            {
                throw new Exception("Invalid to create.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            var data = new WorkStationUnitData();
            data.Add(model);
            data.SaveChanges();
        }

        public ICollection<WorkStationUnit> Find(int pageNo, int pageSize, WorkStationUnit filter)
        {
            List<WorkStationUnit> stations = new WorkStationUnitData().Detail.AllActiveOrInactive().ToList();
            return stations;
        }

        public void Delete(WorkStationUnit model)
        {
            var data = new WorkStationUnitData();
            WorkStationUnit workStation = data.Get(model.Id);
            workStation.AffectedByEmployeeId = model.AffectedByEmployeeId;
            workStation.AffectedDateTime = model.AffectedDateTime;
            data.Remove(workStation);
            data.SaveChanges();
        }

        public WorkStationUnit GetWorkStation(long id)
        {
            WorkStationUnit station = new WorkStationUnitData().Get(id);
            return station;
        }

        public WorkStationUnit GetWorkStationDetail(long id)
        {
            WorkStationUnit station = new WorkStationUnitData().Detail.Get(id);
            return station;
        }

        public void Update(WorkStationUnit model)
        {
            if (IscombinationUsed(model.CompanyId, model.BranchId, model.DepartmentId, model.Id))
            {
                throw new Exception("Invalid to update.");
            }
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            model.Status = model.IsClosed ? EntityStatusEnum.Inactive : model.Status;
            var data = new WorkStationUnitData();
            WorkStationUnit workStation = data.Get(model.Id);
            model.MapTo(workStation);
            data.Replace(workStation);
            data.SaveChanges();
        }

        public ICollection<WorkStationUnitLog> GetLogs(long id)
        {
            List<WorkStationUnitLog> logs = new WorkStationUnitData().Detail.AllLog(id).ToList();
            return logs;
        }
    }
}
