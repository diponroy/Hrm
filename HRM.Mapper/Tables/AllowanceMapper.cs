﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class AllowanceMapper: LogMapper<Allowance,AllowanceLog>
    {
        protected override AllowanceLog MappForeignKeys ( DbContext context, Allowance entity, AllowanceLog log )
        {
            log.AllowanceTypeId = GetLastLogId<AllowanceTypeLog>(entity.AllowanceTypeId);
            return log;
        }
    }
}
