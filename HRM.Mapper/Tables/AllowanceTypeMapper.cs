﻿using System.Data.Entity ;
using HRM.Mapper.Tables.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Log ;

namespace HRM.Mapper.Tables
{
    public class AllowanceTypeMapper : LogMapper<AllowanceType, AllowanceTypeLog>
    {
        protected override AllowanceTypeLog MappForeignKeys ( DbContext context, AllowanceType entity, AllowanceTypeLog log )
        {
            return log;
        }
    }
}
