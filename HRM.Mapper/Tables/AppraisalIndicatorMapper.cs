﻿using System;
using System.Collections.Generic;
using System.Data.Entity ;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Log ;

namespace HRM.Mapper.Tables
{
    public class AppraisalIndicatorMapper: LogMapper<AppraisalIndicator,AppraisalIndicatorLog>
    {
        protected override AppraisalIndicatorLog MappForeignKeys ( DbContext context, AppraisalIndicator entity, AppraisalIndicatorLog log )
        {
            log.ParentId = FindLastLogId<AppraisalIndicatorLog>(entity.ParentId);
            return log;
        }
    }
}
