﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class BonusMapper :LogMapper<Bonus,BonusLog>
    {
        protected override BonusLog MappForeignKeys ( DbContext context, Bonus entity, BonusLog log )
        {
            log.BonusTypeId = GetLastLogId<BonusTypeLog>(entity.BonusTypeId);
            return log;
        }
    }
}
