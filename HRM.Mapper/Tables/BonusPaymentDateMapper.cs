﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class BonusPaymentDateMapper :LogMapper<BonusPaymentDate,BonusPaymentDateLog>
    {
        protected override BonusPaymentDateLog MappForeignKeys ( DbContext context, BonusPaymentDate entity, BonusPaymentDateLog log )
        {
            log.BonusId = GetLastLogId<BonusLog>(entity.BonusId);
            return log;
        }
    }
}
