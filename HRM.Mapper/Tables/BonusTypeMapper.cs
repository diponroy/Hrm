﻿using System;
using System.Collections.Generic;
using System.Data.Entity ;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared ;
using HRM.Model.Table ;
using HRM.Model.Table.Log ;

namespace HRM.Mapper.Tables
{
    public class BonusTypeMapper : LogMapper<BonusType, BonusTypeLog>
    {
        protected override BonusTypeLog MappForeignKeys ( DbContext context, BonusType entity, BonusTypeLog log )
        {
            return log;
        }
    }
}
