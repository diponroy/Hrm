﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class BranchMapper : LogMapper<Branch, BranchLog>
    {
        protected override BranchLog MappForeignKeys(DbContext context, Branch entity, BranchLog log)
        {
            return log;
        }
    }
}
