﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CandidateAchievementMapper : LogMapper<CandidateAchievement, CandidateAchievementLog>
    {
        protected override CandidateAchievementLog MappForeignKeys(DbContext context, CandidateAchievement entity, CandidateAchievementLog log)
        {
            log.CandidateEmploymentId = GetLastLogId<CandidateLog>(entity.CandidateEmploymentId);
            return log;
        }
    }
}
