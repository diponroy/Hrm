﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CandidateEducationMapper : LogMapper<CandidateEducation, CandidateEducationLog>
    {
        protected override CandidateEducationLog MappForeignKeys(DbContext context, CandidateEducation entity, CandidateEducationLog log)
        {
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            return log;
        }
    }
}
