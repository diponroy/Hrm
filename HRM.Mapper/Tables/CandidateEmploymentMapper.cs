﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CandidateEmploymentMapper : LogMapper<CandidateEmployment, CandidateEmploymentLog>
    {
        protected override CandidateEmploymentLog MappForeignKeys(DbContext context, CandidateEmployment entity, CandidateEmploymentLog log)
        {
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            return log;
        }
    }
}
