﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CandidateLanguageMapper : LogMapper<CandidateLanguage, CandidateLanguageLog>
    {
        protected override CandidateLanguageLog MappForeignKeys(DbContext context, CandidateLanguage entity, CandidateLanguageLog log)
        {
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            return log;
        }
    }
}
