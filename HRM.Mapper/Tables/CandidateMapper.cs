﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CandidateMapper : LogMapper<Candidate, CandidateLog>
    {
        protected override CandidateLog MappForeignKeys(DbContext context, Candidate entity, CandidateLog log)
        {
            return log;
        }
    }
}
