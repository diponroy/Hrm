﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CandidateReferenceMapper : LogMapper<CandidateReference, CandidateReferenceLog>
    {
        protected override CandidateReferenceLog MappForeignKeys(DbContext context, CandidateReference entity, CandidateReferenceLog log)
        {
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            return log;
        }
    }
}
