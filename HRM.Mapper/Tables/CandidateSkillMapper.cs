﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CandidateSkillMapper : LogMapper<CandidateSkill, CandidateSkillLog>
    {
        protected override CandidateSkillLog MappForeignKeys(DbContext context, CandidateSkill entity, CandidateSkillLog log)
        {
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            return log;
        }
    }
}
