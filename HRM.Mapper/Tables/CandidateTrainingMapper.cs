﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CandidateTrainingMapper : LogMapper<CandidateTraining, CandidateTrainingLog>
    {
        protected override CandidateTrainingLog MappForeignKeys(DbContext context, CandidateTraining entity, CandidateTrainingLog log)
        {
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            return log;
        }
    }
}
