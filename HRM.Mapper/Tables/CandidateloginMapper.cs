﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CandidateLoginMapper : LogMapper<CandidateLogin, CandidateLoginLog>
    {
        protected override CandidateLoginLog MappForeignKeys(DbContext context, CandidateLogin entity, CandidateLoginLog log)
        {
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            return log;
        }
    }
}
