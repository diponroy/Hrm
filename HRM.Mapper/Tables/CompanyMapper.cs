﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class CompanyMapper : LogMapper<Company, CompanyLog>
    {
        protected override CompanyLog MappForeignKeys(DbContext context, Company entity, CompanyLog log)
        {
            return log;
        }
    }
}
