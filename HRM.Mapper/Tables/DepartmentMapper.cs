﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class DepartmentMapper : LogMapper<Department, DepartmentLog>
    {
        protected override DepartmentLog MappForeignKeys(DbContext context, Department entity, DepartmentLog log)
        {
            return log;
        }
    }
}
