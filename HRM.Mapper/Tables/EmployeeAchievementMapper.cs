﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeAchievementMapper : LogMapper<EmployeeAchievement, EmployeeAchievementLog>
    {
        protected override EmployeeAchievementLog MappForeignKeys(DbContext context, EmployeeAchievement entity, EmployeeAchievementLog log)
        {
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);
            log.EmployeeAppraisalId = GetLastLogId<EmployeeLog>(entity.EmployeeAppraisalId);
            return log;
        }
    }
}
