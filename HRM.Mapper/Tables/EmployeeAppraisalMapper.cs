﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeAppraisalMapper : LogMapper<EmployeeAppraisal, EmployeeAppraisalLog>
    {
        protected override EmployeeAppraisalLog MappForeignKeys(DbContext context, EmployeeAppraisal entity, EmployeeAppraisalLog log)
        {
            log.EmployeeAssignedAppraisalId = GetLastLogId<EmployeeAssignedAppraisalLog>(entity.EmployeeAssignedAppraisalId);
            return log;
        }
    }
}
