﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeAssignedAppraisalMapper:LogMapper<EmployeeAssignedAppraisal,EmployeeAssignedAppraisalLog>
    {
        protected override EmployeeAssignedAppraisalLog MappForeignKeys(DbContext context, EmployeeAssignedAppraisal entity, EmployeeAssignedAppraisalLog log)
        {
            log.AppraisalIndicatorId = GetLastLogId<AppraisalIndicatorLog>(entity.AppraisalIndicatorId);
            log.EmployeeWorkStationUnitId = GetLastLogId<EmployeeWorkStationUnitLog>(entity.EmployeeWorkStationUnitId);
            return log;  
        }
    }
}
