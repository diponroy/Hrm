﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeAttachedTrainingMapper:LogMapper<EmployeeAttachedTraining, EmployeeAttachedTrainingLog>
    {
        protected override EmployeeAttachedTrainingLog MappForeignKeys(DbContext context, EmployeeAttachedTraining entity,
                                                                       EmployeeAttachedTrainingLog log)
        {
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);
            log.EmployeeTrainingId = GetLastLogId<EmployeeTrainingLog>(entity.EmployeeTrainingId);
            return log;
        }
    }
}
