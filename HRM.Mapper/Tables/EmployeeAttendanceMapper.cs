﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeAttendanceMapper:LogMapper<EmployeeAttendance,EmployeeAttendanceLog>
    {
        protected override EmployeeAttendanceLog MappForeignKeys(DbContext context, EmployeeAttendance entity, EmployeeAttendanceLog log)
        {
            log.EmployeeWorkStationUnitId = GetLastLogId<EmployeeWorkStationUnitLog>(entity.EmployeeWorkStationUnitId);
            return log;
        }
    }
}
