﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeDisciplineMapper : LogMapper<EmployeeDiscipline, EmployeeDisciplineLog>
    {
        protected override EmployeeDisciplineLog MappForeignKeys(DbContext context, EmployeeDiscipline entity, EmployeeDisciplineLog log)
        {
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);
            log.DepartmentId = GetLastLogId<DepartmentLog>(entity.DepartmentId);
            return log;
        }
    }
}
