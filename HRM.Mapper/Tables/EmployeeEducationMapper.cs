﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeEducationMapper : LogMapper<EmployeeEducation, EmployeeEducationLog>
    {
        protected override EmployeeEducationLog MappForeignKeys(DbContext context, EmployeeEducation entity, EmployeeEducationLog log)
        {
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);
            return log;
        }
    }
}
