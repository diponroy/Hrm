﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeLoginMapper : LogMapper<EmployeeLogin, EmployeeLoginLog>
    {
        protected override EmployeeLoginLog MappForeignKeys(DbContext context, EmployeeLogin entity, EmployeeLoginLog log)
        {
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);
            return log;
        }
    }
}
