﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeMapper : LogMapper<Employee, EmployeeLog>
    {
        protected override EmployeeLog MappForeignKeys(DbContext context, Employee entity, EmployeeLog log)
        {
            return log;
        }
    }
}
