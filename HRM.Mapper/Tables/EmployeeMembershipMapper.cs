﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeMembershipMapper : LogMapper<EmployeeMembership, EmployeeMembershipLog>
    {
        protected override EmployeeMembershipLog MappForeignKeys(DbContext context, EmployeeMembership entity, EmployeeMembershipLog log)
        {
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);

            return log;
        }
    }
}
