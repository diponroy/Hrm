﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeePayrollInfoMapper : LogMapper<EmployeePayrollInfo, EmployeePayrollInfoLog>
    {
        protected override EmployeePayrollInfoLog MappForeignKeys(DbContext context, EmployeePayrollInfo entity, EmployeePayrollInfoLog log)
        {
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);
            return log;
        }
    }
}
