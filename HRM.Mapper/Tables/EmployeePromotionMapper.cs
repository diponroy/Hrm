﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeePromotionMapper : LogMapper<EmployeePromotion, EmployeePromotionLog>
    {
        protected override EmployeePromotionLog MappForeignKeys(DbContext context, EmployeePromotion entity, EmployeePromotionLog log)
        {
            log.EmployeeWorkStationUnitId = GetLastLogId<EmployeeWorkStationUnitLog>(entity.EmployeeWorkStationUnitId);
            log.EmployeeTypeId = FindLastLogId<EmployeeTypeLog>(entity.EmployeeTypeId);
            log.WorkStationUnitId = FindLastLogId<WorkStationUnitLog>(entity.WorkStationUnitId);
            log.IncrementSalaryStructureId = FindLastLogId<IncrementSalaryStructureLog>(entity.IncrementSalaryStructureId);
            return log;
        }
    }
}
