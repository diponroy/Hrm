﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeReportingMapper : LogMapper<EmployeeReporting, EmployeeReportingLog>
    {
        protected override EmployeeReportingLog MappForeignKeys(DbContext context, EmployeeReporting entity, EmployeeReportingLog log)
        {
            log.EmployeeWorkStationUnitId = GetLastLogId<EmployeeWorkStationUnitLog>(entity.EmployeeWorkStationUnitId);
            log.ParentEmployeeWorkStationUnitId = GetLastLogId<EmployeeWorkStationUnitLog>(entity.ParentEmployeeWorkStationUnitId);
            return log;
        }
    }
}
