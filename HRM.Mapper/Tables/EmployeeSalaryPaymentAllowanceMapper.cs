﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeSalaryPaymentAllowanceMapper : LogMapper<EmployeeSalaryPaymentAllowance, EmployeeSalaryPaymentAllowanceLog>
    {
        protected override EmployeeSalaryPaymentAllowanceLog MappForeignKeys(DbContext context, EmployeeSalaryPaymentAllowance entity,
                                                                             EmployeeSalaryPaymentAllowanceLog log)
        {
            log.EmployeeSalaryPaymentId = GetLastLogId<EmployeeSalaryPaymentLog>(entity.EmployeeSalaryPaymentId);
            log.EmployeeSalaryStructureAllowanceId = GetLastLogId<EmployeeSalaryStructureAllowanceLog>(entity.EmployeeSalaryStructureAllowanceId);
            return log;
        }
    }
}
