﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeSalaryPaymentBonusMapper : LogMapper<EmployeeSalaryPaymentBonus, EmployeeSalaryPaymentBonusLog>
    {
        protected override EmployeeSalaryPaymentBonusLog MappForeignKeys(DbContext context, EmployeeSalaryPaymentBonus entity, EmployeeSalaryPaymentBonusLog log)
        {
            log.EmployeeSalaryPaymentId = GetLastLogId<EmployeeSalaryPaymentLog>(entity.EmployeeSalaryPaymentId);
            log.EmployeeSalaryStructureBonusId = GetLastLogId<EmployeeSalaryStructureBonusLog>(entity.EmployeeSalaryStructureBonusId);
            return log;
        }
    }
}
