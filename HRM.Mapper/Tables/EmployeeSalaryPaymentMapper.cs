﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeSalaryPaymentMapper :LogMapper<EmployeeSalaryPayment, EmployeeSalaryPaymentLog>
    {
        protected override EmployeeSalaryPaymentLog MappForeignKeys(DbContext context, EmployeeSalaryPayment entity, EmployeeSalaryPaymentLog log)
        {
            log.EmployeeSalaryStructureId = GetLastLogId<EmployeeSalaryStructureLog>(entity.EmployeeSalaryStructureId);
            log.EmployeePayrollInfoId = GetLastLogId<EmployeePayrollInfoLog>(entity.EmployeePayrollInfoId);
            return log;
        }
    }
}
