﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeSalaryStructureAllowanceMapper : LogMapper<EmployeeSalaryStructureAllowance, EmployeeSalaryStructureAllowanceLog>
    {
        protected override EmployeeSalaryStructureAllowanceLog MappForeignKeys(DbContext context, EmployeeSalaryStructureAllowance entity, EmployeeSalaryStructureAllowanceLog log)
        {
            log.EmployeeSalaryStructureId = GetLastLogId<EmployeeSalaryStructureLog>(entity.EmployeeSalaryStructureId);
            log.AllowanceId = GetLastLogId<AllowanceLog>(entity.AllowanceId);
            return log;  
        }
    }
}
