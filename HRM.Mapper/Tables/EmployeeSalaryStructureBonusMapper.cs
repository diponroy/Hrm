﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeSalaryStructureBonusMapper : LogMapper<EmployeeSalaryStructureBonus, EmployeeSalaryStructureBonusLog>
    {
        protected override EmployeeSalaryStructureBonusLog MappForeignKeys(DbContext context, EmployeeSalaryStructureBonus entity, EmployeeSalaryStructureBonusLog log)
        {
            log.EmployeeSalaryStructureId = GetLastLogId<EmployeeSalaryStructureLog>(entity.EmployeeSalaryStructureId);
            log.BonusId = GetLastLogId<BonusLog>(entity.BonusId);
            return log;
        }
    }
}
