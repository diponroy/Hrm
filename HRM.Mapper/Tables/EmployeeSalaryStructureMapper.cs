﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeSalaryStructureMapper:LogMapper<EmployeeSalaryStructure,EmployeeSalaryStructureLog>
    {
        protected override EmployeeSalaryStructureLog MappForeignKeys ( DbContext context, EmployeeSalaryStructure entity, EmployeeSalaryStructureLog log )
        {
            log.EmployeeWorkstationUnitId = GetLastLogId<EmployeeWorkStationUnitLog>(entity.EmployeeWorkstationUnitId);
            log.SalaryStructureId = GetLastLogId<SalaryStructureLog>(entity.SalaryStructureId);
            return log;
        }
    }
}
