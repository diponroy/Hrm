﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeTrainingAttachmentMapper : LogMapper<EmployeeTrainingAttachment, EmployeeTrainingAttachmentLog>
    {
        protected override EmployeeTrainingAttachmentLog MappForeignKeys(DbContext context, EmployeeTrainingAttachment entity, EmployeeTrainingAttachmentLog log)
        {
            log.EmployeeTrainingId = GetLastLogId<EmployeeTrainingLog>(entity.EmployeeTrainingId);
            return log;
        }
    }
}
