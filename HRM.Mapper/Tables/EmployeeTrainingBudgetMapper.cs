﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeTrainingBudgetMapper : LogMapper<EmployeeTrainingBudget, EmployeeTrainingBudgetLog>
    {
        protected override EmployeeTrainingBudgetLog MappForeignKeys(DbContext context, EmployeeTrainingBudget entity,
                                                                     EmployeeTrainingBudgetLog log)
        {
            log.EmployeeTrainingId = GetLastLogId<EmployeeTrainingLog>(entity.EmployeeTrainingId);
            return log;
        }
    }
}
