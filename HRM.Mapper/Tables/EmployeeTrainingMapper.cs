﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeTrainingMapper:LogMapper<EmployeeTraining,EmployeeTrainingLog>
    {
        protected override EmployeeTrainingLog MappForeignKeys(DbContext context, EmployeeTraining entity, EmployeeTrainingLog log)
        {
            return log;
        }
    }
}
