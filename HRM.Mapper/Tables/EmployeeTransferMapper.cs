﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeTransferMapper:LogMapper<EmployeeTransfer, EmployeeTransferLog>
    {
        protected override EmployeeTransferLog MappForeignKeys(DbContext context, EmployeeTransfer entity, EmployeeTransferLog log)
        {
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);
            log.EmployeeTypeId = GetLastLogId<EmployeeTypeLog>(entity.EmployeeTypeId);
            log.WorkStationUnitId = GetLastLogId<WorkStationUnitLog>(entity.WorkStationUnitId);
            log.IncrementSalaryStructureId = FindLastLogId<IncrementSalaryStructureLog>(entity.IncrementSalaryStructureId);
            return log;
        }
    }
}
