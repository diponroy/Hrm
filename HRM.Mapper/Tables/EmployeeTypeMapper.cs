﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeTypeMapper : LogMapper<EmployeeType, EmployeeTypeLog>
    {
        protected override EmployeeTypeLog MappForeignKeys(DbContext context, EmployeeType entity, EmployeeTypeLog log)
        {
            log.ParentId = FindLastLogId<EmployeeTypeLog>(entity.ParentId);
            return log;
        }
    }
}
