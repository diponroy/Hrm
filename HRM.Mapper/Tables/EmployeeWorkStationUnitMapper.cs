﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class EmployeeWorkStationUnitMapper : LogMapper<EmployeeWorkStationUnit, EmployeeWorkStationUnitLog>
    {
        protected override EmployeeWorkStationUnitLog MappForeignKeys(DbContext context, EmployeeWorkStationUnit entity,
                                                                      EmployeeWorkStationUnitLog log)
        {
            log.EmployeeId = GetLastLogId<EmployeeLog>(log.EmployeeId);
            log.EmployeeTypeId = GetLastLogId<EmployeeTypeLog>(log.EmployeeTypeId);
            log.WorkStationUnitId = GetLastLogId<WorkStationUnitLog>(log.WorkStationUnitId);
            return log;
        }
    }
}
