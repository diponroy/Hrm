﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class IncrementSalaryStructureAllowanceMapper : LogMapper<IncrementSalaryStructureAllowance, IncrementSalaryStructureAllowanceLog>
    {
        protected override IncrementSalaryStructureAllowanceLog MappForeignKeys(DbContext context, IncrementSalaryStructureAllowance entity,
            IncrementSalaryStructureAllowanceLog log)
        {
            log.AllowanceId = GetLastLogId<AllowanceLog>(entity.AllowanceId);
            log.IncrementSalaryStructureId = GetLastLogId<IncrementSalaryStructureLog>(entity.IncrementSalaryStructureId);
            return log;
        }
    }
}
