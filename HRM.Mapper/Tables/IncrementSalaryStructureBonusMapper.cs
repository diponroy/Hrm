﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class IncrementSalaryStructureBonusMapper : LogMapper<IncrementSalaryStructureBonus, IncrementSalaryStructureBonusLog>
    {
        protected override IncrementSalaryStructureBonusLog MappForeignKeys(DbContext context, IncrementSalaryStructureBonus entity,
            IncrementSalaryStructureBonusLog log)
        {
            log.BonusId = GetLastLogId<BonusLog>(entity.BonusId);
            log.IncrementSalaryStructureId = GetLastLogId<IncrementSalaryStructureLog>(entity.IncrementSalaryStructureId);
            return log;
        }
    }
}
