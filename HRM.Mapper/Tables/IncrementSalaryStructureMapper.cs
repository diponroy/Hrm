﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class IncrementSalaryStructureMapper : LogMapper<IncrementSalaryStructure, IncrementSalaryStructureLog>
    {
        protected override IncrementSalaryStructureLog MappForeignKeys(DbContext context, IncrementSalaryStructure entity, IncrementSalaryStructureLog log)
        {
            log.EmployeeSalaryStructureId = FindLastLogId<EmployeeSalaryStructureLog>(entity.EmployeeSalaryStructureId);
            return log;
        }
    }
}
