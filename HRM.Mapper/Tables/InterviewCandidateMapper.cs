﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class InterviewCandidateMapper:LogMapper<InterviewCandidate,InterviewCandidateLog>
    {
        protected override InterviewCandidateLog MappForeignKeys(DbContext context, InterviewCandidate entity, InterviewCandidateLog log)
        {
            log.InterviewId = GetLastLogId<InterviewLog>(entity.InterviewId);
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            return log;
        }
    }
}
