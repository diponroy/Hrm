﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class InterviewCoordinatorMapper : LogMapper<InterviewCoordinator,InterviewCoordinatorLog>
    {
        protected override InterviewCoordinatorLog MappForeignKeys(DbContext context, InterviewCoordinator entity, InterviewCoordinatorLog log)
        {
            log.InterviewId = GetLastLogId<InterviewLog>(entity.InterviewId);
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);
            return log;
        }
    }
}
