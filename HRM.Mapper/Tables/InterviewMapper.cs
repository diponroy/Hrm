﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper
{
    public class InterviewMapper:LogMapper<Interview,InterviewLog>
    {
        protected override InterviewLog MappForeignKeys(DbContext context, Interview entity, InterviewLog log)
        {
            log.ManpowerVacancyNoticeId = GetLastLogId<ManpowerVacancyNoticeLog>(entity.ManpowerVacancyNoticeId);
            return log;
        }
    }
}
