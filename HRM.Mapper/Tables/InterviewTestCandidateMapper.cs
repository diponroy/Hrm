﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class InterviewTestCandidateMapper:LogMapper<InterviewTestCandidate,InterviewTestCandidateLog>
    {
        protected override InterviewTestCandidateLog MappForeignKeys(DbContext context, InterviewTestCandidate entity,
                                                                     InterviewTestCandidateLog log)
        {
            log.InterviewTestId = GetLastLogId<InterviewTestLog>(entity.InterviewTestId);
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            return log;
        }
    }
}
