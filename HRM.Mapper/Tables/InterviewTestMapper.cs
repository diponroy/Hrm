﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class InterviewTestMapper : LogMapper<InterviewTest, InterviewTestLog>
    {
        protected override InterviewTestLog MappForeignKeys(DbContext context, InterviewTest entity, InterviewTestLog log)
        {
            log.InterviewId = GetLastLogId<InterviewLog>(entity.InterviewId);
            return log;
        }
    }
}
