﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class ManpowerCandidateReferenceMapper : LogMapper<ManpowerCandidateReference, ManpowerCandidateReferenceLog>
    {
        protected override ManpowerCandidateReferenceLog MappForeignKeys(DbContext context, ManpowerCandidateReference entity,
            ManpowerCandidateReferenceLog log)
        {
            log.ManpowerVacancyNoticeCandidateId = GetLastLogId<ManpowerVacancyNoticeCandidateLog>(entity.ManpowerVacancyNoticeCandidateId);
            log.EmployeeId = GetLastLogId<EmployeeLog>(entity.EmployeeId);
            return log;
        }
    }
}
