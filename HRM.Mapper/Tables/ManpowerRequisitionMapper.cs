﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class ManpowerRequisitionMapper : LogMapper<ManpowerRequisition, ManpowerRequisitionLog>
    {
        protected override ManpowerRequisitionLog MappForeignKeys(DbContext context, ManpowerRequisition entity, ManpowerRequisitionLog log)
        {
            return log;
        }
    }
}
