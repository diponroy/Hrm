﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class ManpowerVacancyNoticeCandidateMapper : LogMapper<ManpowerVacancyNoticeCandidate, ManpowerVacancyNoticeCandidateLog>
    {
        protected override ManpowerVacancyNoticeCandidateLog MappForeignKeys(DbContext context, ManpowerVacancyNoticeCandidate entity,
            ManpowerVacancyNoticeCandidateLog log)
        {
            log.ManpowerVacancyNoticeId = GetLastLogId<ManpowerVacancyNoticeLog>(entity.ManpowerVacancyNoticeId);
            log.CandidateId = GetLastLogId<CandidateLog>(entity.CandidateId);
            log.EmployeeId = FindLastLogId<EmployeeLog>(entity.EmployeeId);
            return log;
        }
    }
}
