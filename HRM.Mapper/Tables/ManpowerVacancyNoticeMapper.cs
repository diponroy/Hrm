﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class ManpowerVacancyNoticeMapper : LogMapper<ManpowerVacancyNotice, ManpowerVacancyNoticeLog>
    {
        protected override ManpowerVacancyNoticeLog MappForeignKeys(DbContext context, ManpowerVacancyNotice entity, ManpowerVacancyNoticeLog log)
        {
            log.ManpowerRequisitionId = GetLastLogId<ManpowerRequisitionLog>(entity.ManpowerRequisitionId);
            return log;
        }
    }
}
