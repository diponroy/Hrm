﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class SalaryStructureAllowanceMapper : LogMapper<SalaryStructureAllowance, SalaryStructureAllowanceLog>
    {
        protected override SalaryStructureAllowanceLog MappForeignKeys(DbContext context, SalaryStructureAllowance entity, SalaryStructureAllowanceLog log)
        {
            log.SalaryStructureId = GetLastLogId<SalaryStructureLog>(log.SalaryStructureId);
            log.AllowanceId = GetLastLogId<AllowanceLog>(log.AllowanceId);
            return log;
        }
    }
}
