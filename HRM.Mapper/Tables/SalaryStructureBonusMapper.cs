﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class SalaryStructureBonusMapper : LogMapper<SalaryStructureBonus, SalaryStructureBonusLog>
    {
        protected override SalaryStructureBonusLog MappForeignKeys(DbContext context, SalaryStructureBonus entity,SalaryStructureBonusLog log)
        {
            log.SalaryStructureId = GetLastLogId<SalaryStructureLog>(log.SalaryStructureId);
            log.BonusId = GetLastLogId<BonusLog>(log.BonusId);
            return log;
        }
    }
}
