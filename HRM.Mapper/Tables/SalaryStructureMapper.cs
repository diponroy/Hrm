﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class SalaryStructureMapper : LogMapper<SalaryStructure, SalaryStructureLog>
    {
        protected override SalaryStructureLog MappForeignKeys(DbContext context, SalaryStructure entity, SalaryStructureLog log)
        {
            return log;
        }
    }
}
