﻿using System.Data.Entity;
using System.Linq;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.Log;
using HRM.Utility;

namespace HRM.Mapper.Tables.Shared
{
    public abstract class LogMapper<TEntity, TLog>
        where TEntity : class, IEntityWithLog<TLog>, IAffectedByTrack
        where TLog : class, IEntityLog<TEntity>, new()
    {

        private DbContext _context;

        private void SetContext(DbContext context)
        {
            _context = context;
        }

        protected long GetLastLogId<TSource>(long? id) where TSource : class, ILogPrimaryKeyTrack, IPrimaryKeyTrack
        {
            return _context.Set<TSource>().Where(x => x.Id == id).OrderByDescending(x => x.LogId).First().LogId;
        }
        protected long? FindLastLogId<TSource>(long? id) where TSource : class, ILogPrimaryKeyTrack, IPrimaryKeyTrack
        {
            if (id == null)
            {
                return null;
            }
            var logId = GetLastLogId<TSource>((long)id);
            return logId;
        }

        public TLog FullMapped(DbContext context, TEntity entity)
        {
            SetContext(context);
            TLog log = entity.CreateMappedWithoutVirtuals<TEntity, TLog>();
            return MappForeignKey(_context, entity, log);
        }
        public TLog MappForeignKey(DbContext context, TEntity entity, TLog log)
        {
            SetContext(context);
            MappForeignKeys(_context, entity, log);
            log.AffectedByEmployeeId = FindLastLogId<EmployeeLog>(entity.AffectedByEmployeeId);
            return log;
        }
        protected abstract TLog MappForeignKeys(DbContext context, TEntity entity, TLog log);
    }
}
