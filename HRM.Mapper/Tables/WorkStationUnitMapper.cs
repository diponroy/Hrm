﻿using System.Data.Entity;
using HRM.Mapper.Tables.Shared;
using HRM.Model.Table;
using HRM.Model.Table.Log;

namespace HRM.Mapper.Tables
{
    public class WorkStationUnitMapper : LogMapper<WorkStationUnit, WorkStationUnitLog>
    {
        protected override WorkStationUnitLog MappForeignKeys(DbContext context, WorkStationUnit entity, WorkStationUnitLog log)
        {
            log.CompanyId = FindLastLogId<CompanyLog>(entity.CompanyId);
            log.BranchId = FindLastLogId<BranchLog>(entity.BranchId);
            log.DepartmentId = FindLastLogId<DepartmentLog>(entity.DepartmentId);
            log.ParentId = FindLastLogId<WorkStationUnitLog>(entity.ParentId);
            return log;
        }
    }
}
