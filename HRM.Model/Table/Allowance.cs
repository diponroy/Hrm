﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class Allowance : IAllowance, IEntityWithLog<AllowanceLog>
    {
        public long Id { get; set; }
        public long AllowanceTypeId { get; set; }
        public string Title { get; set; }
        public bool? AsPercentage { get; set; }
        public float Weight { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }

        public virtual AllowanceType AllowanceType { get; set; }

        public virtual ICollection<EmployeeSalaryStructureAllowance> EmployeeSalaryStructureAllowances { get; set; }
        public virtual ICollection<SalaryStructureAllowance> SalaryStructureAllowances { get; set; }

        public virtual ICollection<AllowanceLog> Logs { get; set; }
    }
}
