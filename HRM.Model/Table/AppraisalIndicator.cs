﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class AppraisalIndicator : IAppraisalIndicator, IEntityWithLog<AppraisalIndicatorLog>
    {
        public long Id { get; set; }
        
        public string Title { get; set; }
        public decimal MaxWeight { get; set; }
        public string Remarks { get; set; }
        public long? ParentId { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } } 
       
        public virtual AppraisalIndicator ParentAppraisalIndicator { get; set; }
        public virtual List<AppraisalIndicator> ChildAppraisalIndicators { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }

        public virtual ICollection<AppraisalIndicatorLog> Logs { get; set; }
    }
}
