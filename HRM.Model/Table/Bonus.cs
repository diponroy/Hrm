﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class Bonus : IBonus, IEntityWithLog<BonusLog>
    {
        public long Id { get; set; }
        public long BonusTypeId { get; set; }
        public string Title { get; set; }
        public bool? AsPercentage { get; set; }
        public float Weight { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }

        public virtual BonusType BonusType { get; set; }
        public virtual ICollection<BonusPaymentDate> PaymentDates { get; set; }
        public virtual ICollection<SalaryStructureBonus> SalaryStructureBonuses { get; set; }
        public virtual ICollection<EmployeeSalaryStructureBonus> EmployeeSalaryStructureBonuses { get; set; }
        public virtual ICollection<BonusLog> Logs { get; set; }
    }
}
