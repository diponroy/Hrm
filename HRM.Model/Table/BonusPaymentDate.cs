﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class BonusPaymentDate : IBonusPaymentDate, IEntityWithLog<BonusPaymentDateLog>
    {
        public long Id { get; set; }
        public long BonusId { get; set; }
        public DateTime EstimatedDate { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }

        public virtual Bonus Bonus { get; set; }
        public virtual ICollection<BonusPaymentDateLog> Logs { get; set; }
    }
}
