﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class BonusType : IBonusType, IEntityWithLog<BonusTypeLog>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Remarks { get ; set ; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual ICollection<Bonus> Bonuses { get; set; }

        public virtual ICollection<BonusTypeLog> Logs { get; set; }
    }
}
