﻿using System;
using System.Collections.Generic;
using HRM.Model.Table.IEntity.Shared ;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class Candidate : ICandidate , IEntityWithLog<CandidateLog>
    {
        public long Id { get ; set ; }
        public string TrackNo { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FathersName { get; set; }
        public string MothersName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string BloodGroup { get; set; }
        public string MaritalStatus { get; set; }
        public string Nationality { get; set; }
        public string NationalId { get; set; }
        public string PassportNo { get; set; }
        public string Religion { get; set; }
        public string Email { get; set; }
        public string AlternateEmailAddress { get; set; }
        public string ContactNo { get; set; }
        public string HomePhone { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string DivisionOrState { get; set; }
        public string City { get; set; }
        public string ImageDirectory { get; set; }
        public string CvDirectory { get; set; } 

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public virtual List<CandidateEducation> CandidateEducations { get; set; } 
        public virtual List<CandidateEmployment> CandidateEmployments { get; set; }
        public virtual List<CandidateLanguage> CandidateLanguages { get; set; }
        public virtual List<CandidateReference> CandidateReferences { get; set; }
        public virtual List<CandidateSkill> CandidateSkills { get; set; }
        public virtual List<CandidateTraining> CandidateTrainings { get; set; }
        public virtual List<CandidateLogin> CandidateLogins { get; set; }
        public long? AffectedByEmployeeId { get ; set ; }
        public DateTime? AffectedDateTime { get ; set ; }

        public ICollection<CandidateLog> Logs { get ; set ; }
    }
}
