﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class CandidateEducation : ICandidateEducation, IEntityWithLog<CandidateEducationLog>
    {
        public long Id { get; set; }
        
        public string Title { get; set; }
        public string Institution { get; set; }
        public DateTime? DurationFromDate { get; set; }
        public DateTime? DurationToDate { get; set; }
        public string Result { get; set; }
        public string Major { get; set; }
        public string AttachmentDirectory { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public long CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }
        public virtual ICollection<CandidateEducationLog> Logs { get; set; }
    }
}
