﻿using System;
using System.Collections.Generic;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class CandidateEmployment : ICandidateEmployment, IEntityWithLog<CandidateEmploymentLog>
    {
        public long Id { get; set; }
        public string Institution { get; set; }
        public string Address { get; set; }
        public string Designation { get; set; }
        public string Responsibility { get; set; }
        public DateTime? DurationFromDate { get; set; }
        public DateTime? DurationToDate { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public long CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }

        public virtual List<CandidateAchievement> CandidateAchievements { get; set; }
        public virtual ICollection<CandidateEmploymentLog> Logs { get; set; }
    }
}