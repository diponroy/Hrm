﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class CandidateLanguage : ICandidateLanguage, IEntityWithLog<CandidateLanguageLog>
    {
        public long Id { get; set; }
        
        public string LanguageName { get; set; }
        public string Reading { get; set; }
        public string Writing { get; set; }
        public string Speaking { get; set; }
        public string Listening { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public long CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }
        public virtual ICollection<CandidateLanguageLog> Logs { get; set; }
    }
}