﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class CandidateLogin : ICandidateLogin, IEntityWithLog<CandidateLoginLog>
    {
        public long Id { get; set; }
        
        public string LoginName { get; set; }
        public string Password { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public long CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }
        public virtual ICollection<CandidateLoginLog> Logs { get; set; }
    }
}
