﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class CandidateReference : ICandidateReference, IEntityWithLog<CandidateReferenceLog>
    {
        public long Id { get; set; }
        
        public string Referrer { get; set; }
        public string Designation { get; set; }
        public string CompanyOrOrg { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Relation { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public long CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }
        public virtual ICollection<CandidateReferenceLog> Logs { get; set; }
    }
}
