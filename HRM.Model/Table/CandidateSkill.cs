﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class CandidateSkill : ICandidateSkill, IEntityWithLog<CandidateSkillLog>
    {
        public long Id { get; set; }
        
        public string Title { get; set; }
        public string Description { get; set; }
        public string AttachmentDirectory { get; set; }
        public string Type { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long CandidateId { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual Candidate Candidate { get; set; }
        public virtual ICollection<CandidateSkillLog> Logs { get; set; }
    }
}
