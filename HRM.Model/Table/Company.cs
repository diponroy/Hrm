﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared ;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log ;

namespace HRM.Model.Table
{
    public class Company : ICompany , IEntityWithLog<CompanyLog>
    {
        public long Id { get ; set ; }
        public string Name { get ; set ; }
        public string Address { get; set; }
        public string Remarks { get; set; }
        public DateTime? DateOfCreation { get; set; }
        public DateTime? DateOfClosing { get; set; }
        public bool IsClosed { get { return DateOfClosing != null; } }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return this.Status.ToString(); } }

        public virtual List<WorkStationUnit> WorkStationUnits { set; get; }
        public long? AffectedByEmployeeId { get ; set ; }
        public DateTime? AffectedDateTime { get ; set ; }
        public virtual ICollection<CompanyLog> Logs { get ; set ; }
    }

}
