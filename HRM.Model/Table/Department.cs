﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class Department : IDepartment, IEntityWithLog<DepartmentLog>
    {
        public long Id { get ; set ; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public DateTime? DateOfCreation { get; set; }
        public DateTime? DateOfClosing { get; set; }
        public bool IsClosed { get { return DateOfClosing != null; } }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public virtual List<WorkStationUnit> WorkStationUnits { set; get; }
        public virtual List<EmployeeDiscipline> EmployeeDisciplines { set; get; }

        public virtual ICollection<DepartmentLog> Logs { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
    }
}
