﻿using System;
using System.Collections.Generic;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class Employee : IEmployee, IEntityWithLog<EmployeeLog>
    {
        public long Id { get; set; }
        public string TrackNo { get; set; }

        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName
        {
            get { return string.Format("{0} {1} {2}", Salutation, FirstName, LastName); }
        }

        public string FathersName { get; set; }
        public string MothersName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string BloodGroup { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhone { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string DivisionOrState { get; set; }
        public string City { get; set; }
        public string ImageDirectory { get; set; }


        public EntityStatusEnum? Status { get; set; }

        public string StatusString
        {
            get { return Status.ToString(); }
        }

        public long? CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }

        public virtual ICollection<EmployeeLogin> EmployeeLogin { get; set; }
        public virtual ICollection<EmployeeEducation> EmployeeEducations { get; set; }
        public virtual ICollection<EmployeeMembership> EmployeeMemberships { get; set; }
        public virtual ICollection<EmployeeAchievement> EmployeeAchievements { get; set; }
        public virtual ICollection<EmployeePayrollInfo> EmployeePayrollInfos { get; set; }
        public virtual ICollection<EmployeeAttachedTraining> EmployeeAttachedTrainings { get; set; }
        public virtual ICollection<EmployeeWorkStationUnit> EmployeeWorkStationUnits { get; set; }
        public virtual ICollection<EmployeeTransfer> EmployeeTransfers { get; set; } 
        public virtual ICollection<EmployeeDiscipline> EmployeeDisciplines { get; set; }
        public virtual ICollection<InterviewCoordinator> InterviewCoordinators { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual ICollection<EmployeeLog> Logs { get; set; }
    }
}
