﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.IEntity ;
using HRM.Model.Table.IEntity.Shared ;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Log ;

namespace HRM.Model.Table
{
    public class EmployeeAchievement : IEmployeeAchievement, IEntityWithLog<EmployeeAchievementLog>
    {
        public long Id { get ; set ; }
        public string Title { get ; set ; }
        public string Remarks { get ; set ; }

        public long EmployeeId { get ; set ; }
        public virtual Employee Employee { get; set; }

        public long EmployeeAppraisalId { get ; set ; }
        public virtual EmployeeAppraisal EmployeeAppraisal { get; set; }

        public string AttachmentDirectory { get ; set ; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }

        public EntityStatusEnum? Status { get; set; }

        public string StatusString
        {
            get { return Status.ToString(); }
        }

        public virtual ICollection<EmployeeAchievementLog> Logs { get ; set ; }
    }
}
