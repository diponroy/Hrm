﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeAppraisal : IEmployeeAppraisal, IEntityWithLog<EmployeeAppraisalLog>
    {
        public long Id { get; set; }

        public decimal Weight { get; set; }
        public string Remarks { get; set; }  
        public long EmployeeAssignedAppraisalId { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        
        public virtual List<EmployeeAchievement> EmployeeAchievements { get; set; }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual EmployeeAssignedAppraisal EmployeeAssignedAppraisal { get; set; }
        public virtual ICollection<EmployeeAppraisalLog> Logs { get; set; }
    }
}
