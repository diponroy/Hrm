﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeAttachedTraining : IEmployeeAttachedTraining, IEntityWithLog<EmployeeAttachedTrainingLog>
    {
        public long Id { get; set; }
        public string Remarks { get; set; } 
        public long EmployeeId { get; set; }
        public long EmployeeTrainingId { get; set; }

        public virtual Employee Employee { get; set; } 
        public virtual EmployeeTraining EmployeeTraining { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; } 
        public DateTime? AffectedDateTime { get; set; }
        
        public virtual ICollection<EmployeeAttachedTrainingLog> Logs { get; set; }
    }
}
