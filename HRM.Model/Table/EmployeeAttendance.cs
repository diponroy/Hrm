﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeAttendance : IEmployeeAttendance, IEntityWithLog<EmployeeAttendanceLog>
    {
        public long Id { get; set; }
        public long EmployeeWorkStationUnitId { get; set; }
        public string AttendanceTime { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string Remarks { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public virtual EmployeeWorkStationUnit EmployeeWorkStationUnit { get; set; }

        public ICollection<EmployeeAttendanceLog> Logs { get; set; }
    }
}
