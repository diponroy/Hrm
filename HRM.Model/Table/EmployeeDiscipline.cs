﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeDiscipline : IEmployeeDiscipline, IEntityWithLog<EmployeeDisciplineLog>
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }    
        public string Title { get; set; }
        public DateTime DateOfCreation { get; set; }
        public long DepartmentId { get; set; }
        public string Description { get; set; }
        public string ActionTaken { get; set; }
        public string AttachmentDirectory { get; set; }
        public string Remarks { get; set; }  


        public virtual Employee Employee { get; set; }
        public virtual Department Department { get; set; }  


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }


        public ICollection<EmployeeDisciplineLog> Logs { get; set; }
    }
}
