﻿using System;
using System.Collections.Generic;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeLogin : IEmployeeLogin, IEntityWithLog<EmployeeLoginLog>
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public EntityStatusEnum? Status { get; set; }

        public string StatusString { get { return Status.ToString(); } }
        public virtual Employee Employee { get; set; }
        public virtual ICollection<EmployeeLoginLog> Logs { get; set; }
    }
}
