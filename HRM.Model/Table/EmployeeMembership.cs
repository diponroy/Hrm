﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{

    public class EmployeeMembership : IEmployeeMembership, IEntityWithLog<EmployeeMembershipLog>

    {
        public long Id { get; set; }
        
        public string Organization { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Remarks { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long EmployeeId { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }

        public virtual Employee Employee { get; set; }

        //public virtual Employee EmployeeAppraisal { get; set; }

        public virtual ICollection<EmployeeMembershipLog> Logs { get; set; }

    }
}
