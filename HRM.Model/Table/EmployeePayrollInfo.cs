﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeePayrollInfo : IEmployeePayrollInfo, IEntityWithLog<EmployeePayrollInfoLog>
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string BankName { get; set; }
        public string BankDetail { get; set; }
        public string AccountNo { get; set; }
        public long TinNumber { get; set; }
        public string Remarks { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public virtual Employee Employee { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual ICollection<EmployeeSalaryPayment> SalaryPayments { get; set; }
        public virtual ICollection<EmployeePayrollInfoLog> Logs { get; set; }
    }
}
