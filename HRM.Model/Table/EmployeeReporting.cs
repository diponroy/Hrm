﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeReporting : IEmployeeReporting, IEntityWithLog<EmployeeReportingLog>
    {
        public long Id { get; set; }
        public DateTime? AttachmentDate { get; set; }
        public DateTime? DetachmentDate { get; set; }
        public bool IsDetached { get { return DetachmentDate != null; } }

        public string Remarks { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long EmployeeWorkStationUnitId { get; set; }
        public virtual EmployeeWorkStationUnit EmployeeWorkStationUnit { get; set; }
        public long? ParentEmployeeWorkStationUnitId { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual EmployeeWorkStationUnit ParentEmployeeWorkStationUnit { get; set; }
        public virtual ICollection<EmployeeReportingLog> Logs { get; set; }
    }
}
