﻿using System;
using System.Collections.Generic;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeSalaryPayment : IEmployeeSalaryPayment, IEntityWithLog<EmployeeSalaryPaymentLog>
    {
        public long Id { get; set; }
        public string TrackNo { get; set; }
        public long EmployeeSalaryStructureId { get; set; }
        public DateTime DateOfPayment { get; set; }
        public long EmployeePayrollInfoId { get; set; }
        public float? TotalInAmount { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }


        public virtual EmployeeSalaryStructure EmployeeSalaryStructure { get; set; }
        public virtual EmployeePayrollInfo EmployeePayrollInfo { get; set; }
        public virtual ICollection<EmployeeSalaryPaymentBonus> BonusPayments { get; set; }
        public virtual ICollection<EmployeeSalaryPaymentAllowance> AllowancePayments { get; set; }
        public virtual ICollection<EmployeeSalaryPaymentLog> Logs { get; set; }
    }
}
