﻿using System;
using System.Collections.Generic;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeSalaryPaymentBonus : IEmployeeSalaryPaymentBonus, IEntityWithLog<EmployeeSalaryPaymentBonusLog>
    {
        public long Id { get; set; }
        public long EmployeeSalaryPaymentId { get; set; }
        public long EmployeeSalaryStructureBonusId { get; set; }
        public float? InAmount { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }


        public virtual EmployeeSalaryPayment EmployeeSalaryPayment { get; set; }
        public virtual EmployeeSalaryStructureBonus EmployeeSalaryStructureBonus { get; set; }
        public virtual ICollection<EmployeeSalaryPaymentBonusLog> Logs { get; set; }
    }
}
