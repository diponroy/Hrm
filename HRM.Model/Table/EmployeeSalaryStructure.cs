﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeSalaryStructure : IEmployeeSalaryStructure, IEntityWithLog<EmployeeSalaryStructureLog>
    {
        public long Id { get; set; }
        public DateTime? AttachmentDate { get; set; }
        public DateTime? DetachmentDate { get; set; }
        public bool IsDetached { get { return DetachmentDate != null; } }

        public long SalaryStructureId { get; set; }
        public float? Basic { get; set; }
        public long EmployeeWorkstationUnitId { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual SalaryStructure SalaryStructure { get; set; }
        public virtual EmployeeWorkStationUnit EmployeeWorkStationUnit { get; set; }

        public virtual ICollection<EmployeeSalaryStructureAllowance> EmployeeSalaryStructureAllowances { get; set; }
        public virtual ICollection<EmployeeSalaryStructureBonus> EmployeeSalaryStructureBonuses { get; set; }
        public virtual ICollection<EmployeeSalaryPayment> SalaryPayments { get; set; }
        public ICollection<EmployeeSalaryStructureLog> Logs { get; set; }
    }
}
