﻿using System;
using System.Collections.Generic;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeSalaryStructureAllowance : IEmployeeSalaryStructureAllowance, IEntityWithLog<EmployeeSalaryStructureAllowanceLog>
    {
        public long Id { get; set; }
        public long EmployeeSalaryStructureId { get; set; }
        public long AllowanceId { get; set; }
        public bool? AsPercentage { get; set; }
        public float? Weight { get; set; }
        public DateTime? AttachmentDate { get; set; }
        public DateTime? DetachmentDate { get; set; }
        public bool IsDetached { get { return DetachmentDate != null; } }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual EmployeeSalaryStructure EmployeeSalaryStructure { get; set; }
        public virtual Allowance Allowance { get; set; }
        public virtual ICollection<EmployeeSalaryPaymentAllowance> AllowancePayments { get; set; }
        public ICollection<EmployeeSalaryStructureAllowanceLog> Logs { get; set; }
    }
}
