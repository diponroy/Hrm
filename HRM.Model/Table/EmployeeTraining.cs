﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeTraining : IEmployeeTraining, IEntityWithLog<EmployeeTrainingLog>
    {
        public long Id { get; set; }
        
        public string Title { get; set; }
        public string Description { get; set; }
        public string Organizer { get; set; }
        public string Venue { get; set; }
        public string Trainer { get; set; }
        public bool IsApproved { get; set; }
        public string Remarks { get; set; }
        public DateTime? DurationFromDate { get; set; }
        public DateTime? DurationToDate { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual List<EmployeeAttachedTraining> EmployeeAttachedTrainings { get; set; }
        public virtual List<EmployeeTrainingAttachment> EmployeeTrainingAttachments { get; set; }
        public virtual List<EmployeeTrainingBudget> EmployeeTrainingBudgets { get; set; }
        public ICollection<EmployeeTrainingLog> Logs { get; set; }
    }
}
