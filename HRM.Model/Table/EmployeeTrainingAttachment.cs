﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeTrainingAttachment : IEmployeeTrainingAttachment, IEntityWithLog<EmployeeTrainingAttachmentLog>
    {
        public long Id { get; set; }
        
        public string Title { get; set; }
        public string Directory { get; set; }
        public string Remarks { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long EmployeeTrainingId { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual EmployeeTraining EmployeeTraining { get; set; }
        public ICollection<EmployeeTrainingAttachmentLog> Logs { get; set; }
    }
}
