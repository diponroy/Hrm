﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeTrainingBudget : IEmployeeTrainingBudget, IEntityWithLog<EmployeeTrainingBudgetLog>
    {
        public long Id { get; set; }
        
        public string Description { get; set; }
        public decimal TotalAmount { get; set; }
        public bool IsApproved { get; set; }
        public string Remarks { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public long EmployeeTrainingId { get; set; }
        public virtual EmployeeTraining EmployeeTraining { get; set; }
        public ICollection<EmployeeTrainingBudgetLog> Logs { get; set; }
    }
}
