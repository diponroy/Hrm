﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeTransfer : IEmployeeTransfer, IEntityWithLog<EmployeeTransferLog>
    {
        public long Id { get; set; }
        public bool IsApproved { get; set; }
        public long EmployeeId { get; set; }
        public long EmployeeTypeId { get; set; } 
        public long WorkStationUnitId { get; set; }
        public long? IncrementSalaryStructureId { get; set; }
        public string Remarks { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }


        public virtual Employee Employee { get; set; }
        public virtual WorkStationUnit WorkStationUnit { get; set; }
        public virtual EmployeeType EmployeeType { get; set; }
        public virtual IncrementSalaryStructure IncrementSalaryStructure { get; set; }

        
        
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }  
        
        
        public ICollection<EmployeeTransferLog> Logs { get; set; }
    }
}
