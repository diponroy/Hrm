﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared ;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log ;

namespace HRM.Model.Table
{
    public class EmployeeType : IEmployeeType, IEntityWithLog<EmployeeTypeLog>
    {
        public long Id { get; set; }

        public string Title { get; set; }
        public string Remarks { get; set; }
        public long? ParentId { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public virtual EmployeeType ParentEmployeeType { get; set; }
        public virtual ICollection<EmployeeType> ChildEmployeeTypes { get; set; }
        public virtual ICollection<EmployeeWorkStationUnit> EmployeeWorkStationUnits { get; set; }
        public virtual ICollection<EmployeeTransfer> EmployeeTransfers { get; set; }

        public long ? AffectedByEmployeeId { get ; set ; }
        public DateTime ? AffectedDateTime { get ; set ; }
        public virtual ICollection<EmployeeTypeLog> Logs { get; set; }
    }
}
