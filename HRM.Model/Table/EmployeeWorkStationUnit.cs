﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class EmployeeWorkStationUnit : IEmployeeWorkStationUnit, IEntityWithLog<EmployeeWorkStationUnitLog>
    {
        public long Id { get; set; }

        public DateTime? AttachmentDate { get; set; }
        public DateTime? DetachmentDate { get; set; }
        public bool IsDetached { get { return DetachmentDate != null; } }

        public string Remarks { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        public long WorkStationUnitId { get; set; }
        public virtual WorkStationUnit WorkStationUnit { get; set; }
        public long EmployeeTypeId { get; set; }
        public virtual EmployeeType EmployeeType { get; set; }

        public virtual ICollection<EmployeeSalaryStructure> EmployeeSalaryStructures { get; set; }
        public virtual ICollection<EmployeeAssignedAppraisal> EmployeeAssignedAppraisals { get; set; }
        public virtual ICollection<EmployeeAttendance> EmployeeAttendances { get; set; }



        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual ICollection<EmployeeWorkStationUnitLog> Logs { get; set; }
    }
}
