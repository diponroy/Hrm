﻿namespace HRM.Model.Table.Enums
{
    public enum EntityStatusEnum
    {
        Active  = 0,    //0
        Inactive = 1,   //1
        Removed = 2     //2
    }

}
