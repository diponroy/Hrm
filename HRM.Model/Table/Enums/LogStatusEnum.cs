﻿namespace HRM.Model.Table.Enums
{
    public enum LogStatusEnum
    {
        Added = 0,      //0
        Replaced  = 1,  //1
        Removed = 2     //2
    }
}
