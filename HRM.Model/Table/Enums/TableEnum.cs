﻿namespace HRM.Model.Table.Enums
{
    public enum Table
    {
        AppraisalIndicator,

        Branch,

        Candidate,
        CandidateEmployment,
        CandidateLanguage,
        CandidateEducation,
        CandidateReference,
        CandidateSkill,
        CandidateTraining,
        CandidateAchievement,
        CandidateLogin,
        Company,

        Department,
        
        Employee,
        EmployeeLogin,
        EmployeeEducation,
        EmployeeType,
        EmployeeMembership,
        EmployeePayrollInfo,
        EmployeeTraining,
        EmployeeAttachedTraining,
        EmployeeWorkStationUnit,
        EmployeeReporting,
        EmployeeAssignedAppraisal,
        EmployeeAppraisal,
        EmployeePromotion,
        EmployeeTransfer,
        EmployeeTrainingAttachment,
        EmployeeTrainingBudget,
        
        IncrementSalaryStructure,
        IncrementSalaryStructureAllowance,
        IncrementSalaryStructureBonus,

        Interview,
        InterviewCandidate,
        InterviewCoordinator,
        InterviewTest,

        WorkStationUnit,

        ManpowerVacancyNotice,

        AllowanceType,
        Allowance,
        BonusType,
        Bonus,
        BonusPaymentDate,
        SalaryStructure,
        SalaryStructureBonus,
        SalaryStructureAllowance,
        EmployeeSalaryStructure,
        EmployeeSalaryStructureAllowance,
        EmployeeSalaryStructureBonus,
        EmployeeSalaryPayment,
        EmployeeSalaryPaymentBonus,
        EmployeeSalaryPaymentAllowance ,
        InterviewTestCandidate ,
        ManpowerRequisition ,
        ManpowerVacancyNoticeCandidate,
        ManpowerCandidateReference,
        
        //RequisitionVacancyNotice,
        //RequisitionVacancyNoticeLog ,
        EmployeeAchievement,
        EmployeeDiscipline,
        EmployeeAttendance
    }
}
