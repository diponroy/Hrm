﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table
{
    public interface IEmployeeAttendance:IEntityStatus
    {
        long EmployeeWorkStationUnitId { get; set; }
        string AttendanceTime { get; set; }
        DateTime AttendanceDate { get; set; }  
        string Remarks { get; set; }
    }
}
