﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.Log;

namespace HRM.Model.Table.IEntity
{
    public interface IEntityLog<TEntity> : IAffectedByTrack, IPrimaryKeyTrack, ILogPrimaryKeyTrack
    {
        TEntity LogFor { get; set; }
        EmployeeLog AffectedByEmployeeLog { get; set; }
        LogStatusEnum? LogStatuses { get; set; }
    }
}
