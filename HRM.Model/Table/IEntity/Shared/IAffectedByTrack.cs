﻿using System;

namespace HRM.Model.Table.IEntity.Shared
{
    public interface IAffectedByTrack
    {
        long? AffectedByEmployeeId { get; set; }
        DateTime? AffectedDateTime { get; set; }
    }
}
