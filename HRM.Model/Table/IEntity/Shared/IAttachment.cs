﻿using System;

namespace HRM.Model.Table.IEntity.Shared
{
    public interface IAttachment
    {
        DateTime? AttachmentDate { get; set; }
        DateTime? DetachmentDate { get; set; }
        bool IsDetached { get; }
    }
}
