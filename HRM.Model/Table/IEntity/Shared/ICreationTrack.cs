﻿using System;

namespace HRM.Model.Table.IEntity.Shared
{
    public interface ICreationTrack
    {
        DateTime? DateOfCreation { get; set; }
        DateTime? DateOfClosing { get; set; }
        bool IsClosed { get; }
    }
}
