﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HRM.Model.Table.IEntity.Shared
{
    public interface IDuration
    {
        DateTime? DurationFromDate { get; set; }
        DateTime? DurationToDate { get; set; }
    }
}
