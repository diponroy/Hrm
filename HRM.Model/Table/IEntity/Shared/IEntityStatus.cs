﻿using HRM.Model.Table.Enums;

namespace HRM.Model.Table.IEntity.Shared
{
    public interface IEntityStatus : IPrimaryKeyTrack, IAffectedByTrack
    {
        EntityStatusEnum? Status { get; set; }
        string StatusString { get; }
    }
}
