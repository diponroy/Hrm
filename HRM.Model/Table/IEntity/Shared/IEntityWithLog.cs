﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HRM.Model.Table.IEntity.Shared
{
    public interface IEntityWithLog<TSource>
    {
        ICollection<TSource> Logs { get; set; }
    }
}
