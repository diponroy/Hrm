﻿namespace HRM.Model.Table.IEntity.Shared
{
    public interface ILogPrimaryKeyTrack
    {
        long LogId { get; set; }
    }
}
