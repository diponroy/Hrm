﻿namespace HRM.Model.Table.IEntity.Shared
{
    public interface IPrimaryKeyTrack
    {
        long Id { get; set; }
    }
}
