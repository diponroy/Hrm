﻿namespace HRM.Model.Table.IEntity.Shared
{
    public interface ITrackNoTrack
    {
        string TrackNo { get; set; }
    }
}
