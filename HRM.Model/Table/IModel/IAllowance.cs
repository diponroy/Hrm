﻿using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IAllowance : IEntityStatus
    {
        long AllowanceTypeId { get; set; }
        string Title { get; set; }
        bool? AsPercentage { get; set; }
        float Weight { get; set; }
    }
}
