﻿using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IAllowanceType : IEntityStatus
    {
        string Name { get; set; }
        string Remarks { get ; set ; }
    }
}
