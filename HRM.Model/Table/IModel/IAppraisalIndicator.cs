﻿using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IAppraisalIndicator : IEntityStatus
    {
        string Title { get; set; }
        decimal MaxWeight { get; set; }
        string Remarks { get; set; }
        long? ParentId { get; set; }
    }
}
