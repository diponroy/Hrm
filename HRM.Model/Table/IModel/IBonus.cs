﻿using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IBonus : IEntityStatus
    {
        long BonusTypeId { get; set; }
        string Title { get; set; }
        bool? AsPercentage { get; set; }
        float Weight { get; set; }
    }
}
