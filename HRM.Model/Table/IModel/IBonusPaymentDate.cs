﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IBonusPaymentDate : IEntityStatus
    {
        long BonusId { get; set; }
        DateTime EstimatedDate { get; set; }
    }
}
