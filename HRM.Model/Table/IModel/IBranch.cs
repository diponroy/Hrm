﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    interface IBranch : IEntityStatus, ICreationTrack
    {
        string Name { get; set; }
        string Address { get; set; }
        string Remarks { get; set; }
    }
}
