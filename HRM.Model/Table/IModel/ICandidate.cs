﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface ICandidate : ITrackNoTrack, IEntityStatus
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string FathersName { get; set; }
        string MothersName { get; set; }
        DateTime DateOfBirth { get; set; }
        string Gender { get; set; }
        string BloodGroup { get; set; }
        string MaritalStatus { get; set; }
        string Nationality { get; set; }
        string NationalId { get; set; }
        string PassportNo { get; set; }
        string Religion { get; set; }
        string Email { get; set; }
        string AlternateEmailAddress { get; set; }
        string ContactNo { get; set; }
        string HomePhone { get; set; }
        string PresentAddress { get; set; }
        string PermanentAddress { get; set; }
        string DivisionOrState { get; set; }
        string City { get; set; }
        string ImageDirectory { get; set; }
        string CvDirectory { get; set; }
    }
}
