﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface ICandidateAchievement : IEntityStatus
    {
        string Title { get; set; }
        string LiveUrl { get; set; }
        string Description { get; set; }
        string AttachmentDirectory { get; set; }
        long CandidateEmploymentId { get; set; }
    }
}
