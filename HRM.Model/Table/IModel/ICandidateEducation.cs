﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface ICandidateEducation : IDuration, IEntityStatus
    {
        string Title { get; set; }
        string Institution { get; set; }
        string Result { get; set; }
        string Major { get; set; }
        string AttachmentDirectory { get; set; }
        long CandidateId { get; set; }
    }
}
