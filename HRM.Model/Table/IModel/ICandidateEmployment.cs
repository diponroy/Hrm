﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface ICandidateEmployment : IDuration, IEntityStatus
    {
        string Institution { get; set; }
        string Address { get; set; }
        string Designation { get; set; }
        string Responsibility { get; set; }
        long CandidateId { get; set; }
    }
}
