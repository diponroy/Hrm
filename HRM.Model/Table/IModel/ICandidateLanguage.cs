﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface ICandidateLanguage : IEntityStatus
    {
        string LanguageName { get; set; }
        string Reading { get; set; }
        string Writing { get; set; }
        string Speaking { get; set; }
        string Listening { get; set; }
        long CandidateId { get; set; }
    }
}
