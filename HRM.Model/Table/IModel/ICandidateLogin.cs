﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface ICandidateLogin : IEntityStatus
    {
        string LoginName { get; set; }
        string Password { get; set; }
        long CandidateId { get; set; }
    }
}
