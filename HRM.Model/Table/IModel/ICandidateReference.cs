﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface ICandidateReference : IEntityStatus
    {
        string Referrer { get; set; }
        string Designation { get; set; }
        string CompanyOrOrg { get; set; }
        string ContactNo { get; set; }
        string Email { get; set; }
        string Address { get; set; }
        string Relation { get; set; }
        long CandidateId { get; set; }

    }
}
