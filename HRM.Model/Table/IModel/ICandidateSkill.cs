﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface ICandidateSkill : IEntityStatus
    {
        string Title { get; set; }
        string Description { get; set; }
        string AttachmentDirectory { get; set; }
        string Type { get; set; }
        long CandidateId { get; set; }
    }
}
