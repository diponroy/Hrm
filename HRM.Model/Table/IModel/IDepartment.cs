﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IDepartment : IEntityStatus, ICreationTrack
    {
        string Name { get; set; }
        string Description { get; set; }
        string Remarks { get; set; }
    }
}
