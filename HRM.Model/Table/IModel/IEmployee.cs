﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployee : ITrackNoTrack, IEntityStatus
    {
        string Salutation { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string FullName { get; }
        string FathersName { get; set; }
        string MothersName { get; set; }
        DateTime DateOfBirth { get; set; }
        string Gender { get; set; }
        string BloodGroup { get; set; }
        string Email { get; set; }
        string ContactNo { get; set; }
        string HomePhone { get; set; }
        string OfficePhone { get; set; }
        string PresentAddress { get; set; }
        string PermanentAddress { get; set; }
        string DivisionOrState { get; set; }
        string City { get; set; }
        string ImageDirectory { get; set; }
        long? CandidateId { get; set; }
    }
}
