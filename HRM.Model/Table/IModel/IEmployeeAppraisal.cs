﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeeAppraisal : IEntityStatus
    {
        decimal Weight { get; set; }
        string Remarks { get; set; }
        long EmployeeAssignedAppraisalId { get; set; }
    }
}
