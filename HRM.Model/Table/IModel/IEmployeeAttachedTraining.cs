﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeeAttachedTraining : IEntityStatus
    {
        string Remarks { get; set; }
        long EmployeeId { get; set; }
        long EmployeeTrainingId { get; set; }
    }
}