﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IEmployeeDiscipline : IEntityStatus
    {
        long EmployeeId { get; set; }  
        string Title { get; set; }
        DateTime DateOfCreation { get; set; }
        long DepartmentId { get; set; }
        string Description { get; set; }
        string ActionTaken { get; set; }
        string AttachmentDirectory { get; set; }
        string Remarks { get; set; }
    }
}
