﻿using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IEmployeeLogin : IEntityStatus
    {
        long EmployeeId { get; set; }
        string LoginName { get; set; }
        string Password { get; set; }
    }
}
