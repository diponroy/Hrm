﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeeMembership : IEntityStatus
    {
        string Organization { get; set; }
        string Description { get; set; }
        string Type { get; set; }
        string Remarks { get; set; }
        long EmployeeId { get; set; }
    }
}
