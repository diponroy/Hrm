﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeePayrollInfo : IEntityStatus
    {
        string BankName { get; set; }
        string BankDetail { get; set; }
        string AccountNo { get; set; }
        long TinNumber { get; set; }
        string Remarks { get; set; }
        long EmployeeId { get; set; }
    }
}
