﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeeReporting : IAttachment, IEntityStatus
    {
        string Remarks { get; set; }
        long EmployeeWorkStationUnitId { get; set; }
        long? ParentEmployeeWorkStationUnitId { get; set; }
    }
}