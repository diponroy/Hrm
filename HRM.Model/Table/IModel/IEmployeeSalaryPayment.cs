﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeeSalaryPayment : ITrackNoTrack, IEntityStatus
    {
        long EmployeeSalaryStructureId { get; set; }
        DateTime DateOfPayment { get; set; }
        long EmployeePayrollInfoId { get; set; } 
        float? TotalInAmount { get; set; }
    }
}
