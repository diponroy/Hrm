﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    interface IEmployeeSalaryPaymentAllowance : IEntityStatus
    {
        long EmployeeSalaryPaymentId { get; set; }
        long EmployeeSalaryStructureAllowanceId { get; set; }
        float? InAmount { get; set; }
    }
}
