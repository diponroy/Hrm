﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    interface IEmployeeSalaryPaymentBonus : IEntityStatus
    {
        long EmployeeSalaryPaymentId { get; set; }
        long EmployeeSalaryStructureBonusId { get; set; }
        float? InAmount { get; set; }
    }
}
