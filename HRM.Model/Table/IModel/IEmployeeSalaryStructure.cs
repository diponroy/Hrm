﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    interface IEmployeeSalaryStructure : IEntityStatus, IAttachment
    {
        long SalaryStructureId { get; set; }
        float? Basic { get; set; }
        long EmployeeWorkstationUnitId { get; set; }
    }
}
