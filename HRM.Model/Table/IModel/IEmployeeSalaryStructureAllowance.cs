﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    interface IEmployeeSalaryStructureAllowance : IEntityStatus, IAttachment
    {
        long EmployeeSalaryStructureId { get; set; }
        long AllowanceId { get; set; }
        bool? AsPercentage { get; set; }
        float? Weight { get; set; }
    }
}
