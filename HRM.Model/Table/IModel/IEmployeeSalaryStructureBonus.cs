﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    interface IEmployeeSalaryStructureBonus : IEntityStatus, IAttachment
    {
        long EmployeeSalaryStructureId { get; set; }
        long BonusId { get; set; }
        bool? AsPercentage { get; set; }
        float? Weight { get; set; }
    }
}
