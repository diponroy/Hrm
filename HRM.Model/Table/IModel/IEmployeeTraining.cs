﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeeTraining : IDuration, IEntityStatus
    {
        string Title { get; set; }
        string Description { get; set; }
        string Organizer { get; set; }
        string Venue { get; set; }
        string Trainer { get; set; }
        bool IsApproved { get; set; }
        string Remarks { get; set; }
    }
}
