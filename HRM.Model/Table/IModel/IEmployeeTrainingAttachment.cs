﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeeTrainingAttachment : IEntityStatus
    {
        string Title { get; set; }
        string Directory { get; set; }
        string Remarks { get; set; }
        long EmployeeTrainingId { get; set; }
    }
}
