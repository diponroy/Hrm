﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeeTrainingBudget : IEntityStatus
    {
        string Description { get; set; }
        decimal TotalAmount { get; set; }
        bool IsApproved { get; set; }
        string Remarks { get; set; }
        long EmployeeTrainingId { get; set; }
    }
}
