﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IEmployeeWorkStationUnit : IAttachment, IEntityStatus
    {
        string Remarks { get; set; }
        long EmployeeId { get; set; }
        long WorkStationUnitId { get; set; }
        long EmployeeTypeId { get; set; }
    }
}
