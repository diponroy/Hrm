﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IIncrementSalaryStructure : IEntityStatus
    {
        float? Basic { get; set; }
        bool IsApproved { get; set; }
        long? EmployeeSalaryStructureId { get; set; }
    }
}
