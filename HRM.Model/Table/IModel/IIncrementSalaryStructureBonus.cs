﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface IIncrementSalaryStructureBonus : IEntityStatus
    {
        bool AsPercentage { get; set; }
        decimal Weight { get; set; }
        long IncrementSalaryStructureId { get; set; }
        long BonusId { get; set; }
    }
}