﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IInterview : ITrackNoTrack,IDuration, IEntityStatus
    {
        string Title { get; set; }
        string Remarks { get; set; }
        long ManpowerVacancyNoticeId { get; set; }
    }
}
