﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IInterviewCandidate : IEntityStatus
    {
        DateTime AppointmentDateTime { get; set; }
        DateTime AttendedDateTime { get; set; }
        decimal CurrentSalary { get; set; }
        decimal ExpectedSalary { get; set; }
        decimal ProposedSalary { get; set; }
        long CandidateId { get; set; }
        long InterviewId { get; set; }
        string Remarks { get; set; }
    }
}
