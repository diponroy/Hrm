﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IInterviewCoordinator : IEntityStatus
    {
        long InterviewId { get ; set ; }
        long EmployeeId { get; set; }
        string Remarks { get; set; }
    }
}
