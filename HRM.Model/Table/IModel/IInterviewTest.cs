﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IInterviewTest : IEntityStatus
    {
        string Title { get ; set ; }
        decimal TotalMarks { get ; set ; }
        string Remarks { get ; set ; }
        long InterviewId { get ; set ; }
    }
}
