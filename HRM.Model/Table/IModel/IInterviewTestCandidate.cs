﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IInterviewTestCandidate : IEntityStatus
    {
        Decimal ObtainedMarks { get ; set ; }
        long InterviewTestId { get ; set ; }
        long CandidateId { get ; set ; }
        string Remarks { get ; set ; }
       }
}
