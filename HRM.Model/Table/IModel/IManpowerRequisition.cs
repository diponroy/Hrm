﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared ;

namespace HRM.Model.Table.IModel
{
    public interface IManpowerRequisition : IEntityStatus, ITrackNoTrack
    {
        string Title { get; set; }
        string PositionForDesignation { get; set; }
        int NumberOfPersons { get; set; }
        string Description { get; set; }
        DateTime DateOfCreation { get; set; }
        DateTime DedlineDate { get; set; }
        bool IsApproved { get; set; }
     }
}
