﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime ;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table ;

namespace HRM.Model.Table.IModel
{
    public interface IManpowerVacancyNotice  : IDuration, ITrackNoTrack, IEntityStatus
    {
        string Type { get ; set ; }
        string Title { set ; get ; }
        string Description { get ; set ; }
        long ManpowerRequisitionId { get ; set ; }
    }
}
