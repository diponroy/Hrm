﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IManpowerVacancyNoticeCandidate : IEntityStatus, ITrackNoTrack
    {
        long ManpowerVacancyNoticeId { get; set; }
        long CandidateId { get; set; }
        long? EmployeeId { get; set; }
    }
}
