﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    public interface ISalaryStructure : IEntityStatus
    {
        string Title { get; set; }
        float? Basic { get; set; }
        string Remarks { get; set; }
    }
}
