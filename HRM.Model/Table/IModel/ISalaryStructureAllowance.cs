﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;


namespace HRM.Model.Table.IModel
{
    interface ISalaryStructureAllowance : IEntityStatus
    {
        long SalaryStructureId { get; set; }
        long AllowanceId { get; set; }
        string Remarks { get; set; }     
    }
}
