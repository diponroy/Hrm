﻿using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    internal interface ISalaryStructureBonus : IEntityStatus
    {
        long SalaryStructureId { get; set; }
        long BonusId { get; set; }
        string Remarks { get; set; }
    }
}
