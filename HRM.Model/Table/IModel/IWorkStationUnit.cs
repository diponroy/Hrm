﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HRM.Model.Table.IEntity.Shared;

namespace HRM.Model.Table.IModel
{
    public interface IWorkStationUnit : IEntityStatus, ICreationTrack
    {
        long? CompanyId { get; set; }
        long? BranchId { get; set; }
        long? DepartmentId { get; set; }
        string Remarks { get; set; }
        long? ParentId { get; set; }
        long? EmployeeIdAsInCharge { get; set; }
      }
}
