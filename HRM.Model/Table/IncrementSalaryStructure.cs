﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class IncrementSalaryStructure : IIncrementSalaryStructure, IEntityWithLog<IncrementSalaryStructureLog>
    {
        public long Id { get; set; }
        
        public float? Basic { get; set; }
        public bool IsApproved { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? EmployeeSalaryStructureId { get; set; }
        public virtual EmployeeSalaryStructure EmployeeSalaryStructure { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public ICollection<IncrementSalaryStructureLog> Logs { get; set; }
    }
}