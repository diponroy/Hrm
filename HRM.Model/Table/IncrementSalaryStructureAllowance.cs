﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class IncrementSalaryStructureAllowance : IIncrementSalaryStructureAllowance, IEntityWithLog<IncrementSalaryStructureAllowanceLog>
    {
        public long Id { get; set; }
        
        public bool AsPercentage { get; set; }
        public decimal Weight { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long IncrementSalaryStructureId { get; set; }
        public virtual IncrementSalaryStructure IncrementSalaryStructure { get; set; }
        public long AllowanceId { get; set; }
        public virtual Allowance Allowance { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public ICollection<IncrementSalaryStructureAllowanceLog> Logs { get; set; }
    }
}
