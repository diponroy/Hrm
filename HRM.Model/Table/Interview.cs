﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class Interview : IInterview, IEntityWithLog<InterviewLog>
    {
        public long Id { get ; set ; }
        public string TrackNo { get; set; }
        public string Title { get; set; }
        public string Remarks { get; set; }
        public DateTime? DurationFromDate { get; set; }
        public DateTime? DurationToDate { get; set; }
        public long ManpowerVacancyNoticeId { get; set; }
        
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public virtual ManpowerVacancyNotice ManpowerVacancyNotice { get ; set ; }

        //public virtual List<InterviewCandidate> InterviewCandidates { get ; set ; }

        public virtual List<InterviewCoordinator> InterviewCoordinators { get; set; }

        public virtual List<InterviewTest> InterviewTests { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public ICollection<InterviewLog> Logs { get; set; }
    }
}

