﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms ;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class InterviewCandidate : IInterviewCandidate, IEntityWithLog<InterviewCandidateLog>
    {
        public long Id { get ; set ; }
        public DateTime AppointmentDateTime { get ; set ; }
        public DateTime AttendedDateTime { get; set; }
        public decimal CurrentSalary { get; set; }
        public decimal ExpectedSalary { get; set; }
        public decimal ProposedSalary { get; set; }
        public string Remarks { get; set; }

        public long InterviewId { get; set; }
        public long CandidateId { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public virtual Interview Interview { get ; set ; } 
        public virtual Candidate Candidate { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public ICollection<InterviewCandidateLog> Logs { get; set; }
    }   
}
