﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class InterviewTest : IInterviewTest, IEntityWithLog<InterviewTestLog>
    {
        public long Id { get ; set ; } 
        public string Title { get ; set ; }
        public decimal TotalMarks { get ; set ; }
        public long InterviewId { get; set; }
        public string Remarks { get ; set ; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        
        public virtual Interview Interview { get ; set ; }

        //public virtual List<InterviewTestCandidate> InterviewTestCandidates { get ; set ; } 

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public ICollection<InterviewTestLog> Logs { get; set; }
    }
}
