﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class InterviewTestCandidate : IInterviewTestCandidate, IEntityWithLog<InterviewTestCandidateLog>
    {
        public long Id { get ; set ; }
        public decimal ObtainedMarks { get ; set ; }
        public long InterviewTestId { get; set; }
        public long CandidateId { get; set; }
        public string Remarks { get; set; }


        public EntityStatusEnum? Status { get; set; } 
        public string StatusString { get { return Status.ToString(); } }


        public virtual InterviewTest InterviewTest { get ; set ; }
        public virtual Candidate Candidate { get; set; }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public ICollection<InterviewTestCandidateLog> Logs { get; set; }
    }
}
