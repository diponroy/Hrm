﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class AllowanceLog : IAllowance, IEntityLog<Allowance>
    {

        public long LogId { get; set; }
        public long Id { get; set; }
        public long AllowanceTypeId { get; set; }
        public string Title { get; set; }
        public bool? AsPercentage { get; set; }
        public float Weight { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public EmployeeLog AffectedByEmployeeLog { get; set; }

        public Allowance LogFor { get; set; }
        public virtual AllowanceTypeLog AllowanceTypeLog { get; set; }

    }
}
