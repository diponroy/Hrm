﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class AppraisalIndicatorLog : IAppraisalIndicator, IEntityLog<AppraisalIndicator>
    {
        public long LogId { get; set; }
        public AppraisalIndicator LogFor { get; set; }
        public long Id { get; set; }
        
        public string Title { get; set; }
        public decimal MaxWeight { get; set; }
        public string Remarks { get; set; }
        public long? ParentId { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public EmployeeLog AffectedByEmployeeLog { get; set; }

        public virtual AppraisalIndicatorLog ParentAppraisalIndicatorLog { get; set; }

    }
}
