﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;
using HRM.Model.Table;

namespace HRM.Model.Table.Log
{
    public class BonusLog : IBonus, IEntityLog<Bonus>
    {
        public long LogId { get; set; }
        public Bonus LogFor { get; set; }

        public long Id { get; set; }
        public long BonusTypeId { get; set; }
        public string Title { get; set; }
        public bool? AsPercentage { get; set; }
        public float Weight { get; set; }
        public EntityStatusEnum? Status { get; set; }

        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual BonusTypeLog BonusTypeLog { get; set; }
    }
}
