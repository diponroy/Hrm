﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class BonusPaymentDateLog : IBonusPaymentDate, IEntityLog<BonusPaymentDate>
    {
        public long LogId { get; set; }
        public BonusPaymentDate LogFor { get; set; }

        public long Id { get; set; }
        public long BonusId { get; set; }
        public DateTime EstimatedDate { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual BonusLog BonusLog { get; set; }
        public EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
