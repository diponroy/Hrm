﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class BonusTypeLog : IBonusType, IEntityLog<BonusType>
    {
        public long LogId { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Remarks { get ; set ; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public BonusType LogFor { get; set; }
        public EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
