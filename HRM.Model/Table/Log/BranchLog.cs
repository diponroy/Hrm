﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel ;

namespace HRM.Model.Table.Log
{
    public class BranchLog : IBranch, IEntityLog<Branch>
    {
        public long LogId { get; set; }
        
        public long Id { get ; set ; }
        public string Name { get ; set ; }
        public string Address { get ; set ; }
        public string Remarks { get ; set ; }
        public DateTime? DateOfCreation { get ; set ; }
        public DateTime? DateOfClosing { get ; set ; }
        public bool IsClosed { get { return DateOfClosing != null; } }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual Branch LogFor { get; set; }
    }
}
