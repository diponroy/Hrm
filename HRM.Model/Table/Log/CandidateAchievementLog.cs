﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class CandidateAchievementLog : ICandidateAchievement, IEntityLog<CandidateAchievement>
    {
        public long LogId { get; set; }
        public CandidateAchievement LogFor { get; set; }
        public long Id { get; set; }
        
        public string Title { get; set; }
        public string LiveUrl { get; set; }
        public string Description { get; set; }
        public string AttachmentDirectory { get; set; }
        public long CandidateEmploymentId { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual CandidateEmploymentLog CandidateEmploymentLog { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
