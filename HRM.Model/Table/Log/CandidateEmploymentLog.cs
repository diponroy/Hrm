﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class CandidateEmploymentLog : ICandidateEmployment, IEntityLog<CandidateEmployment>
    {
        public long LogId { get; set; }

        public long Id { get; set; }
        public string Institution { get; set; }
        public string Address { get; set; }
        public string Designation { get; set; }
        public string Responsibility { get; set; }
        public DateTime? DurationFromDate { get; set; }
        public DateTime? DurationToDate { get; set; }
        public long CandidateId { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

       
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual CandidateEmployment LogFor { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual CandidateLog CandidateLog { get; set; }
    }
}
