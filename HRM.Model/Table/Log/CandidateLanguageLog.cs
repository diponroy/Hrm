﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class CandidateLanguageLog : ICandidateLanguage, IEntityLog<CandidateLanguage>
    {
        public long LogId { get; set; }
        public CandidateLanguage LogFor { get; set; }
        public long Id { get; set; }
        
        public string LanguageName { get; set; }
        public string Reading { get; set; }
        public string Writing { get; set; }
        public string Speaking { get; set; }
        public string Listening { get; set; }
        public long CandidateId { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual CandidateLog CandidateLog { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
