﻿using System;


using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class CandidateLog : ICandidate, IEntityLog<Candidate>
    {
        public long LogId { get; set; }
        public Candidate LogFor { get; set; }

        public long Id { get; set; }
        public string TrackNo { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FathersName { get; set; }
        public string MothersName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string BloodGroup { get; set; }
        public string MaritalStatus { get; set; }
        public string Nationality { get; set; }
        public string NationalId { get; set; }
        public string PassportNo { get; set; }
        public string Religion { get; set; }
        public string Email { get; set; }
        public string AlternateEmailAddress { get; set; }
        public string ContactNo { get; set; }
        public string HomePhone { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string DivisionOrState { get; set; }
        public string City { get; set; }
        public string ImageDirectory { get; set; }
        public string CvDirectory { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
