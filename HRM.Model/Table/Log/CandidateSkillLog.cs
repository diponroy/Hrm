﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class CandidateSkillLog : ICandidateSkill, IEntityLog<CandidateSkill>
    {
        public long LogId { get; set; }

        public long Id { get; set; }       
        public string Title { get; set; }
        public string Description { get; set; }
        public string AttachmentDirectory { get; set; }
        public string Type { get; set; }
        public long CandidateId { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual CandidateSkill LogFor { get; set; }
        public virtual CandidateLog CandidateLog { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
