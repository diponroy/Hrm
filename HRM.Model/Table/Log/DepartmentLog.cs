﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel ;

namespace HRM.Model.Table.Log
{
    public class DepartmentLog : IDepartment, IEntityLog<Department>
    {
        public long LogId { get; set; }
        public Department LogFor { get; set; }

        public long Id { get ; set ; }
        public EntityStatusEnum? Status { get ; set ; }
        public string StatusString { get; set; }
        public string Name { get ; set ; }
        public string Description { get ; set ; }
        public string Remarks { get ; set ; }
        public DateTime? DateOfCreation { get ; set ; }
        public DateTime? DateOfClosing { get ; set ; }
        public bool IsClosed { get { return DateOfClosing != null; } }
        public long? AffectedByEmployeeId { get; set; }
        //public string AffectedByEmployeeName { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }
        public string LogStatusString { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
