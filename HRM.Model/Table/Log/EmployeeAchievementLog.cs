﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.IEntity ;
using HRM.Model.Table.IModel ;

namespace HRM.Model.Table.Log
{
    public class EmployeeAchievementLog : IEmployeeAchievement, IEntityLog<EmployeeAchievement>
    {
        public long LogId { get; set; }
        public EmployeeAchievement LogFor { get; set; }
        public long Id { get; set; }
        public string Title { get; set; }
        public string Remarks { get; set; }
        public long EmployeeId { get; set; }
        public long EmployeeAppraisalId { get; set; }
        public string AttachmentDirectory { get; set; }
        public EntityStatusEnum? Status { get; set; } 
        public string StatusString { get { return Status.ToString(); } }


        public EmployeeLog EmployeeLog { get; set; }
        public EmployeeAppraisalLog EmployeeAppraisalLog { get; set; }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }  
        public EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
