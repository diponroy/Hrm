﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;

namespace HRM.Model.Table.Log
{
    public class EmployeeAttendanceLog : IEmployeeAttendance, IEntityLog<EmployeeAttendance>
    {
        public long LogId { get; set; }
        public long Id { get; set; }
        public long EmployeeWorkStationUnitId { get; set; }
        public string AttendanceTime { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string Remarks { get; set; }
        
        public virtual EmployeeWorkStationUnitLog EmployeeWorkStationUnitLog { get; set; }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }


        public EmployeeAttendance LogFor { get; set; }
        public EmployeeLog AffectedByEmployeeLog { get; set; }
        public LogStatusEnum ? LogStatuses { get; set; }
    }
}
