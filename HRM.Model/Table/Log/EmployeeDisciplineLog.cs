﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeDisciplineLog : IEmployeeDiscipline, IEntityLog<EmployeeDiscipline>
    {
        public long LogId { get; set; }  
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string Title { get; set; }
        public DateTime DateOfCreation { get; set; }
        public long DepartmentId { get; set; }
        public string Description { get; set; }
        public string ActionTaken { get; set; }
        public string AttachmentDirectory { get; set; }
        public string Remarks { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get; private set; }   


        public virtual EmployeeLog EmployeeLog { get; set; }
        public virtual DepartmentLog DepartmentLog { get; set; }  
        

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public EmployeeLog AffectedByEmployeeLog { get; set; }
        public LogStatusEnum ? LogStatuses { get; set; }


        public EmployeeDiscipline LogFor { get; set; }
    }
}
