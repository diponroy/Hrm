﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security ;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel ;
using HRM.Model.Table ;

namespace HRM.Model.Table.Log
{
    public class EmployeeLog : IEmployee, IEntityLog<Employee>
    {
        public long LogId { get; set; }
        public long  Id { get; set; }
        public string TrackNo { get; set; }

        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return string.Format("{0} {1} {2}", Salutation, FirstName, LastName);
            }
        }

        public string FathersName { get; set; }
        public string MothersName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string BloodGroup { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhone { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string DivisionOrState { get; set; }
        public string City { get; set; }
        public string ImageDirectory { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? CandidateId { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual Employee LogFor { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
