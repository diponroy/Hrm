﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeLoginLog : IEmployeeLogin, IEntityLog<EmployeeLogin>
    {
        public long LogId { get; set; }
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public virtual EmployeeLogin LogFor { get; set; }
        public virtual EmployeeLog EmployeeLog { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
