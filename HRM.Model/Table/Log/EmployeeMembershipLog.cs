﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeMembershipLog : IEmployeeMembership, IEntityLog<EmployeeMembership>
    {
        public long LogId { get; set; }
        public EmployeeMembership LogFor { get; set; }
        public long Id { get; set; }
        
        public string Organization { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Remarks { get; set; }
        public long EmployeeId { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public EmployeeLog EmployeeLog { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
