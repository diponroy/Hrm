﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeePayrollInfoLog : IEmployeePayrollInfo, IEntityLog<EmployeePayrollInfo>
    {
        public long LogId { get; set; }
        public EmployeePayrollInfo LogFor { get; set; }
        public long Id { get; set; }
        
        public string BankName { get; set; }
        public string BankDetail { get; set; }
        public string AccountNo { get; set; }
        public long TinNumber { get; set; }
        public string Remarks { get; set; }
        public long EmployeeId { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }
        public virtual EmployeeLog EmployeeLog { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
