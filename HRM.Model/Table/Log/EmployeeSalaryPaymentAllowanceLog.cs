﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeSalaryPaymentAllowanceLog : IEmployeeSalaryPaymentAllowance, IEntityLog<EmployeeSalaryPaymentAllowance>
    {
        public long LogId { get; set; }
        public long Id { get; set; }
        public long EmployeeSalaryPaymentId { get; set; } 
        public long EmployeeSalaryStructureAllowanceId { get; set; }
        public float? InAmount { get; set; }


        public EmployeeSalaryPaymentAllowance LogFor { get; set; }  
        public virtual EmployeeSalaryPaymentLog EmployeeSalaryPaymentLog { get; set; }
        public virtual EmployeeSalaryStructureAllowanceLog EmployeeSalaryStructureAllowanceLog { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }  
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
