﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeSalaryPaymentBonusLog : IEmployeeSalaryPaymentBonus, IEntityLog<EmployeeSalaryPaymentBonus>
    {
        public long LogId { get; set; }
        public long Id { get; set; }
        public long EmployeeSalaryPaymentId { get; set; }
        public long EmployeeSalaryStructureBonusId { get; set; }
        public float? InAmount { get; set; }


        public EmployeeSalaryPaymentBonus LogFor { get; set; } 
        public virtual EmployeeSalaryPaymentLog EmployeeSalaryPaymentLog { get; set; }
        public virtual EmployeeSalaryStructureBonusLog EmployeeSalaryStructureBonusLog { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }  
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
