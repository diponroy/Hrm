﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeSalaryPaymentLog : IEmployeeSalaryPayment, IEntityLog<EmployeeSalaryPayment>
    {
        public long LogId { get; set; }
        public EmployeeSalaryPayment LogFor { get; set; }


        public long Id { get; set; }
        public string TrackNo { get; set; }
        public long EmployeeSalaryStructureId { get; set; }
        public DateTime DateOfPayment { get; set; }
        public long EmployeePayrollInfoId { get; set; }
        public float? TotalInAmount { get; set; }


        public virtual EmployeeSalaryStructureLog EmployeeSalaryStructureLog { get; set; }
        public virtual EmployeePayrollInfoLog EmployeePayrollInfoLog { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; } 
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
