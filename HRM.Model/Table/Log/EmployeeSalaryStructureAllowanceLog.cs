﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeSalaryStructureAllowanceLog : IEmployeeSalaryStructureAllowance, IEntityLog<EmployeeSalaryStructureAllowance>
    {
        public long LogId { get; set; }
        public EmployeeSalaryStructureAllowance LogFor { get; set; }

        public long Id { get; set; }
        public long EmployeeSalaryStructureId { get; set; }
        public long AllowanceId { get; set; }
        public bool? AsPercentage { get; set; }
        public float? Weight { get; set; }
        public DateTime? AttachmentDate { get; set; }
        public DateTime? DetachmentDate { get; set; }
        public bool IsDetached { get { return DetachmentDate != null; } }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }

        public virtual EmployeeSalaryStructureLog EmployeeSalaryStructureLog { get; set; }
        public virtual AllowanceLog AllowanceLog { get; set; }
    }
}
