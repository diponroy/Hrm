﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeSalaryStructureLog : IEmployeeSalaryStructure, IEntityLog<EmployeeSalaryStructure>
    {
        public long LogId { get; set; }
        public EmployeeSalaryStructure LogFor { get; set; }

        public long Id { get; set; }
        public DateTime? AttachmentDate { get; set; }
        public DateTime? DetachmentDate { get; set; }
        public bool IsDetached { get { return DetachmentDate != null; } }

        public long SalaryStructureId { get; set; }
        public float? Basic { get; set; }
        public long EmployeeWorkstationUnitId { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }

        public virtual SalaryStructureLog SalaryStructureLog { get; set; }

        public virtual EmployeeWorkStationUnitLog EmployeeWorkstationUnitLog { get; set; }
      
    }
}
