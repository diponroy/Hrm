﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeTrainingAttachmentLog : IEmployeeTrainingAttachment, IEntityLog<EmployeeTrainingAttachment>
    {
        public long LogId { get; set; }
        public EmployeeTrainingAttachment LogFor { get; set; }
        public long Id { get; set; }
        
        public string Title { get; set; }
        public string Directory { get; set; }
        public string Remarks { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long EmployeeTrainingId { get; set; }
        public virtual EmployeeTrainingLog EmployeeTrainingLog { get; set; }
        
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
