﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeTrainingLog : IEmployeeTraining, IEntityLog<EmployeeTraining>
    {
        public long LogId { get; set; }
        public EmployeeTraining LogFor { get; set; }
        public long Id { get; set; }
        
        public string Title { get; set; }
        public string Description { get; set; }
        public string Organizer { get; set; }
        public string Venue { get; set; }
        public string Trainer { get; set; }
        public bool IsApproved { get; set; }
        public string Remarks { get; set; }
        public DateTime? DurationFromDate { get; set; }
        public DateTime? DurationToDate { get; set; }

        public EntityStatusEnum? Status { get; set; }

        public string StatusString { get { return Status.ToString(); } }
        
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
