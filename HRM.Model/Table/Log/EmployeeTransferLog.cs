﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeTransferLog : IEmployeeTransfer, IEntityLog<EmployeeTransfer>
    {
        public long LogId { get; set; }
        public EmployeeTransfer LogFor { get; set; }
        public long Id { get; set; }

        
        public bool IsApproved { get; set; }
        public long EmployeeId { get; set; }
        public long EmployeeTypeId { get; set; }
        public long WorkStationUnitId { get; set; }
        public long? IncrementSalaryStructureId { get; set; }
        public string Remarks { get; set; }


        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }


        public virtual EmployeeLog EmployeeLog { get; set; }
        public virtual EmployeeTypeLog EmployeeTypeLog { get; set; }
        public virtual WorkStationUnitLog WorkStationUnitLog { get; set; }
        public virtual IncrementSalaryStructureLog IncrementSalaryStructureLog { get; set; }   
        
        
        public long? AffectedByEmployeeId { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }  
        public DateTime? AffectedDateTime { get; set; }


        public LogStatusEnum? LogStatuses { get; set; }  
    }
}
