﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class EmployeeWorkStationUnitLog : IEmployeeWorkStationUnit, IEntityLog<EmployeeWorkStationUnit>
    {
        public long LogId { get; set; }
        public EmployeeWorkStationUnit LogFor { get; set; }
        public long Id { get; set; }

        public DateTime? AttachmentDate { get; set; }
        public DateTime? DetachmentDate { get; set; }
        public bool IsDetached { get { return DetachmentDate != null; } }

        public string Remarks { get; set; }
        public long EmployeeId { get; set; }
        public long WorkStationUnitId { get; set; }
        public long EmployeeTypeId { get; set; }
        

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual EmployeeLog EmployeeLog { get; set; }
        public virtual WorkStationUnitLog WorkStationUnitLog { get; set; }
        public virtual EmployeeTypeLog EmployeeTypeLog { get; set; }
    }
}
