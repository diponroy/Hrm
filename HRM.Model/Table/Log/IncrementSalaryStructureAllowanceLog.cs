﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class IncrementSalaryStructureAllowanceLog : IIncrementSalaryStructureAllowance, IEntityLog<IncrementSalaryStructureAllowance>
    {
        public long LogId { get; set; }
        public IncrementSalaryStructureAllowance LogFor { get; set; }
        public long Id { get; set; }
        
        public bool AsPercentage { get; set; }
        public decimal Weight { get; set; }
        public long IncrementSalaryStructureId { get; set; }
        public long AllowanceId { get; set; }

        public EntityStatusEnum? Status { get; set; }

        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual AllowanceLog AllowanceLog { get; set; }
        public virtual IncrementSalaryStructureLog IncrementSalaryStructureLog { get; set; }
    }
}
