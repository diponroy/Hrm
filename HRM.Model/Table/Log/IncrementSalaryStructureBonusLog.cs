﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class IncrementSalaryStructureBonusLog : IIncrementSalaryStructureBonus, IEntityLog<IncrementSalaryStructureBonus>
    {
        public long LogId { get; set; }
        public IncrementSalaryStructureBonus LogFor { get; set; }
        public long Id { get; set; }
        
        public bool AsPercentage { get; set; }
        public decimal Weight { get; set; }

        public EntityStatusEnum? Status { get; set; }

        public string StatusString { get { return Status.ToString(); } }

        public long IncrementSalaryStructureId { get; set; }
        public long BonusId { get; set; }
        
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual IncrementSalaryStructureLog IncrementSalaryStructureLog { get; set; }
        public virtual BonusLog BonusLog { get; set; }
    }
}
