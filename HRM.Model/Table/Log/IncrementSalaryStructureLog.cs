﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class IncrementSalaryStructureLog : IIncrementSalaryStructure, IEntityLog<IncrementSalaryStructure>
    {
        public long LogId { get; set; }
        public IncrementSalaryStructure LogFor { get; set; }
        public long Id { get; set; }

        public float? Basic { get; set; }
        public bool IsApproved { get; set; }
        public long? EmployeeSalaryStructureId { get; set; }
        public virtual EmployeeSalaryStructureLog EmployeeSalaryStructureLog { get; set; }

        public EntityStatusEnum? Status { get; set; }

        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
