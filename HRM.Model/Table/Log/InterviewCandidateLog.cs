﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel ;

namespace HRM.Model.Table.Log
{
    public class InterviewCandidateLog : IInterviewCandidate, IEntityLog<InterviewCandidate>
    {   
        public long Id { get ; set ; }  
        public DateTime AppointmentDateTime { get ; set ; }
        public DateTime AttendedDateTime { get ; set ; }
        public decimal CurrentSalary { get ; set ; }
        public decimal ExpectedSalary { get ; set ; }
        public decimal ProposedSalary { get ; set ; }
        public long CandidateId { get; set; }
        public long InterviewId { get ; set ; }
        public string Remarks { get ; set ; }


        public long LogId { get; set; }


        public EntityStatusEnum? Status { get; set; } 
        public string StatusString { get { return Status.ToString(); } }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }


        public InterviewCandidate LogFor { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual CandidateLog CandidateLog { get; set; }
        public virtual InterviewLog InterviewLog { get; set; }
    }
}
