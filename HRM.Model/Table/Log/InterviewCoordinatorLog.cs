﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class InterviewCoordinatorLog : IInterviewCoordinator, IEntityLog<InterviewCoordinator>
    {
        public long LogId { get ; set ; }
        
        public long Id { get; set; }
        public string Remarks { get; set; }
        public long InterviewId { get; set; }
        public long EmployeeId { get; set; }


        public InterviewCoordinator LogFor { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }  
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }

        public virtual EmployeeLog EmployeeLog { get; set; }
        public virtual InterviewLog InterviewLog { get; set; }
    }
}
