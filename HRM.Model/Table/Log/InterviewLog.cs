﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel ;

namespace HRM.Model.Table.Log
{
    public class InterviewLog : IInterview, IEntityLog<Interview>
    {
        public long LogId { get; set; }
        public Interview LogFor { get; set; }

        public long Id { get ; set ; }
        public EntityStatusEnum? Status { get ; set ; }
        public string StatusString { get { return Status.ToString(); } }
        public string TrackNo { get ; set ; }
        public string Title { get ; set ; }
        public string Remarks { get ; set ; }
        public DateTime? DurationFromDate { get ; set ; }
        public DateTime? DurationToDate { get ; set ; }
        public long ManpowerVacancyNoticeId { get ; set ; }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual ManpowerVacancyNoticeLog ManpowerVacancyNoticeLog { get; set; }
    }
}
