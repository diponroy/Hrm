﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel ;

namespace HRM.Model.Table.Log
{
    public class InterviewTestCandidateLog : IInterviewTestCandidate, IEntityLog<InterviewTestCandidate>
    {
        public long LogId { get; set; }
        public InterviewTestCandidate LogFor { get; set; }
        public long Id { get ; set ; } 
        public decimal ObtainedMarks { get ; set ; }
        public long InterviewTestId { get ; set ; }
        public long CandidateId { get ; set ; }
        public string Remarks { get ; set ; }
        public EntityStatusEnum? Status { get; set; }

        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual InterviewTestLog InterviewTestLog{ get; set; }
        public virtual CandidateLog CandidateLog { get; set; }
    }
}
