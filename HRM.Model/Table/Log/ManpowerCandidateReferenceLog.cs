﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class ManpowerCandidateReferenceLog : IManpowerCandidateReference, IEntityLog<ManpowerCandidateReference>
    {
        public long LogId { get; set; }
        public ManpowerCandidateReference LogFor { get; set; }


        public long Id { get; set; }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }
        public string StatusString { get; private set; }

        public string Remarks { get; set; }
        public long ManpowerVacancyNoticeCandidateId { get; set; }
        public long EmployeeId { get; set; }
        
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual ManpowerVacancyNoticeCandidateLog ManpowerVacancyNoticeCandidateLog { get; set; }
        public virtual EmployeeLog EmployeeReferrerLog { get; set; }
        
    }
}
