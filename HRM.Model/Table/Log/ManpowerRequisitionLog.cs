﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.IEntity ;
using HRM.Model.Table.IModel ;

namespace HRM.Model.Table.Log
{
    public class ManpowerRequisitionLog : IManpowerRequisition, IEntityLog<ManpowerRequisition>
    {
        public long LogId { get; set; }
        public ManpowerRequisition LogFor { get; set; }

        public long Id { get ; set ; }
        public string TrackNo { get ; set ; }
        public string Title { get ; set ; }
        public string PositionForDesignation { get ; set ; }
        public int NumberOfPersons { get ; set ; }
        public string Description { get ; set ; }
        public DateTime DateOfCreation { get ; set ; }
        public DateTime DedlineDate { get ; set ; }
        public bool IsApproved { get ; set ; }
        public long ManpowerVacancyNoticeId { get ; set ; }

        public EntityStatusEnum? Status { get; set; }

        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
    }
}
