﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class ManpowerVacancyNoticeCandidateLog : IManpowerVacancyNoticeCandidate, IEntityLog<ManpowerVacancyNoticeCandidate>
    {
        public long LogId { get; set; }
        public ManpowerVacancyNoticeCandidate LogFor { get; set; }

        public long Id { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get; private set; }
        public string TrackNo { get; set; }

        public long ManpowerVacancyNoticeId { get; set; }
        public long CandidateId { get; set; }
        public long? EmployeeId { get; set; }
        
       
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual ManpowerVacancyNoticeLog ManpowerVacancyNoticeLog { get; set; }
        public virtual CandidateLog CandidateLog { get; set; }
        public virtual EmployeeLog EmployeeLog { get; set; }
    }
}
