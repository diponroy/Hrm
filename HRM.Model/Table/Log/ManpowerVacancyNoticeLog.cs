﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel ;

namespace HRM.Model.Table.Log
{
    public class ManpowerVacancyNoticeLog : IManpowerVacancyNotice, IEntityLog<ManpowerVacancyNotice>
    {
        public long LogId { get; set; }
        public ManpowerVacancyNotice LogFor { get; set; }

        public long Id { get ; set ; }
        public DateTime? DurationFromDate { get ; set ; }
        public DateTime? DurationToDate { get ; set ; }
        public string TrackNo { get ; set ; }
        public EntityStatusEnum? Status { get ; set ; }

        public string StatusString { get { return Status.ToString(); } }
        public string Type { get ; set ; }
        public string Title { get ; set ; }
        public string Description { get ; set ; }
        public long ManpowerRequisitionId { get ; set ; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual ManpowerRequisitionLog ManpowerRequisitionLog { get; set; }
    }
}
