﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class SalaryStructureAllowanceLog : ISalaryStructureAllowance, IEntityLog<SalaryStructureAllowance>
    {
        public long LogId { get; set; }

        public long Id { get; set; }
        public long SalaryStructureId { get; set; }
        public long AllowanceId { get; set; }
        public string Remarks { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual SalaryStructureAllowance LogFor { get; set; }

        public virtual SalaryStructureLog SalaryStructureLog { get; set; }
        public virtual AllowanceLog AllowanceLog { get; set; }

    }
}
