﻿using System;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel;

namespace HRM.Model.Table.Log
{
    public class SalaryStructureBonusLog : ISalaryStructureBonus, IEntityLog<SalaryStructureBonus>
    {
        public long LogId { get; set; }
        public long Id { get; set; }
        public long SalaryStructureId { get; set; }
        public long BonusId { get; set; }
        public string Remarks { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }

        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }
        public virtual SalaryStructureBonus LogFor { get; set; }

        public virtual SalaryStructureLog SalaryStructureLog { get; set; }
        public virtual BonusLog BonusLog { get; set; }

    }
}
