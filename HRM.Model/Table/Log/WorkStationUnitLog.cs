﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity;
using HRM.Model.Table.IModel ;

namespace HRM.Model.Table.Log
{
    public class WorkStationUnitLog : IWorkStationUnit, IEntityLog<WorkStationUnit>
    {
        public long LogId { get; set; }

        public long Id { get; set; }
        public long? CompanyId { get; set; }
        public long? BranchId { get; set; }
        public long? DepartmentId { get; set; }
        public long? ParentId { get; set; }
        public DateTime? DateOfCreation { get; set; }
        public DateTime? DateOfClosing { get; set; }
        public bool IsClosed { get { return DateOfClosing != null; } }

        public long? EmployeeIdAsInCharge { get; set; }
        public string Remarks { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public LogStatusEnum? LogStatuses { get; set; }
        public virtual EmployeeLog AffectedByEmployeeLog { get; set; }

        public virtual WorkStationUnit LogFor { get; set; }

        public virtual CompanyLog CompanyLog { get; set; }
        public virtual BranchLog BranchLog { get; set; }
        public virtual DepartmentLog DepartmentLog { get; set; }

        public virtual WorkStationUnitLog ParentWorkStationUnitLog { get; set; }
    }
}
