﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class ManpowerCandidateReference : IManpowerCandidateReference, IEntityWithLog<ManpowerCandidateReferenceLog>
    {
        public long Id { get; set; }

        public EntityStatusEnum? Status { get; set; }
        public string StatusString 
        {
            get { return Status.ToString(); }
        }
        public string Remarks { get; set; }

        public long ManpowerVacancyNoticeCandidateId { get; set; }
        public virtual ManpowerVacancyNoticeCandidate ManpowerVacancyNoticeCandidate { get; set; }
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
              
        public ICollection<ManpowerCandidateReferenceLog> Logs { get; set; }
    }
}
