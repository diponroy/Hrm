﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms ;
using HRM.Model.Table.Enums ;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class ManpowerRequisition : IManpowerRequisition, IEntityWithLog<ManpowerRequisitionLog>
    {
        public long Id { get ; set ; } 
        public string TrackNo { get ; set ; }
        public string Title { get ; set ; }
        public string PositionForDesignation { get ; set ; }
        public int NumberOfPersons { get ; set ; }
        public string Description { get ; set ; }
        public DateTime DateOfCreation { get ; set ; }
        public DateTime DedlineDate { get ; set ; }
        public bool IsApproved { get ; set ; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }

        public virtual List<ManpowerVacancyNotice> ManpowerVacancyNotices { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public ICollection<ManpowerRequisitionLog> Logs { get; set; }
    }

}
