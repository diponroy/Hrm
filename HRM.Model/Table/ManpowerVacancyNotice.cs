﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime ;
using System.Text;
using System.Windows.Forms ;
using HRM.Model.Table.IEntity.Shared ;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class ManpowerVacancyNotice : IManpowerVacancyNotice, IEntityWithLog<ManpowerVacancyNoticeLog>
    {
        public long Id { get ; set ; }
        public DateTime? DurationFromDate { get ; set ; }
        public DateTime? DurationToDate { get ; set ; }
        public string TrackNo { get ; set ; }
        public EntityStatusEnum? Status { get ; set ; }
        public string StatusString { get { return Status.ToString(); } }
        public string Type { get ; set ; }
        public string Title { get ; set ; }
        public string Description { get ; set ; }

        public long ManpowerRequisitionId { get ; set ; }
        public virtual ManpowerRequisition ManpowerRequisition { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }

        public ICollection<ManpowerVacancyNoticeLog> Logs { get; set; }
        public virtual List<ManpowerVacancyNoticeCandidate> ManpowerVacancyNoticeCandidates { get; set; }
    } 
}
