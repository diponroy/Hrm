﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class ManpowerVacancyNoticeCandidate : IManpowerVacancyNoticeCandidate, IEntityWithLog<ManpowerVacancyNoticeCandidateLog>
    {
        public long Id { get; set; }
        public string TrackNo { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        
        public long ManpowerVacancyNoticeId { get; set; }
        public virtual ManpowerVacancyNotice ManpowerVacancyNotice { get; set; }

        public long CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }

        public long? EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }


        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }

        public ICollection<ManpowerVacancyNoticeCandidateLog> Logs { get; set; }
    }
}
