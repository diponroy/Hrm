﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class SalaryStructure : ISalaryStructure, IEntityWithLog<SalaryStructureLog>
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public float? Basic { get; set; }
        public string Remarks { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual ICollection<SalaryStructureLog> Logs { get; set; }
        public virtual ICollection<EmployeeSalaryStructure> EmployeeSalaryStructures { get; set; }
        public virtual ICollection<SalaryStructureAllowance> SalaryStructureAllowances { get; set; }
        public virtual ICollection<SalaryStructureBonus> SalaryStructureBonuses { get; set; }
    }
}
