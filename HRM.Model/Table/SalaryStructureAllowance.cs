﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class SalaryStructureAllowance : ISalaryStructureAllowance, IEntityWithLog<SalaryStructureAllowanceLog>
    {
        public long Id { get; set; }
        public long SalaryStructureId { get; set; }
        public long AllowanceId { get; set; }
        public string Remarks { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual SalaryStructure SalaryStructure { get; set; }
        public virtual Allowance Allowance { get; set; }
        public virtual ICollection<SalaryStructureAllowanceLog> Logs { get; set; }


    }
}
