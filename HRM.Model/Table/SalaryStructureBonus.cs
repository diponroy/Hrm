﻿using System;
using System.Collections.Generic;
using HRM.Model.Table.Enums;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class SalaryStructureBonus : ISalaryStructureBonus, IEntityWithLog<SalaryStructureBonusLog>
    {
        public long Id { get; set; }
        public long SalaryStructureId { get; set; }
        public long BonusId { get; set; }
        public string Remarks { get; set; }
        public EntityStatusEnum? Status { get; set; }

        public string StatusString {get { return Status.ToString(); } }

        public virtual Bonus Bonus { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public virtual ICollection<SalaryStructureBonusLog> Logs { get; set; }
        public virtual SalaryStructure SalaryStructure { get; set; }
    }
}
