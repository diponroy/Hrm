﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table.IEntity.Shared;
using HRM.Model.Table.IModel ;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;

namespace HRM.Model.Table
{
    public class WorkStationUnit : IWorkStationUnit, IEntityWithLog<WorkStationUnitLog>
    {
        public long Id { get ; set ; }
        public long? CompanyId { get; set; }
        public long? BranchId { get; set; }
        public long? DepartmentId { get; set; }
        public long? ParentId { get; set; }
        public DateTime? DateOfCreation { get; set; }
        public DateTime? DateOfClosing { get; set; }
        public bool IsClosed { get { return DateOfClosing != null; } }

        public long? EmployeeIdAsInCharge { get; set; }
        public string Remarks { get; set; }
        public EntityStatusEnum? Status { get; set; }
        public string StatusString { get { return Status.ToString(); } }
        public virtual Company Company { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual Department Department { get; set; }
        public virtual WorkStationUnit ParentWorkStationUnit { get; set; }

        public virtual IList<WorkStationUnit> ChildWorkStationUnits { get; set; } 
        public virtual List<EmployeeWorkStationUnit> EmployeeWorkStationUnits { get; set; }
        public virtual List<EmployeeTransfer> EmployeeTransfers { get; set; }

        public long? AffectedByEmployeeId { get; set; }
        public DateTime? AffectedDateTime { get; set; }
        public ICollection<WorkStationUnitLog> Logs { get; set; }
    }
}
