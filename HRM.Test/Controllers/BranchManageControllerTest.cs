﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table;
using HRM.Web.Controllers;
using NUnit.Framework;

namespace HRM.Test.Controllers
{
    [TestFixture]
    class BranchManageControllerTest
    {

        [Test]
        public void Create()
        {
            var controller = new BranchController();
            controller.TryToCreate(new Branch()
            {
                Name = "Mokbazer Branch.",
                DateOfCreation = DateTime.Now,
                Address = "Mokbazer Branch Dhaka.",
                Remarks = "Admmin arman asked to add"
            });
        }


        [Test]
        public void FindWithIndexAndPageSize()
        {
            var controller = new BranchController();
            var list = controller.Find(1, 25, new Branch());
        }
    }
}
