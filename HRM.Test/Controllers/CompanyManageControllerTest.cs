﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Model.Table ;
using HRM.Model.Table.Enums ;
using HRM.Web.Controllers ;
using NUnit.Framework ;

namespace HRM.Test.Controllers
{
    [TestFixture]
    class CompanyManageControllerTest
    {
        [Test]
        public void Create ()
        {
            var controller = new CompanyManageController();
            controller.Create(new Company()
            {
                Name = "Pearl Technology",
                DateOfCreation = DateTime.Parse("2014-09-28"),
                Address = "Elephant Road, Dhaka.",
                Remarks = "Admmin asked to add",
                Status = EntityStatusEnum.Active
            });
        }

        [Test]
        public void Get ()
        {
            var controller = new CompanyManageController();
            const long id = 2;
            var list = controller.Get(id);
        }
        [Test]
        public void FindWithIndexAndPageSize ()
        {
            var controller = new CompanyManageController();
            var list = controller.Find(1, 25, new Company());
        }
    }
}
