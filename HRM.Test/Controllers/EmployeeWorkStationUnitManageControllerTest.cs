﻿using System;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Web.Controllers;
using NUnit.Framework;

namespace HRM.Test.Controllers
{
    internal class EmployeeWorkStationUnitManageControllerTest
    {
        private EmployeeWorkStationUnit model = new EmployeeWorkStationUnit();
        private readonly EmployeeWorkStationUnitManageLogic logic = new EmployeeWorkStationUnitManageLogic();
        private EmployeeWorkStationUnitData unitData = new EmployeeWorkStationUnitData();


        [Test]
        public void Find()
        {
            //var controller = new EmployeeWorkStationUnitManageController();
            //var result = controller.Find(1, 25, model);

            //dynamic data = result.Data;
            //Assert.AreEqual(unitData.All().Count(), data.Count);
        }


        [Test]
        public void Create()
        {
            var controller = new EmployeeWorkStationUnitManageController();
            var result = controller.Create(new EmployeeWorkStationUnit
            {
                AttachmentDate = DateTime.Parse("2014-08-08"),
                Remarks = "Very Good",
                EmployeeId = 1,
                WorkStationUnitId = 4,
                EmployeeTypeId = 2,
                Status = EntityStatusEnum.Active
            }) as JsonResult;

            Assert.AreEqual(true, result.Data);
        }


        [Test]
        public void Get()
        {
            EmployeeWorkStationUnit data = logic.Get(7);

            DateTime? attachmentDate = data.AttachmentDate;
            DateTime? detachmentDate = data.DetachmentDate;
            var employeeId = (int) data.EmployeeId;
            var employeeTypeId = (int) data.EmployeeTypeId;
            var workStationUnitId = (int) data.WorkStationUnitId;
            string remarks = data.Remarks;
            string status = data.StatusString;

            Assert.AreEqual(true, attachmentDate != null);
            Assert.AreEqual(DateTime.Parse("2014-08-08"), attachmentDate);
            Assert.AreEqual(null, detachmentDate);
            Assert.AreEqual(1, employeeId);
            Assert.AreEqual(2, employeeTypeId);
            Assert.AreEqual(4, workStationUnitId);
            Assert.AreEqual("Very Good", remarks);
            Assert.AreEqual("Active", status);
        }


        [Test]
        public void Update()
        {
            var controller = new EmployeeWorkStationUnitManageController();
            var result = controller.Update(new EmployeeWorkStationUnit
            {
                Id = 7,
                AttachmentDate = DateTime.Parse("2014-08-08"),
                DetachmentDate = DateTime.Parse("2014-08-18"),
                Remarks = "Very Good",
                EmployeeId = 1,
                WorkStationUnitId = 4,
                EmployeeTypeId = 1,
                Status = EntityStatusEnum.Inactive
            }) as JsonResult;

            Assert.AreEqual(true, result.Data);
        }


        [Test]
        public void Delete()
        {
            var controller = new EmployeeWorkStationUnitManageController();
            var result = controller.Delete(logic.Get(7)) as JsonResult;
            Assert.AreEqual(true, result.Data);
        }
    }
}