﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using NUnit.Framework;

namespace HRM.Test.Data
{
    [TestFixture]
    public class BranchDataTest
    {
        [Test]
        public void Add()
        {
            var data = new BranchData();
            var branch = new Branch()
            {
                Name = "Jamal", 
                DateOfCreation = DateTime.Now, 
                Status = EntityStatusEnum.Active,
               // AffectedDateTime = DateTime.Now,
                AffectedByEmployeeId = 1
            };
            data.Add(branch);
            data.SaveChanges();
        }

        [Test]
        public void Remove()
        {
            var data = new BranchData();
            var branch = data.GetByName("Jamal");
            branch.AffectedByEmployeeId = 1;
            branch.AffectedDateTime = DateTime.Now;
            data.Remove(branch);
            data.SaveChanges();
        }

        [Test]
        public void Replace()
        {
            var data = new BranchData();
            var branch = data.GetByName("Jamal");
            branch.Name = "Jamal1";
            branch.AffectedByEmployeeId = 1;
            data.Replace(branch);
            data.SaveChanges();
        }

        [Test]
        public void GetAll()
        {
            var data = new BranchData();
            var list = data.All().ToList();
        }

        //[Test]
        //public void LastLog()
        //{
        //    var data = new BranchData();
        //    var list = data.FindLastLogId<BranchLog>(1);
        //}
    }
}
