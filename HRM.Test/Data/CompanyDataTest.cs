﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table ;
using HRM.Model.Table ;
using HRM.Model.Table.Enums ;
using NUnit.Framework ;

namespace HRM.Test.Data
{
    [TestFixture]
    public class CompanyDataTest
    {
        [Test]
        public void Add( )
        {
                var _data = new CompanyData();
                var aCompany = new Company()
                {
                    Name = "Test Company",
                    Address = "Test Address",
                    Remarks = "Test Remarks",
                    DateOfCreation = DateTime.Now,
                    Status = EntityStatusEnum.Active,
                    AffectedDateTime = DateTime.Now,
                    AffectedByEmployeeId = 1
                };
                _data.Add(aCompany);
                _data.SaveChanges();
           
           
        }

        //[Test]
        //public void Remove( )
        //{
        //    var _data = new CompanyData() ;
        //    var aCompany = _data.GetByName("Test Company");
        //    aCompany.AffectedByEmployeeId = 1 ;
        //    _data.Remove(aCompany);
        //    _data.SaveChanges();
        //}

        //[Test]
        //public void Replace( )
        //{
        //    var _data = new CompanyData() ;
        //    var aCompany = _data.GetByName("Test Company") ;
        //    aCompany.Name = "Company Test Replace" ;
        //    aCompany.AffectedByEmployeeId = 1 ;
        //    _data.Replace(aCompany);
        //    _data.SaveChanges();
        //}

        [Test]
        public void GetAll ()
        {
            var _data = new CompanyData();
            var list = _data.All().ToList();
        }
    }
}
