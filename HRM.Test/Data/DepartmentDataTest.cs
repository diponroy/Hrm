﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Data.Table;
using HRM.Model.Table;
using NUnit.Framework;

namespace HRM.Test.Data
{
    [TestFixture]
    public class DepartmentDataTest
    {
        [Test]
        public void GetAll()
        {
            var data = new DepartmentData();
            var list = data.All().ToList();
        }

        [Test]
        public void Add()
        {
            var data = new DepartmentData();
            var department = new Department
            {
                Name = "TQA",
                Description = "Testing and Quality Assurance Department",
                Remarks = "Good",
                DateOfCreation = DateTime.Now,
                AffectedDateTime = DateTime.Now,
                AffectedByEmployeeId = 1
            };
            data.Add(department);
            data.SaveChanges();
        }

        [Test]
        public void Replace()
        {
            var data = new DepartmentData();
            var department = data.GetByName("TQA");
            department.Name = "T&QA";
            department.DateOfClosing = DateTime.Now;
            department.AffectedDateTime = DateTime.Now;
            department.AffectedByEmployeeId = 1;
            data.Replace(department);
            data.SaveChanges();
        }

        [Test]
        public void Remove()
        {
            var data = new DepartmentData();
            var department = data.GetByName("TQA");
            department.AffectedDateTime = DateTime.Now;
            department.AffectedByEmployeeId = 1;
            data.Remove(department);
            data.SaveChanges();
        }
    }
}
