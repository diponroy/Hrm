﻿using System;
using HRM.Db;
using NUnit.Framework;

namespace HRM.Test.Db
{
    [TestFixture]
    internal class DbTest
    {
        [Test]
        public void Create()
        {
            try
            {
                new DbInitializer().DropAndCreate();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message, exception);
            }
        }
    }
}
