﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HRM.Db;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using NUnit.Framework;

namespace HRM.Test.Logic
{

    [TestFixture]
    public class EmployeeLogicTest
    {

        private EmployeeManageLogic _logic;
        private Employee _employee;

        [TestFixtureSetUp]
        public void Setup()
        {
            new DbInitializer().DropAndCreate();
            _logic = new EmployeeManageLogic();

            _employee = new Employee
            {
                TrackNo = "1231231",
                Salutation = "Mr.",
                FirstName = "Akram",
                LastName = "Ali",
                FathersName = "Shaed Ali",
                MothersName = "Lutfa Ali",
                DateOfBirth = DateTime.Parse("1983-08-11"),
                Gender = "Male",
                BloodGroup = "B+",
                Email = "Akram@gmail.com",
                ContactNo = "01710565443",
                HomePhone = "01234221399",
                OfficePhone = "345678909876",
                PresentAddress = "13/B, Park Street, North Carolina, USA.",
                PermanentAddress = "California, USA",
                DivisionOrState = "California",
                City = "San Fransisco",
                ImageDirectory = "BC/photo.jpg",
                Status = EntityStatusEnum.Active,

                AffectedByEmployeeId = 1,
                AffectedDateTime = DateTime.Now.AddDays(-10)
            };
        }


        [Test]
        public void Create()
        {
            var employee = _employee.CreateMapped<Employee, Employee>();
            _logic.Create(employee);
            _employee.Id = _employee.Id;
        }


        [Test]
        public void Update()
        {
            var anEmployee = _logic.Get(_employee.Id);
            anEmployee.AffectedByEmployeeId = 1;
            anEmployee.AffectedDateTime = DateTime.Now.AddHours(2);
            _logic.Update(anEmployee);
        }

        [Test]
        public void UpdateByItself()
        {
            var anEmployee = _logic.Get(_employee.Id);
            anEmployee.AffectedByEmployeeId = _employee.Id;
            anEmployee.AffectedDateTime = DateTime.Now.AddHours(2);
            _logic.Update(anEmployee);
        }

    }
}
