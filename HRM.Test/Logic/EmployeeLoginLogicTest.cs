﻿using System;
using HRM.Db;
using HRM.Db.Contexts;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Model.Table.Log;
using HRM.Utility;
using NUnit.Framework;

namespace HRM.Test.Logic
{
    [TestFixture]
    public class EmployeeLoginLogicTest
    {
        private EmployeeLoginLogic _logic;

        private EmployeeLogin _login;
        private string _password;


        [TestFixtureSetUp]
        public void StepUp()
        {
            new DbInitializer().DropAndCreate();
            CreateEmployee();
            _logic = new EmployeeLoginLogic();
        }

        private void CreateEmployee()
        {
            _password = "123456";

            var employee = new Employee()
            {
                TrackNo = "Faisal-13332",
                Salutation = "Mr.",
                FirstName = "Faisal",
                LastName = "Alom",
                FathersName = "Brad Pitt",
                MothersName = "Meryl Streep",
                DateOfBirth = DateTime.Parse("1983-08-11"),
                Gender = "Male",
                BloodGroup = "B+",
                Email = "Faisal@gmail.com",
                ContactNo = "01710565443",
                HomePhone = "01234221399",
                OfficePhone = "345678909876",
                PresentAddress = "13/B, Park Street, North Carolina, USA.",
                PermanentAddress = "California, USA",
                DivisionOrState = "California",
                City = "San Fransisco",
                ImageDirectory = "BC/photo.jpg",
                Status = EntityStatusEnum.Active,
            };

            var log = employee.CreateMapped<Employee, EmployeeLog>();
            log.AffectedByEmployeeId = 1;
            log.AffectedDateTime = DateTime.Now;
            log.LogStatuses = LogStatusEnum.Added;
            log.LogFor = employee;

            var db = new HrmContext();
            db.Set<EmployeeLog>().Add(log);
            db.SaveChanges();

            _login = new EmployeeLogin()
            {
                LoginName = "FaisalAlom",
                Password = _password,
                EmployeeId = employee.Id,
                Status = EntityStatusEnum.Active,
                AffectedByEmployeeId = employee.Id,
                AffectedDateTime = DateTime.Now
            };
        }

        [Test]
        public void Create()
        {
            _logic.Create(_login);
        }
    }
}
