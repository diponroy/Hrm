﻿
/*Test POST:*/
/*success*/
function add() {
    jax.postJson(
        '/UserManage/Add',
        { name: 'nito', age: 14 },
        function (data, status, xhr) {
            alert("add: " +data);
        }, 
        function (xhr, status, error) {
            alert(error);
        });;
}

/*error 404, not found*/
function edit() {
    jax.postJson(
        '/UserManage/edit',
        { name: 'nito', age: 14 },
        function (data, status, xhr) {
            alert(data);
        },
        function (xhr, status, error) {
            alert(error);
        });
}

/*Test GET:*/
/*success*/
function getFirstName() {
    jax.getJson(
        '/UserManage/GetFirstName/' + '1',
        function (data, status, xhr) {
            alert("getFirstName: " +data);
        },
        function (xhr, status, error) {
            alert(error);
        });;
}

/*error 404, not found*/
function getLastName() {
    jax.getJson(
        '/UserManage/GetLastName/' + '1',
        function (data, status, xhr) {
            alert(data);
        },
        function (xhr, status, error) {
            alert(error);
        });
}

function deferredEdit() {

    //jax.getJson(
    //    '../../WebServices/TestWebService.asmx/Edit?name=nitol&age=24',
    //    function(data, status, xhr) {
    //        alert("deferred get success: " + data);
    //    },
    //    function(xhr, status, error) {
    //        alert("deferred get error: " + error);
    //    });

    $.getJSON("../../WebServices/TestWebService.asmx/Edit", { name: 'nito', age: 14 }, function (data) {
        alert(data);
    });
}

function deferredAdd() {
    jax.postJsonDfd(
        '../../WebServices/TestWebService.asmx/Add',
        { name: 'nitol', age: 14 },
        function (data, status, xhr) {
            alert("deferred add success: " + data);
        },
        function (xhr, status, error) {
            alert("deferred add error: " + error);
        });
}

function deferredAddAnother() {
    jax.postJsonDfd(
        '../../WebServices/TestWebService.asmx/AddAnother',
        { name: 'nitol', age: 14 },
        function (data, status, xhr) {
            alert("deferred add another success: " + data);
        },
        function (xhr, status, error) {
            alert("deferred add another error: " + error);
        });
}


function addBlock() {
    //jax.defaults.blockWholeUI = true;
    jax.defaults.blockWholeUI = false;
    jax.defaults.blockContainerId = 'pageContainer';
    jax.postJsonBlock(
        '/UserManage/Add',
        { name: 'nito', age: 14 },
        function (data, status, xhr) {
            alert("add: " + data);
            ToastSuccess("Successful message update");
        },
        function (xhr, status, error) {
            alert(error);
            ToastSuccess(error);
        });;
}

$(document).ready(function () {
    //add();
    //edit();

    //getFirstName();
    //getLastName();

    deferredEdit();
    //alert('start');
    //deferredAdd();
    //deferredAddAnother();
    //alert('end');

    $('#blockButton').click(function () {
        //$.blockUI({
        //    css: {
        //        border: 'none',
        //        padding: '15px',
        //        backgroundColor: '#000',
        //        '-webkit-border-radius': '15px',
        //        '-moz-border-radius': '15px',
        //        'border-radius': '10px',
        //        opacity: .7,
        //        color: '#fff'
        //    },
        //    message: '<h3>Just a moment...</h3>'
        //});
        //setTimeout($.unblockUI, 2000);

        //addBlock();
        ToastSuccess("Successful message update");
    });
});