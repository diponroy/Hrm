﻿/*Test POST:*/

/*success*/
$.mockjax({
    url: '/UserManage/Add',
    type: "POST",
    responseTime: 2000,
    responseText: {
        IsUsed: false
    }
});




/*Test GET:*/

/*success*/
$.mockjax({
    url: '/UserManage/GetFirstName/' + '1',
    type: "GET",
    responseTime: 300,
    responseText: {
        Name: 'nitol'
    }
});



/*Test defrred:*/

/*success*/
$.mockjax({
    url: '/UserManage/Delete',
    type: "POST",
    responseTime: 300,
    responseText: {
        Name: 'nitol'
    }
});

/*success*/
$.mockjax({
    url: '/UserManage/GetName/1',
    type: "GET",
    responseTime: 7000,
    responseText: {
        Name: 'nitol'
    }
});