﻿using System.Linq;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class AllowanceController : HrmController
    {
        private readonly AllowanceData _data = new AllowanceData(new HrmContext());

        public AllowanceController()
        {
        }
        public AllowanceController ( AllowanceData data )
        {
            _data = data;
        }

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, Allowance filter)
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        }

        #endregion

        #region Create

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllowanceTypes()
        {
            return Json(new AllowanceTypeData().Detail.AllActive().ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsTitleUsed(string title)
        {
            return Json(_data.AnyActiveOrInactiveTitle(title), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(Allowance model)
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ContentResult Get ( long id )
        {
            return JsonContent(_data.Detail.Get(id));
        }

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult TitleUsedExceptItself ( string title, long id )
        {
            return Json(_data.AnyActiveOrInactiveTitleExcept(title, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update ( Allowance model )
        {
            Allowance allowance = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(allowance);
            _data.Replace(allowance);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(Allowance model)
        {
            Allowance allowance =_data.Get(model.Id);
            allowance.AffectedByEmployeeId = model.AffectedByEmployeeId;
            allowance.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(allowance);
            _data.Remove(allowance);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ContentResult GetLogs(long id)
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion

        
    }
}
