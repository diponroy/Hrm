﻿using System.Linq;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class AllowanceTypeController : HrmController
    {
        private readonly AllowanceTypeData _data = new AllowanceTypeData(new HrmContext());

        public AllowanceTypeController()
        {
        }

        public AllowanceTypeController ( AllowanceTypeData data )
        {
            _data = data;
        }

        #region list

        public ActionResult Index ()
        {
            return View("List");
        }

        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Find ( int pageNo, int pageSize, AllowanceType filter )
        {
            return Json(_data.AllActiveOrInactive().ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Create

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult IsNameUsed(string name)
        {
            return Json(_data.AnyActiveOrInactiveName(name), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(AllowanceType allowanceType)
        {
            SetAffectedDetail(allowanceType);
            _data.Add(allowanceType);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion
        

        #region Update

        public JsonResult Get ( long id )
        {
            return Json(_data.Detail.Get(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult NameUsedExceptItself(string name, long id)
        {
            return Json(_data.AnyActiveOrInactiveNameExcept(name, id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult Update(AllowanceType model)
        {
            AllowanceType allowanceType = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(allowanceType);
            _data.Replace(allowanceType);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(AllowanceType model)
        {
            AllowanceType allowanceType = _data.Get(model.Id);
            allowanceType.AffectedByEmployeeId = model.AffectedByEmployeeId;
            allowanceType.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(allowanceType);
            _data.Remove(allowanceType);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ContentResult GetLogs(long id)
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion
    }
}
