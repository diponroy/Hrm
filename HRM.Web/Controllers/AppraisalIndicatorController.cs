﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class AppraisalIndicatorController : HrmController
    {
        private readonly AppraisalIndicatorData _data=new AppraisalIndicatorData(new HrmContext());

        private readonly AppraisalIndicatorLogic _logic;
        public AppraisalIndicatorController() 
        {
        }

        protected AppraisalIndicatorController ( AppraisalIndicatorData data )
        {
            _data = data;
        }

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, AppraisalIndicator filter)
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        }

        #endregion

        #region Create

        public ActionResult Create()
        {
            return View();
        }

        public ContentResult GetActiveParents ()
        {
            return JsonContent(new AppraisalIndicatorData().AllActive().ToList());
        }

        [HttpPost]
        public JsonResult IsTitleUsed(string title)
        {
            return Json(_data.AnyActiveOrInactiveTitle(title), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create ( AppraisalIndicator model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }
            
            SetAffectedDetail(model);
            model.ParentId = ( model.Status == EntityStatusEnum.Inactive ) ? null : model.ParentId;  
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get(long id)
        {
            return JsonContent(_data.Detail.Get(id));
        }

        public ContentResult GetAssignableParents ( long id )
        {
            return JsonContent(_logic.GetAssignableParents(id));
        } 

        [HttpPost]
        public JsonResult TitleUsedExceptItself(string title, long id)
        {
            return Json(_data.AnyActiveOrInactiveTitleExcept(title, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update ( AppraisalIndicator model )
        {
            AppraisalIndicator appraisalIndicator = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(appraisalIndicator);
            _data.Replace(appraisalIndicator);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        public ActionResult CheckBeforeDelete ( long id )
        {
            return Json(_logic.HasAnyActiveChild(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( AppraisalIndicator model )
        {
            AppraisalIndicator appraisalIndicator = _data.Get(model.Id);
            appraisalIndicator.AffectedByEmployeeId = model.AffectedByEmployeeId;
            appraisalIndicator.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(appraisalIndicator);
            _data.Remove(appraisalIndicator);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Detail

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_data.Detail.AllLog(id));
        }

        #endregion
    }
}
