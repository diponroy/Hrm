﻿using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class BonusController : HrmController
    {
        private readonly BonusLogic _logic;

        public BonusController (): this(new BonusLogic())
        {
        }

        protected BonusController ( BonusLogic logic )
        {
            _logic = logic;
        }

        public ContentResult Get(long id)
        {
            return JsonContent(_logic.Get(id));
        }

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, Bonus filter)
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }

        #endregion

        #region Create

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetBonusTypes()
        {
            return Json(_logic.GetBonusTypes(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsTitleUsed(string title)
        {
            return Json(_logic.HasTitleUsed(title), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(Bonus bonus)
        {
            SetAffectedDetail(bonus);
            _logic.Create(bonus);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(Bonus bonus)
        {
            SetAffectedDetail(bonus);
            _logic.Remove(bonus);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }

        #endregion

        #region Update

        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult TitleUsedExceptItself(string title, long id)
        {
            return Json(_logic.HasTitleUsedExcept(title, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(Bonus model)
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
