﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;
using Microsoft.Ajax.Utilities;

namespace HRM.Web.Controllers
{
    public class BonusPaymentDateController : HrmController
    {
        private readonly BonusPaymentDateLogic _logic;
        public  BonusPaymentDateController():this(new BonusPaymentDateLogic())
        {
        
        }
        public ContentResult Get ( long id )
        {
            return JsonContent(_logic.Get(id));
        }

        protected BonusPaymentDateController(BonusPaymentDateLogic logic)
        {
            _logic = logic;
        }
        public ActionResult Create ()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Create ( BonusPaymentDate bonusPaymentDate )
        {
            SetAffectedDetail(bonusPaymentDate);
            _logic.Create(bonusPaymentDate);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetBonusTitles ()
        {
            return Json(_logic.GetBonusTitles(),JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            return View("List");
        }
        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo,int pageSize,BonusPaymentDate filter)
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }
        #region Delete

        [HttpPost]
        public ActionResult Delete(BonusPaymentDate bonusPaymentDate)
        {
            SetAffectedDetail(bonusPaymentDate);
            _logic.Remove(bonusPaymentDate);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }

        #endregion

        #region Update

        public ActionResult Update(long id)
        {
            return View(id);
        }
        
        [HttpPost]
        public JsonResult Update(BonusPaymentDate bonusPaymentDate)
        {
            SetAffectedDetail(bonusPaymentDate);
            _logic.Update(bonusPaymentDate);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
