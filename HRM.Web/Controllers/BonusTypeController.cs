﻿using System ;
using System.Collections.Generic ;
using System.Linq ;
using System.Web ;
using System.Web.Mvc ;
using HRM.Logic ;
using HRM.Model.Table ;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class BonusTypeController : HrmController
    {
        public readonly BonusTypeLogic _logic ;

        public BonusTypeController( ) : this(new BonusTypeLogic())
        {
        }

        protected BonusTypeController( BonusTypeLogic logic )
        {
            _logic = logic ;
        }

        #region Create

        public ActionResult Create( )
        {
            return View() ;
        }

        [HttpPost]
        public JsonResult IsNameUsed( string name )
        {
            return Json(_logic.HasNameUsed(name) , JsonRequestBehavior.AllowGet) ;
        }

        [HttpPost]
        public JsonResult Create( BonusType bonusType )
        {
            SetAffectedDetail(bonusType) ;
            _logic.Create(bonusType) ;
            return Json(true , JsonRequestBehavior.AllowGet) ;
        }

        #endregion

        #region list

        public ActionResult Index( )
        {
            return View("List") ;
        }

        public ActionResult List( )
        {
            return View() ;
        }

        [HttpPost]
        public JsonResult Find ( int pageNo, int pageSize, BonusType filter )
        {
            return Json(_logic.Find(pageNo , pageSize , filter) , JsonRequestBehavior.AllowGet) ;
        }

        #endregion

        public JsonResult Get( long id )
        {
            return Json(_logic.Get(id) , JsonRequestBehavior.AllowGet) ;
        }

        #region Update

        [HttpPost]
        public JsonResult NameUsedExceptItself( string name , long id )
        {
            return Json(_logic.HasNameUsedExcept(name , id) , JsonRequestBehavior.AllowGet) ;
        }

        [HttpGet]
        public ActionResult Update( long id )
        {
            return View(id) ;
        }

        [HttpPost]
        public JsonResult Update ( BonusType model )
        {
            SetAffectedDetail(model) ;
            _logic.Update(model) ;
            return Json(true , JsonRequestBehavior.AllowGet) ;
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete ( BonusType model )
        {
            SetAffectedDetail(model) ;
            _logic.Remove(model) ;
            return Json(true , JsonRequestBehavior.AllowGet) ;
        }

        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail( long id )
        {
            return View(id) ;
        }

        [HttpGet]
        public ContentResult GetLogs( long id )
        {
            return JsonContent(_logic.GetLogs(id)) ;
        }

        #endregion
    }
}
