﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class BranchController : HrmController
    {
        private readonly BranchLogic _logic;

        public BranchController() : this(new BranchLogic())
        {
            
        }
        public BranchController(BranchLogic logic)
        {
            _logic = logic;
        }

        #region Views
        // GET: /BranchManage/
        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Update(long id)
        {
            return View(id);
        }

        public ActionResult Detail(long id)
        {
            return View(id);
        }
        #endregion

        #region Create
        [HttpPost]
        public JsonResult IsBranchNameUsed(string name)
        {
            return Json(_logic.HasName(name), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToCreate(Branch branch)
        {
            SetAffectedDetail(branch);
            _logic.Create(branch);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update
        [HttpGet]
        public JsonResult Get(long id)
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsBranchNameUsedExceptBranch(string name, long branchId)
        {
            return Json(_logic.HasNameExceptBranch(name, branchId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToUpdate(Branch branch)
        {
            SetAffectedDetail(branch);
            _logic.Update(branch);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region List
        [HttpPost]
        public JsonResult Find(int pageNo, int pageSize, Branch filter)
        {
            return Json(data: _logic.Find(pageNo, pageSize, filter), behavior: JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var branch = new Branch(){Id = id};
            SetAffectedDetail(branch);
            _logic.Delete(branch);
            return Json(true, behavior: JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Detail
        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }
        #endregion
    }
}
