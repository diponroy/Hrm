﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;

namespace HRM.Web.Controllers
{
    public class CandidateBasicController : Controller
    {
        private readonly CandidateData _data;

        public CandidateBasicController()
            : this(new CandidateData(new HrmContext()))
        {
        }

        public CandidateBasicController(CandidateData data)
        {
            _data = data;
        }

        public ActionResult CreateBasic()
        {
            return View();
        }

        #region Create Basic

        [HttpPost]
        public JsonResult IsEmailUsed(string email)
        {
            return Json(_data.IsEmailUsed(email), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToCreateBasic(Candidate candidate)
        {
            candidate.TrackNo = _data.GenerateTrackNo();
            candidate.AffectedDateTime = DateTime.Now;
            candidate.Status = EntityStatusEnum.Active;
            _data.Add(candidate);
            _data.SaveChanges();
            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
