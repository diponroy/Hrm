﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class CandidateDashboardController : HrmCandidateController
    {
        //
        // GET: /CandidateDashboard/

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        /*id is the notice id*/
        public JsonResult ApplyForNotice(long id)
        {
            var entity = new ManpowerVacancyNoticeCandidate();
            SetAffectedDetail(entity);
            entity.Status = EntityStatusEnum.Active;
            entity.ManpowerVacancyNoticeId = id;
            entity.CandidateId = GetSessionModel().CandidateId;

            var data = new ManpowerVacancyNoticeCandidateData(new HrmContext());
            entity.TrackNo = data.GenerateTrackNo();
            data.Add(entity);
            data.SaveChanges();

            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ContentResult GetNoticesForCandidate()
        {
            var candidateId = GetSessionModel().CandidateId;

            var db = new HrmContext();
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = true;
            var dateTime = DateTime.Now.Date;
            var notices =
                db.GetTable<ManpowerVacancyNotice>()
                    .Where(x => x.Status == EntityStatusEnum.Active)
                    .Where(x => x.DurationFromDate <= dateTime)
                    .Where(x => x.DurationToDate >= dateTime)
                    .Except(
                        db.GetTable<ManpowerVacancyNoticeCandidate>()
                            .Where(x => x.CandidateId == candidateId)
                            .Include(x => x.ManpowerVacancyNotice)
                            .Select(x => x.ManpowerVacancyNotice)
                    )
                    .ToList();
            return JsonContent(notices);
        }


    }
}
