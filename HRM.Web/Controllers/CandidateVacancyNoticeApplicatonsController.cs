﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class CandidateVacancyNoticeApplicatonsController : HrmCandidateController
    {
        private readonly ManpowerVacancyNoticeCandidateData _data;

        public CandidateVacancyNoticeApplicatonsController()
            : this(new ManpowerVacancyNoticeCandidateData(new HrmContext()))
        {
        }

        public CandidateVacancyNoticeApplicatonsController(ManpowerVacancyNoticeCandidateData data)
        {
            _data = data;
        }

        #region Views
        public ActionResult Index()
        {
            return Applications();
        }

        public ActionResult Applications()
        {
            var candidateId = GetSessionModel().CandidateId;
            return View(candidateId);
        } 
        #endregion

        #region Applications
        [HttpGet]
        public ContentResult GetApplications()
        {
            var candidateId = GetSessionModel().CandidateId;
            var applications =
                _data.AllActive()
                    .Where(x => x.CandidateId == candidateId)
                    .Include(x => x.ManpowerVacancyNotice)
                    .ToList();
            return JsonContent(applications);
        }

        [HttpPost]
        public JsonResult RemoveApplication(long id)
        {
            var entity = _data.Get(id);
            SetAffectedDetail(entity);
            _data.Remove(entity);
            _data.SaveChanges();
            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }
        
        #endregion
    }
}
