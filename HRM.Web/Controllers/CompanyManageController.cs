﻿using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table ;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class CompanyManageController : HrmController
    {
        public readonly CompanyManageLogic _logic;

        public CompanyManageController()
            : this(new CompanyManageLogic())
        {
        }

        protected CompanyManageController(CompanyManageLogic logic)
        {
            _logic = logic;
        }

        [HttpGet]
        public JsonResult Get(long id)
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        #region Create

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult IsNameUsed(string name)
        {
            return Json(_logic.HasNameUsed(name), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(Company model)
        {
            SetAffectedDetail(model);
            _logic.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region ListView

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Find(int pageNo, int pageSize, Company filter)
        {
            return Json(_logic.Find(pageNo, pageSize, filter), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update 

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult NameUsedExceptItself(string name, long companyId)
        {
            return Json(_logic.NameUsedExceptItself(name, companyId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Update(Company model)
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(Company model)
        {
            SetAffectedDetail(model);
            _logic.Remove(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }

        #endregion
    }
}
