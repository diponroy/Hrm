﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic ;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class DepartmentManageController : HrmController
    {
        private readonly DepartmentManageLogic _logic;

        public DepartmentManageController() : this(new DepartmentManageLogic())
        {
        }

        protected DepartmentManageController(DepartmentManageLogic logic)
        {
            _logic = logic;
        }

        #region List
        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Find(int pageNo, int pageSize, Department filter)
        {
            return Json(data: _logic.Find(pageNo, pageSize, filter), behavior: JsonRequestBehavior.AllowGet);
        }     
        #endregion

        #region Create

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }  
        [HttpPost]
        public ActionResult Create(Department model)
        {
            SetAffectedDetail(model);
            _logic.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        } 
        [HttpPost]
        public JsonResult IsNameUsed(string name)
        {
            return Json(_logic.NameUsed(name), JsonRequestBehavior.AllowGet);
        }

        #endregion
        
        #region Update

        public JsonResult Get(long id)
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult NameUsedExceptDepartment(long id, string name)
        {
            return Json(_logic.NameUsedExceptDepartment(id, name), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult Update(Department model)
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(Department model)
        {
            SetAffectedDetail(model);
            _logic.Delete(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }

        #endregion
    }
}
