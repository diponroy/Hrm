﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;
using Microsoft.Ajax.Utilities;
using WebGrease.Css.Extensions;

namespace HRM.Web.Controllers
{
    public class EmployeeAchievementManageController : HrmController
    {
        private readonly EmployeeAssignedAppraisalData _employeeAssignedAppraisalData = new EmployeeAssignedAppraisalData();

        private readonly EmployeeAchievementData _data = new EmployeeAchievementData();
        private readonly AppraisalIndicatorData _appraisalData = new AppraisalIndicatorData();

        public EmployeeAchievementManageController ()
        {
        }

        protected EmployeeAchievementManageController(EmployeeAchievementData data)
        {
            _data = data;
        }

        #region View

        [HttpGet]
        public ActionResult Create(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult UpdateIndividual(long id)
        {
            return View(id);
        }

        #endregion

        #region List

        public JsonResult Delete(EmployeeAchievement model)
        {
            EmployeeAchievement empAchievement = _data.Get(model.Id);
            empAchievement.AffectedByEmployeeId = model.AffectedByEmployeeId;
            empAchievement.AffectedDateTime = model.AffectedDateTime;

            _data.Remove(empAchievement);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Create

        [HttpGet]
        public ContentResult Get(long id)
        {
            EmployeeAchievement ac = _data.Detail.Get(id);
            return JsonContent(ac);
        }

        [HttpGet]
        public ContentResult GetAppraisals (long id)
        {
            List<AppraisalIndicator> list = _employeeAssignedAppraisalData.AllActive()
                .Include(x => x.EmployeeWorkStationUnit)
                .Include(x => x.EmployeeWorkStationUnit.Employee)
                .Include(x => x.AppraisalIndicator)
                .Where(x => x.EmployeeWorkStationUnit.Employee.Id == id)
                .Select(x => x.AppraisalIndicator).ToList();
            return JsonContent(list);
        }

        [HttpPost]
        public JsonResult IsTitleUsed ( string title )
        {
            var isUsed = _data.AnyActiveOrInactiveTitle(title);
            if (isUsed)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create ( EmployeeAchievement model )
        {
            SetAffectedDetail(model);
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status should not be removed.");
            }

            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Upload()
        {
            string fileName = null;
            int fileSize;

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];   //Uploaded file
                fileSize = file.ContentLength;
                fileName = file.FileName;
                Stream fileContent = file.InputStream;

                string path = Server.MapPath("~/Uploaded Files/" + fileName);
                string savedPath = @"Uploaded Files/" + fileName;

                if (CheckFileType(fileName))
                {
                    file.SaveAs(path);
                    return Json(savedPath);
                }
            }
            return Json("");
        }

        protected bool CheckFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            if (ext.ToLower() != ".zip" && ext.ToLower() != ".rar" && ext.ToLower() != ".7zip")
            {
                return false;
            }
            return true;
        }

        #endregion

        #region Update

        [HttpPost]
        public ContentResult GetAchievements(long id)
        {
            return JsonContent( _data.GetByEmployeeId(id)
                .Include(x => x.EmployeeAppraisal.EmployeeAssignedAppraisal.AppraisalIndicator).ToList());
        }
  
        [HttpPost]
        public ActionResult Update (EmployeeAchievement model )
        {
            SetAffectedDetail(model);
            
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status should not be removed.");
            }
            var data = new EmployeeAchievementData();
            EmployeeAchievement empAchievement = data.Get(model.Id);
            model.MapTo(empAchievement);

            _data.Replace(empAchievement);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion  

        #region Delete

        [HttpPost]
        public JsonResult Delete(long id)
        {
            EmployeeAchievement achievement = _data.Get(id);
            SetAffectedDetail(achievement);
            _data.Remove(achievement);
            _data.SaveChanges();
            return Json(true, behavior: JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion
    }
}
