﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeAppraisalManageController : HrmController
    {
        private readonly EmployeeAppraisalManageLogic _logic;

        public EmployeeAppraisalManageController() : this(new EmployeeAppraisalManageLogic())
        { 
        }

        protected EmployeeAppraisalManageController(EmployeeAppraisalManageLogic logic)
        {
            _logic = logic;
        }

        #region ListView 
        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        public ContentResult Find(int pageNo, int pageSize, EmployeeAppraisal filter)
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }
        #endregion
        #region Create

        public JsonResult GetEmployeeAssignedAppraisalTypes ()
        {
            return Json(_logic.GetEmployeeAssignedAppraisalTypes(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Create ()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Create ( EmployeeAppraisal employeeAppraisal )
        {
            SetAffectedDetail(employeeAppraisal);
            _logic.Create(employeeAppraisal);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Update
        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_logic.Get(id));
        }

        [HttpPost]
        public JsonResult Update ( EmployeeAppraisal model )
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Delete
        
        [HttpPost]
        public ActionResult Delete ( EmployeeAppraisal employeeAppraisal )
        {
            SetAffectedDetail(employeeAppraisal);
            _logic.Delete(employeeAppraisal);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Detail
        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_logic.GetLogs(id));
        }
        #endregion 
    }
}
