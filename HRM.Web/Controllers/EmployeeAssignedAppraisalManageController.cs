﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.History;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeAssignedAppraisalManageController : HrmController
    {
        private readonly EmployeeAssignedAppraisalData _data = new EmployeeAssignedAppraisalData(new HrmContext());

        public EmployeeAssignedAppraisalManageController ()
        {
        }

        public EmployeeAssignedAppraisalManageController(EmployeeAssignedAppraisalData data)
        {
            _data = data;
        }

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, EmployeeAssignedAppraisal filter)
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        }

        #endregion

        #region Create

        public ContentResult GetAppraisalIndicatorTitles ()
        {
            return JsonContent(new AppraisalIndicatorData().Detail.AllActive().ToList());
        }
        public ContentResult GetEmployeeWorkStationUnits ()
        {
            return JsonContent(new EmployeeWorkStationUnitData().Detail.AllActive().ToList());
        } 

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create ( EmployeeAssignedAppraisal employeeAssignedAppraisal )
        {
            SetAffectedDetail(employeeAssignedAppraisal);
            _data.Add(employeeAssignedAppraisal);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_data.Detail.Get(id));
        }

        [HttpPost]
        public JsonResult Update ( EmployeeAssignedAppraisal model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            EmployeeAssignedAppraisal employeeAssignedAppraisal = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(employeeAssignedAppraisal);
            _data.Replace(employeeAssignedAppraisal);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete 

        [HttpPost]
        public ActionResult Delete ( EmployeeAssignedAppraisal model )
        {
            EmployeeAssignedAppraisal employeeAssignedAppraisal = _data.Get(model.Id);
            employeeAssignedAppraisal.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeAssignedAppraisal.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(employeeAssignedAppraisal);
            _data.Remove(employeeAssignedAppraisal);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }  
        #endregion

        #region Detail

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion
    }
}
