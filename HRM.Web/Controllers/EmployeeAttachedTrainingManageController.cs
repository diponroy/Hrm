﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeAttachedTrainingManageController : HrmController
    {
        private readonly EmployeeAttachedTrainingManageLogic _logic;

        public EmployeeAttachedTrainingManageController() : this(new EmployeeAttachedTrainingManageLogic())
        { 
        }
        protected EmployeeAttachedTrainingManageController (EmployeeAttachedTrainingManageLogic logic)
        {
            _logic = logic;
        }
        public ActionResult Index()
        {
            return View("List");
        }
        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find ( int pageNo, int pageSize, EmployeeAttachedTraining filter )
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }

        public JsonResult GetEmployee ()
        {
            return Json(_logic.GetEmployee(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTrainingTitles ()
        {
            return Json(_logic.GetTrainingTitles(), JsonRequestBehavior.AllowGet);
        }   
        public ActionResult Create ()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Create ( EmployeeAttachedTraining employeeAttachedTraining )
        {
            SetAffectedDetail(employeeAttachedTraining);
            _logic.Create(employeeAttachedTraining);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_logic.Get(id));
        }

        [HttpPost]
        public JsonResult Update ( EmployeeAttachedTraining model )
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( EmployeeAttachedTraining employeeAttachedTraining )
        {
            SetAffectedDetail(employeeAttachedTraining);
            _logic.Remove(employeeAttachedTraining);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_logic.GetLogs(id));
        }

    }
}
