﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeAttendanceController : HrmController
    {
        private readonly EmployeeAttendanceData _data = new EmployeeAttendanceData(new HrmContext());

        public EmployeeAttendanceController()
        {
        }
        public EmployeeAttendanceController ( EmployeeAttendanceData data )
        {
            _data = data;
        }

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find ( int pageNo, int pageSize, EmployeeAttendance filter )
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        } 
        #endregion

        #region Create

        public ActionResult Create ()
        {
            return View();
        }

        public ContentResult GetEmployees ()
        {
            var list = new EmployeeWorkStationUnitData().Detail.AllActive().ToList();
            return JsonContent(list);

        }

        [HttpPost]
        public JsonResult Create ( EmployeeAttendance model )
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ContentResult Get ( long id )
        {
            return JsonContent(_data.Detail.Get(id));
        }

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult Update ( EmployeeAttendance model )
        {
            EmployeeAttendance employeeAttendance = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(employeeAttendance);
            _data.Replace(employeeAttendance);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete ( EmployeeAttendance model )
        {
            EmployeeAttendance employeeAttendance = _data.Get(model.Id);
            employeeAttendance.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeAttendance.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(employeeAttendance);
            _data.Remove(employeeAttendance);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Detail

        [HttpGet]
        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        [HttpGet]
        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion
    }
}
