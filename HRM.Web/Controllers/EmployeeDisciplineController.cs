﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeDisciplineController : HrmController
    {
        private readonly EmployeeDisciplineData _data = new EmployeeDisciplineData(new HrmContext());

        public EmployeeDisciplineController()
        {
        }
        public EmployeeDisciplineController ( EmployeeDisciplineData data )
        {
            _data = data;
        }

        #region List

        public ActionResult Index ()
        {
            return View("List");
        }

        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find ( int pageNo, int pageSize, EmployeeDiscipline filter )
        {
            var data = _data.Detail.AllActiveOrInactive().ToList();
            return JsonContent(data);
        }

        #endregion

        #region Create

        public ActionResult Create ()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Upload ()
        {
            string fileName = null;
            //int fileSize;

            for(int i = 0;i < Request.Files.Count;i++)
            {
                HttpPostedFileBase file = Request.Files[i];   //Uploaded file
                //fileSize = file.ContentLength;
                fileName = file.FileName;
                Stream fileContent = file.InputStream;

                //string path = Path.Combine(@"E:\HRM\HRM\HRM.Web\Uploaded Files\All Images", fileName);
                string path = Server.MapPath("~/Uploaded Files/All Files/" + fileName);

                if(CheckFileType(fileName))
                {
                    file.SaveAs(path);
                    return Json(path);
                }

            }
            return Json("Error occured while uploading!!");
        }

        protected bool CheckFileType ( string fileName )
        {
            string ext = Path.GetExtension(fileName);

            switch(ext.ToLower())
            {
                case ( ".gif" ):
                    { return true; }
                case ( ".png" ):
                    { return true; }
                case ( ".jpg" ):
                    { return true; }
                case ( ".jpeg" ):
                    { return true; }
                case ( ".doc" ):
                    {
                        return true;
                    }
                case ( ".docx" ):
                    {
                        return true;
                    }
                case ( ".pdf" ):
                    {
                        return true;
                    }
                default:
                    { return false; }
            }
        }

        public ContentResult GetEmployees ()
        {
            return JsonContent(new EmployeeData(new HrmContext()).Detail.AllActive().ToList());
        }

        public ContentResult GetDepartments ()
        {
            return JsonContent(new DepartmentData(new HrmContext()).Detail.AllActive().ToList());
        }

        [HttpPost]
        public JsonResult Create ( EmployeeDiscipline model )
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_data.Detail.Get(id));
        }

        [HttpPost]
        public JsonResult Update ( EmployeeDiscipline model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            EmployeeDiscipline employeeDiscipline = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(employeeDiscipline);
            _data.Replace(employeeDiscipline);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete ( EmployeeDiscipline model )
        {
            EmployeeDiscipline employeeDiscipline = _data.Get(model.Id);
            employeeDiscipline.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeDiscipline.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(employeeDiscipline);
            _data.Remove(employeeDiscipline);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion

    }
}
