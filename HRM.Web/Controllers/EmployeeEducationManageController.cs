﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System;
using System.Web.Mvc ;
using HRM.Logic ;
using HRM.Model.Table ;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeEducationManageController : HrmController
    {
        public readonly EmployeeEducationManageLogic _logic ;

        public EmployeeEducationManageController( )
            : this(new EmployeeEducationManageLogic())
        {
        }

        protected EmployeeEducationManageController( EmployeeEducationManageLogic logic )
        {
            _logic = logic ;
        }

        #region List


        [HttpPost]
        public JsonResult GetEducations(long id)
        {
            return Json(_logic.GetByEmployeeId(id));
        }
        #endregion

        #region Create

        [HttpGet]
        public ActionResult Create(long id)
        {
            return View(id) ;
        }

        [HttpPost]
        public JsonResult Create(EmployeeEducation employeeEducation)
        {
            SetAffectedDetail(employeeEducation);
            _logic.Create(employeeEducation);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Upload()
        {
            string fileName = null;
            int fileSize;

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];   //Uploaded file
                fileSize = file.ContentLength;
                fileName = file.FileName;
                Stream fileContent = file.InputStream;

                //string path = Path.Combine(@"E:\HRM\HRM\HRM.Web\Uploaded Files", fileName);
                string path = Server.MapPath("~/Uploaded Files/" + fileName);
                string savedPath = @"Uploaded Files/" + fileName;

                if (CheckFileType(fileName))
                {
                    file.SaveAs(path);
                    return Json(savedPath);
                }
            }
            return Json("");
        }

        protected bool CheckFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);

            switch (ext.ToLower())
            {
                case (".gif"):
                    { return true; }
                case (".png"):
                    { return true; }
                case (".jpg"):
                    { return true; }
                case (".jpeg"):
                    { return true; }
                default:
                    { return false; }
            }
        }

        #endregion

        #region Update
        
        
        public JsonResult Get(long id)
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id) ;
        }

        [HttpGet]
        public ActionResult UpdateIndividual(long id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult Update(EmployeeEducation employeeEducation)
        {
            SetAffectedDetail(employeeEducation);
            _logic.Update(employeeEducation);
            return Json(true , JsonRequestBehavior.AllowGet) ;
        }

        #endregion

        #region Delete
        [HttpPost]
        public JsonResult Delete ( long id )
        {
            var empEducation = new EmployeeEducation() { Id = id };
            SetAffectedDetail(empEducation);
            _logic.Delete(empEducation);
            return Json(true, behavior: JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult DetailIndividual(long id)
        {
            return View(id);
        }

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }
        #endregion
    }
}
