﻿using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeLoginController : HrmController
    {
        private readonly EmployeeLoginLogic _logic;

        public EmployeeLoginController()
            : this(new EmployeeLoginLogic())
        {
        }

        protected EmployeeLoginController(EmployeeLoginLogic logic)
        {
            _logic = logic;
        }

        public ActionResult Index()
        {
            return View("Login");
        }

        public ActionResult Login()
        {
            base.AbandonSession();
            return View();
        }
        public ActionResult LogOut()
        {
            base.AbandonSession();
            return View("Login");
        }

        [HttpPost]
        public ActionResult TryToCreateSession(EmployeeLogin login)
        {
            EmployeeLogin employeeLogin = _logic.FindActiveLogin(login);
            bool hasLogin = employeeLogin != null;
            if (hasLogin)
            {
                SetEmployeeLoginSession(employeeLogin);
            }
            return Json(hasLogin, JsonRequestBehavior.AllowGet);
        }
    }
}
