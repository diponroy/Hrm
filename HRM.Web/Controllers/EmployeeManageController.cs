﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeManageController : HrmController
    {
        private readonly EmployeeManageLogic _logic;


        public EmployeeManageController() : this(new EmployeeManageLogic())
        {
        }

        protected EmployeeManageController(EmployeeManageLogic logic)
        {
            _logic = logic;
        }

        [HttpPost]
        public JsonResult Find(int pageNo, int pageSize, Employee filter)
        {
            return Json(data: _logic.Find(pageNo, pageSize, filter), behavior: JsonRequestBehavior.AllowGet);
        }

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        #endregion

        #region Create

        [HttpGet]
        public ActionResult CreateBasic()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CreateAdditional(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult GenerateTrackNo()
        {
            return Json(_logic.GenerateTrackNo(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsEmailUsed(string email)
        {
            return Json(_logic.HasEmail(email), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Create(Employee employeeBasic)
        {
            SetAffectedDetail(employeeBasic);
            _logic.Create(employeeBasic);
            return Json(employeeBasic.Id, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Upload()
        {
            string fileName = null;
            int fileSize;

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                fileSize = file.ContentLength;
                fileName = file.FileName;
                Stream fileContent = file.InputStream;

                //string path = Path.Combine(@"E:\HRM\HRM\HRM.Web\Uploaded Files", fileName);
                string path = Server.MapPath("~/Uploaded Files/" + fileName);
                string savedPath = @"Uploaded Files/" + fileName;

                if (CheckFileType(fileName))
                {
                    file.SaveAs(path);
                    return Json(savedPath);
                }
            }
            return Json("Error occured while uploading!!");
        }

        protected bool CheckFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);

            switch (ext.ToLower())
            {
                case (".gif"):
                {
                    return true;
                }
                case (".png"):
                {
                    return true;
                }
                case (".jpg"):
                {
                    return true;
                }
                case (".jpeg"):
                {
                    return true;
                }
                default:
                {
                    return false;
                }
            }
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(Employee model)
        {
            SetAffectedDetail(model);
            _logic.Delete(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ContentResult Get(long id)
        {
            return JsonContent(_logic.Get(id));
        }

        [HttpPost]
        public JsonResult IsEmailUsedExceptEmployee(long id, string email)
        {
            return Json(_logic.IsEmailUsedExceptEmployee(id, email), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult UpdateBasic(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult UpdateAdditional(long id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult Update(Employee employeeBasic)
        {
            SetAffectedDetail(employeeBasic);
            _logic.Update(employeeBasic);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAdditional(long id, Employee employeeAdditional)
        {
            SetAffectedDetail(employeeAdditional);
            _logic.UpdateAdditional(id, employeeAdditional);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult AdditionalDetail(long id)
        {
            return View(id);
        }

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }

        #endregion
    }
}