﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic ;
using HRM.Model.Table ;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeMembershipManageController : HrmController
    {
        public readonly EmployeeMembershipManageLogic _logic ;

        public EmployeeMembershipManageController( )
            : this(new EmployeeMembershipManageLogic())
        {
        }

        protected EmployeeMembershipManageController (EmployeeMembershipManageLogic logic )
        {
            _logic = logic ;
        }

        #region Create

        [HttpGet]
        public ActionResult Create (long id)
        {
            return View(id);
        }

        [HttpGet]
        public JsonResult GetEmployees ()
        {
            return Json(_logic.GetEmployees(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create ( EmployeeMembership model )
        {
            SetAffectedDetail(model);
            _logic.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        [HttpGet]
        public JsonResult Get ( long id )
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetMemberships(long id)
        {
            return Json(_logic.GetByEmployeeId(id));
        }

        [HttpGet]
        public ActionResult Update ( long id )
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult UpdateIndividual(long id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult Update ( EmployeeMembership model )
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public JsonResult Delete(long id)
        {
            EmployeeMembership membership = _logic.Get(id);
            SetAffectedDetail(membership);
            _logic.Delete(membership);
            return Json(true, behavior: JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
