﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;
using HRM.Web.Controllers.Table;
using Microsoft.Ajax.Utilities;

namespace HRM.Web.Controllers
{
    public class EmployeePromotionController : HrmController
    {
        private readonly EmployeePromotionData _data = new EmployeePromotionData();
        private readonly EmployeeTypeData _employeeTypeData = new EmployeeTypeData();
        private readonly WorkStationUnitData _workStationUnitData = new WorkStationUnitData();
        private readonly EmployeeWorkStationUnitData _employeeWorkStationUnitData = new EmployeeWorkStationUnitData();
        private readonly SalaryStructureData _salaryStructureData = new SalaryStructureData();
        private readonly EmployeeSalaryStructureData _employeeSalaryStructureData = new EmployeeSalaryStructureData();
        private readonly EmployeeSalaryStructureAllowanceData _employeeSalaryStructureAllowanceData = new EmployeeSalaryStructureAllowanceData();
        private readonly EmployeeSalaryStructureBonusData _employeeSalaryStructureBonusData = new EmployeeSalaryStructureBonusData();
        private readonly IncrementSalaryStructureData _incrementSalaryStructureData = new IncrementSalaryStructureData();

        public EmployeePromotionController()
        {
        }

        protected EmployeePromotionController(EmployeePromotionData data)
        {
            _data = data;
        }

        #region View

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Update(long id)
        {
            return View(id);
        }

        public ActionResult Detail(long id)
        {
            return View(id);
        }

        #endregion

        #region List

        [HttpPost]
        public ContentResult Find(long pageNo, long pageSize, EmployeePromotion filter)
        {
            List<EmployeePromotion> list = _data.Detail.AllActiveOrInactive()
                .Include(x=>x.EmployeeWorkStationUnit.Employee)
                .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit)
                .Include(x=>x.EmployeeWorkStationUnit.WorkStationUnit.Company)
                .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Branch)
                .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Department)
                .Include(x=>x.WorkStationUnit.Company)
                .Include(x => x.WorkStationUnit.Branch)
                .Include(x => x.WorkStationUnit.Department)
                .ToList();
            return JsonContent(list);
        }

        public JsonResult Delete(EmployeePromotion entity)
        {
            SetAffectedDetail(entity);
            EmployeePromotion aPromotion = _data.Get(entity.Id);
            aPromotion.AffectedByEmployeeId = entity.AffectedByEmployeeId;
            aPromotion.AffectedDateTime = entity.AffectedDateTime;

            _data.Remove(aPromotion);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Create

        [HttpPost]
        public ContentResult GetEmployeeWorkStationUnits()
        {
            var employeeWorkstationUnits = _employeeWorkStationUnitData.Detail.AllActive().ToList();
            return JsonContent(employeeWorkstationUnits);
        }

        [HttpPost]
        public ContentResult GetWorkStationUnit(long id)
        {
            return JsonContent(_workStationUnitData.Detail.Get(id));
        }

        [HttpPost]
        public ContentResult GetWorkStationUnits()
        {
            var list = _workStationUnitData.Detail.AllActive().ToList();
            return JsonContent(list);
        }

        [HttpPost]
        public ContentResult GetTypeAndWorkStation(long id)
        {
            var empWS = _employeeWorkStationUnitData.Detail.AllActive()
                        .Single(x => x.Id == id);
            return JsonContent(empWS);
        }

        [HttpPost]
        public ContentResult GetEmployeeTypes()
        {
            return JsonContent(_employeeTypeData.AllActive());
        }

        [HttpPost]
        public ContentResult GetSalaryStructure(long id)
        {
            var salaries = _salaryStructureData.Detail.Get(id);
            return JsonContent(salaries);
        }

        [HttpPost]
        public ContentResult GetSalaryStructures()
        {
            var salaries = _salaryStructureData.AllActive().ToList();
            return JsonContent(salaries);
        }

        [HttpGet]
        public ContentResult GetEmployeeSalaryStructure(long id)      //id: employeeWorkstationUnitId
        {
            var empSalaryStructure =
                _employeeSalaryStructureData.Detail.AllActiveOrInactive().Single(x => x.EmployeeWorkstationUnitId == id);
            return JsonContent(empSalaryStructure);
        }

        [HttpGet]
        public ContentResult GetEmployeeSalaryStructureAllowance(long id)      //id: employeeSalaryStructureId
        {
            var list =
                _employeeSalaryStructureAllowanceData.Detail.AllActiveOrInactive()
                    .Where(x => x.EmployeeSalaryStructureId == id)
                    .Include(x => x.EmployeeSalaryStructure.SalaryStructure)
                    .ToList();
            return JsonContent(list);
        }

        [HttpGet]
        public ContentResult GetEmployeeSalaryStructureBonus(long id)      //id: employeeWorkstationUnitId
        {
            var list =
                _employeeSalaryStructureBonusData.Detail.AllActiveOrInactive()
                    .Where(x => x.EmployeeSalaryStructureId == id)
                    .Include(x => x.EmployeeSalaryStructure.SalaryStructure)
                    .ToList();
            return JsonContent(list);
        }


        [HttpPost]
        public JsonResult Create(EmployeePromotion entity, long salaryStructureId, long employeeSalaryStructureId, long employeeSalaryStructureBasic)
        {
            var previousEP = _data.Detail.AllActive()
                            .Include(x=>x.EmployeeWorkStationUnit.Employee)
                            .Single(x => x.EmployeeWorkStationUnitId == entity.EmployeeWorkStationUnitId);

            var previousEWS = _employeeWorkStationUnitData.Detail.AllActive()
                                .Single(x => x.Id == entity.EmployeeWorkStationUnitId && x.Status == EntityStatusEnum.Active);

            if (entity.EmployeeTypeId != previousEWS.EmployeeTypeId ||entity.WorkStationUnitId != previousEWS.WorkStationUnitId)
            {
                SetAffectedDetail(previousEWS);
                previousEWS.Status = EntityStatusEnum.Removed;
                _employeeWorkStationUnitData.Replace(previousEWS);

                EmployeeWorkStationUnit employeeWorkStationUnit = new EmployeeWorkStationUnit();
                employeeWorkStationUnit.EmployeeId = previousEP.EmployeeWorkStationUnit.EmployeeId;
                employeeWorkStationUnit.EmployeeTypeId = (long)entity.EmployeeTypeId;
                employeeWorkStationUnit.WorkStationUnitId = (long)entity.WorkStationUnitId;
                employeeWorkStationUnit.Status = EntityStatusEnum.Inactive;
                employeeWorkStationUnit.AttachmentDate = DateTime.Now.Date;
                SetAffectedDetail(employeeWorkStationUnit);
                _employeeWorkStationUnitData.Add(employeeWorkStationUnit);
                _employeeWorkStationUnitData.SaveChanges();



                SetAffectedDetail(previousEP);
                previousEP.Status = EntityStatusEnum.Removed;
                _data.Replace(previousEP);

                EmployeePromotion employeePromotion = new EmployeePromotion();
                employeePromotion.EmployeeWorkStationUnitId = employeeWorkStationUnit.Id;
                employeePromotion.EmployeeTypeId = entity.EmployeeTypeId;
                employeePromotion.WorkStationUnitId = entity.WorkStationUnitId;
                employeePromotion.IncrementSalaryStructureId = entity.IncrementSalaryStructureId;
                employeePromotion.IsApproved = true;
                employeePromotion.Remarks = entity.Remarks;
                employeePromotion.Status = EntityStatusEnum.Inactive;
                SetAffectedDetail(employeePromotion);
                _data.Add(employeePromotion);
                _data.SaveChanges();
            }

            return Json("Promotion granted!!!", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ContentResult Get(long id)
        {
            var promotion = _data.Detail.Get(id);
            return JsonContent(promotion);
        }

        [HttpPost]
        public JsonResult UpdateSalaryStructureAllowance(EmployeeSalaryStructureAllowance model)
        {
            EmployeeSalaryStructureAllowance employeeSalaryStructureAllowance = _employeeSalaryStructureAllowanceData.Get(model.Id);
            SetAffectedDetail(employeeSalaryStructureAllowance);
            employeeSalaryStructureAllowance.AllowanceId = model.AllowanceId;
            employeeSalaryStructureAllowance.Status = EntityStatusEnum.Active;
            _employeeSalaryStructureAllowanceData.Replace(employeeSalaryStructureAllowance);
            _employeeSalaryStructureAllowanceData.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateSalaryStructureBonus(EmployeeSalaryStructureBonus model)
        {
            EmployeeSalaryStructureBonus employeeSalaryStructureBonus = _employeeSalaryStructureBonusData.Get(model.Id);
            SetAffectedDetail(employeeSalaryStructureBonus);
            employeeSalaryStructureBonus.BonusId = model.BonusId;
            employeeSalaryStructureBonus.Status = EntityStatusEnum.Active;
            _employeeSalaryStructureBonusData.Replace(employeeSalaryStructureBonus);
            _employeeSalaryStructureBonusData.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateSalaryStructure(EmployeeSalaryStructure model, long employeeWorkStationUnitId)
        {
            /* update EmployeeSalaryStructure */
            var previousESS = _employeeSalaryStructureData.AllActiveOrInactive().Single(x => x.Id == model.Id);
            SetAffectedDetail(previousESS);
            previousESS.Status = EntityStatusEnum.Removed;
            _employeeSalaryStructureData.Replace(previousESS);

            EmployeeSalaryStructure employeeSalaryStructure = new EmployeeSalaryStructure();
            employeeSalaryStructure.SalaryStructureId = model.SalaryStructureId;
            employeeSalaryStructure.EmployeeWorkstationUnitId = employeeWorkStationUnitId;
            employeeSalaryStructure.AttachmentDate = DateTime.Now.Date;
            employeeSalaryStructure.Basic = model.Basic;
            employeeSalaryStructure.Status = EntityStatusEnum.Inactive;
            SetAffectedDetail(employeeSalaryStructure);
            _employeeSalaryStructureData.Add(employeeSalaryStructure);
            _employeeSalaryStructureData.SaveChanges();


            /* update all EmployeeSalaryStructureAllowance */
            List<EmployeeSalaryStructureAllowance> allESSAllowance = _employeeSalaryStructureAllowanceData.All().Where(x=>x.EmployeeSalaryStructureId == previousESS.Id).ToList();
            foreach (var ESSAllowance in allESSAllowance)
            {
                SetAffectedDetail(ESSAllowance);
                ESSAllowance.EmployeeSalaryStructureId = employeeSalaryStructure.Id;
                _employeeSalaryStructureAllowanceData.Replace(ESSAllowance);
            }
            _employeeSalaryStructureAllowanceData.SaveChanges();


            /* update all EmployeeSalaryStructureBonus */
            List<EmployeeSalaryStructureBonus> allESSBonus = _employeeSalaryStructureBonusData.All().Where(x => x.EmployeeSalaryStructureId == previousESS.Id).ToList();
            foreach (var ESSBonus in allESSBonus)
            {
                SetAffectedDetail(ESSBonus);
                ESSBonus.EmployeeSalaryStructureId = employeeSalaryStructure.Id;
                _employeeSalaryStructureBonusData.Replace(ESSBonus);
            }
            _employeeSalaryStructureBonusData.SaveChanges();


            /* update IncrementSalaryStructure */
            var previousISS = _incrementSalaryStructureData.Detail.All().Single(x => x.EmployeeSalaryStructureId == model.Id);
            SetAffectedDetail(previousISS);
            previousISS.Status = EntityStatusEnum.Removed;           
            _incrementSalaryStructureData.Replace(previousISS);

            IncrementSalaryStructure incrementSalaryStructure = new IncrementSalaryStructure();
            incrementSalaryStructure.Basic = model.Basic;
            incrementSalaryStructure.IsApproved = true;
            incrementSalaryStructure.Status = model.Status;
            incrementSalaryStructure.EmployeeSalaryStructureId = employeeSalaryStructure.Id;
            SetAffectedDetail(incrementSalaryStructure);
            _incrementSalaryStructureData.Add(incrementSalaryStructure);
            _incrementSalaryStructureData.SaveChanges();

            return Json(incrementSalaryStructure.Id, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(EmployeePromotion entity)
        {
            SetAffectedDetail(entity);
            if (entity.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            EmployeePromotion aPromotion = _data.Get(entity.Id);
            entity.MapTo(aPromotion);
            _data.Replace(aPromotion);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult DeleteEmployeeSalaryStructureAllowance(long id)
        {
            EmployeeSalaryStructureAllowance employeeSalaryStructureAllowance = _employeeSalaryStructureAllowanceData.Get(id);
            SetAffectedDetail(employeeSalaryStructureAllowance);
            _employeeSalaryStructureAllowanceData.Remove(employeeSalaryStructureAllowance);
            _employeeSalaryStructureAllowanceData.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteEmployeeSalaryStructureBonus(long id)
        {
            EmployeeSalaryStructureBonus employeeSalaryStructureBonus = _employeeSalaryStructureBonusData.Get(id);
            SetAffectedDetail(employeeSalaryStructureBonus);
            _employeeSalaryStructureBonusData.Remove(employeeSalaryStructureBonus);
            _employeeSalaryStructureBonusData.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Detail

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_data.Detail.AllLog(id));
        }

        #endregion
   
    }
}