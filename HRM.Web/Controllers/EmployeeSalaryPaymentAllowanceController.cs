﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeSalaryPaymentAllowanceController : HrmController
    {
        private readonly EmployeeSalaryPaymentAllowanceData _data;

        public EmployeeSalaryPaymentAllowanceController()
        {
        }

        public EmployeeSalaryPaymentAllowanceController(EmployeeSalaryPaymentAllowanceData data)
        {
            _data = data;
        }

        #region Create

        public ContentResult GetEmployeeSalaryStructureAllowances (long id)     /* Employee Salary Structure Id*/
        {
            //return JsonContent(new EmployeeSalaryStructureAllowanceData().Detail.AllActive().ToList());
            var result = new EmployeeSalaryStructureAllowanceData().Detail.AllActive().Where(x => x.EmployeeSalaryStructureId == id);
            return JsonContent(result);
        }

        #endregion

        [HttpPost]
        public ActionResult Delete ( EmployeeSalaryPaymentAllowance model )
        {
            EmployeeSalaryPaymentAllowance employeeSalaryPaymentAllowance = _data.Get(model.Id);
            employeeSalaryPaymentAllowance.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeSalaryPaymentAllowance.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(model); 
            _data.Remove(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
