﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeSalaryPaymentBonusController : HrmController
    {
       private readonly EmployeeSalaryPaymentBonusData _data;

       public EmployeeSalaryPaymentBonusController ()
        {
        }

       public EmployeeSalaryPaymentBonusController ( EmployeeSalaryPaymentBonusData data )
        {
            _data = data;
        }

        #region Create  
       public ContentResult GetEmployeeSalaryStructureBonuses (long id)
       {
           //return JsonContent(new EmployeeSalaryStructureBonusData().Detail.AllActive().ToList());
           var result = new EmployeeSalaryStructureBonusData().Detail.AllActive().Where(x => x.EmployeeSalaryStructureId == id);
           return JsonContent(result);

       }  
       #endregion
    }
}
