﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Web.Controllers.Shared;
using Microsoft.Ajax.Utilities;

namespace HRM.Web.Controllers
{
    public class EmployeeSalaryPaymentController : HrmController
    {
        private readonly EmployeeSalaryPaymentData _data = new EmployeeSalaryPaymentData(new HrmContext());
       // private readonly EmployeeSalaryStructureData _employeeSalaryStructureData = new EmployeeSalaryStructureData();

        public EmployeeSalaryPaymentController()
        {
        }

        public EmployeeSalaryPaymentController(EmployeeSalaryPaymentData data)
        {
            _data = data;
        }

        #region List

        public ActionResult Index ()
        {
            return View("List");
        }

        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find ( int pageNo, int pageSize, EmployeeSalaryPayment filter )
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        }

        #endregion

        #region Create

        //public ContentResult GetEmployeeSalaryStructureId ( long id )     //id = employeeWorkStationUnitId
        //{
        //    var data = JsonContent(new EmployeeSalaryStructureData().Detail.AllActive().Single(x => x.EmployeeWorkstationUnitId == id));
        //    return data;
        //}

        public ContentResult GetEmployeeSalaryStructures ()
        {
            return JsonContent(new EmployeeSalaryStructureData().Detail.AllActive().ToList());
        }
        
        public ContentResult GetEmployeePayrollInfos ()
        {
            var data = JsonContent(new EmployeePayrollInfoData().Detail.AllActive().ToList());
            return data;
        }

        //[HttpPost]
        //public ContentResult LoadPayrollInfo ( long id )    /* id= EmployeeSalaryStructureId*/
        //{
        //    var data = new EmployeeSalaryStructureData().Detail.AllActive().Single(x => x.Id == id);
        //    var empData = new EmployeePayrollInfoData().Detail.AllActive().Single(x => x.EmployeeId == data.EmployeeWorkStationUnit.EmployeeId);
        //    return JsonContent(empData);
        //}

        [HttpPost]
        public ContentResult LoadEmployeeSalaryStructure ( long id )    /* id= EmployeeWorkStationUnitId*/
        {
            var salaryData = new EmployeeSalaryStructureData().Detail.AllActive().Single(x => x.EmployeeWorkStationUnit.Id == id);
            //var salaryData = new EmployeeSalaryStructureData().Detail.AllActive().Single(x => x.Id==data.EmployeeWorkStationUnit.EmployeeId);
            return JsonContent(salaryData);
        }

        [HttpPost]
        public ContentResult LoadPayrollInfo ( long id )    /* id= EmployeeWorkStationUnitId*/
        {
            var unitData = new EmployeeWorkStationUnitData().Detail.AllActive().Single(x => x.Id == id);
            var payrollData = new EmployeePayrollInfoData().Detail.AllActive().Single(x => x.EmployeeId == unitData.EmployeeId);
            return JsonContent(payrollData);
        }

        public ActionResult CreateSalaryPayment ( long id )   /* id= employeeWorkStationUnitId */
        {
            return View(id);
        }

        //[HttpPost]
        //public ActionResult Create(EmployeeSalaryPayment model)
        //{
        //    SetAffectedDetail(model);
        //    _data.Add(model);
        //    _data.SaveChanges();
        //    return Json(true, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult Create ( EmployeeSalaryPayment model, List<EmployeeSalaryPaymentAllowance> allowanceList, List<EmployeeSalaryPaymentBonus> bonusList )
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();  

            var allowanceData = new EmployeeSalaryPaymentAllowanceData();
            foreach(var aAllowance in allowanceList)
            {
                SetAffectedDetail(aAllowance);
                aAllowance.EmployeeSalaryPaymentId = model.Id;   
                aAllowance.Status = EntityStatusEnum.Active;
                allowanceData.Add(aAllowance); 
            }
            allowanceData.SaveChanges();

            var bonusData = new EmployeeSalaryPaymentBonusData();
            foreach(var aBonus in bonusList)
            {
                SetAffectedDetail(aBonus);
                aBonus.EmployeeSalaryPaymentId = model.Id;
                aBonus.Status = EntityStatusEnum.Active;
                bonusData.Add(aBonus);
            }
            bonusData.SaveChanges();  

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
