﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeSalaryStructureAllowanceManageController : HrmController
    {
        private readonly EmployeeSalaryStructureAllowanceManageLogic _logic;

        public EmployeeSalaryStructureAllowanceManageController (): this(new EmployeeSalaryStructureAllowanceManageLogic())
        {
        }

        protected EmployeeSalaryStructureAllowanceManageController ( EmployeeSalaryStructureAllowanceManageLogic logic )
        {
            _logic = logic;
        }

        public JsonResult GetAllowanceTitles ()
        {
            return Json(_logic.GetAllowanceTitles(), JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult Create ( EmployeeSalaryStructureAllowance model )
        {
            SetAffectedDetail(model); 
           _logic.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( EmployeeSalaryStructureAllowance model)
        {
            SetAffectedDetail(model); 
            _logic.Delete(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
