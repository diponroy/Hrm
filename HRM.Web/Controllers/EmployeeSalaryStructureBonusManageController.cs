﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeSalaryStructureBonusManageController : HrmController
    {
        private readonly EmployeeSalaryStructureBonusManageLogic _logic;

        public EmployeeSalaryStructureBonusManageController (): this(new EmployeeSalaryStructureBonusManageLogic())
        {
        }

        protected EmployeeSalaryStructureBonusManageController ( EmployeeSalaryStructureBonusManageLogic logic )
        {
            _logic = logic;
        }

        public JsonResult GetBonusTitles ()
        {
            return Json(_logic.GetBonusTitles(), JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult Create ( EmployeeSalaryStructureBonus model )
        {
            SetAffectedDetail(model); 
           _logic.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( EmployeeSalaryStructureBonus model )
        {
            SetAffectedDetail(model); 
            _logic.Delete(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}
