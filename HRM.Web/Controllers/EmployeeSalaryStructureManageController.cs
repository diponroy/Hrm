﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeSalaryStructureManageController : HrmController
    {
        private readonly EmployeeSalaryStructureManageLogic _logic;

        public EmployeeSalaryStructureManageController () : this(new EmployeeSalaryStructureManageLogic())
        {
        }

        protected EmployeeSalaryStructureManageController ( EmployeeSalaryStructureManageLogic logic )
        {
            _logic = logic;
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_logic.Get(id));
        }
        
        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }
        [HttpPost]
        public ContentResult Find ( int pageNo, int pageSize, EmployeeSalaryStructure filter )
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }

        public ActionResult Create()
        {
            return View();
        }

        public ContentResult GetSalaryStructures ()
        {
            return JsonContent(_logic.GetSalaryStructures());
        }
        public ContentResult GetEmployeeWorkStationUnits ()
        {
            return JsonContent(_logic.GetEmployeeWorkStationUnits());
        } 
        
        [HttpPost]
        public JsonResult Create ( EmployeeSalaryStructure model, List<EmployeeSalaryStructureAllowance> allowanceList, List<EmployeeSalaryStructureBonus> bonusList )
        {
            SetAffectedDetail(model);

            foreach(var aAllowance in allowanceList)
            {
                SetAffectedDetail(aAllowance);
                aAllowance.Status = EntityStatusEnum.Active;   
            }

            foreach(var aBonus in bonusList)
            {
                SetAffectedDetail(aBonus);
                aBonus.Status = EntityStatusEnum.Active; 
            }
            _logic.Create(model, allowanceList, bonusList);  
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult Update ( EmployeeSalaryStructure model )
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( EmployeeSalaryStructure employeeSalaryStructure )
        {
            SetAffectedDetail(employeeSalaryStructure);
            _logic.Remove(employeeSalaryStructure);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_logic.GetLogs(id));
        }
    }
}
