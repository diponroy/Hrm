﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeTrainingAttachmentManageController : HrmController
    {
        private readonly EmployeeTrainingAttachmentManageLogic _logic;

        public EmployeeTrainingAttachmentManageController () : this(new EmployeeTrainingAttachmentManageLogic())
        {
        }

        protected EmployeeTrainingAttachmentManageController ( EmployeeTrainingAttachmentManageLogic logic )
        {
            _logic = logic;
        }
        public ActionResult Index()
        {
            return View("List");
        }
        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, EmployeeTrainingAttachment filter)
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }

        public JsonResult GetTrainingTitles ()
        {
            List<EmployeeTraining> list = _logic.GetTrainingTitles().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create ()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Upload ()
        {
            string fileName = null;
            //int fileSize;

            for(int i = 0;i < Request.Files.Count;i++)
            {
                HttpPostedFileBase file = Request.Files[i];   //Uploaded file
                //fileSize = file.ContentLength;
                fileName = file.FileName;
                Stream fileContent = file.InputStream;

                //string path = Path.Combine(@"E:\HRM\HRM\HRM.Web\Uploaded Files\All Images", fileName);
                string path = Server.MapPath("~/Uploaded Files/All Files/" + fileName);

                if(CheckFileType(fileName))
                {
                    file.SaveAs(path);
                    return Json(path);
                }

            }
            return Json("Error occured while uploading!!");
        }

        protected bool CheckFileType ( string fileName )
        {
            string ext = Path.GetExtension(fileName);

            switch(ext.ToLower())
            {
                case ( ".gif" ):
                    { return true; }
                case ( ".png" ):
                    { return true; }
                case ( ".jpg" ):
                    { return true; }
                case ( ".jpeg" ):
                    { return true; }
                case ( ".doc" ):
                    {
                        return true;
                    }
                case ( ".docx" ):
                    {
                        return true;
                    }
                case ( ".pdf" ):
                    {
                        return true;
                    }
                default:
                    { return false; }
            }
        }

        [HttpPost]
        public JsonResult Create ( EmployeeTrainingAttachment employeeTrainingAttachment )
        {
            SetAffectedDetail(employeeTrainingAttachment);
            _logic.Create(employeeTrainingAttachment);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_logic.Get(id));
        }

        [HttpPost]
        public JsonResult Update ( EmployeeTrainingAttachment model )
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( EmployeeTrainingAttachment employeeTrainingAttachment )
        {
            SetAffectedDetail(employeeTrainingAttachment);
            _logic.Remove(employeeTrainingAttachment);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_logic.GetLogs(id));
        }
    }
}
