﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;
using Microsoft.Ajax.Utilities;

namespace HRM.Web.Controllers
{
    public class EmployeeTrainingBudgetManageController : HrmController
    {
        private readonly EmployeeTrainingBudgetManageLogic _logic;

        public EmployeeTrainingBudgetManageController() : this(new EmployeeTrainingBudgetManageLogic())
        {   
        }

        public EmployeeTrainingBudgetManageController(EmployeeTrainingBudgetManageLogic logic)
        {
            _logic = logic;
        }

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        public ContentResult FInd ( int pageNo, int pageSize, EmployeeTrainingBudget filter )
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }

        public ActionResult Create ()
        {
            return View();
        }

        public JsonResult GetTrainingTitles ()
        {
            return Json(_logic.GetTrainingTitles(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create ( EmployeeTrainingBudget employeeTrainingBudget )
        {
            SetAffectedDetail(employeeTrainingBudget);
            _logic.Create(employeeTrainingBudget);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_logic.Get(id));
        }

        [HttpPost]
        public JsonResult Update ( EmployeeTrainingBudget model )
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( EmployeeTrainingBudget employeeTrainingBudget )
        {
            SetAffectedDetail(employeeTrainingBudget);
            _logic.Remove(employeeTrainingBudget);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_logic.GetLogs(id));
        } 
    }
}
