﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeTrainingManageController : HrmController
    {
        private readonly EmployeeTrainingManageLogic _logic;

        public EmployeeTrainingManageController() : this(new EmployeeTrainingManageLogic())
        {
        }

        protected EmployeeTrainingManageController(EmployeeTrainingManageLogic logic)
        {
            _logic = logic;
        }

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, EmployeeTraining filter)
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Create(EmployeeTraining employeeTraining)
        {
            SetAffectedDetail(employeeTraining);
            _logic.Create(employeeTraining);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(long id)
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_logic.Get(id));
        }

        [HttpPost]
        public JsonResult Update(EmployeeTraining model)
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(EmployeeTraining employeeTraining)
        {
            SetAffectedDetail(employeeTraining);
            _logic.Remove(employeeTraining);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }
    }
}
