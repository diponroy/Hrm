﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeTransferController : HrmController
    {
         private readonly EmployeeTransferData _data = new EmployeeTransferData(new HrmContext());

        public EmployeeTransferController()
        {
        }
        public EmployeeTransferController ( EmployeeTransferData data )
        {
            _data = data;
        }

        #region List

        public ActionResult Index ()
        {
            return View("List");
        }

        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find ( int pageNo, int pageSize, EmployeeTransfer filter )
        {
            var data = _data.Detail.AllActiveOrInactive().ToList();
            return JsonContent(data);
        }

        #endregion
        #region Create

        public ActionResult Create ()
        {
            return View();
        }

        public ContentResult GetEmployees ()
        {
            return JsonContent(new EmployeeData().Detail.AllActive().ToList());
        }
        public ContentResult GetEmployeeTypes ()
        {
            return JsonContent(new EmployeeTypeData().Detail.AllActive().ToList());
        }
        public ContentResult GetWorkStationUnits ()
        {
            return JsonContent(new WorkStationUnitData().Detail.AllActive().ToList());
        }

        public ContentResult GetEmployeeSalaryIncrements ()
        {
            return JsonContent(new IncrementSalaryStructureData().Detail.AllActive().Include(x=>x.EmployeeSalaryStructure.SalaryStructure).ToList());
        }

        [HttpPost]
        public JsonResult Create ( EmployeeTransfer model )
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            var val = _data.Detail.Get(id);
            return JsonContent(val);
        }

        [HttpPost]
        public JsonResult Update ( EmployeeTransfer model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            EmployeeTransfer employeeTransfer = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(employeeTransfer);
            _data.Replace(employeeTransfer);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete ( EmployeeTransfer model )
        {
            EmployeeTransfer employeeTransfer = _data.Get(model.Id);
            employeeTransfer.AffectedByEmployeeId = model.AffectedByEmployeeId;
            employeeTransfer.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(employeeTransfer);
            _data.Remove(employeeTransfer);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion

    }
}
