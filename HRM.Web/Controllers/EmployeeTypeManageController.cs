﻿using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeTypeManageController : HrmController
    {
        private readonly EmployeeTypeManageLogic _logic;

        public EmployeeTypeManageController() : this(new EmployeeTypeManageLogic())
        {
        }

        protected EmployeeTypeManageController(EmployeeTypeManageLogic logic)
        {
            _logic = logic;
        }

        #region Shared

        public ContentResult Get ( long id )
        {
            return JsonContent(_logic.Get(id));
        }

        #endregion

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, EmployeeType filter)
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }
        #endregion

        #region Delete
        public ActionResult CheckToDelete(long id)
        {
            return Json(_logic.HasActiveChilds(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(EmployeeType model)
        {
            SetAffectedDetail(model);
            _logic.Delete(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ContentResult GetActiveParents ( )
        {
            return JsonContent(_logic.GetActiveParents());
        }

        [HttpPost]
        public JsonResult IsTitleUsed(string title)
        {
            return Json(_logic.HasTitle(title), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(EmployeeType model)
        {
            SetAffectedDetail(model);
            _logic.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        [HttpGet]
        public ContentResult GetAssignableParents ( long id )
        {
            return JsonContent(_logic.GetAssignableParents(id));
        }

        [HttpPost]
        public JsonResult TitleUsedExceptItself(string title, long employeeTypeId)
        {
            return Json(_logic.HasTitleUsedExcept(title, employeeTypeId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult Update ( EmployeeType model )
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }
        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }

        #endregion
    }
}
