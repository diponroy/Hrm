﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class EmployeeWorkStationUnitManageController : HrmController
    {
        private readonly EmployeeWorkStationUnitManageLogic logic;
        
        public EmployeeWorkStationUnitManageController(): this(new EmployeeWorkStationUnitManageLogic())
        {
            
        }

        protected EmployeeWorkStationUnitManageController(EmployeeWorkStationUnitManageLogic logic)
        {
            this.logic = logic;
        }

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetEmployees()
        {
            return Json(logic.GetEmployees(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult GetWorkStationUnits()
        {
            return JsonContent(logic.GetWorkStationUnits());
        }

        [HttpPost]
        public ContentResult GetEmployeeTypes()
        {
            return JsonContent(logic.GetEmployeeTypes());
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, EmployeeWorkStationUnit filter)
        {
            return JsonContent(data: logic.Find(pageNo, pageSize, filter));
        }
        #endregion

        #region Create

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        public JsonResult IscombinationUsed(EmployeeWorkStationUnit model)
        {
            return Json(logic.IscombinationUsed(model.EmployeeId, model.WorkStationUnitId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(EmployeeWorkStationUnit model)
        {
            SetAffectedDetail(model);
            logic.Add(model);
            return new EmptyResult();
        } 
        #endregion

        #region Update

        public ContentResult Get(long id)
        {
            return JsonContent(logic.GetDetail(id));
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult Update(EmployeeWorkStationUnit model)
        {
            SetAffectedDetail(model);
            logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(EmployeeWorkStationUnit model)
        {
            SetAffectedDetail(model);
            logic.Remove(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Detail

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        public ContentResult GetLogs(long id)
        {
            return JsonContent(logic.GetLogs(id));
        }
        #endregion
    }
}
