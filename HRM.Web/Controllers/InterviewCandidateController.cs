﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class InterviewCandidateController : HrmController
    {
        private readonly InterviewCandidateData _data=new InterviewCandidateData(new HrmContext());

        public InterviewCandidateController()
        {
            
        }
        public InterviewCandidateController ( InterviewCandidateData data )
        {
            _data = data;
        }
        #region List

        public ActionResult Index ()
        {
            return View("List");
        }

        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find ( int pageNo, int pageSize, InterviewCandidate filter )
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        }

        #endregion

        #region Create

        public ActionResult Create ()
        {
            return View();
        }

        public ContentResult GetInterviewTitles ()
        {
            return JsonContent(new InterviewData(new HrmContext()).Detail.AllActive().ToList());
        }

        public ContentResult GetCandidateInfo ()
        {
            return JsonContent(new CandidateData(new HrmContext()).Detail.AllActive().ToList());
        }

        [HttpPost]
        public ActionResult Create ( InterviewCandidate model )
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_data.Detail.Get(id));
        }

        [HttpPost]
        public JsonResult Update ( InterviewCandidate model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            InterviewCandidate interviewCandidate = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(interviewCandidate);
            _data.Replace(interviewCandidate);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete ( InterviewCandidate model )
        {
            InterviewCandidate interviewCandidate = _data.Get(model.Id);
            interviewCandidate.AffectedByEmployeeId = model.AffectedByEmployeeId;
            interviewCandidate.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(interviewCandidate);
            _data.Remove(interviewCandidate);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion
    }
}
