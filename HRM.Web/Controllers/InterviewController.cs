﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class InterviewController : HrmController
    {
        private readonly InterviewData _data = new InterviewData(new HrmContext());

        public InterviewController()
        {
        }

        public InterviewController(InterviewData data)
        {
            _data = data;
        }

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, Interview filter)
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        }

        #endregion

        #region Create

        public ActionResult CreateForManpowerVacancyNotice(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult HasTrackNoUsed(string trackNo)
        {
            return Json(_data.AnyActiveOrInactiveTrackNo(trackNo), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(Interview model)
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult HasTrackNoUsedExcept ( string trackNo, long id )
        {
            return Json(_data.AnyActiveOrInactiveTrackNoExcept(trackNo, id), JsonRequestBehavior.AllowGet);
        }

        public ContentResult GetVacancyNotices ()
        {
            return JsonContent(new ManpowerVacancyNoticeData().Detail.AllActive().ToList());
        }

        public ContentResult Get(long id)
        {
            return JsonContent(_data.Detail.Get(id));
        }

        [HttpPost]
        public JsonResult Update(Interview model)
        {
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            Interview interview = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(interview);
            _data.Replace(interview);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(Interview model)
        {
            Interview interview = _data.Get(model.Id);
            interview.AffectedByEmployeeId = model.AffectedByEmployeeId;
            interview.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(interview);
            _data.Remove(interview);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ActionResult Detail(long id)
        {
            return View(id);
        }

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion
    }
}
