﻿using System;
using System.Linq;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class InterviewCoordinatorController : HrmController
    {
        private readonly InterviewCoordinatorData _data = new InterviewCoordinatorData(new HrmContext());

        public InterviewCoordinatorController()
        {
        }

        public InterviewCoordinatorController(InterviewCoordinatorData data)
        {
            _data = data;
        }

        #region List

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, InterviewCoordinator filter)
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        }

        #endregion

        #region Create

        public ActionResult Create()
        {
            return View();
        }

        public ContentResult GetInterviewTitles()
        {
            return JsonContent(new InterviewData(new HrmContext()).Detail.AllActive().ToList());
        }

        public ContentResult GetEmployees ()
        {
            return JsonContent(new EmployeeData(new HrmContext()).Detail.AllActive().ToList());
        }

        [HttpPost]
        public ActionResult Create(InterviewCoordinator model)
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ActionResult Update(long id)
        {
            return View(id);
        }

        public ContentResult Get(long id)
        {
            return JsonContent(_data.Detail.Get(id));
        }

        [HttpPost]
        public JsonResult Update(InterviewCoordinator model)
        {
            if (model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            InterviewCoordinator interviewCoordinator = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(interviewCoordinator);
            _data.Replace(interviewCoordinator);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(InterviewCoordinator model)
        {
            InterviewCoordinator interviewCoordinator = _data.Get(model.Id);
            interviewCoordinator.AffectedByEmployeeId = model.AffectedByEmployeeId;
            interviewCoordinator.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(interviewCoordinator);
            _data.Remove(interviewCoordinator);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ActionResult Detail(long id)
        {
            return View(id);
        }

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion
    }
}
