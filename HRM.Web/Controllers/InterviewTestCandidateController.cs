﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class InterviewTestCandidateController : HrmController
    {
        private readonly InterviewTestCandidateData _data = new InterviewTestCandidateData(new HrmContext());

        public InterviewTestCandidateController()
        {
        }
        public InterviewTestCandidateController ( InterviewTestCandidateData data )
        {
            _data = data;
        }

        #region List

        public ActionResult Index ()
        {
            return View("List");
        }

        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find ( int pageNo, int pageSize, InterviewTestCandidate filter )
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        }

        #endregion
        #region Create

        public ActionResult Create ()
        {
            return View();
        }

        public ContentResult GetInterviewTestTitles ()
        {
            return JsonContent(new InterviewTestData(new HrmContext()).Detail.AllActive().ToList());
        }
        public ContentResult GetCandidateNames ()
        {
            return JsonContent(new CandidateData(new HrmContext()).Detail.AllActive().ToList());
        }

        [HttpPost]
        public ActionResult Create ( InterviewTestCandidate model )
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_data.Detail.Get(id));
        }

        [HttpPost]
        public JsonResult Update ( InterviewTestCandidate model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            InterviewTestCandidate interviewTest = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(interviewTest);
            _data.Replace(interviewTest);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete ( InterviewTestCandidate model )
        {
            InterviewTestCandidate interviewTestCandidate = _data.Get(model.Id);
            interviewTestCandidate.AffectedByEmployeeId = model.AffectedByEmployeeId;
            interviewTestCandidate.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(interviewTestCandidate);
            _data.Remove(interviewTestCandidate);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion

    }
}
