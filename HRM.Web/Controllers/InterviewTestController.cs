﻿using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRM.Web.Controllers
{
    public class InterviewTestController : HrmController
    {
        private readonly InterviewTestData _data = new InterviewTestData(new HrmContext());

        public InterviewTestController()
        {
        } 
        public InterviewTestController ( InterviewTestData data )
        {
            _data = data;
        }

        #region List

        public ActionResult Index ()
        {
            return View("List");
        }

        public ActionResult List ()
        {
            return View();
        }

        [HttpPost]
        public ContentResult Find ( int pageNo, int pageSize, InterviewTest filter )
        {
            return JsonContent(_data.Detail.AllActiveOrInactive().ToList());
        }

        #endregion
        #region Create

        public ActionResult Create ()
        {
            return View();
        }

        public ContentResult GetInterviewTitles ()
        {
            return JsonContent(new InterviewData(new HrmContext()).Detail.AllActive().ToList());
        }

        [HttpPost]
        public JsonResult Create ( InterviewTest model )
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ActionResult Update ( long id )
        {
            return View(id);
        }

        public ContentResult Get ( long id )
        {
            return JsonContent(_data.Detail.Get(id));
        }

        [HttpPost]
        public JsonResult Update ( InterviewTest model )
        {
            if(model.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            InterviewTest interviewTest = _data.Get(model.Id);
            SetAffectedDetail(model);
            model.MapTo(interviewTest);
            _data.Replace(interviewTest);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete ( InterviewTest model )
        {
            InterviewTest interviewTest = _data.Get(model.Id);
            interviewTest.AffectedByEmployeeId = model.AffectedByEmployeeId;
            interviewTest.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(interviewTest);
            _data.Remove(interviewTest);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ActionResult Detail ( long id )
        {
            return View(id);
        }

        public ContentResult GetLogs ( long id )
        {
            return JsonContent(_data.Detail.AllLog(id).ToList());
        }

        #endregion
    }
}
