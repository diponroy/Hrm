﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class ManpowerVacancyNoticeManageController : HrmController
    {
        private readonly ManpowerVacancyNoticeData _data = new ManpowerVacancyNoticeData(new HrmContext());

        public ManpowerVacancyNoticeManageController()
        {
        }

        public ManpowerVacancyNoticeManageController ( ManpowerVacancyNoticeData data )
        {
            _data = data;
        }

        public JsonResult GetManpowerRequisitionTitles ()
        {
            return Json(new ManpowerRequisitionData().AllActive().ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create ( ManpowerVacancyNotice model )
        {
            SetAffectedDetail(model);
            _data.Add(model);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( ManpowerVacancyNotice model )
        {
            ManpowerVacancyNotice manpowerVacancyNotice = _data.Get(model.Id);
            manpowerVacancyNotice.AffectedByEmployeeId = model.AffectedByEmployeeId;
            manpowerVacancyNotice.AffectedDateTime = model.AffectedDateTime;
            SetAffectedDetail(manpowerVacancyNotice);
            _data.Remove(manpowerVacancyNotice);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
