﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class SalaryStructureAllowanceManageController : HrmController
    {
        private readonly SalaryStructureAllowanceManageLogic _logic;

        public SalaryStructureAllowanceManageController() : this(new SalaryStructureAllowanceManageLogic())
        {
        }

        protected SalaryStructureAllowanceManageController(SalaryStructureAllowanceManageLogic logic)
        {
            _logic = logic;
        }


        [HttpPost]
        public ContentResult GetSalaryAllowanceList ( long id )    /*id = Salary Structure id*/
        {
            return JsonContent(_logic.GetSalaryAllowanceList(id));
        }

        public JsonResult GetSalaryStructureTitles ()
        {
            return Json(_logic.GetSalaryStructureTitles(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllowanceTitles ()
        {
            return Json(_logic.GetAllowanceTitles(), JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult Create ( SalaryStructureAllowance model )
        {
            SetAffectedDetail(model);
            model.Status=EntityStatusEnum.Active;
            _logic.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update ( SalaryStructureAllowance model )
        {
            SetAffectedDetail(model);
            model.Status=EntityStatusEnum.Active;
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( SalaryStructureAllowance model )
        {
            SetAffectedDetail(model);
            _logic.Delete(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
