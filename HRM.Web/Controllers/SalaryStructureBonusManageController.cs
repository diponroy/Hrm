﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class SalaryStructureBonusManageController : HrmController
    {
        private readonly SalaryStructureBonusManageLogic _logic;

        public SalaryStructureBonusManageController () : this(new SalaryStructureBonusManageLogic())
        {
        }

        protected SalaryStructureBonusManageController ( SalaryStructureBonusManageLogic logic )
        {
            _logic = logic;
        }

        /*Bonus List View*/

        [HttpPost]
        public ContentResult GetSalaryBonusList ( long id )    /*id = Salary Structure id*/
        {
            return JsonContent(_logic.GetSalaryBonusList(id));
        }

        /*End List View*/ 

        public JsonResult GetBonusTitles ()
        {
            return Json(_logic.GetBonusTitles(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create ( SalaryStructureBonus model )
        {
            SetAffectedDetail(model);
            model.Status = EntityStatusEnum.Active;
            _logic.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update ( SalaryStructureBonus model )
        {
            SetAffectedDetail(model);
            model.Status = EntityStatusEnum.Active;
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( SalaryStructureBonus model )
        {
            SetAffectedDetail(model);
            _logic.Delete(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
