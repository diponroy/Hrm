﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers
{
    public class SalaryStructureManageController : HrmController
    {
        private readonly SalaryStructureManageLogic _logic;

        public SalaryStructureManageController() : this(new SalaryStructureManageLogic())
        {
        }

        protected SalaryStructureManageController(SalaryStructureManageLogic logic)
        {
            _logic = logic;
        }

        public ContentResult Get(long id)
        {
            return JsonContent(_logic.Get(id));
        }

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }
        
        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, SalaryStructure filter)
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult IsTitleUsed(string title)
        {
            return Json(_logic.HasTitle(title), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(SalaryStructure model, List<SalaryStructureAllowance> allowanceList, List<SalaryStructureBonus> bonusList  )
        {
            SetAffectedDetail(model);

            if(allowanceList != null && bonusList != null)
            {
                foreach(var aAllowance in allowanceList)
                {
                    SetAffectedDetail(aAllowance);
                    aAllowance.Status = EntityStatusEnum.Active;
                }
                foreach(var aBonus in bonusList)
                {
                    SetAffectedDetail(aBonus);
                    aBonus.Status = EntityStatusEnum.Active;
                }
                _logic.Create(model, allowanceList, bonusList);  
            }

            else if(allowanceList != null && bonusList == null)
            {
                foreach(var aAllowance in allowanceList)
                {
                    SetAffectedDetail(aAllowance);
                    aAllowance.Status = EntityStatusEnum.Active;
                }
                _logic.Create(model, allowanceList); 
 
            }
            else if (allowanceList == null && bonusList != null)
            {
                foreach(var aBonus in bonusList)
                {
                    SetAffectedDetail(aBonus);
                    aBonus.Status = EntityStatusEnum.Active;
                }
                _logic.Create(model, bonusList);   
            }
            else
            {
                _logic.Create(model);   
            }
              
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult TitleUsedExceptItself(string title, long id)
        {
            return Json(_logic.HasTitleUsedExcept(title, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(SalaryStructure model)
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete ( SalaryStructure model )
        {
            SetAffectedDetail(model);
            _logic.Delete(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(long id)
        {
            return View(id);
        }

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }
    }
}
