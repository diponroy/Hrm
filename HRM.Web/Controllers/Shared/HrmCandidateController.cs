﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using HRM.Model.Table;
using HRM.Model.Table.IEntity.Shared;
using HRM.Web.Models;
using Newtonsoft.Json;

namespace HRM.Web.Controllers.Shared
{
    public abstract class HrmCandidateController : Controller
    {
        private const string CandidateLoginSession = "CandidateLogin";

        private readonly bool _permissionEnabled;
        protected HrmCandidateController()
        {
            _permissionEnabled = false;
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (_permissionEnabled)
            {
                if (!HasSessions())
                {
                    Rederect(requestContext, Url.Action("Index", "CandidateLogin"));
                }
            }
            else
            {
                var controllerName = requestContext.RouteData.Values["controller"].ToString();
                if (!controllerName.ToLower().Equals("CandidateLogin".ToLower()) && !HasSessions())
                {
                    Rederect(requestContext, Url.Action("Index", "CandidateLogin"));
                }
            }
        }

        private void Rederect(RequestContext requestContext, string action)
        {
            requestContext.HttpContext.Response.Clear();
            requestContext.HttpContext.Response.Redirect(action);
            requestContext.HttpContext.Response.End();
        }

        protected bool HasSessions()
        {
            return this.Session[CandidateLoginSession] != null;
        }


        protected void AbandonSession()
        {
            if (HasSessions())
            {
                Session.Abandon();
            }
        }

        #region CandidateLoginSession
        protected void SetCandidateLoginSession(CandidateLogin login)
        {
            CandidateLoginSession sessionModel = new CandidateLoginSession()
            {
                CandidateId = login.CandidateId,
                CandidateName = string.Format("{0} {1}", login.Candidate.FirstName, login.Candidate.LastName).Trim()
            };
            this.Session[CandidateLoginSession] = sessionModel;
        }
        protected CandidateLoginSession GetSessionModel()
        {
            return (CandidateLoginSession)Session[CandidateLoginSession];
        }
        protected IAffectedByTrack SetAffectedDetail(IAffectedByTrack entity)
        {
            var sessionModel = GetSessionModel();
            entity.AffectedDateTime = DateTime.Now;
            return entity;
        }

        #endregion

        #region Json
        public ContentResult JsonContent(object data)
        {
            string list = JsonConvert.SerializeObject(data,
                Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            return Content(list, "application/json");
        }
        #endregion
    }
}
