﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using HRM.Model.Table;
using HRM.Model.Table.IEntity.Shared;
using HRM.Web.Models;
using Newtonsoft.Json;

namespace HRM.Web.Controllers.Shared
{
    public abstract class HrmController : Controller
    {
        private const string EmployeeLoginSession = "EmployeeLogin";
        private readonly bool _permissionEnabled;
        protected HrmController()
        {
            _permissionEnabled = false;
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (_permissionEnabled)
            {
                if (!HasSessions())
                {
                    Rederect(requestContext, Url.Action("Index", "EmployeeLogin"));
                }
            }
            else
            {
                var controllerName = requestContext.RouteData.Values["controller"].ToString();
                if (!controllerName.ToLower().Equals("EmployeeLogin".ToLower()) && !HasSessions())
                {
                    Rederect(requestContext, Url.Action("Index", "EmployeeLogin"));
                }
            }
        }

        private void Rederect(RequestContext requestContext, string action)
        {
            requestContext.HttpContext.Response.Clear();
            requestContext.HttpContext.Response.Redirect(action);
            requestContext.HttpContext.Response.End();
        }

        protected bool HasSessions()
        {
            return this.Session[EmployeeLoginSession] != null;
        }


        protected void AbandonSession()
        {
            if (HasSessions())
            {
                Session.Abandon();
            }
        }

        #region EmployeeLoginSession
        protected void SetEmployeeLoginSession(EmployeeLogin login)
        {
            EmployeeLoginSession sessionModel = new EmployeeLoginSession()
            {
                EmployeeId = login.EmployeeId
            };
            this.Session[EmployeeLoginSession] = sessionModel;
        }
        protected IAffectedByTrack SetAffectedDetail(IAffectedByTrack entity)
        {
            var sessionModel = (EmployeeLoginSession)Session[EmployeeLoginSession];

            entity.AffectedByEmployeeId = sessionModel.EmployeeId;
            entity.AffectedDateTime = DateTime.Now;
            return entity;
        }

        #endregion

        #region Json

        public ContentResult JsonContent(object data)
        {
            string list = JsonConvert.SerializeObject(data,
                Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            return Content(list, "application/json");
        }
        #endregion
    }
}
