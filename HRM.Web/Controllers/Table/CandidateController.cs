﻿using System;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers.Table
{
    public class CandidateController : HrmCandidateController
    {
        private readonly CandidateData _data;

        public CandidateController()
            : this(new CandidateData(new HrmContext()))
        {
        }

        public CandidateController(CandidateData data)
        {
            _data = data;
        }

        #region Views
        public ActionResult Update()
        {
            var candidateId = GetSessionModel().CandidateId;
            return View(candidateId);
        }
        #endregion

        #region Update
        [HttpGet]
        public JsonResult Get(long id)
        {
            var candidate = _data.Get(id);
            return Json(candidate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsEmailUsedExceptCandidate(string email, long candidateId)
        {
            return Json(_data.IsEmailUsedExceptCandidate(email, candidateId), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult TryToUpdate(Candidate entity)
        {
            try
            {
                SetAffectedDetail(entity);
                var candidate = _data.Get(entity.Id);
                entity.MapTo(candidate);
                _data.Replace(candidate);
                _data.SaveChanges();
                return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {             
                throw;
            }
        }
        #endregion
    }
}
