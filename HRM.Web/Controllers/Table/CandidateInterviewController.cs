﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers.Table
{
    public class CandidateInterviewController : HrmCandidateController
    {
        private readonly InterviewCandidateData _data;

        public CandidateInterviewController()
            : this(new InterviewCandidateData(new HrmContext()))
        {
        }

        public CandidateInterviewController(InterviewCandidateData data)
        {
            _data = data;
        }

        #region Views
        public ActionResult Index()
        {
            return Interview();
        }

        public ActionResult Interview()
        {
            var candidateId = GetSessionModel().CandidateId;
            return View(candidateId);
        } 
        #endregion

        #region Interview
        [HttpGet]
        public JsonResult GetInterviews()
        {
            var candidateId = GetSessionModel().CandidateId;
            var applications =
                _data.AllActive()
                    .Where(x => x.CandidateId == candidateId)
                    .Include(x => x.Interview.ManpowerVacancyNotice)               
                    .ToList();
            return Json(applications, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
