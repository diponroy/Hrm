﻿using System;
using System.Linq;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers.Table
{
    public class CandidateLoginController : HrmCandidateController 
    {

        private CandidateLoginData _data;

        public CandidateLoginController()
            : this(new CandidateLoginData(new HrmContext()))
        {
            
        }
        public CandidateLoginController(CandidateLoginData data)
        {
            _data = data;
        }

        public ActionResult Index()
        {
            return View("Login");
        }

        public ActionResult Login()
        {
            return View();
        }
        public ActionResult LogOut()
        {
            base.AbandonSession();
            return View("Login");
        }
        public ActionResult CreateForCandidate(long id)
        {
            return View(id);
        }

        public ActionResult ResetPasswordForCandidate()
        {
            var candidateId = GetSessionModel().CandidateId;
            return View(candidateId);
        }

        [HttpPost]
        public JsonResult TryToCreateLogin(CandidateLogin entity)
        {
            if (_data.IsLoginNameUsed(entity.LoginName) || _data.HasLogin(entity.CandidateId))
            {
                throw new Exception("Invalid to add");
            }

            entity.AffectedDateTime = DateTime.Now;
            entity.Password = PasswordUtility.Encode(entity.Password);
            entity.Status = EntityStatusEnum.Active;
            _data.ValidateAndAdd(entity);
            _data.SaveChanges();
            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ResetPassword(CandidateLogin entity)
        {
            var candidate = _data.GetBy(entity.CandidateId);
            candidate.AffectedDateTime = DateTime.Now;
            candidate.Password = PasswordUtility.Encode(entity.Password);
            _data.ValidateAndReplace(candidate);
            _data.SaveChanges();
            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TryToCreateSession(CandidateLogin login)
        {
            login.Password = PasswordUtility.Encode(login.Password);
            CandidateLogin candidateLogin = _data.Detail.AllActive()
                .FirstOrDefault(x =>
                        x.LoginName == login.LoginName
                        && x.Password == login.Password);

            bool hasLogin = candidateLogin != null;
            if (hasLogin)
            {
                SetCandidateLoginSession(candidateLogin);
            }
            return Json(hasLogin, JsonRequestBehavior.AllowGet);
        }
    }
}
