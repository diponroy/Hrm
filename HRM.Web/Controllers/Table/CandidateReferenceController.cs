﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Db.Contexts;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers.Table
{
    public class CandidateReferenceController : HrmCandidateController
    {
        private readonly CandidateReferenceData _data;

        public CandidateReferenceController()
            : this(new CandidateReferenceData(new HrmContext()))
        {
        }

        public CandidateReferenceController(CandidateReferenceData data)
        {
            _data = data;
        }

        #region Views

        public ActionResult Index()
        {
            long id = GetSessionModel().CandidateId;
            return View("ListForCandidate", id);
        }

        /*id is candidate id*/

        public ActionResult ListForCandidate()
        {
            long id = GetSessionModel().CandidateId;
            return View(id);
        }

        /*id is candidate id*/

        public ActionResult CreateForCandidate(long id)
        {
            return View(id);
        }

        /*id is CandidateEducton id*/

        public ActionResult Update(long id)
        {
            return View(id);
        }

        #endregion

        /*id is candidate id*/

        public ContentResult GetForCandidate(long id)
        {
            List<CandidateReference> list =
                _data.GetForCandidate(id).Where(x => x.Status != EntityStatusEnum.Removed).ToList();
            return JsonContent(list);
        }

        [HttpPost]
        public ActionResult TryToDelete(CandidateReference education)
        {
            CandidateReference aEducation = _data.Get(education.Id);
            aEducation.AffectedDateTime = DateTime.Now;
            _data.Remove(aEducation);
            _data.SaveChanges();
            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }

        #region Update
        [HttpGet]
        public ContentResult Get(long id)
        {
            return JsonContent(_data.Get(id));
        }

        [HttpPost]
        public ActionResult TryToUpdate(CandidateReference entity)
        {
            SetAffectedDetail(entity);
            CandidateReference aEducation = _data.Get(entity.Id);
            entity.MapTo(aEducation);
            _data.Replace(aEducation);
            _data.SaveChanges();
            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create

        [HttpPost]
        public ActionResult TryToCreate(CandidateReference entity)
        {
            entity.Status = EntityStatusEnum.Active;
            SetAffectedDetail(entity);
            _data.Add(entity);
            _data.SaveChanges();
            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
