﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers.Table
{
    public class EmployeePayrollInfoController : HrmController
    {
        private readonly EmployeePayrollInfoData _data = new EmployeePayrollInfoData();
        private readonly EmployeeData _employeeData = new EmployeeData();

        public EmployeePayrollInfoController()
        {
        }

        protected EmployeePayrollInfoController(EmployeePayrollInfoData data)
        {
            _data = data;
        }

        #region View

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Update(long id)
        {
            return View(id);
        }

        public ActionResult Detail(long id)
        {
            return View(id);
        }

        #endregion

        #region List

        [HttpPost]
        public ContentResult Find(long pageNo, long pageSize, EmployeePayrollInfo filter)
        {
            List<EmployeePayrollInfo> payrollInfos = _data.Detail.AllActiveOrInactive().ToList();
            return JsonContent(payrollInfos);
        }

        public JsonResult Delete(EmployeePayrollInfo entity)
        {
            EmployeePayrollInfo payrollInfo = _data.Get(entity.Id);
            SetAffectedDetail(payrollInfo);

            _data.Remove(payrollInfo);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Create

        [HttpPost]
        public JsonResult GetEmployees()
        {
            return Json(_employeeData.AllActiveOrInactive().ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(EmployeePayrollInfo entity)
        {
            SetAffectedDetail(entity);
            _data.Add(entity);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ContentResult Get(long id)
        {
            var payrollInfo = _data.Detail.Get(id);
            return JsonContent(payrollInfo);
        }

        [HttpPost]
        public JsonResult Update(EmployeePayrollInfo entity)
        {
            SetAffectedDetail(entity);
            EmployeePayrollInfo payroll = _data.Get(entity.Id);
            entity.MapTo(payroll);
            _data.Replace(payroll);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ContentResult GetLogs(long id)
        {
            var reportingLog = _data.Detail.AllLog(id);
            return JsonContent(reportingLog);
        }

        #endregion
    }
}