﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Utility;
using HRM.Web.Controllers.Shared;
using Microsoft.Ajax.Utilities;

namespace HRM.Web.Controllers.Table
{
    public class EmployeeReportingController : HrmController
    {
        private readonly EmployeeReportingData _data = new EmployeeReportingData();
        private readonly EmployeeWorkStationUnitData _wsData = new EmployeeWorkStationUnitData();

        public EmployeeReportingController()
        {
        }

        protected EmployeeReportingController(EmployeeReportingData data)
        {
            _data = data;
        }

        #region View

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        #endregion

        #region List

        [HttpPost]
        public ContentResult Find(long pageNo, long pageSize, EmployeeReporting filter)
        {
            List<EmployeeReporting> reportings = _data.Detail.AllActiveOrInactive()
                .Include(x => x.EmployeeWorkStationUnit.Employee)
                .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Company)
                .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Branch)
                .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Department)
                .Include(x => x.ParentEmployeeWorkStationUnit.Employee)
                .Include(x => x.ParentEmployeeWorkStationUnit.WorkStationUnit.Company)
                .Include(x => x.ParentEmployeeWorkStationUnit.WorkStationUnit.Branch)
                .Include(x => x.ParentEmployeeWorkStationUnit.WorkStationUnit.Department)
                .ToList();
            return JsonContent(reportings);
        }

        public JsonResult Delete(EmployeeReporting entity)
        {           
            EmployeeReporting reporting = _data.Get(entity.Id);
            SetAffectedDetail(reporting);

            _data.Remove(reporting);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Create

        [HttpPost]
        public ContentResult GetEmployeeWorkStationUnits()
        {
            IEnumerable<EmployeeWorkStationUnit> units = _wsData.Detail.AllActive().ToList();
            return JsonContent(units);
        }

        [HttpPost]
        public JsonResult Create(EmployeeReporting entity)
        {
            SetAffectedDetail(entity);
            _data.Add(entity);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update
        public ContentResult Get(long id)
        {
            var reporting = _data.Detail
                .AllActiveOrInactive()
                .Where(x => x.Id == id)
                .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Company)
                .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Branch)
                .Include(x => x.EmployeeWorkStationUnit.WorkStationUnit.Department)
                .Include(x => x.ParentEmployeeWorkStationUnit.WorkStationUnit.Company)
                .Include(x => x.ParentEmployeeWorkStationUnit.WorkStationUnit.Branch)
                .Include(x => x.ParentEmployeeWorkStationUnit.WorkStationUnit.Department)
                .Select(x=>x).ToList().Single();
            return JsonContent(reporting);
        }

        [HttpPost]
        public JsonResult Update(EmployeeReporting entity)
        {
            SetAffectedDetail(entity);
            EmployeeReporting reporting = _data.Get(entity.Id);
            entity.MapTo(reporting);
            _data.Replace(reporting);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        [HttpGet]
        public ContentResult GetLogs(long id)
        {
            var reportingLog = _data.Detail
                .AllLog()
                .Where(x => x.Id == id)
                .Include(x => x.EmployeeWorkStationUnitLog.EmployeeLog)
                .Include(x => x.EmployeeWorkStationUnitLog.WorkStationUnitLog.CompanyLog)
                .Include(x => x.EmployeeWorkStationUnitLog.WorkStationUnitLog.BranchLog)
                .Include(x => x.EmployeeWorkStationUnitLog.WorkStationUnitLog.DepartmentLog)
                .Include(x=>x.ParentEmployeeWorkStationUnitLog.EmployeeLog)
                .Include(x => x.ParentEmployeeWorkStationUnitLog.WorkStationUnitLog.CompanyLog)
                .Include(x => x.ParentEmployeeWorkStationUnitLog.WorkStationUnitLog.BranchLog)
                .Include(x => x.ParentEmployeeWorkStationUnitLog.WorkStationUnitLog.DepartmentLog)
                .ToList();
            return JsonContent(reportingLog);
        }

        #endregion
    }
}