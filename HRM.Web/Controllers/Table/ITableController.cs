﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using HRM.Model.Table;

namespace HRM.Web.Controllers.Table
{
    //here id means the primary key of the table
    public interface ITableController<TSource>
    {
        /*views*/
        ActionResult Index();
        ActionResult List();
        ActionResult Create();
        ActionResult Update(long id);
        ActionResult Details(long id);


        /*posts & gets*/

        [HttpGet]
        ContentResult Get(long id);

        [HttpPost]
        ContentResult Find(long pageNo, long pageSize, TSource filter);

        [HttpPost]
        JsonResult Create(TSource entity);

        [HttpPost]
        JsonResult Update(TSource entity);

        [HttpPost]
        JsonResult Delete(long id);

        [HttpGet]
        ContentResult GetLogs(long id);
    }
}
