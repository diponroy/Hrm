﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Log;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers.Table
{
    public class ManpowerCandidateReferenceController : HrmController
    {
        private readonly ManpowerCandidateReferenceData _data = new ManpowerCandidateReferenceData();

        public ManpowerCandidateReferenceController()
        {
        }

        protected ManpowerCandidateReferenceController(ManpowerCandidateReferenceData data)
        {
            _data = data;
        }

        #region View

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Detail(long id)
        {
            return View(id);
        }

        #endregion

        #region List

        public ContentResult Find(long pageNo, long pageSize, ManpowerCandidateReference filter)
        {
            List<ManpowerCandidateReference> list = _data.Detail.AllActiveOrInactive()
                .Include(x => x.ManpowerVacancyNoticeCandidate.ManpowerVacancyNotice)
                .Include(x => x.ManpowerVacancyNoticeCandidate.ManpowerVacancyNotice.ManpowerRequisition)
                .Include(x => x.ManpowerVacancyNoticeCandidate.Candidate)
                .ToList();
            return JsonContent(list);
        }

        #endregion

        #region Detail

        public ContentResult GetLogs(long id)
        {
            List<ManpowerCandidateReferenceLog> list = _data.Detail.AllLog(id)
                                .Include(x => x.ManpowerVacancyNoticeCandidateLog.ManpowerVacancyNoticeLog)
                                .Include(x => x.ManpowerVacancyNoticeCandidateLog.ManpowerVacancyNoticeLog.ManpowerRequisitionLog)
                                .Include(x => x.ManpowerVacancyNoticeCandidateLog.CandidateLog)
                                .ToList();
            return JsonContent(list);
        }

        #endregion
    }
}