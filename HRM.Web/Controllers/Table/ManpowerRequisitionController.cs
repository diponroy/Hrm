﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers.Table
{
    public class ManpowerRequisitionController : HrmController
    {
        private readonly ManpowerRequisitionData _data = new ManpowerRequisitionData();

        public ManpowerRequisitionController()
        {
        }

        protected ManpowerRequisitionController(ManpowerRequisitionData data)
        {
            _data = data;
        }

        #region View

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        #endregion

        #region List

        [HttpPost]
        public ContentResult Find(long pageNo, long pageSize, ManpowerRequisition filter)
        {
            List<ManpowerRequisition> list = _data.AllActiveOrInactive().ToList();
            return JsonContent(list);
        }

        public JsonResult Delete(ManpowerRequisition entity)
        {
            SetAffectedDetail(entity);
            ManpowerRequisition aManpowerRequisition = _data.Get(entity.Id);
            aManpowerRequisition.AffectedByEmployeeId = entity.AffectedByEmployeeId;
            aManpowerRequisition.AffectedDateTime = entity.AffectedDateTime;

            _data.Remove(aManpowerRequisition);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Create

        [HttpPost]
        public JsonResult GenerateTrackNo()
        {
            return Json(_data.GenerateTrackNo(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(ManpowerRequisition entity)
        {
            SetAffectedDetail(entity);
            if (entity.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed !!");
            }

            _data.Add(entity);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        public ContentResult Get(long id)
        {
            return JsonContent(_data.Get(id));
        }

        [HttpPost]
        public JsonResult Update(ManpowerRequisition entity)
        {
            SetAffectedDetail(entity);
            if (entity.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            ManpowerRequisition aManpowerRequisition = _data.Get(entity.Id);
            entity.MapTo(aManpowerRequisition);
            _data.Replace(aManpowerRequisition);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_data.Detail.AllLog(id));
        }

        #endregion
    }
}