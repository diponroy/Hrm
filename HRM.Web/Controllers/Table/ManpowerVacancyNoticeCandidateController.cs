﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Log;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers.Table
{
    public class ManpowerVacancyNoticeCandidateController : HrmController
    {
        private readonly ManpowerVacancyNoticeCandidateData _data = new ManpowerVacancyNoticeCandidateData();

        public ManpowerVacancyNoticeCandidateController()
        {
        }

        protected ManpowerVacancyNoticeCandidateController(ManpowerVacancyNoticeCandidateData data)
        {
            _data = data;
        }

        #region View

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        #endregion

        #region List

        public ContentResult Find(long pageNo, long pageSize, ManpowerVacancyNoticeCandidate filter)
        {
            List<ManpowerVacancyNoticeCandidate> list = _data.Detail.AllActiveOrInactive()
                                .Include(x => x.ManpowerVacancyNotice.ManpowerRequisition)
                                .ToList();
            return JsonContent(list);
        }

        public JsonResult Delete(ManpowerVacancyNoticeCandidate entity)
        {
            SetAffectedDetail(entity);
            ManpowerVacancyNoticeCandidate noticeCandidate = _data.Get(entity.Id);
            noticeCandidate.AffectedByEmployeeId = entity.AffectedByEmployeeId;
            noticeCandidate.AffectedDateTime = entity.AffectedDateTime;
            _data.Remove(noticeCandidate);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_data.Detail.AllLog(id)
                                .Include(x => x.ManpowerVacancyNoticeLog.ManpowerRequisitionLog)
                                .ToList());
        }

        #endregion
    }
}