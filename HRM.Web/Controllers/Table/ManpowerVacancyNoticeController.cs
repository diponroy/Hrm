﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Data.Table;
using HRM.Model.Table;
using HRM.Model.Table.Enums;
using HRM.Utility;
using HRM.Web.Controllers.Shared;

namespace HRM.Web.Controllers.Table
{
    public class ManpowerVacancyNoticeController : HrmController
    {
        private readonly ManpowerVacancyNoticeData _data = new ManpowerVacancyNoticeData();

        public ManpowerVacancyNoticeController()
        {
        }

        protected ManpowerVacancyNoticeController(ManpowerVacancyNoticeData data)
        {
            _data = data;
        }

        #region View

        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpGet]
        public ActionResult Detail(long id)
        {
            return View(id);
        }

        #endregion

        #region List

        public ContentResult Find(long pageNo, long pageSize, ManpowerVacancyNotice filter)
        {
            List<ManpowerVacancyNotice> list = _data.Detail.AllActiveOrInactive().ToList();
            return JsonContent(list);
        }

        public JsonResult Delete(ManpowerVacancyNotice entity)
        {
            SetAffectedDetail(entity);
            ManpowerVacancyNotice employeeVacancyNotice = _data.Get(entity.Id);
            employeeVacancyNotice.AffectedByEmployeeId = entity.AffectedByEmployeeId;
            employeeVacancyNotice.AffectedDateTime = entity.AffectedDateTime;
            _data.Remove(entity);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Create

        public JsonResult GetRequisitions()
        {
            return Json(_data.GetManpoweRequisitions(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GenerateTrackNo()
        {
            return Json(_data.GenerateTrackNo(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(ManpowerVacancyNotice entity)
        {
            SetAffectedDetail(entity);
            if (entity.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("Status cannot be removed !!");
            }

            _data.Add(entity);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update

        [HttpGet]
        public ContentResult Get(long id)
        {
            return JsonContent(_data.Detail.Get(id));
        }

        [HttpPost]
        public JsonResult Update(ManpowerVacancyNotice entity)
        {
            SetAffectedDetail(entity);
            if (entity.Status == EntityStatusEnum.Removed)
            {
                throw new Exception("status shouldn't be removed.");
            }

            ManpowerVacancyNotice aManpowerVacancyNotice = _data.Get(entity.Id);
            entity.MapTo(aManpowerVacancyNotice);
            _data.Replace(aManpowerVacancyNotice);
            _data.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Detail

        public ContentResult GetLogs(long id)
        {
            return JsonContent(_data.Detail.AllLog(id));
        }

        #endregion
    }
}