﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRM.Logic;
using HRM.Model.Table;
using HRM.Web.Controllers.Shared;
using Newtonsoft.Json;

namespace HRM.Web.Controllers
{
    public class WorkStationUnitManageController : HrmController
    {
        private readonly WorkStationManageLogic _logic;

        public WorkStationUnitManageController() : this(new WorkStationManageLogic())
        {
            
        }
        public WorkStationUnitManageController(WorkStationManageLogic logic)
        {
            _logic = logic;
        }

        #region Views
        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            return View("List");
        }

        public ActionResult Create()
        {
            return View("Create");
        }

        public ActionResult Update(long id)
        {
            return View("Update", id);
        }

        public ActionResult Detail(long id)
        {
            return View("Detail", id);
        }
        #endregion

        #region List
        [HttpPost]
        public ContentResult Find(int pageNo, int pageSize, WorkStationUnit filter)
        {
            return JsonContent(_logic.Find(pageNo, pageSize, filter));
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var branch = new WorkStationUnit() { Id = id };
            SetAffectedDetail(branch);
            _logic.Delete(branch);
            return Json(true, behavior: JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create
        [HttpGet]
        public JsonResult GetCompanies()
        {
            return Json(_logic.GetCompanies(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBranches()
        {
            return Json(_logic.GetBranches(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDepartments()
        {
            return Json(_logic.GetDepartments(), JsonRequestBehavior.AllowGet);
        }
        public ContentResult GetWorkStations()
        {
            return JsonContent(_logic.GetWorkStations());
        }
        public JsonResult GetEmployees()
        {
            return Json(_logic.GetEmployees(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult IscombinationUsed(WorkStationUnit model)
        {
            return Json(_logic.IscombinationUsed(model.CompanyId, model.BranchId, model.DepartmentId), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult TryToCreate(WorkStationUnit model)
        {
            SetAffectedDetail(model);
            _logic.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update
        public ContentResult GetPossibleParentWorkStations(long id)
        {
            return JsonContent(_logic.GetPossibleParentWorkStations(id));
        }
        [HttpGet]
        public ActionResult GetWorkStation(long id)
        {
            return JsonContent(_logic.GetWorkStation(id));
        }
        public JsonResult IscombinationUsedExcept(WorkStationUnit model, long stationId)
        {
            return Json(_logic.IscombinationUsed(model.CompanyId, model.BranchId, model.DepartmentId, stationId), JsonRequestBehavior.AllowGet);
        }
        public JsonResult TryToUpdate(WorkStationUnit model)
        {
            SetAffectedDetail(model);
            _logic.Update(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Detail

        [HttpGet]
        public ContentResult StationDetail(long id)
        {
            return JsonContent(_logic.GetWorkStationDetail(id));
        }
        public ContentResult GetLogs(long id)
        {
            return JsonContent(_logic.GetLogs(id));
        }
        #endregion
    }
}
