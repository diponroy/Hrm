﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRM.Web.Models
{
    public class CandidateLoginSession
    {
        public long CandidateId { get; set; }
        public string CandidateName { get; set; }
    }
}