﻿/*Validations*/
ko.validation.rules['TitleUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ title: val });
        $.when(
            $.ajax({
                url: '/Allowance/IsTitleUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal;
    },
    message: 'This Allowance Title is already in use.'
};
ko.validation.registerExtenders();

/*View models*/
function AllowanceCreateViewModel() {
    var self = this;
    
    self.allAllowanceTypes = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);
    
    self.title = ko.observable().extend({ required: true, maxLength: 100, TitleUsed: false });
    self.allowanceTypeId = ko.observable().extend({required:true});
    self.asPercentage = ko.observable();
    self.weight = ko.observable().extend({digit:true});
    self.status = ko.observable().extend({ required: true });
    
    
    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.allowanceObj = function () {
        return {
            Title: self.title(),
            AllowanceTypeId: self.allowanceTypeId(),
            AsPercentage: self.hasPercentage(),
            Weight: self.weight(),
            Status: self.status()
        };
    };

    self.hasPercentage = function() {
        if (self.asPercentage())
            return true;
        return false;
    };
    
    self.getAllowanceTypes = function () {
        jax.getJson(
            '/Allowance/GetAllowanceTypes',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Name,
                        value: obj.Id
                    });
                });
                self.allAllowanceTypes(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/Allowance/Create',
            self.allowanceObj(),
            function () {
                ToastSuccess('Allowance created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };
    
    /*initialize & resets*/
    self.reset = function () {
        self.title('');
        self.allowanceTypeId('');
        self.asPercentage('');
        self.weight('');
        self.status('');
        self.removeErrors();
  };

    self.init = function () {
        self.getAllowanceTypes();
        
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        
        self.reset();
        
        activeParentMenu($('li.sub a[href="/Allowance"]'));
    };
}

$(document).ready(function () {
    var vm = new AllowanceCreateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});