﻿function AllowanceViewModel() {
    var self = this;
    self.id = ko.observable();
    self.allowance = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getAllowance = function () {
        jax.getJsonBlock(
            '/Allowance/Get/' + self.id(),
            function (data) {
                self.allowance({
                    title: data.Title,
                    allowanceTypeName: data.AllowanceType.Name,
                    weight:data.Weight
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/Allowance/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var allowance = data[index];
                    array.push({
                        id: allowance.Id,
                        title: allowance.Title,
                        allowanceTypeName: allowance.AllowanceTypeLog.Name,
                        asPercentage:allowance.AsPercentage,
                        weight:allowance.Weight,
                        status: allowance.Status,
                        statusString: allowance.StatusString,
                        affectedByEmployeeName: allowance.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(allowance.affectedDateTime),
                        logStatus: allowance.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.init = function () {
        self.id($('#allowanceId').val());
        self.getAllowance();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new AllowanceViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});