﻿function AllowanceListViewModel() {
    var self = this;
    self.allowances = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.allowances([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/Allowance/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var allowance = data[index];
                    array.push({
                        id: allowance.Id,
                        title: allowance.Title,
                        allowanceTypeName: allowance.AllowanceType.Name,
                        asPercentage: allowance.AsPercentage,
                        weight: allowance.Weight,
                        status: allowance.Status,
                        statusString: allowance.StatusString
                    });
                });
                self.allowances(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/Allowance/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/Allowance/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/Allowance/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        activeParentMenu($('li.sub a[href="/Allowance"]'));
    };
}

$(document).ready(function () {
    var viewModel = new AllowanceListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});