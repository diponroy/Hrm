﻿/*Validations*/
ko.validation.rules['TitleUsedExceptItself'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#allowanceId').val(), title: val });
        $.when(
            $.ajax({
                url: '/Allowance/TitleUsedExceptItself',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            if (textStatus === 'success') {
                isUsed = data;
            } else {
                ToastError(xhr);
                isUsed = null;
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist.'
};
ko.validation.registerExtenders();


/*View models*/
function AllowanceUpdateViewModel() {
    var self = this;
    self.id = ko.observable().extend({ required: true });
    self.title = ko.observable().extend({ required: true, maxLength: 100, TitleUsedExceptItself: false });
    self.allowanceTypeId = ko.observable().extend({ required: true });
    self.allAllowanceTypes = ko.observableArray([]);
    self.asPercentage = ko.observable().extend({ required: true });
    self.weight = ko.observable().extend({ digit: true });
    self.status = ko.observable().extend({ required: true, });
    self.allStatus = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.vmObj = function () {
        return {
            Id: self.id(),
            Title: self.title(),
            AllowanceTypeId: self.allowanceTypeId(),
            AsPercentage: self.hasPercentage(),
            Weight: self.weight(),
            Status: self.status()
        };
    };
    
    self.hasPercentage = function () {
        if (self.asPercentage())
            return true;
        return false;
    };
    
    self.getAllowanceTypes = function () {
        jax.getJson(
            '/Allowance/GetAllowanceTypes',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({ text: obj.Name, value: obj.Id });
                });
                self.allAllowanceTypes(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadAllowance = function () {
        jax.getJson(
            '/Allowance/Get/' + self.id(),
            function (data) {
                self.title(data.Title);
                self.allowanceTypeId(data.AllowanceTypeId);
                self.asPercentage(data.AsPercentage);
                self.weight(data.Weight);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/Allowance/Update',
            self.vmObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadAllowance();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.loadAllowance();
    };
    self.init = function () {
        self.id($('#allowanceId').val());
        self.getAllowanceTypes();
        
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        
        self.loadAllowance();
        
        activeParentMenu($('li.sub a[href="/Allowance"]'));
    };
}

$(document).ready(function () {

    var vm = new AllowanceUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});