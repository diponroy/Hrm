﻿/*Validations*/
ko.validation.rules['NameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/AllowanceType/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal;
    },
    message: 'This Allowance Type is already in use.'
};
ko.validation.registerExtenders();


/*View models*/
function AllowanceTypeViewModel() {
    var self = this;
    self.allStatus = ko.observableArray([]);
    self.name = ko.observable().extend({ required: true, maxLength: 100, NameUsed: false });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.status = ko.observable().extend({required: true,});

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {return (self.errors().length > 0) ? true : false;};
    self.showErrors = function () {self.errors.showAllMessages();};
    self.removeErrors = function () {self.errors.showAllMessages(false);};

    /*objects to post*/
    self.allowanceTypeObj = function () {
        return {
            Name: self.name(),
            Remarks:self.remarks(),
            Status: self.status()
        };
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/AllowanceType/Create',
            self.allowanceTypeObj(),
            function (data) {
                ToastSuccess('Allowance Type created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.name('');
        self.remarks('');
        self.status('');
        self.removeErrors();
        
        activeParentMenu($('li.sub a[href="/AllowanceType"]'));
    };
    
    self.init = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.reset();
    };
}

$(document).ready(function () {
    var vm = new AllowanceTypeViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});