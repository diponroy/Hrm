﻿function AllowanceTypeViewModel() {
    var self = this;
    self.id = ko.observable();
    self.allowanceType = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getAllowanceType = function () {
        jax.getJsonBlock(
            '/AllowanceType/Get/' + self.id(),
            function (data) {
                self.allowanceType({
                    name: data.Name,
                    remarks: data.Remarks,
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/AllowanceType/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var allowanceType = data[index];
                    array.push({
                        id: allowanceType.Id,
                        name: allowanceType.Name,
                        remarks: allowanceType.Remarks == null ? '---' : allowanceType.Remarks,
                        status: allowanceType.Status,
                        statusString: allowanceType.StatusString,
                        affectedByEmployeeName: allowanceType.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(allowanceType.affectedDateTime),
                        logStatus: allowanceType.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#allowanceTypeId').val());
        self.getAllowanceType();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new AllowanceTypeViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});