﻿function AllowanceTypeViewModel() {
    var self = this;
    self.allowanceTypes = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.allowanceTypes([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/AllowanceType/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var allowanceType = data[index];
                    array.push({
                        id: allowanceType.Id,
                        name: allowanceType.Name,
                        remarks:allowanceType.Remarks,
                        status: allowanceType.Status,
                        statusString: allowanceType.StatusString
                    });
                });
                self.allowanceTypes(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/AllowanceType/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/AllowanceType/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/AllowanceType/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/AllowanceType"]'));

    };
}

$(document).ready(function () {
    var viewModel = new AllowanceTypeViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});