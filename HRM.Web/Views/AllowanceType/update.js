﻿/*Validations*/
ko.validation.rules['NameUsedExceptItself'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#allowanceTypeId').val(), name: val });
        $.when(
            $.ajax({
                url: '/AllowanceType/NameUsedExceptItself',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            if (textStatus === 'success') {
                isUsed = data;
            } else {
                ToastError(xhr);
                isUsed = null;
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist.'
};
ko.validation.registerExtenders();


/*View models*/
function AllowanceTypeUpdateViewModel() {
    var self = this;
    self.id = ko.observable();
    self.name = ko.observable().extend({ required: true, maxLength: 100, NameUsedExceptItself: false });
    self.remarks = ko.observable().extend({ maxLength: 150});
    self.status = ko.observable().extend({ required: true, });
    self.allStatus = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.vmObj = function () {
        return {
            Id: self.id(),
            Name: self.name(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.loadAllowanceType = function () {
        jax.getJson(
            '/AllowanceType/Get/' + self.id(),
            function (data) {
                self.name(data.Name);
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/AllowanceType/Update',
            self.vmObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadAllowanceType();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.loadAllowanceType();
    };
    self.init = function () {
        self.id($('#allowanceTypeId').val());
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.loadAllowanceType();
        
        activeParentMenu($('li.sub a[href="/AllowanceType"]'));
    };
}

$(document).ready(function () {

    var vm = new AllowanceTypeUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});