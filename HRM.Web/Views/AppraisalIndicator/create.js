﻿/*Validations*/
ko.validation.rules['titleUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ title: val });
        $.when(
            $.ajax({
                url: '/AppraisalIndicator/IsTitleUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist.'
};
ko.validation.registerExtenders();

function AppraisalIndicatorCreateViewModel() {
    var self = this;
    
    self.allActiveParents = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);
    
    self.title = ko.observable().extend({ required: true, maxlength: 100, titleUsed: false });
    self.maxWeight = ko.observable().extend({ required: true, digit:true });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.parentId = ko.observable();
    self.status = ko.observable().extend({ required: true });
    

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.postObject = function () {
        return {
            Title: self.title(),
            MaxWeight:self.maxWeight(),
            Remarks: self.remarks(),
            ParentId: self.parentId(),
            Status: self.status()
        };
    };

    ///get Parent list
    self.loadParents = function () {
        self.allActiveParents([]);
        jax.getJson(
            '/AppraisalIndicator/GetActiveParents',
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allActiveParents(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/AppraisalIndicator/Create',
            self.postObject(),
            function () {
                ToastSuccess('A new Indicator created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.reset = function () {
        self.loadParents();
        self.title('');
        self.maxWeight('');
        self.remarks('');
        self.parentId('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.loadParents();
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.reset();
        
        activeParentMenu($('li.sub a[href="/AppraisalIndicator"]'));
    };
}

$(document).ready(function () {
    var viewModel = new AppraisalIndicatorCreateViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();
});

