﻿function EmployeeTypeViewModel() {
    var self = this;
    self.id = ko.observable();
    self.appraisalIndicator = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all*/
    self.getAppraisalIndicator = function () {
        jax.getJsonBlock(
            '/AppraisalIndicatorManage/Get/' + self.id(),
            function (data) {
                self.appraisalIndicator({
                    title: data.Title,
                    maxWeight: data.MaxWeight,
                    remarks: data.Remarks,
                    parentTitle: (data.ParentAppraisalIndicator === null) ? 'Not yet assigned' : data.ParentAppraisalIndicator.Title,
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/AppraisalIndicatorManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var object = data[index];
                    array.push({
                        id: object.Id,
                        title: object.Title,
                        maxWeight: object.MaxWeight,
                        remarks: object.Remarks==null?'---':object.Remarks,
                        parentId: object.ParentId,
                        parentTitle: (object.ParentAppraisalIndicatorLog === null) ? "Not yet assigned" : object.ParentAppraisalIndicatorLog.Title,
                        status: object.Status,
                        statusString: object.StatusString,
                        affectedByName: object.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(object.affectedDateTime),
                        logStatus: object.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#appraisalIndicatorId').val());
        self.getAppraisalIndicator();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeTypeViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});