﻿/*view models*/
function AppraisalIndicatorListViewModel() {
    var self = this;
    self.appraisalIndicators = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getAppraisalIndicator = function (pageNo, pageSize, searchFilters) {
        self.appraisalIndicators([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/AppraisalIndicator/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var appraisalIndicator = data[index];
                    array.push({
                        id: appraisalIndicator.Id,
                        title: appraisalIndicator.Title,
                        parentAppraisalIndicatorTitle: (appraisalIndicator.ParentAppraisalIndicator != null) ? appraisalIndicator.ParentAppraisalIndicator.Title : 'Not yet assigned',
                        maxWeight: appraisalIndicator.MaxWeight,
                        remarks: appraisalIndicator.Remarks,
                        status: appraisalIndicator.Status,
                        statusString: appraisalIndicator.StatusString
                    });
                });
                self.appraisalIndicators(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function (item) {
        window.location = '/AppraisalIndicator/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/AppraisalIndicator/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        var hasActiveChilds = null;
        jax.getJsonDfd(
            '/AppraisalIndicator/CheckBeforeDelete/' + item.id,
            function (data) {
                hasActiveChilds = data;
            }
        );
        if (hasActiveChilds === true) {
            bootbox.alert("Can't delete this type because it have one or more dependent type.");
            return;
        }
        bootbox.confirm("Are you sure, you want to delete this type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJson(
            '/AppraisalIndicator/Delete',
            obj,
            function (data) {
                ToastSuccess("Information deleted successfully");
                self.getAppraisalIndicator(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.resetFilers();
        self.getAppraisalIndicator(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/AppraisalIndicator"]'));
    };
}


$(document).ready(function () {
    var vm = new AppraisalIndicatorListViewModel();
    ko.applyBindings(vm);
    vm.init();
});