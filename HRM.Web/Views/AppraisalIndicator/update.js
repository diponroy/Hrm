﻿/*Validations*/
ko.validation.rules['titleUsedExceptItself'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#appraisalIndicatorId').val(), title: val });
        $.when(
            $.ajax({
                url: '/AppraisalIndicator/TitleUsedExceptItself',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            if (textStatus === 'success') {
                isUsed = data;
            } else {
                ToastError(xhr);
                isUsed = null;
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist.'
};
ko.validation.registerExtenders();


/*View models*/
function AppraisalIndicatorUpdateViewModel() {
    var self = this;
    
    self.allAssignableParents = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);
    
    self.id = ko.observable();
    self.title = ko.observable().extend({ required: true, maxLength: 50, titleUsedExceptItself: false });
    self.maxWeight = ko.observable().extend({ required: true, digit:true});
    self.remarks = ko.observable().extend({ maxLength: 150, });
    self.parentId = ko.observable();
    self.status = ko.observable().extend({ required: true, });
    

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Id: self.id(),
            Title: self.title(),
            MaxWeight:self.maxWeight(),
            Remarks: self.remarks(),
            ParentId: self.parentId(),
            Status: self.status()
        };
    };

    ///get Id list
    self.getAssignableParents = function () {
       return jax.getJson(
            '/AppraisalIndicator/GetAssignableParents/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var object = data[index];
                    array.push({
                        title: object.Title,
                        id: object.Id
                    });
                });
                self.allAssignableParents(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.loadAppraisalIndicator = function () {
        jax.getJson(
            '/AppraisalIndicator/Get/' + self.id(),
            function (data) {
                self.title(data.Title);
                self.maxWeight(data.MaxWeight);
                self.remarks(data.Remarks);
                self.parentId(data.ParentId);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/AppraisalIndicator/Update',
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadAppraisalIndicator();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        
        var calls = [];
        calls.push(self.getAssignableParents());
        $.when.apply(this, calls).done(function () {
            self.loadAppraisalIndicator(self.id());
        });
        self.removeErrors();

    };
    self.init = function () {
        self.id($('#appraisalIndicatorId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/AppraisalIndicator"]'));
    };
}

$(document).ready(function () {

    var vm = new AppraisalIndicatorUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});