﻿/*Validations*/
ko.validation.rules['TitleUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ title: val });
        $.when(
            $.ajax({
                url: '/Bonus/IsTitleUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal;
    },
    message: 'This Bonus Title is already in use.'
};
ko.validation.registerExtenders();

/*View models*/
function BonusCreateViewModel() {
    var self = this;
    self.title = ko.observable().extend({ required: true, maxLength: 100, TitleUsed: false });
    self.bonusTypeId = ko.observable().extend({required:true});
    self.allBonusTypes = ko.observableArray([]);
    self.asPercentage = ko.observable(true).extend({required:true});
    self.weight = ko.observable().extend({digit:true});
    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);
    
    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.bonusObj = function () {
        return {
            Title: self.title(),
            BonusTypeId: self.bonusTypeId(),
            AsPercentage: self.hasPercentage(),
            Weight: self.weight(),
            Status: self.status()
        };
    };

    self.hasPercentage = function() {
        if (self.asPercentage())
            return true;
        return false;
    };
    
    self.getBonusTypes = function () {
        jax.getJson(
            '/Bonus/GetBonusTypes',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({ text: obj.Name, value: obj.Id });
                });
                self.allBonusTypes(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/Bonus/Create',
            self.bonusObj(),
            function (data) {
                ToastSuccess('Bonus created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.title('');
        self.bonusTypeId('');
        self.asPercentage('');
        self.weight('');
        self.status('');
        self.removeErrors();
  };

    self.init = function () {
        self.getBonusTypes();
        
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/Bonus"]'));
    };
}

$(document).ready(function () {
    var vm = new BonusCreateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});