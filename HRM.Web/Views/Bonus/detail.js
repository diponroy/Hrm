﻿function BonusViewModel() {
    var self = this;
    self.id = ko.observable();
    self.bonus = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getBonus = function () {
        jax.getJsonBlock(
            '/Bonus/Get/' + self.id(),
            function (data) {
                self.bonus({
                    title: data.Title,
                    bonusTypeName: data.BonusType.Name,
                    weight:data.Weight
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/Bonus/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var bonus = data[index];
                    array.push({
                        id: bonus.Id,
                        title: bonus.Title,
                        bonusTypeName: bonus.BonusTypeLog.Name,
                        asPercentage:bonus.AsPercentage,
                        weight:bonus.Weight,
                        status: bonus.Status,
                        statusString: bonus.StatusString,
                        affectedByEmployeeName: bonus.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(bonus.affectedDateTime),
                        logStatus: bonus.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#bonusId').val());
        self.getBonus();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new BonusViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});