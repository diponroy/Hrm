﻿function BonusListViewModel() {
    var self = this;
    self.bonuses = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.bonuses([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/Bonus/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var bonus = data[index];
                    array.push({
                        id: bonus.Id,
                        title: bonus.Title,
                        bonusTypeName: bonus.BonusType.Name,
                        asPercentage: bonus.AsPercentage,
                        weight: bonus.Weight,
                        status: bonus.Status,
                        statusString: bonus.StatusString
                    });
                });
                self.bonuses(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/Bonus/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/Bonus/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/Bonus/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/Bonus"]'));
    };
}

$(document).ready(function () {
    var viewModel = new BonusListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});