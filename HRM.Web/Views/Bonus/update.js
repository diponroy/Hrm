﻿/*Validations*/
ko.validation.rules['TitleUsedExceptItself'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#bonusId').val(), title: val });
        $.when(
            $.ajax({
                url: '/Bonus/TitleUsedExceptItself',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            if (textStatus === 'success') {
                isUsed = data;
            } else {
                ToastError(xhr);
                isUsed = null;
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist.'
};
ko.validation.registerExtenders();


/*View models*/
function BonusUpdateViewModel() {
    var self = this;
    self.id = ko.observable();
    self.title = ko.observable().extend({ required: true, maxLength: 100, TitleUsedExceptItself: false });
    self.bonusTypeId = ko.observable().extend({ required: true });
    self.allBonusTypes = ko.observableArray([]);
    self.asPercentage = ko.observable().extend({ required: true });
    self.weight = ko.observable().extend({ digit: true });
    self.status = ko.observable().extend({ required: true, });
    self.allStatus = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.vmObj = function () {
        return {
            Id: self.id(),
            Title: self.title(),
            BonusTypeId: self.bonusTypeId(),
            AsPercentage: self.hasPercentage(),
            Weight: self.weight(),
            Status: self.status()
        };
    };
    
    self.hasPercentage = function () {
        if (self.asPercentage())
            return true;
        return false;
    };
    
    self.getBonusTypes = function () {
        jax.getJson(
            '/Bonus/GetBonusTypes',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({ text: obj.Name, value: obj.Id });
                });
                self.allBonusTypes(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadBonus = function () {
        jax.getJson(
            '/Bonus/Get/' + self.id(),
            function (data) {
                self.title(data.Title);
                self.bonusTypeId(data.BonusTypeId);
                self.asPercentage(data.AsPercentage);
                self.weight(data.Weight);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/Bonus/Update',
            self.vmObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadBonus();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.loadBonus();
    };
    self.init = function () {
        self.id($('#bonusId').val());
        self.getBonusTypes();
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.loadBonus();
        
        activeParentMenu($('li.sub a[href="/Bonus"]'));
    };
}

$(document).ready(function () {

    var vm = new BonusUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});