﻿function BonusPaymentDateCreateViewModel() {
    var self = this;
    self.bonusId = ko.observable().extend({ required: true });
    self.allBonusTitles = ko.observableArray([]);
    self.estimatedDate = ko.observable(currentDate()).extend({ required: true });
    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);

    self.errors = ko.validation.group(self);
    self.hasErrors = function() { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function() { self.errors.showAllMessages(); };
    self.removeErrors = function() { self.errors.showAllMessages(false); };

    /* Local Actions */
    self.vmObj = function() {
        return {
            BonusId: self.bonusId(),
            EstimatedDate: self.estimatedDate(),
            Status: self.status()
        };
    };

    self.getBonusTitles = function() {
        jax.getJson(
            '/BonusPaymentDate/GetBonusTitles',
            function (data) {
                var array = [];
                $.each(data, function(index) {
                    var bonus = data[index];
                    array.push({
                        text: bonus.Title,
                        value: bonus.Id
                    });
                });
                self.allBonusTitles(array);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        self.saveToServer();
    };

    /* Server Actions */
    self.saveToServer = function() {
        jax.postJsonBlock(
            '/BonusPaymentDate/Create',
            self.vmObj(),
            function() {
                ToastSuccess('Created successfully');
                self.reset();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };
    self.reset = function() {
        self.bonusId('');
        self.estimatedDate(currentDate());
        self.status('');
        self.removeErrors();
    };

    self.init = function() {
        self.getBonusTitles();
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/BonusPaymentDate"]'));
    };

}

$(document).ready(function() {
    $('.datepicker').datepicker();

    var viewModel = new BonusPaymentDateCreateViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();

    $('#btnShowDatePicker').click(function() {
        $('#btnEstimatedDate').focus();
    });
    $('#btnResetDatePicker').click(function() {
        viewModel.estimatedDate(currentDate());
    });
});


