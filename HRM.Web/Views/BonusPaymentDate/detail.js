﻿function BonusPaymentDateDetailViewModel() {
    var self = this;
    self.id = ko.observable();
    self.bonusPaymentDate = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getBonusPaymentDate = function () {
        jax.getJsonBlock(
            '/BonusPaymentDate/Get/' + self.id(),
            function (data) {
                self.bonusPaymentDate({
                    bonusTitle: data.Bonus.Title,
                    estimatedDate: clientDateStringFromDate(data.EstimatedDate)
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/BonusPaymentDate/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var bonusPaymentDate = data[index];
                    array.push({
                        id: bonusPaymentDate.Id,
                        bonusTitle: bonusPaymentDate.BonusLog.Title,
                        estimatedDate: clientDateTimeStringFromDate(bonusPaymentDate.EstimatedDate),
                        status: bonusPaymentDate.Status,
                        statusString: bonusPaymentDate.StatusString,
                        affectedByEmployeeName: bonusPaymentDate.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(bonusPaymentDate.affectedDateTime),
                        logStatus: bonusPaymentDate.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#bonusPaymentDateId').val());
        self.getBonusPaymentDate();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new BonusPaymentDateDetailViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});