﻿function BonusPaymentDateListViewModel() {
    var self = this;
    self.bonusPaymentDates = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.bonusPaymentDates([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/BonusPaymentDate/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var bonusPaymentDate = data[index];
                    array.push({
                        id: bonusPaymentDate.Id,
                        bonusTitle: bonusPaymentDate.Bonus.Title,
                        estimatedDate: clientDateStringFromDate(bonusPaymentDate.EstimatedDate),
                        status: bonusPaymentDate.Status,
                        statusString: bonusPaymentDate.StatusString
                    });
                });
                self.bonusPaymentDates(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/BonusPaymentDate/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/BonusPaymentDate/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/BonusPaymentDate/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        activeParentMenu($('li.sub a[href="/BonusPaymentDate"]'));
    };
}

$(document).ready(function () {
    var viewModel = new BonusPaymentDateListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});