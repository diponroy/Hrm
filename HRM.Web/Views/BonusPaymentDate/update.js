﻿/*View models*/
function BonusPaymentDateUpdateViewModel() {
    var self = this;
    self.id = ko.observable();
    self.bonusId = ko.observable().extend({ required: true });
    self.allBonusTitles = ko.observableArray([]);
    self.estimatedDate = ko.observable().extend({required:true});
    self.status = ko.observable().extend({ required: true, });
    self.allStatus = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.vmObj = function () {
        return {
            Id: self.id(),
            BonusId: self.bonusId(),
            EstimatedDate: self.estimatedDate(),
            Status: self.status()
        };
    };

   self.getBonusTitles = function () {
        jax.getJson(
            '/BonusPaymentDate/GetBonusTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allBonusTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadBonusPaymentDate = function () {
        jax.getJson(
            '/BonusPaymentDate/Get/' + self.id(),
            function (data) {
                self.bonusId(data.BonusId);
                self.estimatedDate(clientDate(data.EstimatedDate));
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/BonusPaymentDate/Update',
            self.vmObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadBonusPaymentDate();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.loadBonusPaymentDate();
    };
    self.init = function () {
        self.id($('#bonusPaymentDateId').val());
        self.getBonusTitles();
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.loadBonusPaymentDate();
        
        activeParentMenu($('li.sub a[href="/BonusPaymentDate"]'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new BonusPaymentDateUpdateViewModel();
    ko.applyBindings(vm);
    vm.init();
    
    $('#btnShowEstimatedDate').click(function () {
        $('#btnEstimatedDate').focus();
    });
    $('#btnResetEstimatedDate').click(function () {
        vm.estimatedDate(currentDate());
    });
});