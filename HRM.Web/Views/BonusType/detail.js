﻿function BonusTypeDetailViewModel() {
    var self = this;
    self.id = ko.observable();
    self.bonusType = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all*/
    self.getAllowanceType = function () {
        jax.getJsonBlock(
            '/BonusType/Get/' + self.id(),
            function (data) {
                self.bonusType({
                    name: data.Name,
                    remarks: data.Remarks,
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/BonusType/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var allowanceType = data[index];
                    array.push({
                        id: allowanceType.Id,
                        name: allowanceType.Name,
                        remarks: allowanceType.Remarks,
                        status: allowanceType.Status,
                        statusString: allowanceType.StatusString,
                        affectedByEmployeeName: allowanceType.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(allowanceType.affectedDateTime),
                        logStatus: allowanceType.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#bonusTypeId').val());
        self.getAllowanceType();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new BonusTypeDetailViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();
});