﻿function BonusTypeListViewModel() {
    var self = this;
    self.bonusTypes = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.bonusTypes([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/BonusType/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var bonusType = data[index];
                    array.push({
                        id: bonusType.Id,
                        name: bonusType.Name,
                        remarks: bonusType.Remarks,
                        status: bonusType.Status,
                        statusString: bonusType.StatusString
                    });
                });
                self.bonusTypes(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/BonusType/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/BonusType/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/BonusType/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/BonusType"]'));
    };
}

$(document).ready(function () {
    var viewModel = new BonusTypeListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});