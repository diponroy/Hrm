﻿/*Validations*/
ko.validation.rules['NameUsedExceptItself'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#bonusTypeId').val(), name: val });
        $.when(
            $.ajax({
                url: '/BonusType/NameUsedExceptItself',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            if (textStatus === 'success') {
                isUsed = data;
            } else {
                ToastError(xhr);
                isUsed = null;
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist.'
};
ko.validation.registerExtenders();


/*View models*/
function BonusTypeUpdateViewModel() {
    var self = this;
    self.id = ko.observable();
    self.name = ko.observable().extend({ required: true, maxLength: 100, NameUsedExceptItself: false });
    self.remarks = ko.observable().extend({ maxLength: 150});
    self.status = ko.observable().extend({ required: true});
    self.allStatus = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.vmObj = function () {
        return {
            Id: self.id(),
            Name: self.name(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.loadBonusType = function () {
        jax.getJson(
            '/BonusType/Get/' + self.id(),
            function (data) {
                self.name(data.Name);
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/BonusType/Update',
            self.vmObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadBonusType();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.loadBonusType();
    };
    self.init = function () {
        self.id($('#bonusTypeId').val());
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.loadBonusType();
        
        activeParentMenu($('li.sub a[href="/BonusType"]'));
    };
}

$(document).ready(function () {
    var vm = new BonusTypeUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});