﻿/*Validations*/
ko.validation.rules['branchNameUsed'] = {
    validator: function(val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/Branch/IsBranchNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function(data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal;
    },
    message: 'This Branch Name is already in use.'
};
ko.validation.registerExtenders();


/*View models*/

function BranchViewModel() {
    var self = this;
    /*ddl*/
    self.allStatus = ko.observableArray([]);

    /*fields*/
    self.name = ko.observable().extend({
        required: true,
        maxLength: 50,
        branchNameUsed: false
    });
    self.dateOfCreation = ko.observable(currentDate()).extend({
        required: true,
    });
    self.address = ko.observable().extend({
        maxLength: 150,
    });
    self.remarks = ko.observable().extend({
        maxLength: 150,
    });
    self.status = ko.observable().extend({
        required: true,
    });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function() {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function() {
        self.errors.showAllMessages();
    };
    self.removeErrors = function() {
        self.errors.showAllMessages(false);
    };

    /*objects to post*/
    self.branchObj = function() {
        return {
            Name: self.name(),
            DateOfCreation: self.dateOfCreation(),
            Address: self.address(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/Branch/TryToCreate',
            self.branchObj(),
            function(data) {
                ToastSuccess('Branch created successfully');
                self.reset();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function() {
        self.name('');
        self.dateOfCreation(currentDate());
        self.address('');
        self.remarks('');
        self.status('');

        self.removeErrors();
    };
    self.init = function() {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();

        activeParentMenu($('li.sub a[href="/Branch"]'));
    };
}

$(document).ready(function() {
    $('.datepicker').datepicker();

    var vm = new BranchViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowDatePicker').click(function() {
        $('.datepicker').focus();
    });

    $('#btnResetDatePicker').click(function() {
        vm.dateOfCreation(currentDate());
    });
});