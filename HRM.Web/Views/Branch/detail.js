﻿/*View models*/
function BranchViewModel() {
    var self = this;
    self.id = ko.observable();
    self.branch = ko.observable();
    self.logs = ko.observableArray([]);

    self.getBranch = function () {
        jax.getJsonBlock(
            '/Branch/Get/' + self.id(),
            function (data) {
                self.branch({
                    name: data.Name,
                    dateOfCreation: clientDateStringFromDate(data.DateOfCreation),
                    dateOfClosing: (data.DateOfClosing === null) ? '--' : clientDateStringFromDate(data.DateOfClosing),
                    address: data.Address,
                    remarks: data.Remarks,
                    status: data.Status,
                    statusString: data.StatusString,
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/Branch/GetLogs/' + self.id(),
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aBranch = data[index];
                    arr.push({
                        id: aBranch.Id,
                        name: aBranch.Name,
                        address: aBranch.Address,
                        remarks: aBranch.Remarks,
                        dateOfCreation: clientDateStringFromDate(aBranch.DateOfCreation),
                        dateOfClosing: (!aBranch.IsClosed) ? '--' : clientDateStringFromDate(aBranch.DateOfClosing),
                        isClosed: aBranch.IsClosed,
                        status: aBranch.Status,
                        statusString: aBranch.StatusString,
                        affectedByName: aBranch.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(aBranch.AffectedDateTime),
                        logStatus: aBranch.LogStatuses
                    });
                });
                self.logs(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtBranchId').val());
        self.getBranch();
        self.getLogs();
    };
}

$(document).ready(function () {
    var vm = new BranchViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});