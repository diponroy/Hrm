﻿/*view models*/
function BranchListViewModel() {
    var self = this;
    self.branches = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function() {

    };
    self.resetFilers = function() {

    };

    self.getBranches = function(pageNo, pageSize, searchFilters) {
        self.branches([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/Branch/Find',
            obj,
            function(data) {
                var arr = new Array();
                $.each(data, function(index) {
                    var aBranch = data[index];
                    arr.push({
                        id: aBranch.Id,
                        name: aBranch.Name,
                        address: aBranch.Address,
                        dateOfCreation: clientDateStringFromDate(aBranch.DateOfCreation),
                        dateOfClosing: (aBranch.DateOfClosing === null) ? '--' : clientDateStringFromDate(aBranch.DateOfClosing),
                        isClosed: aBranch.DateOfClosing != null,
                        status: aBranch.Status,
                        statusString: aBranch.StatusString
                    });
                });
                self.branches(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.showToEdit = function(item) {
        window.location = '/Branch/Update/' + item.id;
    };
    self.showDetail = function(item) {
        window.location = '/Branch/Detail/' + item.id;
    };

    self.delete = function(id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/Branch/Delete',
            obj,
            function(data) {
                ToastSuccess("Branch deleted successfully");
                self.getBranches(1, 25, self.searchFilers());
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.confirmToDelete = function(item) {
        bootbox.confirm("Are you sure, you want to delete the branch ?", function(result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    self.init = function() {
        self.resetFilers();
        self.getBranches(1, 25, self.searchFilers());

        activeParentMenu($('li.sub a[href="/Branch"]'));
    };
}

$(document).ready(function() {
    var vm = new BranchListViewModel();
    ko.applyBindings(vm);
    vm.init();
});