﻿/*Validations*/
ko.validation.rules['branchNameUsedExceptBranch'] = {
    validator: function(val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ branchId: $('#txtBranchId').val(), name: val });
        $.when(
            $.ajax({
                url: '/Branch/IsBranchNameUsedExceptBranch',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function(data, textStatus, jqXhr) {
            if (textStatus === 'success') {
                isUsed = data;
            } else {
                ToastError(xhr);
                isUsed = null;
            }
        });
        return isUsed === otherVal;
    },
    message: 'This Branch Name is already in use.'
};
ko.validation.registerExtenders();


/*View models*/

function BranchViewModel() {
    var self = this;
    /*ddl*/
    self.allStatus = ko.observableArray([]);

    /*fields*/
    self.id = ko.observable();
    self.name = ko.observable().extend({
        required: true,
        maxLength: 50,
        branchNameUsedExceptBranch: false
    });
    self.dateOfCreation = ko.observable().extend({
        required: true,
    });
    self.hasClosingDate = ko.observable(false);
    self.dateOfClosing = ko.observable().extend({
        required: true,
    });
    self.address = ko.observable().extend({
        maxLength: 150,
    });
    self.remarks = ko.observable().extend({
        maxLength: 150,
    });
    self.status = ko.observable().extend({
        required: true,
    });

    /*errors*/
    self.errorsWithOutStatus = ko.validation.group([self.name, self.dateOfCreation, self.dateOfClosing, self.address, self.remarks]);
    self.errorsWithStatus = ko.validation.group([self.name, self.dateOfCreation, self.address, self.remarks, self.status]);
    self.errors = ko.computed(function() {
        return self.hasClosingDate() ? self.errorsWithOutStatus : self.errorsWithStatus;
    }, this);
    self.hasErrors = function() {
        return ((self.errors()()).length > 0) ? true : false;
    };
    self.showErrors = function() {
        self.errors().showAllMessages();
    };
    self.removeErrors = function() {
        self.errors().showAllMessages(false);
    };

    /*objects to post*/
    self.branchObj = function() {
        return {
            Id: self.id(),
            Name: self.name(),
            DateOfCreation: self.dateOfCreation(),
            DateOfClosing: self.dateOfClosing(),
            Address: self.address(),
            Remarks: self.remarks(),
            Status: (self.hasClosingDate()) ? null : self.status()
        };
    };

    self.setClosedDateTime = function() {
        self.hasClosingDate(true);
        self.dateOfClosing(currentDate());
    };
    self.removeClosedDateTime = function() {
        self.hasClosingDate(false);
        self.dateOfClosing(null);
    };
    self.loadBranch = function() {
        jax.getJson(
            '/Branch/Get/' + self.id(),
            function(data) {
                self.name(data.Name);
                self.dateOfCreation(clientDate(data.DateOfCreation));
                if (data.DateOfClosing != null) {
                    self.hasClosingDate(true);
                    self.dateOfClosing(clientDate(data.DateOfClosing));
                } else {
                    self.removeClosedDateTime();
                }
                self.address(data.Address);
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/Branch/TryToUpdate',
            self.branchObj(),
            function(data) {
                ToastSuccess('Branch updated successfully');
                self.loadBranch();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function() {
        self.loadBranch();
    };
    self.init = function() {
        self.id($('#txtBranchId').val());
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.loadBranch();

        activeParentMenu($('li.sub a[href="/Branch"]'));
    };
}

$(document).ready(function() {

    var vm = new BranchViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowCreationDate').click(function() {
        $('#btnDateOfCreation').focus();
    });
    $('#btnResetCreationDate').click(function() {
        vm.dateOfCreation(currentDate());
    });

    $('#btnShowClosingDate').click(function() {
        $('#btnDateOfClosing').focus();
    });
    $('#btnResetClosingDate').click(function() {
        vm.dateOfClosing(currentDate());
    });
});