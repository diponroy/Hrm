﻿/*Validations*/
ko.validation.rules['candidateEmailUsed'] = {
    async: true,
    validator: function(val, otherVal, callBack) {
        var isUsed;
        var json = JSON.stringify({ candidateId: $('#txtCandidateId').val(), email: val });
        $.ajax({
            url: '/Candidate/IsEmailUsedExceptCandidate',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
        })
            .done(function(data) {
                callBack(data === otherVal.isUsed);
            })
            .fail(function(val2, val1, da) {
                callBack(false);
            });
    },
    message: 'This email is already in use.'
};
ko.validation.registerExtenders();

/*main view model*/

function CandidateUpdateVm() {

    var self = this;

    /*ddl*/
    self.acceptableGenders = ko.observableArray([]);
    self.acceptableMaritalStatus = ko.observableArray([]);

    /*fields*/
    self.id = ko.observable().extend({ require: true });

    /*basics info's*/
    self.firstName = ko.observable().extend({ required: true, maxLength: 50, });
    self.lastName = ko.observable().extend({ required: true, maxLength: 50, });
    self.gender = ko.observable().extend({ required: true });
    self.dateOfBirth = ko.observable(currentDate()).extend({ required: true, });
    self.email = ko.observable().extend({ required: true, email: true, maxLength: 50, candidateEmailUsed: { isUsed: false }, });
    self.contact = ko.observable().extend({ required: true, maxLength: 50, });

    /*personal info's*/
    self.fathersName = ko.observable();
    self.mothersName = ko.observable();
    self.maritalStatus = ko.observable();
    self.nationality = ko.observable();
    self.nationalId = ko.observable();
    self.passportNo = ko.observable();
    self.religion = ko.observable();

    /*address info*/
    self.alternateEmail = ko.observable();
    self.homePhone = ko.observable();
    self.presentAddress = ko.observable();
    self.divisionOrState = ko.observable();
    self.city = ko.observable();
    self.permanentAddress = ko.observable();

    /*defaults*/
    self.trackNo = ko.observable();
    self.status = ko.observable();

    /*object to post*/
    self.candidateObj = function() {
        var obj = {
            /*basics info's*/
            FirstName: self.firstName(),
            LastName: self.lastName(),
            Gender: self.gender(),
            DateOfBirth: self.dateOfBirth(),
            Email: self.email(),
            ContactNo: self.contact(),
            /*personal info's*/
            FathersName: self.fathersName(),
            MothersName: self.mothersName(),
            MaritalStatus: self.maritalStatus(),
            Nationality: self.nationality(),
            NationalId: self.nationalId(),
            PassportNo: self.passportNo(),
            Religion: self.religion(),
            /*address info*/
            AlternateEmailAddress: self.alternateEmail(),
            HomePhone: self.homePhone(),
            PresentAddress: self.presentAddress(),
            DivisionOrState: self.divisionOrState(),
            City: self.city(),
            PermanentAddress: self.permanentAddress(),
            /*defaults*/
            Id: self.id(),
            TrackNo: self.trackNo(),
            Status: self.status(),
    };

    return {
        entity: obj
    };
}

;

/*errors*/
self.errors = ko.validation.group(self);
self.hasErrors = function() { return (self.errors().length > 0) ? true : false; };
self.showErrors = function() { self.errors.showAllMessages(); };
self.removeErrors = function() { self.errors.showAllMessages(false); };


/*datepicker*/
self.isDatepickerFocused = ko.observable(false);

self.showDatepicker = function() {
    self.isDatepickerFocused(true);
};

self.resetDatepicker = function() {
    self.dateOfBirth(currentDate());
};

/*server gets*/
self.load = function() {
    jax.getJsonBlock(
        '/Candidate/Get/' + self.id(),
        function(data) {
            self.firstName(data.FirstName);
            self.lastName(data.LastName);
            self.gender(data.Gender);
            self.dateOfBirth(clientDate(data.DateOfBirth));
            self.email(data.Email);
            self.contact(data.ContactNo);
            /*personal info's*/
            self.fathersName(data.FathersName);
            self.mothersName(data.MothersName);
            self.maritalStatus(data.MaritalStatus);
            self.nationality(data.Nationality);
            self.nationalId(data.NationalId);
            self.passportNo(data.PassportNo);
            self.religion(data.Religion);
            /*address info*/
            self.alternateEmail(data.AlternateEmailAddress);
            self.homePhone(data.HomePhone);
            self.presentAddress(data.PresentAddress);
            self.divisionOrState(data.DivisionOrState);
            self.city(data.City);
            self.permanentAddress(data.PermanentAddress);
            /*defaults*/
            self.trackNo(data.TrackNo);
            self.status(data.Status);
            
            self.removeErrors();
        },
        function(qXhr, textStatus, error) {
            ToastError(error);
        }
    );
};

/*posts to server*/
self.update = function() {
    if (self.hasErrors()) {
        self.showErrors();
        return;
    }
    jax.postJsonBlock(
        '/Candidate/TryToUpdate',
        self.candidateObj(),
        function(data) {
            ToastSuccess('Information updated successfully');
            self.reset();
        },
        function(qXhr, textStatus, error) {
            ToastError(error);
            self.showErrors();
        }
    );
};

/*initializations and resets*/
self.reset = function() {
    self.load();
};

self.init = function() {
    self.id($('#txtCandidateId').val());
    var genders = [
        { text: 'Male', value: 'Male' },
        { text: 'Female', value: 'Female' }
    ];
    self.acceptableGenders(genders);

    var maritialStatuses = [
        { text: 'Married', value: 'Married' },
        { text: 'Unmarried', value: 'Unmarried' }
    ];
    self.acceptableMaritalStatus(maritialStatuses);
    self.load();
    
    activeParentMenu($('li.sub a[href="/Candidate"]'));
};
}

$(document).ready(function() {
    var vm = new CandidateUpdateVm();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});