﻿/*Validations*/
ko.validation.rules['candidateEmailUsed'] = {
    async: true,
    validator: function (val, otherVal, callBack) {
        var isUsed;
        var json = JSON.stringify({ email: val });
        $.ajax({
            url: '/CandidateBasic/IsEmailUsed',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
        })
        .done(function (data) {
            callBack(data === otherVal.isUsed);
        })
        .fail(function() {
            callBack(false);
        });
    },
    message: 'This email is already in use.'
};
ko.validation.registerExtenders();

/*main view model*/
function CandidateVm() {

    var self = this;
   

    /*ddl*/
    self.acceptableGenders = ko.observableArray([]);


    /*fields*/
    self.firstName = ko.observable().extend({
        required: true,
        maxLength: 50,
    });
    self.lastName = ko.observable().extend({
        required: true,
        maxLength: 50,
    });
    self.gender = ko.observable().extend({
        required: true,
    });
    self.dateOfBirth = ko.observable(currentDate()).extend({
        required: true,
    });
    self.email = ko.observable().extend({
        required: true,
        email: true,
        maxLength: 50,
        candidateEmailUsed: {isUsed: false},
    });
    self.contact = ko.observable().extend({
        required: true,
        maxLength: 50,
    });

    /*object to post*/
    self.candidateObj = function () {
        var obj = {
            FirstName: self.firstName(),
            LastName: self.lastName(),
            Gender: self.gender(),
            DateOfBirth: self.dateOfBirth(),
            Email: self.email(),
            ContactNo: self.contact()
        };

        return {
            candidate: obj
        };
    };

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    /*posts to server*/
    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateBasic/TryToCreateBasic',
            self.candidateObj(),
            function (data) {
                ToastSuccess('Candidate created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );

    };

    /*datepicker*/
    self.isDatepickerFocused = ko.observable(false);

    self.showDatepicker = function () {
        self.isDatepickerFocused(true);
    };

    self.resetDatepicker = function () {
        self.dateOfBirth(currentDate());
    };


    /*initializations and resets*/
    self.reset = function () {
        self.firstName('');
        self.lastName('');
        self.gender('');
        self.dateOfBirth(currentDate());
        self.email('');
        self.contact('');

        self.removeErrors();
    };

    self.init = function () {
        var genders = [
            { text: 'Male', value: 'Male' },
            { text: 'Female', value: 'Female' }
        ];
        self.acceptableGenders(genders);
    };
}

$(document).ready(function () {
    var vm = new CandidateVm();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});