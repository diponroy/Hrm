﻿function JobNoticeListVm() {

    var self = this;
    self.notices = ko.observableArray([]);

    /*server Posts*/
    self.load = function() {
        self.notices([]);
        jax.getJsonBlock(
            '/CandidateDashboard/GetNoticesForCandidate',
            function(data) {
                var arr = new Array();
                $.each(data, function(index, notice) {
                    arr.push({
                        id: notice.Id,
                        trackNo: notice.TrackNo,
                        type: notice.Type,
                        title: notice.Title,
                        description: notice.Description,
                        durationFromDate: moment(notice.DurationFromDate).format('MMMM DD YYYY'),
                        durationToDate: moment(notice.DurationToDate).format('MMMM DD YYYY'),
                        status: notice.Status,
                        statusString: notice.notice
                    });
                });
                self.notices(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.apply = function(item) {
        jax.postJsonBlock(
            '/CandidateDashboard/ApplyForNotice',
            { id: item.id },
            function(data) {
                ToastSuccess('Application submited successfully.');
                self.notices.remove(item);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.confirmToApply = function (item) {
        bootbox.confirm("Are you sure, you want to apply for the position ?, make sure you have updated cv and click ok.", function (result) {
            if (result === true) {
                self.apply(item);
            }
        });
    };

    self.showDetail = function(item) {
        alert(item.id);
    };
    
    self.init = function() {
        self.load();
    };
}

$(document).ready(function() {
    var vm = new JobNoticeListVm();
    ko.applyBindings(vm);
    vm.init();
});