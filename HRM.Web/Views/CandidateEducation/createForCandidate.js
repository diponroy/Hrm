﻿function EdutaionCreateVm() {
    var self = this;
    
    /*Properties*/
    self.candidateId = ko.observable();
    
    //self.candidateId = ko.observable().extend({
    //    required: true,
    //});
    
    self.title = ko.observable().extend({
        required: true,
    });
    self.institution = ko.observable().extend({
        required: true,
    });
    self.fromDate = ko.observable(currentDate()).extend({
        required: true,
    });
    self.toDate = ko.observable(currentDate()).extend({
        required: true,
    });
    self.major = ko.observable().extend({
        required: true,
    });
    self.result = ko.observable().extend({
        required: true,
    });
    
    /*object to post*/
    self.candidateObj = function () {
        var obj = {
            CandidateId: self.candidateId(),
            Title: self.title(),
            Institution: self.institution(),
            DurationFromDate: self.fromDate(),
            DurationToDate: self.toDate(),
            Major: self.major(),
            Result: self.result()
        };

        return {
            entity: obj
        };
    };
    


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    /*posts to server*/
    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateEducation/TryToCreate',
            self.candidateObj(),
            function (data) {
                ToastSuccess('Education created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );

    };


    /*Date picker*/
    /*from date*/
    self.isFromDateFocused = ko.observable(false);

    self.showFromDatepicker = function () {
        self.isFromDateFocused(true);
    };

    self.resetFromDatepicker = function () {
        self.fromDate(currentDate());
    };

    /*to date*/
    self.isToDateFocused = ko.observable(false);

    self.showToDatepicker = function () {
        self.isToDateFocused(true);
    };

    self.resetToDatepicker = function () {
        self.toDate(currentDate());
    };

    /*initializations and resets*/
    self.reset = function () {
        self.title('');
        self.institution('');
        self.major('');
        self.result('');
        self.fromDate(currentDate());
        self.toDate(currentDate());

        self.removeErrors();
    };
    self.init = function () {
        self.candidateId($('#txtCandidateId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/CandidateEducation"]'));
    };
}

$(document).ready(function() {
    var vm = new EdutaionCreateVm();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});