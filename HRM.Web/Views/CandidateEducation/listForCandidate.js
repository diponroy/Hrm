﻿function EducationListVm() {

    var self = this;
    self.candidateId = ko.observable();
    self.educations = ko.observableArray([]);

    self.create = function() {
        window.location = '/CandidateEducation/CreateForCandidate/' + self.candidateId();
    };
    self.getEducations = function () {
        self.educations([]);
        var obj = {
            Id: self.candidateId(),
        };
        jax.postJsonBlock(
            '/CandidateEducation/GetForCandidate',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var education = data[index];
                    arr.push({
                        id: education.Id,
                        candidateId: education.CandidateId,
                        title: education.Title,
                        institution: education.Institution,
                        durationFromDate: moment(education.DurationFromDate).format('MMMM DD YYYY'),
                        durationToDate: moment(education.DurationToDate).format('MMMM DD YYYY'),
                        major: education.Major,
                        result: education.Result,
                        status: education.Status
                    });
                });
                self.educations(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.showToUpdate = function (item) {
        window.location = '/CandidateEducation/Update/' + item.id;
    };

    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/CandidateEducation/TryToDelete',
            obj,
            function (data) {
                ToastSuccess("Education deleted successfully");
                self.getEducations();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete the education ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    self.init = function () {
        self.candidateId($('#txtCandidateId').val());
        self.getEducations();
        
        activeParentMenu($('li.sub a[href="/CandidateEducation"]'));
    };
}

$(document).ready(function () {
    var vm = new EducationListVm();
    ko.applyBindings(vm);
    vm.init();
});