﻿function EdutaionCreateVm() {
    var self = this;

    /*Properties*/
    self.id = ko.observable().extend({
        required: true,
    });
    self.candidateId = ko.observable().extend({
        required: true,
    });
    self.title = ko.observable().extend({
        required: true,
    });
    self.institution = ko.observable().extend({
        required: true,
    });
    self.fromDate = ko.observable(currentDate()).extend({
        required: true,
    });
    self.toDate = ko.observable(currentDate()).extend({
        required: true,
    });
    self.major = ko.observable().extend({
        required: true,
    });
    self.result = ko.observable().extend({
        required: true,
    });
    self.status = ko.observable().extend({        
        required: true
    });

    /*object to post*/
    self.candidateObj = function() {
        var obj = {
            Id: self.id(),
            CandidateId: self.candidateId(),
            Title: self.title(),
            Institution: self.institution(),
            DurationFromDate: self.fromDate(),
            DurationToDate: self.toDate(),
            Major: self.major(),
            Result: self.result(),
            Status: self.status()
        };

        return {
            entity: obj
        };
    };


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function() {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function() {
        self.errors.showAllMessages();
    };
    self.removeErrors = function() {
        self.errors.showAllMessages(false);
    };


    self.get = function() {
        jax.getJsonBlock(
            '/CandidateEducation/Get/' + self.id(),
            function(data) {
                self.candidateId(data.CandidateId);
                self.title(data.Title);
                self.institution(data.Institution);
                self.fromDate(clientDate(data.DurationFromDate));
                self.toDate(clientDate(data.DurationToDate));
                self.major(data.Major);
                self.result(data.Result);
                self.status(data.Status);

                self.removeErrors();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*posts to server*/
    self.update = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateEducation/TryToUpdate',
            self.candidateObj(),
            function(data) {
                ToastSuccess('Education updated successfully');
                self.reset();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    /*Date picker*/
    /*from date*/
    self.isFromDateFocused = ko.observable(false);

    self.showFromDatepicker = function() {
        self.isFromDateFocused(true);
    };

    self.resetFromDatepicker = function() {
        self.fromDate(currentDate());
    };

    /*to date*/
    self.isToDateFocused = ko.observable(false);

    self.showToDatepicker = function() {
        self.isToDateFocused(true);
    };

    self.resetToDatepicker = function() {
        self.toDate(currentDate());
    };

    /*initializations and resets*/
    self.reset = function() {
        self.get();
    };
    self.init = function () {
        self.id($('#txtCandidateEducationId').val());
        self.get();
        
        activeParentMenu($('li.sub a[href="/CandidateEducation"]'));
};
}

$(document).ready(function() {
    var vm = new EdutaionCreateVm();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});