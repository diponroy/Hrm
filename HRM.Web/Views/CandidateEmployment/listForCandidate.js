﻿function EmploymentListVm() {

    var self = this;
    self.candidateId = ko.observable();
    self.employments = ko.observableArray([]);

    /*local actions*/
    self.create = function() {
        window.location = '/CandidateEmployment/CreateForCandidate/' + self.candidateId();
    };

    self.showToUpdate = function (item) {
        window.location = '/CandidateEmployment/Update/' + item.id;
    };

    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/CandidateEmployment/TryToDelete',
            obj,
            function (data) {
                ToastSuccess("Employment deleted successfully");
                self.loadEmployments();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete the employment ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };


    /*server gets*/
    self.loadEmployments = function () {
        self.employments([]);
        var obj = {
            Id: self.candidateId(),
        };
        jax.postJsonBlock(
            '/CandidateEmployment/GetForCandidate',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var employment = data[index];                    
                    arr.push({
                        id: employment.Id,
                        candidateId: employment.CandidateId,
                        institution: employment.Institution,
                        address: employment.Address,
                        designation: employment.Designation,
                        responsibility: employment.Responsibility,
                        durationFromDate:moment(employment.DurationFromDate).format('MMMM DD YYYY'),
                        durationToDate: moment(employment.DurationToDate).format('MMMM DD YYYY'),
                        status: employment.Status,
                        statusString: employment.StatusString
                    });
                });
                self.employments(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initializers*/
    self.init = function () {
        self.candidateId($('#txtCandidateId').val());
        self.loadEmployments();
        
        activeParentMenu($('li.sub a[href="/CandidateEmployment"]'));
    };
}

$(document).ready(function () {
    var vm = new EmploymentListVm();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});