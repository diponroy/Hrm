﻿function EmploymentUpdateVm() {
    var self = this;

    /*Properties*/
    self.id = ko.observable().extend({
        required: true,
    });
    self.candidateId = ko.observable().extend({
        required: true,
    });
    self.institution = ko.observable().extend({
        required: true,
    });
    self.fromDate = ko.observable(currentDate()).extend({
        required: true,
    });
    self.toDate = ko.observable(currentDate()).extend({
        required: true,
    });
    self.address = ko.observable();
    self.designation = ko.observable().extend({
        required: true,
    });
    self.responsibility = ko.observable();
    self.status = ko.observable().extend({
        required: true
    });

    /*object to post*/
    self.candidateObj = function () {
        var obj = {
            Id: self.id(),
            CandidateId: self.candidateId(),
            Institution: self.institution(),
            DurationFromDate: self.fromDate(),
            DurationToDate: self.toDate(),
            Designation: self.designation(),
            Responsibility: self.responsibility(),
            Address: self.address(),
            Status: self.status()
        };

        return {
            entity: obj
        };
    };


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };


    self.get = function () {
        jax.getJsonBlock(
            '/CandidateEmployment/Get/' + self.id(),
            function (data) {                
                self.id(data.Id);
                self.candidateId(data.CandidateId);
                self.institution(data.Institution);
                self.address(data.Address);
                self.designation(data.Designation);
                self.responsibility(data.Responsibility);
                self.fromDate(clientDate(data.DurationFromDate));
                self.toDate(clientDate(data.DurationToDate));
                self.status(data.Status);

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*posts to server*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateEmployment/TryToUpdate',
            self.candidateObj(),
            function (data) {
                ToastSuccess('Education updated successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    /*Date picker*/
    /*from date*/
    self.isFromDateFocused = ko.observable(false);

    self.showFromDatepicker = function () {
        self.isFromDateFocused(true);
    };

    self.resetFromDatepicker = function () {
        self.fromDate(currentDate());
    };

    /*to date*/
    self.isToDateFocused = ko.observable(false);

    self.showToDatepicker = function () {
        self.isToDateFocused(true);
    };

    self.resetToDatepicker = function () {
        self.toDate(currentDate());
    };

    /*initializations and resets*/
    self.reset = function () {
        self.get();
    };
    self.init = function () {
        self.id($('#txtCandidateEducationId').val());
        self.get();

        activeParentMenu($('li.sub a[href="/CandidateEmployment"]'));
    };

}

$(document).ready(function () {
    var vm = new EmploymentUpdateVm();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});