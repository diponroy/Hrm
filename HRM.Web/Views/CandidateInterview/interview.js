﻿function InterviewListVm() {

    var self = this;
    self.notices = ko.observableArray([]);

    /*server Posts*/
    self.load = function() {
        self.notices([]);
        jax.getJsonBlock(
            '/CandidateInterview/GetInterviews',
            function(data) {
                var arr = new Array();
                $.each(data, function (index, element) {
                    var notice = element.Interview.ManpowerVacancyNotice;
                    arr.push({
                        id: element.Id,
                        noticeId: notice.Id,
                        trackNo: notice.TrackNo,
                        type: notice.Type,
                        title: notice.Title,
                        description: notice.Description,
                        durationFromDate: moment(notice.DurationFromDate).format('MMMM DD YYYY'),
                        durationToDate: moment(notice.DurationToDate).format('MMMM DD YYYY'),
                        status: notice.Status,
                        statusString: notice.notice,
                        
                        appointmentDateTime: (element.AppointmentDateTime == null) ? '--' : moment(notice.AppointmentDateTime).format('MMMM DD YYYY, dddd - hh:mm a'),
                    });
                });
                self.notices(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
  
    self.init = function() {
        self.load();
    };
}

$(document).ready(function() {
    var vm = new InterviewListVm();
    ko.applyBindings(vm);
    vm.init();
});