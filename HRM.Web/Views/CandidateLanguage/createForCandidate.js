﻿//self.effeciencyLeve = ko.observableArray([]);


function LanguageCreateVm() {

    var self = this;

    /*ddls*/
    self.effeciencyLeve = ko.observableArray([]);


    /*Properties*/
    self.candidateId = ko.observable().extend({
        required: true,
    });
    self.languageName = ko.observable().extend({
        required: true,
    });
    self.reading = ko.observable().extend({
        required: true,
    });
    self.writing = ko.observable().extend({
        required: true,
    });
    self.speaking = ko.observable().extend({
        required: true,
    });
    self.listening = ko.observable().extend({
        required: true,
    });

    /*object to post*/
    self.candidateObj = function () {
        var obj = {
            CandidateId: self.candidateId(),
            LanguageName: self.languageName(),
            Reading: self.reading(),
            Writing: self.writing(),
            Speaking: self.speaking(),
            Listening: self.listening(),
        };

        return {
            entity: obj
        };
    };

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    /*posts to server*/
    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateLanguage/TryToCreate',
            self.candidateObj(),
            function (data) {
                ToastSuccess('Language created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );

    };

    /*initializers and resets*/
    self.reset = function () {
        self.languageName('');
        self.reading(''),
        self.writing(''),
        self.speaking(''),
        self.listening(''),
        
        self.removeErrors();
    };
    self.init = function () {
        var levels = [
            { text: 'Poor' },
            { text: 'Good' },
            { text: 'Satisfactory' },
            { text: 'Excellent' }
        ];
        self.effeciencyLeve(levels);
        
        self.candidateId($('#txtCandidateId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/CandidateLanguage"]'));
    };
}


$(document).ready(function () {
    var vm = new LanguageCreateVm();
    ko.applyBindings(vm);
    vm.init();
});