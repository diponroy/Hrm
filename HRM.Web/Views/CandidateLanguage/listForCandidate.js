﻿function LanguageListVm() {

    var self = this;

    self.candidateId = ko.observable();
    self.languages = ko.observableArray([]);

    self.create = function () {
        window.location = '/CandidateLanguage/CreateForCandidate/' + self.candidateId();
    };

    self.showToUpdate = function (item) {
        window.location = '/CandidateLanguage/Update/' + item.id;
    };

    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete the Language ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    /*server Deletes*/
    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/CandidateLanguage/TryToDelete',
            obj,
            function (data) {
                self.load();
                ToastSuccess("Language deleted successfully");
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*server Posts*/
    self.load = function () {
        self.languages([]);
        var obj = {
            Id: self.candidateId(),
        };
        jax.postJsonBlock(
            '/CandidateLanguage/GetForCandidate',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index, language) {
                    arr.push({
                        id: language.Id,
                        candidateId: language.CandidateId,
                        languageName: language.LanguageName,
                        reading: language.Reading,
                        writing: language.Writing,
                        speaking: language.Speaking,
                        listening: language.Listening,
                    });
                });
                self.languages(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initializations and resets*/
    self.init = function () {
        self.candidateId($('#txtCandidateId').val());       
        self.load();
        
        activeParentMenu($('li.sub a[href="/CandidateLanguage"]'));
    };
}

$(document).ready(function () {
    var vm = new LanguageListVm();
    ko.applyBindings(vm);
    vm.init();
});