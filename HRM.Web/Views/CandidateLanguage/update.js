﻿function LanguageUpdateVm() {

    var self = this;

    /*ddls*/
    self.effeciencyLeve = ko.observableArray([]);


    /*Properties*/
    self.id = ko.observable().extend({
        required: true,
    });
    self.candidateId = ko.observable().extend({
        required: true,
    });
    self.languageName = ko.observable().extend({
        required: true,
    });
    self.reading = ko.observable().extend({
        required: true,
    });
    self.writing = ko.observable().extend({
        required: true,
    });
    self.speaking = ko.observable().extend({
        required: true,
    });
    self.listening = ko.observable().extend({
        required: true,
    });
    self.status = ko.observable().extend({
        required: true,
    });

    /*object to post*/
    self.candidateObj = function () {
        var obj = {
            Id: self.id(),
            CandidateId: self.candidateId(),
            LanguageName: self.languageName(),
            Reading: self.reading(),
            Writing: self.writing(),
            Speaking: self.speaking(),
            Listening: self.listening(),
            Status: self.status()
        };

        return {
            entity: obj
        };
    };

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    /*server gets*/
    self.load = function () {
        jax.getJsonBlock(
            '/CandidateLanguage/Get/' + self.id(),
            function (data) {              
                self.candidateId(data.CandidateId);
                self.languageName(data.LanguageName);
                self.reading(data.Reading);
                self.writing(data.Writing);
                self.speaking(data.Speaking);
                self.listening(data.Listening);
                self.status(data.Status);

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*server posts*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateLanguage/TryToUpdate',
            self.candidateObj(),
            function (data) {
                ToastSuccess('Language updated successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initializers and resets*/
    self.reset = function () {        
        self.load();
    };
    self.init = function () {
        var levels = [
            { text: 'Poor' },
            { text: 'Good' },
            { text: 'Satisfactory' },
            { text: 'Excellent' }
        ];
        self.effeciencyLeve(levels);

        self.id($('#txtCandidateLanguageId').val());
        self.load();
        
        activeParentMenu($('li.sub a[href="/CandidateLanguage"]'));
    };
}


$(document).ready(function () {
    var vm = new LanguageUpdateVm();
    ko.applyBindings(vm);
    vm.init();
});