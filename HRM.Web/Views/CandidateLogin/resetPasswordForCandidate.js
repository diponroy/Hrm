﻿
function EmployeeViewModel() {
    
    /*fields*/
    var self = this;
    self.candidateId = ko.observable().extend({
        required: true,
    });
    self.password = ko.observable().extend({
        required: true,
        maxLength: 50,
    });
    self.confirmedPassword = ko.observable().extend({
        required: true,
        maxLength: 50,
    });

    self.postObject = function() {
        var obj = {
            CandidateId: self.candidateId(),
            Password: self.password()
        };
        return {
            entity: obj
        };
    };
    
    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };
    

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateLogin/ResetPassword',
            self.postObject(),
            function(data) {
                ToastSuccess('Password updated successfully');
                self.reset();
                window.location = '/CandidateLogin/Index';
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*resets*/
    self.reset = function () {
        self.password('');
        self.confirmedPassword('');
        self.removeErrors();
    };
    self.init = function () {
        self.candidateId($('#txtCandidateId').val());
        self.reset();
    };
}

$(document).ready(function() {
    var vm = new EmployeeViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});