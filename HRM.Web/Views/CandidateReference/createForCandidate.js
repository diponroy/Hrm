﻿function ReferenceCreateVm() {

    var self = this;

    /*Properties*/
    self.candidateId = ko.observable();
    self.referrer = ko.observable().extend({
        required: true,
    });
    self.designation = ko.observable();
    self.companyOrOrg = ko.observable();
    self.contactNo = ko.observable().extend({
        required: true,
    });
    self.email = ko.observable().extend({
        required: true,
        email: true,
    });
    self.address = ko.observable();
    self.relation = ko.observable();

    /*object to post*/
    self.candidateObj = function() {
        var obj = {
            CandidateId: self.candidateId(),
            Referrer: self.referrer(),
            Designation: self.designation(),
            CompanyOrOrg: self.companyOrOrg(),
            ContactNo: self.contactNo(),
            Email: self.email(),
            Address: self.address(),
            Relation: self.relation()
        };

        return {
            entity: obj
        };
    };

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function() {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function() {
        self.errors.showAllMessages();
    };
    self.removeErrors = function() {
        self.errors.showAllMessages(false);
    };

    /*posts to server*/
    self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateReference/TryToCreate',
            self.candidateObj(),
            function(data) {
                ToastSuccess('Reference created successfully');
                self.reset();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );

    };
   
    /*initializers and resets*/
    self.reset = function () {
        self.referrer('');
        self.designation('');
        self.companyOrOrg('');
        self.contactNo('');
        self.email('');
        self.address('');
        self.relation('');
        self.removeErrors('');
    };
    self.init = function() {
        self.candidateId($('#txtCandidateId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/CandidateReference"]'));
    };
}


$(document).ready(function() {
    var vm = new ReferenceCreateVm();
    ko.applyBindings(vm);
    vm.init();
});