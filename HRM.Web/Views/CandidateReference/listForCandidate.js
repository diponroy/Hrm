﻿function ReferenceListVm() {

    var self = this;

    self.candidateId = ko.observable();
    self.references = ko.observableArray([]);

    self.create = function() {
        window.location = '/CandidateReference/CreateForCandidate/' + self.candidateId();
    };
    
    self.showToUpdate = function (item) {
        window.location = '/CandidateReference/Update/' + item.id;
    };
    
    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete the reference ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    /*server Deletes*/
    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/CandidateReference/TryToDelete',
            obj,
            function (data) {
                ToastSuccess("Reference deleted successfully");
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    /*server Posts*/
    self.load = function() {
        self.references([]);
        var obj = {
            Id: self.candidateId(),
        };
        jax.postJsonBlock(
            '/CandidateReference/GetForCandidate',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index, reference) {
                    arr.push({
                        id: reference.Id,
                        candidateId: reference.CandidateId,
                        referrer: reference.Referrer,
                        designation: reference.Designation,
                        companyOrOrg: reference.CompanyOrOrg,
                        contactNo: reference.ContactNo,
                        email: reference.Email,
                        address: reference.Address,
                        relation: reference.Relation,
                        status: reference.Status,
                        statusString: reference.StatusString
                    });
                });
                self.references(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    /*initializations and resets*/
    self.init = function () {
        self.candidateId($('#txtCandidateId').val());
        self.load();
        
        activeParentMenu($('li.sub a[href="/CandidateReference"]'));
    };
}

$(document).ready(function() {
    var vm = new ReferenceListVm();
    ko.applyBindings(vm);
    vm.init();
});