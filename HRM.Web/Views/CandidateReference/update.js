﻿function ReferenceUpdateVm() {

    var self = this;

    /*Properties*/
    self.id = ko.observable().extend({
        required: true,
    });
    self.candidateId = ko.observable().extend({
        required: true,
    });
    self.referrer = ko.observable().extend({
        required: true,
    });
    self.designation = ko.observable();
    self.companyOrOrg = ko.observable();
    self.contactNo = ko.observable().extend({
        required: true,
    });
    self.email = ko.observable().extend({
        required: true,
        email: true,
    });
    self.address = ko.observable();
    self.relation = ko.observable();
    self.status = ko.observable();


    /*object to post*/
    self.candidateObj = function () {
        var obj = {
            Id: self.id(),
            CandidateId: self.candidateId(),
            Referrer: self.referrer(),
            Designation: self.designation(),
            CompanyOrOrg: self.companyOrOrg(),
            ContactNo: self.contactNo(),
            Email: self.email(),
            Address: self.address(),
            Relation: self.relation(),
            Status: self.status()
        };

        return {
            entity: obj
        };
    };

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    /*server gets*/
    self.load = function () {
        jax.getJsonBlock(
            '/CandidateReference/Get/' + self.id(),
            function (data) {
                self.candidateId(data.CandidateId);
                self.referrer(data.Referrer);
                self.designation(data.Designation);
                self.companyOrOrg(data.CompanyOrOrg);
                self.contactNo(data.ContactNo);
                self.email(data.Email);
                self.address(data.Address);
                self.relation(data.Relation);
                self.status(data.Status);

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };
    
    /*server posts*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateReference/TryToUpdate',
            self.candidateObj(),
            function (data) {
                ToastSuccess('Reference updated successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initializers and resets*/
    self.reset = function () {
        self.load();
    };
    self.init = function () {
        self.id($('#txtCandidateReferenceId').val());
        self.load();
        
        activeParentMenu($('li.sub a[href="/CandidateReference"]'));
    };
}


$(document).ready(function () {
    var vm = new ReferenceUpdateVm();
    ko.applyBindings(vm);
    vm.init();
});