﻿function SkillCreateVm() {

    var self = this;
    
    /*ddls*/
    self.acceptableTypes = ko.observable([]);
    
    /*properties*/
    self.candidateId = ko.observable().extend({required: true});
    self.title = ko.observable().extend({ required: true });
    self.description = ko.observable();
    self.type = ko.observable().extend({ required: true });

    /*objects to post*/
    self.objectToPost = function () {
        var obj = {
            CandidateId: self.candidateId(),
            Title: self.title(),
            Description: self.description(),
            Type: self.type(),
        };

        return {
            entity: obj
        };
    };

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*server posts*/
    self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateSkill/TryToCreate',
            self.objectToPost(),
            function (data) {
                ToastSuccess('Skill created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*resets and initializations*/
    self.reset = function() {
        self.title('');
        self.description('');
        self.type('');

        self.removeErrors();
    };
    self.init = function () {
        self.candidateId($('#txtCandidateId').val());
        var types = [
            { name: 'Professional' },
            { name: 'Extracurricular' },
            { name: 'Interest' }
        ];
        self.acceptableTypes(types);
        
        self.reset();
        
        activeParentMenu($('li.sub a[href="/CandidateSkill"]'));
    };
}

$(document).ready(function() {
    var vm = new SkillCreateVm();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});
