﻿function SkillListVm() {

    var self = this;
    
    self.candidateId = ko.observable();
    self.allSkills = ko.observableArray();

    self.professionalSkills = ko.computed(function () {
        return $.grep(self.allSkills(), function(element) {
            return element.type.toLowerCase() === 'professional';
        });
    }, this);
    
    self.extraCurricularSkills = ko.computed(function () {
        return $.grep(self.allSkills(), function (element) {
            return element.type.toLowerCase() === 'extra curricular';
        });
    }, this);
    
    self.interestSkills = ko.computed(function () {
        return $.grep(self.allSkills(), function (element) {
            return element.type.toLowerCase() === 'interest';
        });
    }, this);


    self.create = function () {
        window.location = '/CandidateSkill/CreateForCandidate/' + self.candidateId();
    };

    self.showToUpdate = function (item) {
        window.location = '/CandidateSkill/Update/' + item.id;
    };

    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/CandidateSkill/TryToDelete',
            obj,
            function (data) {
                ToastSuccess("Skill deleted successfully");
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete the skill ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    self.load = function () {
        self.allSkills([]);
        var obj = {
            Id: self.candidateId(),
        };
        jax.postJsonBlock(
            '/CandidateSkill/GetForCandidate',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index, skill) {
                    arr.push({
                        id: skill.Id,
                        candidateId: skill.CandidateId,
                        title: skill.Title,
                        description: skill.Description,
                        type: skill.Type,
                        status: skill.Status,
                        statusString: skill.StatusString
                    });
                });
                self.allSkills(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );


    };
    
    /*initializations*/
    self.init = function () {
        self.candidateId($('#txtCandidateId').val());
        self.load();
        
        activeParentMenu($('li.sub a[href="/CandidateSkill"]'));
    };
}

$(document).ready(function() {
    var vm = new SkillListVm();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});