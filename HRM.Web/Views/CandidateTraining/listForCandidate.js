﻿function TrainingListVm() {

    var self = this;
    self.candidateId = ko.observable();
    self.trainings = ko.observableArray([]);

    self.create = function () {
        window.location = '/CandidateTraining/CreateForCandidate/' + self.candidateId();
    };

    self.showToUpdate = function (item) {
        window.location = '/CandidateTraining/Update/' + item.id;
    };

    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/CandidateTraining/TryToDelete',
            obj,
            function (data) {
                ToastSuccess("Training deleted successfully");
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete the training ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    self.load = function () {
        self.trainings([]);
        var obj = {
            Id: self.candidateId(),
        };
        jax.postJsonBlock(
            '/CandidateTraining/GetForCandidate',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var training = data[index];
                    arr.push({
                        id: training.Id,
                        candidateId: training.CandidateId,
                        title: training.Title,
                        institution: training.Institution,
                        address: training.Address,
                        durationFromDate: moment(training.DurationFromDate).format('MMMM DD YYYY'),
                        durationToDate: moment(training.DurationToDate).format('MMMM DD YYYY'),
                        achievement: training.Achievement,
                        status: training.Status,
                        statusString: training.StatusString
                    });
                });
                self.trainings(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );


    };

    self.init = function () {
        self.candidateId($('#txtCandidateId').val());
        self.load();
        
        activeParentMenu($('li.sub a[href="/CandidateTraining"]'));
    };
}


$(document).ready(function() {
    var vm = new TrainingListVm();
    ko.applyBindings(vm);
    vm.init();
});