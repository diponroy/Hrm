﻿function EmploymentUpdateVm() {
    var self = this;

    /*Properties*/
    self.id = ko.observable().extend({
        required: true,
    });
    self.candidateId = ko.observable().extend({
        required: true,
    });
    self.institution = ko.observable().extend({
        required: true,
    });
    self.address = ko.observable();

    self.title = ko.observable().extend({
        required: true,
    });
    self.fromDate = ko.observable(currentDate()).extend({
        required: true,
    });
    self.toDate = ko.observable(currentDate()).extend({
        required: true,
    });
    self.achievement = ko.observable().extend({
        required: true,
    });
    self.status = ko.observable().extend({
        required: true,
    });

    /*object to post*/
    self.candidateObj = function () {
        var obj = {
            Id: self.id(),
            CandidateId: self.candidateId(),
            Institution: self.institution(),
            DurationFromDate: self.fromDate(),
            DurationToDate: self.toDate(),
            Title: self.title(),
            Achievement: self.achievement(),
            Address: self.address(),
            Status: self.status()
        };

        return {
            entity: obj
        };
    };


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };


    self.get = function () {
        jax.getJsonBlock(
            '/CandidateTraining/Get/' + self.id(),
            function (data) {
                self.candidateId(data.CandidateId);
                self.institution(data.Institution);
                self.fromDate(clientDate(data.DurationFromDate));
                self.toDate(clientDate(data.DurationToDate));
                self.title(data.Title);
                self.achievement(data.Achievement);
                self.address(data.Address);
                self.status(data.Status);
   
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*posts to server*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CandidateTraining/TryToUpdate',
            self.candidateObj(),
            function (data) {
                ToastSuccess('Training updated successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    /*Date picker*/
    /*from date*/
    self.isFromDateFocused = ko.observable(false);

    self.showFromDatepicker = function () {
        self.isFromDateFocused(true);
    };

    self.resetFromDatepicker = function () {
        self.fromDate(currentDate());
    };

    /*to date*/
    self.isToDateFocused = ko.observable(false);

    self.showToDatepicker = function () {
        self.isToDateFocused(true);
    };

    self.resetToDatepicker = function () {
        self.toDate(currentDate());
    };

    /*initializations and resets*/
    self.reset = function () {
        self.get();
    };
    self.init = function () {
        self.id($('#txtCandidateTrainingId').val());
        self.get();
        
        activeParentMenu($('li.sub a[href="/CandidateTraining"]'));
    };
}

$(document).ready(function () {
    var vm = new EmploymentUpdateVm();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});