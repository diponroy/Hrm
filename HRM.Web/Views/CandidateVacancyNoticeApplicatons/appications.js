﻿function JobApplicationListVm() {

    var self = this;
    self.notices = ko.observableArray([]);

    /*server Posts*/
    self.load = function() {
        self.notices([]);
        jax.getJsonBlock(
            '/CandidateVacancyNoticeApplicatons/GetApplications',
            function(data) {
                var arr = new Array();
                $.each(data, function (index, element) {

                    var notice = element.ManpowerVacancyNotice;
                    arr.push({
                        id: element.Id,
                        noticeId: notice.Id,
                        trackNo: notice.TrackNo,
                        type: notice.Type,
                        title: notice.Title,
                        description: notice.Description,
                        durationFromDate: moment(notice.DurationFromDate).format('MMMM DD YYYY'),
                        durationToDate: moment(notice.DurationToDate).format('MMMM DD YYYY'),
                        status: notice.Status,
                        statusString: notice.notice
                    });
                });
                self.notices(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.removeApplication = function(item) {
        jax.postJsonBlock(
            '/CandidateVacancyNoticeApplicatons/RemoveApplication',
            { id: item.id },
            function(data) {
                ToastSuccess('Application submited successfully.');
                self.notices.remove(item);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.confirmToRemoveApplication = function (item) {
        bootbox.confirm("Are you sure, you want to remove the application ?", function (result) {
            if (result === true) {
                self.removeApplication(item);
            }
        });
    };
    
    self.init = function() {
        self.load();
    };
}

$(document).ready(function() {
    var vm = new JobApplicationListVm();
    ko.applyBindings(vm);
    vm.init();
});