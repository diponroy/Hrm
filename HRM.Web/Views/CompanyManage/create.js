﻿/*Validations*/
ko.validation.rules['isCompanyNameUsed'] = {
    validator: function(val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/CompanyManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charsat=utf-8',
                data: json,
                async: false,
            })
        ).then(function(data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This name already exist.'
};
ko.validation.registerExtenders();


function CompanyViewModel() {
    var self = this;
    self.name = ko.observable('').extend({ required:true, maxLength: 50, isCompanyNameUsed: false });
    self.address = ko.observable('').extend({ maxLength: 250 });
    self.remarks = ko.observable('').extend({ maxLength: 150 });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.status = ko.observable('').extend({ required: true });

    self.allStatus = ko.observableArray([]);

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /* Local Actions */

    self.getCompanyDetail = function () {
        var companyObj = {
            Name: self.name(),
            DateOfCreation: self.dateOfCreation(),
            Address: self.address(),
            Remarks: self.remarks(),
            Status: self.status()
        };
        return {
            model: companyObj
        };
    };
    self.reset = function () {
        self.name('');
        self.dateOfCreation(currentDate());
        self.address('');
        self.remarks('');
        self.status('');

        self.removeErrors();
    };

   self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        self.saveToServer();
    };

    /* Server Actions */
    self.saveToServer = function() {
        jax.postJsonBlock(
            '/CompanyManage/Create',
            self.getCompanyDetail(),
            function() {
                ToastSuccess('Company has been created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };
    
    self.init = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();

        activeParentMenu($('li.sub a[href="/CompanyManage"]'));
    };
}

$(document).ready(function() {
    $('.datepicker').datepicker();

    var viewModel = new CompanyViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();

    $('#btnShowDatePickerCreation').click(function() {
        $('#btnDateOfCreation').focus();
    });
    $('#btnResetDatePickerCreation').click(function() {
        viewModel.dateOfCreation(currentDate());
    });
});


