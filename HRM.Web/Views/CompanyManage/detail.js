﻿function CompanyViewModel() {
    var self = this;
    self.id = ko.observable();
    self.company = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all*/
    self.getCompany = function () {
        jax.getJsonBlock(
            '/CompanyManage/Get/' + self.id(),
            function (data) {
                self.company({
                    name: data.Name,
                    address: data.Address,
                    remarks: data.Remarks,
                    dateOfCreation: clientDateStringFromDate(data.DateOfCreation),
                    dateOfClosing: (data.DateOfClosing === null) ? '--' : clientDateStringFromDate(data.DateOfClosing)
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/CompanyManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var aCompany = data[index];
                    array.push({
                        id: aCompany.Id,
                        name: aCompany.Name,
                        address: aCompany.Address,
                        remarks: aCompany.Remarks,
                        dateOfCreation: clientDateStringFromDate(aCompany.DateOfCreation),
                        dateOfClosing: (aCompany.DateOfClosing) === null ? '--' : clientDateStringFromDate(aCompany.DateOfClosing),
                        isClosed: aCompany.DateOfClosing != null,
                        status: aCompany.Status,
                        statusString: aCompany.StatusString,
                        affectedByEmployeeId: aCompany.AffectedByEmployeeId,
                        affectedByEmployeeName: aCompany.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(aCompany.affectedDateTime),
                        logStatus: aCompany.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#txtCompanyId').val());
        self.getCompany();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new CompanyViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();
});