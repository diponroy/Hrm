﻿function CompanyViewModel( ) {
    var self = this;
    self.companies = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function( ) {
        };
    self.resetFilers = function( ) {
    };

    self.getCompanyList = function(pageNo, pageSize, searchFilters) {
        self.companies([]);
        var obj = { pageNo : pageNo, pageSize : pageSize, filter : searchFilters };
        jax.postJsonBlock(
            '/CompanyManage/Find',
            obj,
            function(data) {
                var array = new Array();
                $.each(data, function(index) {
                    var aCompany = data[index];
                    array.push({
                        id : aCompany.Id,
                        name : aCompany.Name,
                        address : aCompany.Address,
                        dateOfCreation : clientDateStringFromDate(aCompany.DateOfCreation),
                        dateOfClosing : (aCompany.DateOfClosing === null) ? '--' : clientDateStringFromDate(aCompany.DateOfClosing),
                        isClosed: aCompany.DateOfClosing != null,
                        remarks: aCompany.Remarks,
                        status : aCompany.Status,
                        statusString : aCompany.StatusString
                    });
                });
                self.companies(array);
            },
            function(qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function(item) {
        window.location = '/CompanyManage/Update/' + item.id;
    };

    self.detail = function(item) {
        window.location = '/CompanyManage/Detail/' + item.id;
    };

    self.confirmDelete = function(item) {
        bootbox.confirm("Are you sure to delete the Company ?", function(result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function(id) {
        var obj = { id : id, };
        jax.postJsonBlock(
            '/CompanyManage/Delete',
            obj,
            function() {
                ToastSuccess("Information deleted successfully");
                self.getCompanyList(1, 25, self.searchFilers());
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function( ) {
        self.resetFilers();
        self.getCompanyList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/CompanyManage"]'));
    };
}

$(document).ready(function( ) {
    var viewModel = new CompanyViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});