﻿/*Validations*/
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ companyId: $('#txtCompanyId').val(), name: val });
        $.when(
            $.ajax({
                url: '/CompanyManage/NameUsedExceptItself',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This name already exist'
};
ko.validation.registerExtenders();

function CompanyViewModel( ) {
    var self = this;
    self.id = ko.observable().extend({ required: true });
    self.name = ko.observable().extend({ required: true, maxLength: 50, nameUsed: false });
    self.address = ko.observable().extend({ maxLength: 250 });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });

    self.hasClosingDate = ko.observable(false);
    self.dateOfClosing = ko.observable().extend({ required:true });

    self.allStatus = ko.observableArray([]);
    self.status = ko.observable().extend({ required : true });

    self.errorsWithOutStatus = ko.validation.group([self.name, self.dateOfCreation, self.dateOfClosing, self.address, self.remarks]);
    self.errorsWithStatus = ko.validation.group([self.name, self.dateOfCreation, self.address, self.remarks, self.status]);
    self.errors = ko.computed(function () {
        return self.hasClosingDate() ? self.errorsWithOutStatus : self.errorsWithStatus;
    }, this);
    self.hasErrors = function () {
        return ((self.errors()()).length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors().showAllMessages();
    };
    self.removeErrors = function () {
        self.errors().showAllMessages(false);
    };

    self.reset = function () {
        self.load();
        return true;
    };

    self.getUpdateDetail = function( ) {
        return {
            Id: self.id(),
            Name : self.name(),
            Address : self.address(),
            Remarks : self.remarks(),
            DateOfCreation: self.dateOfCreation(),
            DateOfClosing: self.dateOfClosing(),
            Status:(self.hasClosingDate()) ? null : self.status()
        };
     };

    self.setClosedDateTime = function( ) {
        self.hasClosingDate(true);
        self.dateOfClosing(currentDate());
    };

    self.removeClosedDateTime = function( ) {
        self.hasClosingDate(false);
        self.dateOfClosing(null);
    };

    /*get all*/
    self.load = function () {
        jax.getJson(
           '/CompanyManage/Get/' + self.id(),
            function (data) {
                self.name(data.Name);
                self.address(data.Address),
                self.remarks(data.Remarks);
                self.dateOfCreation(clientDate(data.DateOfCreation));
                if (data.DateOfClosing != null) {
                    self.hasClosingDate(true);
                    self.dateOfClosing(clientDate(data.DateOfClosing));
                } else {
                    self.removeClosedDateTime();
                }
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*Server Actions*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/CompanyManage/Update',
            self.getUpdateDetail(),
            function() {
                ToastSuccess('Information updated successfully');
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
         );
  };

    self.reset = function () {
        self.load();
        self.removeErrors();
    };

   self.init = function () {
       self.id($('#txtCompanyId').val());

       var status = [];
       status.push({ name: 'Active' });
       status.push({ name: 'Inactive' });
       self.allStatus(status);

       self.load();
       
       activeParentMenu($('li.sub a[href="/CompanyManage"]'));
    };
}

$(document).ready(function( ) {
    var viewModel = new CompanyViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();

    $('#btnShowDatePickerCreation').click(function () {
        $('#btnDateOfCreation').focus();
    });
    $('#btnResetDatePickerCreation').click(function () {
        viewModel.dateOfCreation(currentDate());
    });

    $('#btnShowDatePickerClosing').click(function () {
        $('#btnDateOfClosing').focus();
    });

    $('#btnResetDatePickerClosing').click(function () {
        viewModel.dateOfClosing(currentDate());
    });
});