﻿/*Validations*/
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/DepartmentManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Department Name is already in use'
};
ko.validation.registerExtenders();


function DepartmentViewModel() {
    var self = this;
    self.name = ko.observable().extend({ required: true, maxLength: 50, nameUsed: false });
    self.description = ko.observable().extend({ maxLength: 250 });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.status = ko.observable().extend({ required: true });

    self.allStatuses = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    self.getDepartmentDetail = function () {
        var departmentObj = {
            Name: self.name(),
            Description: self.description(),
            Remarks: self.remarks(),
            DateOfCreation: self.dateOfCreation(),
            Status: self.status()
        };

        return {
            model: departmentObj
        };
    };


    /* Actions */
    self.create = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return;
        }
        jax.postJsonBlock(
            '/DepartmentManage/Create',
            self.getDepartmentDetail(),
            function (data) {
                ToastSuccess('Department is created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.reset = function () {
        self.name('');
        self.description('');
        self.remarks('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.status('');

        self.removeErrors();
    };

    self.init = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/DepartmentManage"]'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new DepartmentViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowDatePickerCreation').click(function () {
        $('#btnDateOfCreation').focus();
    });

    $('#btnResetDatePickerCreation').click(function () {
        vm.dateOfCreation(currentDate());
    });
});