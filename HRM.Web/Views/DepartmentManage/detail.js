﻿
function DepartmentLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.department = ko.observable();
    self.logs = ko.observableArray([]);

    self.departmentLogs = ko.observableArray([]);
    

    /* Actions */

    self.getDepartment = function() {
        $.ajax({
            url: '/DepartmentManage/Get/' + self.id(),
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.department({
                    name: data.Name,
                    dateOfCreation: clientDateTimeStringFromDate(data.DateOfCreation),
                    dateOfClosing: (data.DateOfClosing === null) ? '--' : clientDateTimeStringFromDate(data.DateOfClosing),
                    description: data.Description,
                    remarks: data.Remarks
                });
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    self.getLogs = function () {
        self.logs([]);

        $.ajax({
            url: '/DepartmentManage/GetLogs/' + self.id(),
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aDepartment = data[index];
                    arr.push({
                        id : aDepartment.Id,
                        name : aDepartment.Name,
                        description : aDepartment.Description,
                        remarks : aDepartment.Remarks,
                        dateOfCreation: clientDateTimeStringFromDate(aDepartment.DateOfCreation),
                        dateOfClosing: (aDepartment.DateOfClosing === null) ? "---" : clientDateTimeStringFromDate(aDepartment.DateOfClosing),
                        isClosed : aDepartment.DateOfClosing != null,
                        status : aDepartment.Status,
                        statusString : aDepartment.StatusString,
                        affectedByEmployeeId : aDepartment.AffectedByEmployeeId,
                        affectedByName: aDepartment.AffectedByEmployee.FullName,
                        affectedDate : clientDateTimeStringFromDate(aDepartment.AffectedDateTime),
                        logStatus : aDepartment.LogStatuses                    
                    });
                });
                
                self.departmentLogs(arr);
                $.unblockUI();
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.init = function () {
        self.id($('#txtDepartmentId').val());
        self.getDepartment();
        self.getLogs();
    };
}


$(document).ready(function () {
    var vm = new DepartmentLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});