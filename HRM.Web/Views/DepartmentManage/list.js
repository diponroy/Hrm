﻿
function DepartmentListViewModel() {
    var self = this;
    self.departments = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function() {

    };
    self.resetFilers = function() {

    };

    self.getDepartments = function (pageNo, pageSize, searchFilters) {
        self.departments([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/DepartmentManage/Find',
            obj,
            function(data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aDepartment = data[index];
                    arr.push({
                        id: aDepartment.Id,
                        name: aDepartment.Name,
                        description: aDepartment.Description,
                        remarks: aDepartment.Remarks,
                        dateOfCreation: clientDateStringFromDate(aDepartment.DateOfCreation),
                        dateOfClosing: (aDepartment.DateOfClosing === null) ? '---' : clientDateStringFromDate(aDepartment.DateOfClosing),
                        isClosed: aDepartment.DateOfClosing != null,
                        status: aDepartment.Status,
                        statusString: aDepartment.StatusString
                    });
                });
                self.departments(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        self.showToEdit = function(item) {
            window.location = '/DepartmentManage/Update/' + item.id;
        };

        self.showDetail = function(item) {
            window.location = '/DepartmentManage/Detail/' + item.id;
        };

        self.delete = function (id) {
            var obj = {
                id: id,
            };
            jax.postJsonBlock(
                '/DepartmentManage/Delete',
                obj,
                function (data) {
                    ToastSuccess("Department deleted successfully");
                    self.getDepartments(1, 25, self.searchFilers());
                },
                function (qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        };

        self.confirmToDelete = function (item) {
            bootbox.confirm("Are you sure, you want to delete the Department ?", function (result) {
                if (result === true) {
                    self.delete(item.id);
                }
            });
        };
    }

    self.init = function () {
        self.resetFilers();
        self.getDepartments(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/DepartmentManage"]'));
    };
}


$(document).ready(function () {

    var vm = new DepartmentListViewModel();
    ko.applyBindings(vm);
    vm.init();
})