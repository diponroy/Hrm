﻿/*Validations*/
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({id: $('#txtDepartmentId').val(), name: val });
        $.when(
            $.ajax({
                url: '/DepartmentManage/NameUsedExceptDepartment',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Department Name is already in use'
};
ko.validation.registerExtenders();


function DepartmentViewModel() {
    var self = this;
    self.id = ko.observable('').extend({ required: true });
    self.name = ko.observable('').extend({ required: true, maxLength: 50, nameUsed: false });
    self.description = ko.observable('').extend({ maxLength: 250 });
    self.remarks = ko.observable('').extend({ maxLength: 150 });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.dateOfClosing = ko.observable().extend({ required: true });
    self.hasClosingDate = ko.observable(false);
    self.status = ko.observable().extend({ required: true });
    self.allStatuses = ko.observableArray([]);

    self.setClosedDateTime = function () {
        self.hasClosingDate(true);
        self.dateOfClosing(currentDate());
    };
    self.removeClosedDateTime = function () {
        self.hasClosingDate(false);
        self.dateOfClosing(null);
    };

    self.errorsWithOutStatus = ko.validation.group([self.name, self.dateOfCreation, self.dateOfClosing, self.address, self.remarks]);
    self.errorsWithStatus = ko.validation.group([self.name, self.dateOfCreation, self.address, self.remarks, self.status]);
    self.errors = ko.computed(function () {
        return self.hasClosingDate() ? self.errorsWithOutStatus : self.errorsWithStatus;
    }, this);
    self.hasErrors = function () {
        return ((self.errors()()).length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors().showAllMessages();
    };
    self.removeErrors = function () {
        self.errors().showAllMessages(false);
    };

    self.getDepartmentObj = function () {
        return {
            Id:self.id(),
            Name: self.name(),
            Description: self.description(),
            Remarks: self.remarks(),
            DateOfCreation: self.dateOfCreation(),
            DateOfClosing: self.dateOfClosing(),
            Status: (self.hasClosingDate()) ? null : self.status()
        };
    };


    /* Actions */
    self.load = function () {
        jax.getJson(
            '/DepartmentManage/Get/' + self.id(),
            function (data) {
                self.name(data.Name);
                self.dateOfCreation(clientDate(data.DateOfCreation));
                if (data.DateOfClosing != null) {
                    self.hasClosingDate(true);
                    self.dateOfClosing(clientDate(data.DateOfClosing));
                } else {
                    self.removeClosedDateTime();
                }
                self.description(data.Description),
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/DepartmentManage/Update',
            self.getDepartmentObj(),
            function(data) {
                ToastSuccess('Department is updated successfully');
                self.load();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.id($('#txtDepartmentId').val());
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        self.load();
        
        activeParentMenu($('li.sub a[href="/DepartmentManage"]'));
    };
}

$(document).ready(function () {
    var vm = new DepartmentViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowCreationDate').click(function () {
        $('#btnDateOfCreation').focus();
    });
    $('#btnResetCreationDate').click(function () {
        vm.dateOfCreation(currentDate());
    });

    $('#btnShowClosingDate').click(function () {
        $('#btnDateOfClosing').focus();
    });
    $('#btnResetClosingDate').click(function () {
        vm.dateOfClosing(currentDate());
    });
});