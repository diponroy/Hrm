﻿/*Validations*/
ko.validation.rules['titleUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ title: val });
        $.when(
            $.ajax({
                url: '/EmployeeAchievementManage/IsTitleUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exists.'
};
ko.validation.registerExtenders();


function EmployeeAchievementViewModel() {
    var self = this;
    self.title = ko.observable().extend({ required: true, maxLength: 50, titleUsed: false });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    
    self.employeeId = ko.observable();
    self.employeeName = ko.observable();
    
    self.employeeAppraisalId = ko.observable().extend({required: true});
    self.allAppraisal = ko.observableArray([]);
    
    self.directory = ko.observable().extend({ maxLength: 250 });
    
    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };


    /* Local Actions */
    self.employeeAchievementObj = function () {
        return {
            Title: self.title(),
            Remarks: self.remarks(),
            EmployeeId: self.employeeId(),
            EmployeeAppraisalId: self.employeeAppraisalId(),
            AttachmentDirectory: self.directory(),
            Status: self.status()
        };
    };


    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.employeeId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError("Employee Name couldn't be retrieved !!");
            }
        );
    };
    
    self.getAppraisals = function () {
        jax.getJsonBlock(
            '/EmployeeAchievementManage/GetAppraisals/' + self.employeeId(),
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({ text: obj.Title, value: obj.Id });
                });
                self.allAppraisal(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        
        jax.postJsonBlock(
            '/EmployeeAchievementManage/Create',
            self.employeeAchievementObj(),
            function () {
                ToastSuccess('Achievement information is saved successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.goMembership = function () {
        window.location = '/EmployeeMembershipManage/Create/' + self.employeeId();
    };

    /* reset */
    self.reset = function () {
        self.title('');
        self.remarks('');
        self.directory('');
        self.employeeAppraisalId('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.employeeId($('#txtEmployeeId').val());

        self.getEmployee();
        self.getAppraisals();
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeAchievementManage"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeAchievementViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();

    $('#uploadAchievementFile').click(function () {
        var formdata = new FormData();
        var fileInput = document.getElementById('fileInput');

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/EmployeeAchievementManage/Upload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                    viewModel.directory(xhr.responseText);
                    ToastSuccess("File has successfully been uploaded.");
                } else {
                    ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                }
            }
        };
    });
});


