﻿
function EmployeeAchievementLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.achievementId = ko.observable();

    self.employee = ko.observable();
    self.employeeName = ko.observable();
    self.employeeAchievements = ko.observableArray([]);


    /* Actions */

    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.achievementId(),
            null,
            function(data) {
                self.employeeName(data.FullName);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getEmployeeAchievements = function () {
        self.employeeAchievements([]);
        jax.postJsonBlock(
            '/EmployeeAchievementManage/GetAchievements/' + self.achievementId(),
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anAchievement = data[index];
                    arr.push({
                        id: anAchievement.Id,
                        title: anAchievement.Title,
                        appraisal: anAchievement.EmployeeAppraisal.EmployeeAssignedAppraisal.AppraisalIndicator.Title,
                        remarks: anAchievement.Remarks
                    });
                });
                self.employeeAchievements(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    }


    //self.getAchievementLogs = function () {
    //    self.employeeAchievementLogs([]);

    //    $.ajax({
    //        url: '/EmployeeAchievementManage/GetLogs/' + self.achievementId(),
    //        dataType: "json",
    //        type: "GET",
    //        contentType: 'application/json; charset=utf-8',
    //        async: true,
    //        processData: false,
    //        cache: false,
    //        success: function (data) {
    //            var arr = new Array();
    //            $.each(data, function (index) {
    //                var anEmployee = data[index];
    //                arr.push({
    //                    id: anEmployee.Id,
    //                    title: anEmployee.Title,
    //                    remarks: anEmployee.Remarks,
    //                    appraisal: anEmployee.EmployeeAppraisalLog.EmployeeAssignedAppraisalLog.AppraisalIndicatorLog.Title,
    //                    attachment: anEmployee.AttachmentDirectory,

    //                    status: anEmployee.Status,
    //                    statusString: anEmployee.StatusString,
    //                    affectedByName: anEmployee.AffectedByEmployeeLog.FullName,
    //                    affectedDate: clientDateTimeStringFromDate(anEmployee.AffectedDateTime),
    //                    logStatus: anEmployee.LogStatuses
    //                });
    //            });

    //            self.employeeAchievementLogs(arr);
    //            $.unblockUI();
    //        },
    //        error: function (xhr) {
    //            ToastError(xhr);
    //        }
    //    });
    //};

    self.showLog = function (item) {
        window.location = '/EmployeeAchievementManage/DetailIndividual/' + item.id;
    };

    self.init = function () {
        self.achievementId($('#txtAchievementId').val());
        self.getEmployee();
        self.getEmployeeAchievements();
    };
}


$(document).ready(function () {
    var vm = new EmployeeAchievementLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});