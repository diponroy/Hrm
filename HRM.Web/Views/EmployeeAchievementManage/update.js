﻿
function EmployeeAchievementUpdateViewModel() {
    var self = this;

    /* basic */
    self.employeeId = ko.observable(),
    self.employeeName = ko.observable(),

    /*properties*/
    self.employeeAchievements = ko.observableArray([]);


    /* Local Actions */
    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.employeeId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError("Employee Name couldn't be retrieved !!");
            }
        );
    };

    self.load = function () {
        self.employeeAchievements([]);

        jax.postJsonBlock(
            '/EmployeeAchievementManage/GetAchievements/' + self.employeeId(),
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anAchievement = data[index];
                    arr.push({
                        id: anAchievement.Id,
                        title: anAchievement.Title,
                        appraisal: anAchievement.EmployeeAppraisal.EmployeeAssignedAppraisal.AppraisalIndicator.Title,
                        remarks: anAchievement.Remarks
                    });
                });
                self.employeeAchievements(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
   
    self.showToEdit = function (item) {
        window.location = '/EmployeeAchievementManage/UpdateIndividual/' + item.id;
    };

    self.showToCreate = function () {
        window.location = '/EmployeeAchievementManage/Create/' + self.employeeId();
    };

    self.goMembership = function () {
        window.location = '/EmployeeMembershipManage/Update/' + self.employeeId();
    };

    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete this Employee Achievement ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/EmployeeAchievementManage/Delete',
            obj,
            function (data) {
                ToastSuccess("Employee Achievement deleted successfully");
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.reset = function () {
        self.load();
    };
    
    self.init = function () {
        self.employeeId($('#txtEmployeeId').val());
        self.getEmployee();      
        self.load();
        
        activeParentMenu($('li.sub a[href="/EmployeeAchievementManage"]'));
    };
}


$(document).ready(function () {
    var viewModel = new EmployeeAchievementUpdateViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();
});