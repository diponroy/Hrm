﻿
function EmployeeAchievementUpdateIndividualViewModel() {
    var self = this;

    self.id = ko.observable();
    self.title = ko.observable().extend({ required: true, maxLength: 50 });
    self.remarks = ko.observable().extend({ maxLength: 150 });

    self.employeeId = ko.observable();
    self.employeeName = ko.observable();

    self.employeeAppraisalId = ko.observable();
    self.employeeAppraisal = ko.observableArray([]);

    self.directory = ko.observable().extend({ maxLength: 250 });

    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };


    /* actions */

    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.employeeId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError("Employee Name couldn't be retrieved !!");
            }
        );
    };

    self.employeeAchievementObj = function () {
        var empAchievementObj = {
            Id: self.id(),
            Title: self.title(),
            Remarks: self.remarks(),
            EmployeeAppraisalId: self.employeeAppraisalId(),
            EmployeeId: self.employeeId(),
            Status: self.status()
        };

        return empAchievementObj;
    };

    self.load = function () {
        jax.getJsonBlock(
           '/EmployeeAchievementManage/Get/' + self.id(),
            function (data) {
                self.id(data.Id);
                self.employeeId(data.EmployeeId);

                self.getEmployee();

                self.title(data.Title);
                self.remarks(data.Remarks);
                self.employeeAppraisalId(data.EmployeeAppraisalId);
                self.employeeAppraisal(data.EmployeeAppraisal.EmployeeAssignedAppraisal.AppraisalIndicator.Title);
                self.status(data.StatusString);
                self.removeErrors();   
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeAchievementManage/Update',
            self.employeeAchievementObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
         );
    };


    /*resets*/
    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.id($('#txtAchievementId').val());
        self.reset();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        $('#uploadAchievementFile').click(function () {
            var formdata = new FormData();
            var fileInput = document.getElementById('fileInput');

            for (i = 0; i < fileInput.files.length; i++) {
                formdata.append(fileInput.files[i].name, fileInput.files[i]);
            }

            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/EmployeeAchievementManage/Upload');
            xhr.send(formdata);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                        self.directory(xhr.responseText);
                        ToastSuccess("File has successfully been uploaded.");
                    } else {
                        ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                    }
                }
            };
            return false;
        });
        
        activeParentMenu($('li.sub a[href="/EmployeeAchievementManage"]'));
    };
};


$(document).ready(function () {
    var vm = new EmployeeAchievementUpdateIndividualViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});