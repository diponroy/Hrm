﻿function EmployeeAppraisalCreateViewModel() {
    var self = this;
    
    self.allEmployeeAssignedAppraisalTypes = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);
    
    self.weight = ko.observable().extend({ required: true, digit: true });
    self.remarks = ko.observable();
    self.employeeAssignedAppraisalId = ko.observable().extend({ required: true });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Weight: self.weight(),
            Remarks: self.remarks(),
            EmployeeAssignedAppraisalId: self.employeeAssignedAppraisalId(),
            Status: self.status()
        };
    };

    self.getEmployeeAssignedAppraisalTypes = function () {
        jax.getJson(
            '/EmployeeAppraisalManage/GetEmployeeAssignedAppraisalTypes',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Type,
                        value: obj.Id
                    });
                });
                self.allEmployeeAssignedAppraisalTypes(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeAppraisalManage/Create',
            self.postObj(),
            function () {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.weight('');
        self.remarks('');
        self.employeeAssignedAppraisalId('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.getEmployeeAssignedAppraisalTypes();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeAppraisalManage"]'));
    };
}

$(document).ready(function () {
    var vm = new EmployeeAppraisalCreateViewModel();
    ko.applyBindings(vm);
    vm.init();
});