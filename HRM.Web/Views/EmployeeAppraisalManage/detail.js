﻿function EmployeeAppraisalDetailViewModel() {
    var self = this;
    self.logs = ko.observableArray([]);
    
    self.id = ko.observable();
    self.employeeAppraisal = ko.observable();
    
    self.getEmployeeAppraisal = function () {
        jax.getJsonBlock(
            '/EmployeeAppraisalManage/Get/' + self.id(),
            function (data) {
                self.employeeAppraisal({
                    weight: data.Weight,
                    remarks: data.Remarks,
                    employeeAssignedAppraisalType: data.EmployeeAssignedAppraisal.Type
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeAppraisalManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeAppraisal = data[index];
                    array.push({
                        id: employeeAppraisal.Id,
                        weight: employeeAppraisal.Weight,
                        remarks: employeeAppraisal.Remarks,
                        employeeAssignedAppraisalType: employeeAppraisal.EmployeeAssignedAppraisalLog.Type,
                        status: employeeAppraisal.Status,
                        statusString: employeeAppraisal.StatusString,
                        affectedByEmployeeName: employeeAppraisal.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(employeeAppraisal.affectedDateTime),
                        logStatus: employeeAppraisal.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#employeeAppraisalId').val());
        self.getEmployeeAppraisal();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeAppraisalDetailViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});
