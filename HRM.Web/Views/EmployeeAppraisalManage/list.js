﻿/*view models*/
function EmployeeAppraisalListViewModel() {
    var self = this;
    self.employeeAppraisals = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getEmployeeAppraisal = function (pageNo, pageSize, searchFilters) {
        self.employeeAppraisals([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/EmployeeAppraisalManage/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var object = data[index];
                    array.push({
                        id: object.Id,
                        weight: object.Weight,
                        remarks: object.Remarks,
                        employeeAssignedAppraisalType: (object.EmployeeAssignedAppraisal != null) ? object.EmployeeAssignedAppraisal.Type : 'Not yet assigned',
                        status: object.Status,
                        statusString: object.StatusString
                    });
                });
                self.employeeAppraisals(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function (item) {
        window.location = '/EmployeeAppraisalManage/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeAppraisalManage/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        var hasActiveChilds = null;
        jax.getJsonDfd(
            '/EmployeeAppraisalManage/CheckBeforeDelete/' + item.id,
            function (data) {
                hasActiveChilds = data;
            }
        );
        if (hasActiveChilds === true) {
            bootbox.alert("Can't delete this type because it have one or more dependent type.");
            return;
        }
        bootbox.confirm("Are you sure, you want to delete this type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJson(
            '/EmployeeAppraisalManage/Delete',
            obj,
            function (data) {
                ToastSuccess("Information deleted successfully");
                self.getEmployeeAppraisal(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.resetFilers();
        self.getEmployeeAppraisal(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeAppraisalManage"]'));
    };
}


$(document).ready(function () {
    var vm = new EmployeeAppraisalListViewModel();
    ko.applyBindings(vm);
    vm.init();
});