﻿function EmployeeAppraisalUpdateViewModel() {
    var self = this;

    self.allEmployeeAssignedAppraisalTypes = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.id = ko.observable();
    self.weight = ko.observable().extend({ required: true, digit: true });
    self.remarks = ko.observable();
    self.employeeAssignedAppraisalId = ko.observable().extend({ required: true });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Id: self.id(),
            Weight: self.weight(),
            Remarks: self.remarks(),
            EmployeeAssignedAppraisalId: self.employeeAssignedAppraisalId(),
            Status: self.status()
        };
    };

    self.getEmployeeAssignedAppraisalTypes = function () {
        return jax.getJson(
            '/EmployeeAppraisalManage/GetEmployeeAssignedAppraisalTypes',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Type,
                        value: obj.Id
                    });
                });
                self.allEmployeeAssignedAppraisalTypes(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeAppraisal = function () {
        jax.getJson(
            '/EmployeeAppraisalManage/Get/' + self.id(),
            function (data) {
                self.weight(data.Weight);
                self.remarks(data.Remarks);
                self.employeeAssignedAppraisalId(data.EmployeeAssignedAppraisalId);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeAppraisalManage/Update',
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadEmployeeAppraisal();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        
        var calls = [];
        calls.push(self.getEmployeeAssignedAppraisalTypes());
        $.when.apply(this, calls).done(function () {
            self.loadEmployeeAppraisal(self.id());
        });

        self.removeErrors();
    };
    self.init = function () {
        self.id($('#employeeAppraisalId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeAppraisalManage"]'));
    };
}

$(document).ready(function () {
    var vm = new EmployeeAppraisalUpdateViewModel();
    ko.applyBindings(vm);
    vm.init();
});