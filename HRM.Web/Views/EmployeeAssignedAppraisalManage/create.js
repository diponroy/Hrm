﻿/*View models*/
function EmployeeAssignedAppraisalCreateViewModel() {
    var self = this;
    
    self.allAppraisalIndicatorTitle = ko.observableArray([]);
    self.allEmployeeWorkStationUnit = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);
    
    
    self.type = ko.observable().extend({ required: true, maxLength: 50 });
    self.attachmentDate = ko.observable(currentDate()).extend({ required: true });
    self.remarks = ko.observable().extend({maxLength:250});
    self.appraisalIndicatorId = ko.observable().extend({required:true});
    self.employeeWorkStationUnitId = ko.observable().extend({required:true});
    self.status = ko.observable().extend({ required: true });
   

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Type: self.type(),
            AttachmentDate: self.attachmentDate(),
            AppraisalIndicatorId: self.appraisalIndicatorId(),
            EmployeeWorkStationUnitId: self.employeeWorkStationUnitId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.loadAppraisalIndicatorTitles = function () {
        jax.getJson(
            '/EmployeeAssignedAppraisalManage/GetAppraisalIndicatorTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allAppraisalIndicatorTitle(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.loadWorkStationUnits = function () {
        jax.getJson(
            '/EmployeeAssignedAppraisalManage/GetEmployeeWorkStationUnits',
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var obj = data[index];
                    var option = obj.WorkStationUnit.Remarks+'|'+ obj.WorkStationUnit.Company.Name + '|' + obj.WorkStationUnit.Branch.Name + ' | ' + obj.WorkStationUnit.Department.Name;
                    arr.push({
                        text: option,
                        value: obj.Id,
                    });
                });
                self.allEmployeeWorkStationUnit(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeAssignedAppraisalManage/Create',
            self.postObj(),
            function () {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.type('');
        self.attachmentDate(currentDate());
        self.appraisalIndicatorId('');
        self.employeeWorkStationUnitId('');
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.loadAppraisalIndicatorTitles();
        self.loadWorkStationUnits();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeAssignedAppraisalManage"]'));
    };
}

$(document).ready(function () {
    
    $('.datepicker').datepicker();

    var vm = new EmployeeAssignedAppraisalCreateViewModel();
    ko.applyBindings(vm);
    vm.init();
    
    $('#showAttachmentDate').click(function () {
        $('#btnAttachmentDate').focus();
    });
    $('#resetAttachmentDate').click(function () {
        vm.attachmentDate(currentDate());
    });
});




