﻿function EmployeeAssignedAppraisalDetailViewModel() {
    var self = this;
    self.logs = ko.observableArray([]);

    self.id = ko.observable();
    self.employeeAssignedAppraisal = ko.observable();

    self.getEmployeeAssignedAppraisal = function () {
        jax.getJsonBlock(
            '/EmployeeAssignedAppraisalManage/Get/' + self.id(),
            function (data) {
                self.employeeAssignedAppraisal({
                    type: data.Type,
                    attachmentDate: clientDateStringFromDate(data.AttachmentDate),
                    detachmentDate: (data.DetachmentDate)== null ? '--' : clientDateStringFromDate(data.DetachmentDate),
                    appraisalIndicatorTitle: data.AppraisalIndicator.Title,
                    workStationUnitName:data.EmployeeWorkStationUnit.WorkStationUnit.Remarks,
                    companyName: data.EmployeeWorkStationUnit.WorkStationUnit.Company.Name,
                    branchName: data.EmployeeWorkStationUnit.WorkStationUnit.Branch.Name,
                    departmentName: data.EmployeeWorkStationUnit.WorkStationUnit.Department.Name,
                    remarks: data.Remarks,
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeAssignedAppraisalManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeAssignedAppraisal = data[index];
                    array.push({
                        id: employeeAssignedAppraisal.Id,
                        type: employeeAssignedAppraisal.Type,
                        appraisalIndicatorTitle:employeeAssignedAppraisal.AppraisalIndicatorLog.Title,
                        workStationUnitName: employeeAssignedAppraisal.EmployeeWorkStationUnitLog.WorkStationUnitLog.Remarks,
                        attachmentDate: clientDateStringFromDate(employeeAssignedAppraisal.AttachmentDate),
                        detachmentDate: (employeeAssignedAppraisal.DetachmentDate)==null?'--':clientDateStringFromDate(employeeAssignedAppraisal.DetachmentDate),
                        remarks: employeeAssignedAppraisal.Remarks,
                        status: employeeAssignedAppraisal.Status,
                        statusString: employeeAssignedAppraisal.StatusString,
                        affectedByEmployeeName: employeeAssignedAppraisal.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(employeeAssignedAppraisal.affectedDateTime),
                        logStatus: employeeAssignedAppraisal.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#employeeAssignedAppraisalId').val());
        self.getEmployeeAssignedAppraisal();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeAssignedAppraisalDetailViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});
