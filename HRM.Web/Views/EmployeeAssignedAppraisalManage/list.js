﻿/*view models*/
function EmployeeAssignedAppraisalListViewModel() {
    var self = this;
    self.employeeAssignedAppraisals = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getEmployeeAssignedAppraisal = function (pageNo, pageSize, searchFilters) {
        self.employeeAssignedAppraisals([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/EmployeeAssignedAppraisalManage/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var object = data[index];
                    array.push({
                        id: object.Id,
                        type: object.Type,
                        remarks: object.Remarks,
                        appraisalIndicatorTitle: (object.AppraisalIndicator.Title == null) ? '--':object.AppraisalIndicator.Title,
                        company: (object.EmployeeWorkStationUnit.WorkStationUnit.Company.Name==null)?'--':object.EmployeeWorkStationUnit.WorkStationUnit.Company.Name,
                        branch: (object.EmployeeWorkStationUnit.WorkStationUnit.Branch.Name==null)?'--':object.EmployeeWorkStationUnit.WorkStationUnit.Branch.Name,
                        department: (object.EmployeeWorkStationUnit.WorkStationUnit.Department.Name==null)?'--':object.EmployeeWorkStationUnit.WorkStationUnit.Department.Name,
                        workStationUnit: (object.EmployeeWorkStationUnit.WorkStationUnit.Remarks==null)?'--':object.EmployeeWorkStationUnit.WorkStationUnit.Remarks,
                        attachmentDate: (object.AttachmentDate === null) ? '--' : clientDateStringFromDate(object.AttachmentDate),
                        detachmentDate: (object.DetachmentDate === null) ? '--' : clientDateStringFromDate(object.DetachmentDate),
                        isDetached: object.DetachmentDate != null,
                        status: object.Status,
                        statusString: object.StatusString
                    });
                });
                self.employeeAssignedAppraisals(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function (item) {
        window.location = '/EmployeeAssignedAppraisalManage/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeAssignedAppraisalManage/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        var hasActiveChilds = null;
        jax.getJsonDfd(
            '/EmployeeAssignedAppraisalManage/CheckBeforeDelete/' + item.id,
            function (data) {
                hasActiveChilds = data;
            }
        );
        if (hasActiveChilds === true) {
            bootbox.alert("Can't delete this type because it have one or more dependent type.");
            return;
        }
        bootbox.confirm("Are you sure, you want to delete this type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJson(
            '/EmployeeAssignedAppraisalManage/Delete',
            obj,
            function (data) {
                ToastSuccess("Information deleted successfully");
                self.getEmployeeAssignedAppraisal(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.resetFilers();
        self.getEmployeeAssignedAppraisal(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeAssignedAppraisalManage"]'));
    };
}


$(document).ready(function () {
    var vm = new EmployeeAssignedAppraisalListViewModel();
    ko.applyBindings(vm);
    vm.init();
});