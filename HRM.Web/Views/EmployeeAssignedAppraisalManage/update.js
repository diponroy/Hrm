﻿function EmployeeAssignedAppraisalUpdateViewModel() {
    var self = this;

    self.allAppraisalIndicatorTitle = ko.observableArray([]);
    self.allEmployeeWorkStationUnit = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.id = ko.observable();
    self.type = ko.observable().extend({ required: true, maxLength: 50 });
    self.attachmentDate = ko.observable().extend({ required: true });
    self.hasClosingDate = ko.observable(false);
    self.detachmentDate = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.appraisalIndicatorId = ko.observable().extend({required:true});
    self.employeeWorkStationUnitId = ko.observable().extend({required:true});
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errorsWithOutStatus = ko.validation.group([self.type, self.attachmentDate, self.detachmentDate, self.appraisalIndicatorId, self.employeeWorkStationUnitId, self.remarks]);
    self.errorsWithStatus = ko.validation.group([self.type, self.attachmentDate, self.appraisalIndicatorId, self.employeeWorkStationUnitId, self.remarks, self.status]);
    self.errors = ko.computed(function () { return self.hasClosingDate() ? self.errorsWithOutStatus : self.errorsWithStatus; }, this);
    self.hasErrors = function () {return ((self.errors()()).length > 0) ? true : false;};
    self.showErrors = function () {self.errors().showAllMessages();};
    self.removeErrors = function () {self.errors().showAllMessages(false);};

    /*objects to post*/
    self.postObj = function () {
        return {
            Id: self.id(),
            Type: self.type(),
            AttachmentDate: self.attachmentDate(),
            DetachmentDate:self.detachmentDate(),
            AppraisalIndicatorId: self.appraisalIndicatorId(),
            EmployeeWorkStationUnitId: self.employeeWorkStationUnitId(),
            Remarks: self.remarks(),
            Status: (self.hasClosingDate()) ? null : self.status()
        };
    };
    
    self.setClosedDateTime = function () {
        self.hasClosingDate(true);
        self.detachmentDate(currentDate());
    };

    self.removeClosedDateTime = function () {
        self.hasClosingDate(false);
        self.detachmentDate(null);
    };

    self.loadAppraisalIndicatorTitles = function () {
        return jax.getJson(
            '/EmployeeAssignedAppraisalManage/GetAppraisalIndicatorTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allAppraisalIndicatorTitle(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadWorkStationUnits = function () {
        return jax.getJson(
            '/EmployeeAssignedAppraisalManage/GetEmployeeWorkStationUnits',
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var obj = data[index];
                    var option = obj.WorkStationUnit.Remarks + '|' + obj.WorkStationUnit.Company.Name + '|' + obj.WorkStationUnit.Branch.Name + ' | ' + obj.WorkStationUnit.Department.Name;
                    arr.push({
                        text: option,
                        value: obj.Id,
                    });
                });
                self.allEmployeeWorkStationUnit(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeAssignedAppraisal = function () {
        jax.getJson(
            '/EmployeeAssignedAppraisalManage/Get/' + self.id(),
            function (data) {
                self.type(data.Type);
                self.attachmentDate(clientDate(data.AttachmentDate));
                if (data.DetachmentDate != null) {
                    self.hasClosingDate(true);
                    self.detachmentDate(clientDate(data.DetachmentDate));
                } else {
                    self.removeClosedDateTime();
                }
                self.appraisalIndicatorId(data.AppraisalIndicatorId);
                self.employeeWorkStationUnitId(data.EmployeeWorkStationUnitId);
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeAssignedAppraisalManage/Update' ,
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadEmployeeAssignedAppraisal();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        
        var calls = [];
        calls.push(self.loadAppraisalIndicatorTitles());
        calls.push(self.loadWorkStationUnits());
        $.when.apply(this, calls).done(function () {
            self.loadEmployeeAssignedAppraisal(self.id());
        });

        self.removeErrors();
    };
    self.init = function () {
        self.id($('#employeeAssignedAppraisalId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeAssignedAppraisalManage"]'));
    };
}

$(document).ready(function () {
    
    $('.datepicker').datepicker();
    
    var vm = new EmployeeAssignedAppraisalUpdateViewModel();
    ko.applyBindings(vm);
    vm.init();
    
    $('#showAttachmentDate').click(function () {
        $('#btnAttachmentDate').focus();
    });
    $('#resetAttachmentDate').click(function () {
        vm.attachmentDate(currentDate());
    });
    
    $('#showDetachmentDate').click(function () {
        $('#btnDetachmentDate').focus();
    });
    $('#resetDetachmentDate').click(function () {
        vm.detachmentDate(currentDate());
    });
});