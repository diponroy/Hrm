﻿/*View models*/
function EmployeeAttachedTrainingCreateViewModel() {
    var self = this;
    self.allTrainingTitles = ko.observableArray([]);
    self.allEmployees = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.employeeId = ko.observable().extend({ required: true });
    self.employeeTrainingId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            EmployeeId: self.employeeId(),
            EmployeeTrainingId: self.employeeTrainingId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };
    
    self.getEmployees = function () {
        jax.getJson(
            '/EmployeeAttachedTrainingManage/GetEmployee',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.FullName,
                        value: obj.Id
                    });
                });
                self.allEmployees(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getTrainingTitles = function () {
        jax.getJson(
            '/EmployeeAttachedTrainingManage/GetTrainingTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allTrainingTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeAttachedTrainingManage/Create',
            self.postObj(),
            function () {
                ToastSuccess('Information Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    self.reset = function () {
        self.getEmployees();
        self.getTrainingTitles();

        self.employeeId('');
        self.employeeTrainingId('');
        self.remarks('');
        self.status('');

        self.removeErrors();
    };

    self.init = function () {
        var status = [];
        status.push({ name: 'Running', value:0 });
        status.push({ name: 'Completed', value: 1 });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeAttachedTrainingManage"]'));
    };
}

$(document).ready(function () {

    var vm = new EmployeeAttachedTrainingCreateViewModel();
    ko.applyBindings(vm);
    vm.init();
});



