﻿function EmployeeAttachedTrainingDetailViewModel() {
    var self = this;

    self.employeeAttachedTraining = ko.observable();
    self.logs = ko.observableArray([]);

    self.id = ko.observable();

    self.getEmployeeAttachedTraining = function() {
        jax.getJsonBlock(
            '/EmployeeAttachedTrainingManage/Get/' + self.id(),
            function(data) {
                self.employeeAttachedTraining({
                    employeeName: data.Employee.FullName,
                    trainingTitle: data.EmployeeTraining.Title,
                    remarks: data.Remarks
                });
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function() {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeAttachedTrainingManage/GetLogs/' + self.id(),
            function(data) {
                var array = new Array();
                $.each(data, function(index) {
                    var employeeAttachedTraining = data[index];
                    array.push({
                        id: employeeAttachedTraining.Id,
                        employeeName: employeeAttachedTraining.EmployeeLog.FullName,
                        trainingTitle: employeeAttachedTraining.EmployeeTrainingLog.Title,
                        remarks: employeeAttachedTraining.Remarks,
                        status: employeeAttachedTraining.Status,
                        statusString: employeeAttachedTraining.StatusString == 'Active' ? 'Running' : 'Completed',
                        affectedByEmployeeName: employeeAttachedTraining.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(employeeAttachedTraining.affectedDateTime),
                        logStatus: employeeAttachedTraining.LogStatuses
                    });
                });
                self.logs(array);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function() {
        self.id($('#employeeAttachedTrainingId').val());
        self.getEmployeeAttachedTraining();
        self.getLogs();
    };
}

$(document).ready(function() {
    var vm = new EmployeeAttachedTrainingDetailViewModel();
    ko.applyBindings(vm);
    vm.init();
});