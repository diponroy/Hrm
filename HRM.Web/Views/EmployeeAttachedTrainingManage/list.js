﻿function EmployeeAttachedTrainingListViewModel() {
    var self = this;
    self.employeeAttachedTrainings = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.employeeAttachedTrainings([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/EmployeeAttachedTrainingManage/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeAttachedTraining = data[index];
                    array.push({
                        id: employeeAttachedTraining.Id,
                        employeeName: employeeAttachedTraining.Employee.FullName,
                        employeeTrainingTitle: employeeAttachedTraining.EmployeeTraining.Title,
                        remarks: employeeAttachedTraining.Remarks,
                        status: employeeAttachedTraining.Status,
                        statusString: employeeAttachedTraining.StatusString == 'Active' ? 'Running' : 'Completed'
                    });
                });
                self.employeeAttachedTrainings(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/EmployeeAttachedTrainingManage/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeAttachedTrainingManage/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/EmployeeAttachedTrainingManage/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeAttachedTrainingManage"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeAttachedTrainingListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});