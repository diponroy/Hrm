﻿/*View models*/
function EmployeeAttachedTrainingUpdateViewModel() {
    var self = this;
    
    self.allEmployees = ko.observableArray([]);
    self.allTrainingTitles = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.id = ko.observable();
    self.employeeId = ko.observable().extend({ required: true });
    self.employeeTrainingId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });
    
    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Id: self.id(),
            EmployeeId: self.employeeId(),
            EmployeeTrainingId: self.employeeTrainingId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };
    
    self.getEmployees = function () {
        return jax.getJson(
            '/EmployeeAttachedTrainingManage/GetEmployee',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.FullName,
                        value: obj.Id
                    });
                });
                self.allEmployees(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getTrainingTitles = function () {
        return jax.getJson(
            '/EmployeeAttachedTrainingManage/GetTrainingTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allTrainingTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeAttachedTraining = function () {
        jax.getJsonBlock(
            '/EmployeeAttachedTrainingManage/Get/' + self.id(),
            function (data) {
                self.employeeId(data.EmployeeId);
                self.employeeTrainingId(data.EmployeeTrainingId);
                self.remarks(data.Remarks);
                self.status(data.Status);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeAttachedTrainingManage/Update',
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadEmployeeAttachedTraining();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        
        var status = [];
        status.push({ name: 'Running', value:0 });
        status.push({ name: 'Completed', value: 1 });
        self.allStatus(status);

        var calls = [];
        calls.push(self.getEmployees());
        calls.push(self.getTrainingTitles());
        $.when.apply(this, calls).done(function () {
            $.when.apply(this, self.loadEmployeeAttachedTraining(self.id())).done(function () {
                self.removeErrors();
            });
        });
    };
    
    self.init = function () {
        self.id($('#employeeAttachedTrainingId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeAttachedTrainingManage"]'));
    };
}

$(document).ready(function () {
    var vm = new EmployeeAttachedTrainingUpdateViewModel();
    ko.applyBindings(vm);
    vm.init();
});