﻿function EmployeeAttendanceViewModel() {
    var self = this;

    self.allEmployee = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.employeeWorkStationUnitId = ko.observable().extend({ required: true });
    self.attendanceDate = ko.observable(currentDate()).extend({ required: true });
    self.attendanceTime = ko.observable(moment().format('h:mm')).extend({ required: true });
    self.remarks = ko.observable('').extend({ maxLength: 250 });
    self.status = ko.observable('').extend({ required: true });

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };


    /* Local Actions */
    self.postObj = function () {
        return {
            EmployeeWorkStationUnitId: self.employeeWorkStationUnitId(),
            AttendanceDate: self.attendanceDate(),
            AttendanceTime: self.attendanceTime(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };
    
    self.getEmployees = function () {
        jax.getJson(
            '/EmployeeAttendance/GetEmployees',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Employee.FullName,
                        value: obj.Id
                    });
                });
                self.allEmployee(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /* Server Actions */
    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeAttendance/Create',
            self.postObj(),
            function () {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };
    
    self.reset = function () {
        self.employeeWorkStationUnitId('');
        self.attendanceDate(currentDate());
        self.attendanceTime(moment().format('h:mm'));
        self.remarks('');
        self.status('');

        self.getEmployees();

        self.removeErrors();
    };

    self.init = function () {

        var status = [];
        status.push({ name: 'In', value: 0 });
        status.push({ name: 'Out', value: 1 });
        self.allStatus(status);

        self.reset();

        activeParentMenu($('li.sub a[href="/EmployeeAttendance"]'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();
    
    var viewModel = new EmployeeAttendanceViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
    
    
    $('.clockpicker').clockpicker({
        donetext: 'Done'
    })
    .find('input').change(function () {
	    console.log(this.value);
	});
   
    var input = $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });

     //Manually toggle to the minutes view
    $('#check-minutes').click(function (e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
                .clockpicker('toggleView', 'minutes');
    });
    
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
    
    $('#btnShowAttendanceDate').click(function () {
        $('#btnAttendanceDate').focus();
    });
    
    $('#btnResetAttendanceDate').click(function () {
        viewModel.attendanceDate(currentDate());
    });
});