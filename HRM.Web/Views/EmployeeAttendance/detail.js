﻿function EmployeeAttendanceDetailViewModel() {
    var self = this;

    self.employeeAttendance = ko.observable();
    self.logs = ko.observableArray([]);

    self.id = ko.observable();

    self.getEmployeeAttendance = function() {
        jax.getJsonBlock(
            '/EmployeeAttendance/Get/' + self.id(),
            function(data) {
                self.employeeAttendance({
                    employee: data.EmployeeWorkStationUnit.Employee.FullName,
                    attendanceDate: clientDateStringFromDate(data.AttendanceDate),
                    attendanceTime: data.AttendanceTime,
                    remarks: data.Remarks
                });
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function() {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeAttendance/GetLogs/' + self.id(),
            function(data) {
                var array = new Array();
                $.each(data, function(index) {
                    var employeeAttendance = data[index];
                    array.push({
                        id: employeeAttendance.Id,
                        employee: employeeAttendance.EmployeeWorkStationUnitLog.EmployeeLog.FullName,
                        attendanceDate: clientDateStringFromDate(employeeAttendance.AttendanceDate),
                        attendanceTime: employeeAttendance.AttendanceTime,
                        remarks: employeeAttendance.Remarks,
                        status: employeeAttendance.Status,
                        statusString: employeeAttendance.StatusString == 'Active' ? 'In' : 'Out',
                        affectedByEmployeeName: employeeAttendance.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(employeeAttendance.affectedDateTime),
                        logStatus: employeeAttendance.LogStatuses
                    });
                });
                self.logs(array);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function() {
        self.id($('#txtEmployeeAttenanceId').val());
        self.getEmployeeAttendance();
        self.getLogs();
    };
}

$(document).ready(function() {
    var vm = new EmployeeAttendanceDetailViewModel();
    ko.applyBindings(vm);
    vm.init();
});