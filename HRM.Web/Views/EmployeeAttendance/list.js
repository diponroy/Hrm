﻿function EmployeeAttendanceListViewModel() {
    var self = this;
    self.employeeAttendances = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.employeeAttendances([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/EmployeeAttendance/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeAttendance = data[index];
                    array.push({
                        id: employeeAttendance.Id,
                        employeeName: employeeAttendance.EmployeeWorkStationUnit.Employee.FullName,
                        time: employeeAttendance.AttendanceTime,
                        date: clientDateStringFromDate(employeeAttendance.AttendanceDate),
                        remarks: employeeAttendance.Remarks,
                        status: employeeAttendance.Status,
                        statusString: employeeAttendance.StatusString == 'Active' ? 'In' : 'Out'
                    });
                });
                self.employeeAttendances(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/EmployeeAttendance/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeAttendance/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/EmployeeAttendance/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        activeParentMenu($('li.sub a[href="/EmployeeAttendance"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeAttendanceListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});