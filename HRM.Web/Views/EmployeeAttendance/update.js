﻿/*View models*/
function EmployeeAttendanceUpdateViewModel() {
    var self = this;

    self.allEmployee = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);
    
    self.id = ko.observable();
    self.employeeWorkStationUnitId = ko.observable().extend({ required: true });
    self.attendanceDate = ko.observable(currentDate()).extend({ required: true });
    self.attendanceTime = ko.observable(moment().format('h:mm')).extend({ required: true });
    self.remarks = ko.observable('').extend({ maxLength: 250 });
    self.status = ko.observable('').extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Id: self.id(),
            EmployeeWorkStationUnitId: self.employeeWorkStationUnitId(),
            AttendanceDate: self.attendanceDate(),
            AttendanceTime: self.attendanceTime(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.getEmployees = function () {
        jax.getJson(
            '/EmployeeAttendance/GetEmployees',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Employee.FullName,
                        value: obj.Id
                    });
                });
                self.allEmployee(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeAttendance = function () {
        jax.getJsonBlock(
            '/EmployeeAttendance/Get/' + self.id(),
            function (data) {
                self.employeeWorkStationUnitId(data.EmployeeWorkStationUnitId);
                self.attendanceDate(data.AttendanceDate);
                self.attendanceTime(data.AttendanceTime);
                self.remarks(data.Remarks);
                self.status(data.Status);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeAttendance/Update',
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        var status = [];
        status.push({ name: 'In', value: 0 });
        status.push({ name: 'Out', value: 1 });
        self.allStatus(status);

        var calls = [];
        calls.push(self.getEmployees());
        $.when.apply(this, calls).done(function () {
            $.when.apply(this, self.loadEmployeeAttendance(self.id())).done(function () {
                self.removeErrors();
            });
        });
    };

    self.init = function () {
        self.id($('#txtAttendanceId').val());
        self.reset();

        activeParentMenu($('li.sub a[href="/EmployeeAttendance"]'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();
    
    var vm = new EmployeeAttendanceUpdateViewModel();
    ko.applyBindings(vm);
    vm.init();
    
    $('.clockpicker').clockpicker({
        donetext: 'Done'
    })
    .find('input').change(function () {
        console.log(this.value);
    });

    var input = $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });

    //Manually toggle to the minutes view
    $('#check-minutes').click(function (e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
                .clockpicker('toggleView', 'minutes');
    });

    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }

    $('#btnShowAttendanceDate').click(function () {
        $('#btnAttendanceDate').focus();
    });

    $('#btnResetAttendanceDate').click(function () {
        vm.attendanceDate(currentDate());
    });
});