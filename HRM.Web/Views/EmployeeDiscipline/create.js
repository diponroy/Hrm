﻿/*View models*/
function EmployeeDisciplineCreateViewModel() {
    var self = this;

    self.allEmployees = ko.observableArray([]);
    self.allDepartments = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.employeeId = ko.observable().extend({ required: true });
    self.title = ko.observable().extend({ required: true, maxLength: 100});
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.departmentId = ko.observable().extend({ required: true });
    self.description = ko.observable().extend({ maxLength: 250 });
    self.actionTaken = ko.observable().extend({ maxLength: 250 });
    self.directory = ko.observable().extend({ maxLength: 250 });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.status = ko.observable().extend({ required: true });


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            EmployeeId:self.employeeId(),
            Title: self.title(),
            DateOfCreation:self.dateOfCreation(),
            DepartmentId: self.departmentId(),
            Description: self.description(),
            ActionTaken: self.actionTaken(),
            AttachmentDirectory: self.directory(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.getEmployees = function () {
        jax.getJson(
            '/EmployeeDiscipline/GetEmployees',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.FullName,
                        value: obj.Id
                    });
                });
                self.allEmployees(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getDepartments = function () {
        jax.getJson(
            '/EmployeeDiscipline/GetDepartments',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Name,
                        value: obj.Id
                    });
                });
                self.allDepartments(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeDiscipline/Create',
            self.postObj(),
            function () {
                ToastSuccess('Employee Discipline created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.employeeId('');
        self.title('');
        self.dateOfCreation(currentDate());
        self.departmentId('');
        self.description('');
        self.actionTaken('');
        self.directory('');
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.getEmployees();
        self.getDepartments();

        var status = [];
        status.push({ name: 'Pending', value:0 });
        status.push({ name: 'Closed', value:1 });
        self.allStatus(status);
        
        $('#uploadFile').click(function () {
            var formdata = new FormData();
            var fileInput = document.getElementById('fileInput');

            for (i = 0; i < fileInput.files.length; i++) {
                formdata.append(fileInput.files[i].name, fileInput.files[i]);
            }

            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/EmployeeDiscipline/Upload');
            xhr.send(formdata);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                        self.directory(xhr.responseText);
                        ToastSuccess("File successfully been uploaded.");
                    } else {
                        ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                    }
                }
            };
            return false;
        });
        self.reset();
        activeParentMenu($('li.sub a[href="/EmployeeDiscipline"]'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();
    
    var vm = new EmployeeDisciplineCreateViewModel();
    ko.applyBindings(vm);
    vm.init();
    
    $('#btnShowDatePickerCreation').click(function () {
        $('#btnDateOfCreation').focus();
    });
    $('#btnResetDatePickerCreation').click(function () {
        vm.dateOfCreation(currentDate());
    });
});