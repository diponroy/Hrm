﻿function EmployeeDisciplineViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employeeDiscipline = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all*/
    self.getemployeeDiscipline = function () {
        jax.getJsonBlock(
            '/EmployeeDiscipline/Get/' + self.id(),
            function (data) {
                self.employeeDiscipline({
                    employee: data.Employee.FullName,
                    title: data.Title
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeDiscipline/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeDiscipline = data[index];
                    array.push({
                        id: employeeDiscipline.Id,
                        employee: employeeDiscipline.EmployeeLog.FullName,
                        title: employeeDiscipline.Title,
                        dateOfCreation: clientDateStringFromDate(employeeDiscipline.DateOfCreation),
                        department: employeeDiscipline.DepartmentLog.Name,
                        description: employeeDiscipline.Description,
                        actionTaken: employeeDiscipline.ActionTaken,
                        directory: employeeDiscipline.AttachmentDirectory,
                        remarks: employeeDiscipline.Remarks,
                        status: employeeDiscipline.Status,
                        statusString: employeeDiscipline.StatusString=='Active'?'Pending':'Closed',
                        affectedByEmployeeId: employeeDiscipline.AffectedByEmployeeId,
                        affectedByEmployeeName: employeeDiscipline.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(employeeDiscipline.affectedDateTime),
                        logStatus: employeeDiscipline.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#txtEmployeeDisciplineId').val());
        self.getemployeeDiscipline();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeDisciplineViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});