﻿function EmployeeDisciplineListViewModel() {
    var self = this;
    self.employeeDisciplines = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () { };
    self.resetFilers = function () { };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.employeeDisciplines([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/EmployeeDiscipline/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeDiscipline = data[index];
                    array.push({
                        id: employeeDiscipline.Id,
                        employee:employeeDiscipline.Employee.FullName,
                        title: employeeDiscipline.Title,
                        dateOfCreation: clientDateStringFromDate(employeeDiscipline.DateOfCreation),
                        department: employeeDiscipline.Department.Name,
                        description: employeeDiscipline.Description,
                        actionTaken: employeeDiscipline.ActionTaken,
                        attachmentDirectory: employeeDiscipline.AttachmentDirectory,
                        remarks: employeeDiscipline.Remarks,
                        status: employeeDiscipline.Status,
                        statusString: employeeDiscipline.StatusString == 'Active' ? 'Pending' : 'Closed'
                    });
                });
                self.employeeDisciplines(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/EmployeeDiscipline/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeDiscipline/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/EmployeeDiscipline/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        activeParentMenu($('li.sub a[href="/EmployeeDiscipline"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeDisciplineListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});