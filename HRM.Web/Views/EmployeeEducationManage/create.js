﻿
function EmployeeEducationViewModel() {
    var self = this;

    /* basic */
        self.employeeId = ko.observable(),
        self.employeeName = ko.observable(),

        /*properties*/
        self.degreeTitle = ko.observable().extend({ required: true, maxLength: 50 }),
        self.institution = ko.observable().extend({ required: true, maxLength: 100 }),
        self.durationFromDate = ko.observable(currentDate()).extend({ required: true }),
        self.durationToDate = ko.observable(currentDate()).extend({ required: true }),
        self.result = ko.observable().extend({ required: true, number: true, maxLength: 20 }),
        self.major = ko.observable().extend({ required: true, maxLength: 50 }),
        self.educationRemarks = ko.observable().extend({ maxLength: 150 }),
        self.educationDirectory = ko.observable().extend({ maxLength: 250 }),
        self.educationStatus = ko.observable().extend({ required: true }),
        self.allEducationStatuses = ko.observableArray([]),

        self.btnVisibility = ko.observable(true),

        /*errors*/
        self.errors = ko.validation.group(self),
        self.hasErrors = function () { return (self.errors().length > 0) ? true : false; },
        self.showErrors = function () { self.errors.showAllMessages(); },
        self.removeErrors = function () { self.errors.showAllMessages(false); },


        /* actions */
        self.getEmployee = function() {
            jax.postJsonBlock(
                '/EmployeeManage/Get/' + self.employeeId(),
                null,
                function(data) {
                    self.employeeName(data.FullName);
                },
                function(qXhr, textStatus, error) {
                    ToastError("Employee Name couldn't be retrieved !!");
                }
            );
        };

    self.getEmployeeEducationDetail = function() {
        var empEducationObj = {
            Title: self.degreeTitle(),
            Institution: self.institution(),
            DurationFromDate: self.durationFromDate(),
            DurationToDate: self.durationToDate(),
            Result: self.result(),
            Major: self.major(),
            Remarks: self.educationRemarks(),
            AttachmentDirectory: self.educationDirectory(),
            EmployeeId: self.employeeId(),
            Status: self.educationStatus()
        };

        return empEducationObj;
    };

    self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/EmployeeEducationManage/Create',
            self.getEmployeeEducationDetail(),
            function() {
                ToastSuccess('Education information saved successfully');
                self.reset();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.goAchievement = function () {
        window.location = '/EmployeeAchievementManage/Create/' + self.employeeId();
    };


    /*resets*/
    self.reset = function() {
        self.degreeTitle('');
        self.institution('');
        self.durationFromDate(currentDate());
        self.durationToDate(currentDate());
        self.result('');
        self.major('');
        self.educationRemarks('');
        self.educationDirectory('');
        self.educationStatus('');
        self.removeErrors();
    }; 

    self.init = function () {
        self.employeeId($('#txtEmployeeId').val());
        self.getEmployee();

        var educationStatus = [];
        educationStatus.push({ name: 'Active' });
        educationStatus.push({ name: 'Inactive' });
        self.allEducationStatuses(educationStatus);
        
        activeParentMenu($('li.sub a[href="/EmployeeEducationManage"]'));
    };
};


$(document).ready(function() {
    $('.datepicker').datepicker();

    var vm = new EmployeeEducationViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowFromDate').click(function() {
        $('#btnFromDate').focus();
    });
    $('#btnResetFromDate').click(function() {
        vm.durationFromDate(currentDate());
    });

    $('#btnShowToDate').click(function() {
        $('#btnToDate').focus();
    });
    $('#btnResetToDate').click(function() {
        vm.durationToDate(currentDate());
    });

    $('#uploadEducationImage').click(function() {
        var formdata = new FormData();
        var fileInput = document.getElementById('fileInput');

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/EmployeeEducationManage/Upload');
        xhr.send(formdata);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                    vm.educationDirectory(xhr.responseText);
                    ToastSuccess("Image has successfully been uploaded.");
                } else {
                    ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                }
            }
        };
    });
});