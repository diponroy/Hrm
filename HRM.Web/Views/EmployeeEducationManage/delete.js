﻿function EmployeeEduViewModel() {
    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete this ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/EmployeeEducationManage/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeEduViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});