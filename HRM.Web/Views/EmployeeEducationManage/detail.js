﻿
function EmployeeEducationLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.eduId = ko.observable();

    self.employeeName = ko.observable();
    self.employee = ko.observable();
    self.employeeEducations = ko.observableArray([]);

    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.eduId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getEmployeeEducations = function () {

        self.employeeEducations([]);

        jax.postJsonBlock(
            '/EmployeeEducationManage/GetEducations/' + self.eduId(),
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeEducation = data[index];
                    arr.push({
                        id: anEmployeeEducation.Id,
                        title: anEmployeeEducation.Title,
                        major: anEmployeeEducation.Major,
                        institution: anEmployeeEducation.Institution,
                        toDate: clientDateStringFromDate(anEmployeeEducation.DurationToDate).substr(6, 9)
                    });
                });
                self.employeeEducations(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    }

    self.showLog = function(item) {
        window.location = '/EmployeeEducationManage/DetailIndividual/' + item.id;
    };

    self.init = function () {
        self.eduId($('#txtEmployeeEducationId').val());
        self.getEmployee();
        self.getEmployeeEducations();
    };
};


$(document).ready(function () {
    var vm = new EmployeeEducationLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});