﻿
function EmployeeEducationLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.degreeTitle = ko.observable();

    self.employeeEducationLogs = ko.observableArray([]);


    /* Actions */

    self.getEmployeeEducation = function () {
        jax.postJsonBlock(
            '/EmployeeEducationManage/Get/' + self.id(),
            null,
            function (data) {
                self.degreeTitle(data.Title + ' in ' + data.Major);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getEducationLogs = function () {
        self.employeeEducationLogs([]);

        $.ajax({
            url: '/EmployeeEducationManage/GetLogs/' + self.id(),
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEducation = data[index];
                    arr.push({
                        id: anEducation.Id,
                        title: anEducation.Title,
                        institution: anEducation.Institution,
                        durationFromDate: (anEducation.DurationFromDate === null) ? "---" : clientDateStringFromDate(anEducation.DurationFromDate),
                        durationToDate: (anEducation.DurationToDate === null) ? "---" : clientDateStringFromDate(anEducation.DurationToDate),
                        result: anEducation.Result,
                        major: anEducation.Major,
                        remarks: anEducation.Remarks,
                        attachmentDirectory: anEducation.AttachmentDirectory,

                        status: anEducation.Status,
                        statusString: anEducation.StatusString,
                        affectedByName: anEducation.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(anEducation.AffectedDateTime),
                        logStatus: anEducation.LogStatuses
                    });
                });

                self.employeeEducationLogs(arr);
                $.unblockUI();
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.init = function () {
        self.id($('#txtEmployeeEducationId').val());
        self.getEmployeeEducation();
        self.getEducationLogs();
    };
}


$(document).ready(function () {
    var vm = new EmployeeEducationLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});