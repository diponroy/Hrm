﻿
function EmployeeEducationUpdateViewModel() {
    var self = this;

    /* basic */
    self.employeeId = ko.observable(),
    self.employeeName = ko.observable(),

    /*properties*/
    self.employeeEducations = ko.observableArray([]);


    /* actions */
    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.employeeId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError("Employee Name couldn't be retrieved !!");
            }
        );
    };

    self.load = function() {
        self.employeeEducations([]);

        jax.postJsonBlock(
            '/EmployeeEducationManage/GetEducations/' + self.employeeId(),
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeEducation = data[index];
                    arr.push({
                        id: anEmployeeEducation.Id,
                        title: anEmployeeEducation.Title,
                        major: anEmployeeEducation.Major,
                        institution: anEmployeeEducation.Institution,
                        toDate: clientDateStringFromDate(anEmployeeEducation.DurationToDate).substr(6, 9)
                    });
                });
                self.employeeEducations(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.showToEdit = function(item) {
        window.location = '/EmployeeEducationManage/UpdateIndividual/' + item.id;
    };

    self.showToCreate = function () {
        window.location = '/EmployeeEducationManage/Create/' + self.employeeId();
        $.EmployeeEducationViewModel.btnVisibility(false);
    };

    self.goAchievement = function () {
        window.location = '/EmployeeAchievementManage/Update/' + self.employeeId();
    };

    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete this Employee ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/EmployeeEducationManage/Delete',
            obj,
            function (data) {
                ToastSuccess("Employee Education deleted successfully");
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };


    /*resets*/
    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.employeeId($('#txtEmployeeId').val());
        self.getEmployee();
        self.load();
    };
    
    activeParentMenu($('li.sub a[href="/EmployeeEducationManage"]'));
};


$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new EmployeeEducationUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});