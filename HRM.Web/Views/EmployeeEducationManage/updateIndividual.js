﻿
function EmployeeEducationUpdateIndividualViewModel() {
    var self = this;

    /* basic */
    self.employeeId = ko.observable(),
        self.employeeName = ko.observable(),

        /*properties*/
    self.id = ko.observable(),
    self.degreeTitle = ko.observable().extend({ required: true, maxLength: 50 }),
    self.institution = ko.observable().extend({ required: true, maxLength: 100 }),
    self.durationFromDate = ko.observable(currentDate()).extend({ required: true }),
    self.durationToDate = ko.observable(currentDate()).extend({ required: true }),
    self.result = ko.observable().extend({ required: true, number: true, maxLength: 20 }),
    self.major = ko.observable().extend({ required: true, maxLength: 50 }),
    self.educationRemarks = ko.observable().extend({ maxLength: 150 }),
    self.educationDirectory = ko.observable().extend({ maxLength: 250 }),
    self.educationStatus = ko.observable().extend({ required: true }),
    self.allEducationStatuses = ko.observableArray([]),


    /*errors*/
    self.errors = ko.validation.group(self),
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; },
    self.showErrors = function () { self.errors.showAllMessages(); },
    self.removeErrors = function () { self.errors.showAllMessages(false); },


    /*datepicker*/
    self.isFromDatepickerFocused = ko.observable(false);

    self.showFromDatepicker = function () {
        self.isFromDatepickerFocused(true);
    };

    self.resetFromDatepicker = function () {
        self.durationFromDate(currentDate());
    };

    self.isToDatepickerFocused = ko.observable(false);

    self.showToDatepicker = function () {
        self.isToDatepickerFocused(true);
    };

    self.resetToDatepicker = function () {
        self.durationToDate(currentDate());
    };


    /* actions */
    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.employeeId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError("Employee Name couldn't be retrieved !!");
            }
        );
    };

    self.getEmployeeEducationDetail = function () {
        var empEducationObj = {
            Id:self.id(),
            Title: self.degreeTitle(),
            Institution: self.institution(),
            DurationFromDate: self.durationFromDate(),
            DurationToDate: self.durationToDate(),
            Result: self.result(),
            Major: self.major(),
            Remarks: self.educationRemarks(),
            AttachmentDirectory: self.educationDirectory(),
            EmployeeId: self.employeeId(),
            Status: self.educationStatus()
        };

        return empEducationObj;
    };

    self.load = function() {
        jax.postJsonBlock(
            '/EmployeeEducationManage/Get/' + self.id(),
            null,
            function (data) {
                self.id(data.Id);
                self.degreeTitle(data.Title);
                self.institution(data.Institution);
                self.durationFromDate(clientDate(data.DurationFromDate));
                self.durationToDate(clientDate(data.DurationToDate));
                self.result(data.Result);
                self.major(data.Major);
                self.educationRemarks(data.Remarks);
                self.educationDirectory(data.AttachmentDirectory);
                self.educationStatus(data.StatusString);
                self.employeeId(data.EmployeeId);

                self.getEmployee();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );       
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/EmployeeEducationManage/Update',
            self.getEmployeeEducationDetail(),
            function () {
                ToastSuccess('Education information is saved successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    /*resets*/
    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.id($('#txtEducationId').val());

        var educationStatus = [];
        educationStatus.push({ name: 'Active' });
        educationStatus.push({ name: 'Inactive' });
        self.allEducationStatuses(educationStatus);

        self.load();
    };
};


$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new EmployeeEducationUpdateIndividualViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#uploadEducationImage').click(function () {
        var formdata = new FormData();
        var fileInput = document.getElementById('fileInput');

        for (i = 0; i < fileInput.files.length; i++) {
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/EmployeeEducationManage/Upload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                    vm.educationDirectory(xhr.responseText);
                    ToastSuccess("Image has successfully been uploaded.");
                } else {
                    ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                }
            }
        };
    });
});