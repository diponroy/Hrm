﻿
function EmployeeViewModel() {
    
    /*fields*/
    var self = this;
    self.loginName = ko.observable().extend({
        required: true,
        maxLength: 50,
    });
    self.password = ko.observable().extend({
        required: true,
        maxLength: 50,
    });

    self.postObject = function() {
        var obj = {
            LoginName: self.loginName(),
            Password: self.password()
        };
        return {
            login: obj
        };
    };
    
    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };
    

    /**/
    self.TryToLogin = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeLogin/TryToCreateSession',
            self.postObject(),
            function(data) {
                if (data === true) {
                    ToastSuccess('logged in successfully');
                    self.reset();
                    window.location = '/Dashboard/Index';
                } else {
                    ToastError('login information not found. Try again.');
                }

            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*resets*/
    self.reset = function() {
        self.loginName('Admin');
        self.password('123');
        self.removeErrors();
    };
    self.init = function() {
        self.reset();
    };
}

$(document).ready(function() {
    var vm = new EmployeeViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});