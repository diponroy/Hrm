﻿
define('vm.employee.achievement.create',
    ['jquery', 'ko', 'hrm', 'jax'],
    function($, ko, htm, jax) {

        ko.validation.rules['titleUsed'] = {
            validator: function(val, otherVal) {
                var isUsed;
                var json = JSON.stringify({ title: val });
                $.when(
                    $.ajax({
                        url: '/EmployeeAchievementManage/IsTitleUsed',
                        dataType: "json",
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        data: json,
                        async: false,
                    })
                ).then(function(data, textStatus, jqXhr) {
                    isUsed = (textStatus === 'success') ? data : null;
                    if (textStatus != 'success') {
                        ToastError(jqXhr);
                    }
                });
                return isUsed === otherVal;
            },
            message: 'This title already exist.'
        };
        ko.validation.registerExtenders();


        var

            /* basic */
            employeeId = ko.observable(),
            employeeName = ko.observable(),

            /*properties*/
            title = ko.observable().extend({ required: true, maxLength: 50, titleUsed: false }),
            achievementRemarks = ko.observable().extend({ maxLength: 150 }),
            achievementDirectory = ko.observable().extend({ maxLength: 250 }),
            achievementStatus = ko.observable().extend({ required: true }),
            employeeAppraisalId = ko.observable().extend({ required: true }),

            allAchievementStatuses = ko.observableArray([]),
            allAppraisals = ko.observableArray([]),


            /*errors*/
            errors = ko.validation.group([title, achievementRemarks, achievementDirectory, achievementStatus, employeeAppraisalId]),
            hasErrors = function() { return (errors().length > 0) ? true : false; },
            showErrors = function() { errors.showAllMessages(); },
            removeErrors = function() { errors.showAllMessages(false); },


            /* actions */
            getEmployee = function() {
                jax.postJsonBlock(
                    '/EmployeeManage/Get/' + employeeId(),
                    null,
                    function(data) {
                        employeeName(data.FullName);
                    },
                    function(qXhr, textStatus, error) {
                        ToastError("Employee Name could not be retrieved !!");
                    }
                );
            },

            getAppraisals = function() {
                jax.getJsonBlock(
                    '/EmployeeAchievementManage/GetAppraisals',
                    function(data) {
                        var array = [];
                        $.each(data, function(index) {
                            var obj = data[index];
                            array.push({ text: obj.Title, value: obj.Id });
                        });
                        allAppraisals(array);
                    },
                    function(qXhr, textStatus, error) {
                        ToastError(error);
                    }
                );
            },

            getEmployeeAchievementDetail = function() {
                var empAchievementObj = {
                    Title: title(),
                    Remarks: achievementRemarks(),
                    EmployeeId: employeeId(),
                    EmployeeAppraisalId: employeeAppraisalId(),
                    AttachmentDirectory: achievementDirectory(),
                    Status: achievementStatus()
                };

                return empAchievementObj;
            },

            create = function() {
                if (hasErrors()) {
                    showErrors();
                    return;
                }

                jax.postJsonBlock(
                    '/EmployeeAchievementManage/Create',
                    getEmployeeAchievementDetail(),
                    function() {
                        ToastSuccess('Employee Achievement is saved successfully');
                        reset();
                    },
                    function(qXhr, textStatus, error) {
                        ToastError(error);
                        showErrors();
                    }
                );
            },


            /*resets*/
            reset = function() {
                title('');
                achievementRemarks('');
                achievementDirectory('');
                achievementStatus('');
                employeeAppraisalId('');

                removeErrors();
            },

            init = function() {
                $('#linkage4').click(function() {
                    getEmployee();
                    getAppraisals();
                    reset();
                });
                
                var achievementStatus = [];
                achievementStatus.push({ name: 'Active' });
                achievementStatus.push({ name: 'Inactive' });
                allAchievementStatuses(achievementStatus);

                $('#uploadAchievementFile').click(function() {
                    var formdata = new FormData();
                    var fileInput = document.getElementById('fileInput');

                    for (i = 0; i < fileInput.files.length; i++) {
                        formdata.append(fileInput.files[i].name, fileInput.files[i]);
                    }

                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', '/EmployeeAchievementManage/Upload');
                    xhr.send(formdata);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                                educationDirectory(xhr.responseText);
                                ToastSuccess("Attachment has successfully been uploaded.");
                            } else {
                                ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                            }
                        }
                    };

                    return false;
                });
            };


        return {
            allAchievementStatuses: allAchievementStatuses,
            allAppraisals: allAppraisals,

            employeeId: employeeId,
            employeeName: employeeName,
            employeeAppraisalId: employeeAppraisalId,

            title: title,
            achievementRemarks: achievementRemarks,
            achievementDirectory: achievementDirectory,
            achievementStatus: achievementStatus,

            create: create,
            reset: reset,
            init: init
        };
    });