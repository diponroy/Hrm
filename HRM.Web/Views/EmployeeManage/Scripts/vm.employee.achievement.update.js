﻿
define('vm.employee.achievement.update',
    ['jquery', 'ko', 'hrm', 'jax'],
    function ($, ko, htm, jax) {

        ko.validation.rules['titleUsedExceptItself'] = {
            validator: function (val, otherVal) {
                var isUsed;
                var json = JSON.stringify({ id: id(), title: val });
                $.when(
                    $.ajax({
                        url: '/EmployeeAchievementManage/TitleUsedExceptItself',
                        dataType: "json",
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        data: json,
                        async: false,
                    })
                ).then(function (data, textStatus, jqXhr) {
                    if (textStatus === 'success') {
                        isUsed = data;
                    } else {
                        ToastError(xhr);
                        isUsed = null;
                    }
                });
                return isUsed === otherVal;
            },
            message: 'This Title is already in use.'
        };
        ko.validation.registerExtenders();


        var

            /* basic */
            employeeId = ko.observable(),
            employeeName = ko.observable(),

            /*properties*/
            id = ko.observable(),
            title = ko.observable().extend({ required: true, maxLength: 50, titleUsed: false }),
            achievementRemarks = ko.observable().extend({ maxLength: 150 }),
            achievementDirectory = ko.observable().extend({ maxLength: 250 }),
            achievementStatus = ko.observable().extend({ required: true }),
            employeeAppraisalId = ko.observable().extend({ required: true }),

            allAchievementStatuses = ko.observableArray([]),
            allAppraisals = ko.observableArray([]),
            allEmployeeAchievements = ko.observableArray([]),


            /*errors*/
            errors = ko.validation.group([title, achievementRemarks, achievementDirectory, achievementStatus, employeeAppraisalId]),
            hasErrors = function () { return (errors().length > 0) ? true : false; },
            showErrors = function () { errors.showAllMessages(); },
            removeErrors = function () { errors.showAllMessages(false); },


            /* actions */
            getEmployee = function () {
                jax.postJsonBlock(
                    '/EmployeeManage/Get/' + employeeId(),
                    null,
                    function (data) {
                        employeeName(data.FullName);
                    },
                    function (qXhr, textStatus, error) {
                        ToastError(error);
                    }
                );

                getEmployeeEducations();
            },

            getAppraisals = function () {
                jax.getJsonBlock(
                    '/EmployeeAchievementManage/GetAppraisals',
                    function (data) {
                        var array = [];
                        $.each(data, function (index) {
                            var obj = data[index];
                            array.push({ text: obj.Title, value: obj.Id });
                        });
                        allAppraisals(array);
                    },
                    function (qXhr, textStatus, error) {
                        ToastError(error);
                    }
                );
            },

            getEmployeeAchievement = function (item) {
                jax.getJsonBlock(
                   '/EmployeeAchievementManage/Get/' + item.id(),
                    function (data) {
                        title(data.Title);
                        achievementRemarks(data.Remarks);
                        achievementDirectory(data.AttachmentDirectory);
                        employeeAppraisalId(data.EmployeeAppraisalId);
                        achievementStatus(data.StatusString);
                        removeErrors();
                    },
                    function (qXhr, textStatus, error) {
                        ToastError(error);
                    }
                );
            },

            getEmployeeAchievements = function () {
                allEmployeeAchievements([]);

                jax.postJsonBlock(
                    '/EmployeeAchievementManage/GetEducations/' + employeeId(),
                    null,
                    function (data) {
                        var arr = new Array();
                        $.each(data, function (index) {
                            var anAchievement = data[index];
                            arr.push({
                                id: anAchievement.Id,
                                title: anAchievement.Title,
                                achievementRemarks: anAchievement.Remarks,
                                achievementDirectory: anAchievement.AttachmentDirectory,
                                employeeAppraisalId: anAchievement.EmployeeAppraisalId,
                                achievementStatus: anAchievement.StatusString,

                                viewId: 'divView' + anAchievement.Id,
                                updateId: 'divUpdate' + anAchievement.Id,
                                uploadId: 'divUpload' + anAchievement.Id,
                                titleId: 'spanTitle' + anAchievement.Id,
                                appraisalId: 'spanMajor' + anAchievement.Id,
                                remarksId: 'spanInstitution' + anAchievement.Id
                            });
                        });
                        allEmployeeAchievements(arr);
                    },
                    function (qXhr, textStatus, error) {
                        ToastError(error);
                    }
                );
            },

            getEmployeeAchievementDetail = function () {
                var empAchievementObj = {
                    Id: id(),
                    Title: title(),
                    Remarks: achievementRemarks(),
                    AttachmentDirectory: achievementDirectory(),
                    EmployeeAppraisalId: employeeAppraisalId(),
                    EmployeeId: employeeId(),
                    Status: achievementStatus()
                };

                return {
                    model: empAchievementObj
                };
            },

            showToUpdate = function (item) {
                $("[id^='divUpdate']").each(function () {
                    if ($(this).is(":visible")) {
                        $(this).fadeOut().hide();
                        $(this).prev("[id^='divView']:first").fadeIn().show();
                    }
                    if ($('#educationStack').is(":visible")) {
                        $('#educationStack').slideUp('slow');
                        $('#btnAchievementStack').hide().slideDown('slow');
                    }
                });

                $('#' + item.viewId).slideUp('slow');
                $('#' + item.updateId).hide().slideDown('slow');

                getEmployeeAchievement(item);
            },

            upload = function (item) {
                var formdata = new FormData();
                var inputFile;
                if (item != null) {

                    alert($('#' + item.uploadId));
                    inputFile = $('#' + item.uploadId);

                    for (i = 0; i < inputFile.files.length; i++) {
                        formdata.append(inputFile.files[i].name, inputFile.files[i]);
                    }

                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', '/EmployeeAchievementManage/Upload');
                    xhr.send(formdata);
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                                educationDirectory(xhr.responseText);
                                ToastSuccess("Image has successfully been uploaded.");
                            } else {
                                ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                            }
                        }
                    };
                }

                return false;
            },

            create = function () {
                if (hasErrors()) {
                    showErrors();
                    return;
                }

                jax.postJsonBlock(
                    '/EmployeeAchievementManage/Create',
                    getEmployeeAchievementDetail(),
                    function () {
                        ToastSuccess('Achievement information is saved successfully');
                        resetCreate();
                        cancelCreate();
                        getEmployeeAchievements();
                    },
                    function (qXhr, textStatus, error) {
                        ToastError(error);
                        showErrors();
                    }
                );
            },

            update = function (item) {
                if (self.hasErrors()) {
                    self.showErrors();
                    return;
                }
                jax.postJsonBlock(
                    '/EmployeeAchievementManage/Update',
                    getEmployeeAchievementDetail(),
                    function () {
                        ToastSuccess('Achievement information is updated successfully');
                        self.load();
                    },
                    function (qXhr, textStatus, error) {
                        ToastError(error);
                    }
                );
            },


            /*resets*/
            resetCreate = function () {
                title('');
                achievementRemarks('');
                achievementDirectory('');
                employeeAppraisalId('');
                achievementStatus('');
                removeErrors();
            },

            resetUpdate = function (item) {
                getEmployeeAchievement(item);
            },

            resetStack = function () {
                resetCreate();
                resetUpdate();

                $('#btnAchievementStack').css('display', 'block');
                $('#educationStack').css('display', 'none');
            },

            cancelCreate = function () {
                $('#btnAchievementStack').hide().slideDown('slow');
                $('#educationStack').slideUp('slow');
            },

            cancelUpdate = function (item) {
                $('#' + item.viewId).fadeIn().show();
                $('#' + item.updateId).fadeOut().hide();
            },

            init = function (ID) {
                employeeId(ID);
                $('#linkage4').click(function () {
                    getEmployee();
                    getAppraisals();
                    getEmployeeAchievements();
                    resetStack();
                });

                var achievementStatus = [];
                achievementStatus.push({ name: 'Active' });
                achievementStatus.push({ name: 'Inactive' });
                allAchievementStatuses(achievementStatus);

                $('#btnAddAchievement').click(function () {
                    $("[id^='divUpdate']").each(function () {
                        if ($(this).is(":visible")) {
                            $(this).fadeOut().hide();
                            $(this).prev("[id^='divView']:first").fadeIn().show();
                        }
                    });
                    resetCreate();
                    $('#educationStack').hide().slideDown('slow');
                    $('#btnAchievementStack').slideUp('slow');
                });

                $('#uploadEducationImage').click(function () {
                    var formdata = new FormData();
                    var fileInput = document.getElementById('fileInput');

                    for (i = 0; i < fileInput.files.length; i++) {
                        formdata.append(fileInput.files[i].name, fileInput.files[i]);
                    }

                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', '/EmployeeAchievementManage/Upload');
                    xhr.send(formdata);
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                                educationDirectory(xhr.responseText);
                                ToastSuccess("Attachment has successfully been uploaded.");
                            } else {
                                ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                            }
                        }
                    };

                    return false;
                });
            };


        return {
            allAchievementStatuses: allAchievementStatuses,
            allAppraisals: allAppraisals,

            employeeId: employeeId,
            employeeName: employeeName,
            employeeAppraisalId: employeeAppraisalId,

            title: title,
            achievementRemarks: achievementRemarks,
            achievementDirectory: achievementDirectory,
            achievementStatus: achievementStatus,

            showToUpdate: showToUpdate,
            upload: upload,
            create: create,
            update: update,
            resetCreate: resetCreate,
            resetUpdate: resetUpdate,
            cancelCreate: cancelCreate,
            cancelUpdate: cancelUpdate,
            init: init
        };
    })
