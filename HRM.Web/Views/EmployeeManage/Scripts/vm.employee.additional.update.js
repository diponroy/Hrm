﻿define('vm.employee.additional.update',
    ['jquery', 'ko', 'hrm', 'jax'],
    function ($, ko, htm, jax) {
        var

        /* basic */
        employeeId = ko.observable(),
        employeeName = ko.observable(),

        /* additional */
        fathersName = ko.observable().extend({ maxLength: 50 }),
        mothersName = ko.observable().extend({ maxLength: 50 }),
        bloodGroup = ko.observable(),
        officePhone = ko.observable().extend({ maxLength: 50 }),
        homePhone = ko.observable().extend({ maxLength: 50 }),
        permanentAddress = ko.observable().extend({ maxLength: 250 }),
        city = ko.observable().extend({ maxLength: 50 }),
        imageDirectory = ko.observable().extend({ maxLength: 250 }),

        allBloodGroups = ko.observableArray([]),


        /*errors*/
        errors = ko.validation.group([fathersName, mothersName, bloodGroup, officePhone, homePhone, permanentAddress, city, imageDirectory]),
        hasErrors = function () { return (errors().length > 0) ? true : false; },
        showErrors = function () { errors.showAllMessages(); },
        removeErrors = function () { errors.showAllMessages(false); },


        getEmployee = function () {
            jax.postJsonBlock(
                '/EmployeeManage/Get/' + employeeId(),
                null,
                function (data) {
                    employeeName(data.FullName);
                    fathersName(data.FathersName);
                    mothersName(data.MothersName);
                    bloodGroup(data.BloodGroup);
                    officePhone(data.OfficePhone);
                    homePhone(data.HomePhone);
                    permanentAddress(data.PermanentAddress);
                    city(data.City);
                    imageDirectory(data.ImageDirectory);
                },
                function (qXhr, textStatus, error) {
                    ToastError('Employee ID couldn\'t be retrieved !!');
                }
            );
        },

        getEmployeeAdditionalDetail = function () {
            var employeeObj = {
                FathersName: fathersName(),
                MothersName: mothersName(),
                BloodGroup: bloodGroup(),
                ImageDirectory: imageDirectory(),
                HomePhone: homePhone(),
                OfficePhone: officePhone(),
                PermanentAddress: permanentAddress(),
                City: city(),
            };

            return {
                id: employeeId(),
                employeeAdditional: employeeObj
            };
        },


        /* actions */
        update = function () {
            if (hasErrors()) {
                showErrors();
                return;
            }

            jax.postJsonBlock(
                '/EmployeeManage/UpdateAdditional',
                getEmployeeAdditionalDetail(),
                function (data) {
                    ToastSuccess('Employee Additional Info is created successfully');
                    getEmployee();
                },
                function (qXhr, textStatus, error) {
                    ToastError(error);
                    showErrors();
                }
            );
        },

        reset = function () {
            fathersName('');
            mothersName('');
            bloodGroup('');
            imageDirectory('');
            officePhone('');
            homePhone('');
            permanentAddress('');
            city('');

            removeErrors();
        },

        init = function (ID) {
            employeeId(ID);
            getEmployee();

            var bloodGroup = [];
            bloodGroup.push({ name: 'A+' });
            bloodGroup.push({ name: 'A-' });
            bloodGroup.push({ name: 'B+' });
            bloodGroup.push({ name: 'B-' });
            bloodGroup.push({ name: 'AB+' });
            bloodGroup.push({ name: 'AB-' });
            bloodGroup.push({ name: 'O+' });
            bloodGroup.push({ name: 'O-' });
            allBloodGroups(bloodGroup);


            $('#uploadBasicImage').click(function () {
                var formdata = new FormData();
                var fileInput = document.getElementById('fileInput');

                for (i = 0; i < fileInput.files.length; i++) {
                    formdata.append(fileInput.files[i].name, fileInput.files[i]);
                }

                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/EmployeeManage/Upload');
                xhr.send(formdata);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                            imageDirectory(xhr.responseText);
                            ToastSuccess("Image has successfully been uploaded.");
                        } else {
                            ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                        }
                    }
                };

                return false;
            });
        };


        return {
            allBloodGroups: allBloodGroups,

            employeeId: employeeId,
            employeeName: employeeName,

            fathersName: fathersName,
            mothersName: mothersName,
            bloodGroup: bloodGroup,
            officePhone: officePhone,
            homePhone: homePhone,
            permanentAddress: permanentAddress,
            city: city,
            imageDirectory: imageDirectory,

            update: update,
            reset: reset,
            init: init
        };
    });