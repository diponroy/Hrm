﻿
define('vm.employee.basic.create',
    ['jquery', 'ko', 'hrm', 'jax'],
    function ($, ko, htm, jax) {

        var createdCallBack = null,
            setCreatedCallBack = function(callback) {
                createdCallBack = callback;
            };


        /*Validations*/
        ko.validation.rules['emailUsed'] = {
            validator: function(val, otherVal) {
                var isUsed;
                var json = JSON.stringify({ email: val });
                $.when(
                    $.ajax({
                        url: '/EmployeeManage/IsEmailUsed',
                        dataType: "json",
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        data: json,
                        async: false,
                    })
                ).then(function(data, textStatus, jqXhr) {
                    isUsed = (textStatus === 'success') ? data : null;
                });
                return isUsed === otherVal;
            },
            message: '  This email address is already in use!!'
        };
        ko.validation.registerExtenders();

        var

        /*ddl or additionals*/
        allSalutations = ko.observableArray([]),
        allGenders = ko.observableArray([]),
        allDivisions = ko.observableArray([]),
        allEmployeeStatuses = ko.observableArray([]),

        /*properties*/
        trackNo = ko.observable().extend({ required: true }),
        salutation = ko.observable().extend({ required: true }),
        firstName = ko.observable().extend({ required: true, maxLength: 50 }),
        lastName = ko.observable().extend({ maxLength: 50 }),
        dateOfBirth = ko.observable(currentDate()).extend({ required: true }),
        gender = ko.observable().extend({ required: true }),
        email = ko.observable().extend({ required: true, maxLength: 50, email: true, emailUsed: false }),
        contact = ko.observable().extend({ required: true, maxLength: 50, digit: true }),
        presentAddress = ko.observable().extend({ required: true, maxLength: 250 }),
        division = ko.observable().extend({ required: true, maxLength: 250 }),
        statusEmployee = ko.observable().extend({ required: true }),

        /*errors*/
        errors = ko.validation.group([trackNo, salutation, firstName, lastName, dateOfBirth, gender, email, contact, presentAddress, division, statusEmployee]),
        hasErrors = function() { return (errors().length > 0) ? true : false; },
        showErrors = function() { errors.showAllMessages(); },
        removeErrors = function() { errors.showAllMessages(false); },

        /*post object*/
        getEmployeeBasicDetail = function() {
            var employeeObj = {
                TrackNo: trackNo(),
                Salutation: salutation(),
                FirstName: firstName(),
                LastName: lastName(),
                DateOfBirth: dateOfBirth(),
                Gender: gender(),
                Email: email(),
                ContactNo: contact(),
                PresentAddress: presentAddress(),
                DivisionOrState: division(),
                Status: statusEmployee()
            };

            return {
                employeeBasic: employeeObj
            };
        },


        /* actions */
        generateCode = function() {
            $.ajax({
                url: '/EmployeeManage/GenerateTrackNo',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                async: true,
                processData: false,
                cache: false,
                success: function(data) {
                    trackNo(data);
                },
                error: function(xhr) {
                    ToastError(xhr);
                }
            });
            return true;
        },

        create = function() {
            if (hasErrors()) {
                showErrors();
                return;
            }

            jax.postJsonBlock(
                '/EmployeeManage/Create',
                getEmployeeBasicDetail(),
                function(data) {
                    ToastSuccess('Employee is created successfully');
                    getIdByTrackNo();
                    reset();
                },
                function(qXhr, textStatus, error) {
                    ToastError(error);
                    showErrors();
                }
            );
        },

        getIdByTrackNo = function() {
            jax.postJsonBlock(
                '/EmployeeManage/GetByTrackNo' ,
                {track: trackNo()},
                function(data) {
                    if (createdCallBack != null) {
                        createdCallBack(data.Id);
                    }   
                },
                function(qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        },

        /*resets*/
        reset = function () {
            salutation('');
            firstName('');
            lastName('');
            dateOfBirth('');
            dateOfBirth(currentDate());
            gender('');
            email('');
            contact('');
            presentAddress('');
            division('');
            statusEmployee('');


            removeErrors();
        },

        init = function() {

            var salutation = [];
            salutation.push({ name: 'Mr.' });
            salutation.push({ name: 'Mrs.' });
            allSalutations(salutation);

            var gender = [];
            gender.push({ name: 'Male' });
            gender.push({ name: 'Female' });
            allGenders(gender);

            var division = [];
            division.push({ name: 'Dhaka' });
            division.push({ name: 'Rajshahi' });
            division.push({ name: 'Rangpur' });
            division.push({ name: 'Sylhet' });
            division.push({ name: 'Barisal' });
            division.push({ name: 'Chittagong' });
            division.push({ name: 'Khulna' });
            allDivisions(division);

            var statusEmp = [];
            statusEmp.push({ name: 'Active' });
            statusEmp.push({ name: 'Inactive' });
            allEmployeeStatuses(statusEmp);

            $('#btnShowDatePickerCreation').click(function() {
                $('#btnDateOfCreation').focus();
            });

            $('#btnResetDatePickerCreation').click(function() {
                dateOfBirth(currentDate());
            });

            $('#btnAddBasic').click(function () {
                window.location = '/EmployeeManage/CreateProfile';
            });
            $('#btnAddAdditional').click(function () {
                window.location = '/EmployeeAdditionalManage/Create/' + 2;
            });
            $('#btnAddEducation').click(function () {
                window.location = '/EmployeeEducationManage/Create/' + 2;
            });
            $('#btnAddAchievement').click(function () {
                window.location = '/EmployeeAchievementManage/Create/' + 2;
            });
        };

        return {
            allSalutations: allSalutations,
            allGenders: allGenders,
            allDivisions: allDivisions,
            allEmployeeStatuses: allEmployeeStatuses,

            trackNo: trackNo,
            salutation: salutation,
            firstName: firstName,
            lastName: lastName,
            dateOfBirth: dateOfBirth,
            gender: gender,
            email: email,
            contact: contact,
            presentAddress: presentAddress,
            division: division,
            statusEmployee: statusEmployee,

            generateCode: generateCode,
            create: create,
            reset: reset,
            init: init,

            //callbacks
            setCreatedCallBack: setCreatedCallBack
        };
    });
