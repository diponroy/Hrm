﻿
define('vm.employee.basic.update',
    ['jquery', 'ko', 'hrm', 'jax'],
    function($, ko, htm, jax) {

        /*Validations*/
        ko.validation.rules['emailUsed'] = {
            validator: function(val, otherVal) {
                var isUsed;
                var json = JSON.stringify({ id: id(), email: val });
                $.when(
                    $.ajax({
                        url: '/EmployeeManage/IsEmailUsedExceptEmployee',
                        dataType: "json",
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        data: json,
                        async: false,
                    })
                ).then(function(data, textStatus, jqXhr) {
                    isUsed = (textStatus === 'success') ? data : null;
                });
                return isUsed === otherVal;
            },
            message: '  This email address is already in use!!'
        };
        ko.validation.registerExtenders();

        var

            /*ddl or additionals*/
            allSalutations = ko.observableArray([]),
            allGenders = ko.observableArray([]),
            allDivisions = ko.observableArray([]),
            allEmployeeStatuses = ko.observableArray([]),

            /*properties*/
            id = ko.observable(),
            trackNo = ko.observable().extend({ required: true }),
            salutation = ko.observable().extend({ required: true }),
            firstName = ko.observable().extend({ required: true, maxLength: 50 }),
            lastName = ko.observable().extend({ maxLength: 50 }),
            dateOfBirth = ko.observable(currentDate()).extend({ required: true }),
            gender = ko.observable().extend({ required: true }),
            email = ko.observable().extend({ required: true, maxLength: 50, email: true, emailUsed: false }),
            contact = ko.observable().extend({ required: true, maxLength: 50, digit: true }),
            presentAddress = ko.observable().extend({ required: true, maxLength: 250 }),
            division = ko.observable().extend({ required: true, maxLength: 250 }),
            status = ko.observable().extend({ required: true }),

            /*errors*/
            errors = ko.validation.group([trackNo, salutation, firstName, lastName, dateOfBirth, gender, email, contact, presentAddress, division, status]),
            hasErrors = function() { return (errors().length > 0) ? true : false; },
            showErrors = function() { errors.showAllMessages(); },
            removeErrors = function() { errors.showAllMessages(false); },


            /*post object*/
            getEmployeeBasicDetail = function() {
                var employeeObj = {
                    Id: id(),
                    TrackNo: trackNo(),
                    Salutation: salutation(),
                    FirstName: firstName(),
                    LastName: lastName(),
                    DateOfBirth: dateOfBirth(),
                    Gender: gender(),
                    Email: email(),
                    ContactNo: contact(),
                    PresentAddress: presentAddress(),
                    DivisionOrState: division(),
                    Status: status()
                };

                return {
                    employeeBasic: employeeObj
                };
            },

            load = function () {
                jax.getJson(
                    '/EmployeeManage/Get/' + id(),
                    function(data) {
                        trackNo(data.TrackNo);
                        salutation(data.Salutation),
                        firstName(data.FirstName);
                        lastName(data.LastName);
                        dateOfBirth(clientDate(data.DateOfBirth));
                        gender(data.Gender);
                        email(data.Email);
                        contact(data.ContactNo),
                        presentAddress(data.PresentAddress);
                        division(data.DivisionOrState);
                        status(data.StatusString);
                        removeErrors();
                    },
                    function(qXhr, textStatus, error) {
                        ToastError("Error while loading the Employee !!");
                    }
                );
            },

            update = function() {
                if (hasErrors()) {
                    showErrors();
                    return;
                }

                jax.postJsonBlock(
                    '/EmployeeManage/Update',
                    getEmployeeBasicDetail(),
                    function(data) {
                        ToastSuccess('Employee\'s basic info is updated successfully');
                        load();
                    },
                    function(qXhr, textStatus, error) {
                        ToastError(error);
                        showErrors();
                    }
                );
            },

            reset = function() {
                load();
            },

            init = function (ID) {
                id(ID);

                var salutation = [];
                salutation.push({ name: 'Mr.' });
                salutation.push({ name: 'Mrs.' });
                allSalutations(salutation);

                var gender = [];
                gender.push({ name: 'Male' });
                gender.push({ name: 'Female' });
                allGenders(gender);

                var division = [];
                division.push({ name: 'Dhaka' });
                division.push({ name: 'Rajshahi' });
                division.push({ name: 'Rangpur' });
                division.push({ name: 'Sylhet' });
                division.push({ name: 'Barisal' });
                division.push({ name: 'Chittagong' });
                division.push({ name: 'Khulna' });
                allDivisions(division);

                var statusEmp = [];
                statusEmp.push({ name: 'Active' });
                statusEmp.push({ name: 'Inactive' });
                allEmployeeStatuses(statusEmp);

                $('#btnShowDatePickerCreation').click(function() {
                    $('#btnDateOfCreation').focus();
                });

                $('#btnResetDatePickerCreation').click(function() {
                    dateOfBirth(currentDate());
                });

                load();
            };


        return {
            allSalutations: allSalutations,
            allGenders: allGenders,
            allDivisions: allDivisions,
            allEmployeeStatuses: allEmployeeStatuses,

            trackNo: trackNo,
            salutation: salutation,
            firstName: firstName,
            lastName: lastName,
            dateOfBirth: dateOfBirth,
            gender: gender,
            email: email,
            contact: contact,
            presentAddress: presentAddress,
            division: division,
            status: status,

            update: update,
            reset: reset,
            init: init
        };
    });
