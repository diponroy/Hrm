﻿
define('vm.employee.education.create',
    ['jquery', 'ko', 'hrm', 'jax'],
    function($, ko, htm, jax) {
        var

            /* basic */
            employeeId = ko.observable(),
            employeeName = ko.observable(),

            /*properties*/
            degreeTitle = ko.observable().extend({ required: true, maxLength: 50 }),
            institution = ko.observable().extend({ required: true, maxLength: 100 }),
            durationFromDate = ko.observable(currentDate()).extend({ required: true }),
            durationToDate = ko.observable(currentDate()).extend({ required: true }),
            result = ko.observable().extend({ required: true, number: true, maxLength: 20 }),
            major = ko.observable().extend({ required: true, maxLength: 50 }),
            educationRemarks = ko.observable().extend({ maxLength: 150 }),
            educationDirectory = ko.observable().extend({ maxLength: 250 }),
            educationStatus = ko.observable().extend({ required: true }),

            allEducationStatuses = ko.observableArray([]),


            /*errors*/
            errors = ko.validation.group([degreeTitle, institution, durationFromDate, durationToDate, result, major, educationRemarks, educationDirectory, educationStatus]),
            hasErrors = function() { return (errors().length > 0) ? true : false; },
            showErrors = function() { errors.showAllMessages(); },
            removeErrors = function() { errors.showAllMessages(false); },


            /* actions */
            getEmployee = function() {
                jax.postJsonBlock(
                    '/EmployeeManage/Get/' + employeeId(),
                    null,
                    function(data) {
                        employeeName(data.FullName);
                    },
                    function(qXhr, textStatus, error) {
                        ToastError("Employee Name couldn't be retrieved !!");
                    }
                );
            },

            getEmployeeEducationDetail = function() {
                var empEducationObj = {
                    Title: degreeTitle(),
                    Institution: institution(),
                    DurationFromDate: durationFromDate(),
                    DurationToDate: durationToDate(),
                    Result: result(),
                    Major: major(),
                    Remarks: educationRemarks(),
                    AttachmentDirectory: educationDirectory(),
                    EmployeeId: employeeId(),
                    Status: educationStatus()
                };

                return empEducationObj;
            },

            create = function() {
                if (hasErrors()) {
                    showErrors();
                    return;
                }
                $('#btnEducationStack').hide().slideDown('slow');
                $('#educationStack').slideUp('slow');
                

                jax.postJsonBlock(
                    '/EmployeeEducationManage/Create',
                    getEmployeeEducationDetail(),
                    function() {
                        ToastSuccess('Education information saved successfully');
                        reset();
                        $('#educationStack').slideUp('slow');
                        $('#btnEducationStack').hide().slideDown('slow');
                    },
                    function(qXhr, textStatus, error) {
                        ToastError(error);
                        showErrors();
                    }
                );
            },


            /*resets*/
            reset = function() {
                degreeTitle('');
                institution('');
                durationFromDate(currentDate());
                durationToDate(currentDate());
                result('');
                major('');
                educationRemarks('');
                educationDirectory('');
                educationStatus('');
                removeErrors();
            },

            cancel = function() {
                $('#educationStack').slideUp('slow');
                $('#btnEducationStack').hide().slideDown('slow');
            },

            init = function () {
                $('#linkage3').click(function() {
                    getEmployee();
                });

                var educationStatus = [];
                educationStatus.push({ name: 'Active' });
                educationStatus.push({ name: 'Inactive' });
                allEducationStatuses(educationStatus);

                $('#btnShowFromDate').click(function() {
                    $('#btnFromDate').focus();
                });
                $('#btnResetFromDate').click(function() {
                    durationFromDate(currentDate());
                });

                $('#btnShowToDate').click(function() {
                    $('#btnToDate').focus();
                });
                $('#btnResetToDate').click(function() {
                    durationToDate(currentDate());
                });

                $('#btnAddEducation').click(function () {
                    $('#educationStack').hide().slideDown('slow');
                    $('#btnEducationStack').slideUp('slow');
                });

                $('#uploadEducationImage').click(function() {
                    var formdata = new FormData();
                    var fileInput = document.getElementById('fileInput');

                    for (i = 0; i < fileInput.files.length; i++) {
                        formdata.append(fileInput.files[i].name, fileInput.files[i]);
                    }

                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', '/EmployeeEducationManage/Upload');
                    xhr.send(formdata);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                                educationDirectory(xhr.responseText);
                                ToastSuccess("Image has successfully been uploaded.");
                            } else {
                                ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                            }
                        }
                    };

                    return false;
                });  
            };


        return {
            allEducationStatuses: allEducationStatuses,

            employeeId: employeeId,
            employeeName: employeeName,

            degreeTitle: degreeTitle,
            institution: institution,
            durationFromDate: durationFromDate,
            durationToDate: durationToDate,
            result: result,
            major: major,
            educationRemarks: educationRemarks,
            educationDirectory: educationDirectory,
            educationStatus: educationStatus,

            create: create,
            reset: reset,
            cancel: cancel,
            init: init
        };
    });