﻿
define('vm.employee.education.update',
    ['jquery', 'ko', 'hrm', 'jax'],
    function($, ko, htm, jax) {
        var

            /* basic */
            employeeId = ko.observable(),
            employeeName = ko.observable(),

            /*properties*/
            id = ko.observable(),
            degreeTitle = ko.observable().extend({ required: true, maxLength: 50 }),
            institution = ko.observable().extend({ required: true, maxLength: 100 }),
            durationFromDate = ko.observable(currentDate()).extend({ required: true }),
            durationToDate = ko.observable(currentDate()).extend({ required: true }),
            result = ko.observable().extend({ required: true, number: true, maxLength: 20 }),
            major = ko.observable().extend({ required: true, maxLength: 50 }),
            educationRemarks = ko.observable().extend({ maxLength: 150 }),
            educationDirectory = ko.observable().extend({ maxLength: 250 }),
            educationStatus = ko.observable().extend({ required: true }),

            allEducationStatuses = ko.observableArray([]),
            employeeEducations = ko.observableArray([]),

            /*errors*/
            errors = ko.validation.group([degreeTitle, institution, durationFromDate, durationToDate, result, major, educationRemarks, educationDirectory, educationStatus]),
            hasErrors = function() { return (errors().length > 0) ? true : false; },
            showErrors = function() { errors.showAllMessages(); },
            removeErrors = function() { errors.showAllMessages(false); },


            /* actions */
            getEmployee = function() {
                jax.postJsonBlock(
                    '/EmployeeManage/Get/' + employeeId(),
                    null,
                    function(data) {
                        employeeName(data.FullName);
                    },
                    function(qXhr, textStatus, error) {
                        ToastError(error);
                    }
                );

                getEmployeeEducations();
            },

            getEmployeeEducation = function (item) {
                jax.postJsonBlock(
                    '/EmployeeEducationManage/Get/' + item.id,
                    null,
                    function (data) {
                        id(data.Id);
                        degreeTitle(data.Title);
                        institution(data.Institution);
                        durationFromDate(clientDate(data.DurationFromDate));
                        durationToDate(clientDate(data.DurationToDate));
                        result(data.Result);
                        major(data.Major);
                        educationRemarks(data.Remarks);
                        educationDirectory(data.AttachmentDirectory);
                        educationStatus(data.StatusString);
                    },
                    function (qXhr, textStatus, error) {
                        ToastError(error);
                    }
                );
            },

            getEmployeeEducations = function() {
                employeeEducations([]);

                jax.postJsonBlock(
                    '/EmployeeEducationManage/GetEducations/' + employeeId(),
                    null,
                    function(data) {
                        var arr = new Array();
                        $.each(data, function(index) {
                            var anEmployeeEducation = data[index];
                            arr.push({
                                id: anEmployeeEducation.Id,
                                title: anEmployeeEducation.Title,
                                major: anEmployeeEducation.Major,
                                institution: anEmployeeEducation.Institution,
                                toDate: clientDateStringFromDate(anEmployeeEducation.DurationToDate).substr(6, 9),

                                viewId: 'divView' + anEmployeeEducation.Id,
                                updateId: 'divUpdate' + anEmployeeEducation.Id,
                                uploadId: 'divUpload' + anEmployeeEducation.Id,
                                titleId: 'spanTitle' + anEmployeeEducation.Id,
                                majorId: 'spanMajor' + anEmployeeEducation.Id,
                                institutionId: 'spanInstitution' + anEmployeeEducation.Id,
                                toDateId: 'spanToDate' + anEmployeeEducation.Id

                            });
                        });
                        employeeEducations(arr);
                    },
                    function(qXhr, textStatus, error) {
                        ToastError(error);
                    }
                );
            },

            getEmployeeEducationDetail = function() {
                var empEducationObj = {
                    Id: id(),
                    Title: degreeTitle(),
                    Institution: institution(),
                    DurationFromDate: durationFromDate(),
                    DurationToDate: durationToDate(),
                    Result: result(),
                    Major: major(),
                    Remarks: educationRemarks(),
                    AttachmentDirectory: educationDirectory(),
                    EmployeeId: employeeId(),
                    Status: educationStatus()
                };

                return {
                    employeeEducation: empEducationObj
                };
            },

            showToUpdate = function (item) {
                $("[id^='divUpdate']").each(function () {
                    if ($(this).is(":visible")) {
                        $(this).fadeOut().hide();
                        $(this).prev("[id^='divView']:first").fadeIn().show();
                    }
                    if ($('#educationStack').is(":visible")) {
                        $('#educationStack').slideUp('slow');
                        $('#btnEducationStack').hide().slideDown('slow');
                    }                   
                });

                $('#' + item.viewId).slideUp('slow');
                $('#' + item.updateId).hide().slideDown('slow');

                getEmployeeEducation(item);
            },

            upload = function (item) {
                var formdata = new FormData();
                var inputFile;
                if (item != null) {
                    
                    alert($('#' + item.uploadId));
                    inputFile = $('#' + item.uploadId);

                    for (i = 0; i < inputFile.files.length; i++) {
                        formdata.append(inputFile.files[i].name, inputFile.files[i]);
                    }

                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', '/EmployeeEducationManage/Upload');
                    xhr.send(formdata);
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                                educationDirectory(xhr.responseText);
                                ToastSuccess("Image has successfully been uploaded.");
                            } else {
                                ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                            }
                        }
                    };
                }

                //else {
                //    inputFile = document.getElementById('fileInput');

                //    for (i = 0; i < inputFile.files.length; i++) {
                //        formdata.append(inputFile.files[i].name, inputFile.files[i]);
                //    }

                //    var xhr = new XMLHttpRequest();
                //    xhr.open('POST', '/EmployeeEducationManage/Upload');
                //    xhr.send(formdata);
                //    xhr.onreadystatechange = function () {
                //        if (xhr.readyState == 4 && xhr.status == 200) {
                //            if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                //                educationDirectory(xhr.responseText);
                //                ToastSuccess("Image has successfully been uploaded.");
                //            } else {
                //                ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                //            }
                //        }
                //    };
                //}

                return false;
            },

            create = function() {
                if (hasErrors()) {
                    showErrors();
                    return;
                }

                jax.postJsonBlock(
                    '/EmployeeEducationManage/Create',
                    getEmployeeEducationDetail(),
                    function() {
                        ToastSuccess('Education information saved successfully');
                        resetCreate();
                        cancelCreate();
                        getEmployeeEducations();
                    },
                    function(qXhr, textStatus, error) {
                        ToastError(error);
                        showErrors();
                    }
                );
            },

            update = function(item) {
                if (hasErrors()) {
                    showErrors();
                    return;
                }

                jax.postJsonBlock(
                    '/EmployeeEducationManage/Update',
                    getEmployeeEducationDetail(),
                    function() {
                        ToastSuccess('Education information is updated successfully');
                        getEmployeeEducation(item);
                        $('#' + item.titleId).text(degreeTitle());
                        $('#' + item.majorId).text(major());
                        $('#' + item.institutionId).text(institution());
                        $('#' + item.toDateId).text(clientDateStringFromDate(toDate()));
                    },
                    function(qXhr, textStatus, error) {
                        ToastError(error);
                        showErrors();
                    }
                );
            },


            /*resets*/
            resetCreate = function() {
                degreeTitle('');
                institution('');
                durationFromDate(currentDate());
                durationToDate(currentDate());
                result('');
                major('');
                educationRemarks('');
                educationDirectory('');
                educationStatus('');
                removeErrors();
            },

            resetUpdate = function(item) {
                getEmployeeEducation(item);
            },

            resetStack = function() {
                resetCreate();
                resetUpdate();

                $('#btnEducationStack').css('display', 'block');
                $('#educationStack').css('display', 'none');
            },

            cancelCreate = function () {
                $('#btnEducationStack').hide().slideDown('slow');
                $('#educationStack').slideUp('slow');
            },

            cancelUpdate = function (item) {
                $('#' + item.viewId).fadeIn().show();
                $('#' + item.updateId).fadeOut().hide();

                //$('#' + item.viewId).hide().slideDown('slow');
                //$('#' + item.updateId).slideUp('slow');
            },

            init = function(ID) {
                employeeId(ID);
                $('#linkage3').click(function() {
                    getEmployee();
                    resetStack();
                });

                var educationStatus = [];
                educationStatus.push({ name: 'Active' });
                educationStatus.push({ name: 'Inactive' });
                allEducationStatuses(educationStatus);

                $('#btnShowFromDate').click(function() {
                    $('#btnFromDate').focus();
                });
                $('#btnResetFromDate').click(function() {
                    durationFromDate(currentDate());
                });

                $('#btnShowToDate').click(function() {
                    $('#btnToDate').focus();
                });
                $('#btnResetToDate').click(function() {
                    durationToDate(currentDate());
                });

                $('#btnAddEducation').click(function () {
                    $("[id^='divUpdate']").each(function () {
                        if ($(this).is(":visible")) {
                            $(this).fadeOut().hide();
                            $(this).prev("[id^='divView']:first").fadeIn().show();
                        }
                    });
                    resetCreate();
                    $('#educationStack').hide().slideDown('slow');
                    $('#btnEducationStack').slideUp('slow');
                });

                $('#uploadEducationImage').click(function () {
                    var formdata = new FormData();
                    var fileInput = document.getElementById('fileInput');

                    for (i = 0; i < fileInput.files.length; i++) {
                        formdata.append(fileInput.files[i].name, fileInput.files[i]);
                    }

                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', '/EmployeeEducationManage/Upload');
                    xhr.send(formdata);
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                                educationDirectory(xhr.responseText);
                                ToastSuccess("Image has successfully been uploaded.");
                            } else {
                                ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                            }
                        }
                    };

                    return false;
                });
            };


        return {
            allEducationStatuses: allEducationStatuses,
            employeeEducations:employeeEducations,

            employeeId: employeeId,
            employeeName: employeeName,

            degreeTitle: degreeTitle,
            institution: institution,
            durationFromDate: durationFromDate,
            durationToDate: durationToDate,
            result: result,
            major: major,
            educationRemarks: educationRemarks,
            educationDirectory: educationDirectory,
            educationStatus: educationStatus,

            showToUpdate: showToUpdate,
            upload: upload,
            create: create,
            update: update,
            resetCreate: resetCreate,
            resetUpdate: resetUpdate,
            cancelCreate: cancelCreate,
            cancelUpdate: cancelUpdate,
            init: init
        };
    })
