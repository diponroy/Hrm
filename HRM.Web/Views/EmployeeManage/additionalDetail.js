﻿
function EmployeeAddtionalLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employee = ko.observable();

    self.employeeAddtionalLogs = ko.observableArray([]);


    /* Actions */

    self.getEmployee = function () {
        $.ajax({
            url: '/EmployeeManage/Get/' + self.id(),
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.employee({
                    name: data.FullName,
                    email: data.Email,
                    contact: data.ContactNo
                });
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.getAdditionalLogs = function () {
        self.employeeAddtionalLogs([]);

        $.ajax({
            url: '/EmployeeManage/GetLogs/' + self.id(),
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployee = data[index];
                    arr.push({
                        id: anEmployee.Id,
                        fathersName: anEmployee.FathersName,
                        mothersName: anEmployee.MothersName,
                        bloodGroup: anEmployee.BloodGroup,
                        image: anEmployee.ImageDirectory,
                        homePhone: anEmployee.HomePhone,
                        officePhone: anEmployee.OfficePhone,
                        permanentAddress: anEmployee.PermanentAddress,
                        city: anEmployee.City,

                        status: anEmployee.Status,
                        statusString: anEmployee.StatusString,
                        affectedByName: anEmployee.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(anEmployee.AffectedDateTime),
                        logStatus: anEmployee.LogStatuses
                    });
                });

                self.employeeAddtionalLogs(arr);
                $.unblockUI();
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.init = function () {
        self.id($('#txtEmployeeId').val());
        self.getEmployee();
        self.getAdditionalLogs();
    };
}


$(document).ready(function () {
    var vm = new EmployeeAddtionalLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});