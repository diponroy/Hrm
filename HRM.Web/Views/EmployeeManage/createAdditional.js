﻿
function EmployeeAdditionalViewModel() {
    var self = this;

    /* basic */
    self.employeeId = ko.observable();
    self.employeeName = ko.observable();

    /* additional */
    self.fathersName = ko.observable().extend({ maxLength: 50 });
    self.mothersName = ko.observable().extend({ maxLength: 50 });
    self.bloodGroup = ko.observable();
    self.officePhone = ko.observable().extend({ maxLength: 50 });
    self.homePhone = ko.observable().extend({ maxLength: 50 });
    self.permanentAddress = ko.observable().extend({ maxLength: 250 });
    self.city = ko.observable().extend({ maxLength: 50 });
    self.imageDirectory = ko.observable().extend({ maxLength: 250 });

    self.allBloodGroups = ko.observableArray([]);


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };


    self.reset = function () {
        self.fathersName('');
        self.mothersName('');
        self.bloodGroup('');
        self.imageDirectory('');
        self.officePhone('');
        self.homePhone('');
        self.permanentAddress('');
        self.city('');

        self.removeErrors();
    };

    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.employeeId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getEmployeeContactDetail = function () {
        var employeeObj = {
            FathersName: self.fathersName(),
            MothersName: self.mothersName(),
            BloodGroup: self.bloodGroup(),
            ImageDirectory: self.imageDirectory(),
            HomePhone: self.homePhone(),
            OfficePhone: self.officePhone(),
            PermanentAddress: self.permanentAddress(),
            City: self.city(),
        };

        return {
            id: self.employeeId(),
            employeeAdditional: employeeObj
        };
    };


    /* actions */
    self.create = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return;
        }

        jax.postJsonBlock(
            '/EmployeeManage/UpdateAdditional',
            self.getEmployeeContactDetail(),
            function (data) {
                ToastSuccess('Employee Additional Info is created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.goEducation = function () {
        window.location = '/EmployeeEducationManage/Create/' + self.employeeId();
        $.EmployeeEducationViewModel.btnVisibility(true);
    };

    self.init = function () {
        self.employeeId($('#txtEmployeeId').val());
        self.getEmployee();

        var bloodGroup = [];
        bloodGroup.push({ name: 'A+' });
        bloodGroup.push({ name: 'A-' });
        bloodGroup.push({ name: 'B+' });
        bloodGroup.push({ name: 'B-' });
        bloodGroup.push({ name: 'AB+' });
        bloodGroup.push({ name: 'AB-' });
        bloodGroup.push({ name: 'O+' });
        bloodGroup.push({ name: 'O-' });
        self.allBloodGroups(bloodGroup);


        $('#uploadBasicImage').click(function () {
            var formdata = new FormData();
            var fileInput = document.getElementById('fileInput');

            for (i = 0; i < fileInput.files.length; i++) {
                formdata.append(fileInput.files[i].name, fileInput.files[i]);
            }

            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/EmployeeManage/Upload');
            xhr.send(formdata);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                        self.imageDirectory(xhr.responseText);
                        ToastSuccess("Image has successfully been uploaded.");
                    } else {
                        ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                    }
                }
            };

            return false;
        });

        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeManage"]'));
    };
}


$(document).ready(function () {

    var vm = new EmployeeAdditionalViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});