﻿
function EmployeeLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employee = ko.observable();

    self.employeeBasicLogs = ko.observableArray([]);


    /* Actions */

    self.getEmployee = function () {
        $.ajax({
            url: '/EmployeeManage/Get/' + self.id(),
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.employee({
                    name: data.FullName,
                    email: data.Email,
                    contact: data.ContactNo
                });
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.getBasicLogs = function () {
        self.employeeBasicLogs([]);

        $.ajax({
            url: '/EmployeeManage/GetLogs/' + self.id(),
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployee = data[index];
                    arr.push({
                        id: anEmployee.Id,
                        trackNo: anEmployee.TrackNo,
                        salutation: anEmployee.Salutation,
                        firstName: anEmployee.FirstName,
                        lastName: anEmployee.LastName,
                        dateOfBirth: (anEmployee.DateOfBirth === null) ? "---" : clientDateStringFromDate(anEmployee.DateOfBirth),
                        gender: anEmployee.Gender,
                        email: anEmployee.Email,
                        contact: anEmployee.ContactNo,
                        presentAddress: anEmployee.PresentAddress,
                        division: anEmployee.DivisionOrState,

                        status: anEmployee.Status,
                        statusString: anEmployee.StatusString,
                        affectedByName: anEmployee.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(anEmployee.AffectedDateTime),
                        logStatus: anEmployee.LogStatuses
                    });
                });

                self.employeeBasicLogs(arr);
                $.unblockUI();
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.showAdditionalLog = function () {
        window.location = '/EmployeeManage/AdditionalDetail/' + self.id();
    };

    self.showEducationLog = function () {
        window.location = '/EmployeeEducationManage/Detail/' + self.id();
    };

    self.showAchievementLog = function () {
        window.location = '/EmployeeAchievementManage/Detail/' + self.id();
    };

    self.showMembershipLog = function () {
        window.location = '/EmployeeMembershipManage/Detail/' + self.id();
    };

    self.init = function () {
        self.id($('#txtEmployeeId').val());
        self.getEmployee();
        self.getBasicLogs();
    };
}


$(document).ready(function () {
    var vm = new EmployeeLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});