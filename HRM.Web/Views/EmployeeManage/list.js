﻿
function EmployeeListViewModel() {
    var self = this;
    self.employees = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getEmployees = function (pageNo, pageSize, searchFilters) {
        self.employees([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/EmployeeManage/Find',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployee = data[index];
                    arr.push({
                        id: anEmployee.Id,
                        trackNo: anEmployee.TrackNo,
                        name: anEmployee.FullName,
                        email: anEmployee.Email,
                        contactNo: anEmployee.ContactNo,
                        status: anEmployee.Status,
                        statusString: anEmployee.StatusString
                    });
                });
                self.employees(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        self.showToEdit = function (item) {
            window.location = '/EmployeeManage/UpdateBasic/' + item.id;
        };

        self.showDetail = function (item) {
            window.location = '/EmployeeManage/Detail/' + item.id;
        };

        self.delete = function (id) {
            var obj = {
                id: id,
            };
            jax.postJsonBlock(
                '/EmployeeManage/Delete',
                obj,
                function (data) {
                    ToastSuccess("Employee deleted successfully");
                    self.getEmployees(1, 25, self.searchFilers());
                },
                function (qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        };

        self.confirmToDelete = function (item) {
            bootbox.confirm("Are you sure, you want to delete this Employee ?", function (result) {
                if (result === true) {
                    self.delete(item.id);
                }
            });
        };
    }

    self.init = function () {
        self.resetFilers();
        self.getEmployees(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeManage"]'));
    };
}


$(document).ready(function () {

    var vm = new EmployeeListViewModel();
    ko.applyBindings(vm);
    vm.init();
})