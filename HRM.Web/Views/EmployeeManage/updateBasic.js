﻿/*Validations*/
ko.validation.rules['emailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#txtEmployeeId').val(), email: val });
        $.when(
            $.ajax({
                url: '/EmployeeManage/IsEmailUsedExceptEmployee',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This email address is already in use!!'
};
ko.validation.registerExtenders();


function EmployeeUpdateBasicViewModel() {
    var self = this;

    self.employeeName = ko.observable();

    self.id = ko.observable();
    self.trackNo = ko.observable().extend({ required: true });
    self.salutation = ko.observable().extend({ required: true });
    self.firstName = ko.observable().extend({ required: true, maxLength: 50 });
    self.lastName = ko.observable().extend({ maxLength: 50 });
    self.dateOfBirth = ko.observable(currentDate()).extend({ required: true });
    self.gender = ko.observable().extend({ required: true });
    self.email = ko.observable().extend({ required: true, maxLength: 50, email: true, emailUsed: false });
    self.contact = ko.observable().extend({ required: true, maxLength: 50, digit: true });
    self.presentAddress = ko.observable().extend({ required: true, maxLength: 250 });
    self.division = ko.observable().extend({ required: true, maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    self.allSalutations = ko.observableArray([]);
    self.allGenders = ko.observableArray([]);
    self.allDivisions = ko.observableArray([]);
    self.allStatuses = ko.observableArray([]);


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };


    self.getEmployeeBasicDetail = function () {
        var employeeObj = {
            Id:self.id(),
            TrackNo: self.trackNo(),
            Salutation: self.salutation(),
            FirstName: self.firstName(),
            LastName: self.lastName(),
            DateOfBirth: self.dateOfBirth(),
            Gender: self.gender(),
            Email: self.email(),
            ContactNo: self.contact(),
            PresentAddress: self.presentAddress(),
            DivisionOrState: self.division(),
            Status: self.status()
        };

        return {
            employeeBasic: employeeObj
        };
    };


    /* actions */

    self.load = function() {
        jax.getJson(
            '/EmployeeManage/Get/' + self.id(),
            function (data) {
                self.employeeName(data.FullName);

                self.trackNo(data.TrackNo);
                self.salutation(data.Salutation),
                self.firstName(data.FirstName);
                self.lastName(data.LastName);
                self.dateOfBirth(clientDate(data.DateOfBirth));
                self.gender(data.Gender);
                self.email(data.Email);
                self.contact(data.ContactNo),
                self.presentAddress(data.PresentAddress);
                self.division(data.DivisionOrState);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function(qXhr, textStatus, error) {
                ToastError("Error while loading the Employee !!");
            }
        );
    };

    self.update = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return;
        }

        jax.postJsonBlock(
            '/EmployeeManage/Update',
            self.getEmployeeBasicDetail(),
            function (data) {
                ToastSuccess('Employee is updated successfully');
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.goAdditional = function () {
        window.location = '/EmployeeManage/UpdateAdditional/' + self.id();
    };


    /*datepicker*/
    self.isDatepickerFocused = ko.observable(false);

    self.showDatepicker = function () {
        self.isDatepickerFocused(true);
    };

    self.resetDatepicker = function () {
        self.dateOfBirth(currentDate());
    };

    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.id($('#txtEmployeeId').val());

        var salutation = [];
        salutation.push({ name: 'Mr.' });
        salutation.push({ name: 'Mrs.' });
        self.allSalutations(salutation);

        var gender = [];
        gender.push({ name: 'Male' });
        gender.push({ name: 'Female' });
        self.allGenders(gender);

        var division = [];
        division.push({ name: 'Dhaka' });
        division.push({ name: 'Rajshahi' });
        division.push({ name: 'Rangpur' });
        division.push({ name: 'Sylhet' });
        division.push({ name: 'Barisal' });
        division.push({ name: 'Chittagong' });
        division.push({ name: 'Khulna' });
        self.allDivisions(division);

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeManage"]'));
    };
};
    

$(document).ready(function () {

    $('.datepicker').datepicker();

    var vm = new EmployeeUpdateBasicViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});