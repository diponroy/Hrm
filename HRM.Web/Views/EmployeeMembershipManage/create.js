﻿function EmployeeMembershipViewModel() {
    var self = this;
    self.organization = ko.observable().extend({ maxLength: 100, required: true });
    self.description = ko.observable().extend({ maxLength: 250 });
    self.type = ko.observable().extend({ maxLength: 20 });
    self.employeeId = ko.observable();
    self.allEmployee = ko.observableArray([]);
    self.remarks = ko.observable().extend({ maxLength: 150, required: true });
    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);
    
    self.employeeId = ko.observable();
    self.employeeName = ko.observable();

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };
    
    self.employeeMembershipObj = function () {
        return {
            Organization: self.organization(),
            Description: self.description(),
            Type:self.type(),
            EmployeeId: self.employeeId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };
    
    self.reset = function () {
        self.organization('');
        self.description('');
        self.type('');
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.employeeId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError("Employee Name couldn't be retrieved !!");
            }
        );
    };
    
    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        self.saveToServer();
    };

    /* Server Actions */
    self.saveToServer = function () {
        jax.postJsonBlock(
            '/EmployeeMembershipManage/Create',
            self.employeeMembershipObj(),
            function () {
                ToastSuccess('Membership information preserved successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.init = function () {
        self.employeeId($('#txtEmployeeId').val());

        self.getEmployee();
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeMembershipManage"]'));
    };
}

$(document).ready(function() {
    var viewModel = new EmployeeMembershipViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});