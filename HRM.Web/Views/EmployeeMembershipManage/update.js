﻿
function EmployeeMembershipUpdateViewModel() {
    var self = this;

    /* basic */
    self.employeeId = ko.observable(),
    self.employeeName = ko.observable(),

    /*properties*/
    self.employeeMemberships = ko.observableArray([]);


    /* Local Actions */
    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.employeeId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError("Employee Name couldn't be retrieved !!");
            }
        );
    };

    self.load = function () {
        self.employeeMemberships([]);

        jax.postJsonBlock(
            '/EmployeeMembershipManage/GetMemberships/' + self.employeeId(),
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anMembership = data[index];
                    arr.push({
                        id: anMembership.Id,
                        organization: anMembership.Organization,
                        type: anMembership.Type,
                        description: anMembership.Description
                    });
                });
                self.employeeMemberships(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.showToEdit = function (item) {
        window.location = '/EmployeeMembershipManage/UpdateIndividual/' + item.id;
    };

    self.showToCreate = function () {
        window.location = '/EmployeeMembershipManage/Create/' + self.employeeId();
    }

    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete this Employee Membership ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/EmployeeMembershipManage/Delete',
            obj,
            function (data) {
                ToastSuccess("Employee Membership deleted successfully");
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.employeeId($('#txtEmployeeId').val());
        self.getEmployee();
        self.load();
        
        activeParentMenu($('li.sub a[href="/EmployeeMembershipManage"]'));
    };
}


$(document).ready(function () {
    var viewModel = new EmployeeMembershipUpdateViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();
});