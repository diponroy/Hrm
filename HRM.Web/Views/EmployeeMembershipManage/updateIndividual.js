﻿
function EmployeeMembershipUpdateIndividualViewModel() {
    var self = this;
    self.id = ko.observable().extend({ required: true });
    self.organization = ko.observable().extend({ maxLength: 100 });
    self.description = ko.observable().extend({ maxLength: 250 });
    self.type = ko.observable().extend({ maxLength: 30 });
    self.employeeId = ko.observable();
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);

    self.employeeId = ko.observable();
    self.employeeName = ko.observable();

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.employeeMembershipObj = function () {
        return {
            Id: self.id(),
            Organization: self.organization(),
            Description: self.description(),
            Type: self.type(),
            EmployeeId: self.employeeId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };


    /* Actions */
    self.getEmployee = function () {
        jax.postJsonBlock(
            '/EmployeeManage/Get/' + self.employeeId(),
            null,
            function (data) {
                self.employeeName(data.FullName);
            },
            function (qXhr, textStatus, error) {
                ToastError("Employee Name couldn't be retrieved !!");
            }
        );
    };

    self.load = function () {
        jax.getJsonBlock(
           '/EmployeeMembershipManage/Get/' + self.id(),
            function (data) {
                self.employeeId(data.EmployeeId);

                self.getEmployee();

                self.organization(data.Organization);
                self.description(data.Description),
                self.type(data.Type);
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeMembershipManage/Update',
            self.employeeMembershipObj(),
            function () {
                ToastSuccess('Membership information updated successfully');
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
         );
    };

    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.id($('#txtMembershipId').val());
        self.reset();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        
        activeParentMenu($('li.sub a[href="/EmployeeMembershipManage"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeMembershipUpdateIndividualViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});