﻿
function EmployeePayrollInfoViewModel() {
    var self = this;

    self.employeeId = ko.observable().extend({ required: true });
    self.bankName = ko.observable().extend({ required: true }),
    self.bankDetails = ko.observable().extend({ required: true }),
    self.accountNo = ko.observable().extend({ required: true }),
    self.tin = ko.observable().extend({ maxLength: 150, required: true }),
    self.remarks = ko.observable().extend({ required: true }),
    self.status = ko.observable().extend({ required: true }),

    self.allStatuses = ko.observableArray([]),
    self.employees = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self),
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; },
    self.showErrors = function () { self.errors.showAllMessages(); },
    self.removeErrors = function () { self.errors.showAllMessages(false); },


    /* actions */
    self.loadEmployees = function () {
        self.employees([]);
        jax.postJsonBlock(
            '/EmployeePayrollInfo/GetEmployees',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeWsUnit = data[index];
                    arr.push({
                        id: anEmployeeWsUnit.Id,
                        name: anEmployeeWsUnit.FullName
                    });
                });
                self.employees(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getEmployeePayrollInfo = function () {
        var empPayrollObj = {
            EmployeeId: self.employeeId(),
            BankName: self.bankName(),
            BankDetail: self.bankDetails(),
            AccountNo: self.accountNo(),
            TinNumber: self.tin(),
            Remarks: self.remarks(),
            Status: self.status()
        };

        return empPayrollObj;
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/EmployeePayrollInfo/Create',
            self.getEmployeePayrollInfo(),
            function () {
                ToastSuccess('Employee Payroll Info is saved successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    /*resets*/
    self.reset = function () {
        self.employeeId('');
        self.bankName('');
        self.bankDetails('');
        self.accountNo('');
        self.tin('');
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.loadEmployees();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);
    };
};


$(document).ready(function () {

    var vm = new EmployeePayrollInfoViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});