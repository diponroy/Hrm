﻿
function EmployeePayrollInfoLogViewModel() {
    var self = this;
    self.id = ko.observable();

    self.payrollInfoLogs = ko.observableArray([]);


    /* Actions */
    self.getLogs = function () {
        self.payrollInfoLogs([]);

        jax.getJson(
            '/EmployeePayrollInfo/GetLogs/' + self.id(),
            function (data) {
                var arr = new Array();

                $.each(data, function (index) {
                    var aPayrollInfo = data[index];
                    arr.push({
                        id: aPayrollInfo.Id,
                        employeeName: aPayrollInfo.EmployeeLog.FullName,
                        bankName: aPayrollInfo.BankName,
                        bankDetails: aPayrollInfo.BankDetail,
                        accountNo: aPayrollInfo.AccountNo,
                        tin: aPayrollInfo.TinNumber,
                        remarks: aPayrollInfo.Remarks,

                        status: aPayrollInfo.Status,
                        statusString: aPayrollInfo.StatusString,
                        affectedByName: aPayrollInfo.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(aPayrollInfo.AffectedDateTime),
                        logStatus: aPayrollInfo.LogStatuses
                    });
                });

                self.payrollInfoLogs(arr);
                $.unblockUI();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtPayrollId').val());
        self.getLogs();
    };
}


$(document).ready(function () {
    var vm = new EmployeePayrollInfoLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});