﻿
function EmployeePayrollInfoListViewModel() {
    var self = this;
    self.paryrollInfos = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getPayrollInfos = function (pageNo, pageSize, searchFilters) {
        self.paryrollInfos([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/EmployeePayrollInfo/Find',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aPayrollInfo = data[index];
                    arr.push({
                        id: aPayrollInfo.Id,
                        employeeName: aPayrollInfo.Employee.FullName,
                        bankName: aPayrollInfo.BankName,
                        bankDetails: aPayrollInfo.BankDetail,
                        accountNo: aPayrollInfo.AccountNo,
                        tin: aPayrollInfo.TinNumber,
                        remarks: aPayrollInfo.Remarks,
                        status: aPayrollInfo.Status,
                        statusString: aPayrollInfo.StatusString
                    });
                });

                self.paryrollInfos(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        self.showToEdit = function (item) {
            window.location = '/EmployeePayrollInfo/Update/' + item.id;
        };

        self.showDetail = function (item) {
            window.location = '/EmployeePayrollInfo/Detail/' + item.id;
        };

        self.delete = function (id) {
            var obj = {
                id: id,
            };

            jax.postJsonBlock(
                '/EmployeePayrollInfo/Delete',
                obj,
                function (data) {
                    ToastSuccess("Employee Payroll Info is deleted successfully");
                    self.getPayrollInfos(1, 25, self.searchFilers());
                },
                function (qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        };

        self.confirmToDelete = function (item) {
            bootbox.confirm("Are you sure, you want to delete this Employee Payroll Info ?", function (result) {
                if (result === true) {
                    self.delete(item.id);
                }
            });
        };
    }

    self.init = function () {
        self.resetFilers();
        self.getPayrollInfos(1, 25, self.searchFilers());
    };
}


$(document).ready(function () {

    var vm = new EmployeePayrollInfoListViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
})