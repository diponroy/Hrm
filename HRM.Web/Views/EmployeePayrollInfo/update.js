﻿
function EmployeePayrollInfoUpdateViewModel() {
    var self = this;

    self.id = ko.observable();
    self.employeeId = ko.observable().extend({ required: true });
    self.employeeName = ko.observable();
    self.bankName = ko.observable().extend({ maxLength: 100, required: true }),
    self.bankDetails = ko.observable().extend({ maxLength: 150, required: true }),
    self.accountNo = ko.observable().extend({ maxLength: 100, required: true }),
    self.tin = ko.observable().extend({ required: true }),
    self.remarks = ko.observable().extend({ required: true }),
    self.status = ko.observable().extend({ required: true }),

    self.allStatuses = ko.observableArray([]),


    /*errors*/
    self.errors = ko.validation.group(self),
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; },
    self.showErrors = function () { self.errors.showAllMessages(); },
    self.removeErrors = function () { self.errors.showAllMessages(false); },


    /* actions */

    self.getEmployeePayrollInfo = function () {
        var empPayrollObj = {
            Id: self.id(),
            EmployeeId: self.employeeId(),
            BankName: self.bankName(),
            BankDetail: self.bankDetails(),
            AccountNo: self.accountNo(),
            TinNumber: self.tin(),
            Remarks: self.remarks(),
            Status: self.status()
        };

        return empPayrollObj;
    };

    self.load = function () {
        jax.getJson(
            '/EmployeePayrollInfo/Get/' + self.id(),
            function (data) {
                self.employeeId(data.Employee.Id);
                self.employeeName(data.Employee.FullName);
                self.bankName(data.BankName);
                self.bankDetails(data.BankDetail);
                self.accountNo(data.AccountNo);
                self.tin(data.TinNumber);
                self.remarks(data.Remarks);
                self.status(data.StatusString);

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/EmployeePayrollInfo/Update',
            self.getEmployeePayrollInfo(),
            function () {
                ToastSuccess('Employee Payroll Info is updated successfully');
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    /*resets*/
    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.id($('#txtPayrollId').val());

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);
        self.reset();
    };
};


$(document).ready(function () {

    var vm = new EmployeePayrollInfoUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});