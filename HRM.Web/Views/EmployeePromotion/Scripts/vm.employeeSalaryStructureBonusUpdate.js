﻿function EmployeeSalaryBonusUpdateVm() {
    var self = this;
    
    self.storedItemId = ko.observable();
    self.storedItemSalaryStructureId = ko.observable();
    self.storedItemSalaryStructureTitle = ko.observable();
    self.storedItemBonusId = ko.observable();

    self.callback = this.createdCallBack;
    self.allBonusTitles = ko.observableArray([]);

    self.id = ko.observable().extend({ required: true });
    self.salaryStructureId = ko.observable().extend({ required: true });
    self.salaryStructureTitle = ko.observable();
    self.bonusId = ko.observable().extend({ required: true });

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };
    
    self.postObj = function () {
        return {
            Id: self.id(),
            EmployeeSalaryStructureId: self.salaryStructureId(),
            BonusId: self.bonusId()
        };
    };
    
    /*All DDL Load*/
    self.getBonusTitles = function () {
        jax.getJson(
            '/SalaryStructureBonusManage/GetBonusTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allBonusTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeePromotion/UpdateSalaryStructureBonus',
            self.postObj(),
            function () {
                ToastSuccess('Salary Bonus is updated successfully');
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
        this.createdCallBack();
    };

    self.reset = function () {
        self.id(self.storedItemId());
        self.salaryStructureId(self.storedItemSalaryStructureId());
        self.salaryStructureTitle(self.storedItemSalaryStructureTitle());
        self.bonusId(self.storedItemBonusId());
        self.removeErrors();
    };

    self.init = function (id, salaryStructureId, salaryStructureTitle, bonusId) {
        self.getBonusTitles();
        
        self.storedItemId(id);
        self.storedItemSalaryStructureId(salaryStructureId);
        self.storedItemSalaryStructureTitle(salaryStructureTitle);
        self.storedItemBonusId(bonusId);

        self.id(id);
        self.salaryStructureId(salaryStructureId);
        self.salaryStructureTitle(salaryStructureTitle);
        self.bonusId(bonusId);
    };
}