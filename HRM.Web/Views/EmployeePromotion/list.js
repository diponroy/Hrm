﻿
function EmployeePromotionListViewModel() {
    var self = this;
    self.promotions = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getPromotions = function (pageNo, pageSize, searchFilters) {
        self.promotions([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/EmployeePromotion/Find',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aPromotion = data[index];
                    arr.push({
                        id: aPromotion.Id,
                        employeeWSUEmployee: aPromotion.EmployeeWorkStationUnit.Employee.FullName,
                        employeeWSUCompany: aPromotion.EmployeeWorkStationUnit.WorkStationUnit.Company.Name,
                        employeeWSUBranch: aPromotion.EmployeeWorkStationUnit.WorkStationUnit.Branch.Name,
                        employeeWSUDepartment: aPromotion.EmployeeWorkStationUnit.WorkStationUnit.Department.Name,
                        employeeWSUWorkStationName: aPromotion.EmployeeWorkStationUnit.WorkStationUnit.Remarks,
                        company: aPromotion.WorkStationUnit.Company.Name,
                        branch: aPromotion.WorkStationUnit.Branch.Name,
                        department: aPromotion.WorkStationUnit.Department.Name,
                        workStationUnit: aPromotion.WorkStationUnit.Remarks,
                        employeeType: aPromotion.EmployeeType.Title,
                        remarks: aPromotion.Remarks,
                        approval: aPromotion.IsApproved,
                        status: aPromotion.Status,
                        statusString: aPromotion.StatusString
                    });
                });

                self.promotions(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        self.showToEdit = function (item) {
            window.location = '/EmployeePromotion/Update/' + item.id;
        };

        self.showDetail = function (item) {
            window.location = '/EmployeePromotion/Detail/' + item.id;
        };

        self.delete = function (id) {
            var obj = {
                id: id,
            };

            jax.postJsonBlock(
                '/EmployeePromotion/Delete',
                obj,
                function (data) {
                    ToastSuccess("Employee Promotion Info is deleted successfully");
                    self.getPromotions(1, 25, self.searchFilers());
                },
                function (qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        };

        self.confirmToDelete = function (item) {
            bootbox.confirm("Are you sure, you want to delete this Employee Promotion Info ?", function (result) {
                if (result === true) {
                    self.delete(item.id);
                }
            });
        };
    }

    self.init = function () {
        self.resetFilers();
        self.getPromotions(1, 25, self.searchFilers());

        activeParentMenu($('li.sub a[href="/EmployeePromotion"]'));
    };
}


$(document).ready(function () {

    var vm = new EmployeePromotionListViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
})