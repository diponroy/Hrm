﻿
function EmployeePromotionUpdateViewModel() {
    var self = this;

    self.employeeId = ko.observable().extend({ required: true }),
    self.employeeName = ko.observable(),
    self.employeeTypeId = ko.observable(),
    self.employeeWorkStationUnitId = ko.observable(),
    self.workStationUnitId = ko.observable(),
    self.employeeWorkStationUnitId = ko.observable(),
    self.salaryStructureId = ko.observable(),
    self.employeeSalaryStructureId = ko.observable(),
    self.incrementSalaryStructureId = ko.observable(),

    self.remarks = ko.observable().extend({ maxLength: 150 }),
    self.approval = ko.observable().extend({ required: true }),
    self.allApprovals = ko.observable().extend({ required: true }),
    self.status = ko.observable().extend({ required: true }),
    self.allStatuses = ko.observableArray([]),
    self.workStationUnits = ko.observableArray([]),
    self.employeeWorkStationUnits = ko.observableArray([]),
    self.employeeTypes = ko.observableArray([]),
    self.salaryStructures = ko.observableArray([]),


    self.employeeSalaryStructureTitle = ko.observable().extend({ required: true, maxLength: 100 });
    self.employeeSalaryStructureBasic = ko.observable().extend({ digit: true });
    self.employeeSalaryStructureStatus = ko.observable().extend();
    self.employeeSalaryStructureStatusString = ko.observable().extend();
    self.allEmployeeSalaryStructureStatuses = ko.observableArray([]);

    self.salaryAllowances = ko.observableArray([]);
    self.salaryBonuses = ko.observableArray([]);


    /* vm */
    //self.employeeSalaryAllowanceUpdate = new EmployeeSalaryAllowanceUpdateVm();

    //EmployeeSalaryAllowanceUpdateVm.prototype.createdCallBack = function () {
    //    $('#divSalaryAllowanceUpdate').modal('hide');
    //    self.loadAllowanceList();
    //};

    //self.employeeSalaryBonusUpdate = new EmployeeSalaryBonusUpdateVm();

    //EmployeeSalaryBonusUpdateVm.prototype.createdCallBack = function () {
    //    $('#divSalaryBonusUpdate').modal('hide');
    //    self.loadBonusList();
    //};


    /*errors*/
    self.errors = ko.validation.group(self),
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; },
    self.showErrors = function () { self.errors.showAllMessages(); },
    self.removeErrors = function () { self.errors.showAllMessages(false); },


    /* loader actions */
    self.loadEmployeeWorkStationUnit = function () {
        self.employeeWorkStationUnits([]);

        jax.postJsonBlock(
            '/EmployeePromotion/GetEmployeeWorkStationUnits',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aEWSUnit = data[index];
                    arr.push({
                        unitId: aEWSUnit.Id,
                        unit: 'Employee: ' + aEWSUnit.Employee.FullName + ' | ' + 'Company: ' + aEWSUnit.WorkStationUnit.Company.Name + ' | '
                            + 'Branch: ' + aEWSUnit.WorkStationUnit.Branch.Name + ' | ' + 'Department: ' + aEWSUnit.WorkStationUnit.Department.Name
                    });
                });
                self.employeeWorkStationUnits(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeTypes = function (employeeId) {
        self.employeeTypes([]);

        jax.postJsonBlock(
            '/EmployeePromotion/GetEmployeeTypes/' + employeeId,
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeType = data[index];
                    arr.push({
                        id: anEmployeeType.Id,
                        title: anEmployeeType.Title
                    });
                });
                self.employeeTypes(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadWorkStationUnits = function () {
        self.workStationUnits([]);

        jax.postJsonBlock(
            '/EmployeePromotion/GetWorkStationUnits',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anWorkStationUnit = data[index];
                    arr.push({
                        id: anWorkStationUnit.Id,
                        fullName: 'Company: ' + anWorkStationUnit.Company.Name + ' -- Branch: ' + anWorkStationUnit.Branch.Name + ' -- Department: ' + anWorkStationUnit.Department.Name
                    });
                });
                self.workStationUnits(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadSalaryStructures = function () {
        self.salaryStructures([]);

        jax.postJsonBlock(
            '/EmployeePromotion/GetSalaryStructures',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aSalaryStructure = data[index];
                    arr.push({
                        id: aSalaryStructure.Id,
                        text: 'Title: ' + aSalaryStructure.Title + ' | Basic: ' + aSalaryStructure.Basic
                    });
                });
                self.salaryStructures(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeSalaryStructure = function () {
        jax.getJsonBlock(
            '/EmployeePromotion/GetEmployeeSalaryStructure/' + self.employeeWorkStationUnitId(),
            function (data) {
                self.employeeSalaryStructureId(data.Id);
                self.salaryStructureId(data.SalaryStructure.Id);

                self.employeeSalaryStructureTitle(data.SalaryStructure.Title);
                self.employeeSalaryStructureBasic(data.Basic);
                self.employeeSalaryStructureStatus(data.Status);
                self.employeeSalaryStructureStatusString(data.StatusString);

                self.loadAllowanceList();
                self.loadBonusList();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        /* alternate method */
        //jax.postJsonBlock(
        //    '/EmployeePromotion/GetEmployeeSalaryStructure',
        //    {employeeWorkstationUnitId : self.employeeWorkStationUnitId()},
        //    function (data) {
        //        $.each(data, function (index) {
        //            self.salaryStructureId(data.SalaryStructure.Id);
        //            self.employeeSalaryStructureId(data.Id);
        //        });
        //    },
        //    function (qXhr, textStatus, error) {
        //        ToastError(error);
        //    }
        //);
    };

    self.loadTypeAndWorkstation = function (employeeWsUnitId) {

        jax.postJsonBlock(
            '/EmployeePromotion/GetTypeAndWorkStation/' + employeeWsUnitId,
            null,
            function (data) {
                self.employeeTypeId(data.EmployeeType.Id);
                self.workStationUnitId(data.WorkStationUnit.Id);
                self.employeeWorkStationUnitId(data.Id);
                self.loadEmployeeSalaryStructure();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    //self.loadDetailedSalaryStructure = function () {
    //    jax.getJsonBlock(
    //        '/EmployeePromotion/GetEmployeeSalaryStructure/' + self.employeeSalaryStructureId(),
    //        function (data) {
    //            self.employeeSalaryStructureTitle(data.SalaryStructure.Title);
    //            self.employeeSalaryStructureBasic(data.Basic);
    //            self.employeeSalaryStructureStatus(data.Status);
    //            self.employeeSalaryStructureStatusString(data.StatusString);

    //            self.loadAllowanceList();
    //            self.loadBonusList();
    //        },
    //        function (qXhr, textStatus, error) {
    //            ToastError(error);
    //        }
    //    );
    //};

    self.loadAllowanceList = function () {
        self.salaryAllowances();

        jax.getJsonBlock(
            '/EmployeePromotion/GetEmployeeSalaryStructureAllowance/' + self.employeeSalaryStructureId(),
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anAllowance = data[index];
                    arr.push({
                        id: anAllowance.Id,
                        allowanceSalaryStructureId: anAllowance.EmployeeSalaryStructure.SalaryStructure.Id,
                        allowanceSalaryStructureTitle: anAllowance.EmployeeSalaryStructure.SalaryStructure.Title,
                        allowanceId: anAllowance.Allowance.Id,
                        allowanceTitle: anAllowance.Allowance.Title,
                        weight: anAllowance.Weight,
                        percentage: anAllowance.AsPercentage,
                        status: anAllowance.Status,
                        statusString: anAllowance.StatusString
                    });
                });
                self.salaryAllowances(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadBonusList = function () {
        self.salaryBonuses();

        jax.getJsonBlock(
            '/EmployeePromotion/GetEmployeeSalaryStructureBonus/' + self.employeeSalaryStructureId(),
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aBonus = data[index];
                    arr.push({
                        id: aBonus.Id,
                        bonusSalaryStructureId: aBonus.EmployeeSalaryStructure.SalaryStructure.Id,
                        bonusSalaryStructureTitle: aBonus.EmployeeSalaryStructure.SalaryStructure.Title,
                        bonusId: aBonus.Bonus.Id,
                        bonusTitle: aBonus.Bonus.Title,
                        weight: aBonus.Weight,
                        percentage: aBonus.AsPercentage,
                        status: aBonus.Status,
                        statusString: aBonus.StatusString
                    });
                });
                self.salaryBonuses(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.load = function () {
        jax.getJson(
            '/EmployeePromotion/Get/' + self.id(),
            function (data) {
                self.employeeId(data.EmployeeId);
                self.employeeName(data.Employee.FullName);
                self.employeeTypeId(data.BankName);
                self.employeeWorkStationUnitId(data.BankDetail);
                self.salaryStructureId(data.AccountNo);
                self.employeeSalaryStructureId(data.Remarks);
                self.incrementSalaryStructureId(data.StatusString);

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };


    /* other actions */
    self.getSalaryStructure = function () {
        var empSalaryStructure = {
            Id: self.employeeSalaryStructureId(),
            Basic: self.employeeSalaryStructureBasic(),
            Status: self.employeeSalaryStructureStatus(),
            SalaryStructureId: self.salaryStructureId()
        };

        return {
            model: empSalaryStructure,
            employeeWorkStationUnitId: self.workStationUnitId()
        };
    };

    self.getEmployeePromotion = function () {
        var empPromotionObj = {
            EmployeeWorkStationUnitId: self.employeeWorkStationUnitId(),
            EmployeeTypeId: self.employeeTypeId(),
            WorkStationUnitId: self.workStationUnitId(),
            IncrementSalaryStructureId: self.incrementSalaryStructureId(),
            IsApproved: self.approval(),
            Remarks: self.remarks(),
            Status: self.status()
        };

        return {
            entity: empPromotionObj,
            salaryStructureId: self.salaryStructureId(),
            employeeSalaryStructureId: self.employeeSalaryStructureId(),
            employeeSalaryStructureBasic: self.employeeSalaryStructureBasic()
        };
    };

    self.create = function () {
        //if (self.hasErrors()) {
        //    self.showErrors();
        //    return;
        //}

        jax.postJsonBlock(
            '/EmployeePromotion/Create',
            self.getEmployeePromotion(),
            function (data) {
                ToastSuccess('Employee Promotion is saved successfully');
                self.loadEmployeeWorkStationUnit();
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.updateAllowance = function (item) {
        $('#divSalaryAllowanceUpdate').removeData("modal").modal();
        self.employeeSalaryAllowanceUpdate.init(item.id, item.allowanceSalaryStructureId, item.allowanceSalaryStructureTitle, item.allowanceId);
    };

    self.updateBonus = function (item) {
        $('#divSalaryBonusUpdate').removeData("modal").modal();
        self.employeeSalaryBonusUpdate.init(item.id, item.bonusSalaryStructureId, item.bonusSalaryStructureTitle, item.bonusId);
    };

    self.updateSalaryStructure = function () {
        if (self.employeeSalaryStructureBasic() != 0 && self.employeeSalaryStructureBasic() != null) {
            jax.postJsonBlock(
                '/EmployeePromotion/UpdateSalaryStructure',
                self.getSalaryStructure(),
                function (data) {
                    ToastSuccess('Employee Salary Structure is updated successfully');
                    self.incrementSalaryStructureId(data);
                    self.loadEmployeeSalaryStructure();
                },
                function (qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        } else {
            bootbox.alert("Basic Salary should be meaningful !!!");
        }
    };


    self.confirmDeleteAllowance = function (item) {
        bootbox.confirm("Do you want to delete this Allowance?", function (result) {
            if (result === true) {
                self.deleteAllowance(item);
            }
        });
    };

    self.deleteAllowance = function (item) {
        jax.postJsonBlock(
            '/EmployeePromotion/DeleteEmployeeSalaryStructureAllowance',
            { id: item.id },
            function () {
                ToastSuccess("Salary Structure Allowance deleted successfully");
                self.loadAllowanceList();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.confirmDeleteBonus = function (item) {
        bootbox.confirm("Do you want to delete this Bonus?", function (result) {
            if (result === true) {
                self.deleteBonus(item);
            }
        });
    };

    self.deleteBonus = function (item) {
        jax.postJsonBlock(
            '/EmployeePromotion/DeleteEmployeeSalaryStructureBonus',
            { id: item.id },
            function () {
                ToastSuccess("Salary Structure Bonus deleted successfully");
                self.loadBonusList();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };


    /* select change events */
    self.employeeWorkStationUnitId.subscribe(function (newValue) {
        self.loadTypeAndWorkstation(newValue);
    });


    /*resets*/
    self.reset = function () {
        self.load();
        self.removeErrors();
    };

    self.closeSalaryStructure = function () {
        $('#updateSalaryStack').slideUp('slow');
        $('#lowerStack').slideDown('slow');
    };

    self.init = function () {
        self.loadEmployeeWorkStationUnit();
        self.loadEmployeeTypes();
        self.loadWorkStationUnits();
        self.loadSalaryStructures();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        var employeeSalaryStructureStatus = [];
        employeeSalaryStructureStatus.push({ name: 'Active' });
        employeeSalaryStructureStatus.push({ name: 'Inactive' });
        self.allEmployeeSalaryStructureStatuses(employeeSalaryStructureStatus);

        var approval = [];
        approval.push({ name: 'Initiated', value: 'False' });
        self.allApprovals(approval);

        activeParentMenu($('li.sub a[href="/EmployeePromotion"]'));
    };
};


$(document).ready(function () {

    var vm = new EmployeePromotionUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $("#btnUpdateSalaryStructure").click(function () {
        if (vm.employeeSalaryStructureId() === undefined || vm.employeeSalaryStructureId() === '') {
            bootbox.alert("You have to select an Employee Workstation Unit first !!!");
            return;
        }
        vm.loadEmployeeSalaryStructure();
        $('#updateSalaryStack').slideDown('slow');
        $('#lowerStack').slideUp('slow');
    });
});