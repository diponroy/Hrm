﻿
function EmployeeReportingViewModel() {
    var self = this;

    self.employeeWorkStationUnitId = ko.observable(),
    self.parentWorkStationUnitId = ko.observable(),
    self.attachmentDate = ko.observable(currentDate()).extend({ required: true }),
    self.detachmentDate = ko.observable(currentDate()).extend({ required: true }),
    self.remarks = ko.observable().extend({ maxLength: 150 }),
    self.status = ko.observable().extend({required: true}),
    self.allStatuses = ko.observableArray([]),

    self.employeeWorkStationUnits = ko.observableArray([]),
    self.parentWorkStationUnits = ko.observableArray([]),


    /*errors*/
    self.errors = ko.validation.group(self),
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; },
    self.showErrors = function () { self.errors.showAllMessages(); },
    self.removeErrors = function () { self.errors.showAllMessages(false); },


    /* actions */
    self.loadWorkStationUnits = function () {
        self.employeeWorkStationUnits([]);
        self.parentWorkStationUnits([]);

        jax.postJsonBlock(
            '/EmployeeReporting/GetEmployeeWorkStationUnits',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeWorkStationUnit = data[index];
                    arr.push({
                        id: anEmployeeWorkStationUnit.Id,
                        fullName: 'Employee: ' + anEmployeeWorkStationUnit.Employee.FullName + ' | Company: ' + anEmployeeWorkStationUnit.WorkStationUnit.Company.Name + ' | Branch: ' + anEmployeeWorkStationUnit.WorkStationUnit.Branch.Name + ' | Department: ' + anEmployeeWorkStationUnit.WorkStationUnit.Department.Name
                    });
                });
                self.employeeWorkStationUnits(arr);
                self.parentWorkStationUnits(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getEmployeeReporting = function () {
        var empReportingObj = {
            EmployeeWorkStationUnitId: self.employeeWorkStationUnitId(),
            ParentEmployeeWorkStationUnitId: self.parentWorkStationUnitId(),
            AttachmentDate: self.attachmentDate(),
            DetachmentDate: self.detachmentDate(),
            Remarks: self.remarks(),
            Status: self.status()
        };

        return empReportingObj;
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/EmployeeReporting/Create',
            self.getEmployeeReporting(),
            function () {
                ToastSuccess('Employee Reporting is saved successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    /*datepicker*/
    self.isAttachmentDateFocused = ko.observable(false);

    self.showAttachmentDate = function () {
        self.isAttachmentDateFocused(true);
    };

    self.resetAttachmentDate = function () {
        self.attachmentDate(currentDate());
    };

    self.isDetachmentDateFocused = ko.observable(false);

    self.showDetachmentDate = function () {
        self.isDetachmentDateFocused(true);
    };

    self.resetDetachmentDate = function () {
        self.detachmentDate(currentDate());
    };


    /*resets*/
    self.reset = function () {
        self.employeeWorkStationUnitId('');
        self.parentWorkStationUnitId('');
        self.remarks('');
        self.status('');
        self.attachmentDate(currentDate());
        self.detachmentDate(currentDate());
        self.removeErrors();
    };

    self.init = function () {
        self.loadWorkStationUnits();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);
        
        activeParentMenu($('li.sub a[href="/EmployeeReporting"]'));
    };
};


$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new EmployeeReportingViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});