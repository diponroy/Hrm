﻿
function EmployeeReportingLogViewModel() {
    var self = this;
    self.id = ko.observable();

    self.reportingLogs = ko.observableArray([]);


    /* Actions */
    self.getLogs = function () {
        self.reportingLogs([]);

        jax.getJson(
            '/EmployeeReporting/GetLogs/' + self.id(),
            function (data) {
                var arr = new Array();

                $.each(data, function (index) {
                    var aReporting = data[index];
                    arr.push({
                        id: aReporting.Id,
                        employeeName: aReporting.EmployeeWorkStationUnitLog.EmployeeLog.FullName,
                        employeeWUC: aReporting.EmployeeWorkStationUnitLog.WorkStationUnitLog.CompanyLog.Name,
                        employeeWUB: aReporting.EmployeeWorkStationUnitLog.WorkStationUnitLog.BranchLog.Name,
                        employeeWUD: aReporting.EmployeeWorkStationUnitLog.WorkStationUnitLog.DepartmentLog.Name,
                        reportedName: (aReporting.ParentEmployeeWorkStationUnitLog.EmployeeLog === null) ? '---' : aReporting.ParentEmployeeWorkStationUnitLog.EmployeeLog.FullName,
                        reportedWUC: (aReporting.ParentEmployeeWorkStationUnitLog === null) ? '' : 'Company: ' + aReporting.ParentEmployeeWorkStationUnitLog.WorkStationUnitLog.CompanyLog.Name,
                        reportedWUB: (aReporting.ParentEmployeeWorkStationUnitLog === null) ? '' : 'Branch: ' + aReporting.ParentEmployeeWorkStationUnitLog.WorkStationUnitLog.BranchLog.Name,
                        reportedWUD: (aReporting.ParentEmployeeWorkStationUnitLog === null) ? '' : 'Department: ' + aReporting.ParentEmployeeWorkStationUnitLog.WorkStationUnitLog.DepartmentLog.Name,
                        attachmentDate: clientDateStringFromDate(aReporting.AttachmentDate),
                        detachmentDate: (aReporting.DetachmentDate === null) ? '---' : clientDateStringFromDate(aReporting.DetachmentDate),
                        remarks: aReporting.Remarks,

                        status: aReporting.Status,
                        statusString: aReporting.StatusString,
                        affectedByName: aReporting.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(aReporting.AffectedDateTime),
                        logStatus: aReporting.LogStatuses
                    });
                });

                self.reportingLogs(arr);
                $.unblockUI();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtReportingId').val());
        self.getLogs();
    };
}


$(document).ready(function () {
    var vm = new EmployeeReportingLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});