﻿
function EmployeeReportingListViewModel() {
    var self = this;
    self.reportings = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getReportings = function (pageNo, pageSize, searchFilters) {
        self.reportings([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/EmployeeReporting/Find',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aReporting = data[index];
                    arr.push({
                        id: aReporting.Id,
                        employeeName: aReporting.EmployeeWorkStationUnit.Employee.FullName,
                        employeeWUC:  aReporting.EmployeeWorkStationUnit.WorkStationUnit.Company.Name,
                        employeeWUB: aReporting.EmployeeWorkStationUnit.WorkStationUnit.Branch.Name,
                        employeeWUD: aReporting.EmployeeWorkStationUnit.WorkStationUnit.Department.Name,
                        reportedName: (aReporting.ParentEmployeeWorkStationUnit === null)?'---' : aReporting.ParentEmployeeWorkStationUnit.Employee.FullName,
                        reportedWUC: (aReporting.ParentEmployeeWorkStationUnit === null)? '' : 'Company: ' + aReporting.ParentEmployeeWorkStationUnit.WorkStationUnit.Company.Name,
                        reportedWUB: (aReporting.ParentEmployeeWorkStationUnit === null) ? '' :'Branch: ' + aReporting.ParentEmployeeWorkStationUnit.WorkStationUnit.Branch.Name,
                        reportedWUD: (aReporting.ParentEmployeeWorkStationUnit === null) ? '' :'Department: ' + aReporting.ParentEmployeeWorkStationUnit.WorkStationUnit.Department.Name,
                        attachmentDate: clientDateStringFromDate(aReporting.AttachmentDate),
                        detachmentDate: (aReporting.DetachmentDate === null) ? '---' : clientDateStringFromDate(aReporting.DetachmentDate),
                        remarks: aReporting.Remarks,
                        status: aReporting.Status,
                        statusString: aReporting.StatusString
                    });
                });

                self.reportings(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        self.showToEdit = function (item) {
            window.location = '/EmployeeReporting/Update/' + item.id;
        };

        self.showDetail = function (item) {
            window.location = '/EmployeeReporting/Detail/' + item.id;
        };

        self.delete = function (id) {
            var obj = {
                id: id,
            };

            jax.postJsonBlock(
                '/EmployeeReporting/Delete',
                obj,
                function (data) {
                    ToastSuccess("Employee Reporting is deleted successfully");
                    self.getReportings(1, 25, self.searchFilers());
                },
                function (qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        };

        self.confirmToDelete = function (item) {
            bootbox.confirm("Are you sure, you want to delete this Employee Reporting ?", function (result) {
                if (result === true) {
                    self.delete(item.id);
                }
            });
        };
    }

    self.init = function () {
        self.resetFilers();
        self.getReportings(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeReporting"]'));
    };
}


$(document).ready(function () {

    var vm = new EmployeeReportingListViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
})