﻿
function EmployeeReportingUpdateViewModel() {
    var self = this;

    self.id = ko.observable(),
    self.employeeWorkStationUnitId = ko.observable(),
    self.parentWorkStationUnitId = ko.observable(),
    self.employeeWS = ko.observable(),
    self.parentWS = ko.observable(),
    self.attachmentDate = ko.observable(currentDate()).extend({ required: true }),
    self.detachmentDate = ko.observable(currentDate()).extend({ required: true }),
    self.remarks = ko.observable().extend({ maxLength: 150 }),
    self.status = ko.observable().extend({ required: true }),
    self.allStatuses = ko.observableArray([]),


    /*errors*/
    self.errors = ko.validation.group(self),
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; },
    self.showErrors = function () { self.errors.showAllMessages(); },
    self.removeErrors = function () { self.errors.showAllMessages(false); },


    /* actions */

    self.getEmployeeReporting = function () {
        var empReportingObj = {
            Id: self.id(),
            EmployeeWorkStationUnitId: self.employeeWorkStationUnitId(),
            ParentEmployeeWorkStationUnitId: self.parentWorkStationUnitId(),
            AttachmentDate: self.attachmentDate(),
            DetachmentDate: self.detachmentDate(),
            Remarks: self.remarks(),
            Status: self.status()
        };

        return empReportingObj;
    };

    self.load = function () {
        jax.getJson(
            '/EmployeeReporting/Get/' + self.id(),
            function (data) {
                self.employeeWS('Company: ' + data.EmployeeWorkStationUnit.WorkStationUnit.Company.Name + ' | Branch: ' + data.EmployeeWorkStationUnit.WorkStationUnit.Branch.Name + ' | Department: ' + data.EmployeeWorkStationUnit.WorkStationUnit.Department.Name);
                self.employeeWorkStationUnitId(data.EmployeeWorkStationUnitId),
                self.parentWS((data.ParentEmployeeWorkStationUnit === null) ? ' ---- ' : 'Company: ' + data.ParentEmployeeWorkStationUnit.WorkStationUnit.Company.Name + ' | Branch: ' + data.ParentEmployeeWorkStationUnit.WorkStationUnit.Branch.Name + ' | Department: ' + data.ParentEmployeeWorkStationUnit.WorkStationUnit.Department.Name);
                self.parentWorkStationUnitId(data.ParentEmployeeWorkStationUnitId);
                self.remarks(data.Remarks),
                self.attachmentDate(clientDate(data.AttachmentDate));
                self.detachmentDate(clientDate(data.DetachmentDate));
                self.status(data.StatusString);

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/EmployeeReporting/Update',
            self.getEmployeeReporting(),
            function () {
                ToastSuccess('Employee Reporting is updated successfully');
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    /*datepicker*/
    self.isAttachmentDateFocused = ko.observable(false);

    self.showAttachmentDate = function () {
        self.isAttachmentDateFocused(true);
    };

    self.resetAttachmentDate = function () {
        self.attachmentDate(currentDate());
    };

    self.isDetachmentDateFocused = ko.observable(false);

    self.showDetachmentDate = function () {
        self.isDetachmentDateFocused(true);
    };

    self.resetDetachmentDate = function () {
        self.detachmentDate(currentDate());
    };


    /*resets*/
    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.id($('#txtReportingId').val());

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeReporting"]'));
    };
};


$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new EmployeeReportingUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});