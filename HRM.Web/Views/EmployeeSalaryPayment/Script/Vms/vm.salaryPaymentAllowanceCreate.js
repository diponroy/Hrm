﻿function SalaryPaymentAllowanceCreateVm() {
    var self = this;

    self.callback = this.createdCallBack;

    self.allEmployeeSalaryStrucrureAllowance = ko.observableArray([]);

    self.employeeSalaryStructureAllowanceId = ko.observable().extend({ required: true });
    self.inAmount = ko.observable().extend({ required: true, digit: true });

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };


    self.getEmployeeSalaryStructureAllowances = function (id) {
        jax.getJson(
            '/EmployeeSalaryPaymentAllowance/GetEmployeeSalaryStructureAllowances/'+id,
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        //text: obj.EmployeeSalaryStructure.SalaryStructure.Title+' ---- '+obj.Allowance.Title,
                        text:obj.Allowance.Title,
                        value: obj.Id
                    });
                });
                self.allEmployeeSalaryStrucrureAllowance(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getSalaryStructureTitle = function (employeeSalaryStructureAllowanceId) {
        var result = $.grep(self.allEmployeeSalaryStrucrureAllowance(), function (e) { return e.value == employeeSalaryStructureAllowanceId; });
        return result[0].text;
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        this.createdCallBack({
            EmployeeSalaryStructureAllowanceId: self.employeeSalaryStructureAllowanceId(),
            InAmount: self.inAmount(),
            salaryTitleAllowance:self.getSalaryStructureTitle(self.employeeSalaryStructureAllowanceId())
        });
    };

    self.reset = function () {
        self.employeeSalaryStructureAllowanceId('');
        self.inAmount('');
        self.removeErrors();
    };

    self.init = function (employeeSalaryStructureId) {
        self.reset();
        //alert('after:' + employeeSalaryStructureId);
        self.getEmployeeSalaryStructureAllowances(employeeSalaryStructureId);
    };
}