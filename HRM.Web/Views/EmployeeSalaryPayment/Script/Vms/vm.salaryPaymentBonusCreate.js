﻿function SalaryPaymentBonusCreateVm() {
    var self = this;

    self.callback = this.createdCallBack;

    self.allEmployeeSalaryStrucrureBonus = ko.observableArray([]);

    self.employeeSalaryStructureBonusId = ko.observable().extend({ required: true });
    self.inAmount = ko.observable().extend({ required: true, digit: true });

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };


    self.getEmployeeSalaryStructureBonuses = function (id) {
        jax.getJson(
            '/EmployeeSalaryPaymentBonus/GetEmployeeSalaryStructureBonuses/'+id,
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        //text: obj.EmployeeSalaryStructure.SalaryStructure.Title + ' ---- ' + obj.Bonus.Title,
                        text: obj.BOnus.Title,
                        value: obj.Id
                    });
                });
                self.allEmployeeSalaryStrucrureBonus(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getSalaryStructureTitle = function (employeeSalaryStructureBonusId) {
        var result = $.grep(self.allEmployeeSalaryStrucrureBonus(), function (e) { return e.value == employeeSalaryStructureBonusId; });
        return result[0].text;
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        this.createdCallBack({
            EmployeeSalaryStructureBonusId: self.employeeSalaryStructureBonusId(),
            InAmount: self.inAmount(),
            salaryTitleBonus:self.getSalaryStructureTitle(self.employeeSalaryStructureBonusId())
        });
    };

    self.reset = function () {
        self.employeeSalaryStructureBonusId('');
        self.inAmount('');
        self.removeErrors();
    };

    self.init = function (employeeSalaryStructureId) {
        self.reset();
        self.getEmployeeSalaryStructureBonuses(employeeSalaryStructureId);
    };
}