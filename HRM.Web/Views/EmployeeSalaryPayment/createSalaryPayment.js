﻿/*View models*/
function EmployeeSalaryPaymentCreateViewModel() {
    var self = this;
    
    /* Modal Filds and Arrays and vms*/
    self.salaryPaymentAllowances = ko.observableArray([]);
    self.salaryPaymentBonuses = ko.observableArray([]);

    /* Modal View Models */
    self.salaryPaymentAllowanceCreate = new SalaryPaymentAllowanceCreateVm();
    self.salaryPaymentBonusCreate = new SalaryPaymentBonusCreateVm();
  

    /* Salary Payment form filds and Arrays */
    self.allEmployeeSalaryStructure = ko.observableArray([]);
    self.allPayrollInfo = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);
    self.employeeSalaryPaymentAllowances = ko.observableArray([]);

    self.employeeWorkStationUnitId = ko.observable();
    self.trackNo = ko.observable().extend({ required: true, maxLength: 50 });
    self.employeeSalaryStructureId = ko.observable().extend({ required: true });
    self.dateOfPayment = ko.observable(currentDate()).extend({ required: true });
    self.employeePayrollInfoId = ko.observable().extend({ required: true });
    self.totalInAmount = ko.observable().extend({ digit: true });
    self.status = ko.observable().extend({ required: true });
    

    /*Allowance and Bonus Create*/
    SalaryPaymentAllowanceCreateVm.prototype.createdCallBack = function (data) {
        //alert(data.EmployeeSalaryStructureAllowanceId + 'and ' + data.InAmount);

        $('#divSalaryPaymentAllowanceCreate').modal('hide');
        ToastSuccess("A new Salary Payment Allowance created successfully");

        var allowances = self.salaryPaymentAllowances();
        allowances.push(data);
        self.salaryPaymentAllowances(allowances);
    };
    
    SalaryPaymentBonusCreateVm.prototype.createdCallBack = function (data) {
        //alert(data.EmployeeSalaryStructureBonusId + 'and ' + data.inAmount);

        $('#divSalaryPaymentBonusCreate').modal('hide');
        ToastSuccess("A new Salary Payment Bonus created successfully");

        var bonuses = self.salaryPaymentBonuses();
        bonuses.push(data);
        self.salaryPaymentBonuses(bonuses);
    };

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function() { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function() { self.errors.showAllMessages(); };
    self.removeErrors = function() { self.errors.showAllMessages(false); };

    /*Get a Single id value*/
    //self.getEmployeeSalaryStructureId = function() {
    //    jax.getJson(
    //        '/EmployeeSalaryPayment/GetEmployeeSalaryStructureId/' + self.employeeWorkStationUnitId(),
    //        function (data) {
    //            self.employeeSalaryStructureId = data.Id;
    //        },
    //        function(qXhr, textStatus, error) {
    //            ToastError(error);
    //        }
    //    );
    //};

    /*objects to post*/
    self.postObj = function() {
        return {
            model: {
                TrackNo: self.trackNo(),
                EmployeeSalaryStructureId: self.employeeSalaryStructureId(),
                DateOfPayment: self.dateOfPayment(),
                employeePayrollInfoId: self.employeePayrollInfoId(),
                totalInAmount: self.totalInAmount(),
                Status: self.status()
            },
            allowanceList: self.salaryPaymentAllowances(),
            bonusList: self.salaryPaymentBonuses()
        };
    };

    self.getEmployeeSalaryStructures = function() {
        jax.getJson(
            '/EmployeeSalaryPayment/GetEmployeeSalaryStructures',
            function(data) {
                var array = [];
                $.each(data, function(index) {
                    var obj = data[index];
                    array.push({
                        //if(obj.EmployeeWorkStationUnit)
                        text: obj.EmployeeWorkStationUnit.Employee.FullName +'---'+obj.SalaryStructure.Title + '----' + obj.Basic,
                        value: obj.Id
                    });
                });
                self.allEmployeeSalaryStructure(array);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getEmployeePayrollInfos = function () {
        jax.getJson(
            '/EmployeeSalaryPayment/GetEmployeePayrollInfos' ,
            function(data) {
                var array = [];
                $.each(data, function(index) {
                    var obj = data[index];
                    array.push({
                        text: obj.BankName + ' ---- ' + obj.AccountNo,
                        value: obj.Id
                    });
                });
                self.allPayrollInfo(array);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        
        );
    };
    
    /* Load EmployeeSalaryStructureId and PayrollInfoId according employeeWorkStationUnitId */
    self.loadEmployeeSalaryStructure = function () {
        jax.postJson(
            '/EmployeeSalaryPayment/LoadEmployeeSalaryStructure',
            { id: self.employeeWorkStationUnitId() },
            function (data) {
                self.employeeSalaryStructureId(data.Id);
                //alert(self.employeeSalaryStructureId());
                self.loadPayrollInfo();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.loadPayrollInfo = function () {
        jax.postJson(
            '/EmployeeSalaryPayment/LoadPayrollInfo',
            { id: self.employeeWorkStationUnitId() },
            function (data) {
                self.employeePayrollInfoId(data.Id);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeSalaryPayment/Create' ,
            self.postObj(),
            function() {
                ToastSuccess('Added successfully');
                self.reset();
                self.salaryPaymentAllowances([]);
                self.salaryPaymentBonuses([]);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };
    
    /*initialize & resets*/
    self.reset = function () {
        
        var calls = [];
        calls.push(self.getEmployeeSalaryStructures());
        calls.push(self.getEmployeePayrollInfos());
        $.when.apply(this, calls).done(function () {
            $.when.apply(this, self.loadEmployeeSalaryStructure()).done(function () {
            self.removeErrors();
                });
            });

        self.trackNo('');
        self.dateOfPayment(currentDate());
        self.totalInAmount('');
        self.status('');
        self.removeErrors();
    };
    
    self.init = function() {
        self.employeeWorkStationUnitId($('#txtEmployeeWorkStationUnitId').val());
        
        self.reset();
        
        var status = [];
        status.push({ name: 'Paid', value: 0 });
        status.push({ name: 'Pending', value: 1 });
        self.allStatus(status);
        
        activeParentMenu($('li.sub a[href="/EmployeeSalaryPayment"]'));
    };
}

$(document).ready(function() {
    $('.datepicker').datepicker();

    var vm = new EmployeeSalaryPaymentCreateViewModel();
    ko.applyBindings(vm);
    vm.init();

    $('#showDateOfPayment').click(function() {
        $('#btnDateOfPayment').focus();
    });
    $('#resetDateOfPayment').click(function() {
        vm.dateOfPayment(currentDate());
    });

    /*Pop Up Allowance modal*/
    $("#btnAllowance").click(function () {
        //alert(vm.employeeSalaryStructureId());
        vm.salaryPaymentAllowanceCreate.init(vm.employeeSalaryStructureId());
        $('#divSalaryPaymentAllowanceCreate').removeData("modal").modal();
    });
    
    /*Pop Up Bonus modal*/
    $("#btnBonus").click(function () {
        //vm.salaryPaymentBonusCreate.reset();
        vm.salaryPaymentBonusCreate.init(vm.employeeSalaryStructureId());
        $('#divSalaryPaymentBonusCreate').removeData("modal").modal();
    });
});




