﻿function EmployeeSalaryPaymentListViewModel() {
    var self = this;
    self.employeeSalaryPayments = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.employeeSalaryPayments([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/EmployeeSalaryPayment/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var empSalaryPayment = data[index];
                    array.push({
                        id: empSalaryPayment.Id,
                        empSalaryStructureTitle: empSalaryPayment.EmployeeSalaryStructure.SalaryStructure.Title,
                        empSalaryStructureBasic: empSalaryPayment.EmployeeSalaryStructure.SalaryStructure.Basic,
                        paymentDate: clientDateStringFromDate(empSalaryPayment.DateOfPayment),
                        bankName: empSalaryPayment.EmployeePayrollInfo.BankName,
                        account:empSalaryPayment.EmployeePayrollInfo.AccountNo,
                        total: empSalaryPayment.TotalInAmount,
                        status: empSalaryPayment.Status,
                        statusString: empSalaryPayment.StatusString == 'Active' ? 'Paid' : 'Pending'
                });
                });
                self.employeeSalaryPayments(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/Allowance/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/Allowance/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/Allowance/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.confirmChange = function () {
        bootbox.confirm("You have to select a employee first !!!", function (result) {
            if (result === true) {
                self.changeLocation();
            }
        });
    };
    self.changeLocation = function () {
        window.location = '/EmployeeWorkStationUnitManage/List/';
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        activeParentMenu($('li.sub a[href="/EmployeeSalaryPayment"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeSalaryPaymentListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});