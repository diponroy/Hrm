﻿function EmployeeSalaryAllowanceCreateVm() {
	var self = this;

	self.callback = this.createdCallBack;
	self.allAllowanceTitles = ko.observableArray([]);
	self.allowances = ko.observableArray([]);

	self.allowanceId = ko.observable().extend({ required: true });
    self.asPercentage = ko.observable();
	self.weight = ko.observable().extend({digit:true});
	self.attachmentDate = ko.observable(currentDate()).extend({ required: true });
    
    ///Date focus on pop-up
    self.isDatePickerFocus = ko.observable(false);
	self.showAttachmentDate = function() {
	    self.isDatePickerFocus(true);
	};
    self.resetAttachmentDate = function() {
        self.attachmentDate(currentDate());
    };

	self.errors = ko.validation.group(self);
	self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
	self.showErrors = function () { self.errors.showAllMessages(); };
	self.removeErrors = function () { self.errors.showAllMessages(false); };

	self.vmObj = function () {
		return {
			AllowanceId: self.allowanceId(),
			AsPercentage: self.hasPercentage(),
			Weight: self.weight(),
			AttachmentDate:self.attachmentDate()
		};
	};
    
    //get field value asPercentage()
	self.hasPercentage = function () {
	    if (self.asPercentage())
	        return true;
	    return false;
	};


	self.getAllowanceTitles = function () {
		jax.getJson(
            '/EmployeeSalaryStructureAllowanceManage/GetAllowanceTitles',
            function (data) {
            	var array = [];
            	$.each(data, function (index) {
            		var obj = data[index];
            		array.push({
            			text: obj.Title,
            			value: obj.Id
            		});
            	});
            	self.allAllowanceTitles(array);
            },
            function (qXhr, textStatus, error) {
            	ToastError(error);
            }
        );
	};
    
	//title show in list
	self.getTitle = function (allowanceId) {
		var result = $.grep(self.allAllowanceTitles(), function (e) { return e.value == allowanceId; });
		return result[0].text;
	};

	self.create = function () {
		if (self.hasErrors()) {
			self.showErrors();
			return;
		}
		this.createdCallBack({
			allowanceId: self.allowanceId(),
			asPercentage: self.asPercentage(),
			weight: self.weight(),
			attachmentDate:self.attachmentDate(),
			allowanceTitle: self.getTitle(self.allowanceId())
		});
	};

	self.reset = function () {
		self.allowanceId('');
		self.asPercentage('');
		self.weight('');
		self.attachmentDate(currentDate());
		self.removeErrors();
	};

	self.init = function () {
	    self.reset();
		self.getAllowanceTitles();
	};
}