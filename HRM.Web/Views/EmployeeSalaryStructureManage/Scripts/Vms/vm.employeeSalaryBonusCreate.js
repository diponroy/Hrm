﻿function EmployeeSalaryBonusCreateVm() {
    var self = this;

    self.callback = this.createdCallBack;
    self.allBonusTitles = ko.observableArray([]);
    self.bonuses = ko.observableArray([]);

    self.bonusId = ko.observable().extend({ required: true });
    self.asPercentage = ko.observable();
    self.weight = ko.observable().extend({digit:true});
    self.attachmentDate = ko.observable(currentDate()).extend({ required: true });

    ///Date focus on pop-up
    self.isDatePickerFocus = ko.observable(false);
    self.showAttachmentDate = function () {
        self.isDatePickerFocus(true);
    };
    self.resetAttachmentDate = function () {
        self.attachmentDate(currentDate());
    };

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.vmObj = function () {
        return {
            bonusId: self.bonusId(),
            AsPercentage: self.hasPercentage(),
            Weight: self.weight(),
            AttachmentDate: self.attachmentDate()
        };
    };

    //get field value asPercentage()
    self.hasPercentage = function () {
        if (self.asPercentage())
            return true;
        return false;
    };


    self.getBonusTitles = function () {
        jax.getJson(
            '/EmployeeSalaryStructureBonusManage/GetBonusTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allBonusTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    //title show in list
    self.getTitle = function (bonusId) {
        var result = $.grep(self.allBonusTitles(), function (e) { return e.value == bonusId; });
        return result[0].text;
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        this.createdCallBack({
            bonusId: self.bonusId(),
            asPercentage: self.asPercentage(),
            weight: self.weight(),
            attachmentDate: self.attachmentDate(),
            bonusTitle: self.getTitle(self.bonusId())
        });
    };

    self.reset = function () {
        self.bonusId('');
        self.asPercentage('');
        self.weight('');
        self.attachmentDate(currentDate());
        self.removeErrors();
    };

    self.init = function () {
        self.reset();
        self.getBonusTitles();
    };
}