﻿
function EmployeeSalaryStructureViewModel() {
    var self = this;

    /*DDL*/
    self.salaryStructures = ko.observableArray([]);
    self.employeeWorkStationUnits = ko.observableArray([]);
    self.allStatuses = ko.observableArray([]);
    
    /*Pop-Up List Array*/
    self.employeeSalaryStructureAllowances = ko.observableArray([]);
    self.employeeSalaryStructureBonuses = ko.observableArray([]);
    
    /*pop-up view model*/
    self.employeeSalaryAllowanceCreate = new EmployeeSalaryAllowanceCreateVm();
    self.employeeSalaryBonusCreate = new EmployeeSalaryBonusCreateVm();


    EmployeeSalaryAllowanceCreateVm.prototype.createdCallBack = function(data) {
        $('#divEmployeeSalaryAllowanceCreate').modal('hide');
        ToastSuccess("A new Employee Salary Structure Allowance created successfully");

        data.attachmentDate = clientDateStringFromDate(data.attachmentDate);
        var allowances = self.employeeSalaryStructureAllowances();
        allowances.push(data);
        self.employeeSalaryStructureAllowances(allowances);
    };
    EmployeeSalaryBonusCreateVm.prototype.createdCallBack = function(data) {
        $('#divEmployeeSalaryBonusCreate').modal('hide');
        ToastSuccess("A new Employee Salary Structure Bonus created successfully");

        data.attachmentDate = clientDateStringFromDate(data.attachmentDate);
        var bonuses = self.employeeSalaryStructureBonuses();
        bonuses.push(data);
        self.employeeSalaryStructureBonuses(bonuses);
    };
    
    /*fields*/
    self.salaryStructureId = ko.observable().extend({ required: true });
    self.basic = ko.observable().extend({ required: true, digit: true });
    self.employeeWorkStationUnitId = ko.observable().extend({ required: true });
    self.attachmentDate = ko.observable(currentDate()).extend({ required: true });
    self.status = ko.observable().extend({ required: true });
    

    self.vmObj = function() {
        return {
            model: {
                SalaryStructureId: self.salaryStructureId(),
                Basic: self.basic(),
                EmployeeWorkstationUnitId: self.employeeWorkStationUnitId(),
                AttachmentDate: self.attachmentDate(),
                Status: self.status()
            },
            allowanceList: self.employeeSalaryStructureAllowances(),
            bonusList: self.employeeSalaryStructureBonuses()
        };
    };
    
    //self.getAllowanceBasic = function (salaryStructureId) {
    //    var result = $.grep(self.salaryStructures().SalaryStructureAllowances, function (e) { return e.value == allowanceId; });
    //    return result[0].text;
    //};

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function() { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function() { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /* Actions */
    self.loadSalaryStructures = function() {
        self.salaryStructures([]);
        jax.getJson(
            '/EmployeeSalaryStructureManage/GetSalaryStructures',
            function(data) {
                var arr = new Array();
                $.each(data, function(index) {
                    var salaryStructure = data[index];
                    //two list in one drop-down
                    //$.each(salaryStructure.SalaryStructureAllowances, function(i) {
                    //    var salaryAllowance = salaryStructure.SalaryStructureAllowances[i];
                    //    var option = salaryStructure.Title + '|' + (salaryAllowance == undefined ? 'no Allowance' : salaryAllowance.Remarks);
                    //    arr.push({
                    //        title: option,
                    //        salaryId: salaryStructure.Id
                    //    });
                    //    self.salaryStructures(arr);
                    //});
                    arr.push({
                        title: salaryStructure.Title,
                        salaryId: salaryStructure.Id
                    });
                    self.salaryStructures(arr);
                }),
                function(qXhr, textStatus, error) {
                    ToastError(error);
                };
            });
    };

    self.loadWorkStationUnits = function() {
        self.employeeWorkStationUnits([]);
        jax.getJson(
            '/EmployeeSalaryStructureManage/GetEmployeeWorkStationUnits',
            function(data) {
                var arr = new Array();
                $.each(data, function(index) {
                    var obj = data[index];
                    var option = obj.Employee.FullName + ' | ' + obj.WorkStationUnit.Company.Name + ' | ' + obj.WorkStationUnit.Branch.Name + ' | ' + obj.WorkStationUnit.Department.Name;
                    arr.push({
                        unit: option,
                        unitId: obj.Id,
                    });
                });
                self.employeeWorkStationUnits(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function() {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeSalaryStructureManage/Create',
            self.vmObj(),
            function(data) {
                ToastSuccess('Employee Salary Structure created successfully');
                self.reset();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.confirmDeleteAllowance = function(item) {
        bootbox.confirm("Do you want to delete the Allowance?", function(result) {
            if (result === true) {
                self.employeeSalaryStructureAllowances.remove(item);
                ToastSuccess("Information deleted successfully");
            }
        });
    };
    
    self.confirmDeleteBonus = function (item) {
        bootbox.confirm("Do you want to delete the Bonus?", function (result) {
            if (result === true) {
                self.employeeSalaryStructureBonuses.remove(item);
                ToastSuccess("Information deleted successfully");
            }
        });
    };

    self.reset = function() {
        self.salaryStructureId('');
        self.basic('');
        self.employeeWorkStationUnitId('');
        self.attachmentDate(currentDate());
        self.status('');
        self.removeErrors();
    };

    self.init = function() {
        self.loadSalaryStructures();
        self.loadWorkStationUnits();
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);
        self.reset();

        //initialize two vm
        self.employeeSalaryAllowanceCreate.init();
        self.employeeSalaryBonusCreate.init();
        
        activeParentMenu($('li.sub a[href="/EmployeeSalaryStructureManage"]'));
    };
}

$(document).ready(function() {
    $('.datepicker').datepicker();

    var vm = new EmployeeSalaryStructureViewModel();
    ko.applyBindings(vm);
    vm.init();

    $('#btnShowDatePicker').click(function() {
        $('.datepicker').focus();
    });

    $('#btnResetDatePicker').click(function() {
        vm.attachmentDate(currentDate());
    });

    //Call pop-up form
    $("#btnAllowance").click(function() {
        vm.employeeSalaryAllowanceCreate.reset();
        $('#divEmployeeSalaryAllowanceCreate').removeData("modal").modal();
    });
    
    $("#btnBonus").click(function () {
        vm.employeeSalaryBonusCreate.reset();
        $('#divEmployeeSalaryBonusCreate').removeData("modal").modal();
    });
});