﻿function EmployeeSalaryStructureViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employeeSalaryStructure = ko.observable();
    self.logs = ko.observable([]);

    self.getEmployeeSalaryStructure = function () {
        jax.getJsonBlock(
            '/EmployeeSalaryStructureManage/Get/' + self.id(),
            function (data) {
                self.employeeSalaryStructure({
                    structureTitle: data.SalaryStructure.Title,
                    employeeName: (data.EmployeeWorkStationUnit.Employee) == null ? '--' : data.EmployeeWorkStationUnit.Employee.FullName,
                    basic: data.Basic,
                    companyName: (data.EmployeeWorkStationUnit.WorkStationUnit.Company) == null ? '--' : data.EmployeeWorkStationUnit.WorkStationUnit.Company.Name,
                    branchName: (data.EmployeeWorkStationUnit.WorkStationUnit.Branch) == null ? '--' : data.EmployeeWorkStationUnit.WorkStationUnit.Branch.Name,
                    departmentName: (data.EmployeeWorkStationUnit.WorkStationUnit.Department) == null ? '--' : data.EmployeeWorkStationUnit.WorkStationUnit.Department.Name,
                    attachmentDate: clientDateStringFromDate(data.AttachmentDate),
                    detachmentDate: (data.DetachmentDate) == null ? '--' : clientDateStringFromDate(data.DetachmentDate)
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeSalaryStructureManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeSalaryStructure = data[index];
                    array.push({
                        id: employeeSalaryStructure.Id,
                        structureTitle: employeeSalaryStructure.SalaryStructureLog.Title,
                        basic: employeeSalaryStructure.Basic,
                        
                        employeeName: (employeeSalaryStructure.EmployeeWorkstationUnitLog.EmployeeLog) == null ? '--' : employeeSalaryStructure.EmployeeWorkstationUnitLog.EmployeeLog.FullName,
                        companyName: (employeeSalaryStructure.EmployeeWorkstationUnitLog.WorkStationUnitLog.CompanyLog) == null ? '--' : employeeSalaryStructure.EmployeeWorkstationUnitLog.WorkStationUnitLog.CompanyLog.Name,
                        branchName: (employeeSalaryStructure.EmployeeWorkstationUnitLog.WorkStationUnitLog.BranchLog) == null ? '--' : employeeSalaryStructure.EmployeeWorkstationUnitLog.WorkStationUnitLog.BranchLog.Name,
                        departmentName: (employeeSalaryStructure.EmployeeWorkstationUnitLog.WorkStationUnitLog.DepartmentLog) == null ? '--' : employeeSalaryStructure.EmployeeWorkstationUnitLog.WorkStationUnitLog.DepartmentLog.Name,

                        attachmentDate: clientDateStringFromDate(employeeSalaryStructure.AttachmentDate),
                        detachmentDate: (employeeSalaryStructure.DetachmentDate === null) ? '--' : clientDateStringFromDate(employeeSalaryStructure.DetachmentDate),
                        isDetached: employeeSalaryStructure.DetachmentDate != null,
                        status: employeeSalaryStructure.Status,
                        statusString: employeeSalaryStructure.StatusString,

                        affectedByEmployeeId: employeeSalaryStructure.AffectedByEmployeeId,
                        affectedByName: employeeSalaryStructure.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(employeeSalaryStructure.AffectedDateTime),
                        logStatus: employeeSalaryStructure.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*resets*/
    self.reset = function () {
        self.getEmployeeSalaryStructure();
        self.getLogs();
    };

    self.init = function () {
        self.id($('#employeeSalaryStructureId').val());
        self.getEmployeeSalaryStructure();
        self.getLogs();
    };
}


$(document).ready(function () {
    var vm = new EmployeeSalaryStructureViewModel();
    vm.init();
    ko.applyBindings(vm);
});

