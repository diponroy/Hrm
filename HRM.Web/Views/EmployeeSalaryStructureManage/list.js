﻿function EmployeeSalaryStructureViewModel() {
    var self = this;
    self.employeeSalaryStructures = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getAllList = function (pageNo, pageSize, searchFilters) {
        self.employeeSalaryStructures([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/EmployeeSalaryStructureManage/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var modelObj = data[index];
                    array.push({
                        id: modelObj.Id,
                        salaryTitle: modelObj.SalaryStructure.Title,
                        employee: modelObj.EmployeeWorkStationUnit.Employee.FullName,
                        company: modelObj.EmployeeWorkStationUnit.WorkStationUnit.Company.Name,
                        branch: modelObj.EmployeeWorkStationUnit.WorkStationUnit.Branch.Name,
                        department: modelObj.EmployeeWorkStationUnit.WorkStationUnit.Department.Name,
                        workStationName: modelObj.EmployeeWorkStationUnit.WorkStationUnit.Remarks,
                        basic: modelObj.Basic,
                        attachmentDate: clientDateStringFromDate(modelObj.AttachmentDate),
                        detachmentDate: (modelObj.DetachmentDate === null) ? '--' : clientDateStringFromDate(modelObj.DetachmentDate),
                        IsDetached: modelObj.DetachmentDate != null,
                        status: modelObj.Status,
                        statusString: modelObj.StatusString
                    });
                });
                self.employeeSalaryStructures(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/EmployeeSalaryStructureManage/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeSalaryStructureManage/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the Company ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/EmployeeSalaryStructureManage/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getAllList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getAllList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeSalaryStructureManage"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeSalaryStructureViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});