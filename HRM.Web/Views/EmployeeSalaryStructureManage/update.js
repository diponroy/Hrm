﻿function EmployeeSalaryStructureViewModel() {
    var self = this;
    self.id = ko.observable().extend({required:true});
    self.salaryStructureId = ko.observable().extend({ required: true });
    self.salaryStructures = ko.observableArray([]);
    self.basic = ko.observable().extend({ required: true, digit: true });
    self.employeeWorkStationUnitId = ko.observable().extend({ required: true });
    self.employeeWorkStationUnits = ko.observableArray([]);
    self.attachmentDate = ko.observable(currentDate()).extend({ required: true });
    self.detachmentDate = ko.observable(currentDate()).extend({ required: true });
    self.hasClosingDate = ko.observable(false);
    self.status = ko.observable().extend({ required: true });
    self.allStatuses = ko.observableArray([]);

    self.errorsWithoutStatus = ko.validation.group([self.salaryStructureId, self.basic, self.employeeWorkStationUnitId, self.attachmentDate, self.detachmentDate]);
    self.errorsWithStatus = ko.validation.group([self.salaryStructureId, self.basic, self.employeeWorkStationUnitId, self.attachmentDate, self.status]);
    self.errors = ko.computed(function () {
        return self.hasClosingDate() ? self.errorsWithoutStatus : self.errorsWithStatus;
    }, this);
    self.hasErrors = function () {
        return ((self.errors()()).length > 0) ? true : false;
    };
    self.showErrors = function () {self.errors().showAllMessages();};
    self.removeErrors = function () {self.errors().showAllMessages(false);};

    self.getUpdateDetail = function () {
        return {
            Id: self.id(),
            SalaryStructureId: self.salaryStructureId(),
            Basic: self.basic(),
            EmployeeWorkstationUnitId: self.employeeWorkStationUnitId(),
            AttachmentDate: self.attachmentDate(),
            DetachmentDate: self.detachmentDate(),
            Status: (self.hasClosingDate()) ? null : self.status()
        };
    };

    self.setClosedDateTime = function () {
        self.hasClosingDate(true);
        self.detachmentDate(currentDate());
    };

    self.removeClosedDateTime = function () {
        self.hasClosingDate(false);
        self.detachmentDate(null);
    };
    
    self.loadSalaryStructures = function () {
        self.salaryStructures([]);
        return jax.getJson(
            '/EmployeeSalaryStructureManage/GetSalaryStructures',
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var salaryStructure = data[index];
                    arr.push({
                        title: salaryStructure.Title,
                        salaryId: salaryStructure.Id
                    });
                });
                self.salaryStructures(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadWorkStationUnits = function () {
        self.employeeWorkStationUnits([]);
        return jax.getJson(
            '/EmployeeSalaryStructureManage/GetEmployeeWorkStationUnits',
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var obj = data[index];
                    var option = obj.Employee.FullName + ' | ' + obj.WorkStationUnit.Company.Name + ' | ' + obj.WorkStationUnit.Branch.Name + ' | ' + obj.WorkStationUnit.Department.Name;
                    arr.push({
                        unit: option,
                        unitId: obj.Id,
                    });
                });
                self.employeeWorkStationUnits(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*get all*/
    self.load = function () {
        jax.getJson(
           '/EmployeeSalaryStructureManage/Get/' + self.id(),
            function (data) {
                self.salaryStructureId(data.SalaryStructureId);
                self.basic(data.Basic),
                self.employeeWorkStationUnitId(data.EmployeeWorkstationUnitId);
                self.attachmentDate(clientDate(data.AttachmentDate));
                if (data.DetachmentDate != null) {
                    self.hasClosingDate(true);
                    self.detachmentDate(clientDate(data.DetachmentDate));
                } else {
                    self.removeClosedDateTime();
                }
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*Server Actions*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeSalaryStructureManage/Update',
            self.getUpdateDetail(),
            function () {
                ToastSuccess('Information updated successfully');
                self.load(self.id());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
         );
    };

    self.reset = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);
        
        var calls = [];
        calls.push(self.loadSalaryStructures());
        calls.push(self.loadWorkStationUnits(self.id()));
        $.when.apply(this, calls).done(function () {
            $.when.apply(this, self.load(self.id())).done(function () {
                self.removeErrors();
            });
        });
    };

    self.init = function () {
        self.id($('#employeeSalaryStructureId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeSalaryStructureManage"]'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();

    var viewModel = new EmployeeSalaryStructureViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();

    $('#btnShowDatePicker').click(function () {
        $('#btnAttachmentDate').focus();
    });
    $('#btnResetDatePicker').click(function () {
        viewModel.attachmentDate(currentDate());
    });

    $('#btnShowClosing').click(function () {
        $('#btnDetachmentDate').focus();
    });

    $('#btnResetClosing').click(function () {
        viewModel.detachmentDate(currentDate());
    });
});