﻿function EmployeeTrainingAttachmentViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employeeTrainingAttachment = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getEmployeeTrainingAttachment = function () {
        jax.getJsonBlock(
            '/EmployeeTrainingAttachmentManage/Get/' + self.id(),
            function (data) {
                self.employeeTrainingAttachment({
                    title: data.Title,
                    directory:data.Directory,
                    trainingTitle: data.EmployeeTraining.Title,
                    remarks: data.Remarks
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeTrainingAttachmentManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var attachmentObj = data[index];
                    array.push({
                        id: attachmentObj.Id,
                        title: attachmentObj.Title,
                        directory: attachmentObj.Directory,
                        trainingTitle: attachmentObj.EmployeeTrainingLog.Title,
                        remarks: attachmentObj.Remarks,
                        status: attachmentObj.Status,
                        statusString: attachmentObj.StatusString,
                        affectedByEmployeeName: attachmentObj.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(attachmentObj.affectedDateTime),
                        logStatus: attachmentObj.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#txtEmployeeTrainingAttachmentId').val());
        self.getEmployeeTrainingAttachment();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeTrainingAttachmentViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});