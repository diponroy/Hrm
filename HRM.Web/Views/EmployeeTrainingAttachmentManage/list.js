﻿function EmployeeTrainingAttachmentListViewModel() {
    var self = this;
    self.employeeTrainingAttachments = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.employeeTrainingAttachments([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/EmployeeTrainingAttachmentManage/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeTrainingAttachment = data[index];
                    array.push({
                        id: employeeTrainingAttachment.Id,
                        title: employeeTrainingAttachment.Title,
                        directory: employeeTrainingAttachment.Directory,
                        employeeTrainingTitle: employeeTrainingAttachment.EmployeeTraining.Title,
                        remarks: employeeTrainingAttachment.Remarks,
                        status: employeeTrainingAttachment.Status,
                        statusString: employeeTrainingAttachment.StatusString
                    });
                });
                self.employeeTrainingAttachments(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/EmployeeTrainingAttachmentManage/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeTrainingAttachmentManage/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/EmployeeTrainingAttachmentManage/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeTrainingAttachmentManage"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeTrainingAttachmentListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});