﻿/*View models*/
function EmployeeTrainingAttachmentUpdateViewModel() {
    var self = this;
    
    self.allTrainingTitles = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);
    
    self.id = ko.observable();
    self.employeeTrainingId = ko.observable().extend({ required: true });
    self.title = ko.observable().extend({ required: true, maxLength: 100 });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.directory = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Id:self.id(),
            Title: self.title(),
            EmployeeTrainingId: self.employeeTrainingId(),
            Directory: self.directory(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.getTrainingTitles = function () {
        jax.getJson(
            '/EmployeeTrainingAttachmentManage/GetTrainingTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allTrainingTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeTrainingAttachment = function () {
        jax.getJson(
            '/EmployeeTrainingAttachmentManage/Get/' + self.id(),
            function (data) {
                self.id(data.Id);
                self.title(data.Title);
                self.employeeTrainingId(data.EmployeeTrainingId);
                self.directory(data.Directory);
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeTrainingAttachmentManage/Update',
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadEmployeeTrainingAttachment();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.getTrainingTitles();
        self.loadEmployeeTrainingAttachment();
    };
    self.init = function () {
        self.getTrainingTitles();
        self.id($('#txtemployeeTrainingAttachmentId').val());
        
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.loadEmployeeTrainingAttachment();
        
        $('#uploadFile').click(function () {
            var formdata = new FormData();
            var fileInput = document.getElementById('fileInput');

            for (i = 0; i < fileInput.files.length; i++) {
                formdata.append(fileInput.files[i].name, fileInput.files[i]);
            }

            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/EmployeeTrainingAttachmentManage/Upload');
            xhr.send(formdata);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    if (xhr.responseText.indexOf("Uploaded Files") != -1) {
                        self.directory(xhr.responseText);
                        ToastSuccess("File successfully been uploaded.");
                    } else {
                        ToastError("Error while uploading or" + "<br>File format is not supported !!!");
                    }
                }
            };
            return false;
        });

        activeParentMenu($('li.sub a[href="/EmployeeTrainingAttachmentManage"]'));
    };
}

$(document).ready(function () {

    var vm = new EmployeeTrainingAttachmentUpdateViewModel();
    ko.applyBindings(vm);
    vm.init();
});