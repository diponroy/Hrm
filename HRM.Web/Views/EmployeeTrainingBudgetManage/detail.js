﻿function EmployeeTrainingBudgetViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employeeTrainingBudget = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getEmployeeTrainingBudget = function () {
        jax.getJsonBlock(
            '/EmployeeTrainingBudgetManage/Get/' + self.id(),
            function (data) {
                self.employeeTrainingBudget({
                    trainingTitle: data.EmployeeTraining.Title,
                    description: data.Description,
                    totalAmount:data.TotalAmount,
                    remarks: data.Remarks
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeTrainingBudgetManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var budgetObj = data[index];
                    array.push({
                        id: budgetObj.Id,
                        trainingTitle: budgetObj.EmployeeTrainingLog.Title,
                        description: budgetObj.Description,
                        totalAmount: budgetObj.TotalAmount,
                        isApproved: budgetObj.IsApproved,
                        remarks: budgetObj.Remarks,
                        status: budgetObj.Status,
                        statusString: budgetObj.StatusString,
                        affectedByEmployeeName: budgetObj.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(budgetObj.affectedDateTime),
                        logStatus: budgetObj.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#employeeTrainingBudgetId').val());
        self.getEmployeeTrainingBudget();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeTrainingBudgetViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});