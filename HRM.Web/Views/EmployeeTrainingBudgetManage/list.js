﻿function EmployeeTrainingBudgetListViewModel() {
    var self = this;
    self.employeeTrainingBudgets = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.employeeTrainingBudgets([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/EmployeeTrainingBudgetManage/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeTrainingBudget = data[index];
                    array.push({
                        id: employeeTrainingBudget.Id,
                        description: employeeTrainingBudget.Description,
                        totalAmount: employeeTrainingBudget.TotalAmount,
                        isApproved: employeeTrainingBudget.IsApproved,
                        employeeTrainingTitle:employeeTrainingBudget.EmployeeTraining.Title,
                        remarks: employeeTrainingBudget.Remarks,
                        status: employeeTrainingBudget.Status,
                        statusString: employeeTrainingBudget.StatusString
                    });
                });
                self.employeeTrainingBudgets(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/EmployeeTrainingBudgetManage/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeTrainingBudgetManage/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/EmployeeTrainingBudgetManage/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeTrainingBudgetManage"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeTrainingBudgetListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});