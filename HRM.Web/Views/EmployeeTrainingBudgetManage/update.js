﻿/*View models*/
function EmployeeTrainingBudgetUpdateViewModel() {
    var self = this;
    self.id = ko.observable();
    self.allTrainingTitles = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.employeeTrainingId = ko.observable().extend({ required: true });
    self.description = ko.observable().extend({ required: true, maxLength: 250 });
    self.totalAmount = ko.observable().extend({ required: true, digit: true });
    self.isApproved = ko.observable();
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Id: self.id(),
            EmployeeTrainingId: self.employeeTrainingId(),
            Description: self.description(),
            TotalAmount: self.totalAmount(),
            IsApproved: self.hasApproved(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.hasApproved = function () {
        if (self.isApproved())
            return true;
        return false;
    };
    
    self.getTrainingTitles = function () {
        jax.getJson(
            '/EmployeeTrainingBudgetManage/GetTrainingTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allTrainingTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeTrainingBudget = function () {
        jax.getJson(
            '/EmployeeTrainingBudgetManage/Get/' + self.id(),
            function (data) {
                self.employeeTrainingId(data.EmployeeTrainingId);
                self.description(data.Description);
                self.totalAmount(data.TotalAmount);
                self.isApproved(data.IsApproved);
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeTrainingBudgetManage/Update',
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadEmployeeTrainingBudget();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.getTrainingTitles();
        self.loadEmployeeTrainingBudget();
    };
    self.init = function () {
        self.getTrainingTitles();
        self.id($('#employeeTrainingBudgetId').val());
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.loadEmployeeTrainingBudget();
        
        activeParentMenu($('li.sub a[href="/EmployeeTrainingBudgetManage"]'));
    };
}

$(document).ready(function () {
    
    var vm = new EmployeeTrainingBudgetUpdateViewModel();
    ko.applyBindings(vm);
    vm.init();
});