﻿/*View models*/
function EmployeeTrainingCreateViewModel() {
    var self = this;
    self.title = ko.observable().extend({ required: true, maxLength: 100 });
    self.description = ko.observable().extend({ maxLength: 250 });
    self.organizer = ko.observable().extend({ maxLength: 150 });
    self.venue = ko.observable().extend({ maxLength: 150 });
    self.trainer = ko.observable().extend({ maxLength: 100 });
    self.fromDate = ko.observable(currentDate());
    self.toDate = ko.observable(currentDate());
    self.isApproved = ko.observable();
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Title: self.title(),
            Description: self.description(),
            Organizer: self.organizer(),
            Venue: self.venue(),
            Trainer: self.trainer(),
            IsApproved: self.hasApproved(),
            Remarks: self.remarks(),
            DurationFromDate: self.fromDate(),
            DurationToDate: self.toDate(),
            Status: self.status()
        };
    };

    self.hasApproved = function () {
        if (self.isApproved())
            return true;
        return false;
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeTrainingManage/Create',
            self.postObj(),
            function () {
                ToastSuccess('Information Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.title('');
        self.description('');
        self.organizer('');
        self.venue('');
        self.trainer('');
        self.isApproved('');
        self.remarks('');
        self.fromDate(currentDate());
        self.toDate(currentDate());
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeTrainingManage"]'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();
    
    var vm = new EmployeeTrainingCreateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
    
    $('#btnShowFromDate').click(function () {
        $('#btnFromDate').focus();
    });
    $('#btnResetFromDate').click(function () {
        vm.fromDate(currentDate());
    });
    
    $('#btnShowToDate').click(function () {
        $('#btnToDate').focus();
    });
    $('#btnResetToDate').click(function () {
        vm.toDate(currentDate());
    });
});