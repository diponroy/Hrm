﻿function EmployeeTrainingViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employeeTraining = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getEmployeeTraining = function () {
        jax.getJsonBlock(
            '/EmployeeTrainingManage/Get/' + self.id(),
            function (data) {
                self.employeeTraining({
                    title: data.Title,
                    description: data.Description,
                    organizer: data.Organizer,
                    venue: data.Venue,
                    trainer: data.Trainer,
                    fromDate: clientDateStringFromDate(data.DurationFromDate),
                    toDate: clientDateStringFromDate(data.DurationToDate),
                    remarks: data.Remarks
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeTrainingManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var allowance = data[index];
                    array.push({
                        id: allowance.Id,
                        title: allowance.Title,
                        description: allowance.Description,
                        organizer: allowance.Organizer,
                        venue: allowance.Venue,
                        trainer: allowance.Trainer,
                        isApproved: allowance.IsApproved,
                        remarks: allowance.Remarks,
                        fromDate: clientDateStringFromDate(allowance.DurationFromDate),
                        toDate: clientDateStringFromDate(allowance.DurationToDate),
                        status: allowance.Status,
                        statusString: allowance.StatusString,
                        affectedByEmployeeName: allowance.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(allowance.affectedDateTime),
                        logStatus: allowance.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#employeeTrainingId').val());
        self.getEmployeeTraining();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeTrainingViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});