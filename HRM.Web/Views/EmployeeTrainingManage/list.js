﻿function EmployeeTrainingListViewModel() {
    var self = this;
    self.employeeTrainings = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getList = function (pageNo, pageSize, searchFilters) {
        self.employeeTrainings([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/EmployeeTrainingManage/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeTraining = data[index];
                    array.push({
                        id: employeeTraining.Id,
                        title: employeeTraining.Title,
                        description: employeeTraining.Description,
                        organizer: employeeTraining.Organizer,
                        venue: employeeTraining.Venue,
                        trainer: employeeTraining.Trainer,
                        isApproved: employeeTraining.IsApproved,
                        remarks: employeeTraining.Remarks,
                        durationFromDate: clientDateStringFromDate(employeeTraining.DurationFromDate),
                        durationToDate: clientDateStringFromDate(employeeTraining.DurationToDate),
                        status: employeeTraining.Status,
                        statusString: employeeTraining.StatusString
                    });
                });
                self.employeeTrainings(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/EmployeeTrainingManage/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeTrainingManage/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/EmployeeTrainingManage/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeTrainingManage"]'));
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeTrainingListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});