﻿/*View models*/
function EmployeeTrainingUpdateViewModel() {
    var self = this;
    self.id = ko.observable();
    self.title = ko.observable().extend({ required: true, maxLength: 100 });
    self.description = ko.observable().extend({ maxLength: 250 });
    self.organizer = ko.observable().extend({ maxLength: 150 });
    self.venue = ko.observable().extend({ maxLength: 150 });
    self.trainer = ko.observable().extend({ maxLength: 100 });
    self.fromDate = ko.observable(currentDate());
    self.toDate = ko.observable(currentDate());
    self.isApproved = ko.observable();
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Id: self.id(),
            Title: self.title(),
            Description: self.description(),
            Organizer: self.organizer(),
            Venue: self.venue(),
            Trainer: self.trainer(),
            IsApproved: self.hasApproved(),
            Remarks: self.remarks(),
            DurationFromDate: self.fromDate(),
            DurationToDate: self.toDate(),
            Status: self.status()
        };
    };

    self.hasApproved = function () {
        if (self.isApproved())
            return true;
        return false;
    };

    self.loadEmployeeTraining = function () {
        jax.getJson(
            '/EmployeeTrainingManage/Get/' + self.id(),
            function (data) {
                self.title(data.Title);
                self.description(data.Description);
                self.organizer(data.Organizer);
                self.venue(data.Venue);
                self.trainer(data.Trainer);
                self.isApproved(data.IsApproved);
                self.remarks(data.Remarks);
                self.fromDate(clientDate(data.DurationFromDate));
                self.toDate(clientDate(data.DurationToDate));
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeTrainingManage/Update',
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadEmployeeTraining();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.loadEmployeeTraining();
    };
    self.init = function () {
        self.id($('#employeeTrainingId').val());
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.loadEmployeeTraining();
        
        activeParentMenu($('li.sub a[href="/EmployeeTrainingManage"]'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new EmployeeTrainingUpdateViewModel();
    ko.applyBindings(vm);
    vm.init();
    
    $('#btnShowFromDate').click(function () {
        $('#btnFromDate').focus();
    });
    $('#btnResetFromDate').click(function () {
        vm.fromDate(currentDate());
    });

    $('#btnShowToDate').click(function () {
        $('#btnToDate').focus();
    });
    $('#btnResetToDate').click(function () {
        vm.toDate(currentDate());
    });
});