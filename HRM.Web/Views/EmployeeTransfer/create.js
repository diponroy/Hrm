﻿function EmployeeTransferCreateViewModel() {
    var self = this;
    
    self.allEmployees = ko.observableArray([]);
    self.allEmployeeTypes = ko.observableArray([]);
    self.allWorkStationUnits = ko.observableArray([]);
    self.allSalaryIncrements = ko.observableArray([]);
    self.allStatuses = ko.observableArray([]);
    
    self.employeeId = ko.observable().extend({ required: true });
    self.employeeTypeId = ko.observable().extend({ required: true });
    self.workStationUnitId = ko.observable().extend({ required: true });
    self.incrementSalaryStructureId = ko.observable();
    self.isApproved = ko.observable();
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });
    
    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.postObj = function () {
        return {
            EmployeeId: self.employeeId(),
            EmployeeTypeId: self.employeeTypeId(),
            WorkStationUnitId: self.workStationUnitId(),
            IncrementSalaryStructureId:self.incrementSalaryStructureId(),
            IsApproved: self.hasApproved(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };
    
    self.hasApproved = function () {
        if (self.isApproved())
            return true;
        return false;
    };
    
    /* Actions */
    self.loadEmployees = function () {
        self.allEmployees([]);
        jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/GetEmployees',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeWsUnit = data[index];
                    arr.push({
                        text: anEmployeeWsUnit.FullName,
                        id: anEmployeeWsUnit.Id
                    });
                });
                self.allEmployees(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.loadEmployeeTypes = function () {
        self.allEmployeeTypes([]);
        jax.postJsonBlock(
            '/EmployeeTransfer/GetEmployeeTypes',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeType = data[index];
                    arr.push({
                        text: anEmployeeType.Title,
                        id: anEmployeeType.Id
                    });
                });
                self.allEmployeeTypes(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadWorkStationUnits = function () {
        self.allWorkStationUnits([]);
        jax.postJsonBlock(
            '/EmployeeTransfer/GetWorkStationUnits',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anWorkStationUnit = data[index];
                    arr.push({
                        text: 'Company: ' + anWorkStationUnit.Company.Name + ' -- Branch: ' + anWorkStationUnit.Branch.Name + ' -- Department: ' + anWorkStationUnit.Department.Name + ' -- Unit:' + anWorkStationUnit.Remarks,
                        id: anWorkStationUnit.Id
                    });
                });
                self.allWorkStationUnits(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.loadEmployeeSalaryIncrements = function () {
        self.allSalaryIncrements([]);
        jax.postJsonBlock(
            '/EmployeeTransfer/GetEmployeeSalaryIncrements',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var object = data[index];
                    arr.push({
                        text: 'increment Basic: '+object.Basic+' -- Salary Structure Title: '+object.SalaryStructure.Title,
                        id: object.Id
                    });
                });
                self.allSalaryIncrements(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeTransfer/Create',
            self.postObj(),
            function () {
                ToastSuccess('Information Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };
    
    self.reset = function () {
        self.loadEmployees();
        self.loadWorkStationUnits();
        self.loadEmployeeTypes();
        self.loadEmployeeSalaryIncrements();

        self.employeeId('');
        self.employeeTypeId('');
        self.workStationUnitId('');
        self.isApproved('');
        self.remarks('');
        self.status('');
        
        self.removeErrors();
    };

    self.init = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        self.reset();

        activeParentMenu($('li.sub a[href="/EmployeeTransfer"]'));
    };
}

$(document).ready(function () {

    var vm = new EmployeeTransferCreateViewModel();
    ko.applyBindings(vm);
    vm.init();
});
