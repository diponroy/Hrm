﻿function EmployeeTransferDetailViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employeeTransfer = ko.observable();
    self.logs = ko.observableArray([]);
    
    /*get all info*/
    self.getEmployeeTransfer = function () {
        jax.getJsonBlock(
            '/EmployeeTransfer/Get/' + self.id(),
            function (data) {
                self.employeeTransfer({
                    employeeName: data.Employee.FullName,
                    employeeType: data.EmployeeType.Title,
                    workStationUnitName: 'Company: ' + data.WorkStationUnit.Company.Name + ' | Branch: ' + data.WorkStationUnit.Branch.Name + ' | Dept: ' + data.WorkStationUnit.Department.Name
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeTransfer/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeTransfer = data[index];
                    array.push({
                        id: employeeTransfer.Id,
                        employeeName: employeeTransfer.EmployeeLog.FullName,
                        employeeType:employeeTransfer.EmployeeTypeLog.Title,
                        companyName: employeeTransfer.WorkStationUnitLog.CompanyLog.Name,
                        branchName: employeeTransfer.WorkStationUnitLog.BranchLog.Name,
                        departmentName: employeeTransfer.WorkStationUnitLog.DepartmentLog.Name,
                        workStationUnitName: employeeTransfer.WorkStationUnitLog.Remarks,
                        salaryTitle: employeeTransfer.IncrementSalaryStructureLog == null ? '--' : employeeTransfer.IncrementSalaryStructureLog.EmployeeSalaryStructureLog.SalaryStructureLog.Title,
                        incrementBasic: employeeTransfer.IncrementSalaryStructureLog == null ? '--' : employeeTransfer.IncrementSalaryStructureLog.Basic,
                        isApproved: employeeTransfer.IsApproved,
                        remarks: employeeTransfer.Remarks,
                        statusString: employeeTransfer.StatusString,
                        affectedByEmployeeId: employeeTransfer.AffectedByEmployeeId,
                        affectedByName: employeeTransfer.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(employeeTransfer.AffectedDateTime),
                        logStatus: employeeTransfer.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    /*resets*/
    self.reset = function () {
        self.getEmployeeTransfer();
        self.getLogs();
    };

    self.init = function () {
        self.id($('#txtEmployeeTransferId').val());
        self.getEmployeeTransfer();
        self.getLogs();
    };
}

$(document).ready(function () {

    var vm = new EmployeeTransferDetailViewModel();
    ko.applyBindings(vm);
    vm.init();
});