﻿/*view models*/
function EmployeeTransferListViewModel() {
    var self = this;
    self.employeeTransfers = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getEmployeeTransfer = function (pageNo, pageSize, searchFilters) {
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/EmployeeTransfer/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var employeeTransfer = data[index];
                    array.push({
                        id: employeeTransfer.Id,
                        employeeName: employeeTransfer.Employee.FullName,
                        employeeType: employeeTransfer.EmployeeType.Title,
                        companyName: employeeTransfer.WorkStationUnit.Company.Name,
                        branchName: employeeTransfer.WorkStationUnit.Branch.Name,
                        departmentName: employeeTransfer.WorkStationUnit.Department.Name,
                        workStationUnitName: employeeTransfer.WorkStationUnit.Remarks,
                        salaryTitle: employeeTransfer.IncrementSalaryStructure === null ? '--' : employeeTransfer.IncrementSalaryStructure.EmployeeSalaryStructure.SalaryStructure.Title,
                        incrementBasic: employeeTransfer.IncrementSalaryStructure === null ? '--' : employeeTransfer.IncrementSalaryStructure.Basic,
                        isApproved: employeeTransfer.IsApproved,
                        remarks: employeeTransfer.Remarks,
                        status: employeeTransfer.Status,
                        statusString: employeeTransfer.StatusString
                    });
                });
                self.employeeTransfers(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function (item) {
        window.location = '/EmployeeTransfer/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/EmployeeTransfer/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete this type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJson(
            '/EmployeeTransfer/Delete',
            obj,
            function (data) {
                ToastSuccess("Information deleted successfully");
                self.getEmployeeTransfer(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.resetFilers();
        self.getEmployeeTransfer(1, 25, self.searchFilers());

        activeParentMenu($('li.sub a[href="/EmployeeTransfer"]'));
    };
}


$(document).ready(function () {
    var vm = new EmployeeTransferListViewModel();
    ko.applyBindings(vm);
    vm.init();
});