﻿/*Validations*/
ko.validation.rules['titleUsed'] = {
    validator: function(val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ title: val });
        $.when(
            $.ajax({
                url: '/EmployeeTypeManage/IsTitleUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function(data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exists.'
};
ko.validation.registerExtenders();


function EmployeeTypeViewModel() {
    var self = this;
    self.title = ko.observable().extend({ required: true, maxlength: 100, titleUsed: false });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.parentId = ko.observable();
    self.allActiveParents = ko.observableArray([]);
    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);

    self.errors = ko.validation.group(self);
    self.hasErrors = function() { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function() { self.errors.showAllMessages(); };
    self.removeErrors = function() { self.errors.showAllMessages(false); };

    self.employeeTypeObject = function() {
        return {
            Title: self.title(),
            Remarks: self.remarks(),
            ParentId: self.parentId(),
            Status: self.status()
        };
    };

    self.loadParents = function() {
        self.allActiveParents([]);
        jax.getJson(
            '/EmployeeTypeManage/GetActiveParents',
            function(data) {
                var array = new Array();
                $.each(data, function(index) {
                    var anEmployeetype = data[index];
                    array.push({
                        text: anEmployeetype.Title,
                        value: anEmployeetype.Id
                    });
                });
                self.allActiveParents(array);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/EmployeeTypeManage/Create',
            self.employeeTypeObject(),
            function() {
                ToastSuccess('A new Employee Type created successfully');
                self.reset();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };


    self.reset = function() {
        self.loadParents();
        self.title('');
        self.remarks('');
        self.parentId('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.loadParents();
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeTypeManage"]'));
    };
}


$(document).ready(function() {
    var viewModel = new EmployeeTypeViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();
});

