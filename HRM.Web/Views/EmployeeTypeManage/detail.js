﻿function EmployeeTypeViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employeeType = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all*/
    self.getEmployeeType = function () {
        jax.getJsonBlock(
            '/EmployeeTypeManage/Get/' + self.id(),
            function (data) {
                self.employeeType({
                    title: data.Title,
                    remarks: data.Remarks,
                    parentTitle: (data.ParentEmployeeType === null) ? 'Not yet assigned' : data.ParentEmployeeType.Title,
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/EmployeeTypeManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var aEmployeeType = data[index];
                    array.push({
                        id: aEmployeeType.Id,
                        title: aEmployeeType.Title,
                        remarks: aEmployeeType.Remarks,
                        parentId: aEmployeeType.ParentId,
                        parentTitle: (aEmployeeType.ParentEmployeeTypeLog === null) ? "Not yet assigned" : aEmployeeType.ParentEmployeeTypeLog.Title,
                        status: aEmployeeType.Status,
                        statusString: aEmployeeType.StatusString,
                        affectedByName: aEmployeeType.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(aEmployeeType.affectedDateTime),
                        logStatus: aEmployeeType.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtEmployeeTypeId').val());
        self.getEmployeeType();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new EmployeeTypeViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});