﻿/*view models*/
function EmployeeTypeListViewModel( ) {
    var self = this;
    self.employeeTypes = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function( ) {
    };
    self.resetFilers = function( ) {
    };

    self.getEmployeeTypes = function(pageNo, pageSize, searchFilters) {
        self.employeeTypes([]);
        var obj = {
            pageNo : pageNo,
            pageSize : pageSize,
            filter : searchFilters
        };
        jax.postJsonBlock(
            '/EmployeeTypeManage/Find',
            obj,
            function(data) {
                var array = new Array();
                $.each(data, function(index) {
                    var aEmployeeType = data[index];
                    array.push({
                        id : aEmployeeType.Id,
                        title : aEmployeeType.Title,
                        remarks : aEmployeeType.Remarks,
                        parentTypeTitle: (aEmployeeType.ParentEmployeeType != null) ? aEmployeeType.ParentEmployeeType.Title : '----Not yet assigned----',
                        status : aEmployeeType.Status,
                        statusString : aEmployeeType.StatusString
                    });
                });
                self.employeeTypes(array);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function(item) {
        window.location = '/EmployeeTypeManage/Update/' + item.id;
    };

    self.detail = function(item) {
        window.location = '/EmployeeTypeManage/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        var hasActiveChilds = null;
        jax.getJsonDfd(
            '/EmployeeTypeManage/CheckToDelete/' + item.id,
            function(data) {
                hasActiveChilds = data;
            }
        );
        if (hasActiveChilds===true) {
            bootbox.alert("Can't delete this type because it have one or more dependent type.");
            return;
        }
        bootbox.confirm("Are you sure, you want to delete this type ?", function(result) {
        if (result === true) {
            self.delete(item.id);
        }
        });
    };
   self.delete = function(id) {
        var obj = { id : id, };
        jax.postJson(
            '/EmployeeTypeManage/Delete',
            obj,
            function(data) {
                ToastSuccess("Information deleted successfully");
                self.getEmployeeTypes(1, 25, self.searchFilers());
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function( ) {
        self.resetFilers();
        self.getEmployeeTypes(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeTypeManage"]'));
    };
}


$(document).ready(function () {
    var vm = new EmployeeTypeListViewModel();
    ko.applyBindings(vm);
    vm.init();
});