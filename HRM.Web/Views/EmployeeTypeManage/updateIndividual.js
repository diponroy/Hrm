﻿/*Validations*/
ko.validation.rules['titleUsedExceptItself'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ employeeTypeId: $('#txtEmployeeTypeId').val(), title: val });
        $.when(
            $.ajax({
                url: '/EmployeeTypeManage/TitleUsedExceptItself',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            if (textStatus === 'success') {
                isUsed = data;
            } else {
                ToastError(xhr);
                isUsed = null;
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist.'
};
ko.validation.registerExtenders();


/*View models*/
function EmployeeTypeUpdateViewModel() {
    var self = this;
    self.id = ko.observable();
    self.title = ko.observable().extend({ required: true, maxLength: 50, titleUsedExceptItself: false });
    self.remarks = ko.observable().extend({ maxLength: 150, });
    self.parentId = ko.observable();
    self.allAssignableParents = ko.observableArray([]);
    self.status = ko.observable().extend({ required: true, });
    self.allStatus = ko.observableArray([]);

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.vmObj = function () {
        return {
            Id: self.id(),
            Title: self.title(),
            Remarks: self.remarks(),
            ParentId: self.parentId(),
            Status: self.status()
        };
    };

    ///get Id list
    self.getAssignableParents = function () {
        self.allAssignableParents([]);
        jax.getJson(
            '/EmployeeTypeManage/GetAssignableParents/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var anEmployeeType = data[index];
                    array.push({
                        title: anEmployeeType.Title,
                        id: anEmployeeType.Id
                    });
                });
                self.allAssignableParents(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.loadEmployeeType = function () {
        jax.getJson(
            '/EmployeeTypeManage/Get/' + self.id(),
            function (data) {
                self.title(data.Title);
                self.remarks(data.Remarks);
                self.parentId(data.ParentId);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeTypeManage/Update',
            self.vmObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadEmployeeType();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.loadEmployeeType();
    };
    self.init = function () {
        self.id($('#txtEmployeeTypeId').val());
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.getAssignableParents();
        self.loadEmployeeType();
    };
}

$(document).ready(function () {

    var vm = new EmployeeTypeUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});