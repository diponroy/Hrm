﻿/*Validations*/
ko.validation.rules['combinationUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var obj = {
            EmployeeId: otherVal().employeeId,
            WorkStationUnitId: val,
        };
        var json = JSON.stringify({ model: obj });
        $.when(
            $.ajax({
                url: '/EmployeeWorkStationUnitManage/IscombinationUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal().isUsed;
    },
    message: 'This Employee is already assigned to this WorkStationUnit !!'
};
ko.validation.registerExtenders();


function EmployeeWorkStationUnitViewModel() {
    var self = this;

    self.employeeId = ko.observable().extend({ required: true });
    self.employees = ko.observableArray([]);

    self.employeeTypeId = ko.observable().extend({ required: true });
    self.employeeTypes = ko.observableArray([]);

    self.workStationUnitId = ko.observable().extend({
        combinationUsed: ko.computed(function () {
            return {
                employeeId: self.employeeId(),
                isUsed: false
            };
        })
    });
    self.workStationUnits = ko.observableArray([]);

    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.remarks = ko.observable().extend({maxLength: 150});

    self.status = ko.observable().extend({ required: true });
    self.allStatuses = ko.observableArray([]);

    self.getEmployeeWorkStationUnitDetail = function () {
        var employeeWorkStationUnitObj = {
            EmployeeId: self.employeeId(),
            WorkStationUnitId: self.workStationUnitId(),
            EmployeeTypeId: self.employeeTypeId(),
            AttachmentDate: self.dateOfCreation(),
            Remarks: self.remarks(),
            Status: self.status()
        };

        return {
            model: employeeWorkStationUnitObj
        };
    };


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };



    /* Actions */
    self.loadEmployees = function() {
        self.employees([]);
        jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/GetEmployees',
            null,
            function(data) {
                var arr = new Array();
                $.each(data, function(index) {
                    var anEmployeeWsUnit = data[index];
                    arr.push({
                        id: anEmployeeWsUnit.Id,
                        name: anEmployeeWsUnit.FullName
                    });
                });
                self.employees(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadWorkStationUnits = function () {
        self.workStationUnits([]);
        jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/GetWorkStationUnits',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anWorkStationUnit = data[index];
                    arr.push({
                        id: anWorkStationUnit.Id,
                        fullName: 'Company: ' + anWorkStationUnit.Company.Name + ' -- Branch: ' + anWorkStationUnit.Branch.Name + ' -- Department: ' + anWorkStationUnit.Department.Name
                    });
                });
                self.workStationUnits(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeTypes = function () {
        self.employeeTypes([]);
        jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/GetEmployeeTypes',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeType = data[index];
                    arr.push({
                        id: anEmployeeType.Id,
                        title: anEmployeeType.Title
                    });
                });
                self.employeeTypes(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/Create',
            self.getEmployeeWorkStationUnitDetail(),
            function (data) {
                ToastSuccess('Employee Workstation Unit is created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.reset = function () {
        self.loadEmployees();
        self.loadWorkStationUnits();
        self.loadEmployeeTypes();

        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.status('');
        self.remarks('');

        self.removeErrors();
    };

    self.init = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeWorkStationUnitManage"]'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new EmployeeWorkStationUnitViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowDatePicker').click(function () {
        $('.datepicker').focus();
    });

    $('#btnResetDatePicker').click(function () {
        vm.dateOfCreation(currentDate());
    });
});