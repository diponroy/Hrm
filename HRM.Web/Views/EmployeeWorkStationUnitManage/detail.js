﻿
function EmployeeWorkStationUnitLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.employeeWorkStationUnit = ko.observable();
    self.employeeWorkStationUnitLogs = ko.observableArray([]);

    /* Actions */
    self.getEmployeeWorkStationUnit = function () {
        jax.getJsonBlock(
            '/EmployeeWorkStationUnitManage/Get/' + self.id(),
            function (data) {
                self.employeeWorkStationUnit({
                    employeeName: data.Employee.FullName,
                    company: data.WorkStationUnit.Company.Name,
                    branch: data.WorkStationUnit.Branch.Name,
                    department: data.WorkStationUnit.Department.Name,
                    workStationUnit: data.WorkStationUnit.Remarks,
                    remarks: data.Remarks,
                    employeeType: data.EmployeeType.Title,
                    dateOfCreation: clientDateTimeStringFromDate(data.AttachmentDate),
                    dateOfClosing: (data.DetachmentDate === null) ? '---' : clientDateTimeStringFromDate(data.DetachmentDate)
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.employeeWorkStationUnitLogs([]);
        jax.getJsonBlock(
            '/EmployeeWorkStationUnitManage/GetLogs/' + self.id(),
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeWorkStationUnit = data[index];
                    arr.push({
                        id: anEmployeeWorkStationUnit.Id,
                        employeeName: anEmployeeWorkStationUnit.EmployeeLog.FullName,
                        company: (anEmployeeWorkStationUnit.WorkStationUnitLog.CompanyLog === null) ? "---" : anEmployeeWorkStationUnit.WorkStationUnitLog.CompanyLog.Name,
                        branch: (anEmployeeWorkStationUnit.WorkStationUnitLog.BranchLog === null) ? "---" : anEmployeeWorkStationUnit.WorkStationUnitLog.BranchLog.Name,
                        department: (anEmployeeWorkStationUnit.WorkStationUnitLog.DepartmentLog === null) ? "---" : anEmployeeWorkStationUnit.WorkStationUnitLog.DepartmentLog.Name,
                        workStationUnit: (anEmployeeWorkStationUnit.ParentWorkStationUnitLog === null) ? "---" : anEmployeeWorkStationUnit.WorkStationUnitLog.Remarks,
                        employeeType: anEmployeeWorkStationUnit.EmployeeTypeLog.Title,
                        dateOfCreation: clientDateTimeStringFromDate(anEmployeeWorkStationUnit.AttachmentDate),
                        dateOfClosing: (anEmployeeWorkStationUnit.DetachmentDate === null) ? "---" : clientDateTimeStringFromDate(anEmployeeWorkStationUnit.DetachmentDate),
                        isClosed: anEmployeeWorkStationUnit.DetachmentDate != null,
                        status: anEmployeeWorkStationUnit.Status,
                        statusString: anEmployeeWorkStationUnit.StatusString,
                        affectedByName: anEmployeeWorkStationUnit.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(anEmployeeWorkStationUnit.AffectedDateTime),
                        logStatus: anEmployeeWorkStationUnit.LogStatuses
                    });
                });
                self.employeeWorkStationUnitLogs(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtEmployeeWorkStationUnitId').val());
        self.getEmployeeWorkStationUnit();
        self.getLogs();
    };
}


$(document).ready(function () {
    var vm = new EmployeeWorkStationUnitLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});