﻿
function EmployeeWorkStationUnitListViewModel() {
    var self = this;
    self.employeeWorkStationUnits = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getEmployeeWorkStationUnits = function(pageNo, pageSize, searchFilters) {
        self.employeeWorkStationUnits([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/Find',
            obj,
            function(data) {
                var arr = new Array();
                $.each(data, function(index) {
                    var empStation = data[index];
                    arr.push({
                        id: empStation.Id,
                        employee: empStation.Employee.FullName,
                        company: empStation.WorkStationUnit.Company.Name,
                        branch: empStation.WorkStationUnit.Branch.Name,
                        department: empStation.WorkStationUnit.Department.Name,
                        workStationUnit: empStation.WorkStationUnit.Remarks,
                        employeeType: empStation.EmployeeType.Title,
                        dateOfCreation: clientDateStringFromDate(empStation.AttachmentDate),
                        dateOfClosing: (empStation.DetachmentDate === null) ? '---' : clientDateStringFromDate(empStation.DetachmentDate),
                        isClosed: empStation.DetachmentDate != null,
                        remarks: empStation.Remarks,
                        status: empStation.Status,
                        statusString: empStation.StatusString
                    });
                });
                self.employeeWorkStationUnits(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        self.showToEdit = function(item) {
            window.location = '/EmployeeWorkStationUnitManage/Update/' + item.id;
        };

        self.showDetail = function(item) {
            window.location = '/EmployeeWorkStationUnitManage/Detail/' + item.id;
        };
        
        /*Salary Payment Create*/
        self.createSalaryPayment = function (item) {
            window.location = '/EmployeeSalaryPayment/CreateSalaryPayment/' + item.id;
        };


        self.confirmToDelete = function (item) {
            bootbox.confirm("Are you sure, you want to delete the Employee Workstation Unit ?", function (result) {
                if (result === true) {
                    self.delete(item.id);
                }
            });
        };
        self.delete = function (id) {
            var obj = {id: id,};
            jax.postJsonBlock(
                '/EmployeeWorkStationUnitManage/Delete',
                obj,
                function(data) {
                    ToastSuccess("Employee Workstation Unit deleted successfully");
                    self.getEmployeeWorkStationUnits(1, 25, self.searchFilers());
                },
                function(qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        };
    };
    
    self.init = function () {
        self.resetFilers();
        self.getEmployeeWorkStationUnits(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/EmployeeWorkStationUnitManage"]'));
    };
}


$(document).ready(function () {

    var vm = new EmployeeWorkStationUnitListViewModel();
    ko.applyBindings(vm);
    vm.init();
})