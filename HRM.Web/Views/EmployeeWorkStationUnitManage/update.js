﻿function EmployeeWorkStationUnitViewModel() {
    var self = this;
    self.id = ko.observable('').extend({ required: true });
    self.employeeId = ko.observable().extend({ required: true });
    self.employees = ko.observableArray([]);

    self.employeeTypeId = ko.observable().extend({ required: true });
    self.employeeTypes = ko.observableArray([]);

    self.workStationUnitId = ko.observable().extend({ required: true });
    self.workStationUnits = ko.observableArray([]);

    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.dateOfClosing = ko.observable().extend({ required: true });
    self.hasClosingDate = ko.observable(false);

    self.remarks = ko.observable().extend({maxLength: 150});

    self.status = ko.observable().extend({ required: true });
    self.allStatuses = ko.observableArray([]);

    self.setClosedDateTime = function () {
        self.hasClosingDate(true);
        self.dateOfClosing(currentDate());
    };
    self.removeClosedDateTime = function () {
        self.hasClosingDate(false);
        self.dateOfClosing(null);
    };

    self.errorsWithOutStatus = ko.validation.group([self.employeeId, self.employeeTypeId, self.workStationUnitId, self.dateOfCreation, self.remarks]);
    self.errorsWithStatus = ko.validation.group([self.employeeId, self.employeeTypeId, self.workStationUnitId, self.dateOfCreation, self.remarks, self.status]);
    self.errors = ko.computed(function () {
        return self.hasClosingDate() ? self.errorsWithOutStatus : self.errorsWithStatus;
    }, this);
    self.hasErrors = function () {
        return ((self.errors()()).length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors().showAllMessages();
    };
    self.removeErrors = function () {
        self.errors().showAllMessages(false);
    };

    self.getEmployeeWorkStationUnitObj = function () {
        return {
            Id: self.id(),
            EmployeeId: self.employeeId(),
            EmployeeTypeId: self.employeeTypeId(),
            WorkStationUnitId: self.workStationUnitId(),
            AttachmentDate: self.dateOfCreation(),
            DetachmentDate: self.dateOfClosing(),
            Remarks: self.remarks(),
            Status: (self.hasClosingDate()) ? null : self.status()
        };
    };


    /* Actions */
    self.loadEmployees = function () {
        return jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/GetEmployees',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeWsUnit = data[index];
                    arr.push({
                        id: anEmployeeWsUnit.Id,
                        name: anEmployeeWsUnit.FullName
                    });
                });
                self.employees(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadWorkStationUnits = function () {
        return jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/GetWorkStationUnits',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anWorkStationUnit = data[index];
                    arr.push({
                        id: anWorkStationUnit.Id,
                        fullName: 'Company: ' + anWorkStationUnit.Company.Name + ' -- Branch: ' + anWorkStationUnit.Branch.Name + ' -- Department: ' + anWorkStationUnit.Department.Name
                    });
                });
                self.workStationUnits(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.loadEmployeeTypes = function () {
        return jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/GetEmployeeTypes',
            null,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var anEmployeeType = data[index];
                    arr.push({
                        id: anEmployeeType.Id,
                        title: anEmployeeType.Title
                    });
                });
                self.employeeTypes(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.load = function (id) {
        jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/Get/' + id,
            null,
            function (data) {
                self.employeeId(data.EmployeeId);
                self.employeeTypeId(data.EmployeeTypeId);
                self.workStationUnitId(data.WorkStationUnitId);
                self.dateOfCreation(clientDate(data.AttachmentDate));
                if (data.DetachmentDate != null) {
                    self.hasClosingDate(true);
                    self.dateOfClosing(clientDate(data.DetachmentDate));
                } else {
                    self.removeClosedDateTime();
                }
                self.remarks(data.Remarks);
                self.status(data.StatusString);

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/EmployeeWorkStationUnitManage/Update',
            self.getEmployeeWorkStationUnitObj(),
            function(data) {
                ToastSuccess('Employee Workstation Unit is updated successfully');
                self.reset();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.reset = function () {
        jax.block();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        var calls = [];
        calls.push(self.loadEmployees());
        calls.push(self.loadWorkStationUnits());
        calls.push(self.loadEmployeeTypes());
        $.when.apply(this, calls).done(function() {
            self.load(self.id());
        });
    };

    self.init = function () {
        self.id($('#txtEmployeeWorkStationUnitId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/EmployeeWorkStationUnitManage"]'));
    };
}

$(document).ready(function () {
    var vm = new EmployeeWorkStationUnitViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowDatePicker').click(function () {
        $('#btnDateOfCreation').focus();
    });
    $('#btnResetDatePicker').click(function () {
        vm.dateOfCreation(currentDate());
    });

    $('#btnShowClosingDate').click(function () {
        $('#btnDateOfClosing').focus();
    });
    $('#btnResetClosingDate').click(function () {
        vm.dateOfClosing(currentDate());
    });
});