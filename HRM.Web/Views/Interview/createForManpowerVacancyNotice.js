﻿ko.validation.rules['TrackNoUsed'] = {
    validator: function(val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ trackNo: val });
        $.when(
            $.ajax({
                url: '/Interview/HasTrackNoUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function(data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal;
    },
    message: 'info already in use.'
};
ko.validation.registerExtenders();

/*View models*/

function InterviewCreateViewModel() {
    var self = this;

    self.allStatus = ko.observableArray([]);

    self.trackNo = ko.observable().extend({ required: true, maxLength: 50, TrackNoUsed: false });
    self.title = ko.observable().extend({ maxLength: 50 });
    self.manpowerVacancyNoticeId = ko.observable();
    self.durationFromDate = ko.observable(currentDate()).extend({ required: true });
    self.durationToDate = ko.observable(currentDate()).extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function() { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function() { self.errors.showAllMessages(); };
    self.removeErrors = function() { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function() {
        return {
            TrackNo: self.trackNo(),
            Title: self.title(),
            ManpowerVacancyNoticeId: self.manpowerVacancyNoticeId(),
            DurationFromDate: self.durationFromDate(),
            DurationToDate: self.durationToDate(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/Interview/Create/' + self.manpowerVacancyNoticeId(),
            self.postObj(),
            function() {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function() {
        self.trackNo('');
        self.title('');
        self.durationFromDate(currentDate());
        self.durationToDate(currentDate());
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.init = function() {

        self.manpowerVacancyNoticeId($('#txtManpowerVacancyNoticeId').val());

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/Interview"]'));
        var value = $('li.sub a[href="/Interview"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}

$(document).ready(function() {

    $('.datepicker').datepicker();

    var vm = new InterviewCreateViewModel();
    ko.applyBindings(vm);
    vm.init();

    $('#showFromDate').click(function() {
        $('#btnFromDate').focus();
    });
    $('#resetFromDate').click(function() {
        vm.durationFromDate(currentDate());
    });

    $('#showToDate').click(function() {
        $('#btnToDate').focus();
    });
    $('#resetToDate').click(function() {
        vm.durationToDate(currentDate());
    });
});




