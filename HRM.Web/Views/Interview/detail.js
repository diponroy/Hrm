﻿function InterviewDetailViewModel() {
    var self = this;
    self.id = ko.observable();
    self.interview = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getInterview = function () {
        jax.getJsonBlock(
            '/Interview/Get/' + self.id(),
            function (data) {
                self.interview({
                    trackNo: data.TrackNo,
                    title: data.Title,
                    manpowerVacancyNoticeTitle: data.ManpowerVacancyNotice.Title,
                    durationFromDate: data.DurationFromDate == null ? '--' : clientDateStringFromDate(data.DurationFromDate),
                    durationToDate: data.DurationToDate == null ? '--' : clientDateStringFromDate(data.DurationToDate),
                    remarks: data.Remarks
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/Interview/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interview = data[index];
                    array.push({
                        id: interview.Id,
                        trackNo: interview.TrackNo,
                        title: interview.Title,
                        manpowerVacancyNoticeTitle: interview.ManpowerVacancyNoticeLog.Title,
                        manpowerVacancyNoticeType: interview.ManpowerVacancyNoticeLog.Type,
                        durationFromDate: interview.DurationFromDate==null?'--': clientDateStringFromDate(interview.DurationFromDate),
                        durationToDate: interview.DurationToDate == null ? '--' : clientDateStringFromDate(interview.DurationToDate),
                        remarks: interview.Remarks,
                        status: interview.Status,
                        statusString: interview.StatusString,
                        affectedByEmployeeName: interview.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(interview.affectedDateTime),
                        logStatus: interview.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#interviewId').val());
        self.getInterview();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new InterviewDetailViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
}); 