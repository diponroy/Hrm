﻿/*view models*/
function InterviewListViewModel() {
    var self = this;
    self.interviews = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getInterview = function (pageNo, pageSize, searchFilters) {
        self.interviews([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/Interview/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interview = data[index];
                    array.push({
                        id: interview.Id,
                        trackNo: interview.TrackNo,
                        title: interview.Title,
                        remarks: interview.Remarks,
                        durationFromDate: (interview.DurationFromDate === null) ? '--' : clientDateStringFromDate(interview.DurationFromDate),
                        durationToDate: (interview.DurationToDate === null) ? '--' : clientDateStringFromDate(interview.DurationToDate),
                        manpowerVacancyNoticeTitle: interview.ManpowerVacancyNotice.Title,
                        manpowerVacancyNoticeType: interview.ManpowerVacancyNotice.Type,
                        status: interview.Status,
                        statusString: interview.StatusString
                    });
                });
                self.interviews(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function (item) {
        window.location = '/Interview/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/Interview/Detail/' + item.id;
    };
    
    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete this item?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    
   self.delete = function (id) {
        var obj = { id: id, };
        jax.postJson(
            '/Interview/Delete',
            obj,
            function (data) {
                ToastSuccess("Information deleted successfully");
                self.getInterview(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
   };
    
   self.confirmChange = function () {
       bootbox.confirm("You have to create a notice first!!!", function (result) {
           if (result === true) {
               self.changeLocation();
           }
       });
   };
   self.changeLocation = function () {
       window.location = '/ManpowerVacancyNotice/List/';
   };
    
    self.init = function () {
        self.resetFilers();
        self.getInterview(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/Interview"]'));
        var value = $('li.sub a[href="/Interview"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}


$(document).ready(function () {
    var vm = new InterviewListViewModel();
    ko.applyBindings(vm);
    vm.init();
});