﻿/*Validations*/
ko.validation.rules['TrackNoUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#txtInterviewId').val(), title: val });
        $.when(
            $.ajax({
                url: '/Interview/HasTrackNoUsedExcept',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist'
};
ko.validation.registerExtenders();

function InterviewUpdateViewModel() {
    var self = this;
    
    self.allStatus = ko.observableArray([]);
    self.allManpowerVacancyNoticeTitle = ko.observableArray([]);
    
    self.id = ko.observable().extend({ required: true });
    self.trackNo = ko.observable().extend({ required: true, maxLength: 50, TrackNoUsed: false });
    self.title = ko.observable().extend({ maxLength: 50 });
    self.manpowerVacancyNoticeId = ko.observable();
    self.durationFromDate = ko.observable(currentDate()).extend({ required: true });
    self.durationToDate = ko.observable(currentDate()).extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.postObject = function () {
        return {
            Id: self.id(),
            TrackNo: self.trackNo(),
            Title: self.title(),
            ManpowerVacancyNoticeId: self.manpowerVacancyNoticeId(),
            DurationFromDate: self.durationFromDate(),
            DurationToDate: self.durationToDate(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.loadVacancyNotices = function() {
        jax.getJson(
            '/Interview/GetVacancyNotices',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: 'Title: '+ obj.Title+'|'+'Type: '+obj.Type,
                        value: obj.Id
                    });
                });
                self.allManpowerVacancyNoticeTitle(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*get all*/
    self.loadInterview = function () {
        jax.getJson(
           '/Interview/Get/' + self.id(),
            function (data) {
                self.trackNo(data.TrackNo);
                self.title(data.Title);
                self.manpowerVacancyNoticeId(data.ManpowerVacancyNoticeId),
                self.durationFromDate((data.DurationFromDate == null) ? '--' : clientDate(data.DurationFromDate));
                self.durationToDate((data.DurationToDate == null) ? '--' : clientDate(data.DurationToDate));
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*Server Actions*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/Interview/Update',
            self.postObject(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadInterview();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
         );
    };

    self.reset = function () {
        self.loadVacancyNotices();
        self.loadInterview();
        self.removeErrors();
    };

    self.init = function () {
        self.id($('#txtInterviewId').val());

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.loadVacancyNotices();
        self.loadInterview();
        
        activeParentMenu($('li.sub a[href="/Interview"]'));
        var value = $('li.sub a[href="/Interview"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}

$(document).ready(function () {
    var viewModel = new InterviewUpdateViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();

    $('#btnShowDurationFromDate').click(function () {
        $('#btnDurationFromDate').focus();
    });
    $('#btnResetDurationFromDate').click(function () {
        viewModel.durationFromDate(currentDate());
    });

    $('#btnShowDurationToDate').click(function () {
        $('#btnDurationToDate').focus();
    });

    $('#btnResetDurationToDate').click(function () {
        viewModel.durationToDate(currentDate());
    });
});