﻿/*View models*/
function InterviewCandidateCreateViewModel() {
    var self = this;

    self.allInterviewTitles = ko.observableArray([]);
    self.allCandidateNames = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.appointmentDateTime = ko.observable(currentDate()).extend({ required: true });
    self.attendedDateTime = ko.observable(currentDate()).extend({ required: true });
    self.currentSalary = ko.observable().extend({ digit: true });
    self.expectedSalary = ko.observable().extend({ digit: true });
    self.proposedSalary = ko.observable().extend({ digit: true });
    self.interviewId = ko.observable().extend({ required: true });
    self.candidateId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            AppointmentDateTime: self.appointmentDateTime(),
            AttendedDateTime:self.appointmentDateTime(),
            CurrentSalary: self.currentSalary(),
            ExpectedSalary: self.expectedSalary(),
            ProposedSalary: self.proposedSalary(),
            InterviewId: self.interviewId(),
            CandidateId: self.candidateId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.getInterviewTitles = function () {
        jax.getJson(
            '/InterviewCandidate/GetInterviewTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allInterviewTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getCandidateInfo = function () {
        jax.getJson(
            '/InterviewCandidate/GetCandidateInfo',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.FirstName+' '+obj.LastName+'|'+obj.ContactNo,
                        value: obj.Id
                    });
                });
                self.allCandidateNames(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/InterviewCandidate/Create',
            self.postObj(),
            function () {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.appointmentDateTime('');
        self.attendedDateTime('');
        self.currentSalary('');
        self.expectedSalary('');
        self.proposedSalary('');
        self.interviewId('');
        self.candidateId('');
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.getInterviewTitles();
        self.getCandidateInfo();
        
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/InterviewCandidate"]'));
        var value = $('li.sub a[href="/InterviewCandidate"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}

$(document).ready(function () {
    
    $('.datepicker').datepicker();
    
    var vm = new InterviewCandidateCreateViewModel();
    ko.applyBindings(vm);
    vm.init();
    
    $('#btnShowAppointmentDateTime').click(function () {
        $('#btnAppointmentDateTime').focus();
    });
    $('#btnResetAppointmentDateTime').click(function () {
        vm.appointmentDateTime(currentDate());
    });
    
    $('#btnShowAttendedDateTime').click(function () {
        $('#btnAttendedDateTime').focus();
    });
    $('#btnResetAttendedDateTime').click(function () {
        vm.attendedDateTime(currentDate());
    });
});