﻿function InterviewCandidateDetailViewModel() {
    var self = this;
    self.id = ko.observable();
    self.interviewCandidate = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getInterviewCandidate = function () {
        jax.getJsonBlock(
            '/InterviewCandidate/Get/' + self.id(),
            function (data) {
                self.interviewCandidate({
                    appointmentDateTime: clientDateTimeStringFromDate(data.AppointmentDateTime),
                    attendedDateTime: clientDateTimeStringFromDate(data.AttendedDateTime),
                    currentSalary: data.CurrentSalary,
                    expectedSalary: data.ExpectedSalary,
                    proposedSalary: data.ProposedSalary,
                    interviewTitle: data.Interview.Title,
                    candidateName: data.Candidate.FirstName+' '+data.Candidate.LastName,
                    candidateContactNo: data.Candidate.ContactNo,
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/InterviewCandidate/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interviewCandidate = data[index];
                    array.push({
                        id: interviewCandidate.Id,
                        appointmentDateTime: clientDateTimeStringFromDate(interviewCandidate.AppointmentDateTime),
                        attendedDateTime: clientDateTimeStringFromDate(interviewCandidate.AttendedDateTime),
                        currentSalary: interviewCandidate.CurrentSalary,
                        expectedSalary: interviewCandidate.ExpectedSalary,
                        proposedSalary: interviewCandidate.ProposedSalary,
                        interviewTitle: interviewCandidate.InterviewLog.Title,
                        candidateName: interviewCandidate.CandidateLog.FirstName+' '+ interviewCandidate.CandidateLog.LastName,
                        candidateContactNo: interviewCandidate.CandidateLog.ContactNo,
                        remarks: interviewCandidate.Remarks,
                        status: interviewCandidate.Status,
                        statusString: interviewCandidate.StatusString,
                        affectedByEmployeeName: interviewCandidate.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(interviewCandidate.affectedDateTime),
                        logStatus: interviewCandidate.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtInterviewCandidateId').val());
        self.getInterviewCandidate();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new InterviewCandidateDetailViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});