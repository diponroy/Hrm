﻿/*view models*/
function InterviewCandidateListViewModel() {
    var self = this;
    self.interviewCandidates = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getInterviewCandidate = function (pageNo, pageSize, searchFilters) {
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/InterviewCandidate/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interviewCandidate = data[index];
                    array.push({
                        id: interviewCandidate.Id,
                        appointmentDateTime: clientDateTimeStringFromDate(interviewCandidate.AppointmentDateTime),
                        attendedDateTime: clientDateTimeStringFromDate(interviewCandidate.AttendedDateTime),
                        currentSalary: interviewCandidate.CurrentSalary,
                        expectedSalary: interviewCandidate.ExpectedSalary,
                        proposedSalary: interviewCandidate.ProposedSalary,
                        interviewTitle: interviewCandidate.Interview.Title,
                        candidateName: interviewCandidate.Candidate.FirstName +' '+ interviewCandidate.Candidate.LastName,
                        candidateContactNo: interviewCandidate.Candidate.ContactNo,
                        remarks: interviewCandidate.Remarks,
                        status: interviewCandidate.Status,
                        statusString: interviewCandidate.StatusString
                    });
                });
                self.interviewCandidates(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function (item) {
        window.location = '/InterviewCandidate/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/InterviewCandidate/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete this type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJson(
            '/InterviewCandidate/Delete',
            obj,
            function (data) {
                ToastSuccess("Information deleted successfully");
                self.geInterviewCandidate(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.resetFilers();
        self.getInterviewCandidate(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/InterviewCandidate"]'));
        var value = $('li.sub a[href="/InterviewCandidate"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}


$(document).ready(function () {
    var vm = new InterviewCandidateListViewModel();
    ko.applyBindings(vm);
    vm.init();
});