﻿/*View models*/
function InterviewCoordinatorCreateViewModel() {
    var self = this;

    self.allEmployees = ko.observableArray([]);
    self.allInterviewTitles = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);
   

    self.employeeId = ko.observable().extend({ required: true });
    self.interviewId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            EmployeeId: self.employeeId(),
            InterviewId: self.interviewId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };
    
    self.getEmployees = function () {
        jax.getJson(
            '/InterviewCoordinator/GetEmployees',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.FullName,
                        value: obj.Id
                    });
                });
                self.allEmployees(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getInterviewTitles = function () {
        jax.getJson(
            '/InterviewCoordinator/GetInterviewTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allInterviewTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/InterviewCoordinator/Create',
            self.postObj(),
            function () {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.employeeId('');
        self.interviewId('');
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        
        self.getEmployees();
        self.getInterviewTitles();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/InterviewCoordinator"]'));
        var value = $('li.sub a[href="/InterviewCoordinator"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}

$(document).ready(function () {
    var vm = new InterviewCoordinatorCreateViewModel();
    ko.applyBindings(vm);
    vm.init();
});