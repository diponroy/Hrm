﻿function InterviewCoordinatorDetailViewModel() {
    var self = this;
    self.id = ko.observable();
    self.interviewCoordinator = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getInterviewCoordinator = function () {
        jax.getJsonBlock(
            '/InterviewCoordinator/Get/' + self.id(),
            function (data) {
                self.interviewCoordinator({
                    employeeName: data.Employee.FullName,
                    interviewTitle: data.Interview.Title,
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/InterviewCoordinator/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interviewCoordinator = data[index];
                    array.push({
                        id: interviewCoordinator.Id,
                        employeeName: interviewCoordinator.EmployeeLog.FullName,
                        interviewTitle: interviewCoordinator.InterviewLog.Title,
                        remarks: interviewCoordinator.Remarks,
                        status: interviewCoordinator.Status,
                        statusString: interviewCoordinator.StatusString,
                        affectedByEmployeeName: interviewCoordinator.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(interviewCoordinator.affectedDateTime),
                        logStatus: interviewCoordinator.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtInterviewCoordinatorId').val());
        self.getInterviewCoordinator();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new InterviewCoordinatorDetailViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});