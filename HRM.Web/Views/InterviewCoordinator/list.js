﻿/*view models*/
function InterviewCoordinatorListViewModel() {
    var self = this;
    self.interviewCoordinators = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getInterviewCoordinator = function (pageNo, pageSize, searchFilters) {
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/InterviewCoordinator/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interviewCoordinator = data[index];
                    array.push({
                        id: interviewCoordinator.Id,
                        employeeName: interviewCoordinator.Employee.FullName,
                        interviewTitle: interviewCoordinator.Interview.Title,
                        remarks: interviewCoordinator.Remarks,
                        status: interviewCoordinator.Status,
                        statusString: interviewCoordinator.StatusString
                    });
                });
                self.interviewCoordinators(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function (item) {
        window.location = '/InterviewCoordinator/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/InterviewCoordinator/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete this type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJson(
            '/InterviewCoordinator/Delete',
            obj,
            function (data) {
                ToastSuccess("Information deleted successfully");
                self.getInterviewCoordinator(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.resetFilers();
        self.getInterviewCoordinator(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/InterviewCoordinator"]'));
        var value = $('li.sub a[href="/InterviewCoordinator"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}


$(document).ready(function () {
    var vm = new InterviewCoordinatorListViewModel();
    ko.applyBindings(vm);
    vm.init();
});