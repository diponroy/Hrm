﻿function InterviewCoordinatorUpdateViewModel() {
    var self = this;

    self.allEmployees = ko.observableArray([]);
    self.allInterviewTitles = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.id = ko.observable().extend({ required: true });
    self.employeeId = ko.observable().extend({ required: true });
    self.interviewId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.postObject = function () {
        return {
            Id: self.id(),
            EmployeeId: self.employeeId(),
            InterviewId: self.interviewId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.getEmployees = function () {
        return jax.getJson(
            '/InterviewCoordinator/GetEmployees',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.FullName,
                        value: obj.Id
                    });
                });
                self.allEmployees(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getInterviewTitles = function () {
        return jax.getJson(
            '/InterviewCoordinator/GetInterviewTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allInterviewTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*get values in txt box*/
    self.loadInterviewCoordinator = function (id) {
        jax.getJson(
           '/InterviewCoordinator/Get/' + id,
            function (data) {
                self.employeeId(data.EmployeeId);
                self.interviewId(data.InterviewId),
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*Server Actions*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/InterviewCoordinator/Update',
            self.postObject(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadInterviewCoordinator();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
         );
    };

    self.reset = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        
        var calls = [];
        calls.push(self.getEmployees());
        calls.push(self.getInterviewTitles());
        $.when.apply(this,calls).done(function() {
            self.loadInterviewCoordinator(self.id());
        });
        
        self.removeErrors();
    };

    self.init = function () {
        self.id($('#txtInterviewCoordinatorId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/InterviewCoordinator"]'));
        var value = $('li.sub a[href="/InterviewCoordinator"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}

$(document).ready(function () {
    var viewModel = new InterviewCoordinatorUpdateViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});