﻿/*View models*/
function InterviewTestCreateViewModel() {
    var self = this;
    
    self.allInterviewTitles = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.title = ko.observable().extend({ maxLength: 250 });
    self.totalMarks = ko.observable().extend({ digit: true });
    self.interviewId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength:250 });
    self.status = ko.observable().extend({ required: true });
    
    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Title: self.title(),
            TotalMarks:self.totalMarks(),
            InterviewId: self.interviewId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

   self.getInterviewTitles = function () {
        jax.getJson(
            '/InterviewTest/GetInterviewTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allInterviewTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/InterviewTest/Create',
            self.postObj(),
            function () {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.title('');
        self.totalMarks('');
        self.interviewId('');
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.getInterviewTitles();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/InterviewTest"]'));
        var value = $('li.sub a[href="/InterviewTest"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}

$(document).ready(function () {
    var vm = new InterviewTestCreateViewModel();
    ko.applyBindings(vm);
    vm.init();
});