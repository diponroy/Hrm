﻿function InterviewTestDetailViewModel() {
    var self = this;
    self.id = ko.observable();
    self.interviewTest = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getInterviewTest = function () {
        jax.getJsonBlock(
            '/InterviewTest/Get/' + self.id(),
            function (data) {
                self.interviewTest({
                    title: data.Title,
                    totalMarks: data.TotalMarks,
                    interviewTitle: data.Interview.Title,
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/InterviewTest/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interviewTest = data[index];
                    array.push({
                        id: interviewTest.Id,
                        title: interviewTest.Title,
                        totalMarks: interviewTest.TotalMarks,
                        interviewTitle: interviewTest.InterviewLog.Title,
                        remarks: interviewTest.Remarks,
                        status: interviewTest.Status,
                        statusString: interviewTest.StatusString,
                        affectedByEmployeeName: interviewTest.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(interviewTest.affectedDateTime),
                        logStatus: interviewTest.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#interviewTestId').val());
        self.getInterviewTest();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new InterviewTestDetailViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});