﻿/*view models*/
function InterviewTestListViewModel() {
    var self = this;
    self.interviewTests = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getInterviewTest = function (pageNo, pageSize, searchFilters) {
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/InterviewTest/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interviewTest = data[index];
                    array.push({
                        id: interviewTest.Id,
                        title: interviewTest.Title,
                        totalMarks: interviewTest.TotalMarks,
                        interviewTitle: interviewTest.Interview.Title,
                        remarks: interviewTest.Remarks,
                        status: interviewTest.Status,
                        statusString: interviewTest.StatusString
                    });
                });
                self.interviewTests(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function (item) {
        window.location = '/InterviewTest/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/InterviewTest/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete this type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJson(
            '/InterviewTest/Delete',
            obj,
            function (data) {
                ToastSuccess("Information deleted successfully");
                self.getInterviewTest(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.resetFilers();
        self.getInterviewTest(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/InterviewTest"]'));
        var value = $('li.sub a[href="/InterviewTest"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}


$(document).ready(function () {
    var vm = new InterviewTestListViewModel();
    ko.applyBindings(vm);
    vm.init();
});