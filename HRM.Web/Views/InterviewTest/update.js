﻿function InterviewTestUpdateViewModel() {
    var self = this;

    self.allStatus = ko.observableArray([]);
    self.allInterviewTitles = ko.observableArray([]);

    self.id = ko.observable().extend({ required: true });
    self.title = ko.observable().extend({ maxLength: 250 });
    self.totalMarks = ko.observable().extend({ digit: true });
    self.interviewId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.postObject = function () {
        return {
            Id: self.id(),
            Title: self.title(),
            TotalMarks: self.totalMarks(),
            InterviewId: self.interviewId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.loadInterviewTitles = function () {
        self.allInterviewTitles([]);
        jax.getJson(
            '/InterviewTest/GetInterviewTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allInterviewTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*get values in txt box*/
    self.loadInterviewTest = function () {
        jax.getJson(
           '/InterviewTest/Get/' + self.id(),
            function (data) {
                self.title(data.Title);
                self.totalMarks(data.TotalMarks);
                self.interviewId(data.InterviewId),
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*Server Actions*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/InterviewTest/Update',
            self.postObject(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadInterviewTest();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
         );
    };

    self.reset = function () {
        self.loadInterviewTest();
        self.removeErrors();
    };

    self.init = function () {
        self.id($('#interviewTestId').val());

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.loadInterviewTitles();
        self.loadInterviewTest();
        
        activeParentMenu($('li.sub a[href="/InterviewTest"]'));
        var value = $('li.sub a[href="/InterviewTest"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}

$(document).ready(function () {
    var viewModel = new InterviewTestUpdateViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});