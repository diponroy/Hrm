﻿/*View models*/
function InterviewTestCandidateCreateViewModel() {
    var self = this;

    self.allInterviewTestTitles = ko.observableArray([]);
    self.allCandidateNames = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    self.obtainedMarks = ko.observable().extend({ digit: true });
    self.interviewTestId = ko.observable().extend({ required: true });
    self.candidateId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            ObtainedMarks: self.obtainedMarks(),
            InterviewTestId: self.interviewTestId(),
            CandidateId: self.candidateId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.getInterviewTestTitles = function () {
        jax.getJson(
            '/InterviewTestCandidate/GetInterviewTestTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allInterviewTestTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getCandidateNames = function () {
        jax.getJson(
            '/InterviewTestCandidate/GetCandidateNames',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.FirstName+' '+obj.LastName+'|'+obj.PresentAddress,
                        value: obj.Id
                    });
                });
                self.allCandidateNames(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/InterviewTestCandidate/Create',
            self.postObj(),
            function () {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.obtainedMarks('');
        self.interviewTestId('');
        self.candidateId('');
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        self.getInterviewTestTitles();
        self.getCandidateNames();

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/InterviewTestCandidate"]'));
        var value = $('li.sub a[href="/InterviewTestCandidate"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}

$(document).ready(function () {
    var vm = new InterviewTestCandidateCreateViewModel();
    ko.applyBindings(vm);
    vm.init();
});