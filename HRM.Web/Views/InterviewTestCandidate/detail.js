﻿function InterviewTestCandidateDetailViewModel() {
    var self = this;
    self.id = ko.observable();
    self.interviewTestCandidate = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getInterviewTestCandidate = function () {
        jax.getJsonBlock(
            '/InterviewTestCandidate/Get/' + self.id(),
            function (data) {
                self.interviewTestCandidate({
                    obtainedMarks: data.ObtainedMarks,
                    interviewTestTitle: data.InterviewTest.Title,
                    candidateName: data.Candidate.FirstName + ' ' + data.Candidate.LastName,
                    candidateAddress:data.Candidate.PresentAddress
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/InterviewTestCandidate/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interviewTestCandidate = data[index];
                    array.push({
                        id: interviewTestCandidate.Id,
                        obtainedMarks: interviewTestCandidate.ObtainedMarks,
                        interviewTestTitle: interviewTestCandidate.InterviewTestLog.Title,
                        candidateName: interviewTestCandidate.CandidateLog.FirstName + ' ' + interviewTestCandidate.CandidateLog.LastName,
                        candidateAddress: interviewTestCandidate.CandidateLog.PresentAddress,
                        remarks: interviewTestCandidate.Remarks,
                        status: interviewTestCandidate.Status,
                        statusString: interviewTestCandidate.StatusString,
                        affectedByEmployeeName: interviewTestCandidate.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(interviewTestCandidate.affectedDateTime),
                        logStatus: interviewTestCandidate.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtInterviewTestCandidateId').val());
        self.getInterviewTestCandidate();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new InterviewTestCandidateDetailViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});