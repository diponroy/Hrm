﻿/*view models*/
function InterviewTestCandidateListViewModel() {
    var self = this;
    self.interviewTestCandidates = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getInterviewTestCandidate = function (pageNo, pageSize, searchFilters) {
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/InterviewTestCandidate/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var interviewTestCandidate = data[index];
                    array.push({
                        id: interviewTestCandidate.Id,
                        obtainedMarks: interviewTestCandidate.ObtainedMarks,
                        interviewTestTitle: interviewTestCandidate.InterviewTest.Title,
                        candidateName: 'Name: '+interviewTestCandidate.Candidate.FirstName + ' ' + interviewTestCandidate.Candidate.LastName,
                        candidateAddress: 'Address: '+interviewTestCandidate.Candidate.PresentAddress,
                        remarks: interviewTestCandidate.Remarks,
                        status: interviewTestCandidate.Status,
                        statusString: interviewTestCandidate.StatusString
                    });
                });
                self.interviewTestCandidates(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function (item) {
        window.location = '/InterviewTestCandidate/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/InterviewTestCandidate/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete this type ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJson(
            '/InterviewTestCandidate/Delete',
            obj,
            function (data) {
                ToastSuccess("Information deleted successfully");
                self.getInterviewTestCandidate(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.resetFilers();
        self.getInterviewTestCandidate(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/InterviewTestCandidate"]'));
        var value = $('li.sub a[href="/InterviewTestCandidate"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}


$(document).ready(function () {
    var vm = new InterviewTestCandidateListViewModel();
    ko.applyBindings(vm);
    vm.init();
});