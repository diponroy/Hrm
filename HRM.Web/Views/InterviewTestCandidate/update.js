﻿function InterviewTestCandidateUpdateViewModel() {
    var self = this;

    self.allInterviewTestTitles = ko.observableArray([]);
    self.allCandidateNames = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);


    self.id = ko.observable().extend({ required: true });
    self.obtainedMarks = ko.observable().extend({ digit: true });
    self.interviewTestId = ko.observable().extend({ required: true });
    self.candidateId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 250 });
    self.status = ko.observable().extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.postObject = function () {
        return {
            Id: self.id(),
            ObtainedMarks: self.obtainedMarks(),
            InterviewTestId: self.interviewTestId(),
            CandidateId: self.candidateId(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.getInterviewTestTitles = function () {
        return jax.getJson(
            '/InterviewTestCandidate/GetInterviewTestTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allInterviewTestTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getCandidateNames = function () {
        return jax.getJson(
            '/InterviewTestCandidate/GetCandidateNames',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.FirstName + ' ' + obj.LastName + '|' + obj.PresentAddress,
                        value: obj.Id
                    });
                });
                self.allCandidateNames(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };


    /*get values in txt box*/
    self.loadInterviewTestCandidate = function () {
        jax.getJson(
           '/InterviewTestCandidate/Get/' + self.id(),
            function (data) {
                self.obtainedMarks(data.ObtainedMarks);
                self.interviewTestId(data.InterviewTestId),
                self.candidateId(data.CandidateId),
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*Server Actions*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/InterviewTestCandidate/Update',
            self.postObject(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadInterviewTestCandidate();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
         );
    };

    self.reset = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        
        var calls = [];
        calls.push(self.getInterviewTestTitles());
        calls.push(self.getCandidateNames());
        $.when.apply(this, calls).done(function () {
            self.loadInterviewTestCandidate(self.id());
        });
        
        self.removeErrors();
    };

    self.init = function () {
        self.id($('#txtInterviewTestCandidateId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/InterviewTestCandidate"]'));
        var value = $('li.sub a[href="/InterviewTestCandidate"]').parents('li:eq(1)').find('a:first');
        activeParentMenu(value);
    };
}

$(document).ready(function () {
    var viewModel = new InterviewTestCandidateUpdateViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});