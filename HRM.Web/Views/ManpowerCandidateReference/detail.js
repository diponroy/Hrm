﻿
function ManpowerCandidateReferenceLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.manpowerCandidateReferenceLogs = ko.observableArray([]);


    /* Actions */

    self.getLogs = function () {
        self.manpowerCandidateReferenceLogs([]);

        jax.getJson(
            '/ManpowerCandidateReference/GetLogs/' + self.id(),
            function (data) {
                var arr = new Array();

                $.each(data, function (index) {
                    var aReference = data[index];
                    arr.push({
                        id: aReference.Id,
                        noticeCandidateTrackNo: aReference.ManpowerVacancyNoticeCandidateLog.TrackNo,
                        noticeTrackNo: aReference.ManpowerVacancyNoticeCandidateLog.ManpowerVacancyNoticeLog.TrackNo,
                        noticeTitle: aReference.ManpowerVacancyNoticeCandidateLog.ManpowerVacancyNoticeLog.ManpowerRequisitionLog.PositionForDesignation,
                        noticeVacancy: aReference.ManpowerVacancyNoticeCandidateLog.ManpowerVacancyNoticeLog.ManpowerRequisitionLog.NumberOfPersons,

                        candidateTrackNo: aReference.ManpowerVacancyNoticeCandidateLog.CandidateLog.TrackNo,
                        candidateName: aReference.ManpowerVacancyNoticeCandidateLog.CandidateLog.FirstName + ' ' + aReference.ManpowerVacancyNoticeCandidateLog.CandidateLog.LastName,

                        employeeTrackNo: 'Track No: ' + aReference.EmployeeReferrerLog.TrackNo,
                        employeeName: 'Name: ' + aReference.EmployeeReferrerLog.FullName,

                        remarks: aReference.Remarks,
                        status: aReference.Status,
                        statusString: aReference.StatusString,
                        affectedByName: aReference.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(aReference.AffectedDateTime),
                        logStatus: aReference.LogStatuses
                    });
                });

                self.manpowerCandidateReferenceLogs(arr);
                $.unblockUI();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtCandidateReferenceId').val());
        self.getLogs();
    };
}


$(document).ready(function () {
    var vm = new ManpowerCandidateReferenceLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});