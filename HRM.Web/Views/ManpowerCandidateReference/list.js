﻿
function ManpowerCandidateReferenceListViewModel() {
    var self = this;
    self.manpowerCandidateReferences = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getManpowerCandidateReferences = function (pageNo, pageSize, searchFilters) {
        self.manpowerCandidateReferences([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/ManpowerCandidateReference/Find',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aReference = data[index];
                    arr.push({
                        id: aReference.Id,
                        noticeCandidateTrackNo: aReference.ManpowerVacancyNoticeCandidate.TrackNo,
                        noticeTrackNo:  aReference.ManpowerVacancyNoticeCandidate.ManpowerVacancyNotice.TrackNo,
                        noticeTitle: aReference.ManpowerVacancyNoticeCandidate.ManpowerVacancyNotice.ManpowerRequisition.PositionForDesignation,
                        noticeVacancy: aReference.ManpowerVacancyNoticeCandidate.ManpowerVacancyNotice.ManpowerRequisition.NumberOfPersons,

                        candidateTrackNo: aReference.ManpowerVacancyNoticeCandidate.Candidate.TrackNo,
                        candidateName: aReference.ManpowerVacancyNoticeCandidate.Candidate.FirstName + ' ' + aReference.ManpowerVacancyNoticeCandidate.Candidate.LastName,

                        employeeTrackNo: 'Track No: ' + aReference.Employee.TrackNo,
                        employeeName: 'Name: ' + aReference.Employee.FullName,

                        remarks: aReference.Remarks,
                        status: aReference.Status,
                        statusString: aReference.StatusString
                    });
                });

                self.manpowerCandidateReferences(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );      
    }

    self.showDetail = function (item) {
        window.location = '/ManpowerCandidateReference/Detail/' + item.id;
    };

    self.init = function () {
        self.resetFilers();
        self.getManpowerCandidateReferences(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/ManpowerCandidateReference"]'));
    };
}


$(document).ready(function () {
    var vm = new ManpowerCandidateReferenceListViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
})