﻿
function ManpowerRequisitionViewModel() {
    var self = this;
    self.trackNo = ko.observable().extend({ required: true, maxLength: 20});
    self.title = ko.observable().extend({ required: true, maxLength: 250 });
    self.position = ko.observable().extend({ required: true, maxLength: 50 });
    self.number = ko.observable().extend({ required: true, min:1, digit: true});
    self.description = ko.observable().extend({ maxLength: 250 }); 
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.deadline = ko.observable(currentDate()).extend({ required: true });
    self.approvalState = ko.observable().extend({ required: true});
    self.status = ko.observable().extend({ required: true });

    self.allStatuses = ko.observableArray([]);
    self.allApprovals = ko.observableArray([]);


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    self.getManpowerRequisitionDetail = function () {
        var requisitionObj = {
            TrackNo: self.trackNo(),
            Title: self.title(),
            PositionForDesignation: self.position(),
            NumberOfPersons: self.number(),
            Description: self.description(),
            DateOfCreation: self.dateOfCreation(),
            DedlineDate: self.deadline(),
            IsApproved: self.approvalState(),
            Status: self.status()
        };

        return {
            entity: requisitionObj
        };
    };


    /* Actions */

    self.generateCode = function () {
        $.ajax({
            url: '/ManpowerRequisition/GenerateTrackNo',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.trackNo(data);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    },

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/ManpowerRequisition/Create',
            self.getManpowerRequisitionDetail(),
            function (data) {
                ToastSuccess('Manpower Requisition is created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.reset = function () {
        self.trackNo('');
        self.title('');
        self.position('');
        self.number('');
        self.description('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.deadline('');
        self.deadline(currentDate());
        self.approvalState('');
        self.status('');

        self.removeErrors();
    };

    self.init = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        var approvalState = [];
        approvalState.push({ name: 'Approved', value: 'True' });
        approvalState.push({ name: 'Pending', value: 'False' });
        self.allApprovals(approvalState);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/ManpowerRequisition"]'));
    };
}


$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new ManpowerRequisitionViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowFromDate').click(function () {
        $('#btnFromDate').focus();
    });
    $('#btnResetFromDate').click(function () {
        vm.dateOfCreation(currentDate());
    });

    $('#btnShowToDate').click(function () {
        $('#btnToDate').focus();
    });
    $('#btnResetToDate').click(function () {
        vm.deadline(currentDate());
    });
});