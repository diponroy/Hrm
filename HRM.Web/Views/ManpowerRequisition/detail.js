﻿
function ManpowerRequisitionLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.manpowerRequisition = ko.observable();
    self.manpowerRequisitionLogs = ko.observableArray([]);


    /* Actions */

    self.getManpowerRequisition = function () {
        jax.getJson(
            '/ManpowerRequisition/Get/' + self.id(),
            function (data) {
                self.manpowerRequisition({
                    title: data.Title,
                    position: data.PositionForDesignation,
                    number: data.NumberOfPersons
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.manpowerRequisitionLogs([]);

        jax.getJson(
            '/ManpowerRequisition/GetLogs/' + self.id(),
            function (data) {
                var arr = new Array();

                $.each(data, function (index) {
                    var aRequisition = data[index];
                    arr.push({
                        id: aRequisition.Id,
                        trackNo: aRequisition.TrackNo,
                        title: aRequisition.Title,
                        position: aRequisition.PositionForDesignation,
                        number: aRequisition.NumberOfPersons,
                        description: aRequisition.Description,
                        dateOfCreation: clientDateTimeStringFromDate(aRequisition.DateOfCreation),
                        deadline: clientDateTimeStringFromDate(aRequisition.DedlineDate),
                        approvalState: aRequisition.IsApproved,

                        status: aRequisition.Status,
                        statusString: aRequisition.StatusString,
                        affectedByName: aRequisition.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(aRequisition.AffectedDateTime),
                        logStatus: aRequisition.LogStatuses
                    });
                });

                self.manpowerRequisitionLogs(arr);
                $.unblockUI();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtRequisitionId').val());
        self.getManpowerRequisition();
        self.getLogs();
    };
}


$(document).ready(function () {
    var vm = new ManpowerRequisitionLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});