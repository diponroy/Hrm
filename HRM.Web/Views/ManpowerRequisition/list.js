﻿
function ManpowerRequisitionListViewModel() {
    var self = this;
    self.manpowerRequisitions = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getManpowerRequisitions = function (pageNo, pageSize, searchFilters) {
        self.manpowerRequisitions([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/ManpowerRequisition/Find',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aRequisition = data[index];
                    arr.push({
                        id: aRequisition.Id,
                        trackNo: aRequisition.TrackNo,
                        title: aRequisition.Title,
                        position: aRequisition.PositionForDesignation,
                        number: aRequisition.NumberOfPersons,
                        description: aRequisition.Description,
                        dateOfCreation: clientDateStringFromDate(aRequisition.DateOfCreation),
                        deadline: (aRequisition.DedlineDate === null) ? '---' : clientDateStringFromDate(aRequisition.DedlineDate),
                        approvalState: aRequisition.IsApproved,
                        isClosed: aRequisition.DedlineDate != null,
                        status: aRequisition.Status,
                        statusString: aRequisition.StatusString
                    });
                });

                self.manpowerRequisitions(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        self.showToEdit = function (item) {
            window.location = '/ManpowerRequisition/Update/' + item.id;
        };

        self.showDetail = function (item) {
            window.location = '/ManpowerRequisition/Detail/' + item.id;
        };

        self.delete = function (id) {
            var obj = {
                id: id,
            };

            jax.postJsonBlock(
                '/ManpowerRequisition/Delete',
                obj,
                function (data) {
                    ToastSuccess("Manpower Requisition is deleted successfully");
                    self.getManpowerRequisitions(1, 25, self.searchFilers());
                },
                function (qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        };

        self.confirmToDelete = function (item) {
            bootbox.confirm("Are you sure, you want to delete this Manpower Requisition Order?", function (result) {
                if (result === true) {
                    self.delete(item.id);
                }
            });
        };
    };

    self.init = function () {
        self.resetFilers();
        self.getManpowerRequisitions(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/ManpowerRequisition"]'));
    };
}


$(document).ready(function () {

    var vm = new ManpowerRequisitionListViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
})