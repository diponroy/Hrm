﻿
function ManpowerRequisitionViewModel() {
    var self = this;
    self.id = ko.observable();
    self.trackNo = ko.observable().extend({ required: true, maxLength: 20 });
    self.title = ko.observable().extend({ required: true, maxLength: 250 });
    self.position = ko.observable().extend({ required: true, maxLength: 50 });
    self.number = ko.observable().extend({ required: true, min: 1, digit: true });
    self.description = ko.observable().extend({ maxLength: 250 });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.deadline = ko.observable(currentDate()).extend({ required: true });
    self.approvalState = ko.observable().extend({ required: true });
    self.status = ko.observable().extend({ required: true });

    self.allStatuses = ko.observableArray([]);
    self.allApprovals = ko.observableArray([]);

    /* errors */
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    self.getManpowerRequisitionObj = function () {
        return {
            Id: self.id(),
            TrackNo: self.trackNo(),
            Title: self.title(),
            PositionForDesignation: self.position(),
            NumberOfPersons: self.number(),
            Description: self.description(),
            DateOfCreation: self.dateOfCreation(),
            DedlineDate: self.deadline(),
            IsApproved: self.approvalState(),
            Status: self.status()
        };
    };


    /* Actions */
    self.load = function () {
        jax.getJson(
            '/ManpowerRequisition/Get/' + self.id(),
            function (data) {
                self.trackNo(data.TrackNo);
                self.title(data.Title),
                self.position(data.PositionForDesignation);
                self.number(data.NumberOfPersons);
                self.description(data.Description),
                self.dateOfCreation(clientDate(data.DateOfCreation));
                self.deadline(clientDate(data.DedlineDate));
                self.approvalState((data.IsApproved.toString().toLowerCase() == 'true') ? self.allApprovals()[0].value : self.allApprovals()[1].value);
                self.status(data.StatusString);

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/ManpowerRequisition/Update',
            self.getManpowerRequisitionObj(),
            function (data) {
                ToastSuccess('Manpower Requisition Order is updated successfully');
                self.load();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    }

    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.id($('#txtRequisitionId').val());

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        var approvalState = [];
        approvalState.push({ name: 'Approved', value: 'True' });
        approvalState.push({ name: 'Pending', value: 'False' });
        self.allApprovals(approvalState);

        self.load();
        
        activeParentMenu($('li.sub a[href="/ManpowerRequisition"]'));
    };
}


$(document).ready(function () {
    var vm = new ManpowerRequisitionViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowFromDate').click(function () {
        $('#btnFromDate').focus();
    });
    $('#btnResetFromDate').click(function () {
        vm.dateOfCreation(currentDate());
    });

    $('#btnShowToDate').click(function () {
        $('#btnToDate').focus();
    });
    $('#btnResetToDate').click(function () {
        vm.deadline(currentDate());
    });
});