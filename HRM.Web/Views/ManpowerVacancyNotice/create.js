﻿
function ManpowerVacancyNoticeViewModel() {
    var self = this;
    self.trackNo = ko.observable().extend({ required: true, maxLength: 20 });
    self.type = ko.observable().extend({ maxLength: 250 });
    self.title = ko.observable().extend({ maxLength: 250 });
    self.description = ko.observable().extend({ maxLength: 250 });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.dateOfClosing = ko.observable(currentDate()).extend({ required: true });
    self.status = ko.observable().extend({ required: true });
    self.requisition = ko.observable().extend({ required: true });

    self.allStatuses = ko.observableArray([]);
    self.allRequisitions = ko.observableArray([]);


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };


    self.getRequisitions = function () {
        jax.getJson(
            '/ManpowerVacancyNotice/GetRequisitions',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        name: 'Track- ' + obj.TrackNo + '; Position- ' + obj.PositionForDesignation + '; Deadline- ' + clientDateStringFromDate(obj.DedlineDate),
                        value: obj.Id
                    });
                });
                self.allRequisitions(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getManpowerVacancyNoticeDetail = function () {
        var noticeObj = {
            TrackNo: self.trackNo(),
            Type: self.type(),
            Title: self.title(),
            Description: self.description(),
            DurationFromDate: self.dateOfCreation(),
            DurationToDate: self.dateOfClosing(),
            ManpowerRequisitionId: self.requisition(),
            Status: self.status()
        };

        return {
            entity: noticeObj
        };
    };


    /* Actions */

    self.generateCode = function () {
        $.ajax({
            url: '/ManpowerVacancyNotice/GenerateTrackNo',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.trackNo(data);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    },


    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/ManpowerVacancyNotice/Create',
            self.getManpowerVacancyNoticeDetail(),
            function (data) {
                ToastSuccess('Manpower Vacancy Notice is created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.reset = function () {
        self.trackNo('');
        self.type('');
        self.title('');
        self.description('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.dateOfClosing('');
        self.dateOfClosing(currentDate());
        self.requisition('');
        self.status('');

        self.removeErrors();
    };

    self.init = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        self.getRequisitions();

        self.reset();
        
        activeParentMenu($('li.sub a[href="/ManpowerVacancyNotice"]'));
    };
}


$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new ManpowerVacancyNoticeViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowFromDate').click(function () {
        $('#btnFromDate').focus();
    });
    $('#btnResetFromDate').click(function () {
        vm.dateOfCreation(currentDate());
    });

    $('#btnShowToDate').click(function () {
        $('#btnToDate').focus();
    });
    $('#btnResetToDate').click(function () {
        vm.dateOfClosing(currentDate());
    });
});