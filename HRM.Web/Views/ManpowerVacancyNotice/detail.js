﻿
function ManpowerVacancyNoticeLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.manpowerVacancyNotice = ko.observable();
    self.manpowerVacancyNoticeLogs = ko.observableArray([]);


    /* Actions */

    self.getManpowerVacancyNotice = function () {
        jax.getJson(
            '/ManpowerVacancyNotice/Get/' + self.id(),
            function (data) {
                self.manpowerVacancyNotice({
                    title: data.Title,
                    number: data.ManpowerRequisition.NumberOfPersons
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.manpowerVacancyNoticeLogs([]);

        jax.getJson(
            '/ManpowerVacancyNotice/GetLogs/' + self.id(),
            function (data) {
                var arr = new Array();

                $.each(data, function (index) {
                    var aNotice = data[index];
                    arr.push({
                        id: aNotice.Id,
                        trackNo: aNotice.TrackNo,
                        type: aNotice.Type,
                        title: aNotice.Title,
                        description: aNotice.Description,
                        from: clientDateTimeStringFromDate(aNotice.DurationFromDate),
                        to: clientDateTimeStringFromDate(aNotice.DurationToDate),
                        approvalState: aNotice.IsApproved,

                        status: aNotice.Status,
                        statusString: aNotice.StatusString,
                        affectedByName: aNotice.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(aNotice.AffectedDateTime),
                        logStatus: aNotice.LogStatuses
                    });
                });

                self.manpowerVacancyNoticeLogs(arr);
                $.unblockUI();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtNoticeId').val());
        self.getManpowerVacancyNotice();
        self.getLogs();
    };
}


$(document).ready(function () {
    var vm = new ManpowerVacancyNoticeLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});