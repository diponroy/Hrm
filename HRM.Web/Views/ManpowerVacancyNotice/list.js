﻿
function ManpowerVacancyNoticeListViewModel() {
    var self = this;
    self.manpowerVacancyNotices = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getManpowerVacancyNotices = function (pageNo, pageSize, searchFilters) {
        self.manpowerVacancyNotices([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/ManpowerVacancyNotice/Find',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aRequisition = data[index];
                    arr.push({
                        id: aRequisition.Id,
                        trackNo: aRequisition.TrackNo,
                        type: aRequisition.Type,
                        title: aRequisition.Title,
                        description: aRequisition.Description,
                        dateOfCreation: clientDateStringFromDate(aRequisition.DurationFromDate),
                        dateOfClosing: clientDateStringFromDate(aRequisition.DurationToDate),
                        requisition: 'Position: ' + aRequisition.ManpowerRequisition.PositionForDesignation + '\nVacancy: ' + aRequisition.ManpowerRequisition.NumberOfPersons,
                        status: aRequisition.Status,
                        statusString: aRequisition.StatusString
                    });
                });

                self.manpowerVacancyNotices(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        self.showToEdit = function (item) {
            window.location = '/ManpowerVacancyNotice/Update/' + item.id;
            
        };

        self.showDetail = function (item) {
            window.location = '/ManpowerVacancyNotice/Detail/' + item.id;
        };
        
        self.createForManpowerVacancyNotice = function (item) {
            window.location = '/Interview/CreateForManpowerVacancyNotice/' + item.id;
        };

        self.delete = function (id) {
            var obj = {
                id: id,
            };

            jax.postJsonBlock(
                '/ManpowerVacancyNotice/Delete',
                obj,
                function (data) {
                    ToastSuccess("Manpower Vacancy Notice is deleted successfully");
                    self.getManpowerVacancyNotices(1, 25, self.searchFilers());
                },
                function (qXhr, textStatus, error) {
                    ToastError(error);
                }
            );
        };

        self.confirmToDelete = function (item) {
            bootbox.confirm("Are you sure, you want to delete this Manpower Vacancy Notice?", function (result) {
                if (result === true) {
                    self.delete(item.id);
                }
            });
        };
    }

    self.init = function () {
        self.resetFilers();
        self.getManpowerVacancyNotices(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/ManpowerVacancyNotice"]'));
    };
}


$(document).ready(function () {
    var vm = new ManpowerVacancyNoticeListViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
})