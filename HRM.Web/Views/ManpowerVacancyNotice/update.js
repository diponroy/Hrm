﻿
function ManpowerVacancyNoticeViewModel() {
    var self = this;
    self.id = ko.observable();
    self.trackNo = ko.observable().extend({ required: true, maxLength: 20 });
    self.type = ko.observable().extend({ maxLength: 250 });
    self.title = ko.observable().extend({ maxLength: 250 });
    self.description = ko.observable().extend({ maxLength: 250 });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.dateOfClosing = ko.observable(currentDate()).extend({ required: true });
    self.status = ko.observable().extend({ required: true });
    self.requisitionId = ko.observable().extend({ required: true });
    self.requisition = ko.observable().extend({ required: true });

    self.allStatuses = ko.observableArray([]);


    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };


    self.getManpowerVacancyNoticeDetail = function () {
        var noticeObj = {
            Id: self.id(),
            TrackNo: self.trackNo(),
            Type: self.type(),
            Title: self.title(),
            Description: self.description(),
            DurationFromDate: self.dateOfCreation(),
            DurationToDate: self.dateOfClosing(),
            ManpowerRequisitionId: self.requisitionId(),
            Status: self.status()
        };

        return {
            entity: noticeObj
        };
    };


    /* Actions */
    self.load = function () {
        jax.getJson(
            '/ManpowerVacancyNotice/Get/' + self.id(),
            function (data) {
                self.trackNo(data.TrackNo);
                self.type(data.Type),
                self.title(data.Title);
                self.description(data.Description),
                self.dateOfCreation(clientDate(data.DurationFromDate));
                self.dateOfClosing(clientDate(data.DurationToDate));
                self.requisitionId(data.ManpowerRequisitionId);
                self.requisition('Track: ' + data.ManpowerRequisition.TrackNo + '; Position: ' + data.ManpowerRequisition.PositionForDesignation + '; Vacancy: ' + data.ManpowerRequisition.NumberOfPersons);
                self.status(data.StatusString);

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };


    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        jax.postJsonBlock(
            '/ManpowerVacancyNotice/Update',
            self.getManpowerVacancyNoticeDetail(),
            function (data) {
                ToastSuccess('Manpower Vacancy Notice is updated successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };

    self.reset = function () {
        self.load();
    };

    self.init = function () {
        self.id($('#txtNoticeId').val());

        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatuses(status);

        self.reset();
        
        activeParentMenu($('li.sub a[href="/ManpowerVacancyNotice"]'));
    };
}


$(document).ready(function () {
    $('.datepicker').datepicker();

    var vm = new ManpowerVacancyNoticeViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();

    $('#btnShowFromDate').click(function () {
        $('#btnFromDate').focus();
    });
    $('#btnResetFromDate').click(function () {
        vm.dateOfCreation(currentDate());
    });

    $('#btnShowToDate').click(function () {
        $('#btnToDate').focus();
    });
    $('#btnResetToDate').click(function () {
        vm.dateOfClosing(currentDate());
    });
});