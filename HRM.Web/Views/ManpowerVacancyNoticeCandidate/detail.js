﻿
function ManpowerVacancyNoticeCandidateLogViewModel() {
    var self = this;
    self.id = ko.observable();
    self.manpowerVacancyNoticeCandidateLogs = ko.observableArray([]);


    /* Actions */

    self.getLogs = function () {
        self.manpowerVacancyNoticeCandidateLogs([]);

        jax.getJson(
            '/ManpowerVacancyNoticeCandidate/GetLogs/' + self.id(),
            function (data) {
                var arr = new Array();

                $.each(data, function (index) {
                    var aCandidate = data[index];
                    arr.push({
                        id: aCandidate.Id,
                        trackNo: aCandidate.TrackNo,
                        noticeTrackNo: aCandidate.ManpowerVacancyNoticeLog.TrackNo,
                        noticeTitle: aCandidate.ManpowerVacancyNoticeLog.ManpowerRequisitionLog.PositionForDesignation,
                        noticeVacancy: aCandidate.ManpowerVacancyNoticeLog.ManpowerRequisitionLog.NumberOfPersons,

                        candidateTrackNo: aCandidate.CandidateLog.TrackNo,
                        candidateName: aCandidate.CandidateLog.FirstName + ' ' + aCandidate.CandidateLog.LastName,

                        employeeTrackNo: (aCandidate.EmployeeLog === null) ? '--- Not Applicable ---' : 'Track No: ' + aCandidate.EmployeeLog.TrackNo,
                        employeeName: (aCandidate.EmployeeLog === null) ? '' : 'Name: ' + aCandidate.EmployeeLog.Salutation + ' ' + aCandidate.EmployeeLog.FirstName + ' ' + aCandidate.EmployeeLog.LastName,

                        status: aCandidate.Status,
                        statusString: aCandidate.StatusString,
                        affectedByName: aCandidate.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(aCandidate.AffectedDateTime),
                        logStatus: aCandidate.LogStatuses                  
                    });
                });

                self.manpowerVacancyNoticeCandidateLogs(arr);
                $.unblockUI();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.id($('#txtNoticeCandidateId').val());
        self.getLogs();
    };
}


$(document).ready(function () {
    var vm = new ManpowerVacancyNoticeCandidateLogViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
});