﻿
function ManpowerVacancyNoticeCandidateListViewModel() {
    var self = this;
    self.manpowerVacancyNoticeCandidates = ko.observableArray([]);

    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getManpowerVacancyNoticeCandidates = function (pageNo, pageSize, searchFilters) {
        self.manpowerVacancyNoticeCandidates([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };

        jax.postJsonBlock(
            '/ManpowerVacancyNoticeCandidate/Find',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aCandidate = data[index];
                    arr.push({
                        id: aCandidate.Id,
                        trackNo: aCandidate.TrackNo,
                        noticeTrackNo: aCandidate.ManpowerVacancyNotice.TrackNo,
                        noticeTitle: aCandidate.ManpowerVacancyNotice.ManpowerRequisition.PositionForDesignation,
                        noticeVacancy: aCandidate.ManpowerVacancyNotice.ManpowerRequisition.NumberOfPersons,

                        candidateTrackNo: aCandidate.Candidate.TrackNo,
                        candidateName: aCandidate.Candidate.FirstName + ' ' + aCandidate.Candidate.LastName,

                        employeeTrackNo: (aCandidate.Employee === null) ? '--- Not Applicable ---' : 'Track No: ' + aCandidate.Employee.TrackNo,
                        employeeName: (aCandidate.Employee === null) ? '' : 'Name: ' + aCandidate.Employee.FullName,
                      
                        status: aCandidate.Status,
                        statusString: aCandidate.StatusString
                    });
                });

                self.manpowerVacancyNoticeCandidates(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );

        self.showDetail = function (item) {
            window.location = '/ManpowerVacancyNoticeCandidate/Detail/' + item.id;
        };

        self.delete = function (id) {
            var obj = {
                id: id,
            };

            jax.postJsonBlock(
                '/ManpowerVacancyNoticeCandidate/Delete',
                obj,
                function (data) {
                    ToastSuccess("Manpower Vacancy Notice Candidate is deleted successfully");
                    self.getManpowerVacancyNoticeCandidates(1, 25, self.searchFilers());
                },
                function (qXhr, textStatus, error) {
                    ToastError(error + '<br/>Error in deleting Notice Candidate !!');
                }
            );
        };

        self.confirmToDelete = function (item) {
            bootbox.confirm("Are you sure, you want to delete this Manpower Vacancy Notice Candidate?", function (result) {
                if (result === true) {
                    self.delete(item.id);
                }
            });
        };
    }

    self.init = function () {
        self.resetFilers();
        self.getManpowerVacancyNoticeCandidates(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/ManpowerVacancyNoticeCandidate"]'));
    };
}


$(document).ready(function () {
    var vm = new ManpowerVacancyNoticeCandidateListViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
})