﻿function SalaryAllowanceCreateVm() {
    var self = this;

    self.callback = this.createdCallBack;
    self.allAllowanceTitles = ko.observableArray([]);

    self.allowanceId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 150 });

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.vmObj = function () {
        return {
            AllowanceId: self.allowanceId(),
            Remarks: self.remarks(),
        };
    };

    self.getAllowanceTitles = function () {
        jax.getJson(
            '/SalaryStructureAllowanceManage/GetAllowanceTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allAllowanceTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getTitle = function (allowanceId) {
        var result = $.grep(self.allAllowanceTitles(), function (e) { return e.value == allowanceId; });
        return result[0].text;
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        this.createdCallBack({
            allowanceId: self.allowanceId(),
            remarks: self.remarks(),
            allowanceTitle: self.getTitle(self.allowanceId())
        });
    };

    self.reset = function () {
        self.allowanceId('');
        self.remarks('');
        self.removeErrors();
    };

    self.init = function () {
        self.reset();
        self.getAllowanceTitles();
    };
}