﻿function SalaryAllowanceCreateOnUpdateVm() {
    var self = this;

    self.id = ko.observable();
    self.callback = this.createdCallBack;
    self.allAllowanceTitles = ko.observableArray([]);

    self.salaryStructureId = ko.observable();
    self.salaryStructureTitle = ko.observable();
    self.allowanceId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 150 });

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.postObj = function () {
        return {
            SalaryStructureId:self.salaryStructureId(),
            AllowanceId: self.allowanceId(),
            Remarks: self.remarks()
        };
    };

    self.getAllowanceTitles = function () {
        jax.getJson(
            '/SalaryStructureAllowanceManage/GetAllowanceTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allAllowanceTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/SalaryStructureAllowanceManage/Create',
            self.postObj(),
            function () {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
        this.createdCallBack();
    };

    self.reset = function () {
        self.allowanceId('');
        self.remarks('');
        self.removeErrors();
    };

    self.init = function (id,title) {
        self.reset();
        self.getAllowanceTitles();
        self.salaryStructureId(id);
        self.salaryStructureTitle(title);
    };
}