﻿function SalaryAllowanceUpdateVm() {
    var self = this;
    
    self.storedItemId = ko.observable();
    self.storedItemSalaryStructureId = ko.observable();
    self.storedItemSalaryStructureTitle = ko.observable();
    self.storedItemAllowanceId = ko.observable();
    self.storedItemRemarks = ko.observable();

    self.callback = this.createdCallBack;
    self.allAllowanceTitles = ko.observableArray([]);

    self.id = ko.observable().extend({ required: true });
    self.salaryStructureId = ko.observable().extend({ required: true });
    self.salaryStructureTitle = ko.observable();
    self.allowanceId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    
    self.callback = this.createdCallBack;
    

    /* errors */
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };
    
    self.postObj = function () {
        return {
            Id: self.id(),
            SalaryStructureId: self.salaryStructureId(),
            AllowanceId: self.allowanceId(),
            Remarks: self.remarks(),
        };
    };
    
    /*All DDL Load*/
    self.getAllowanceTitles = function () {
        jax.getJson(
            '/SalaryStructureAllowanceManage/GetAllowanceTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allAllowanceTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/SalaryStructureAllowanceManage/Update',
            self.postObj(),
            function () {
                ToastSuccess('Salary Allowance is updated successfully');
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
        this.createdCallBack();
    };

    self.reset = function () {
        self.id(self.storedItemId());
        self.salaryStructureId(self.storedItemSalaryStructureId());
        self.salaryStructureTitle(self.storedItemSalaryStructureTitle());
        self.allowanceId(self.storedItemAllowanceId());
        self.remarks(self.storedItemRemarks());
        self.removeErrors();
    };

    self.init = function (id, salaryStructureId, salaryStructureTitle, allowanceId, remarks) {
        self.getAllowanceTitles();
        
        self.storedItemId(id);
        self.storedItemSalaryStructureId(salaryStructureId);
        self.storedItemSalaryStructureTitle(salaryStructureTitle);
        self.storedItemAllowanceId(allowanceId);
        self.storedItemRemarks(remarks);

        self.id(id);
        self.salaryStructureId(salaryStructureId);
        self.salaryStructureTitle(salaryStructureTitle);
        self.allowanceId(allowanceId);
        self.remarks(remarks);
    };
}