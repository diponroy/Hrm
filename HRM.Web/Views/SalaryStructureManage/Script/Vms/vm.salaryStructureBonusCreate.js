﻿function SalaryBonusCreateVm() {
    var self = this;

    self.callback = this.createdCallBack;

    self.bonusId = ko.observable('').extend({ required: true });
    self.allBonusTitles = ko.observableArray([]);
    self.remarks = ko.observable('').extend({ maxLength: 150 });

    self.errors = ko.validation.group(self);
    self.hasErrors = function() { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function() { self.errors.showAllMessages(); };
    self.removeErrors = function() { self.errors.showAllMessages(false); };

    self.vmObject = function() {
        return {
            BonusId: self.bonusId(),
            Remarks: self.remarks(),
        };
    };

    self.getBonusTitles = function() {
        jax.getJson(
            '/SalaryStructureBonusManage/GetBonusTitles',
            function(data) {
                var array = [];
                $.each(data, function(index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allBonusTitles(array);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getTitle = function (bonusId) {
        var result = $.grep(self.allBonusTitles(), function (e) { return e.value == bonusId; });
        return result[0].text;
    };

    self.create = function() {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        this.createdCallBack({
            bonusId: self.bonusId(),
            remarks: self.remarks(),
            bonusTitle: self.getTitle(self.bonusId())
        });
    };

    self.reset = function() {
        self.bonusId('');
        self.remarks('');
        self.removeErrors();
    };

    self.init = function() {
        self.reset();
        self.getBonusTitles();
    };
}