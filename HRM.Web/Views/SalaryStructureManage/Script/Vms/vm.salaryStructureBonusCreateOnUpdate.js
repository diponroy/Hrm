﻿function SalaryBonusCreateOnUpdateVm() {
    var self = this;

    self.id = ko.observable();
    self.callback = this.createdCallBack;
    self.allBonusTitles = ko.observableArray([]);

    self.salaryStructureId = ko.observable();
    self.salaryStructureTitle = ko.observable();
    self.bonusId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 150 });

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.postObj = function () {
        return {
            SalaryStructureId:self.salaryStructureId(),
            BonusId: self.bonusId(),
            Remarks: self.remarks()
        };
    };

    self.getBonusTitles = function () {
        jax.getJson(
            '/SalaryStructureBonusManage/GetBonusTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allBonusTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/SalaryStructureBonusManage/Create',
            self.postObj(),
            function () {
                ToastSuccess('Added successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
        this.createdCallBack();
    };

    self.reset = function () {
        self.bonusId('');
        self.remarks('');
        self.removeErrors();
    };

    self.init = function (id, title) {
        self.reset();
        self.getBonusTitles();
        self.salaryStructureId(id);
        self.salaryStructureTitle(title);
    };
}