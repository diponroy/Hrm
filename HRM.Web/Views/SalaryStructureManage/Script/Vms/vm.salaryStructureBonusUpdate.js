﻿function SalaryBonusUpdateVm() {
    var self = this;

    self.storedItemId = ko.observable();
    self.storedItemSalaryStructureId = ko.observable();
    self.storedItemSalaryStructureTitle = ko.observable();
    self.storedItemBonusId = ko.observable();
    self.storedItemRemarks = ko.observable();

    self.callback = this.createdCallBack;
    self.allSalaryStructureTitles = ko.observableArray([]);
    self.allBonusTitles = ko.observableArray([]);

    self.id = ko.observable().extend({ required: true });
    self.salaryStructureId = ko.observable().extend({ required: true });
    self.salaryStructureTitle = ko.observable();
    self.bonusId = ko.observable().extend({ required: true });
    self.remarks = ko.observable().extend({ maxLength: 150 });

    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };
    
    self.postObj = function () {
        return {
            Id: self.id(),
            SalaryStructureId: self.salaryStructureId(),
            BonusId: self.bonusId(),
            Remarks: self.remarks(),
        };
    };
    
    /*All DDL Load*/
    self.getBonusTitles = function () {
        jax.getJson(
            '/SalaryStructureBonusManage/GetBonusTitles',
            function (data) {
                var array = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    array.push({
                        text: obj.Title,
                        value: obj.Id
                    });
                });
                self.allBonusTitles(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/SalaryStructureBOnusManage/Update',
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
        this.createdCallBack();
    };

    self.reset = function () {
        self.id(self.storedItemId());
        self.salaryStructureId(self.storedItemSalaryStructureId());
        self.salaryStructureTitle(self.storedItemSalaryStructureTitle());
        self.bonusId(self.storedItemBonusId());
        self.remarks(self.storedItemRemarks());
        self.removeErrors();
    };

    self.init = function (id, salaryStructureId, salaryStructureTitle, bonusId, remarks) {
        self.getBonusTitles();
        
        self.storedItemId(id);
        self.storedItemSalaryStructureId(salaryStructureId);
        self.storedItemSalaryStructureTitle(salaryStructureTitle);
        self.storedItemBonusId(bonusId);
        self.storedItemRemarks(remarks);

        self.id(id);
        self.salaryStructureId(salaryStructureId);
        self.salaryStructureTitle(salaryStructureTitle);
        self.bonusId(bonusId);
        self.remarks(remarks);

    };
}