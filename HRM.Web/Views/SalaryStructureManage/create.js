﻿/*Validations*/
ko.validation.rules['titleUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ title: val });
        $.when(
            $.ajax({
                url: '/SalaryStructureManage/IsTitleUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist.'
};
ko.validation.registerExtenders();


function SalaryStructureCreateViewModel() {
    var self = this;
    
    self.salaryStructureAllowances = ko.observableArray([]);
    self.salaryStructureBonuses = ko.observableArray([]);

    self.salaryAllowanceCreate = new SalaryAllowanceCreateVm();
    self.salaryBonusCreate = new SalaryBonusCreateVm();


    /*Allowance and Bonus Create*/
    SalaryAllowanceCreateVm.prototype.createdCallBack = function (data) {
        $('#divSalaryAllowanceCreate').modal('hide');
        ToastSuccess("A new Salary Structure Allowance created successfully");
        
        var allowances = self.salaryStructureAllowances();
        allowances.push(data);
        self.salaryStructureAllowances(allowances);
    };
    
    SalaryBonusCreateVm.prototype.createdCallBack = function (data) {
        $('#divSalaryBonusCreate').modal('hide');
        ToastSuccess("A new Salary Structure Bonus created successfully");
        
        var bonuses = self.salaryStructureBonuses();
        bonuses.push(data);
        self.salaryStructureBonuses(bonuses);
    };
    
    self.title = ko.observable().extend({ required: true, maxlength: 100, titleUsed: false });
    self.basic = ko.observable().extend({ required: true, digit: true });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.status = ko.observable().extend({ required: true });
    self.allStatus = ko.observableArray([]);


    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    self.vmObject = function () {
        return {
            model: {
                Title: self.title(),
                Basic: self.basic(),
                Remarks: self.remarks(),
                Status: self.status(),
            },
            allowanceList: self.salaryStructureAllowances(),
            bonusList: self.salaryStructureBonuses()
        };
    };

    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/SalaryStructureManage/Create',
            self.vmObject(),
            function () {
                ToastSuccess("A new Salary Structure created successfully");
                self.reset();
                self.salaryStructureAllowances([]);
                self.salaryStructureBonuses([]);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
                self.showErrors();
            }
        );
    };
    

    /*Delete allowance and bonus*/
    self.confirmDeleteAllowance = function (item) {
        bootbox.confirm("Do you want to delete the Allowance?", function (result) {
            if (result === true) {
                self.salaryStructureAllowances.remove(item);
                ToastSuccess("Information deleted successfully");
                
            }
        });
    };
    
    self.confirmDeleteBonus = function (item) {
        bootbox.confirm("Do you want to delete the Bonus?", function (result) {
            if (result === true) {
                self.salaryStructureBonuses.remove(item);
                ToastSuccess("Information deleted successfully");
            }
        });
    };
    
    self.reset = function () {
        self.title('');
        self.basic('');
        self.remarks('');
        self.status('');
        self.removeErrors();
    };

    self.init = function () {
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.reset();

        /*load allowance titles*/
        self.salaryAllowanceCreate.init();
        self.salaryBonusCreate.init();
        
        activeParentMenu($('li.sub a[href="/SalaryStructureManage"]'));
    };
}

$(document).ready(function () {
    var viewModel = new SalaryStructureCreateViewModel();
    ko.applyBindingsWithValidation(viewModel);
    viewModel.init();
    
    $("#btnAllowance").click(function () {
        viewModel.salaryAllowanceCreate.reset();
        $('#divSalaryAllowanceCreate').removeData("modal").modal();
    });
    
    $("#btnBonus").click(function () {
        viewModel.salaryBonusCreate.reset();
        $('#divSalaryBonusCreate').removeData("modal").modal();
    });
});

