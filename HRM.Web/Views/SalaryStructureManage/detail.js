﻿function SalaryStructureViewModel() {
    var self = this;
    self.id = ko.observable();
    self.salaryStructure = ko.observable();
    self.logs = ko.observableArray([]);

    /*get all info*/
    self.getSalaryStructure = function () {
        jax.getJsonBlock(
            '/SalaryStructureManage/Get/' + self.id(),
            function (data) {
                self.salaryStructure({
                    title: data.Title,
                    basic: data.Basic,
                    remarks: data.Remarks
                });
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getLogs = function () {
        self.logs([]);
        jax.getJsonBlock(
            '/SalaryStructureManage/GetLogs/' + self.id(),
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var allowance = data[index];
                    array.push({
                        id: allowance.Id,
                        title: allowance.Title,
                        basic: allowance.Basic,
                        remarks: allowance.Remarks,
                        status: allowance.Status,
                        statusString: allowance.StatusString,
                        affectedByEmployeeName: allowance.AffectedByEmployeeLog.FullName,
                        affectedDate: clientDateTimeStringFromDate(allowance.affectedDateTime),
                        logStatus: allowance.LogStatuses
                    });
                });
                self.logs(array);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.init = function () {
        self.id($('#salaryStructureId').val());
        self.getSalaryStructure();
        self.getLogs();
    };
}

$(document).ready(function () {
    var viewModel = new SalaryStructureViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});