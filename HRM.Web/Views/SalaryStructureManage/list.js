﻿function SalaryStructureListViewModel() {
    var self = this;
    self.salaryStructures = ko.observableArray([]),

    /*search filers*/
    self.searchFilers = function () {
    };
    self.resetFilers = function () {
    };

    self.getSalaryStructureList = function (pageNo, pageSize, searchFilters) {
        self.salaryStructures([]);
        var obj = { pageNo: pageNo, pageSize: pageSize, filter: searchFilters };
        jax.postJsonBlock(
            '/SalaryStructureManage/Find',
            obj,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var salaryStructure = data[index];
                    array.push({
                        id: salaryStructure.Id,
                        title: salaryStructure.Title,
                        basic: salaryStructure.Basic,
                        remarks: salaryStructure.Remarks,
                        status: salaryStructure.Status,
                        statusString: salaryStructure.StatusString
                    });
                });
                self.salaryStructures(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    self.update = function (item) {
        window.location = '/SalaryStructureManage/Update/' + item.id;
    };

    self.detail = function (item) {
        window.location = '/SalaryStructureManage/Detail/' + item.id;
    };

    self.confirmDelete = function (item) {
        bootbox.confirm("Are you sure to delete the Structure ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };
    self.delete = function (id) {
        var obj = { id: id, };
        jax.postJsonBlock(
            '/SalaryStructureManage/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getSalaryStructureList(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.init = function () {
        self.resetFilers();
        self.getSalaryStructureList(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/SalaryStructureManage"]'));
    };
}

$(document).ready(function () {
    var viewModel = new SalaryStructureListViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});