﻿/*Validations*/
ko.validation.rules['TitleUsedExceptItself'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#salaryStructureId').val(), title: val });
        $.when(
            $.ajax({
                url: '/SalaryStructureManage/TitleUsedExceptItself',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            if (textStatus === 'success') {
                isUsed = data;
            } else {
                ToastError(xhr);
                isUsed = null;
            }
        });
        return isUsed === otherVal;
    },
    message: 'This title already exist.'
};
ko.validation.registerExtenders();

/*View models*/
function SalaryStructureUpdateViewModel() {
    var self = this;
    
    self.salaryAllowances = ko.observableArray([]);
    self.salaryBonuses = ko.observableArray([]);
  
    /*field Salary structure*/
    self.id = ko.observable();
    self.title = ko.observable().extend({ required: true, maxLength: 100, TitleUsedExceptItself: false });
    self.basic = ko.observable().extend({ required: true, digit: true });
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.status = ko.observable().extend({ required: true, });
    self.allStatus = ko.observableArray([]);

    
    /*Allowance create and update*/
    self.salaryAllowanceCreateOnUpdate = new SalaryAllowanceCreateOnUpdateVm();
    self.salaryAllowanceUpdate = new SalaryAllowanceUpdateVm();
    
    SalaryAllowanceCreateOnUpdateVm.prototype.createdCallBack = function () {
        $('#divSalaryAllowanceCreateOnUpdate').modal('hide');
        self.getSalaryAllowanceList();
    };
    
    SalaryAllowanceUpdateVm.prototype.createdCallBack = function () {
        $('#divSalaryAllowanceUpdate').modal('hide');
        self.getSalaryAllowanceList();
    };

    /*Bonus create and update*/
    self.salaryBonusCreateOnUpdate = new SalaryBonusCreateOnUpdateVm();
    self.salaryBonusUpdate = new SalaryBonusUpdateVm();
    
    SalaryBonusCreateOnUpdateVm.prototype.createdCallBack = function () {
        $('#divSalaryBonusCreateOnUpdate').modal('hide');
        self.getSalaryBonusList();
    };
    
    SalaryBonusUpdateVm.prototype.createdCallBack = function () {
        $('#divSalaryBonusUpdate').modal('hide');
        self.getSalaryBonusList();
    };

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () { return (self.errors().length > 0) ? true : false; };
    self.showErrors = function () { self.errors.showAllMessages(); };
    self.removeErrors = function () { self.errors.showAllMessages(false); };

    /*objects to post*/
    self.postObj = function () {
        return {
            Id: self.id(),
            Title: self.title(),
            Basic: self.basic(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };

    self.loadSalaryStructure = function () {
        jax.getJson(
            '/SalaryStructureManage/Get/' + self.id(),
            function (data) {
                self.title(data.Title);
                self.basic(data.Basic);
                self.remarks(data.Remarks);
                self.status(data.StatusString);
                
                self.getSalaryAllowanceList();

                self.removeErrors();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/SalaryStructureManage/Update',
            self.postObj(),
            function () {
                ToastSuccess('Information updated successfully');
                self.loadSalaryStructure();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getSalaryAllowanceList = function () {
        self.salaryAllowances([]);
        jax.postJsonBlock(
            '/SalaryStructureAllowanceManage/GetSalaryAllowanceList/' + self.id(),
            null,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var salaryAllowance = data[index];
                    array.push({
                        id: salaryAllowance.Id,
                        salaryStructureId: salaryAllowance.SalaryStructure.Id,
                        salaryStructureTitle: salaryAllowance.SalaryStructure.Title,
                        allowanceId: salaryAllowance.Allowance.Id,
                        allowanceTitle: salaryAllowance.Allowance.Title,
                        remarks: salaryAllowance.Remarks,
                        status: salaryAllowance.Status,
                        statusString: salaryAllowance.StatusString
                    });
                });
                self.salaryAllowances(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getSalaryBonusList = function () {
        self.salaryBonuses([]);
        jax.postJsonBlock(
            '/SalaryStructureBonusManage/GetSalaryBonusList/' + self.id(),
            null,
            function (data) {
                var array = new Array();
                $.each(data, function (index) {
                    var salaryBonus = data[index];
                    array.push({
                        id: salaryBonus.Id,
                        salaryStructureId: salaryBonus.SalaryStructure.Id,
                        salaryStructureTitle: salaryBonus.SalaryStructure.Title,
                        bonusId: salaryBonus.Bonus.Id,
                        bonusTitle: salaryBonus.Bonus.Title,
                        remarks: salaryBonus.Remarks,
                        status: salaryBonus.Status,
                        statusString: salaryBonus.StatusString
                    });
                });
                self.salaryBonuses(array);
            },
            function (qxhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*Update allowance and bonus*/
    self.updateAllowance = function(item) {
        $('#divSalaryAllowanceUpdate').removeData("modal").modal();
        self.salaryAllowanceUpdate.init(item.id, item.salaryStructureId, item.salaryStructureTitle, item.allowanceId, item.remarks);
    };
    
    self.updateBonus = function (item) {
        $('#divSalaryBonusUpdate').removeData("modal").modal();
        self.salaryBonusUpdate.init(item.id, item.salaryStructureId, item.salaryStructureTitle, item.bonusId, item.remarks);
    };
    
    /*Delete allowance and bonus*/
    self.confirmDeleteAllowance = function(item) {
        bootbox.confirm("Do you want to delete the Allowance?", function (result) {
            if (result === true) {
                self.deleteAllowance(item);
            }
        });
    };

    self.deleteAllowance = function (item) {
        var obj = { model: item };
        jax.postJsonBlock(
            '/SalaryStructureAllowanceManage/Delete',
            obj,
            function() {
                ToastSuccess("Information deleted successfully");
                self.getSalaryAllowanceList();
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.confirmDeleteBonus = function (item) {
        bootbox.confirm("Do you want to delete the Bonus?", function (result) {
            if (result === true) {
                self.deleteBonus(item);
            }
        });
    };

    self.deleteBonus = function (item) {
        var obj = { model: item };
        jax.postJsonBlock(
            '/SalaryStructureBonusManage/Delete',
            obj,
            function () {
                ToastSuccess("Information deleted successfully");
                self.getSalaryBonusList();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*initialize & resets*/
    self.reset = function () {
        self.loadSalaryStructure();
    };
    
    self.init = function () {
        self.id($('#salaryStructureId').val());
        
        var status = [];
        status.push({ name: 'Active' });
        status.push({ name: 'Inactive' });
        self.allStatus(status);
        self.loadSalaryStructure();

        self.getSalaryAllowanceList();
        self.getSalaryBonusList();
       
        self.salaryAllowanceCreateOnUpdate.init();
        self.salaryBonusCreateOnUpdate.init();
        
        self.salaryAllowanceUpdate.init();
        self.salaryBonusUpdate.init();
        
        activeParentMenu($('li.sub a[href="/SalaryStructureManage"]'));
    };
}

$(document).ready(function () {
    var vm = new SalaryStructureUpdateViewModel();
    ko.applyBindingsWithValidation(vm);
    vm.init();
    
    $("#btnCreateAllowance").click(function () {
        vm.salaryAllowanceCreateOnUpdate.reset();
        $('#divSalaryAllowanceCreateOnUpdate').removeData("modal").modal();
        vm.salaryAllowanceCreateOnUpdate.init(vm.id(), vm.title());
    });

    $("#btnCreateBonus").click(function () {
        vm.salaryBonusCreateOnUpdate.reset();
        $('#divSalaryBonusCreateOnUpdate').removeData("modal").modal();
        vm.salaryBonusCreateOnUpdate.init(vm.id(), vm.title());
    });
    
});