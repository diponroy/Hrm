﻿/*Validations*/
ko.validation.rules['combinationUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var obj = {
            CompanyId: otherVal().companyId,
            BranchId: otherVal().branchId,
            DepartmentId: val,
        };
        var json = JSON.stringify({ model: obj });
        $.when(
            $.ajax({
                url: '/WorkStationUnitManage/IscombinationUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal().isUsed;
    },
    message: 'This combination of company, branch, department is already in use.'
};
ko.validation.registerExtenders();


function WorkStationViewModel() {
    var self = this;
    
    /*drop down lists*/
    self.allCompany = ko.observableArray([]);
    self.allBranch = ko.observableArray([]);
    self.allDepartment = ko.observableArray([]);
    self.allWrokStation = ko.observableArray([]);
    self.allEmployee = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    /*fields*/
    self.companyId = ko.observable();
    self.branchId = ko.observable();
    self.departmentId = ko.observable().extend({
        combinationUsed : ko.computed(function() {
            return {                
                companyId: self.companyId(),
                branchId: self.branchId(),
                isUsed: false
            };
        })
    });
    self.parentWorkStationId = ko.observable();
    self.dateOfCreation = ko.observable().extend({ required: true });
    self.employeeIdIncharge = ko.observable();
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.status = ko.observable().extend({ required: true });
    
    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return (self.errors().length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    /*post object*/
    self.postObject = function() {
        return {            
            CompanyId: self.companyId(),
            BranchId: self.branchId(),
            DepartmentId: self.departmentId(),
            ParentId: self.parentWorkStationId(),
            DateOfCreation: self.dateOfCreation(),
            EmployeeIdAsInCharge: self.employeeIdIncharge(),
            Remarks: self.remarks(),
            Status: self.status()
        };
    };
    
    
    /*server gets*/
    self.getCompanies = function () {
        jax.getJson(
            '/WorkStationUnitManage/GetCompanies',
            function (data) {
                var arr = [];
                $.each(data, function(index) {
                    var obj = data[index];
                    arr.push({ text: obj.Name, value: obj.Id });
                });
                self.allCompany(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getBranches = function () {
        jax.getJson(
            '/WorkStationUnitManage/GetBranches',
            function (data) {
                var arr = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    arr.push({ text: obj.Name, value: obj.Id });
                });
                self.allBranch(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getDepartments = function () {
        jax.getJson(
            '/WorkStationUnitManage/GetDepartments',
            function (data) {
                var arr = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    arr.push({ text: obj.Name, value: obj.Id });
                });
                self.allDepartment(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getWorkStations = function () {
        jax.getJson(
            '/WorkStationUnitManage/GetWorkStations',
            function (data) {
                var arr = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    arr.push({ text: obj.Remarks, value: obj.Id });
                });
                self.allWrokStation(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.getEmployees = function () {
        jax.getJson(
            '/WorkStationUnitManage/GetEmployees',
            function (data) {
                var arr = [];
                $.each(data, function (index) {
                    var obj = data[index];
                    arr.push({ text: obj.FullName, value: obj.Id });
                });
                self.allEmployee(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };


    /*Server posts*/
    self.create = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/WorkStationUnitManage/TryToCreate',
            { model: self.postObject()},
            function (data) {
                ToastSuccess('Work Station created successfully');
                self.reset();
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.reset = function () {
        self.companyId("");
        self.branchId("");
        self.departmentId("");
        self.parentWorkStationId("");
        self.dateOfCreation(currentDate());
        self.employeeIdIncharge("");
        self.remarks("");
        self.status("");

        self.removeErrors();
    };

    /*resets*/
    self.init = function () {
        self.dateOfCreation(currentDate());

        self.getCompanies();
        self.getBranches();
        self.getDepartments();
        self.getWorkStations();
        self.getEmployees();
        
        var arr = [];
        arr.push({ text: 'Active', value: 'Active' });
        arr.push({ text: 'Inactive', value: 'Inactive' });
        self.allStatus(arr);
        
        activeParentMenu($('li.sub a[href="/WorkStationUnitManage"]'));
    };
}


$(document).ready(function () {
    
    $('.datepicker').datepicker();

    var vm = new WorkStationViewModel();
    vm.init();
    ko.applyBindingsWithValidation(vm);
    
    $('#btnShowDatePicker').click(function () {
        $('.datepicker').focus();
    });
    $('#btnResetDatePicker').click(function () {
        vm.dateOfCreation(currentDate());
    });
});

