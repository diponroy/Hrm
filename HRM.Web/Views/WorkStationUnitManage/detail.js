﻿
function WorkStationViewModel() {
    var self = this;
    self.logs = ko.observable([]);

    /*fields*/
    self.id = ko.observable();
    self.companyName = ko.observable();
    self.branchName = ko.observable();
    self.departmentName = ko.observable();
    self.parentCompanyName = ko.observable();
    self.parentBranchName = ko.observable();
    self.parentDepartmentName = ko.observable();
    self.dateOfCreation = ko.observable();
    self.dateOfClosing = ko.observable();
    self.remarks = ko.observable();
    self.status = ko.observable();

    /*server gets*/
    self.getWorkStaion = function(id) {
        jax.getJsonBlock(
            '/WorkStationUnitManage/StationDetail/' + id,
            function(data) {
                var isNull = function(variable) {
                    return variable == null;
                };
                self.companyName(isNull(data.Company) ? '--' : data.Company.Name);
                self.branchName(isNull(data.Branch) ? '--' : data.Branch.Name);
                self.departmentName(isNull(data.Department) ? '--' : data.Department.Name);
                self.parentCompanyName(isNull(data.ParentWorkStationUnit) ? '--' : data.ParentWorkStationUnit.Company.Name);
                self.parentBranchName(isNull(data.ParentWorkStationUnit) ? '--' : data.ParentWorkStationUnit.Branch.Name);
                self.parentDepartmentName(isNull(data.ParentWorkStationUnit) ? '--' : data.ParentWorkStationUnit.Department.Name);
                self.dateOfCreation(clientDateStringFromDate(data.DateOfCreation));
                self.dateOfClosing(isNull(data.DateOfClosing) ? '--' : clientDateStringFromDate(data.Company.Name));
                self.remarks(data.Remarks);
                self.status(data.StatusString);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getWorkStations = function(pageNo, pageSize, searchFilters) {
        self.logs([]);
        jax.getJsonBlock(
            '/WorkStationUnitManage/GetLogs/' + self.id(),
            function(data) {
                var arr = new Array();
                $.each(data, function(index) {
                    var aStation = data[index];
                    arr.push({
                        id: aStation.Id,
                        companyName: (aStation.CompanyLog == null) ? '' : aStation.CompanyLog.Name,
                        branchName: (aStation.BranchLog == null) ? '' : aStation.BranchLog.Name,
                        departmentName: (aStation.DepartmentLog == null) ? '' : aStation.DepartmentLog.Name,

                        parentCompanyName: (aStation.ParentWorkStationUnitLog != null && aStation.ParentWorkStationUnitLog.CompanyLog != null) ? aStation.ParentWorkStationUnitLog.CompanyLog.Name : '',
                        parentBranchName: (aStation.ParentWorkStationUnitLog != null && aStation.ParentWorkStationUnitLog.BranchLog != null) ? aStation.ParentWorkStationUnitLog.BranchLog.Name : '',
                        parentDepartmentName: (aStation.ParentWorkStationUnitLog != null && aStation.ParentWorkStationUnitLog.DepartmentLog != null) ? aStation.ParentWorkStationUnitLog.DepartmentLog.Name : '',

                        inCharge: 'InCharge',
                        remarks: aStation.Remarks,
                        dateOfCreation: clientDateStringFromDate(aStation.DateOfCreation),
                        dateOfClosing: (aStation.DateOfClosing === null) ? '---' : clientDateStringFromDate(aStation.DateOfClosing),
                        isClosed: aStation.DateOfClosing != null,
                        status: aStation.Status,
                        statusString: aStation.StatusString,


                        affectedByEmployeeId: aStation.AffectedByEmployeeId,
                        affectedByName: aStation.AffectedByEmployee.FullName,
                        affectedDate: clientDateTimeStringFromDate(aStation.AffectedDateTime),
                        logStatus: aStation.LogStatuses
                    });
                });
                self.logs(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*resets*/
    self.reset = function() {
        self.getWorkStaion(self.id());
        self.getWorkStations(1, 25, null);
    };

    self.init = function() {
        self.id($('#txtWorkStationId').val());
        self.reset();
    };
}


$(document).ready(function() {
    var vm = new WorkStationViewModel();
    vm.init();
    ko.applyBindingsWithValidation(vm);
});

