﻿
function WorkStationUnitViewModel() {
    var self = this;
    self.workStations = ko.observableArray([]);
    
    /*search filers*/
    self.searchFilers = function () {

    };
    self.resetFilers = function () {

    };

    self.getWorkStations = function (pageNo, pageSize, searchFilters) {
        self.workStations([]);
        var obj = {
            pageNo: pageNo,
            pageSize: pageSize,
            filter: searchFilters
        };
        jax.postJsonBlock(
            '/WorkStationUnitManage/Find',
            obj,
            function (data) {
                var arr = new Array();
                $.each(data, function (index) {
                    var aStation = data[index];
                    arr.push({
                        id: aStation.Id,
                        companyName: (aStation.Company == null) ? '' : aStation.Company.Name,
                        branchName: (aStation.Branch == null) ? '' : aStation.Branch.Name,
                        departmentName: (aStation.Department == null) ? '' : aStation.Department.Name,
                        
                        parentCompanyName: (aStation.ParentWorkStationUnit != null && aStation.ParentWorkStationUnit.Company != null) ? aStation.ParentWorkStationUnit.Company.Name : '',
                        parentBranchName: (aStation.ParentWorkStationUnit != null && aStation.ParentWorkStationUnit.Branch != null) ? aStation.ParentWorkStationUnit.Branch.Name : '',
                        parentDepartmentName: (aStation.ParentWorkStationUnit != null && aStation.ParentWorkStationUnit.Department != null) ? aStation.ParentWorkStationUnit.Department.Name : '',

                        dateOfCreation: clientDateStringFromDate(aStation.DateOfCreation),
                        dateOfClosing: (aStation.DateOfClosing === null) ? '---' : clientDateStringFromDate(aStation.DateOfClosing),
                        isClosed: aStation.DateOfClosing != null,
                        status: aStation.Status,
                        statusString: aStation.StatusString
                    });
                });
                self.workStations(arr);
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.showToEdit = function (item) {
        window.location = '/WorkStationUnitManage/Update/' + item.id;
    };

    self.showDetail = function (item) {
        window.location = '/WorkStationUnitManage/Detail/' + item.id;
    };

    self.delete = function (id) {
        var obj = {
            id: id,
        };
        jax.postJsonBlock(
            '/WorkStationUnitManage/Delete',
            obj,
            function (data) {
                ToastSuccess("Work Station deleted successfully");
                self.getWorkStations(1, 25, self.searchFilers());
            },
            function (qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };
    
    self.confirmToDelete = function (item) {
        bootbox.confirm("Are you sure, you want to delete the Work Station ?", function (result) {
            if (result === true) {
                self.delete(item.id);
            }
        });
    };

    self.init = function () {
        self.resetFilers();
        self.getWorkStations(1, 25, self.searchFilers());
        
        activeParentMenu($('li.sub a[href="/WorkStationUnitManage"]'));
    };

}

$(document).ready(function () {
    var vm = new WorkStationUnitViewModel();
    ko.applyBindings(vm);
    vm.init();
});