﻿/*Validations*/
ko.validation.rules['combinationUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var obj = {
            model: {
                CompanyId: otherVal().companyId,
                BranchId: otherVal().branchId,
                DepartmentId: val,
            },
            stationId: otherVal().id
        };
        var json = JSON.stringify(obj);
        $.when(
            $.ajax({
                url: '/WorkStationUnitManage/IscombinationUsedExcept',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
            if (textStatus != 'success') {
                ToastError(jqXhr);
            }
        });
        return isUsed === otherVal().isUsed;
    },
    message: 'This combination of company, branch, department is already in use.'
};
ko.validation.registerExtenders();

function WorkStationViewModel() {
    var self = this;

    /*drop down lists*/
    self.allCompany = ko.observableArray([]);
    self.allBranch = ko.observableArray([]);
    self.allDepartment = ko.observableArray([]);
    self.allWrokStation = ko.observableArray([]);
    self.allEmployee = ko.observableArray([]);
    self.allStatus = ko.observableArray([]);

    /*fields*/
    self.id = ko.observable();
    self.companyId = ko.observable();
    self.branchId = ko.observable();
    self.departmentId = ko.observable().extend({
        combinationUsed: ko.computed(function () {
            return {
                id: self.id(),
                companyId: self.companyId(),
                branchId: self.branchId(),
                isUsed: false
            };
        })
    });
    self.parentWorkStationId = ko.observable();
    self.dateOfCreation = ko.observable().extend({ required: true });
    self.dateOfClosing = ko.observable().extend({ required: true });
    self.employeeIdIncharge = ko.observable();
    self.remarks = ko.observable().extend({ maxLength: 150 });
    self.status = ko.observable().extend({ required: true });
    
    /*post obj*/
    self.postObject = function () {
        return {
            Id: self.id(),
            CompanyId: self.companyId(),
            BranchId: self.branchId(),
            DepartmentId: self.departmentId(),
            ParentId: self.parentWorkStationId(),
            DateOfCreation: self.dateOfCreation(),
            DateOfClosing: (self.hasClosingDate()) ? self.dateOfClosing() : null,
            EmployeeIdAsInCharge: self.employeeIdIncharge(),
            Remarks: self.remarks(),
            Status: (self.hasClosingDate()) ? null : self.status()
        };
    };

    /*flags*/
    self.hasClosingDate = ko.observable(false);

    self.setClosedDateTime = function() {
        self.hasClosingDate(true);
        self.dateOfClosing(currentDate());
    };

    self.removeClosedDateTime = function() {
        self.hasClosingDate(false);
        self.dateOfClosing(null);
    };

    /*errors*/
    self.errorsWithOutStatus = ko.validation.group([self.dateOfCreation, self.dateOfClosing, self.remarks]);
    self.errorsWithStatus = ko.validation.group([self.dateOfCreation, self.remarks, self.status]);
    self.errors = ko.computed(function () {
        return self.hasClosingDate() ? self.errorsWithOutStatus : self.errorsWithStatus;
    }, this);
    self.hasErrors = function () {
        return ((self.errors()()).length > 0) ? true : false;
    };
    self.showErrors = function () {
        self.errors().showAllMessages();
    };
    self.removeErrors = function () {
        self.errors().showAllMessages(false);
    };

    /*server gets*/
    self.getCompanies = function() {
        return jax.getJson(
            '/WorkStationUnitManage/GetCompanies',
            function(data) {
                var arr = [];
                $.each(data, function(index) {
                    var obj = data[index];
                    arr.push({ text: obj.Name, value: obj.Id });
                });
                self.allCompany(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getBranches = function() {
        return jax.getJson(
            '/WorkStationUnitManage/GetBranches',
            function(data) {
                var arr = [];
                $.each(data, function(index) {
                    var obj = data[index];
                    arr.push({ text: obj.Name, value: obj.Id });
                });
                self.allBranch(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getDepartments = function() {
        return jax.getJson(
            '/WorkStationUnitManage/GetDepartments',
            function(data) {
                var arr = [];
                $.each(data, function(index) {
                    var obj = data[index];
                    arr.push({ text: obj.Name, value: obj.Id });
                });
                self.allDepartment(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getWorkStations = function(id) {
        return jax.getJson(
            '/WorkStationUnitManage/GetPossibleParentWorkStations/' +id,
            function(data) {
                var arr = [];
                $.each(data, function(index) {
                    var obj = data[index];
                    arr.push({ text: obj.Remarks, value: obj.Id });
                });
                self.allWrokStation(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getEmployees = function() {
        return jax.getJson(
            '/WorkStationUnitManage/GetEmployees',
            function(data) {
                var arr = [];
                $.each(data, function(index) {
                    var obj = data[index];
                    arr.push({ text: obj.FullName, value: obj.Id });
                });
                self.allEmployee(arr);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    self.getWorkStaion = function(id) {
        return jax.getJsonBlock(
            '/WorkStationUnitManage/GetWorkStation/' + id,
            function(data) {
                var ifNull = function(variable, value) {
                    return (variable == null) ? value : variable;
                };
                self.companyId(ifNull(data.CompanyId, ''));
                self.branchId(ifNull(data.BranchId, ''));
                self.departmentId(ifNull(data.DepartmentId, ''));
                self.parentWorkStationId(ifNull(data.ParentId, ''));
                self.dateOfCreation(clientDate(data.DateOfCreation));
                if (data.DateOfClosing != null) {
                    self.hasClosingDate(true);
                    self.dateOfClosing(clientDate(data.DateOfClosing));
                } else {
                    self.removeClosedDateTime();
                }
                self.employeeIdIncharge(ifNull(data.EmployeeIdAsInCharge, ''));
                self.remarks(data.Remarks);
                self.status(data.Status);
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*Server posts*/
    self.update = function () {
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        jax.postJsonBlock(
            '/WorkStationUnitManage/TryToUpdate',
            { model: self.postObject() },
            function(data) {
                ToastSuccess('Work Station updated successfully');
                //self.reset();
                self.getWorkStaion(self.id());
            },
            function(qXhr, textStatus, error) {
                ToastError(error);
            }
        );
    };

    /*resets*/
    self.reset = function() {
        jax.block();

        var arr = [];
        arr.push({ text: 'Active', value: 0 });
        arr.push({ text: 'Inactive', value: 1 });
        self.allStatus(arr);

        var calls = [];
        calls.push(self.getCompanies());
        calls.push(self.getBranches());
        calls.push(self.getDepartments());
        calls.push(self.getWorkStations(self.id()));
        calls.push(self.getEmployees());
        $.when.apply(this, calls).done(function () {           
            $.when.apply(this, self.getWorkStaion(self.id())).done(function () {
                self.removeErrors();
            });
        });       
    };

    self.init = function() {
        self.id($('#txtWorkStationId').val());
        self.reset();
        
        activeParentMenu($('li.sub a[href="/WorkStationUnitManage"]'));
    };
}


$(document).ready(function () {

    var vm = new WorkStationViewModel();
    vm.init();
    ko.applyBindingsWithValidation(vm);

    $('#btnShowCreationDate').click(function() {
        $('#btnDateOfCreation').focus();
    });
    $('#btnResetCreationDate').click(function() {
        vm.dateOfCreation(currentDate());
    });

    $('#btnShowClosingDate').click(function() {
        $('#btnDateOfClosing').focus();
    });
    $('#btnResetClosingDate').click(function() {
        vm.dateOfClosing(currentDate());
    });
});

